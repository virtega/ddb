<?php

namespace App\Tests\TestTraits;

use App\Entity\UserRoles;
use App\Security\User\LdapUser;
use App\Security\User\LdapUserProvider;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


trait LdapUserTrait
{
    /**
     * List of role-key with expected Role Hierarchy
     *
     * @var array
     */
    private static $ldapUserRolesHierarchy = array(
        'admin'   => array(UserRoles::ADMIN,    UserRoles::EDITOR,   UserRoles::USER,     UserRoles::VISITOR),
        'editor'  => array(UserRoles::EDITOR,   UserRoles::USER,     UserRoles::VISITOR),
        'user'    => array(UserRoles::USER,     UserRoles::VISITOR),
        'visitor' => array(UserRoles::VISITOR),
    );

    /**
     * Hold previously changed role, avoid changing on subsequence request
     *
     * @var ?string
     */
    private $ldapUserRoleChanged;


    /**
     * Method to set session & cookie for LDAP user login.
     * Since External user provider, login user for real on lDAP.
     *
     * @param  array    $role      @see self::$ldapUserRolesHierarchy
     * @param  ?string  $username
     * @param  ?string  $email
     * @param  ?string  $password
     *
     * @return LdapUser
     */
    private function loginLdap(string $role, ?string $username = null, ?string $email = null, ?string $password = null): LdapUser
    {
        $username = ($username ?? getEnv('LDAP_LOGIN_USERNAME'));
        $email    = ($email    ?? getEnv('LDAP_LOGIN_EMAIL'));
        $password = ($password ?? getEnv('LDAP_LOGIN_PASSWORD'));

        $this->adjustUserRoles($role);

        $ldapProvider = $this->client->getContainer()->get('test.' . LdapUserProvider::class);

        // query user
        $ldapProvider->bindingLdapForQuery($username, $password);
        $user = $ldapProvider->loadUserByUsername($email);

        // mock session
        $token = new UsernamePasswordToken($user, array('_email' => $email, '_password' => $password), 'psl_user', $user->getRoles());

        $session = $this->client->getContainer()->get('session');
        $session->set('_security_main', serialize($token));
        $session->save();

        // session on cookie
        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);

        return $user;
    }

    /**
     * Clear session and cookie
     *
     * @return void
     */
    private function loggoutLdap()
    {
        // clear session
        $session = $this->client->getContainer()->get('session');
        $session->set('_security_main', null);
        $session->save();

        // clear cookie
        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    /**
     * Method to update API Request Headers - populate for API Request
     *
     * @param  array    $headers
     * @param  array    $role        @see self::$ldapUserRolesHierarchy
     * @param  string   $header_key
     * @param  ?string  $username
     * @param  ?string  $password
     *
     * @return void
     */
    private function ldapUserApiHeading(array &$headers,  string $role, string $heading_key = 'HTTP_X-API-TOKEN', ?string $username = null, ?string $password = null)
    {
        $this->adjustUserRoles($role);

        $headers[$heading_key]    = getenv('SECURITY_API_TOKEN');
        $headers['PHP_AUTH_USER'] = ($username ?? getenv('LDAP_LOGIN_USERNAME'));
        $headers['PHP_AUTH_PW']   = ($password ?? getenv('LDAP_LOGIN_PASSWORD'));
    }


    /**
     * Method to physically change User Role on Database Level
     *
     * @param  string      $new_role Role key @see $ldapUserRolesHierarchy
     * @param  string|null $username; defaulting to getenv('LDAP_LOGIN_USERNAME')
     *
     * @return void
     */
    private function adjustUserRoles(string $new_role, ?string $username = null)
    {
        if (
            (empty($this->ldapUserRoleChanged)) ||
            ($new_role != $this->ldapUserRoleChanged)
        ) {
            if (empty($username)) {
                $username = getenv('LDAP_LOGIN_USERNAME');
            }

            $roles = (self::$ldapUserRolesHierarchy[$new_role] ?? array());

            $em = $this->client->getContainer()->get('doctrine.orm.default_entity_manager');
            $userRole = $em->getRepository(UserRoles::class);

            $user = $userRole->getUserRoleEnt($username);
            $userRole->createUpdateUserRoles($user, $user->getUsername(), $roles, false);

            $this->ldapUserRoleChanged = $new_role;
        }
    }
}
