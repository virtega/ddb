<?php

namespace App\Tests\TestTraits;

/**
 * Helper method to change $_ENV config, so Client adapt to them.
 *
 * @since  1.2.0
 */
trait EnvHardChangeTrait
{
    /**
     * @var array
     */
    private $envToPreserve = [];

    /**
     * @var array
     */
    private $envOriginalEnv = [];

    /**
     * Store envs locally.
     */
    private function _envBackup(): void
    {
        foreach ($this->envToPreserve as $key) {
            $this->envOriginalEnv[$key] = $_ENV[$key];
        }
    }

    /**
     * Method to override single ENV value
     *
     * @param string $key
     * @param string $value
     * @param bool   $backup  Mark to backup and restore given key
     *
     * @return [type] [description]
     */
    private function _envOverride(string $key, string $value, bool $backup = false): void
    {
        if ($backup) {
            if (!in_array($key, $this->envToPreserve)) {
                $this->envToPreserve[]      = $key;
                $this->envOriginalEnv[$key] = $_ENV[$key];
            }
        }

        $_ENV[$key] = $value;
    }

    /**
     * Restored locally
     */
    private function _envRestore(): void
    {
        foreach ($this->envOriginalEnv as $key => $val) {
            $_ENV[$key] = $val;
        }
    }
}
