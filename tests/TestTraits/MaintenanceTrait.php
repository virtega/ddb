<?php

namespace App\Tests\TestTraits;

trait MaintenanceTrait
{
    /**
     * Maintenance File name
     *
     * @var string
     */
    private static $file = '.maintenance.lock';

    /**
     * Project Site Root Absolute Path
     *
     * @var string
     */
    private $root_path;

    /**
     * Method to set Project Site Path
     *
     * @param  string $path
     *
     * @return void
     */
    private function mntnncSetRootPath(string $path)
    {
        $this->root_path = $path;
    }

    /**
     * Method to get Maintenance file Absolute Path
     *
     * @return string
     */
    private function mntnncGetFilePath()
    {
        static $file;

        if (empty($file)) {
            if (empty($this->root_path)) {
                $this->mntnncSetRootPath( $this->client->getKernel()->getProjectDir() );
            }
            $file = $this->root_path . DIRECTORY_SEPARATOR . self::$file;
        }

        return $file;
    }

    /**
     * Method to place Maintenance File
     *
     * @return void
     */
    private function mntnncSetMaintenanceModeOn()
    {
        if (!file_exists($this->mntnncGetFilePath())) {
            file_put_contents($this->mntnncGetFilePath(), getenv('APP_ENV') . '|UnitTest|' . time());
        }
    }

    /**
     * Method to remove Maintenance File
     *
     * @return [type] [description]
     */
    private  function mntnncSetMaintenanceModeOff()
    {
        if (file_exists($this->mntnncGetFilePath())) {
            unlink($this->mntnncGetFilePath());
        }
    }
}
