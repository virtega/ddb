<?php

namespace App\Tests\Functional;

use App\Utility\LotusConnection;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Dotenv\Dotenv;

/**
 * @testdox FUNCTIONAL | Base Setup
 */
class BaseFunctionalTest extends TestCase
{
    static $ormPurgerExcludes = [
        'country_taxonomy',
    ];

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        // reload Dotenv and enforce putenv
        (new Dotenv(true))->loadEnv(dirname(__DIR__) . '/../.env');
    }

    public static function getLotusStatus(): bool
    {
        static $lotusCurrentStatus;

        do {
            if (null !== $lotusCurrentStatus) {
                break;
            }

            $dsn = getenv('LOTUS_DB_DSN');
            if (empty($dsn)) {
                break;
            }

            $dsnOpts = LotusConnection::_extractDsnString($dsn);
            if (
                (empty($dsnOpts['host'])) ||
                (empty($dsnOpts['port']))
            ) {
                break;
            }

            exec("ping -c 1 -W 3 {$dsnOpts['host']}", $output, $status);
            if (0 != $status) {
                break;
            }

            $lotusCurrentStatus = true;
        } while (false);

        return (!empty($lotusCurrentStatus));
    }

    public static function getTestEmail(): string
    {
        return 'test@test-this-should-went-no-where-BB1A035.com';
    }

    /**
     * @testdox Checking PHP Settings
     */
    public function testPhpSettings()
    {
        $timezone = date_default_timezone_get();
        $this->assertSame('UTC', $timezone);
    }
}
