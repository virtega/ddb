<?php

namespace App\Tests\Functional\Utility;

use App\Utility\MsSqlRemoteConnection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Psr\Log\LoggerInterface;

/**
 * @testdox FUNCTIONAL | Utility | MsSqlRemoteConnection
 */
class MsSqlRemoteConnectionUtilityFunctionalTest extends WebTestCase
{
    /**
     * @var LoggerInterface  Mocked class basics
     */
    private $logger;

    protected function setUp(): void
    {
        $this->logger = $this->createConfiguredMock(LoggerInterface::class, [
            'debug' => true,
            'error' => true,
            'info'  => true,
        ]);
    }

    /**
     * @testdox Testing class initiation and start up info
     */
    public function testInitiateClassAndInfo(): void
    {
        $remote = new MsSqlRemoteConnection($this->logger, 'opt1=D;opt2=E;port=2;host=A:1;dbname=B;', 'userNm', 'pa$$word!', null, getenv('MSSQLREMOTECONN_TIMEOUT'));

        $this->assertInstanceOf(MsSqlRemoteConnection::class, $remote);

        $this->assertSame([
            'dsn'       => 'opt1=D;opt2=E;port=2;host=A:1;dbname=B;',
            'host'      => 'A',
            'port'      => '1',
            'dbname'    => 'B',
            'options'   => [
                'opt1' => 'D',
                'opt2' => 'E',
            ],
            'username'  => 'userNm',
            'password'  => '*********',
            'connected' => false,
            'error'     => true,
            'error-msg' => 'Not Initiated',
        ], $remote->getInfo());

        $this->assertFalse($remote->isConnected());
    }

    /**
     * @testdox Testing get connection without initiation
     */
    public function testGetConnectionSkipInit(): void
    {
        $remote = new MsSqlRemoteConnection($this->logger, 'opt1=D;opt2=E;port=2;host=A:1;dbname=B;', 'userNm', 'pa$$word!');

        $connection = null;
        try {
            $connection = $remote->getConnection();
        }
        catch (\Exception $e) {
            $this->assertSame('Not Initiated', $e->getMessage());
        }

        $this->assertNull($connection);
    }

    /**
     * @dataProvider getMakeConnectionErrorsProvider
     *
     * @testdox Testing makeConnection() Stacks
     */
    public function testMakeConnectionErrors($dsn, $username, $password, $error): void
    {
        $remote = new MsSqlRemoteConnection($this->logger, $dsn, $username, $password, null, getenv('MSSQLREMOTECONN_TIMEOUT'));

        $connection = null;
        try {
            $connection = $remote->makeConnection(true);
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\MsSqlRemoteConnectionException::class, $e);
            $this->assertStringContainsStringIgnoringCase('MySQL Remote Database Connection failed', $e->getMessage());
            $this->assertStringContainsStringIgnoringCase($error, $e->getMessage());
        }

        $this->assertNull($connection);
    }

    public function getMakeConnectionErrorsProvider(): array
    {
        return [
            // #0
            [
                '',
                '',
                '',
                'Incomplete Access Details'
            ],
            // #1
            [
                'host=example.google.com',
                '',
                '',
                'Incomplete Access Details'
            ],
            // #2
            [
                '',
                'user.name',
                '',
                'Incomplete Access Details'
            ],
            // #3
            [
                '',
                '',
                'pass$$word',
                'Incomplete Access Details'
            ],
            // #4
            [
                'host=example.google.com',
                'user.name',
                'pass$$word',
                'Incomplete Access Details'
            ],
            // #5
            [
                'host=example.google.com;port:80',
                'user.name',
                'pass$$word',
                'Incomplete Access Details'
            ],
            // #6
            [
                'host=example.google.com;port:80;dbname=testdb',
                'user.name',
                'pass$$word',
                'Incomplete Access Details'
            ],
            // #7
            [
                'host=example.google.com;port=80;dbname:testdb',
                'user.name',
                'pass$$word',
                'Incomplete Access Details'
            ],
            // #8
            [
                'port=3333;host=ABC.ABC.test.thisIsInvalidDomain;dbname=B;',
                'abc',
                'def',
                'Host did not Reachable; ping failed'
            ],
            // #9
            [
                'port=80;host=pslgroup.com;dbname=B;',
                'abc',
                'def',
                'Host did not Responded; fsockopen() failed'
            ],
        ];
    }

    /**
     * @dataProvider getDsnStringProvider
     *
     * @testdox Testing _extractDsnString(): Stacks
     */
    public function testExtractDsnString($dsn, $expect): void
    {
        $result = MsSqlRemoteConnection::_extractDsnString($dsn);

        $this->assertSame($expect, $result);
    }

    public function getDsnStringProvider(): array
    {
        return [
            [ // 0
                '',
                [
                    'host'    => '',
                    'port'    => '',
                    'dbname'  => '',
                    'options' => [],
                ],
            ],
            [ // 1
                'host=A',
                [
                    'host'    => 'A',
                    'port'    => '',
                    'dbname'  => '',
                    'options' => [],
                ],
            ],
            [ // 2
                'port=1;dbname=B',
                [
                    'host'    => '',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => [],
                ],
            ],
            [ // 3
                'host=A:1',
                [
                    'host'    => 'A',
                    'port'    => '1',
                    'dbname'  => '',
                    'options' => [],
                ],
            ],
            [ // 4
                'port=1;dbname=B;host=C;opt1=D;opt2=E',
                [
                    'host'    => 'C',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => [
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ],
                ],
            ],
            [ // 5
                'host=A:1;port=2;dbname=B;opt1=D;opt2=E',
                [
                    'host'    => 'A',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => [
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ],
                ],
            ],
            [ // 6
                'port=2;host=A:1;dbname=B;opt1=D;opt2=E',
                [
                    'host'    => 'A',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => [
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ],
                ],
            ],
            [ // 7
                'opt1=D;opt2=E;port=2;host=A:1;dbname=B;',
                [
                    'host'    => 'A',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => [
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ],
                ],
            ],
            [ // 8
                'opt1=D;opt2=E;port=2;',
                [
                    'host'    => '',
                    'port'    => '2',
                    'dbname'  => '',
                    'options' => [
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ],
                ],
            ],
        ];
    }

    /**
     * @testdox Testing makeConnection() with Test Config
     */
    public function testMakeAndGetConnection(): void
    {
        // reading from .evn
        self::bootKernel();
        $root = self::$kernel->getProjectDir();
        (new \Symfony\Component\Dotenv\Dotenv(true))->loadEnv($root . '/.env');

        if ($dsn = getenv('LOTUS_DB_DSN')) {
            $username = getenv('LOTUS_DB_USER');
            $password = getenv('LOTUS_DB_PASSWORD');
        }
        elseif ($dsn = getenv('CONFERENCE_DB_DSN')) {
            $username = getenv('CONFERENCE_DB_USER');
            $password = getenv('CONFERENCE_DB_PASSWORD');
        }

        if (empty($dsn)) {
            $this->markTestSkipped('Skip as No MsSQL Config were provided.');
            return;
        }

        $connection = null;
        try {
            $remote = new MsSqlRemoteConnection($this->logger, $dsn, $username, 'dummy-password', null, getenv('MSSQLREMOTECONN_TIMEOUT'));
            $remote->makeConnection(true);
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\MsSqlRemoteConnectionException::class, $e);
            $this->assertStringContainsStringIgnoringCase('MySQL Remote Database Connection failed', $e->getMessage());
            $this->assertStringContainsStringIgnoringCase('Adaptive Server connection failed', $e->getMessage());
        }
        $this->assertNull($connection);

        $remote = new MsSqlRemoteConnection($this->logger, $dsn, $username, $password, null, getenv('MSSQLREMOTECONN_TIMEOUT'));
        $connected = $remote->makeConnection(true);
        $this->assertTrue($connected);
        $this->assertTrue($remote->isConnected());

        $this->assertInstanceOf(\PDO::class, $remote->getConnection());

        $this->assertSame($dsn, $remote->getInfo()['dsn']);
        $this->assertTrue($remote->getInfo()['connected']);
        $this->assertFalse($remote->getInfo()['error']);
        $this->assertFalse($remote->getInfo()['error-msg']);

        $connected = $remote->makeConnection(true);
        $this->assertTrue($connected);
        $this->assertTrue($remote->isConnected());
    }
}
