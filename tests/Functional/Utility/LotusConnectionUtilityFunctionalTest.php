<?php

namespace App\Tests\Functional\Utility;

use App\Tests\Functional\BaseFunctionalTest;
use App\Utility\LotusConnection;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * @testdox FUNCTIONAL | Utility | LotusConnection
 */
class LotusConnectionUtilityFunctionalTest extends TestCase
{
    /**
     * @var Mockery of LotusConnection;
     */
    private $mockLotusConnection;

    /**
     * Set Up ENV.
     *
     * @var array
     */
    private $cfg = array(
        'LOTUS_DB_DSN'      => 'host=A:1;dbname=B;opt1=D;opt2=E',
        'LOTUS_DB_USER'     => 'USER',
        'LOTUS_DB_PASSWORD' => 'PASSWORD',
        'LOTUS_DB_UPSYNC'   => 'true',
    );

    /**
     * Original ENV value.
     *
     * @var array
     */
    private $cfgBK = array();

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        // save env
        foreach ($this->cfg as $name => $value) {
            $this->cfgBK[$name] = getenv($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        // reset all $cfg
        foreach ($this->cfgBK as $name => $value) {
            putenv("{$name}={$value}");
        }
    }

    /**
     * Method to initiate the Class.
     *
     * @param bool $withEnv Flag to define ENV from $this->cfg first
     */
    private function classInitiation($withEnv)
    {
        if (!empty($withEnv)) {
            foreach ($this->cfg as $name => $value) {
                putenv("{$name}={$value}");
            }
        }

        $logger = $this->createConfiguredMock(LoggerInterface::class, array(
            'debug' => true,
            'error' => true,
            'info'  => true,
        ));

        $this->mockLotusConnection = new LotusConnection($logger, getenv('LOTUS_DB_DSN'), getenv('LOTUS_DB_USER'), getenv('LOTUS_DB_PASSWORD'), getenv('LOTUS_DB_UPSYNC'));
    }

    /**
     * @testdox LotusConnection Bad Initiation, without config
     *
     * Most of these test will be skip if ENV config is Defined.
     */
    public function testBasicsBadInitiation()
    {
        // skip the following if remote Db config in place
        if (!empty($this->cfgBK['LOTUS_DB_DSN'])) {
            $this->markTestSkipped('Skipping 2 assertion, as Remote DB is configured.');

            return true;
        }

        $this->classInitiation(false);
        $result = $this->mockLotusConnection->isConnected();
        $this->assertSame(false, $result);

        $upsync = filter_var($this->cfgBK['LOTUS_DB_UPSYNC'], FILTER_VALIDATE_BOOLEAN);

        $result = $this->mockLotusConnection->isUpSync();
        $this->assertSame($upsync, $result);

        $result = $this->mockLotusConnection->getInfo();
        $expect = array(
            'dsn'       => false,
            'host'      => false,
            'port'      => false,
            'dbname'    => false,
            'options'   => false,
            'username'  => false,
            'password'  => false,
            'connected' => false,
            'upsync'    => $upsync,
            'error'     => true,
            'error-msg' => 'Not Initiated',
        );
        $this->assertSame($expect, $result);

        try {
            $result = $this->mockLotusConnection->makeConnection();
        }
        catch (\Exception $e) {
            $this->assertSame($expect['error-msg'], $e->getMessage());
            $this->assertSame(1, $e->getCode());
        }
    }

    /**
     * @testdox LotusConnection at Initiation, with dummy config
     */
    public function testBasicsAtInitiation()
    {
        $this->classInitiation(true);

        $result = $this->mockLotusConnection->isConnected();
        $this->assertSame(false, $result);

        $upsync = filter_var($this->cfg['LOTUS_DB_UPSYNC'], FILTER_VALIDATE_BOOLEAN);

        $result = $this->mockLotusConnection->isUpSync();
        $this->assertSame($upsync, $result);

        $result = $this->mockLotusConnection->getInfo();
        $expect = array(
            'dsn'       => $this->cfg['LOTUS_DB_DSN'],
            'host'      => 'A',
            'port'      => '1',
            'dbname'    => 'B',
            'options'   => array('opt1' => 'D', 'opt2' => 'E'),
            'username'  => $this->cfg['LOTUS_DB_USER'],
            'password'  => str_repeat('*', strlen($this->cfg['LOTUS_DB_PASSWORD'])),
            'connected' => false,
            'upsync'    => $upsync,
            'error'     => true,
            'error-msg' => 'Not Initiated',
        );
        $this->assertSame($expect, $result);

        try {
            $result = $this->mockLotusConnection->getConnection();
        }
        catch (\Exception $e) {
            $this->assertSame($expect['error-msg'], $e->getMessage());
            $this->assertSame(502, $e->getCode());
        }

        putenv('LOTUS_DB_DSN=opt1=D;opt2=E');
        putenv('LOTUS_DB_UPSYNC=true');
        $this->classInitiation(false);
        try {
            $result = $this->mockLotusConnection->makeConnection(true);
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\LotusConnectionException::class, $e);
            $this->assertStringContainsStringIgnoringCase('Incomplete Access Details', $e->getMessage());
            $this->assertSame(502, $e->getCode());
        }

        putenv('LOTUS_DB_DSN=host=impossible-domain.com.x.x.g:9999;dbname=A');
        $this->classInitiation(false);
        try {
            $result = $this->mockLotusConnection->makeConnection(true);
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\LotusConnectionException::class, $e);
            $this->assertStringContainsStringIgnoringCase('Host did not Reachable', $e->getMessage());
            $this->assertSame(502, $e->getCode());
        }

        putenv('LOTUS_DB_DSN=host=google.com:9999;dbname=A');
        $this->classInitiation(false);
        try {
            $result = $this->mockLotusConnection->makeConnection(true);
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\LotusConnectionException::class, $e);
            $this->assertStringContainsStringIgnoringCase('Host did not Responded', $e->getMessage());
            $this->assertSame(502, $e->getCode());
        }

        putenv('LOTUS_DB_DSN=host=google.com:80;dbname=A');
        $this->classInitiation(false);
        $result = $this->mockLotusConnection->makeConnection();
        $this->assertFalse($result);
    }

    /**
     * @testdox LotusConnection Initiation, with ENV Config
     *
     * Most of these test will be skip if ENV config is NOT Defined.
     */
    public function testMakeConnection()
    {
        if (!BaseFunctionalTest::getLotusStatus()) {
            $this->markTestSkipped('Skip as Lotus Server is not Reachable.');
            return;
        }

        $this->assertTrue(is_string($this->cfgBK['LOTUS_DB_DSN']));

        if (
            (empty($this->cfgBK['LOTUS_DB_DSN'])) ||
            (empty($this->cfgBK['LOTUS_DB_USER'])) ||
            (empty($this->cfgBK['LOTUS_DB_PASSWORD']))
        ) {
            $this->markTestSkipped('Test Skipped, as Remote DB is not configured on Test ENV.');
            return;
        }

        putenv('LOTUS_DB_UPSYNC=true');

        $this->classInitiation(false);

        $result = $this->mockLotusConnection->makeConnection();
        $this->assertTrue($result);

        // try make another connection on same session
        $result = $this->mockLotusConnection->makeConnection();
        $this->assertTrue($result);

        $result = $this->mockLotusConnection->getConnection();
        $this->assertInstanceOf(\PDO::class, $result);

        putenv('LOTUS_DB_PASSWORD=this-is-a-wrong-password');
        $this->classInitiation(false);

        try {
            $result = $this->mockLotusConnection->makeConnection();
        }
        catch (\Exception $e) {
            $this->assertNotEmpty(stripos($e->getMessage(), 'connection failed: '));
        }
    }

    /**
     * @dataProvider getDsnStringProvider
     *
     * @testdox Testing _extractDsnString(): Stacks
     */
    public function testExtractDsnString($dsn, $expect)
    {
        $result = LotusConnection::_extractDsnString($dsn);

        $this->assertSame($expect, $result);
    }

    public function getDsnStringProvider()
    {
        return array(
            array( // 0
                '',
                array(
                    'host'    => '',
                    'port'    => '',
                    'dbname'  => '',
                    'options' => array(),
                ),
            ),
            array( // 1
                'host=A',
                array(
                    'host'    => 'A',
                    'port'    => '',
                    'dbname'  => '',
                    'options' => array(),
                ),
            ),
            array( // 2
                'port=1;dbname=B',
                array(
                    'host'    => '',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => array(),
                ),
            ),
            array( // 3
                'host=A:1',
                array(
                    'host'    => 'A',
                    'port'    => '1',
                    'dbname'  => '',
                    'options' => array(),
                ),
            ),
            array( // 4
                'port=1;dbname=B;host=C;opt1=D;opt2=E',
                array(
                    'host'    => 'C',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => array(
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ),
                ),
            ),
            array( // 5
                'host=A:1;port=2;dbname=B;opt1=D;opt2=E',
                array(
                    'host'    => 'A',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => array(
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ),
                ),
            ),
            array( // 6
                'port=2;host=A:1;dbname=B;opt1=D;opt2=E',
                array(
                    'host'    => 'A',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => array(
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ),
                ),
            ),
            array( // 7
                'opt1=D;opt2=E;port=2;host=A:1;dbname=B;',
                array(
                    'host'    => 'A',
                    'port'    => '1',
                    'dbname'  => 'B',
                    'options' => array(
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ),
                ),
            ),
            array( // 8
                'opt1=D;opt2=E;port=2;',
                array(
                    'host'    => '',
                    'port'    => '2',
                    'dbname'  => '',
                    'options' => array(
                        'opt1' => 'D',
                        'opt2' => 'E',
                    ),
                ),
            ),
        );
    }
}
