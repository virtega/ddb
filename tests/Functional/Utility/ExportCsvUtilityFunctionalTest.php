<?php

namespace App\Tests\Functional\Utility;

use App\Utility\ExportCsv;
use PHPUnit\Framework\TestCase;

/**
 * @testdox FUNCTIONAL | Utility | Export CSV
 */
class ExportCsvUtilityFunctionalTest extends TestCase
{
    /**
     * @var array
     */
    private $temporaries = array();

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        foreach ($this->temporaries as $temp) {
            if (file_exists($temp)) {
                unlink($temp);
            }
        }
    }

    /**
     * @testdox Check if File Created, and is Read-Writable
     */
    public function testFileCreation()
    {
        $export = new ExportCsv(__FUNCTION__);
        $path   = $export->getFilePath();
        $this->assertFileExists($path);
        $this->assertFileIsReadable($path);
        $this->assertFileIsWritable($path);

        return $path;
    }

    /**
     * @testdox Check if Failed File Created
     */
    public function testFileCreationFailure()
    {
        try {
            // linux
            $export = new ExportCsv("\0");
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Unable to create', $e->getMessage());
        }
    }

    /**
     * @depends testFileCreation
     *
     * @testdox Check if the File Read-Writable
     */
    public function testFileReadWritable($path)
    {
        $this->assertFileIsReadable($path);

        $this->assertFileIsWritable($path);

        return $path;
    }

    /**
     * @depends testFileReadWritable
     *
     * @testdox Check if File can be Removable
     */
    public function testFileRemoval($path)
    {
        unlink($path);

        $this->assertFileNotExists($path);
    }

    /**
     * @testdox Writing a Row Cells by an Array
     */
    public function testWriteRowCells()
    {
        $export = new ExportCsv(__FUNCTION__);
        $cells  = array(1, 2, '3', '4', 5, 6);
        $export->writeRowCells($cells);

        $path    = $export->getFilePath();
        $content = file_get_contents($path);

        $this->assertStringContainsString('1,2,3,4,5,6' . PHP_EOL, $content);

        $this->temporaries[] = $path;
    }

    /**
     * @testdox Writing several Row of Cells with special characters with an Array
     */
    public function testWriteRows()
    {
        $export = new ExportCsv(__FUNCTION__);
        $cells  = array(
            array("God's Plan", 2, '3', '4', "\0x02AC", 6),
            array(1, 2, '3', '4', 5, 'Perfect'),
            array('Let You Down', 2, '3', '4', 'Dua-Lipa' . PHP_EOL . 'IDGAF', 6),
        );
        $export->writeRows($cells);

        $path   = $export->getFilePath();
        $handle = fopen($path, 'r');

        $row = 0;
        while (false !== ($data = fgetcsv($handle, 1000))) {
            $num = count($data);
            for ($c = 0; $c < $num; ++$c) {
                if (addslashes($data[$c]) != $data[$c]) {
                    $this->assertStringContainsString($cells[$row][$c], $data[$c]);
                }
                else {
                    $this->assertEquals($cells[$row][$c], $data[$c]);
                }
            }
            ++$row;
        }

        fclose($handle);

        $this->temporaries[] = $path;
    }
}
