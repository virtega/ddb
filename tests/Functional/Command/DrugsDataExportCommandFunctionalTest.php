<?php

namespace App\Tests\Functional\Command;

use App\Command\DrugsDataExportCommand;
use App\Service\MailService;
use App\Service\SearchService;
use App\Tests\Fixtures\CountryFixtures;
use App\Tests\Fixtures\Drug\BrandDrugRepoFixtures;
use App\Tests\Fixtures\Drug\ExperimentalDrugRepoFixtures;
use App\Tests\Fixtures\Drug\GenericDrugRepoFixtures;
use App\Tests\Functional\BaseFunctionalTest;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @testdox FUNCTIONAL | Command | Drugs Data Export Command
 */
class DrugsDataExportCommandFunctionalTest extends WebTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $entityManager;

    /**
     * @var SearchService
     */
    private $ss;

    /**
     * @var MailService
     */
    private $ms;

    /**
     * @var array
     */
    private $msArgs;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new CountryFixtures());
        $loader->addFixture(new BrandDrugRepoFixtures());
        $loader->addFixture(new ExperimentalDrugRepoFixtures());
        $loader->addFixture(new GenericDrugRepoFixtures());

        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->purge();

        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();
        $this->ss = $client->getContainer()->get('test.' . SearchService::class);
        $this->ms = $client->getContainer()->get('test.' . MailService::class);

        parent::setUp();
    }

    /**
     * @testdox Expect Failure Data Export
     */
    public function testDataExport001()
    {
        $error = false;
        try {
            $this->runCommandGetOutput();
        }
        catch (\Exception $e) {
            $error = $e;
        }

        $this->assertNotEmpty($error);
        $this->assertStringContainsStringIgnoringCase('Not enough arguments (missing:', $error->getMessage());
        $this->assertStringContainsString('email', $error->getMessage());
        $this->assertStringContainsString('search', $error->getMessage());

        $error = false;
        try {
            $this->runCommandGetOutput(array(
                'email' => BaseFunctionalTest::getTestEmail(),
            ));
        }
        catch (\Exception $e) {
            $error = $e;
        }

        $this->assertNotEmpty($error);
        $this->assertStringContainsStringIgnoringCase('Not enough arguments (missing:', $error->getMessage());
        $this->assertStringContainsString('search', $error->getMessage());

        $error = false;
        try {
            $this->runCommandGetOutput(array(
                'search' => '^test',
            ));
        }
        catch (\Exception $e) {
            $error = $e;
        }

        $this->assertNotEmpty($error);
        $this->assertStringContainsStringIgnoringCase('Not enough arguments (missing:', $error->getMessage());
        $this->assertStringContainsString('email', $error->getMessage());
    }

    /**
     * @testdox Check Complete Data Export
     */
    public function testDataExport002()
    {
        $options = array(
            'email'  => BaseFunctionalTest::getTestEmail(),
            'search' => '^test',
        );
        $output = $this->runCommandGetOutput($options);

        $this->assertStringContainsStringIgnoringCase('Exporting Drugs Data', $output);
        $this->assertStringContainsString($options['email'], $output);
        $this->assertStringContainsString($options['search'], $output);
        $this->assertStringContainsString('any', $output);
        $this->assertStringContainsStringIgnoringCase('Complete', $output);

        $ms = $this->getSendSwiftMessage();
        $this->assertArrayHasKey($options['email'], $ms->getTo());
        $this->assertStringContainsStringIgnoringCase('Your Drugs Export on ' . date('Y-m-d'), $ms->getSubject());
        $this->assertStringContainsStringIgnoringCase('Drugs Export Data Attached', $ms->getBody());
        $this->assertStringContainsStringIgnoringCase('Export complete, file attached.', $ms->getBody());
        $this->assertStringContainsStringIgnoringCase('Export complete.', $ms->getBody());

        $file = $ms->getChildren()[0];

        $this->assertSame('attachment', $file->getDisposition());
        $this->assertStringContainsStringIgnoringCase('csv', $file->getContentType());

        $raw = explode(PHP_EOL, $file->getBody());
        $raw = array_filter($raw);
        $data = array_map('str_getcsv', $raw);

        $this->assertEquals(41, count($data[0]));
        $this->assertEquals(11, count($data));

        $mock = $this->formatExportContent($file->getBody());
        $this->assertSame('7428e3f39e626e49d5ac4fcaa4bdcae4', md5($mock));
    }

    /**
     * @testdox Check Simple Data Export
     */
    public function testDataExport003()
    {
        $options = array(
            'email'    => BaseFunctionalTest::getTestEmail(),
            'search'   => '^test',
            'complete' => 0,
        );
        $output = $this->runCommandGetOutput($options);

        $this->assertStringContainsStringIgnoringCase('Exporting Drugs Data', $output);
        $this->assertStringContainsString($options['email'], $output);
        $this->assertStringContainsString($options['search'], $output);
        $this->assertStringContainsString('any', $output);
        $this->assertStringContainsStringIgnoringCase('Names Only', $output);

        $ms = $this->getSendSwiftMessage();
        $file = $ms->getChildren()[0];
        $raw = explode(PHP_EOL, $file->getBody());
        $raw = array_filter($raw);
        $data = array_map('str_getcsv', $raw);

        $this->assertEquals(6, count($data[0]));
        $this->assertEquals(11, count($data));

        $mock = $this->formatExportContent($file->getBody());
        $this->assertSame('e0d426408a6420147486adaa13623c0f', md5($mock));
    }

    /**
     * @testdox Check Complete Data Limited Export
     */
    public function testDataExport004()
    {
        $options = array(
            'email'    => BaseFunctionalTest::getTestEmail(),
            'search'   => '^test',
            'type'     => 'generic',
            'complete' => 1,
        );
        $output = $this->runCommandGetOutput($options);

        $this->assertStringContainsStringIgnoringCase('Exporting Drugs Data', $output);
        $this->assertStringContainsString($options['email'], $output);
        $this->assertStringContainsString($options['search'], $output);
        $this->assertStringContainsString($options['type'], $output);
        $this->assertStringContainsStringIgnoringCase('Complete', $output);

        $ms = $this->getSendSwiftMessage();
        $file = $ms->getChildren()[0];
        $raw = explode(PHP_EOL, $file->getBody());
        $raw = array_filter($raw);
        $data = array_map('str_getcsv', $raw);

        $this->assertEquals(41, count($data[0]));
        $this->assertEquals(7, count($data));

        $mock = $this->formatExportContent($file->getBody());
        $this->assertSame('d34c81780e3aec8e81c5da03fab16641', md5($mock));
    }

    private function getSendSwiftMessage()
    {
        $reflection = new \ReflectionClass($this->ms);
        $reflection_property = $reflection->getProperty('email');
        $reflection_property->setAccessible(true);
        return $reflection_property->getValue($this->ms);
    }

    private function convertObjArr($obj)
    {
        $reflector = new \ReflectionObject($obj);
        $nodes     = $reflector->getProperties();

        $out = array();
        foreach ($nodes as $node) {
            $nod = $reflector->getProperty($node->getName());
            $nod->setAccessible(true);
            $out[$node->getName()] = $nod->getValue($obj);
        }

        return $out;
    }

    private function formatExportContent($content)
    {
        $content = explode(PHP_EOL, $content);

        foreach ($content as $ci => $cont) {
            // creation data
            $cont = preg_replace("/\"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\"/", "\"0000-00-00 00:00:00\"", $cont);
            // main drug ID
            $cont = preg_replace("/^\d{1,12},/", "0,", $cont);
            // reference drug ID
            $cont = preg_replace("/,\d{1,12},/", ",0,", $cont);

            $content[$ci] = $cont;
        }

        asort($content);

        $content = implode(PHP_EOL, $content);

        return $content;
    }

    private function runCommandGetOutput($options = array())
    {
        $logger = $this->createConfiguredMock(LoggerInterface::class, array(
            'debug' => true,
            'error' => true,
            'info'  => true,
        ));

        $application = new Application(static::$kernel);
        $application->add(new DrugsDataExportCommand($this->ss, $this->ms, $logger));

        $command = $application->find('app:drugs-export-data');
        $options = array_merge(array('command' => $command->getName()), $options);

        $commandTester = new CommandTester($command);
        $commandTester->execute($options);

        $output = $commandTester->getDisplay();

        return $output;
    }
}
