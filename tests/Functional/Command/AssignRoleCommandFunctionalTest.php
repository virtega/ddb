<?php

namespace App\Tests\Functional\Command;

use App\Command\AssignRoleCommand;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @testdox FUNCTIONAL | Command | Assign Roles
 */
class AssignRoleCommandFunctionalTest extends WebTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $entityManager;
    private $em;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->em = $client->getContainer()->get('doctrine')->getManager();

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->em->close();
        $this->em = null;
    }

    /**
     * @dataProvider getRoleAssignmentsErrorProvider
     *
     * @testdox Checking argument requirements
     */
    public function testAssignRole001($arguments, $contains)
    {
        $error = false;
        try {
            $this->runCommandGetOutput($arguments);
        }
        catch (\Exception $e) {
            $error = $e;
        }

        $this->assertNotEmpty($error);
        foreach ($contains as $contain) {
            $this->assertStringContainsStringIgnoringCase($contain, $error->getMessage());
        }
    }

    public function getRoleAssignmentsErrorProvider()
    {
        return array(
            // #0
            array(
                array(),
                array(
                    'Not enough arguments (missing:',
                    'username',
                    'role',
                )
            ),
            // #1
            array(
                array(
                    'role'     => 'admin',
                ),
                array(
                    'Not enough arguments (missing:',
                    'username',
                )
            ),
            // #2
            array(
                array(
                    'username' => 'John.Doe',
                ),
                array(
                    'Not enough arguments (missing:',
                    'role',
                )
            ),
            // #3
            array(
                array(
                    'username' => array('John.Doe', 'Irfan.Mikael'),
                    'role'     => 'user',
                ),
                array(
                    'Argument "username" is Invalid',
                )
            ),
            // #4
            array(
                array(
                    'username' => 'John.Doe',
                    'role'     => array('admin', 'editor'),
                ),
                array(
                    'Argument "role" is Invalid',
                )
            ),
            // #5
            array(
                array(
                    'username' => 'John.Doe',
                    'role'     => 'unknown',
                ),
                array(
                    'Role is Unknown',
                )
            ),
            // #6
            array(
                array(
                    'username' => 'John.Doe@gmail.com',
                    'role'     => 'admin',
                ),
                array(
                    'Argument "username" is Invalid; remove email domain',
                )
            ),
            // #7
            array(
                array(
                    'username' => 'case+arnold',
                    'role'     => 'admin',
                ),
                array(
                    'Argument "username" format is Invalid',
                )
            ),
            // #8
            array(
                array(
                    'username' => 'case-arnold',
                    'role'     => 'admin',
                ),
                array(
                    'Argument "username" format is Invalid',
                )
            ),
        );
    }

    /**
     * @dataProvider getRoleAssignmentsProvider
     *
     * @testdox Checking Role assignments
     */
    public function testAssignRole002($role_key, $roles)
    {
        $error = false;
        try {
            $output = $this->runCommandGetOutput(array(
                'username' => 'John.Doe',
                'role'     => $role_key,
            ));
        }
        catch (\Exception $e) {
            $error = $e;
        }

        $table_view = '| ' . implode(', ', $roles);

        $this->assertEmpty($error);
        $this->assertStringContainsStringIgnoringCase('John.Doe', $output);
        $this->assertStringContainsStringIgnoringCase($table_view, $output);
        $this->assertStringContainsStringIgnoringCase('is Completed', $output);

        $stored = $this->em->getConnection()->fetchColumn('SELECT `roles` FROM `user_roles` WHERE `username` = :username LIMIT 1;', array(
            ':username' => 'John.Doe'
        ));
        $stored = json_decode($stored, true);
        $this->assertNotEmpty($stored);
        $this->assertEquals(count($roles), count($stored));
        foreach ($roles as $role) {
            $this->assertTrue(in_array($role, $stored));
        }
    }

    public function getRoleAssignmentsProvider()
    {
        return array(
            // #0
            array(
                'visitor',
                array('ROLE_VISITOR')
            ),
            // #1
            array(
                'user',
                array('ROLE_USER', 'ROLE_VISITOR')
            ),
            // #2
            array(
                'editor',
                array('ROLE_EDITOR', 'ROLE_USER', 'ROLE_VISITOR')
            ),
            // #3
            array(
                'admin',
                array('ROLE_ADMIN', 'ROLE_EDITOR', 'ROLE_USER', 'ROLE_VISITOR')
            ),
            // #4
            array(
                'Editor',
                array('ROLE_EDITOR', 'ROLE_USER', 'ROLE_VISITOR')
            ),
            // #5
            array(
                'USER',
                array('ROLE_USER', 'ROLE_VISITOR')
            ),
        );
    }

    private function runCommandGetOutput($options = array())
    {
        $logger = $this->createConfiguredMock(LoggerInterface::class, array(
            'debug' => false,
            'error' => false,
            'info'  => false,
        ));

        $application = new Application(static::$kernel);
        $application->add(new AssignRoleCommand($this->em, $logger));

        $command = $application->find('app:assign-role');
        $options = array_merge(array('command' => $command->getName()), $options);

        $commandTester = new CommandTester($command);
        $commandTester->execute($options);

        $output = $commandTester->getDisplay();

        return $output;
    }
}
