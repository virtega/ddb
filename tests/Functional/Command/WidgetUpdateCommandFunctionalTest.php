<?php

namespace App\Tests\Functional\Command;

use App\Command\WidgetUpdatesCommand;
use App\Tests\Fixtures\CountryFixtures;
use App\Tests\Fixtures\Drug\BrandDrugRepoFixtures;
use App\Tests\Fixtures\Drug\ExperimentalDrugRepoFixtures;
use App\Tests\Fixtures\Drug\GenericDrugRepoFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @testdox FUNCTIONAL | Command | Widget Update Command
 */
class WidgetUpdateCommandFunctionalTest extends WebTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $entityManager;
    private $em;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new CountryFixtures());
        $loader->addFixture(new BrandDrugRepoFixtures());
        $loader->addFixture(new ExperimentalDrugRepoFixtures());
        $loader->addFixture(new GenericDrugRepoFixtures());

        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->purge();

        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->em = $client->getContainer()->get('doctrine')->getManager();

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->em->close();
        $this->em = null;
    }

    /**
     * @testdox Run First Widget Update
     */
    public function testWidgetUpdate001()
    {
        $output = $this->runCommandGetOutput();

        $this->assertStringContainsStringIgnoringCase('==> COMMAND: Update Widgets', $output);

        $this->fixtureTests();
    }

    /**
     * @testdox Run Second Widget Update: With Dashboard widget Cache
     */
    public function testWidgetUpdate002()
    {
        $conn = $this->em->getConnection();

        $conn->exec('INSERT INTO `variable` (`name`, `type`, `value`) VALUES ("dashboard-widget-tokens-cache", "integer", "[]");');

        $output = $this->runCommandGetOutput();

        $this->assertStringContainsStringIgnoringCase('==> COMMAND: Update Widgets', $output);

        // nothing change
        $this->fixtureTests();
    }

    private function fixtureTests($override = false)
    {
        // drug + conference + journal
        $tableCount = 58 + 8 + 10;
        if ((!empty($override)) && (!empty($override['table']))) {
            $tableCount = $override['table'];
        }

        $table = $this->queryCountCollumn('table', 'variable');
        $this->assertEquals($tableCount, $table);

        $set = array(
            'experimental'       => 'widget-experimental-count',
            'generic'            => 'widget-generic-count',
            'brand'              => 'widget-brand-count',
            'generic_ref_level1' => 'widget-generic-ref-level-1-count',
            'generic_ref_level2' => 'widget-generic-ref-level-2-count',
            'generic_ref_level3' => 'widget-generic-ref-level-3-count',
            'generic_ref_level4' => 'widget-generic-ref-level-4-count',
            'generic_synonym'    => 'widget-generic-synonym-count',
            'generic_typo'       => 'widget-generic-typo-count',
            'brand_synonym'      => 'widget-brand-synonym-count',
            'brand_typo'         => 'widget-brand-typo-count',
        );

        foreach ($set as $table => $variable) {
            if ('generic' === $table) {
                foreach ([true, false] as $bool) {
                    $rowCount = $this->queryCountCollumn('table', $table, $bool);
                    $varValue = $this->queryCountCollumn('get', $variable, $bool);
                    $this->assertEquals($varValue, $rowCount, "A1. '{$variable}' is {$varValue} not {$rowCount}, fullcode: " . ($bool ? 'Y' : 'N'));
                }
            }
            else {
                $rowCount = $this->queryCountCollumn('table', $table, null);
                $varValue = $this->queryCountCollumn('get', $variable, null);
                $this->assertEquals($varValue, $rowCount, "A2. '{$variable}' is {$varValue} not {$rowCount}");
            }
        }

        // $tableCount from here
        $set = array(
            'widget-brand-comments-count'                            => 5,
            'widget-brand-count'                                     => 5,
            'widget-brand-creation-date-count'                       => 2,
            'widget-brand-invalid-non_orphan-count'                  => 1,
            'widget-brand-invalid-orphan-count'                      => 1,
            'widget-brand-synonym-count'                             => 2,
            'widget-brand-synonym-orphan-count'                      => 0,
            'widget-brand-typo-count'                                => 3,
            'widget-brand-typo-orphan-count'                         => 0,
            'widget-brand-valid-non_orphan-count'                    => 2,
            'widget-brand-valid-orphan-count'                        => 1,

            'widget-experimental-brand-invalid-count'                => 0,
            'widget-experimental-brand-valid-count'                  => 1,
            'widget-experimental-comments-count'                     => 5,
            'widget-experimental-count'                              => 5,
            'widget-experimental-creation-date-count'                => 1,
            'widget-experimental-experimental-invalid-count'         => 0,
            'widget-experimental-experimental-valid-count'           => 3,
            'widget-experimental-generic-invalid-count'              => 0,
            'widget-experimental-generic-valid-count'                => 1,

            'widget-generic-comments-count'                          => 6,
            'widget-generic-count'                                   => 6,
            'widget-generic-creation-date-count'                     => 1,
            'widget-generic-invalid-non_orphan-count'                => 0,
            'widget-generic-invalid-non_orphan-no-fullcode-count'    => 0,
            'widget-generic-invalid-orphan-count'                    => 1,
            'widget-generic-invalid-orphan-no-fullcode-count'        => 1,
            'widget-generic-no-fullcode-count'                       => 7,
            'widget-generic-ref-level-1-count'                       => 16,
            'widget-generic-ref-level-2-count'                       => 16,
            'widget-generic-ref-level-3-count'                       => 42,
            'widget-generic-ref-level-4-count'                       => 35,
            'widget-generic-synonym-count'                           => 2,
            'widget-generic-synonym-no-fullcode-count'               => 2,
            'widget-generic-synonym-orphan-count'                    => 0,
            'widget-generic-synonym-orphan-no-fullcode-count'        => 0,
            'widget-generic-typo-count'                              => 3,
            'widget-generic-typo-no-fullcode-count'                  => 3,
            'widget-generic-typo-orphan-count'                       => 0,
            'widget-generic-typo-orphan-no-fullcode-count'           => 0,
            'widget-generic-valid-non_orphan-count'                  => 3,
            'widget-generic-valid-non_orphan-no-fullcode-count'      => 3,
            'widget-generic-valid-orphan-count'                      => 2,
            'widget-generic-valid-orphan-no-fullcode-count'          => 3,

            'widget-specific-brand-count'                            => 2,
            'widget-specific-brand-generic-count'                    => 1,
            'widget-specific-brand-generic-no-fullcode-count'        => 1,
            'widget-specific-brand-no-fullcode-count'                => 2,
            'widget-specific-complete-count'                         => 2,
            'widget-specific-complete-no-fullcode-count'             => 2,
            'widget-specific-experimental-count'                     => 2,
            'widget-specific-experimental-generic-count'             => 1,
            'widget-specific-experimental-generic-no-fullcode-count' => 1,
            'widget-specific-experimental-no-fullcode-count'         => 2,
            'widget-specific-generic-brand-count'                    => 1,
            'widget-specific-generic-brand-no-fullcode-count'        => 1,
            'widget-specific-generic-count'                          => 2,
            'widget-specific-generic-no-fullcode-count'              => 3,
        );

        if ((!empty($override)) && (!empty($override['count']))) {
            $set = array_merge($set, $override['count']);
        }

        foreach ($set as $variable => $value) {
            $count = $this->queryCountCollumn('get', $variable);
            $this->assertEquals($count, $value, "B. '{$variable}' is {$count} not {$value}");
        }
    }

    private function queryCountCollumn($query = false, $type, $genericFullCode = null)
    {
        switch ($query) {
            case 'count':
                $sql = 'SELECT COUNT(*) AS `count` FROM `variable` WHERE `type` = "integer" AND `name` LIKE "widget-%";';
                break;

            case 'get':
                if (false === $genericFullCode) {
                    $type = str_replace('-count', '-no-fullcode-count', $type);
                }
                $sql = 'SELECT `value` FROM `variable` WHERE `type` = "integer" AND `name` = "' . $type . '" LIMIT 1;';
                break;

            case 'table':
            case false:
            default:
                if ((true === $genericFullCode) && ('generic' === $type)) {
                    $sql = 'SELECT COUNT(*) as `count` FROM `generic` WHERE `Level1` != "" AND `Level2` != "" AND `Level3` != "" AND `Level4` != "" AND `Level5` != "";';
                }
                else {
                    $sql = 'SELECT COUNT(*) as `count` FROM `' . $type . '`;';
                }
                break;
        }

        $conn = $this->em->getConnection();

        return (int) $conn->fetchColumn($sql);
    }

    private function runCommandGetOutput($options = array())
    {
        $logger = $this->createConfiguredMock(LoggerInterface::class, array(
            'debug' => true,
            'error' => true,
            'info'  => true,
        ));

        $application = new Application(static::$kernel);
        $application->add(new WidgetUpdatesCommand($this->em, $logger));

        $command = $application->find('app:update-widgets');
        $options = array_merge(array('command' => $command->getName()), $options);

        $commandTester = new CommandTester($command);
        $commandTester->execute($options);

        $output = $commandTester->getDisplay();

        return $output;
    }
}
