<?php

namespace App\Tests\Functional\Command;

use App\Command\DrugsSyncDownCommand;
use App\Tests\Fixtures\CountryFixtures;
use App\Tests\Functional\BaseFunctionalTest;
use App\Utility\LotusConnection;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @testdox FUNCTIONAL | Command | Source Sync Data Command
 */
class SourceSyncDataCommandFunctionalTest extends WebTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $entityManager;
    private $em;

    /**
     * @var LotusConnection
     */
    private $lc;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new CountryFixtures());

        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->purge();

        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        if (!BaseFunctionalTest::getLotusStatus()) {
            $this->markTestSkipped('Skip as Lotus Server is not Reachable.');
            return;
        }

        $client = static::createClient();

        $this->em = $client->getContainer()->get('doctrine')->getManager();
        $this->lc = $client->getContainer()->get('test.' . LotusConnection::class);

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->em->close();
        $this->em = null;
    }

    /**
     * @testdox Run First Source Sync
     */
    public function testSourceSync001()
    {
        $output = $this->runCommandGetOutput();

        $this->assertStringContainsStringIgnoringCase('==> COMMAND: Sync Down Drugs Record', $output);
        $this->assertStringContainsStringIgnoringCase('==> COMMAND: Update Widgets', $output);

        $earlier    = array(
            'experimental'       => 0,
            'generic_ref_level1' => 0,
            'generic_ref_level2' => 0,
            'generic_ref_level3' => 0,
            'generic_ref_level4' => 0,
        );

        foreach ($earlier as $key => $val) {
            $earlier[$key] = $this->queryCountCollumn('table', $key);
            $this->assertGreaterThan(0, $earlier[$key]);
        }

        $variable = $this->queryCountCollumn('variable', 'experimental');
        $lastid   = $this->queryCountCollumn('last-id', 'experimental');
        $this->assertEquals($variable, $lastid);

        return $earlier;
    }

    /**
     * @depends testSourceSync001
     *
     * @testdox Continue Run Source Sync
     */
    public function testSourceSync002($earlier)
    {
        $output = $this->runCommandGetOutput(array(
            '--all' => 1,
        ));

        $this->assertStringContainsStringIgnoringCase('==> COMMAND: Sync Down Drugs Record', $output);
        $this->assertStringContainsStringIgnoringCase('==> COMMAND: Update Widgets', $output);

        $count = $this->queryCountCollumn('table', 'experimental');
        $this->assertGreaterThan($earlier['experimental'], $count);
        $earlier['experimental'] = $count;

        for ($i = 1; $i <= 4; $i++) {
            $count = $this->queryCountCollumn('table', 'generic_ref_level' . $i);
            $this->assertEquals($earlier['generic_ref_level' . $i], $count);
        }

        $new = array(
            'brand',
            'brand_typo',
            'brand_synonym',
            'generic',
            'generic_typo',
            'generic_synonym',
        );

        foreach ($new as $key) {
            $earlier[$key] = $this->queryCountCollumn('table', $key);
            $this->assertGreaterThan(0, $earlier[$key]);
        }

        foreach (array('experimental', 'generic', 'brand') as $type) {
            $variable = $this->queryCountCollumn('variable', $type);
            $lastid   = $this->queryCountCollumn('last-id', $type);
            $this->assertEquals($variable, $lastid);
        }

        return $earlier;
    }

    /**
     * @depends testSourceSync002
     *
     * @testdox Retry Run Source Sync
     */
    public function testSourceSync003($earlier)
    {
        $output = $this->runCommandGetOutput();

        $this->assertStringContainsStringIgnoringCase('==> COMMAND: Sync Down Drugs Record', $output);
        $this->assertStringNotContainsStringIgnoringCase('==> COMMAND: Update Widgets', $output);

        foreach ($earlier as $key => $val) {
            $count = $this->queryCountCollumn('count', $key);
            $this->assertEquals($val, $count);
        }

        foreach (array('experimental', 'generic', 'brand') as $type) {
            $variable = $this->queryCountCollumn('variable', $type);
            $lastid   = $this->queryCountCollumn('last-id', $type);
            $this->assertEquals($variable, $lastid);
        }

        return $earlier;
    }

    /**
     * @depends testSourceSync003
     *
     * @testdox Retry Run Source Sync with no Previous ID
     */
    public function testSourceSync004($earlier)
    {
        $conn = $this->em->getConnection();

        $conn->exec('UPDATE `variable` SET `value` = (CAST(`value` AS UNSIGNED) - 2000) WHERE `name` = "source-sync-experimental-last-id";');
        $conn->exec('UPDATE `variable` SET `value` = (CAST(`value` AS UNSIGNED) - 2000) WHERE `name` = "source-sync-brand-last-id";');
        $conn->exec('UPDATE `variable` SET `value` = (CAST(`value` AS UNSIGNED) - 2000) WHERE `name` = "source-sync-generic-last-id";');

        $output = $this->runCommandGetOutput(array(
            '--all' => 1,
        ));

        $this->assertStringContainsStringIgnoringCase('==> COMMAND: Sync Down Drugs Record', $output);
        $this->assertStringContainsStringIgnoringCase('==> COMMAND: Update Widgets', $output);

        foreach ($earlier as $key => $val) {
            $count = $this->queryCountCollumn('count', $key);
            $this->assertEquals($val, $count);
        }

        foreach (array('experimental', 'generic', 'brand') as $type) {
            $variable = $this->queryCountCollumn('variable', $type);
            $lastid     = $this->queryCountCollumn('last-id', $type);
            $this->assertEquals($variable, $lastid);
        }
    }

    private function queryCountCollumn($query = false, $type)
    {
        switch ($query) {
            case 'variable':
                $sql = 'SELECT `value` FROM `variable` WHERE `name` = "source-sync-' . $type . '-last-id" LIMIT 1;';
                break;

            case 'last-id':
                $sql = 'SELECT `id` FROM `' . $type . '` ORDER BY `id` DESC LIMIT 1;';
                break;

            case 'table':
            case false:
            default:
                $sql = 'SELECT COUNT(*) as `count` FROM `' . $type . '`;';
                break;
        }

        $conn = $this->em->getConnection();

        return (int) $conn->fetchColumn($sql);
    }

    private function runCommandGetOutput($options = array())
    {
        $logger = $this->createConfiguredMock(LoggerInterface::class, array(
            'debug' => true,
            'error' => true,
            'info'  => true,
        ));

        $application = new Application(static::$kernel);
        $application->add(new DrugsSyncDownCommand($this->em, $logger, $this->lc));

        $command = $application->find('app:drugs-sync-down');
        $options = array_merge(array('command' => $command->getName()), $options);

        $commandTester = new CommandTester($command);
        $commandTester->execute($options);

        $output = $commandTester->getDisplay();

        return $output;
    }
}
