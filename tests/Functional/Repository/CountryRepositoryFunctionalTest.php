<?php

namespace App\Tests\Functional\Repository;

use App\Entity\Country;
use App\Tests\Fixtures\CountryFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox FUNCTIONAL | Repository | Country
 */
class CountryRepositoryFunctionalTest extends KernelTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new CountryFixtures());

        $purger   = new ORMPurger($this->entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $purger   = new ORMPurger($this->entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->purge();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    /**
     * @testdox Checking formatted Countries array
     */
    public function testGetCountryList()
    {
        $stmt = $this->entityManager->getConnection()
            ->prepare('SELECT COUNT(*) AS `count` FROM `country`;');
        $stmt->execute();
        $count = (int) $stmt->fetchColumn();

        $countries = $this->entityManager->getRepository(Country::class)->getCountryList();

        $this->assertEquals($count, count($countries));

        $end = end($countries);

        $this->assertInstanceOf(Country::class, $end);
    }

    /**
     * @testdox Checking Distinct Capital City array
     */
    public function testGetDistictCapitalCities()
    {
        $stmt = $this->entityManager->getConnection()
            ->prepare('SELECT COUNT(*) AS `count` FROM (SELECT `capital_city` FROM `country` WHERE `capital_city` IS NOT NULL GROUP BY `capital_city`) x;');
        $stmt->execute();
        $count = (int) $stmt->fetchColumn();

        $cities = $this->entityManager->getRepository(Country::class)->getDistictCapitalCities();

        $this->assertEquals($count, count($cities));
    }

    /**
     * @testdox Checking formatted Grouped Countries array
     */
    public function testGetCountryGroupList()
    {
        $countries = $this->entityManager->getRepository(Country::class)->getCountryGroupList();

        $this->assertEquals(3, count($countries));
        $this->assertArrayHasKey('EU5', $countries);
        $this->assertArrayHasKey('US', $countries);
        $this->assertArrayHasKey('Others', $countries);

        $this->assertEquals(5, count($countries['EU5']));
        $ext = array_column($countries['EU5'], 'iso_code');
        sort($ext);
        $this->assertSame(array('DE', 'ES', 'FR', 'GB', 'IT'), $ext);

        $this->assertGreaterThanOrEqual(1, count($countries['US']));
        $this->assertSame(['US'], array_column($countries['US'], 'iso_code'));

        $this->assertGreaterThan(1, count($countries['Others']));
        $this->assertContains('AU', array_column($countries['Others'], 'iso_code'));
    }
}
