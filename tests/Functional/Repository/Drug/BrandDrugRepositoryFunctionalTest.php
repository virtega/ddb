<?php

namespace App\Tests\Functional\Repository\Drug;

use App\Entity\Brand;
use App\Entity\BrandSynonym;
use App\Entity\BrandTypo;
use App\Entity\Generic;
use App\Tests\Fixtures\Drug\BrandDrugRepoFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox FUNCTIONAL | Repository | Brand Drug
 */
class BrandDrugRepositoryFunctionalTest extends KernelTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var BrandRepository
     */
    private $repo;

    /**
     * @var array
     */
    private static $expects_synonyms = array(
        array(
            'n' => 'test-1S-A',
            'c' => array('AU', 'US'),
        ),
        array(
            'n' => 'test-1S-B',
            'c' => array('MY', 'JP'),
        ),
    );

    /**
     * @var array
     */
    private static $expects_typos = array(
        'test-1T-A',
        'test-1T-B',
        'test-1T-C',
    );

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new BrandDrugRepoFixtures());

        $purger   = new ORMPurger($this->entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());

        parent::setUp();

        $this->repo = $this->entityManager->getRepository(Brand::class);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $purger   = new ORMPurger($this->entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->purge();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    /**
     * @testdox Checking get Family Formatting
     */
    public function testGetDrugFamily()
    {
        $res = $this->repo->getDrugFamily('test-1', 'name');

        // overall
        $this->assertEquals(5, count($res));

        $this->assertArrayHasKey('main', $res);
        $this->assertArrayHasKey('synonyms', $res);
        $this->assertArrayHasKey('synonyms_keys', $res);
        $this->assertArrayHasKey('typos', $res);
        $this->assertArrayHasKey('typos_keys', $res);

        // main
        $this->assertEquals('test-1', $res['main']->getName());
        $this->assertEquals('ABC123', $res['main']->getUid());
        $this->assertEquals(0, $res['main']->getValid());
        $this->assertEquals(1, $res['main']->getAutoencodeexclude());
        $this->assertEquals('test-1-ABC123-Comment', $res['main']->getComments());

        // downlink-1
        $this->assertEquals('test-downlink-2', $res['main']->getGeneric()->getName());

        // downlink-2
        $this->assertEquals('test-downlink-1', $res['main']->getGeneric()->getExperimental()->getName());

        // synonyms & typos
        $this->checkKeywords($res, self::$expects_synonyms, self::$expects_typos);
    }

    /**
     * @testdox Checking Drug Update Process
     */
    public function testUpdateDrug()
    {
        $drug = $this->repo->findOneBy(array('name' => 'test-2'));

        $update = array(
            'name'     => 'test-2B',
            'comments' => 'test-2B-DEF123-Comment-Update',
        );
        $res = $this->repo->updateDrug($drug->getId(), $update);
        $this->repo->flushAllNow();

        $this->assertNotNull($res->getId());

        $this->assertEquals($drug->getValid(), $res->getValid());
        $this->assertEquals($drug->getUid(), $res->getUid());

        $this->assertEquals($update['name'], $res->getName());
        $this->assertEquals($update['comments'], $res->getComments());

        $now = new \DateTime();
        $this->assertEquals($now->format('Y-m-d H:i'), $res->getCreationDate()->format('Y-m-d H:i'));
    }

    /**
     * @testdox Checking Drug Insert Process
     */
    public function testInsertDrug()
    {
        $creationDt = new \DateTime();
        $new = array(
            'name'          => 'test-4',
            'valid'         => 0,
            'unid'          => 'JKL123',
            'comments'      => 'test-4-JKL123-Comment-Insert',
            'creation_date' => $creationDt->format('Y-m-d H:i:s'),
        );
        $this->repo->updateDrug(null, $new);
        $this->repo->flushAllNow();

        $res = $this->repo->findOneBy(array('name' => $new['name']));

        $this->assertNotNull($res->getId());
        $this->assertEquals($new['name'], $res->getName());
        $this->assertEquals($new['valid'], $res->getValid());
        $this->assertEquals($new['unid'], $res->getUid());
        $this->assertEquals($new['comments'], $res->getComments());

        $this->assertEquals($creationDt->format('Y-m-d H:i'), $res->getCreationDate()->format('Y-m-d H:i'));
    }

    /**
     * @testdox Checking Drug Insert with Generic Relation Process
     */
    public function testInsertDrugWithGeneric()
    {
        $newGen = array(
            'name'     => 'test-sub-test-5',
            'valid'    => 0,
            'unid'     => 'MNO789SUB5',
            'comments' => 'test-sub-test-5-MNO789SUB5-Comment-Insert',
        );
        $this->entityManager->getRepository(Generic::class)->updateDrug(null, $newGen);
        $this->repo->flushAllNow();
        $generic = $this->entityManager->getRepository(Generic::class)->findOneBy(array('name' => $newGen['name']));

        $creationDt = new \DateTime();
        $new = array(
            'name'          => 'test-5',
            'valid'         => 0,
            'unid'          => 'MNO789',
            'creation_date' => $creationDt->format('Y-m-d H:i:s'),
            'generic'       => $generic,
        );
        $this->repo->updateDrug(null, $new);
        $this->repo->flushAllNow();

        $res = $this->repo->findOneBy(array('name' => $new['name']));

        $insert = array(
            'comments' => 'test-5-MNO789-Comment-Insert',
            'generic'  => $generic->getId(),
        );
        $this->repo->updateDrug($res->getId(), $insert);
        $this->repo->flushAllNow();

        $res = $this->repo->findOneBy(array('name' => $new['name']));

        $this->assertNotNull($res->getId());
        $this->assertEquals($new['name'], $res->getName());
        $this->assertEquals($new['valid'], $res->getValid());
        $this->assertEquals($new['unid'], $res->getUid());
        $this->assertEquals($insert['comments'], $res->getComments());

        $this->assertEquals($creationDt->format('Y-m-d H:i'), $res->getCreationDate()->format('Y-m-d H:i'));

        $this->assertEquals($generic->getId(), $res->getGeneric()->getId());
    }

    /**
     * @testdox Checking Drug & Keywords Delete Process
     */
    public function testDeleteDrug()
    {
        $drug = $this->repo->findOneBy(array('name' => 'test-1'));
        $down = $drug->getGeneric()->getId();

        // by object
        $count = $this->repo->removeDrug($drug);
        $this->repo->flushAllNow();

        // number of drugs/keywords removed
        $this->assertEquals((1 + 2 + 3), $count);

        // double check
        $drug = $this->repo->findOneBy(array('name' => 'test-1'));
        $this->assertNull($drug);

        // double check synonyms
        foreach (self::$expects_synonyms as $synonym) {
            $keyrepo = $this->entityManager->getRepository(BrandSynonym::class);
            $found   = $keyrepo->findOneBy(array('name' => $synonym['n']));
            $this->assertNull($found);
        }

        // double check typos
        foreach (self::$expects_typos as $typo) {
            $keyrepo = $this->entityManager->getRepository(BrandTypo::class);
            $found   = $keyrepo->findOneBy(array('name' => $typo));
            $this->assertNull($found);
        }

        // check down->down-link; still intact
        $found = $this->entityManager
            ->getRepository(Generic::class)
            ->findOneBy(array('id' => $down));
        $this->assertNotNull($found->getExperimental());

        // reheck but pass id
        $drug  = $this->repo->findOneBy(array('name' => 'test-3'));
        $count = $this->repo->removeDrug($drug->getId());
        $this->repo->flushAllNow();
        $this->assertEquals(1, $count);
    }

    /**
     * @testdox Checking Keywords Update, Insert & Delete Process
     */
    public function testSynoymUpdateInsertDelete()
    {
        $objects = array('synonym' => array(), 'typo' => array());

        $res = $this->repo->getDrugFamily('test-1', 'name');

        // keywords
        $keywords = array_merge(
            // original
            self::$expects_synonyms,
            // new
            array(
                array(
                    'n' => 'test-1S-NEW',
                    'c' => array('AN'),
                ),
            )
        );
        // update
        $keywords[1]['c'] = array('PK', 'NE');
        foreach ($keywords as $index => $keyword) {
            $id = 0;
            if (isset($res['synonyms_keys'][$index])) {
                $id = $res['synonyms_keys'][$index];
            }
            $data = array(
                'name'    => $keyword['n'],
                'country' => $keyword['c'],
                // object or ID
                'brand'   => (0 == $index ? $res['main']->getId() : $res['main']),
            );
            $objects['synonym'][$index] = $this->repo->updateSynonym($id, $data);
        }

        // typos
        $typos = array_merge(
            // original
            self::$expects_typos,
            // new
            array(
                'test-1T-D',
            )
        );
        // update
        $typos[1] = 'test-1T-B-CHANGE';
        foreach ($typos as $index => $typo) {
            $id = 0;
            if (isset($res['typos_keys'][$index])) {
                $id = $res['typos_keys'][$index];
            }
            $data = array(
                'name'  => $typo,
                // object or ID
                'brand' => (0 == $index ? $res['main']->getId() : $res['main']),
            );
            $objects['typo'][$index] = $this->repo->updateTypo($id, $data);
        }

        $this->repo->flushAllNow();

        // synonyms & typos
        $res = $this->repo->getDrugFamily('test-1', 'name');
        $this->checkKeywords($res, $keywords, $typos);

        // remove synonym
        $removed = $this->repo->removeSynonym($objects['synonym'][0]->getId());
        $this->assertEquals(1, $removed);
        unset($keywords[0]);
        $keywords = array_values($keywords);

        // remove typo
        $removed = $this->repo->removeTypo($objects['typo'][0]->getId());
        $this->assertEquals(1, $removed);
        unset($typos[0]);
        $typos = array_values($typos);

        $this->repo->flushAllNow();

        // recheck synonyms & typos
        $res = $this->repo->getDrugFamily('test-1', 'name');
        $this->checkKeywords($res, $keywords, $typos);
    }

    private function checkKeywords($res, $synonyms, $typos)
    {
        // synonyms
        $this->assertEquals(count($synonyms), count($res['synonyms']));

        foreach ($res['synonyms_keys'] as $index => $key) {
            $this->assertEquals($synonyms[$index]['n'], $res['synonyms'][$key]->getName());

            foreach ($synonyms[$index]['c'] as $c) {
                $this->assertContains($c, $res['synonyms'][$key]->getCountry());
            }

            $this->assertEquals($res['main']->getId(), $res['synonyms'][$key]->getBrand()->getId());
        }

        // typos
        $this->assertEquals(count($typos), count($res['typos']));

        foreach ($res['typos_keys'] as $index => $key) {
            $this->assertEquals($typos[$index], $res['typos'][$key]->getName());

            $this->assertEquals($res['main']->getId(), $res['typos'][$key]->getBrand()->getId());
        }
    }
}
