<?php

namespace App\Tests\Functional\Repository\Drug;

use App\Entity\Brand;
use App\Entity\Experimental;
use App\Entity\Generic;
use App\Entity\GenericSynonym;
use App\Entity\GenericTypo;
use App\Tests\Fixtures\Drug\GenericDrugRepoFixtures;
use App\Utility\Repository\DrugRepositoryHelpers as DRHelpers;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox FUNCTIONAL | Repository | Generic Drug
 */
class GenericDrugRepositoryFunctionalTest extends KernelTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var GenericRepository
     */
    private $repo;

    /**
     * @var array
     */
    private static $expects_synonyms = array(
        array(
            'n' => 'test-1S-A',
            'c' => array('AU', 'US'),
        ),
        array(
            'n' => 'test-1S-B',
            'c' => array('MY', 'JP'),
        ),
    );

    /**
     * @var array
     */
    private static $expects_typos = array(
        'test-1T-A',
        'test-1T-B',
        'test-1T-C',
    );

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new GenericDrugRepoFixtures());

        $purger   = new ORMPurger($this->entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());

        parent::setUp();

        $this->repo = $this->entityManager->getRepository(Generic::class);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $purger   = new ORMPurger($this->entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->purge();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    /**
     * @testdox Checking get Family Formatting
     */
    public function testGetDrugFamily()
    {
        $res = $this->repo->getDrugFamily('test-1', 'name');

        // overall
        $this->assertEquals(5, count($res));

        $this->assertArrayHasKey('main', $res);
        $this->assertArrayHasKey('synonyms', $res);
        $this->assertArrayHasKey('synonyms_keys', $res);
        $this->assertArrayHasKey('typos', $res);
        $this->assertArrayHasKey('typos_keys', $res);

        // main
        $this->assertEquals('test-1', $res['main']->getName());
        $this->assertEquals('ABC123', $res['main']->getUid());
        $this->assertEquals('A1', $res['main']->getLevel1());
        $this->assertEquals('A2', $res['main']->getLevel2());
        $this->assertEquals('A3', $res['main']->getLevel3());
        $this->assertEquals('A4', $res['main']->getLevel4());
        $this->assertEquals('A5', $res['main']->getLevel5());
        $this->assertEquals(1, $res['main']->getValid());
        $this->assertEquals(1, $res['main']->getNodoublebounce());
        $this->assertEquals(1, $res['main']->getAutoencodeexclude());
        $this->assertEquals('test-1-ABC123-Comment', $res['main']->getComments());

        // synonyms & typos
        $this->checkKeywords($res, self::$expects_synonyms, self::$expects_typos);

        // downlink
        $this->assertEquals('test-downlink', $res['main']->getExperimental()->getName());

        // uplink
        $up = $this->entityManager->getRepository(Brand::class)->findOneBy(array('generic' => $res['main']->getId()));
        $this->assertEquals('test-uplink', $up->getName());
    }

    /**
     * @testdox Checking Drug Update Process
     */
    public function testUpdateDrug()
    {
        $drug = $this->repo->findOneBy(array('name' => 'test-2'));

        $update = array(
            'name'     => 'test-2B',
            'comments' => 'test-2B-DEF123-Comment-Update',
        );
        $res = $this->repo->updateDrug($drug->getId(), $update);
        $this->repo->flushAllNow();

        $this->assertNotNull($res->getId());

        $this->assertEquals($drug->getValid(), $res->getValid());
        $this->assertEquals($drug->getUid(), $res->getUid());

        $this->assertEquals($update['name'], $res->getName());
        $this->assertEquals($update['comments'], $res->getComments());

        $now = new \DateTime();
        $this->assertEquals($now->format('Y-m-d H:i'), $res->getCreationDate()->format('Y-m-d H:i'));
    }

    /**
     * @testdox Checking Drug Update with Experimental Relation Process
     */
    public function testUpdateDrugWithExperimentalRelation()
    {
        $newExp = array(
            'name'     => 'test-sub-test-2V',
            'type'     => 'generic',
            'valid'    => 1,
            'unid'     => 'XYZ0123',
            'comments' => 'test-sub-test-2V-XYZ0123-Comment-Insert',
        );
        $this->entityManager->getRepository(Experimental::class)->updateDrug(null, $newExp);
        $this->repo->flushAllNow();
        $experimental = $this->entityManager->getRepository(Experimental::class)->findOneBy(array('name' => $newExp['name']));

        $drug = $this->repo->findOneBy(array('name' => 'test-2'));

        $creationDt = new \DateTime();
        $update = array(
            'name'          => 'test-2V',
            'creation_date' => $creationDt->format('Y-m-d H:i:s'),
            'experimental'  => $experimental,
        );
        $res = $this->repo->updateDrug($drug->getId(), $update);
        $this->repo->flushAllNow();

        $res = $this->repo->findOneBy(array('name' => $update['name']));

        $insert = array(
            'comments'     => 'test-2V-DEF123-Comment-Update',
            'level_1'      => 'A',
            'level_2'      => '01',
            'level_3'      => 'A',
            'level_4'      => 'B',
            'level_5'      => '16',
            'experimental' => $experimental->getId(),
        );
        $this->repo->updateDrug($res->getId(), $insert);
        $this->repo->flushAllNow();

        $this->assertNotNull($res->getId());

        $this->assertEquals($drug->getValid(), $res->getValid());
        $this->assertEquals($drug->getUid(), $res->getUid());

        $this->assertEquals($update['name'], $res->getName());
        $this->assertEquals($insert['comments'], $res->getComments());
        $this->assertEquals($insert['level_1'], $res->getLevel1());
        $this->assertEquals($insert['level_2'], $res->getLevel2());
        $this->assertEquals($insert['level_3'], $res->getLevel3());
        $this->assertEquals($insert['level_4'], $res->getLevel4());
        $this->assertEquals($insert['level_5'], $res->getLevel5());

        $this->assertEquals($creationDt->format('Y-m-d H:i'), $res->getCreationDate()->format('Y-m-d H:i'));

        $this->assertEquals($experimental->getId(), $res->getExperimental()->getId());
    }

    /**
     * @testdox Checking Drug Insert Process
     */
    public function testInsertDrug()
    {
        $new = array(
            'name'     => 'test-4',
            'valid'    => 0,
            'unid'     => 'JKL123',
            'comments' => 'test-4-JKL123-Comment-Insert',
        );
        $this->repo->updateDrug(null, $new);
        $this->repo->flushAllNow();

        $res = $this->repo->findOneBy(array('name' => $new['name']));

        $this->assertNotNull($res->getId());
        $this->assertEquals($new['name'], $res->getName());
        $this->assertEquals($new['valid'], $res->getValid());
        $this->assertEquals($new['unid'], $res->getUid());
        $this->assertEquals($new['comments'], $res->getComments());

        $now = new \DateTime();
        $this->assertEquals($now->format('Y-m-d H:i'), $res->getCreationDate()->format('Y-m-d H:i'));
    }

    /**
     * @testdox Checking Drug & Keywords Delete Process
     */
    public function testDeleteDrug()
    {
        $drug = $this->repo->findOneBy(array('name' => 'test-1'));
        $down = $drug->getExperimental()->getId();
        $main = $drug->getId();

        // by object
        $count = $this->repo->removeDrug($drug);
        $this->repo->flushAllNow();

        // number of drugs/keywords removed
        $this->assertEquals((1 + 1 + 2 + 3), $count);

        // double check
        $drug = $this->repo->findOneBy(array('name' => 'test-1'));
        $this->assertNull($drug);

        // double check synonyms
        foreach (self::$expects_synonyms as $synonym) {
            $keyrepo = $this->entityManager->getRepository(GenericSynonym::class);
            $found   = $keyrepo->findOneBy(array('name' => $synonym['n']));
            $this->assertNull($found);
        }

        // double check typos
        foreach (self::$expects_typos as $typo) {
            $keyrepo = $this->entityManager->getRepository(GenericTypo::class);
            $found   = $keyrepo->findOneBy(array('name' => $typo));
            $this->assertNull($found);
        }

        // uplink
        $found = $this->entityManager->getRepository(Brand::class)->findOneBy(array('generic' => $main));
        $this->assertNull($found);

        // reheck but pass id
        $drug  = $this->repo->findOneBy(array('name' => 'test-3'));
        $count = $this->repo->removeDrug($drug->getId());
        $this->repo->flushAllNow();
        $this->assertEquals(1, $count);
    }

    /**
     * @testdox Checking Keywords Update, Insert & Delete Process
     */
    public function testSynoymUpdateInsertDelete()
    {
        $objects = array('synonym' => array(), 'typo' => array());

        $res = $this->repo->getDrugFamily('test-1', 'name');

        // keywords
        $keywords = array_merge(
        // original
        self::$expects_synonyms,
            // new
            array(
                array(
                    'n' => 'test-1S-NEW',
                    'c' => array('AN'),
                ),
            )
        );
        // update
        $keywords[1]['c'] = array('PK', 'NE');
        foreach ($keywords as $index => $keyword) {
            $id = 0;
            if (isset($res['synonyms_keys'][$index])) {
                $id = $res['synonyms_keys'][$index];
            }
            $data = array(
                'name'    => $keyword['n'],
                'country' => $keyword['c'],
                // object or ID
                'generic' => (0 == $index ? $res['main'] : $res['main']->getId()),
            );
            $objects['synonym'][$index] = $this->repo->updateSynonym($id, $data);
        }

        // typos
        $typos = array_merge(
            // original
            self::$expects_typos,
            // new
            array(
                'test-1T-D',
            )
        );
        // update
        $typos[1] = 'test-1T-B-CHANGE';
        foreach ($typos as $index => $typo) {
            $id = 0;
            if (isset($res['typos_keys'][$index])) {
                $id = $res['typos_keys'][$index];
            }
            $data = array(
                'name'    => $typo,
                // object or ID
                'generic' => (0 == $index ? $res['main'] : $res['main']->getId()),
            );
            $objects['typo'][$index] = $this->repo->updateTypo($id, $data);
        }

        $this->repo->flushAllNow();

        // synonyms & typos
        $res = $this->repo->getDrugFamily('test-1', 'name');
        $this->checkKeywords($res, $keywords, $typos);

        // remove synonym
        $removed = $this->repo->removeSynonym($objects['synonym'][0]->getId());
        $this->assertEquals(1, $removed);
        unset($keywords[0]);
        $keywords = array_values($keywords);

        // remove typo
        $removed = $this->repo->removeTypo($objects['typo'][0]->getId());
        $this->assertEquals(1, $removed);
        unset($typos[0]);
        $typos = array_values($typos);

        $this->repo->flushAllNow();

        // recheck synonyms & typos
        $res = $this->repo->getDrugFamily('test-1', 'name');
        $this->checkKeywords($res, $keywords, $typos);
    }

    /**
     * @testdox Checking Level 1 References Getter
     */
    public function testGetAllGenericRefLevel1()
    {
        $res = $this->repo->getAllGenericRefLevel1();
        $this->checkGenericRefLevelGetter($res);
    }

    /**
     * @testdox Checking Level 2 References Getter
     */
    public function testGetAllGenericRefLevel2()
    {
        $res = $this->repo->getAllGenericRefLevel2();
        $this->checkGenericRefLevelGetter($res);
    }

    /**
     * @testdox Checking Level 3 References Getter
     */
    public function testGetAllGenericRefLevel3()
    {
        $res = $this->repo->getAllGenericRefLevel3();
        $this->checkGenericRefLevelGetter($res);
    }

    /**
     * @testdox Checking Level 4 References Getter
     */
    public function testGetAllGenericRefLevel4()
    {
        $res = $this->repo->getAllGenericRefLevel4();
        $this->checkGenericRefLevelGetter($res);
    }

    private function checkKeywords($res, $synonyms, $typos)
    {
        // synonyms
        $this->assertEquals(count($synonyms), count($res['synonyms']));

        foreach ($res['synonyms_keys'] as $index => $key) {
            $this->assertEquals($synonyms[$index]['n'], $res['synonyms'][$key]->getName());

            foreach ($synonyms[$index]['c'] as $c) {
                $this->assertContains($c, $res['synonyms'][$key]->getCountry());
            }

            $this->assertEquals($res['main']->getId(), $res['synonyms'][$key]->getGeneric()->getId());
        }

        // typos
        $this->assertEquals(count($typos), count($res['typos']));

        foreach ($res['typos_keys'] as $index => $key) {
            $this->assertEquals($typos[$index], $res['typos'][$key]->getName());

            $this->assertEquals($res['main']->getId(), $res['typos'][$key]->getGeneric()->getId());
        }
    }

    private function checkGenericRefLevelGetter($results)
    {
        $keys = array_keys($results);
        $key  = rand(0, (count($keys) - 1));
        $key  = $keys[$key];

        $this->assertFalse(is_numeric($key));

        $this->arrayHasKey('code', $results[$key]);
        $this->assertEquals($key, $results[$key]['code']);

        $this->arrayHasKey('name', $results[$key]);
        $this->assertNotEmpty($results[$key]['name']);
    }

    /**
     * @dataProvider getDownloadWriteDrugCollumnsForAnyProvider
     *
     * @testdox Checking downloadWriteDrugCollumnsForComplete / Less(): Stacks
     */
    public function testDownloadWriteDrugCollumnsForBoth($type, $maxKeywordCount, $expects_complete, $expects_less)
    {
        switch ($type) {
            case 'experimental':
                $repo = $this->entityManager->getRepository(Experimental::class);
                break;

            case 'generic':
                $repo = $this->entityManager->getRepository(Generic::class);
                break;

            case 'brand':
                $repo = $this->entityManager->getRepository(Brand::class);
                break;
        }
        $drugs = $repo->getDrugFamily($expects_complete[1], 'name');

        $this->updateInterventionDownloadWriteDrugCollumnsForBoth($expects_complete, $drugs['main']);
        $result = DRHelpers::downloadWriteDrugCollumnsForComplete(array($type => $drugs), $maxKeywordCount);
        $this->assertSame($expects_complete, $result);

        $this->updateInterventionDownloadWriteDrugCollumnsForBoth($expects_less, $drugs['main']);
        $result = DRHelpers::downloadWriteDrugCollumnsForLess(array($type => $drugs['main']));
        $this->assertSame($expects_less, $result);
    }

    /**
     * @see GenericDrugRepoFixtures
     */
    public function getDownloadWriteDrugCollumnsForAnyProvider()
    {
        return array(
            array(
                'generic',
                array(
                    'generic' => array(
                        'synonym' => 5,
                        'typo'    => 5,
                    ),
                ),
                array(
                    // drug
                    'INTERVENTION::GET_ID',
                    'test-1',
                    'Yes',
                    'ABC123',
                    'test-1-ABC123-Comment',
                    '',
                    'A1',
                    'A2',
                    'A3',
                    'A4',
                    'A5',
                    'Yes',
                    'Yes',
                    // keyword - synonym
                    'test-1S-A',
                    'AU, US',
                    'test-1S-B',
                    'MY, JP',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    // keyword - typo
                    'test-1T-A',
                    'test-1T-B',
                    'test-1T-C',
                    '',
                    '',
                ),
                array(
                    // drug
                    'INTERVENTION::GET_ID',
                    'test-1',
                ),
            ),
            array(
                'generic',
                array(
                    'generic' => array(
                        'synonym' => 2,
                        'typo'    => 2,
                    ),
                ),
                array(
                    // drug
                    'INTERVENTION::GET_ID',
                    'test-3',
                    'Yes',
                    'GHI789',
                    'test-3-GHI789-Comment',
                    'INTERVENTION::CHECK_DATE',
                    'C1',
                    'C2',
                    'C3',
                    'C4',
                    'C5',
                    'Yes',
                    'No',
                    // keyword - synonym
                    '',
                    '',
                    '',
                    '',
                    // keyword - typo
                    '',
                    '',
                ),
                array(
                    // drug
                    'INTERVENTION::GET_ID',
                    'test-3',
                ),
            ),
            array(
                'brand',
                array(
                    'brand' => array(
                        'synonym' => 1,
                        'typo'    => 2,
                    ),
                ),
                array(
                    // drug
                    'INTERVENTION::GET_ID',
                    'test-uplink',
                    'Yes',
                    'UPLINK',
                    'test-UPLINK-Comment',
                    '',
                    'No',
                    // keyword - synonym
                    '',
                    '',
                    // keyword - typo
                    '',
                    '',
                ),
                array(
                    // drug
                    'INTERVENTION::GET_ID',
                    'test-uplink',
                ),
            ),
            array(
                'experimental',
                array(),
                array(
                    // drug
                    'INTERVENTION::GET_ID',
                    'test-downlink',
                    'Experimental',
                    'Yes',
                    'DOWNLINK',
                    'test-DOWNLINK-Comment',
                    '',
                ),
                array(
                    // drug
                    'INTERVENTION::GET_ID',
                    'test-downlink',
                ),
            ),
            array(
                'experimental',
                array(),
                array(
                    // drug
                    '',
                    'INTERVENTION::REMOVE_THIS',
                    '',
                    '',
                    '',
                    '',
                    '',
                ),
                array(
                    // drug
                    '',
                    '',
                ),
            ),
            array(
                'generic',
                array(
                    'generic' => array(
                        'synonym' => 3,
                        'typo'    => 3,
                    ),
                ),
                array(
                    // drug
                    '',
                    'INTERVENTION::REMOVE_THIS',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    // keyword - synonym
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    // keyword - typo
                    '',
                    '',
                    '',
                ),
                array(
                    // drug
                    '',
                    '',
                ),
            ),
            array(
                'brand',
                array(
                    'brand' => array(
                        'synonym' => 1,
                        'typo'    => 1,
                    ),
                ),
                array(
                    // drug
                    '',
                    'INTERVENTION::REMOVE_THIS',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    // keyword - synonym
                    '',
                    '',
                    // keyword - typo
                    '',
                ),
                array(
                    // drug
                    '',
                    '',
                ),
            ),
        );
    }

    private function updateInterventionDownloadWriteDrugCollumnsForBoth(&$set, $drug)
    {
        foreach ($set as $ei => $expect) {
            if (false !== strpos($expect, 'INTERVENTION::')) {
                $help = explode('::', $expect);
                switch ($help[1]) {
                    case 'GET_ID':
                        if (!empty($drug)) {
                            $set[$ei] = $drug->getId();
                        }
                        break;

                    case 'CHECK_DATE':
                        if (!empty($drug)) {
                            $set[$ei] = $drug->getCreationDate()->format('Y-m-d H:i:s');
                        }
                        break;

                    case 'REMOVE_THIS':
                        $set[$ei] = '';
                        break;
                }
            }
        }
    }
}
