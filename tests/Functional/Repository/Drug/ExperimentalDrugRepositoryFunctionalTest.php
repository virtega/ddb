<?php

namespace App\Tests\Functional\Repository\Drug;

use App\Entity\Experimental;
use App\Entity\Generic;
use App\Tests\Fixtures\Drug\ExperimentalDrugRepoFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox FUNCTIONAL | Repository | Experimental Drug
 */
class ExperimentalDrugRepositoryFunctionalTest extends KernelTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var ExperimentalRepository
     */
    private $repo;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new ExperimentalDrugRepoFixtures());

        $purger   = new ORMPurger($this->entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());

        parent::setUp();

        $this->repo = $this->entityManager->getRepository(Experimental::class);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $purger   = new ORMPurger($this->entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->purge();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    /**
     * @testdox Checking get Family Formatting
     */
    public function testGetDrugFamily()
    {
        $res = $this->repo->getDrugFamily('test-1', 'name');

        $this->assertEquals(1, count($res));

        $this->assertArrayHasKey('main', $res);

        $this->assertEquals('test-1', $res['main']->getName());
        $this->assertEquals('ABC123', $res['main']->getUid());
        $this->assertEquals(1, $res['main']->getValid());

        // uplink
        $up = $this->entityManager->getRepository(Generic::class)->findOneBy(array('experimental' => $res['main']->getId()));
        $this->assertEquals('test-uplink', $up->getName());
    }

    /**
     * @testdox Checking Drug Update Process
     */
    public function testUpdateDrug()
    {
        $drug = $this->repo->findOneBy(array('name' => 'test-2'));

        $update = array(
            'name'     => 'test-2B',
            'comments' => 'test-2B-DEF123-Comment-Update',
        );
        $res = $this->repo->updateDrug($drug->getId(), $update);
        $this->repo->flushAllNow();

        $this->assertNotNull($res->getId());

        $this->assertEquals($drug->getType(), $res->getType());
        $this->assertEquals($drug->getUid(), $res->getUid());

        $this->assertEquals($update['name'], $res->getName());
        $this->assertEquals($update['comments'], $res->getComments());

        $now = new \DateTime();
        $this->assertEquals($now->format('Y-m-d H'), $res->getCreationDate()->format('Y-m-d H'));
    }

    /**
     * @testdox Checking Drug Insert Process
     */
    public function testInsertDrug()
    {
        $creationDt = new \DateTime();
        $new = array(
            'name'          => 'test-4',
            'type'          => 'experimental',
            'valid'         => 0,
            'unid'          => 'JKL123',
            'comments'      => 'test-4-JKL123-Comment-Insert',
            'creation_date' => $creationDt->format('Y-m-d H:i:s'),
        );
        $this->repo->updateDrug(null, $new);
        $this->repo->flushAllNow();

        $res = $this->repo->findOneBy(array('name' => $new['name']));

        $this->assertNotNull($res->getId());
        $this->assertEquals($new['name'], $res->getName());
        $this->assertEquals($new['valid'], $res->getValid());
        $this->assertEquals($new['type'], $res->getType());
        $this->assertEquals($new['unid'], $res->getUid());
        $this->assertEquals($new['comments'], $res->getComments());

        $this->assertEquals($creationDt->format('Y-m-d H:i'), $res->getCreationDate()->format('Y-m-d H:i'));
    }

    /**
     * @testdox Checking Drug Delete Process
     */
    public function testDeleteDrug()
    {
        $drug = $this->repo->findOneBy(array('name' => 'test-1'));
        $main = $drug->getId();

        // by object
        $count = $this->repo->removeDrug($drug);
        $this->repo->flushAllNow();

        // number of drug removed
        $this->assertEquals((1 + 1), $count);

        // uplink
        $found = $this->entityManager
        ->getRepository(Generic::class)
        ->findOneBy(array('experimental' => $main));
        $this->assertNull($found);

        // double check
        $found = $this->repo->findOneBy(array('name' => 'test-1'));
        $this->assertNull($found);

        // reheck but pass id
        $drug  = $this->repo->findOneBy(array('name' => 'test-3'));
        $count = $this->repo->removeDrug($drug->getId());
        $this->repo->flushAllNow();
        $this->assertEquals(1, $count);
    }
}
