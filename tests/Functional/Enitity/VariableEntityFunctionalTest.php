<?php

namespace App\Tests\Functional\Entity\Drug;

use App\Entity\Generic;
use App\Entity\Variable;
use App\Tests\Fixtures\Drug\BrandDrugRepoFixtures;
use App\Tests\Fixtures\Drug\GenericDrugRepoFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox FUNCTIONAL | Entity | Variable
 */
class VariableEntityFunctionalTest extends KernelTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $entityManager;

    /**
     * @var VariableRepository
     */
    private static $varRepo;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new BrandDrugRepoFixtures());
        $loader->addFixture(new GenericDrugRepoFixtures());

        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($loader->getFixtures());

        self::$varRepo = self::$entityManager->getRepository(Variable::class);
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->purge();

        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * @dataProvider getVariableCountGetterProvider
     *
     * @testdox Checking Variable Count Getters
     */
    public function testVariableCountGetters($expect, $function, $arguments)
    {
        $arguments = array_values($arguments);

        $results = call_user_func_array(array(self::$varRepo, $function), $arguments);

        $this->assertSame($expect, $results);
    }

    public function getVariableCountGetterProvider()
    {
        return array(
            // #0
            array(2,
                'getExperimentalDrugCount',
                array('type' => null, 'valid' => null),
            ),
            // #1
            array(2,
                'getExperimentalDrugCount',
                array('type' => 'experimental', 'valid' => 1),
            ),
            // #2
            array(0,
                'getExperimentalDrugCount',
                array('type' => 'generic', 'valid' => 0),
            ),
            // #3
            array(0,
                'getExperimentalDrugCount',
                array('type' => 'brand', 'valid' => 1),
            ),
            // #4
            array(2,
                'getGenericDrugCount',
                array('valid' => 1, 'orphan' => 0),
            ),
            // #5
            array(1,
                'getGenericDrugCount',
                array('valid' => 0, 'orphan' => 1),
            ),
            // #6
            array(5,
                'getGenericDrugCount',
                array('valid' => null, 'orphan' => null),
            ),
            // #7
            array(1,
                'getBrandDrugCount',
                array('valid' => 0, 'orphan' => 1),
            ),
            // #8
            array(2,
                'getBrandDrugCount',
                array('valid' => 1, 'orphan' => 0),
            ),
            // #9
            array(3,
                'getBrandDrugCount',
                array('valid' => 1, 'orphan' => null),
            ),
            // #10
            array(5,
                'getBrandDrugCount',
                array('valid' => null, 'orphan' => null),
            ),
            // #11
            array(2,
                'getGenericSynonymCount',
                array('orphan' => null),
            ),
            // #12
            array(2,
                'getGenericSynonymCount',
                array('orphan' => 0),
            ),
            // #13
            array(0,
                'getGenericSynonymCount',
                array('orphan' => 1),
            ),
            // #14
            array(3,
                'getGenericTypoCount',
                array('orphan' => null),
            ),
            // #15
            array(3,
                'getGenericTypoCount',
                array('orphan' => 0),
            ),
            // #16
            array(0,
                'getGenericTypoCount',
                array('orphan' => 1),
            ),
            // #17
            array(2,
                'getBrandSynonymCount',
                array('orphan' => null),
            ),
            // #18
            array(2,
                'getBrandSynonymCount',
                array('orphan' => 0),
            ),
            // #19
            array(0,
                'getBrandSynonymCount',
                array('orphan' => 1),
            ),
            // #20
            array(3,
                'getBrandTypoCount',
                array('orphan' => null),
            ),
            // #21
            array(0,
                'getBrandTypoCount',
                array('orphan' => 1),
            ),
            // #22
            array(3,
                'getBrandTypoCount',
                array('orphan' => 0),
            ),
            // #23
            array(2,
                'getSpecificEBCount',
                array(),
            ),
            // #24
            array(0,
                'getSpecificEGCount',
                array(),
            ),
            // #25
            array(1,
                'getSpecificGBCount',
                array(),
            ),
            // #26
            array(0,
                'getSpecificExperimentalCount',
                array(),
            ),
            // #27
            array(2,
                'getSpecificGenericCount',
                array(),
            ),
            // #28
            array(2,
                'getSpecificBrandCount',
                array(),
            ),
            // #29
            array(0,
                'getDrugSpecificFieldCount',
                array('drug' => 'experimental', 'field' => 'creation_date'),
            ),
            // #30
            array(2,
                'getDrugSpecificFieldCount',
                array('drug' => 'brand', 'field' => 'creation_date'),
            ),
            // #31
            array(1,
                'getDrugSpecificFieldCount',
                array('drug' => 'generic', 'field' => 'creation_date'),
            ),
        );
    }
}
