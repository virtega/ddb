<?php

namespace App\Tests\Functional\Security;

use App\Tests\TestTraits\LdapUserTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox FUNCTIONAL | Security | LDAP Authenticator
 */
class LdapAuthenticatorFunctionalTest extends WebTestCase
{
    use LdapUserTrait;

    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client = null;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @testdox Checking checkCredentials()
     */
    public function testCheckCredentials()
    {
        try {
            $this->loginLdap('visitor', '');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\Symfony\Component\Ldap\Exception\ConnectionException::class, $e);
            $this->assertStringContainsStringIgnoringCase('invalid', $e->getMessage(), '', true);
        }

        try {
            $this->loginLdap('visitor', null, '');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\Symfony\Component\Security\Core\Exception\UsernameNotFoundException::class, $e);
            $this->assertStringContainsStringIgnoringCase('not found', $e->getMessage(), '', true);
        }

        try {
            $this->loginLdap('visitor', null, null, '');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\Symfony\Component\Security\Core\Exception\UsernameNotFoundException::class, $e);
            $this->assertStringContainsStringIgnoringCase('not found', $e->getMessage(), '', true);
        }


        try {
            $this->loginLdap('visitor', null, '', null);
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\Symfony\Component\Security\Core\Exception\UsernameNotFoundException::class, $e);
            $this->assertStringContainsStringIgnoringCase('not found', $e->getMessage(), '', true);
        }
    }
}
