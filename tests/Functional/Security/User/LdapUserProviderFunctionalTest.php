<?php

namespace App\Tests\Functional\Security;

use App\Security\User\LdapUser;
use App\Security\User\LdapUserProvider;
use App\Tests\TestTraits\LdapUserTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * @testdox FUNCTIONAL | Security | User | LDAP User Provider
 */
class LdapUserProviderFunctionalTest extends WebTestCase
{
    use LdapUserTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Client
     */
    private $client = null;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @testdox Checking loadUserByUsername() Illegal binding method
     */
    public function testLoadUserByUsernameIllegal()
    {
        $this->adjustUserRoles('user');

        $ldapProvider = $this->client->getContainer()->get('test.' . LdapUserProvider::class);

        try {
            $user = $ldapProvider->loadUserByUsername('test@mail.com');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(UsernameNotFoundException::class, $e);
            $this->assertStringContainsStringIgnoringCase('Binding were skipped', $e->getMessage(), '');
        }
    }

    /**
     * @testdox Checking loadUserByUsername() Unknown User
     */
    public function testLoadUserByUsernameUnknown()
    {
        $this->adjustUserRoles('user');

        $ldapProvider = $this->client->getContainer()->get('test.' . LdapUserProvider::class);
        $ldapProvider->bindingLdapForQuery(getEnv('LDAP_LOGIN_USERNAME'), getEnv('LDAP_LOGIN_PASSWORD'));

        try {
            $user = $ldapProvider->loadUserByUsername('test@mail.com');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(UsernameNotFoundException::class, $e);
            $this->assertStringContainsStringIgnoringCase('not found', $e->getMessage(), '');
        }

        try {
            $user = $ldapProvider->loadUserByUsername('alien.test.user.unknown');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(UsernameNotFoundException::class, $e);
            $this->assertStringContainsStringIgnoringCase('not found', $e->getMessage(), '');
        }
    }


    /**
     * @testdox Checking loadUserByUsername() Wrong Query Format
     */
    public function testLoadUserByUsernameWrongQueryFormat()
    {
        $this->adjustUserRoles('user');

        $ldapProvider = $this->client->getContainer()->get('test.' . LdapUserProvider::class);
        $ldapProvider->bindingLdapForQuery(getEnv('LDAP_LOGIN_USERNAME'), getEnv('LDAP_LOGIN_PASSWORD'));

        $query = getenv('LDAP_EXTRA_QUERY');
        putenv('LDAP_EXTRA_QUERY="(this not | a | right format!)"');

        try {
            $user = $ldapProvider->loadUserByUsername('alien.test.user.unknown');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(UsernameNotFoundException::class, $e);
            $this->assertStringContainsStringIgnoringCase('not found', $e->getMessage(), '');
        }

        putenv('LDAP_EXTRA_QUERY=' . $query);
    }

    /**
     * @testdox Checking loadUserByUsername() OK
     */
    public function testLoadUserByUsernameOk()
    {
        $error = false;

        $ldapProvider =  $this->client->getContainer()->get('test.' . LdapUserProvider::class);

        try {
            $this->adjustUserRoles('admin');

            $ldapProvider->bindingLdapForQuery(getEnv('LDAP_LOGIN_EMAIL'), getEnv('LDAP_LOGIN_PASSWORD'));
            $user = $ldapProvider->loadUserByUsername(getEnv('LDAP_LOGIN_EMAIL'));

            $this->assertInstanceOf(LdapUser::class, $user);
        }
        catch (\Exception $e) {
            $error = $e;
        }

        $this->assertEmpty($error);
    }
}
