<?php

namespace App\Tests\Functional\Security;

use App\Security\User\LdapUser;
use App\Tests\TestTraits\LdapUserTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Ldap\Entry;

/**
 * @testdox FUNCTIONAL | Security | User | LDAP User
 */
class LdapUserFunctionalTest extends WebTestCase
{
    use LdapUserTrait;

    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client = null;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @testdox Checking Basic Object
     */
    public function testBasicObject()
    {
        try {
            $ent = new Entry('', []);
            $user = new LdapUser('', null, [], $ent);
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\InvalidArgumentException::class, $e);
            $this->assertStringContainsStringIgnoringCase('empty', $e->getMessage(), '');
        }
    }

    /**
     * @testdox Checking Structure
     */
    public function testStructure()
    {
        $user = $this->loginLdap('admin');
        $entry = $user->getLdapEntry();

        $this->assertEquals($entry->getAttribute('sAMAccountName')[0], $user->getUsername());
        $this->assertSame(self::$ldapUserRolesHierarchy['admin'], $user->getRoles());
        $this->assertInstanceOf(Entry::class, $entry);

        $this->assertEquals('', $user->getPassword());
        $this->assertEquals($entry->getAttribute('name')[0], $user->getDisplayName());
        $this->assertEquals($entry->getAttribute('givenName')[0], $user->getNickName());
        $this->assertEquals($entry->getAttribute('title')[0], $user->getPosition());

        $this->assertStringContainsStringIgnoringCase($entry->getAttribute('mail')[0], $user->getEmail(), '');

        $flatten = (string) $user;
        $this->assertEquals($user->getUsername(), $flatten);

        $clone = clone $user;
        $this->assertTrue($user->isEqualTo($clone));

        $mockedEnt  = $this->getMockEntry('Irfan.Mikael', 'Irfan.Mikael@gmail.com');
        $mockedUser = new LdapUser('Irfan.Mikael', null, array('ROLE_VISITOR'), $mockedEnt);
        $this->assertFalse($user->isEqualTo($mockedUser));

        $mockedEnt  = $this->getMockEntry(getEnv('LDAP_LOGIN_USERNAME'), 'Irfan.Mikael@gmail.com');
        $mockedUser = new LdapUser(getEnv('LDAP_LOGIN_USERNAME'), null, array('ROLE_VISITOR'), $mockedEnt);
        $this->assertFalse($user->isEqualTo($mockedUser));

        $mockedEnt     = $this->getMockEntry(getEnv('LDAP_LOGIN_USERNAME'), strtolower(getEnv('LDAP_LOGIN_EMAIL')));
        $mockedRoles   = $user->getRoles();
        $mockedRoles[] = 'ROLE_UNKNOWN';
        $mockedUser    = new LdapUser(getEnv('LDAP_LOGIN_USERNAME'), null, $mockedRoles, $mockedEnt);
        $this->assertFalse($user->isEqualTo($mockedUser));
    }

    private function getMockEntry($username, $email, $name = 'Mocked', $title = 'Tester')
    {
        return $this->createTestProxy(Entry::class, array(
            'dn'         => 'mocked',
            'attributes' => array(
                'sAMAccountName' => array($username),
                'name'           => array($username),
                'givenName'      => array($name),
                'mail'           => array($email),
                'title'          => array($title),
            ),
        ));
    }
}
