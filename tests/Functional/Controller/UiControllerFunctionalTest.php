<?php

namespace App\Tests\Functional\Controller;

use App\Entity\Brand;
use App\Entity\Experimental;
use App\Entity\Generic;
use App\Entity\Variable;
use App\Tests\Functional\BaseFunctionalTest;
use App\Tests\Fixtures\CountryFixtures;
use App\Tests\Fixtures\Drug\BrandDrugRepoFixtures;
use App\Tests\Fixtures\Drug\ExperimentalDrugRepoFixtures;
use App\Tests\Fixtures\Drug\GenericDrugRepoFixtures;
use App\Tests\TestTraits\LdapUserTrait;
use App\Utility\LotusConnection;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;

/**
 * @testdox FUNCTIONAL | Controller | UI
 */
class UiControllerFunctionalTest extends WebTestCase
{
    use LdapUserTrait;
    use \App\Tests\TestTraits\EnvHardChangeTrait;

    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $entityManager;

    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var array
     */
    private $ids;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new ExperimentalDrugRepoFixtures());
        $loader->addFixture(new GenericDrugRepoFixtures());
        $loader->addFixture(new BrandDrugRepoFixtures());
        $loader->addFixture(new CountryFixtures());
        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($loader->getFixtures());

        $conn = self::$entityManager->getConnection();
        $conn->exec('TRUNCATE TABLE `variable`;');

        $application = new Application(static::$kernel);
        $application->setAutoExit(false);

        $fp = tmpfile();
        $input    = new StringInput('app:update-widgets');
        $output = new StreamOutput($fp);
        $application->run($input, $output);
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->purge();

        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->ids = array(
            'experimental'    => self::$entityManager->getRepository(Experimental::class)->getDrug('test-1', 'name')->getId(),
            'experimental-gn' => self::$entityManager->getRepository(Experimental::class)->getDrug('test-2', 'name')->getId(),
            'experimental-dt' => self::$entityManager->getRepository(Experimental::class)->getDrug('test-3', 'name')->getId(),
            'generic'         => self::$entityManager->getRepository(Generic::class)->getDrug('test-1', 'name')->getId(),
            'generic-dt'      => self::$entityManager->getRepository(Generic::class)->getDrug('test-3', 'name')->getId(),
            'brand'           => self::$entityManager->getRepository(Brand::class)->getDrug('test-1', 'name')->getId(),
            'brand-dt'        => self::$entityManager->getRepository(Brand::class)->getDrug('test-3', 'name')->getId(),
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->client = null;
    }

    /**
     * @testdox checking Page Availability via Router & Path, without login
     */
    public function testUIFailLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/page-did-not-exits');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $client->request('GET', '/dashboard');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase('Redirecting to /login', $client->getResponse()->getContent());

        $client->request('GET', '/page-did-not-exits-after-login');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getPageDetailsProvider
     *
     * @testdox Checking Page Availability via Router & Path
     */
    public function testPageLoads($pageName, $pageUrl, $method = 'GET', $elements = array(), $expectStatus = 200, $contentCheck = false)
    {
        $this->login();

        $pageParams = array();
        if ('GET' == $method) {
            if (isset($elements['id'])) {
                if (is_string($elements['id'])) {
                    $elements['id'] = $this->ids[$elements['id']];
                }
                $pageParams['id'] = $elements['id'];
                $pageUrl .= $elements['id'];
            }
            // just copy
            foreach (['q', 'type', 'has-code'] as $gt) {
                if (isset($elements[$gt])) {
                    $pageParams[$gt] = $elements[$gt];
                }
            }
        }

        $url = $this->router->generate($pageName, $pageParams);
        $this->assertSame($pageUrl, $url);

        $this->client->request($method, $url);

        $this->assertEquals($expectStatus, $this->client->getResponse()->getStatusCode());

        if (!empty($contentCheck)) {
            $this->assertStringContainsStringIgnoringCase($contentCheck, $this->client->getResponse()->getContent());
        }
    }

    public function getPageDetailsProvider()
    {
        return array(
            // #0
            array(
                'app_logout',
                '/logout',
                'GET',
                array(),
                302,
                '/login',
            ),
            // #1 - logged in
            array(
                'login',
                '/login',
                'GET',
                array(),
                302
            ),
            // #2 - logged in
            array(
                'root',
                '/',
                'GET',
                array(),
                302
            ),
            // #3
            array(
                'dashboard',
                '/dashboard',
            ),
            // #4
            array(
                'drug_register_experimental',
                '/register/experimental',
            ),
            // #5
            array(
                'drug_register_generic',
                '/register/generic',
            ),
            // #6
            array(
                'drug_register_brand',
                '/register/brand',
            ),
            // #7
            array(
                'drug_edit_experimental',
                '/modify/experimental/',
                'GET',
                array(
                    'id' => 'experimental',
                ),
            ),
            // #8
            array(
                'drug_edit_experimental',
                '/modify/experimental/',
                'GET',
                array(
                    'id' => 'experimental-gn',
                ),
            ),
            // #9
            array(
                'drug_edit_experimental',
                '/modify/experimental/',
                'GET',
                array(
                    'id' => 'experimental-dt',
                ),
            ),
            // #10
            array(
                'drug_edit_experimental',
                '/modify/experimental/',
                'GET',
                array(
                    'id' => 99999999,
                ),
                302,
            ),
            // #11
            array(
                'drug_edit_generic',
                '/modify/generic/',
                'GET',
                array(
                    'id' => 'generic',
                ),
            ),
            // #12
            array(
                'drug_edit_generic',
                '/modify/generic/',
                'GET',
                array(
                    'id' => 'generic-dt',
                ),
            ),
            // #13
            array(
                'drug_edit_generic',
                '/modify/generic/',
                'GET',
                array(
                    'id' => 99999999,
                ),
                302,
            ),
            // #14
            array(
                'drug_edit_brand',
                '/modify/brand/',
                'GET',
                array(
                    'id' => 'brand',
                ),
            ),
            // #15
            array(
                'drug_edit_brand',
                '/modify/brand/',
                'GET',
                array(
                    'id' => 'brand-dt',
                ),
            ),
            // #16
            array(
                'drug_edit_brand',
                '/modify/brand/',
                'GET',
                array(
                    'id' => 99999999,
                ),
                302,
            ),
            // #17
            array(
                'drug_open_map',
                '/drug/map/open',
            ),
            // #18
            array(
                'drug_map',
                '/drug/map/experimental/',
                'GET',
                array(
                    'type' => 'experimental',
                    'id'   => 'experimental',
                ),
            ),
            // #19
            array(
                'drug_map',
                '/drug/map/experimental/',
                'GET',
                array(
                    'type' => 'experimental',
                    'id'   => 99999999,
                ),
                302,
                '/drug/map/open',
            ),
            // #20
            array(
                'drug_map',
                '/drug/map/generic/',
                'GET',
                array(
                    'type' => 'generic',
                    'id'   => 'generic',
                ),
            ),
            // #21
            array(
                'drug_map',
                '/drug/map/generic/',
                'GET',
                array(
                    'type' => 'generic',
                    'id'   => 99999999,
                ),
                302,
                '/drug/map/open',
            ),
            // #22
            array(
                'drug_map',
                '/drug/map/brand/',
                'GET',
                array(
                    'type' => 'brand',
                    'id'   => 'brand',
                ),
            ),
            // #23
            array(
                'drug_map',
                '/drug/map/brand/',
                'GET',
                array(
                    'type' => 'brand',
                    'id'   => 99999999,
                ),
                302,
                '/drug/map/open',
            ),
            // #24
            array(
                'drug_search',
                '/drug/search',
            ),
            // #25
            array(
                'drug_search',
                '/drug/search',
                'POST',
                array(
                    'q'    => '^ryn',
                    'type' => 'experimental',
                ),
            ),
            // #26
            array(
                'drug_search',
                '/drug/search?q=' . urlencode('^ryn') . '&type=generic',
                'GET',
                array(
                    'q'    => '^ryn',
                    'type' => 'generic',
                ),
            ),
            // #27 - editor login - admin only page
            array(
                'admin_manage_roles',
                '/admin/manage-roles',
                'GET',
                array(),
                403
            ),
            // #28 - reengage with cache, with code
            array(
                'dashboard',
                '/dashboard',
                'GET',
                array(),
                200,
                'Showing Drugs count filtered by Generic with Full-Code only.'
            ),
            // #29 - dashboard no-code
            array(
                'dashboard',
                '/dashboard?has-code=no',
                'GET',
                array(
                    'has-code' => 'no'
                ),
                200,
                'Showing Drugs count by absolute records number.'
            ),
        );
    }

    /**
     * @testdox Checking Dashboard Page Widget data structure
     */
    public function testDashboardBasicWidgetStructure()
    {
        $this->removeDashboardCache();

        $this->login();
        $this->client->request('GET', '/dashboard');
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $data = self::$entityManager->getRepository(Variable::class)->getVariable('dashboard-widget-tokens-cache')->getValue();

        foreach (['has_code', 'no_code'] as $fc) {
            $this->assertArrayHasKey('head', $data[$fc]);
            $this->assertArrayHasKey('title', $data[$fc]['head']);

            $this->assertArrayHasKey('opt_has_code', $data[$fc]);

            $this->assertArrayHasKey('widget_value', $data[$fc]);
            $this->assertArrayHasKey('drugs', $data[$fc]['widget_value']);
                $this->assertEquals(5, count($data[$fc]['widget_value']['drugs']));
                $this->assertArrayHasKey('experimental_count', $data[$fc]['widget_value']['drugs']);
                $this->assertArrayHasKey('generic_count', $data[$fc]['widget_value']['drugs']);
                $this->assertArrayHasKey('brand_count', $data[$fc]['widget_value']['drugs']);
                $this->assertArrayHasKey('generic_keywords', $data[$fc]['widget_value']['drugs']);
                $this->assertArrayHasKey('brand_keywords', $data[$fc]['widget_value']['drugs']);
            $this->assertArrayHasKey('specific_drugs', $data[$fc]['widget_value']);
                $this->assertEquals(8, count($data[$fc]['widget_value']['specific_drugs']));
                $this->assertArrayHasKey('sum', $data[$fc]['widget_value']['specific_drugs']);
                $this->assertArrayHasKey('all', $data[$fc]['widget_value']['specific_drugs']);
                $this->assertArrayHasKey('e_g', $data[$fc]['widget_value']['specific_drugs']);
                $this->assertArrayHasKey('g_b', $data[$fc]['widget_value']['specific_drugs']);
                $this->assertArrayHasKey('b_g', $data[$fc]['widget_value']['specific_drugs']);
                $this->assertArrayHasKey('e', $data[$fc]['widget_value']['specific_drugs']);
                $this->assertArrayHasKey('g', $data[$fc]['widget_value']['specific_drugs']);
                $this->assertArrayHasKey('b', $data[$fc]['widget_value']['specific_drugs']);
            $this->assertArrayHasKey('drug_breaks_down', $data[$fc]['widget_value']);
                $this->assertEquals(9, count($data[$fc]['widget_value']['drug_breaks_down']));
            $this->assertArrayHasKey('top_level_stats', $data[$fc]['widget_value']);
                $this->assertEquals(9, count($data[$fc]['widget_value']['top_level_stats']));
                $this->assertArrayHasKey('drug_sync', $data[$fc]['widget_value']['top_level_stats']);
                $this->assertArrayHasKey('drug_initial', $data[$fc]['widget_value']['top_level_stats']);
                $this->assertArrayHasKey('journal_initial', $data[$fc]['widget_value']['top_level_stats']);
                $this->assertArrayHasKey('journal_data', $data[$fc]['widget_value']['top_level_stats']);
                $this->assertArrayHasKey('journal_sync', $data[$fc]['widget_value']['top_level_stats']);
                $this->assertArrayHasKey('conference_initial', $data[$fc]['widget_value']['top_level_stats']);
                $this->assertArrayHasKey('conference_data', $data[$fc]['widget_value']['top_level_stats']);
                $this->assertArrayHasKey('conference_sync', $data[$fc]['widget_value']['top_level_stats']);
                $this->assertArrayHasKey('cache', $data[$fc]['widget_value']['top_level_stats']);

            $this->assertArrayHasKey('template', $data[$fc]);

            $this->assertArrayHasKey('build', $data[$fc]);
        }
    }

    /**
     * @testdox Checking Dashboard Page Widget: Creation-date Count
     */
    public function testDashboardWidgetCalculationCreationDateCount()
    {
        $data = self::$entityManager->getRepository(Variable::class)->getVariable('dashboard-widget-tokens-cache')->getValue();
        foreach (array('has_code', 'no_code') as $fc) {
            foreach ($data[$fc]['widget_value']['top_level_stats']['drug_initial']['captions'] as $fi => $captions) {
                if (stripos($captions[1], 'creation date') !== false) {
                    $this->assertStringContainsStringIgnoringCase('0 Creation date synced', $captions[1]);
                    break;
                }
            }
        }
        self::$entityManager->getRepository(Variable::class)->updateWidgetValue('ext-script-import-creation-date-count', 2, 'integer', true);
        self::$entityManager->getRepository(Variable::class)->updateWidgetValue('ext-script-import-creation-date-last', 0, 'integer', true);

        $this->removeDashboardCache();

        $this->login();
        $this->client->request('GET', '/dashboard');
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $data = self::$entityManager->getRepository(Variable::class)->getVariable('dashboard-widget-tokens-cache')->getValue();
        foreach (array('has_code', 'no_code') as $fc) {
            foreach ($data[$fc]['widget_value']['top_level_stats']['drug_initial']['captions'] as $fi => $captions) {
                if (stripos($captions[1], 'creation dates') !== false) {
                    $this->assertStringContainsStringIgnoringCase('2 Creation dates Synced', $captions[1]);
                    break;
                }
            }
        }
    }

    /**
     * @testdox Checking Dashboard Page Widget: Countries count
     */
    public function testDashboardWidgetCalculationCountriescount()
    {
        $this->removeDashboardCache();

        $this->login();
        $this->client->request('GET', '/dashboard');
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $data = self::$entityManager->getRepository(Variable::class)->getVariable('dashboard-widget-tokens-cache')->getValue();
        foreach (array('has_code', 'no_code') as $fc) {
            foreach ($data[$fc]['widget_value']['top_level_stats']['drug_initial']['captions'] as $fi => $captions) {
                if (stripos($captions[1], 'countries') !== false) {
                    $this->assertStringContainsStringIgnoringCase('253 Countries synced', $captions[1]);
                    break;
                }
            }
        }
    }

    /**
     * @testdox Checking Dashboard Page Widget: UpSync Indicator
     */
    public function testDashboardWidgetCalculationUpsyncIndicator()
    {
        if (!BaseFunctionalTest::getLotusStatus()) {
            $this->markTestSkipped('Skip as Lotus Server is not Reachable.');
            return;
        }

        $dsn = getenv('LOTUS_DB_DSN');
        if (empty($dsn)) {
            $this->markTestSkipped('Skipping an assertion, as Remote DB is not configured.');

            return true;
        }

        $data = self::$entityManager->getRepository(Variable::class)->getVariable('dashboard-widget-tokens-cache')->getValue();
        foreach (array('has_code', 'no_code') as $fc) {
            $this->assertStringContainsString('Connection OK', $data[$fc]['widget_value']['top_level_stats']['drug_sync']['captions'][0][1]);
        }
    }

    /**
     * @dataProvider getDrugControllerPostProvider
     *
     * @testdox Checking Drug New / Edit POST: Stack
     */
    public function testDrugControllerPost($type, $giveParams)
    {
        $this->login();

        $url = $this->router->generate("drug_register_{$type}");
        $this->assertSame("/register/{$type}", $url);

        $params = array();
        if ($giveParams) {
            $params = array(
                'name'  => "{$type}-on-fly-test-creation-" . microtime(),
            );
        }
        else {
            $params = array(
                'not' => 'empty',
            );
        }

        if ($giveParams) {
            // new drug
            $this->client->request('POST', $url, $params);
            $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

            // get edit url with new id
            $editUrl = $this->client->getResponse()->headers->get('location');
            $this->assertRegExp("/\/modify\/{$type}\/[0-9]+/i", $editUrl);

            // re-sent as an update
            $params['valid'] = 1;
            $this->client->request('POST', $editUrl, $params);
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }
        else {
            $this->client->followRedirects(true);
            $this->client->request('POST', $url, $params);
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
            $this->assertStringContainsStringIgnoringCase('Drug Name is empty.', $this->client->getResponse()->getContent());
        }
    }

    public function getDrugControllerPostProvider()
    {
        return array(
            // 0 - EXPERIMENTAL REGISTRY: empty post
            array('experimental', false),
            // 1 - EXPERIMENTAL REGISTRY / EDIT: OK post
            array('experimental', true),
            // 2 - GENERIC REGISTRY: empty post
            array('generic', false),
            // 3 - GENERIC REGISTRY / EDIT: OK post
            array('generic', true),
            // 4 - BRAND REGISTRY: empty post
            array('brand', false),
            // 5 - BRAND REGISTRY / EDIT: OK post
            array('brand', true),
        );
    }

    /**
     * @testdox Checking Dashboard notice and Widget as Lotus Connection Failed
     */
    public function testLotusConnDashboardFailed()
    {
        $topDangerAlert = '.flash-messages .alert.alert-danger';
        $lotusCard      = '#drug_sync-conn-info-card';

        $this->_envBackup();

        $this->_envOverride('LOTUS_DB_DSN', 'host=impossible-domain.com.x.x.g:9999;dbname=A', true);
        $this->_envOverride('LOTUS_DB_UPSYNC', 'true', true);
        $this->removeDashboardCache();
        $this->login();

        $crawler = $this->client->request('GET', '/dashboard');
        $this->assertNotEmpty( $crawler->filter($topDangerAlert)->count() );
        $this->assertStringContainsStringIgnoringCase('Drugs Remote Connection error', $crawler->filter($topDangerAlert)->eq(0)->text());
        $this->assertStringContainsStringIgnoringCase('Host did not Reachable; ping failed', $crawler->filter($topDangerAlert)->eq(0)->text());
        $this->assertStringContainsStringIgnoringCase('Up-Sync will fail', $crawler->filter($lotusCard)->eq(0)->text());
        $this->assertStringContainsStringIgnoringCase('Unable to make connection', $crawler->filter($lotusCard)->eq(0)->text());

        $this->_envOverride('LOTUS_DB_DSN', 'host=google.com:9999;dbname=A', true);
        $this->_envOverride('LOTUS_DB_UPSYNC', 'true', true);
        $this->removeDashboardCache();
        $this->login();

        $crawler = $this->client->request('GET', '/dashboard');
        $this->assertNotEmpty( $crawler->filter($topDangerAlert)->count() );
        $this->assertStringContainsStringIgnoringCase('Drugs Remote Connection error', $crawler->filter($topDangerAlert)->eq(0)->text());
        $this->assertStringContainsStringIgnoringCase('Host did not Responded', $crawler->filter($topDangerAlert)->eq(0)->text());
        $this->assertStringContainsStringIgnoringCase('Up-Sync will fail', $crawler->filter($lotusCard)->eq(0)->text());
        $this->assertStringContainsStringIgnoringCase('Unable to make connection', $crawler->filter($lotusCard)->eq(0)->text());

        $this->_envRestore();
    }

    /**
     * @testdox Checking Dashboard notice and Widget as Lotus Connection is OK
     */
    public function testLotusConnDashboardUpOk()
    {
        $lotusCard = '#drug_sync-conn-info-card';

        if (!BaseFunctionalTest::getLotusStatus()) {
            $this->markTestSkipped('Skip as Lotus Server is not Reachable.');
            return;
        }

        $this->login();
        $lotusConn = $this->client->getContainer()->get('test.' . LotusConnection::class);

        if ($lotusConn->isUpSync()) {
            $this->removeDashboardCache();
            $this->login();

            $crawler = $this->client->request('GET', '/dashboard');
            $this->assertStringContainsStringIgnoringCase('Connection OK', $crawler->filter($lotusCard)->eq(0)->text());

            $this->_envBackup();

            $this->_envOverride('LOTUS_DB_UPSYNC', 'false', true);
            $this->removeDashboardCache();
            $this->login();

            $crawler = $this->client->request('GET', '/dashboard');
            $this->assertStringContainsStringIgnoringCase('Up-Sync disabled', $crawler->filter($lotusCard)->eq(0)->text());

            $this->_envRestore();
        }
        else {
            $this->markTestSkipped('Skip as Lotus Server UpSync is Disabled.');
            return;
        }
    }

    private function login()
    {
        $this->client = static::createClient(['debug' => false]);
        $this->router = $this->client->getContainer()->get('router');
        $this->loginLdap('editor');
    }

    private function removeDashboardCache()
    {
        $cache = self::$entityManager->getRepository(Variable::class)->getVariable('dashboard-widget-tokens-cache');
        if ($cache) {
            self::$entityManager->remove($cache);
            self::$entityManager->flush();
        }
    }
}
