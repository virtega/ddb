<?php

namespace App\Tests\Functional\Controller\Security;

use App\Tests\TestTraits\LdapUserTrait;
use App\Tests\TestTraits\MaintenanceTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox FUNCTIONAL | Controller-Security | API
 */
class ApiControllerSecurityFunctionalTest extends WebTestCase
{
    use LdapUserTrait;
    use MaintenanceTrait;

    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client = null;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();

        $this->mntnncSetMaintenanceModeOff();
    }

    /**
     * @testdox Checking Unauthenticated Access
     */
    public function testDefaultApiControllerUnauthenticated()
    {
        $this->client->request('GET', '/v1/hello');
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());

        $this->client->request('POST', '/v1/hello');
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @testdox Checking On Exceptions Access
     */
    public function testDefaultApiControllerError()
    {
        $this->client->request('POST', '/v1/this-path-is-not-available');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

        $headers = array(
            'CONTENT_TYPE'     => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'  => 'HTTP/1.0',
            'HTTP_ACCEPT'      => 'application/json',
            'CACHE_CONTROL'    => 'no-cache',
        );
        $this->client->request('POST', '/v1/map-update-drug-relations', array(), array(), $headers);
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());

        $headers = array(
            'CONTENT_TYPE'     => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'  => 'HTTP/1.0',
            'HTTP_ACCEPT'      => 'application/json',
            'CACHE_CONTROL'    => 'no-cache',
        );
        $this->ldapUserApiHeading($headers, 'user');

        $this->client->request('POST', '/v1/hello', array(), array(), $headers);

        if (200 != $this->client->getResponse()->getStatusCode()) {
            die(
                PHP_EOL
                . PHP_EOL
                . str_repeat('=', 60)
                . PHP_EOL
                . 'UnitTest Halted. Please recheck your LDAP credentials.'
                . PHP_EOL
                . str_repeat('=', 60)
                . PHP_EOL
                . PHP_EOL
            );
        }

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $headers = array(
            'CONTENT_TYPE'     => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'  => 'HTTP/1.0',
            'HTTP_ACCEPT'      => 'application/json',
            'CACHE_CONTROL'    => 'no-cache',
        );
        $this->ldapUserApiHeading($headers, 'user');

        $this->client->request('POST', '/v1/search-by-name', array(), array(), $headers);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @testdox Checking Authenticated Access with X-API-TOKEN Header
     */
    public function testDefaultApiControllerAuthenticatedWithXAPITOKEN()
    {
        $headers = array(
            'CONTENT_TYPE'     => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'  => 'HTTP/1.0',
            'HTTP_ACCEPT'      => 'application/json',
            'CACHE_CONTROL'    => 'no-cache'
        );
        $this->ldapUserApiHeading($headers, 'user', 'HTTP_X-API-TOKEN');

        $crawler = $this->client->request('POST', '/v1/hello', array(), array(), $headers);
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        return $response;
    }

    /**
     * @depends testDefaultApiControllerAuthenticatedWithXAPITOKEN
     *
     * @testdox Checking the Response structure
     */
    public function testDefaultApiControllerAuthenticatedResponse($response)
    {
        $payload = $response->getContent();
        $payload = json_decode($payload, true);

        $this->assertNotEmpty($payload);

        $this->assertArrayHasKey('msg', $payload);
        $this->assertArrayHasKey('data', $payload);
        $this->assertArrayHasKey('success', $payload);
        $this->assertArrayHasKey('drugdb', $payload);
        $this->assertArrayHasKey('version', $payload['drugdb']);
        $this->assertArrayHasKey('date', $payload['drugdb']);

        $this->assertNotEmpty($payload['msg']);
        $this->assertNotEmpty($payload['success']);
    }

    /**
     * @testdox Checking Authenticated Access with X-AUTH-TOKEN Header
     */
    public function testDefaultApiControllerAuthenticatedWithXAUTHTOKEN()
    {
        $headers = array(
            'CONTENT_TYPE'      => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'   => 'HTTP/1.0',
            'HTTP_ACCEPT'       => 'application/json',
            'CACHE_CONTROL'     => 'no-cache',
        );
        $this->ldapUserApiHeading($headers, 'user', 'HTTP_X-AUTH-TOKEN');

        $crawler = $this->client->request('POST', '/v1/hello', array(), array(), $headers);
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @testdox Checking Authenticated Access with prefixed username
     */
    public function testDefaultApiControllerAuthenticatedWithPrefixedUsername()
    {
        $prefix = 'PSL\\';
        $username = getenv('LDAP_LOGIN_USERNAME');
        if (strpos($username, 'PSL\\') !== false) {
            $username = str_replace($prefix, '', $username);
        }

        $headers = array(
            'CONTENT_TYPE'       => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'    => 'HTTP/1.0',
            'HTTP_ACCEPT'        => 'application/json',
            'CACHE_CONTROL'      => 'no-cache',
        );
        $this->ldapUserApiHeading($headers, 'user', 'HTTP_X-API-TOKEN', $prefix . $username);

        $crawler = $this->client->request('POST', '/v1/hello', array(), array(), $headers);
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @testdox Checking Wrong Authentication
     */
    public function testDefaultApiControllerWrongAuthentication()
    {
        $headers = array(
            'CONTENT_TYPE'      => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'   => 'HTTP/1.0',
            'HTTP_ACCEPT'       => 'application/json',
            'CACHE_CONTROL'     => 'no-cache',
            'HTTP_X-AUTH-TOKEN' => 'this-is-not-the-key-' . time(),
        );
        $this->ldapUserApiHeading($headers, 'user', 'DUMMY-HEADING');

        $crawler = $this->client->request('POST', '/v1/hello', array(), array(), $headers);
        $response = $this->client->getResponse();
        $this->assertEquals(401, $response->getStatusCode());
    }

    /**
     * @testdox Checking Missing Config Authentication
     */
    public function testDefaultApiControllerMissingConfigAuthentication()
    {
        $token = getenv('SECURITY_API_TOKEN');

        putenv('SECURITY_API_TOKEN=');
        $headers = array(
            'CONTENT_TYPE'      => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'   => 'HTTP/1.0',
            'HTTP_ACCEPT'       => 'application/json',
            'CACHE_CONTROL'     => 'no-cache',
            'HTTP_X-AUTH-TOKEN' => 'this-is-not-the-key-' . time(),
        );
        $this->ldapUserApiHeading($headers, 'user', 'DUMMY-HEADING');

        $crawler = $this->client->request('POST', '/v1/hello', array(), array(), $headers);
        $response = $this->client->getResponse();
        $this->assertEquals(401, $response->getStatusCode());

        putenv('SECURITY_API_TOKEN=' . $token);
    }

    /**
     * @testdox Checking API on Maintenance Mode
     */
    public function testDefaultApiControllerOnMaintenanceMode()
    {
        $this->mntnncSetMaintenanceModeOn();

        $headers = array(
            'CONTENT_TYPE'      => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'   => 'HTTP/1.0',
            'HTTP_ACCEPT'       => 'application/json',
            'CACHE_CONTROL'     => 'no-cache',
        );
        $this->ldapUserApiHeading($headers, 'user');

        $crawler = $this->client->request('POST', '/v1/hello', array(), array(), $headers);
        $response = $this->client->getResponse();
        $this->assertEquals(503, $response->getStatusCode());


        $this->mntnncSetMaintenanceModeOff();
    }

    /**
     * @testdox Checking Admin API Access and Attributes
     */
    public function testAdminAPIAccessAndAttributes()
    {
        $headers = array(
            'CONTENT_TYPE'     => 'application/x-www-form-urlencoded',
            'SERVER_PROTOCOL'  => 'HTTP/1.0',
            'HTTP_ACCEPT'      => 'application/json',
            'CACHE_CONTROL'    => 'no-cache',
        );

        $this->ldapUserApiHeading($headers, 'editor');
        $this->client->request('GET', '/v1/user-roles-list', array(), array(), $headers);
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());

        $this->ldapUserApiHeading($headers, 'editor');
        $this->client->request('POST', '/v1/set-user-role', array(), array(), $headers);
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());

        $this->ldapUserApiHeading($headers, 'editor');
        $this->client->request('POST', '/v1/remove-user-role', array(), array(), $headers);
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());

        $this->ldapUserApiHeading($headers, 'admin');
        $this->client->request('GET', '/v1/user-roles-list', array(), array(), $headers);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(isset($response['data']));
        $this->assertTrue(isset($response['data'][0]['id']));
        $this->assertTrue(isset($response['data'][0]['username']));
        $this->assertTrue(isset($response['data'][0]['role']));

        $this->ldapUserApiHeading($headers, 'admin');
        $this->client->request('POST', '/v1/set-user-role', array(
            'role' => 'admin',
        ), array(), $headers);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase('Missing Argument', $this->client->getResponse()->getContent(), '', true);
        $this->assertStringContainsString('username', $this->client->getResponse()->getContent(), '', true);

        $this->ldapUserApiHeading($headers, 'admin');
        $this->client->request('POST', '/v1/set-user-role', array(
            'username' => 'Irfan.Mikael',
        ), array(), $headers);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase('Missing Argument', $this->client->getResponse()->getContent(), '', true);
        $this->assertStringContainsString('role', $this->client->getResponse()->getContent(), '', true);

        $this->ldapUserApiHeading($headers, 'admin');
        $this->client->request('POST', '/v1/set-user-role', array(
            'username' => 'Irfan.Mikael',
            'role'     => 'ROLE_ADMIN',
        ), array(), $headers);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase('unknown role type', $this->client->getResponse()->getContent(), '', true);

        $this->ldapUserApiHeading($headers, 'admin');
        $this->client->request('POST', '/v1/set-user-role', array(
            'username' => 'Irfan.Mikael',
            'role'     => 'admin'
        ), array(), $headers);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue($response['success']);

        $this->ldapUserApiHeading($headers, 'admin');
        $this->client->request('POST', '/v1/remove-user-role', array(), array(), $headers);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase('Missing Argument', $this->client->getResponse()->getContent(), '', true);
        $this->assertStringContainsString('username', $this->client->getResponse()->getContent(), '', true);

        $this->ldapUserApiHeading($headers, 'admin');
        $this->client->request('POST', '/v1/remove-user-role', array(
            'username' => 'Irfan.Mikael',
        ), array(), $headers);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue($response['success']);
    }
}
