<?php

namespace App\Tests\Functional\Controller\Security;

use App\Service\LoginCookieService;
use App\Tests\TestTraits\LdapUserTrait;
use App\Tests\TestTraits\MaintenanceTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox FUNCTIONAL | Controller-Security | UI
 */
class UiControllerSecurityFunctionalTest extends WebTestCase
{
    use LdapUserTrait;
    use MaintenanceTrait;

    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client = null;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();

        $this->mntnncSetMaintenanceModeOff();
    }

    /**
     * @testdox Checking Unauthenticated Access
     */
    public function testDefaultUiControllerUnauthenticated()
    {
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->client->request('GET', '/dashboard');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        $this->client->request('GET', '/alien-page-' . time());
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @testdox Checking Simple Flash Messages on login
     */
    public function testDefaultUiControllerSimpleFlashMessages()
    {
        $alerts = [
            'msg'     => 'info',
            'error'   => 'danger',
            'danger'  => 'danger',
            'warning' => 'warning',
            'success' => 'success',
            'info'    => 'info'
        ];
        $msg = 'This is a test message';

        foreach ($alerts as $get => $class) {
            $crawler = $this->client->request('GET', '/login?' . $get . '=' . urlencode($msg));
            $this->assertNotEmpty( $crawler->filter('.flash-messages .alert.alert-' . $class)->count() );
            $this->assertStringContainsString($msg, $crawler->filter('.flash-messages .alert.alert-' . $class)->eq(0)->text());
        }
    }

    /**
     * @testdox Checking Authentication Basic Requirement
     */
    public function testLoginControllerAuthenticationRequirement()
    {
        $this->client->followRedirects(true);

        $this->client->request('GET', '/login?redirect=/drug/search', array());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = $this->client->getResponse()->getContent();
        $found_tag = array();
        preg_match('/(?=.*type="hidden")(?=.*name="_target_path")(?=.*value="([^"]*)")/', $content, $found_tag);
        $this->assertEquals('/drug/search', $found_tag[1]);

        $this->client->request('POST', '/login', array('email' => 'test@test.com'));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertStringNotContainsStringIgnoringCase('Invalid credentials', $this->client->getResponse()->getContent());
    }

    /**
     * @testdox Checking Authentication
     */
    public function testLoginControllerAuthentication()
    {
        $this->client->followRedirects(true);
        $this->client->request('POST', '/login', array(
            '_email'    => 'test@test.com',
            '_password' => 'asd'
        ));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase('Invalid credentials', $this->client->getResponse()->getContent());
    }

    /**
     * @testdox Checking False Authentication
     */
    public function testLoginControllerAuthenticationFalse()
    {
        $this->client->followRedirects(true);
        $this->client->request('POST', '/login', array(
            '_email'    => 'this-is-not-an-email',
            '_password' => 'this-is-not-a-password',
        ));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase('Invalid credentials', $this->client->getResponse()->getContent());
    }

    /**
     * @testdox Checking Unauthenticated Access on Dashboard
     */
    public function testDashboardControllerUnauthenticated()
    {
        $this->client->followRedirects(false);
        $this->client->request('GET', '/dashboard');

        $this->assertFalse($this->client->getResponse()->isSuccessful());
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @testdox Checking Authentication Access on Dashboard
     */
    public function testDashboardControllerAuthentication()
    {
        $this->client->followRedirects(true);
        $this->client->request('POST', '/login', array(
            '_email'    => getEnv('LDAP_LOGIN_EMAIL'),
            '_password' => getEnv('LDAP_LOGIN_PASSWORD'),
        ));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $content = $this->client->getResponse()->getContent();

        $headers = $this->client->getResponse()->headers;
        $this->assertNotEmpty($headers->getCookies());

        $this->assertStringContainsString('page_dashboard', $content);

        $this->assertStringContainsString(getEnv('LDAP_LOGIN_EMAIL'), $content, true);
        $this->assertStringContainsString(getEnv('LDAP_LOGIN_USERNAME'), $content, true);
    }

    /**
     * @testdox Checking Authenticated Access on Dashboard
     */
    public function testDashboardControllerAuthenticated()
    {
        try {
            $this->loginLdap('visitor');
        }
        catch (\Exception $e) {
            if (
                ($e instanceof \Symfony\Component\Ldap\Exception\ConnectionException) &&
                ('Invalid credentials' == $e->getMessage())
            ) {
                die(
                    PHP_EOL
                    . PHP_EOL
                    . str_repeat('=', 60)
                    . PHP_EOL
                    . 'UnitTest Halted. Please recheck your LDAP credentials.'
                    . PHP_EOL
                    . str_repeat('=', 60)
                    . PHP_EOL
                    . PHP_EOL
                );
            }
        }
        $this->client->request('GET', '/dashboard');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $content = $this->client->getResponse()->getContent();

        $headers = $this->client->getResponse()->headers;
        $this->assertNotEmpty($headers->getCookies());

        $this->assertStringContainsString('page_dashboard', $content);

        $this->assertStringContainsString(getEnv('LDAP_LOGIN_EMAIL'), $content, true);
        $this->assertStringContainsString(getEnv('LDAP_LOGIN_USERNAME'), $content, true);

        $this->loggoutLdap();
        $this->client->request('GET', '/dashboard');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @testdox Checking Access on Dashboard On Maintenance
     */
    public function testDashboardControllerAuthenticatedOnMaintenance()
    {
        // soft - to maintenance while not on Maintenance Mode
        $this->client->followRedirects(false);
        $this->client->request('GET', '/maintenance');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        // Maintenance Mode ON
        $this->mntnncSetMaintenanceModeOn();

        // USER - login as Visitor
        $this->loginLdap('visitor');

        // soft redirect - with Visitor user
        $this->client->followRedirects(false);
        $this->client->request('GET', '/dashboard');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        $this->loggoutLdap();

        // hard redirect - half
        $this->client->followRedirects(false);
        $this->client->request('GET', '/dashboard');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        // hard redirect - complete
        $this->client->followRedirects(true);
        $this->client->request('GET', '/dashboard');
        $this->assertEquals(503, $this->client->getResponse()->getStatusCode());

        // soft redirect - half - soft without user
        $this->client->followRedirects(false);
        $this->client->request('GET', '/login');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        // soft redirect - complete - soft without user
        $this->client->followRedirects(true);
        $this->client->request('GET', '/login');
        $this->assertEquals(503, $this->client->getResponse()->getStatusCode());

        // on maintenance page
        $this->client->followRedirects(true);
        $this->client->request('GET', '/maintenance');
        $this->assertEquals(503, $this->client->getResponse()->getStatusCode());

        // on non login to for admin knocking
        $this->client->followRedirects(true);
        $this->client->request('GET', '/login?admin');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        // USER - login as admin - during maintenance page
        $this->loginLdap('admin');

        // loading page during maintenance
        $this->client->followRedirects(true);
        $this->client->request('GET', '/dashboard');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        // Maintenance Mode OFF
        $this->mntnncSetMaintenanceModeOff();

        // complete - to maintenance while not on Maintenance Mode
        $this->client->followRedirects(true);
        $this->client->request('GET', '/maintenance');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        // complete - move logged in user back
        $this->client->followRedirects(false);
        $this->client->request('GET', '/login');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        // abstract route logout test
        $this->client->followRedirects(true);
        $this->client->request('GET', '/logout');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->client->followRedirects(false);
        $this->client->request('GET', '/dashboard');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @testdox Checking Authentication on Admin Pages
     */
    public function testAdminPagesControllerCheck()
    {
        $this->loginLdap('visitor');
        $this->client->followRedirects(true);
        $this->client->request('GET', '/admin/manage-roles');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        $this->loginLdap('editor');
        $this->client->followRedirects(true);
        $this->client->request('GET', '/admin/manage-roles');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        $this->loginLdap('admin');
        $this->client->followRedirects(true);
        $this->client->request('GET', '/admin/manage-roles');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @testdox Checking Lock Screen Cookie Encryption if failure
     */
    public function testLoginCookieFailure()
    {
        /**
         * Set empty cookie for lock screen
         */
        $this->client->restart();
        $this->client->followRedirects(true);

        $this->client->getCookieJar()->set(
            \Symfony\Component\BrowserKit\Cookie::fromString(LoginCookieService::$cookieName . '=')
        );

        $crawler = $this->client->request('GET', '/login');
        $this->validateNormalLoginScreenForm($crawler);

        /**
         * Set invalid "encryption" cookie for lock screen
         */
        $this->client->restart();
        $this->client->followRedirects(true);

        $this->client->getCookieJar()->set(
            \Symfony\Component\BrowserKit\Cookie::fromString(LoginCookieService::$cookieName . '=' . md5('ABCDEFGHI12345' . rand(100, 999)))
        );

        $crawler = $this->client->request('GET', '/login');
        $this->validateNormalLoginScreenForm($crawler);

        /**
         * set incomplete cookie for lock screen
         */
        $this->client->restart();
        $this->client->followRedirects(true);

        $crypto = $this->client->getContainer()->get('test.' . \App\Utility\Crypto::class);
        $encrypted = $crypto->encrypt(json_encode([
            'login' => [
                'name'  => 'example-username',
                // 'email' => '',
            ],
        ]));

        // manually, so need to wrapped it
        $encrypted = '"' . urlencode($encrypted) . '"';

        $this->client->getCookieJar()->set(
            \Symfony\Component\BrowserKit\Cookie::fromString(LoginCookieService::$cookieName . '=' . $encrypted)
        );

        $crawler = $this->client->request('GET', '/login');
        $this->validateNormalLoginScreenForm($crawler);
    }

    /**
     * @testdox Checking Authenticated user Lock Screen
     */
    public function testAuthenticatedUserLockScreen()
    {
        if (
            (empty(getenv('LDAP_LOGIN_EMAIL'))) ||
            (empty(getenv('LDAP_LOGIN_PASSWORD')))
        ) {
            $this->markTestSkipped('Skipped as Login LDAP Login given.');
            return;
        }

        /**
         * Basic Login check
         */
        $this->client->followRedirects(true);

        $loginTextFields = 'body .login-container input.form-control';

        // normal login page - without cookie
        $crawler = $this->client->request('GET', '/login');
        $this->validateNormalLoginScreenForm($crawler);

        // login
        $this->loginLdap('admin');

        // route-logout - but dont clean out cookie
        // $this->loggoutLdap();
        $crawler = $this->client->request('GET', '/logout');

        /**
         * Lock-Screen Check
         */
        // cookie name: drugdb-settings
        $this->assertNotNull( $this->client->getCookieJar()->get(LoginCookieService::$cookieName) );
        $this->assertNotEmpty( $this->client->getCookieJar()->get(LoginCookieService::$cookieName)->getValue() );

        // should see the lock screen
        $this->assertEquals(1, $crawler->filter('body.page-login.lockscreen')->count());

        $this->assertStringContainsStringIgnoringCase('You have logged out', $crawler->filter('body .login-container')->text());
        $this->assertStringContainsStringIgnoringCase('Welcome back', $crawler->filter('body .login-container')->text());
        $this->assertStringContainsStringIgnoringCase('Change user', $crawler->filter('body .login-container')->text());

        $this->assertEquals(2, $crawler->filter($loginTextFields)->count());

        $this->assertSame('_password', $crawler->filter($loginTextFields)->getNode(0)->getAttribute('name'));
        $this->assertSame('password', $crawler->filter($loginTextFields)->getNode(0)->getAttribute('type'));

        // cookie unpack successfully
        $this->assertSame('_email', $crawler->filter($loginTextFields)->getNode(1)->getAttribute('name'));
        $this->assertSame('hidden', $crawler->filter($loginTextFields)->getNode(1)->getAttribute('type'));
        $this->assertSame(getEnv('LDAP_LOGIN_EMAIL'), $crawler->filter($loginTextFields)->getNode(1)->getAttribute('value'));

        // get login form
        $form = $crawler->selectButton('Sign in')->form();
        $form['_password'] = getEnv('LDAP_LOGIN_PASSWORD');

        // submit form
        $crawler = $this->client->submit($form);

        // should be on dashboard
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $crawler->filter('body.page_dashboard')->count());

        /**
         * Theme selector Check
         */
        // set template cookie for theme
        $this->client->getCookieJar()->set(
            \Symfony\Component\BrowserKit\Cookie::fromString('drugdb-theme=dark-theme')
        );

        // logout again, then re-login again (same as above, skip checking)
        $crawler = $this->client->request('GET', '/logout');
        $this->assertEquals(1, $crawler->filter('body.page-login.lockscreen.dark-theme')->count());
        $form = $crawler->selectButton('Sign in')->form();
        $form['_password'] = getEnv('LDAP_LOGIN_PASSWORD');
        $crawler = $this->client->submit($form);

        // should be on dashboard with dark theme
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $crawler->filter('body.page_dashboard.dark-theme')->count());

        // clear all
        $this->loggoutLdap();
    }

    /**
     * Shared method to check if basic Login screen shown
     *
     * @param Crawler $crawler
     */
    private function validateNormalLoginScreenForm(\Symfony\Component\DomCrawler\Crawler $crawler): void
    {
        $loginTextFields = 'body .login-container input.form-control';

        $this->assertEquals(1, $crawler->filter('body.page-login')->count());
        $this->assertEquals(2, $crawler->filter($loginTextFields)->count());
        $this->assertSame('_email', $crawler->filter($loginTextFields)->getNode(0)->getAttribute('name'));
        $this->assertSame('email', $crawler->filter($loginTextFields)->getNode(0)->getAttribute('type'));
        $this->assertSame('_password', $crawler->filter($loginTextFields)->getNode(1)->getAttribute('name'));
        $this->assertSame('password', $crawler->filter($loginTextFields)->getNode(1)->getAttribute('type'));
    }
}
