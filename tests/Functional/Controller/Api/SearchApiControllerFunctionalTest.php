<?php

namespace App\Tests\Functional\Controller\Api;

use App\Exception\ApiIncompleteException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @testdox FUNCTIONAL | Controller | Search API
 */
class SearchApiControllerFunctionalTest extends ApiControllerFunctionalTest
{
    /**
     * @testdox Check Search Page Failure
     */
    public function testSearchPageFailue()
    {
        $this->authenticateApiClient();
        $url = $this->getDrugUrl();

        $this->client->request('POST', $url, array());

        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase(ApiIncompleteException::MESSAGE, $this->client->getResponse()->getContent());
    }

    /**
     * @testdox Check Search Page by Name
     */
    public function testSearchPageByName()
    {
        $this->authenticateApiClient();
        $url = $this->getDrugUrl();

        $params = array(
            'q' => '^test',
        );
        $this->client->request('GET', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertTrue(empty($response['msg']));

        $this->assertSame(10, count($response['data']));
        $this->assertEquals(10, $response['count']);
        $this->assertEquals(10, $response['search_count']);

        $this->assertTrue($response['success']);
    }

    /**
     * @testdox Check Search Page by Name with Defaulting Type
     */
    public function testSearchPageByNameWithDefaultingType()
    {
        $this->authenticateApiClient();
        $url = $this->getDrugUrl();

        $params = array(
            'q'    => '^test',
            'type' => '',
        );
        $this->client->request('GET', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertTrue(empty($response['msg']));

        $this->assertSame(10, count($response['data']));
        $this->assertEquals(10, $response['count']);
        $this->assertEquals(10, $response['search_count']);

        $this->assertTrue($response['success']);
    }

    /**
     * @testdox Check Search Page by Name with Type and Exclude
     */
    public function testSearchPageByNameWithTypeAndExclude()
    {
        $this->authenticateApiClient();
        $url = $this->getDrugUrl();

        $params = array(
            'q'        => '^test',
            'type'     => 'brand',
            'excludes' => json_encode(array(
                'brand' => array($this->ids['brand-dt']),
            )),
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertTrue(empty($response['msg']));

        $this->assertSame(4, count($response['data']));
        $this->assertEquals(4, $response['count']);
        $this->assertEquals(4, $response['search_count']);

        $this->assertTrue($response['success']);
    }

    /**
     * @testdox Check Search Auto-complete by Name with Exclude
     */
    public function testSearchAutocompleteByNameWithExclude()
    {
        $this->authenticateApiClient();
        $url = $this->getDrugUrl();

        $params = array(
            'q'        => '^test',
            'type'     => 'any',
            'excludes' => json_encode(array(
                'brand' => array($this->ids['brand-dt']),
            )),
            'auct'     => true
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);

        $this->assertSame(9, count($response));

        $aDrug = $response[rand(0, (count($response) - 1))];

        $this->assertSame(
            array('id', 'family', 'name', 'desc'),
            array_keys($aDrug)
        );
    }

    /**
     * @testdox Check Search Journal
     */
    public function testJournalSearch()
    {
        $this->authenticateApiClient();
        $url = $this->getJournalUrl();

        $params = [];
        $this->client->request('POST', $url, $params);

        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase(ApiIncompleteException::MESSAGE, $this->client->getResponse()->getContent());


        $params = [
            'q' => 'test',
        ];
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(true);

        $this->assertSame(2, count($response['data']));

        $aJournal = $response['data'][rand(0, (count($response['data']) - 1))];
        $this->assertSame(
            [
                'id',
                'name',
                'abbreviation',
                'status',
                'is_medline',
                'is_secondline',
                'is_dgabstract',
                'is_global_edition',
                'medline_issn',
                'jr1',
                'specialties',
            ],
            array_keys($aJournal)
        );

        $this->assertSame(
            [
                'Journal Test A RF346GH',
                'Journal Test B HJ345X',
            ],
            array_column($response['data'], 'name')
        );

        $this->assertSame(
            [
                'JTA 6GH',
                'JTB 45X',
            ],
            array_column($response['data'], 'abbreviation')
        );

        $this->assertSame(
            [
                'Active',
                'Archived',
            ],
            array_column($response['data'], 'status')
        );

        $this->assertSame(
            [
                true,
                false,
            ],
            array_column($response['data'], 'is_medline')
        );

        $this->assertSame(
            [
                false,
                true,
            ],
            array_column($response['data'], 'is_secondline')
        );

        $this->assertSame(
            [
                false,
                true,
            ],
            array_column($response['data'], 'is_dgabstract')
        );

        $this->assertSame(
            [
                true,
                true,
            ],
            array_column($response['data'], 'is_global_edition')
        );

        $this->assertSame(
            [
                '1234-X65A',
                '345D-X65A',
            ],
            array_column($response['data'], 'medline_issn')
        );

        $this->assertSame(
            [
                0.5,
                0.2,
            ],
            array_column($response['data'], 'jr1')
        );

        $this->assertSame(
            [
                [
                    'Immunology',
                    'Urology',
                ],
                [],
            ],
            array_column($response['data'], 'specialties')
        );
    }

    /**
     * @testdox Check Search Conference
     */
    public function testConferenceSearch()
    {
        $this->authenticateApiClient();
        $url = $this->getConferenceUrl();

        $params = [];
        $this->client->request('POST', $url, $params);

        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase(ApiIncompleteException::MESSAGE, $this->client->getResponse()->getContent());


        $params = [
            'q' => 'test',
        ];
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(true);

        $this->assertSame(4, count($response['data']));
        $aConference = $response['data'][rand(0, (count($response['data']) - 1))];
        $this->assertSame(
            [
                'id',
                'name',
                'event_span',
                'locality',
                'is_cruise',
                'is_online',
                'is_key_conference',
                'has_guide',
                'is_discontinued',
                'specialties',
            ],
            array_keys($aConference)
        );

        $this->assertSame(
            [
                'Conference C Test DF345F',
                'Conference D Test KF938S',
                'Conference A Test S69RTU',
                'Conference B Test RD996G',
            ],
            array_column($response['data'], 'name')
        );

        $this->assertSame(
            [
                'Between Monday, 10th and Tuesday, 18th Aug 2020',
                'On Tuesday, 3rd Mar 2020',
                'Between Tuesday, 19th Feb and Thursday, 30th May 2019',
                'Between Monday, 19th Mar 2018 and Wednesday, 20th Mar 2019',
            ],
            array_column($response['data'], 'event_span')
        );

        $this->assertSame(
            [
                'Voronezh, Russia, Northern Europe',
                'South Gate Central, Singapore, West Africa',
                'Petaling Jaya, Malaysia, Southeast Asia',
                'Tondo, Philippines, Southeast Asia',
            ],
            array_column($response['data'], 'locality')
        );

        $this->assertSame(
            [
                false,
                true,
                false,
                false,
            ],
            array_column($response['data'], 'is_cruise')
        );

        $this->assertSame(
            [
                true,
                false,
                false,
                false,
            ],
            array_column($response['data'], 'is_online')
        );

        $this->assertSame(
            [
                true,
                true,
                true,
                false,
            ],
            array_column($response['data'], 'is_key_conference')
        );

        $this->assertSame(
            [
                true,
                true,
                true,
                false,
            ],
            array_column($response['data'], 'has_guide')
        );

        $this->assertSame(
            [
                false,
                false,
                false,
                false,
            ],
            array_column($response['data'], 'is_discontinued')
        );

        $this->assertSame(
            [
                [
                    'Haematology and Oncology',
                    'Psychiatry',
                    'Transplant Surgery',
                ],
                [],
                [
                    'Nephrology',
                    'Urology',
                ],
                [],
            ],
            array_column($response['data'], 'specialties')
        );
    }

    private function getDrugUrl()
    {
        static $searchApiUrl;

        if (is_null($searchApiUrl)) {
            $searchApiUrl = $this->router->generate('api_search_by_name');
            $this->assertSame('/v1/search-by-name', $searchApiUrl);
        }

        return $searchApiUrl;
    }

    private function getJournalUrl()
    {
        static $searchApiUrl;

        if (is_null($searchApiUrl)) {
            $searchApiUrl = $this->router->generate('api_journal_search_by_name');
            $this->assertSame('/v1/search-journal-by-name', $searchApiUrl);
        }

        return $searchApiUrl;
    }

    private function getConferenceUrl()
    {
        static $searchApiUrl;

        if (is_null($searchApiUrl)) {
            $searchApiUrl = $this->router->generate('api_conference_search_by_name');
            $this->assertSame('/v1/search-conference-by-name', $searchApiUrl);
        }

        return $searchApiUrl;
    }
}
