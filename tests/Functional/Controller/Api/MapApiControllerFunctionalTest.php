<?php

namespace App\Tests\Functional\Controller\Api;

use App\Exception\ApiIncompleteException;

/**
 * @testdox FUNCTIONAL | Controller | Map API
 */
class MapApiControllerFunctionalTest extends ApiControllerFunctionalTest
{
    /**
     * @testdox Check Get Drug On Map Invalid Request
     */
    public function testDrugPostsOnMapInvalids()
    {
        $routes = [
            'api_get_map_drug' => [
                'uri'   => '/v1/get-map-drug',
                'check' => ['type', 'id'],
            ],
            'api_add_new_map_drug' => [
                'uri'   => '/v1/add-new-map-drug',
                'check' => ['type', 'name', 'valid'],
            ],
        ];

        $this->authenticateApiClient();

        $this->validateGeneralInvalidPostRequests($routes);
    }

    /**
     * @testdox Check Get Drug On Map
     */
    public function testGetDrugOnMap()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_map_drug');
        $this->assertSame('/v1/get-map-drug', $url);

        $params = array(
            'type' => 'experimental',
            'id'   => $this->ids['experimental'],
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertTrue(empty($response['msg']));
        $this->assertFalse(empty($response['data']));
        $this->assertTrue($response['success']);

        $this->assertArrayHasKey('operators', $response['data']);
        $this->assertSame(2, count($response['data']['operators']));

        $this->assertArrayHasKey('links', $response['data']);
        $this->assertSame(1, count($response['data']['links']));

        return $response['data'];
    }

    /**
     * @testdox Check Add Drug On Map
     */
    public function testAddDrugOnMap()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_add_new_map_drug');
        $this->assertSame('/v1/add-new-map-drug', $url);

        $params = array(
            'type'                => 'experimental',
            'name'                => 'api-create-map-drug-on-the-fly',
            'mapped_experimental' => $this->ids['experimental'],
        );
        $this->client->request('POST', $url, $params);
        $response = $this->responseConvertAndCheck();

        $this->assertStringContainsStringIgnoringCase("Invalid Request, Missing Argument: 'valid'", $response['msg']);
        $this->assertFalse($response['success']);

        $params = array(
            'type'                => 'experimental',
            'name'                => 'api-create-map-drug-on-the-fly',
            'valid'               => 'false',
            'mapped_experimental' => $this->ids['experimental'],
        );
        $this->client->request('POST', $url, $params);
        $response = $this->responseConvertAndCheck();

        $this->assertTrue(empty($response['msg']));
        $this->assertFalse(empty($response['data']));
        $this->assertTrue($response['success']);

        $this->assertArrayHasKey('operators', $response['data']);
        $this->assertSame(1, count($response['data']['operators']));

        $this->assertArrayHasKey('links', $response['data']);
        $this->assertSame(0, count($response['data']['links']));
    }

    /**
     * @depends testGetDrugOnMap
     *
     * @testdox Check Update Drug Relation On Map
     */
    public function testUpdateDrugRelationsOnMap($params)
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_update_drug_relations');
        $this->assertSame('/v1/map-update-drug-relations', $url);

        $params['operators'] = json_encode($params['operators']);
        $params['links']     = json_encode(array());

        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertSame('complete', $response['msg']);

        $this->assertArrayHasKey('changes-count', $response['data']);
        $this->assertSame(1, $response['data']['changes-count']);

        $this->assertSame(1, $response['count']);
        $this->assertTrue($response['success']);
    }


    /**
     * @testdox Check Update Drug Relation Failure
     */
    public function testUpdateDrugRelationsOnMapFailure()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_update_drug_relations');
        $this->assertSame('/v1/map-update-drug-relations', $url);

        $this->client->request('POST', $url, array());
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsStringIgnoringCase(ApiIncompleteException::MESSAGE, $this->client->getResponse()->getContent());
    }
}
