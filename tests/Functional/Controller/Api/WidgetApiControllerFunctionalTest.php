<?php

namespace App\Tests\Functional\Controller\Api;

/**
 * @testdox FUNCTIONAL | Controller | Widget API
 */
class WidgetApiControllerFunctionalTest extends ApiControllerFunctionalTest
{
    /**
     * @testdox Check Widget Update
     */
    public function testWidgetUpdate()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_update_widgets');
        $this->assertSame('/v1/widgets-update', $url);
        $this->client->request('GET', $url);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertStringContainsString('processing', strtolower($response['msg']));
        $this->assertSame(0, $response['count']);
        $this->assertTrue($response['success']);
    }
}
