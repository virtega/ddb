<?php

namespace App\Tests\Functional\Controller\Api;

/**
 * @testdox FUNCTIONAL | Controller | Drug API
 */
class DrugApiControllerFunctionalTest extends ApiControllerFunctionalTest
{
    /**
     * @testdox Check Get A Drug Set for Invalid Request
     */
    public function testGetADrugSetInvalids()
    {
        $routes = [
            'api_get_drug' => [
                'uri'   => '/v1/get-drug',
                'check' => ['type', 'id'],
            ],
            'api_get_drug_family' => [
                'uri'   => '/v1/get-drug-family',
                'check' => ['type', 'id'],
            ],
            'api_get_drug_entire_family' => [
                'uri'   => '/v1/get-drug-entire-family',
                'check' => ['type', 'id'],
            ],
            'api_add_new_drug' => [
                'uri'   => '/v1/add-new-drug',
                'check' => ['type', 'name', 'valid'],
            ],
            'api_delete_drug' => [
                'uri'   => '/v1/remove-drug',
                'check' => ['type', 'id'],
            ],
        ];

        $this->authenticateApiClient();

        $this->validateGeneralInvalidPostRequests($routes);
    }

    /**
     * @testdox Check Get A Drug
     */
    public function testGetADrug()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_drug');
        $this->assertSame('/v1/get-drug', $url);

        $params = array(
            'type' => 'experimental',
            'id'   => $this->ids['experimental'],
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertArrayNotHasKey('main', $response['data']);
        $this->assertSame($params['id'], $response['data']['id']);
        $this->assertSame('test-1', $response['data']['name']);
        $this->assertSame('ABC123', $response['data']['uid']);
        $this->assertSame('test-1-ABC123-Comment', $response['data']['comments']);

        $this->assertSame(1, $response['count']);
        $this->assertTrue($response['success']);
    }

    /**
     * @testdox Check Get A Drug Family
     */
    public function testGetADrugFamily()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_drug_family');
        $this->assertSame('/v1/get-drug-family', $url);

        $params = [
            'type' => 'experimental',
            'id'   => $this->ids['experimental'],
        ];
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        // note additional level "main"
        $this->assertArrayHasKey('main', $response['data']);
        $this->assertSame($params['id'], $response['data']['main']['id']);
        $this->assertSame('test-1', $response['data']['main']['name']);
        $this->assertSame('ABC123', $response['data']['main']['uid']);
        $this->assertSame('test-1-ABC123-Comment', $response['data']['main']['comments']);

        $this->assertSame(1, $response['count']);
        $this->assertTrue($response['success']);
    }

    /**
     * @testdox Check Get A Drug Entire Family
     */
    public function testGetADrugEntireFamily()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_drug_entire_family');
        $this->assertSame('/v1/get-drug-entire-family', $url);

        // check: unknown drug id
        $params = [
            'type'  => 'experimental',
            'id'    => 999999999, // impossibly high
        ];
        $this->client->request('POST', $url, $params);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Unknown experimental Drug #999999999', $response['msg']);

        // check: normal
        $params = [
            'type' => 'experimental',
            'id'   => $this->ids['experimental'],
        ];
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertArrayHasKey('experimental', $response['data']);
        $this->assertArrayHasKey('generic', $response['data']);
        $this->assertArrayHasKey('brand', $response['data']);

        // note additional level by drug-type, index then "main"
        $this->assertNotEmpty($response['data']['experimental']);
        $this->assertSame($params['id'], $response['data']['experimental'][0]['main']['id']);
        $this->assertSame('test-1', $response['data']['experimental'][0]['main']['name']);
        $this->assertSame('ABC123', $response['data']['experimental'][0]['main']['uid']);
        $this->assertSame('test-1-ABC123-Comment', $response['data']['experimental'][0]['main']['comments']);

        // expected has have down-link from fixture
        $this->assertNotEmpty($response['data']['generic']);
        $this->assertSame('test-uplink', $response['data']['generic'][0]['main']['name']);
        $this->assertSame('UPLINK', $response['data']['generic'][0]['main']['uid']);
        $this->assertSame('test-UPLINK-Comment', $response['data']['generic'][0]['main']['comments']);

        // with deep relation - back to experimental above
        $this->assertNotEmpty($response['data']['generic'][0]['main']['experimental']);
        $this->assertSame($params['id'], $response['data']['generic'][0]['main']['experimental']['id']);
        $this->assertSame('test-1', $response['data']['generic'][0]['main']['experimental']['name']);
        $this->assertSame('ABC123', $response['data']['generic'][0]['main']['experimental']['uid']);
        $this->assertSame('test-1-ABC123-Comment', $response['data']['generic'][0]['main']['experimental']['comments']);

        $this->assertEmpty($response['data']['brand']);

        $this->assertSame(2, $response['count']);
        $this->assertTrue($response['success']);
    }

    /**
     * @testdox Check Create A Drug
     */
    public function testCreateADrug()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_add_new_drug');
        $this->assertSame('/v1/add-new-drug', $url);

        $params = array(
            'type'  => 'generic',
            'name'  => 'api-create-drug-on-the-fly',
            'valid' => 1,
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertFalse(empty($response['data']['id']));
        $this->assertSame($params['name'], $response['data']['name']);
        $this->assertSame($params['valid'], $response['data']['valid']);

        $this->assertSame(1, $response['count']);
        $this->assertTrue($response['success']);

        return array(
            'type' => $params['type'],
            'id'   => $response['data']['id']
        );
    }

    /**
     * @depends testCreateADrug
     *
     * @testdox Check Delete A Drug
     */
    public function testDeleteADrug($params)
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_delete_drug');
        $this->assertSame('/v1/remove-drug', $url);

        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertSame('1 Record Removed, meanwhile 0 detached.', $response['msg']);
        $this->assertTrue(empty($response['data']));
        $this->assertGreaterThanOrEqual(1, $response['count']);
        $this->assertTrue($response['success']);
    }
}
