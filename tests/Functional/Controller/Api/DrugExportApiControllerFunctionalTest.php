<?php

namespace App\Tests\Functional\Controller\Api;

use App\Tests\Functional\BaseFunctionalTest;

/**
 * @testdox FUNCTIONAL | Controller | Drug Export API
 */
class DrugExportApiControllerFunctionalTest extends ApiControllerFunctionalTest
{
    /**
     * @testdox Check Export by Name Search Invalid Request
     */
    public function testByNameSearchInvalidRequest()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_export_name_search');
        $this->assertSame('/v1/export-name-search', $url);

        $params = [];
        $this->client->request('POST', $url, $params);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'q\'', $response['msg']);
    }

    /**
     * @testdox Check Export by Name Search Less Download
     */
    public function testByNameSearchLessDownload()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_export_name_search');
        $this->assertSame('/v1/export-name-search', $url);

        $params = array(
            'q'    => '^test-',
            'type' => 'any',
            'less' => 1,
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        $this->assertSame('text/csv', $this->client->getResponse()->headers->get('content-type'));
        $this->assertStringContainsString(
            'attachment; filename=',
            $this->client->getResponse()->headers->get('content-disposition')
        );
        $this->assertStringContainsString(
            \App\Service\CoreService::BRANDING . '-search-export-simple-' . date('Y-m-d') . '-',
            $this->client->getResponse()->headers->get('content-disposition')
        );

        $this->assertSame('file', $this->client->getResponse()->getFile()->getType());
        $this->assertSame('text/plain', $this->client->getResponse()->getFile()->getMimeType());
        $this->assertStringContainsString(
            \App\Service\CoreService::BRANDING . '-search-export-' . date('Y-m-d') . '-',
            $this->client->getResponse()->getFile()->getBasename()
        );

        $this->assertSame('csv', $this->client->getResponse()->getFile()->getExtension());
    }

    /**
     * @testdox Check Export by Name Search Complete Download
     */
    public function testByNameSearchCompleteDownload()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_export_name_search');
        $this->assertSame('/v1/export-name-search', $url);

        $params = array(
            'q'    => '^test-',
            'type' => 'experimental',
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        $this->assertSame('text/csv', $this->client->getResponse()->headers->get('content-type'));
        $this->assertStringContainsString(
            'attachment; filename=',
            $this->client->getResponse()->headers->get('content-disposition')
        );
        $this->assertStringContainsString(
            \App\Service\CoreService::BRANDING . '-search-export-complete-' . date('Y-m-d') . '-',
            $this->client->getResponse()->headers->get('content-disposition')
        );

        $this->assertSame('file', $this->client->getResponse()->getFile()->getType());
        $this->assertSame('text/plain', $this->client->getResponse()->getFile()->getMimeType());
        $this->assertStringContainsString(
            \App\Service\CoreService::BRANDING . '-search-export-' . date('Y-m-d') . '-',
            $this->client->getResponse()->getFile()->getBasename()
        );

        $this->assertSame('csv', $this->client->getResponse()->getFile()->getExtension());
    }

    /**
     * @testdox Check Export by Name Search Email
     */
    public function testByNameSearchEmail()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_export_name_search');
        $this->assertSame('/v1/export-name-search', $url);

        $params = array(
            'q'     => '^test-',
            'type'  => 'any',
            'less'  => 1,
            'email' => BaseFunctionalTest::getTestEmail()
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertStringContainsString('processing', strtolower($response['msg']));
        $this->assertSame(0, $response['count']);
        $this->assertTrue($response['success']);
    }

    /**
     * @testdox Check Export by Id Search Less Download
     */
    public function testByIdSearchLessDownload()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_export_id_search');
        $this->assertSame('/v1/export-id-search', $url);

        $params = array(
            'experimental' => array(
                $this->ids['experimental'],
                $this->ids['experimental-gn'],
                $this->ids['experimental-dt'],
            ),
            'type'         => 'experimental',
            'less'         => 1,
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        $this->assertSame('text/csv', $this->client->getResponse()->headers->get('content-type'));
        $this->assertStringContainsString(
            'attachment; filename',
            $this->client->getResponse()->headers->get('content-disposition')
        );
        $this->assertStringContainsString(
            \App\Service\CoreService::BRANDING . '-map-export-simple-' . date('Y-m-d') . '-',
            $this->client->getResponse()->headers->get('content-disposition')
        );

        $this->assertSame('file', $this->client->getResponse()->getFile()->getType());
        $this->assertSame('text/plain', $this->client->getResponse()->getFile()->getMimeType());
        $this->assertStringContainsString(
            \App\Service\CoreService::BRANDING . '-map-export-' . date('Y-m-d') . '-',
            $this->client->getResponse()->getFile()->getBasename()
        );

        $this->assertSame('csv', $this->client->getResponse()->getFile()->getExtension());
    }

    /**
     * @testdox Check Export by Id Search Complete Download
     */
    public function testByIdSearchCompleteDownload()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_export_id_search');
        $this->assertSame('/v1/export-id-search', $url);

        $params = array(
            'generic' => implode(',', array(
                $this->ids['generic'],
                $this->ids['generic-dt'],
            )),
            'type'    => 'generic',
        );
        $this->client->request('POST', $url, $params);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        $this->assertSame('text/csv', $this->client->getResponse()->headers->get('content-type'));
        $this->assertStringContainsString(
            'attachment; filename=',
            $this->client->getResponse()->headers->get('content-disposition')
        );
        $this->assertStringContainsString(
            \App\Service\CoreService::BRANDING . '-map-export-complete-' . date('Y-m-d') . '-',
            $this->client->getResponse()->headers->get('content-disposition')
        );

        $this->assertSame('file', $this->client->getResponse()->getFile()->getType());
        $this->assertSame('text/plain', $this->client->getResponse()->getFile()->getMimeType());
        $this->assertStringContainsString(
            \App\Service\CoreService::BRANDING . '-map-export-' . date('Y-m-d') . '-',
            $this->client->getResponse()->getFile()->getBasename()
        );

        $this->assertSame('csv', $this->client->getResponse()->getFile()->getExtension());
    }
}
