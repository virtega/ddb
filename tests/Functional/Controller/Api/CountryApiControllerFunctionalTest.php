<?php

namespace App\Tests\Functional\Controller\Api;

/**
 * @testdox FUNCTIONAL | Controller | Country API
 */
class CountryApiControllerFunctionalTest extends ApiControllerFunctionalTest
{
    /**
     * @testdox Check Country List
     */
    public function testGetCountryList()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_country_list');
        $this->assertSame('/v1/get-country-list', $url);
        $this->client->request('GET', $url);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertSame('complete', $response['msg']);
        $this->assertSame(253, $response['count']);
        $this->assertTrue($response['success']);
        $this->assertSame(253, count($response['data']));

        $rand = rand(0, 251);
        $this->assertArrayHasKey('isoCode', $response['data'][$rand]);
        $this->assertArrayHasKey('name', $response['data'][$rand]);
        $this->assertArrayHasKey('capitalCity', $response['data'][$rand]);
    }

    /**
     * @testdox Check Country Grouped List
     */
    public function testGetCountryGroupList()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_country_group_list');
        $this->assertSame('/v1/get-country-group-list', $url);
        $this->client->request('GET', $url);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertSame('complete', $response['msg']);
        $this->assertSame(3, $response['count']);
        $this->assertTrue($response['success']);
        $this->assertSame(3, count($response['data']));

        $this->assertArrayHasKey('EU5', $response['data']);
        $this->assertArrayHasKey('US', $response['data']);
        $this->assertArrayHasKey('Others', $response['data']);

        $this->assertEquals(5, count($response['data']['EU5']));
        $ext = array_column($response['data']['EU5'], 'iso_code');
        sort($ext);
        $this->assertSame(['DE', 'ES', 'FR', 'GB', 'IT'], $ext);

        $this->assertGreaterThanOrEqual(1, count($response['data']['US']));
        $this->assertSame(['US'], array_column($response['data']['US'], 'iso_code'));

        $this->assertGreaterThan(1, count($response['data']['Others']));
        $this->assertContains('AU', array_column($response['data']['Others'], 'iso_code'));
    }

    /**
     * @testdox Check Complete Countries-Regions List
     */
    public function testGetCompleteCountriesRegionsList()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_countries_regions');
        $this->assertSame('/v1/get-countries-regions', $url);
        $this->client->request('GET', $url);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertSame('complete', $response['msg']);
        $this->assertSame(42, $response['count']);
        $this->assertTrue($response['success']);
        $this->assertSame(42, count($response['data']));

        $rand = rand(0, 41);
        $inner = count($response['data'][$rand]);
        $inner = rand(0, ($inner - 1));
        $this->assertSame(2, count($response['data'][$rand][$inner]));
        $this->assertArrayHasKey('id', $response['data'][$rand][$inner]);
        $this->assertArrayHasKey('name', $response['data'][$rand][$inner]);

        $count = array_map('count', $response['data']);
        $count = array_sum($count);
        $this->assertSame(147, $count);
    }

    /**
     * @testdox Check A Country Details
     */
    public function testGetACountryDetails()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_a_country_details');
        $this->assertSame('/v1/get-a-country-details', $url);

        $this->client->request('POST', $url);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'country_iso\'', $response['msg']);

        $this->client->request('POST', $url, ['country_iso' => 'zz']);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Unknown Country with Code: \'zz\'', $response['msg']);

        $this->client->request('POST', $url, ['country_iso' => 'fr']);
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();
        $this->assertSame('complete', $response['msg']);
        $this->assertSame(2, $response['count']);
        $this->assertTrue($response['success']);
        $this->assertSame(2, count($response['data']));

        $this->assertArrayHasKey('country', $response['data']);
        $this->assertSame(3, count($response['data']['country']));
        $this->assertSame([
            'iso_code'     => 'FR',
            'name'         => 'France',
            'capital_city' => 'Paris',
        ], $response['data']['country']);

        $this->assertArrayHasKey('regions', $response['data']);
        $this->assertSame(2, count($response['data']['regions']));
        $this->assertSame([
            [
                [
                    'id'   => 2,
                    'name' => 'Euroasia',
                ],
                [
                    'id'   => 250,
                    'name' => 'Europe',
                ],
                [
                    'id'   => 254,
                    'name' => 'Western Europe',
                ],
            ],
            [
                [
                    'id'   => 2,
                    'name' => 'Euroasia',
                ],
                [
                    'id'   => 250,
                    'name' => 'Europe',
                ],
                [
                    'id'   => 5000,
                    'name' => 'EU5',
                ],
            ],
        ], $response['data']['regions']);
    }

    /**
     * @testdox Check A City Countries
     */
    public function testGetACityCountries()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_a_city_countries');
        $this->assertSame('/v1/get-a-city-countries', $url);

        $this->client->request('POST', $url);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'city\'', $response['msg']);

        $this->client->request('POST', $url, ['city' => 'an alien city']);
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();
        $this->assertSame('no-data', $response['msg']);
        $this->assertSame(0, $response['count']);
        $this->assertFalse($response['success']);
        $this->assertSame(0, count($response['data']));

        $this->client->request('POST', $url, ['city' => 'belgrade']);
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();
        $this->assertSame('complete', $response['msg']);
        $this->assertSame(3, $response['count']);
        $this->assertTrue($response['success']);
        $this->assertSame(3, count($response['data']));
        $this->assertSame([
            [
                'iso_code'     => 'RS',
                'name'         => 'Serbia',
                'capital_city' => 'Belgrade',
            ],
            [
                'iso_code'     => 'CS',
                'name'         => 'Serbia and Montenegro',
                'capital_city' => 'Belgrade',
            ],
            [
                'iso_code'     => 'YU',
                'name'         => 'Yugoslavia',
                'capital_city' => 'Belgrade',
            ],
        ], $response['data']);
    }

    /**
     * @testdox Check A Country Regions
     */
    public function testGetACountryRegions()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_a_country_regions');
        $this->assertSame('/v1/get-a-country-regions', $url);

        $this->client->request('POST', $url);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'country_iso\'', $response['msg']);

        $this->client->request('POST', $url, ['country_iso' => 'zz']);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Unknown Country with Code: \'zz\'', $response['msg']);

        $this->client->request('POST', $url, ['country_iso' => 'my']);
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertTrue($response['success']);
        $this->assertSame(1, count($response['data']));
        $this->assertSame(1, $response['count']);
        $this->assertSame(3, count($response['data'][0]));
        $this->assertSame([
            [
                'id'   => 2,
                'name' => 'Euroasia',
            ],
            [
                'id'   => 200,
                'name' => 'Asia',
            ],
            [
                'id'   => 203,
                'name' => 'Southeast Asia',
            ],
        ], $response['data'][0]);
    }

    /**
     * @testdox Check A Region Countries
     */
    public function testGetARegionCountries()
    {
        $this->authenticateApiClient();

        $url = $this->router->generate('api_get_a_region_countries');
        $this->assertSame('/v1/get-a-region-countries', $url);

        $this->client->request('POST', $url);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'region_id\'', $response['msg']);

        $this->client->request('POST', $url, ['region_id' => '99999999']);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Unknown Region with Code: \'99999999\'', $response['msg']);

        $this->client->request('POST', $url, ['region_id' => '203']);
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertTrue($response['success']);
        $this->assertSame(13, count($response['data']));
        $this->assertSame(13, $response['count']);

        $this->assertSame([
            [
              "iso_code"     => "BN",
              "name"         => "Brunei",
              "capital_city" => "Bandar Seri Begawan",
            ],
            [
              "iso_code"     => "CC",
              "name"         => "Cocos [Keeling] Islands",
              "capital_city" => "West Island",
            ],
            [
              "iso_code"     => "CX",
              "name"         => "Christmas Island",
              "capital_city" => "Flying Fish Cove",
            ],
            [
              "iso_code"     => "ID",
              "name"         => "Indonesia",
              "capital_city" => "Jakarta",
            ],
            [
              "iso_code"     => "KH",
              "name"         => "Cambodia",
              "capital_city" => "Phnom Penh",
            ],
            [
              "iso_code"     => "LA",
              "name"         => "Laos",
              "capital_city" => "Vientiane",
            ],
            [
              "iso_code"     => "MM",
              "name"         => "Myanmar [Burma]",
              "capital_city" => "Nay Pyi Taw",
            ],
            [
              "iso_code"     => "MY",
              "name"         => "Malaysia",
              "capital_city" => "Kuala Lumpur",
            ],
            [
              "iso_code"     => "PH",
              "name"         => "Philippines",
              "capital_city" => "Manila",
            ],
            [
              "iso_code"     => "SG",
              "name"         => "Singapore",
              "capital_city" => "Singapore",
            ],
            [
              "iso_code"     => "TH",
              "name"         => "Thailand",
              "capital_city" => "Bangkok",
            ],
            [
              "iso_code"     => "TL",
              "name"         => "Timor-Leste",
              "capital_city" => "Dili",
            ],
            [
              "iso_code"     => "VN",
              "name"         => "Vietnam",
              "capital_city" => "Hanoi",
            ],
        ], $response['data']);
    }
}
