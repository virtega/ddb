<?php

namespace App\Tests\Functional\Controller\Api;

use App\Entity\Brand;
use App\Entity\Experimental;
use App\Entity\Generic;
use App\Tests\Fixtures\ConferenceFixtures;
use App\Tests\Fixtures\CountryFixtures;
use App\Tests\Fixtures\Drug\BrandDrugRepoFixtures;
use App\Tests\Fixtures\Drug\ExperimentalDrugRepoFixtures;
use App\Tests\Fixtures\Drug\GenericDrugRepoFixtures;
use App\Tests\Fixtures\JournalFixtures;
use App\Tests\Fixtures\SpecialtyTaxonomyFixtures;
use App\Tests\TestTraits\LdapUserTrait;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;

/**
 * @testdox FUNCTIONAL | Controller | API
 */
abstract class ApiControllerFunctionalTest extends WebTestCase
{
    use LdapUserTrait;

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected static $entityManager;

    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    protected $client;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var array
     */
    protected $ids;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new ExperimentalDrugRepoFixtures());
        $loader->addFixture(new GenericDrugRepoFixtures());
        $loader->addFixture(new BrandDrugRepoFixtures());
        $loader->addFixture(new CountryFixtures());
        $loader->addFixture(new SpecialtyTaxonomyFixtures());
        $loader->addFixture(new ConferenceFixtures());
        $loader->addFixture(new JournalFixtures());
        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($loader->getFixtures());

        $application = new Application(static::$kernel);
        $application->setAutoExit(false);

        $fp = tmpfile();
        $input  = new StringInput('app:update-widgets');
        $output = new StreamOutput($fp);
        $application->run($input, $output);
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->purge();

        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $container    = $this->client->getContainer();

        $this->router = $container->get('router');

        $this->ids = array(
            'experimental'    => self::$entityManager->getRepository(Experimental::class)->getDrug('test-1', 'name')->getId(),
            'experimental-gn' => self::$entityManager->getRepository(Experimental::class)->getDrug('test-2', 'name')->getId(),
            'experimental-dt' => self::$entityManager->getRepository(Experimental::class)->getDrug('test-3', 'name')->getId(),
            'generic'         => self::$entityManager->getRepository(Generic::class)->getDrug('test-1', 'name')->getId(),
            'generic-dt'      => self::$entityManager->getRepository(Generic::class)->getDrug('test-3', 'name')->getId(),
            'brand'           => self::$entityManager->getRepository(Brand::class)->getDrug('test-1', 'name')->getId(),
            'brand-dt'        => self::$entityManager->getRepository(Brand::class)->getDrug('test-3', 'name')->getId(),
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->client = null;
    }

    /**
     * @testdox Check API Index
     */
    public function testIndex()
    {
        $this->authenticateApiClient('user');

        $url = $this->router->generate('api_index');
        $this->assertSame('/v1/index', $url);

        $url = $this->router->generate('api_hello');
        $this->assertSame('/v1/hello', $url);
        $this->client->request('POST', $url);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->assertFalse(empty($response['msg']));
        $this->assertEquals('complete', $response['msg']);
        $this->assertTrue(!empty($response['data']));
        $this->assertSame(2, $response['count']);
        $this->assertTrue($response['success']);

        $this->assertArrayHasKey('user',        $response['data']);
        $this->assertArrayHasKey('roles',       $response['data']['user']);
        $this->assertArrayHasKey('username',    $response['data']['user']);
        $this->assertArrayHasKey('email',       $response['data']['user']);
        $this->assertArrayHasKey('displayName', $response['data']['user']);
    }

    protected function authenticateApiClient($role = 'editor', $heading_key = 'HTTP_X-API-TOKEN', ?string $username = null, ?string $password = null)
    {
        $this->adjustUserRoles($role);
        $this->client->setServerParameter($heading_key, getenv('SECURITY_API_TOKEN'));
        $this->client->setServerParameter('PHP_AUTH_USER', ($username ?? getenv('LDAP_LOGIN_USERNAME')));
        $this->client->setServerParameter('PHP_AUTH_PW',   ($password ?? getenv('LDAP_LOGIN_PASSWORD')));
    }

    /**
     * Shared method to check and unpack response
     *
     * @param bool $doGeneralCheck Meant for success Response structural check
     *
     * @return array Unpack JOSN
     */
    protected function responseConvertAndCheck(bool $doGeneralCheck = true)
    {
        $response = $this->client->getResponse()->getContent();
        $response = json_decode($response, true);

        $this->assertTrue(is_array($response));
        $this->assertFalse(empty($response));

        if ($doGeneralCheck) {
            $this->assertArrayHasKey('msg', $response);
            $this->assertArrayHasKey('data', $response);
            $this->assertArrayHasKey('count', $response);
            $this->assertArrayHasKey('success', $response);
            $this->assertArrayHasKey('drugdb', $response);
            $this->assertSame(2, count($response['drugdb']));
            $this->assertArrayHasKey('version', $response['drugdb']);
            $this->assertArrayHasKey('date', $response['drugdb']);
        }

        return $response;
    }

    /**
     * Shared method to check Invalids responses with given basic request structure.
     *
     * @param array $routes
     *
     * @return void
     */
    protected function validateGeneralInvalidPostRequests(array $routes): void
    {
        foreach ($routes as $route => $set) {
            $url = $this->router->generate($route);
            $this->assertSame($set['uri'], $url);

            if (in_array('type', $set['check'])) {
                $params = [];
                $this->client->request('POST', $url, $params);
                $this->assertSame(400, $this->client->getResponse()->getStatusCode());
                $response = $this->responseConvertAndCheck(false);
                $this->assertSame(false, $response['success']);
                $this->assertSame(0, $response['count']);
                $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'type\'', $response['msg']);
            }

            if (in_array('id', $set['check'])) {
                $params = [
                    'type' => 'brand',
                ];
                $this->client->request('POST', $url, $params);
                $this->assertSame(400, $this->client->getResponse()->getStatusCode());
                $response = $this->responseConvertAndCheck(false);
                $this->assertSame(false, $response['success']);
                $this->assertSame(0, $response['count']);
                $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'id\'', $response['msg']);
            }

            if (in_array('name', $set['check'])) {
                $params = [
                    'type' => 'brand',
                ];
                $this->client->request('POST', $url, $params);
                $this->assertSame(400, $this->client->getResponse()->getStatusCode());
                $response = $this->responseConvertAndCheck(false);
                $this->assertSame(false, $response['success']);
                $this->assertSame(0, $response['count']);
                $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'name\'', $response['msg']);
            }

            if (in_array('valid', $set['check'])) {
                $params = [
                    'type' => 'brand',
                    'name' => 'testing-invalid',
                ];
                $this->client->request('POST', $url, $params);
                $this->assertSame(400, $this->client->getResponse()->getStatusCode());
                $response = $this->responseConvertAndCheck(false);
                $this->assertSame(false, $response['success']);
                $this->assertSame(0, $response['count']);
                $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'valid\'', $response['msg']);
            }

            if (in_array('type', $set['check'])) {
                $params = array(
                    'type'  => 'alien-type',
                    'id'    => $this->ids['experimental'],
                    'name'  => 'testing-invalid',
                    'valid' => 0,
                );
                $this->client->request('POST', $url, $params);
                $this->assertSame(400, $this->client->getResponse()->getStatusCode());
                $response = $this->responseConvertAndCheck(false);
                $this->assertSame(false, $response['success']);
                $this->assertSame(0, $response['count']);
                $this->assertStringContainsStringIgnoringCase('Unknown Drug Type', $response['msg']);
            }
        }
    }
}
