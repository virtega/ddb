<?php

namespace App\Tests\Functional\Controller\Api;

/**
 * This test is depending on 3rd party service, results may change.
 *
 * @testdox FUNCTIONAL | Controller | NCBI E-Utilities API Controller
 */
class NcbiQueryApiControllerFunctionalTest extends ApiControllerFunctionalTest
{
    /**
     * @testdox Check Endpoint Authentication
     */
    public function testEndpointRouteAndAuthentication()
    {
        $this->authenticateApiClient('visitor');

        $url = $this->router->generate('api_ncbi_search_journal_by_name');
        $this->assertSame('/v1/ncbi-search-journal-by-name', $url);
        $this->client->request('POST', $url);

        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck();

        $this->authenticateApiClient('user');

        $url = $this->router->generate('api_ncbi_search_journal_by_issn');
        $this->assertSame('/v1/ncbi-search-journal-by-issn', $url);
        $this->client->request('POST', $url);

        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertSame(0, $response['count']);
        $this->assertStringContainsStringIgnoringCase('Invalid Request, Missing Argument: \'term\'', $response['msg']);

        $url = $this->router->generate('api_ncbi_search_journal_by_name');
        $this->client->request('POST', $url, [
            'term'       => 'test',
            'page'       => 0,
            'maxperpage' => 0,
        ]);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertStringContainsStringIgnoringCase('Invalid current-page Value: \'page\'.', $response['msg']);

        $url = $this->router->generate('api_ncbi_search_journal_by_issn');
        $this->client->request('POST', $url, [
            'term'       => 'test',
            'page'       => 1,
            'maxperpage' => 0,
        ]);
        $this->assertSame(400, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(false);
        $this->assertSame(false, $response['success']);
        $this->assertStringContainsStringIgnoringCase('Invalid maximum-per-page Value: \'maxperpage\'.', $response['msg']);
    }

    /**
     * @dataProvider getSearchByNameStacks
     *
     * @testdox Check NCBI Query Search By Name Stacks
     */
    public function testSearchByNameStacks($queryPayload, $expectedUidCount)
    {
        $this->authenticateApiClient('user');

        $url = $this->router->generate('api_ncbi_search_journal_by_name');
        $this->assertSame('/v1/ncbi-search-journal-by-name', $url);

        $this->_validateSearchJournalAndStructure('JOURNAL', $url, $queryPayload, $expectedUidCount);
    }

    public function getSearchByNameStacks()
    {
        return [
            // #0
            [
                [
                    'term' => 'AAPS PharmSci',
                ],
                2,
            ],
            // #1
            [
                [
                    'term'       => 'American Association of Pharmaceutical Scientists.',
                    'page'       => 5,
                    'maxperpage' => 5,
                ],
                5,
            ],
            // #2
            [
                [
                    'term'       => 'TESTATESTBTESTCTESTD',
                    'maxperpage' => 2,
                ],
                0,
            ],
            // #3
            [
                [
                    'term' => 'Reproductive Medicine Review',
                ],
                1,
            ],
        ];
    }

    /**
     * @dataProvider getSearchByIssnStacks
     *
     * @testdox Check NCBI Query Search By ISSN Stacks
     */
    public function testSearchByIssnStacks($queryPayload, $expectedUidCount)
    {
        $this->authenticateApiClient('user');

        $url = $this->router->generate('api_ncbi_search_journal_by_issn');
        $this->assertSame('/v1/ncbi-search-journal-by-issn', $url);

        $this->_validateSearchJournalAndStructure('ISSN', $url, $queryPayload, $expectedUidCount);
    }

    public function getSearchByIssnStacks()
    {
        return [
            // #0
            [
                [
                    'term' => '1522-1059',
                ],
                1,
            ],
            // #1
            [
                [
                    'term'       => '0000-0000',
                    'maxperpage' => 2,
                ],
                0,
            ],
            // #2
            [
                [
                    'term'       => '2352-*',
                    'page'       => 10,
                    'maxperpage' => 7,
                ],
                7,
            ],
        ];
    }

    private function _validateSearchJournalAndStructure($field, $url, $queryPayload, $expectedUidCount)
    {
        $this->client->request('POST', $url, $queryPayload);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $response = $this->responseConvertAndCheck(true);

        $this->assertArrayHasKey('pagination', $response);
        $this->assertSame([
            'page',
            'overall',
            'perpage',
            'maxpage',
        ], array_keys($response['pagination']));

        $this->assertArrayHasKey('ncbi_query', $response);
        $this->assertSame([
            'term',
            'field',
        ], array_keys($response['ncbi_query']));
        $this->assertSame($queryPayload['term'], $response['ncbi_query']['term']);
        $this->assertSame($field, $response['ncbi_query']['field']);

        $this->assertTrue($response['count'] >= $expectedUidCount);

        if (empty($expectedUidCount)) {
            return;
        }

        $random = array_keys($response['data']);
        $random = $random[rand(0, (count($random) - 1))];
        $aJournal = $response['data'][$random] ?? null;

        $this->assertNotEmpty($aJournal);
        $this->assertSame([
            'uid',
            'host',
            'nlmuniqueid',
            'daterevised',
            'authorlist',
            'publicationinfolist',
            'resourceinfolist',
            'titlemainlist',
            'titlemainsort',
            'titleotherlist',
            'issnlist',
            'isbn',
            'country',
            'currentindexingstatus',
            'medlineta',
            'isoabbreviation',
            'startyear',
            'jrid',
            'endyear',
            'language',
            'broadheading',
            'continuationnotes',
        ], array_keys($aJournal));

        $this->_checkSubArrayStructure($aJournal, 'authorlist', false, [
            'collectivename',
            'lastname',
            'forename',
            'namequalifier',
            'otherinformation',
            'titleassociatedwithname',
            'aiid',
        ]);

        $this->_checkSubArrayStructure($aJournal,  'publicationinfolist', true, [
            'imprint',
            'place',
            'publisher',
            'dateissued',
            'edition',
            'datesofserialpublication',
        ]);

        $this->_checkSubArrayStructure($aJournal,  'resourceinfolist', true, [
            'typeofresource',
            'resourceunit',
        ]);

        $this->_checkSubArrayStructure($aJournal,  'titlemainlist', true, [
            'sorttitle',
            'title',
            'aiid',
        ]);

        $this->_checkSubArrayStructure($aJournal,  'titleotherlist', false, [
            'titlealternate',
            'otherinformation',
            'aiid',
        ]);

        $this->_checkSubArrayStructure($aJournal,  'issnlist', false, [
            'issn',
            'validyn',
            'issntype',
        ]);
    }

    private function _checkSubArrayStructure($array, $key, $required, $subs)
    {
        if ($required) {
            $this->assertNotEmpty($array[$key]);
        }

        if (!empty($array[$key])) {
            $index = rand(0, (count($array[$key]) - 1));
            $this->assertNotEmpty($array[$key][$index]);
            $this->assertSame($subs, array_keys($array[$key][$index]));
        }
    }
}
