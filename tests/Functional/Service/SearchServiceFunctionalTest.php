<?php

namespace App\Tests\Functional\Service;

use App\Entity\Experimental;
use App\Service\SearchService;
use App\Tests\Fixtures\Drug\BrandDrugRepoFixtures;
use App\Tests\Fixtures\Drug\ExperimentalDrugRepoFixtures;
use App\Tests\Fixtures\Drug\GenericDrugRepoFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox FUNCTIONAL | Service | Search Service
 */
class SearchServiceFunctionalTest extends WebTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $entityManager;
    private $em;

    /**
     * @var SearchService
     */
    private $ss;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new BrandDrugRepoFixtures());
        $loader->addFixture(new ExperimentalDrugRepoFixtures());
        $loader->addFixture(new GenericDrugRepoFixtures());

        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        $isMsql = (self::$entityManager->getConnection()->getDatabasePlatform() instanceof \Doctrine\DBAL\Platforms\MySqlPlatform);
        if ($isMsql) {
            self::$entityManager->getConnection()
                ->prepare('SET FOREIGN_KEY_CHECKS=0;')
                ->execute();
        }

        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->purge();

        if ($isMsql) {
            self::$entityManager->getConnection()
                ->prepare('SET FOREIGN_KEY_CHECKS=1;')
                ->execute();
        }

        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->em = $client->getContainer()->get('doctrine')->getManager();
        $this->ss = $client->getContainer()->get('test.' . SearchService::class);

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->em->close();
        $this->em = null;

        parent::tearDown();
    }

    /**
     * @testdox Checking Drugs Search for Search Page
     */
    public function testSearchForSearchPage()
    {
        try {
            $this->ss->execute();
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Missing search query', $e->getMessage());
        }

        try {
            $this->ss->setTerm('^test');
            $this->ss->setType('unknown');
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Unsupported Drug-Type', $e->getMessage());
        }

        $result = $this->ss
            ->setTerm('^test')
            ->setType('any')
            ->execute();

        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('overall-count', $result);
        $this->assertArrayHasKey('search-results', $result);
        $this->assertEquals($result['overall-count'], count($result['search-results']));

        $this->assertEquals(7, count($result['search-results'][0]));

        $max_inner = 0;
        foreach ($result['search-results'] as $sr_index => $sr_set){
            foreach ($sr_set as $sr_type => $sr_results) {
                foreach ($sr_results as $sr_results_single) {
                    if (!empty($sr_results_single)) {
                        if ($max_inner == 0) {
                            $sr_results_single = array_keys($sr_results_single);
                            $this->assertSame(array('id', 'name'), $sr_results_single);
                        }
                        $max_inner = max($max_inner, count($sr_results_single));
                    }
                }
            }
        }
        $this->assertEquals(2, $max_inner);

        $experimental = $this->em->getRepository(Experimental::class)->getDrug('test-2', 'name');

        $result2 = $this->ss
            ->setTerm('^test')
            ->setType('any')
            ->setExclusionList(array(
                'experimental' => array(
                    $experimental->getId(),
                ),
            ))
            ->execute();

        $this->assertNotEmpty($result2);
        $this->assertArrayHasKey('overall-count', $result);
        $this->assertArrayHasKey('search-results', $result);
        $this->assertEquals($result2['overall-count'], count($result2['search-results']));
        $this->assertLessThan(count($result['search-results']), count($result2['search-results']));
        $this->assertEquals(1, ((int) $result['overall-count'] - (int) $result2['overall-count']));

        // reset to check structure
        $result2['search-results'] = array_values($result2['search-results']);
        $any = $result2['search-results'][rand(0, ($result2['overall-count'] - 1))];

        foreach (array('experimental', 'generic', 'generic_synonym', 'generic_typo', 'brand', 'brand_synonym', 'brand_typo') as $key) {
            $this->assertArrayHasKey($key, $any);
            if (!empty($any[$key])) {
                foreach ($any[$key] as $drug) {
                    foreach (array('id', 'name') as $attr) {
                        $this->assertArrayHasKey($attr, $drug);
                        $this->assertNotEmpty($drug[$attr]);
                    }
                }
            }
        }
    }

    /**
     * @testdox Checking Drugs Search for AutoComplete
     */
    public function testSearchForAutoComplete()
    {
        $this->ss->setAutoComplete(true);

        try {
            $this->ss->setTerm('');
            $this->ss->execute();
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Missing search query', $e->getMessage());
        }

        try {
            $this->ss->setTerm('^test');
            $this->ss->setType('unknown');
            $this->ss->execute();
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Unsupported Drug-Type', $e->getMessage());
        }

        $result = $this->ss
            ->setTerm('^test')
            ->setType('any')
            ->execute();

        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('overall-count', $result);
        $this->assertArrayHasKey('search-results', $result);
        $this->assertEquals($result['overall-count'], count($result['search-results']));

        $experimental = $this->em->getRepository(Experimental::class)->getDrug('test-2', 'name');

        $result2 = $this->ss
            ->setTerm('^test')
            ->setType('any')
            ->setExclusionList(array(
                'experimental' => array(
                    $experimental->getId(),
                ),
            ))
            ->execute();

        $this->assertNotEmpty($result2);
        $this->assertArrayHasKey('overall-count', $result2);
        $this->assertArrayHasKey('search-results', $result2);
        $this->assertEquals($result2['overall-count'], count($result2['search-results']));
        $this->assertLessThan(count($result['search-results']), count($result2['search-results']));
        $this->assertEquals(1, ((int) $result['overall-count'] - (int) $result2['overall-count']));

        // reset to check structure
        $result2['search-results'] = array_values($result2['search-results']);
        $any = $result2['search-results'][rand(0, ($result2['overall-count'] - 1))];

        foreach (array('id', 'family', 'name', 'desc') as $key) {
            $this->assertArrayHasKey($key, $any);
            $this->assertNotEmpty($any[$key]);
        }
    }

    /**
     * @testdox Checking Drugs Search for Export
     */
    public function testSearchForExport()
    {
        $this->ss->setExport(true);

        try {
            $this->ss->execute();
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Missing search query', $e->getMessage());
        }

        try {
            $this->ss->setTerm('^test');
            $this->ss->setType('unknown');
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Unsupported Drug-Type', $e->getMessage());
        }

        $result = $this->ss
            ->setTerm('^test')
            ->setType('any')
            ->setExportComplete(false)
            ->execute();

        $this->assertArrayHasKey('file-path', $result);
        $this->assertNotEmpty($result['file-path']);
        $this->assertFileExists($result['file-path']);

        $csv = array_map('str_getcsv', file($result['file-path']));

        $this->assertEquals(6, count($csv[0]));
        $this->assertEquals(10, (count($csv) - 1));

        $result = $this->ss
            ->setTerm('^test')
            ->setType('any')
            ->setExportComplete(true)
            ->execute();

        $this->assertArrayHasKey('file-path', $result);
        $this->assertNotEmpty($result['file-path']);
        $this->assertFileExists($result['file-path']);

        $csv = array_map('str_getcsv', file($result['file-path']));
        $this->assertEquals(41, count($csv[0]));
        $this->assertEquals(10, (count($csv) - 1));
    }

    /**
     * @testdox Checking Drugs Search IDs for Export
     */
    public function testSearchIdsForExport()
    {
        $experimental = $this->em->getRepository(Experimental::class)->getDrug('test-1', 'name');

        $this->ss->setExport(true);
        $this->ss->setGivenList(array(
            'experimental' => array(
                $experimental->getId(),
            ),
        ));
        $this->ss->setExportComplete(true);
        $this->ss->setExportPrefix('export-test-001');
        $result = $this->ss->execute();

        $this->assertArrayHasKey('file-path', $result);
        $this->assertNotEmpty($result['file-path']);
        $this->assertStringContainsString('export-test-001', $result['file-path']);
        $this->assertFileExists($result['file-path']);

        $csv = array_map('str_getcsv', file($result['file-path']));
        $this->assertEquals(27, count($csv[0]));
        $this->assertEquals(1, (count($csv) - 1));

        $this->ss->setExportComplete(false);
        $this->ss->setExportPrefix('export-test-small');
        $result = $this->ss->execute();

        $this->assertArrayHasKey('file-path', $result);
        $this->assertNotEmpty($result['file-path']);
        $this->assertStringContainsString('export-test-small', $result['file-path']);
        $this->assertFileExists($result['file-path']);

        $csv = array_map('str_getcsv', file($result['file-path']));
        $this->assertEquals(6, count($csv[0]));
        $this->assertEquals(1, (count($csv) - 1));
    }
}
