<?php

namespace App\Tests\Functional\Service;

use App\Service\JournalCoreService;
use App\Service\JournalSourceService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Security;

/**
 * @testdox FUNCTIONAL | Service | JournalCoreService
 */
class JournalCoreServiceFunctionalTest extends WebTestCase
{
    /**
     * @var JournalCoreService
     */
    private $journalCoreService;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->journalCoreService = $client->getContainer()->get('test.' . JournalCoreService::class);

        parent::setUp();
    }

    /**
     * @testdox Checking Class Initialization
     */
    public function testInit()
    {
        $this->assertInstanceOf(JournalCoreService::class, $this->journalCoreService);
    }

    /**
     * @testdox Checking getFormDefaults() basic structure
     */
    public function testGetFormDefaults()
    {
        $result = JournalCoreService::getFormDefaults();

        $keys = [
            'name',
            'abbreviation',
            'specialty',
            'volume',
            'issue',
            'jr1',
            'jr2',
            'issn',
            'ismedline',
            'issecondline',
            'isdgabstract',
            'notbeingupdated',
            'drugsmonitored',
            'globaleditionjournal',
            'manualcreation',
            'deleted',
            'urlurl',
            'urlupdatedon',
            'urltoupdate',
            'urlviewedon',
            'urlviewedoncomment',
            'urlfrequency',
            'urlnote',
            'urlusername',
            'urlpassword',
            'publishername',
            'publisherregurl',
            'audiencelanguages',
            'audiencecountry',
            'audienceregion',
            'edition',
            'copyright',
            'absabstract',
            'absregforabstract',
            'absftavailable',
            'absregforfulltext',
            'absfeeforfulltext',
            'abscomments',
            'valstatus',
            'valauthor',
            'valdatecomposed',
            'valclearedby',
            'valclearedon',
            'valmodified',
            'valexported',
            'artodrhtmlcode',
            'artodrcgival',
            'artodrprinter',
        ];

        $this->assertTrue(is_array($result));
        $this->assertSame(count($keys), count($result));

        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
            $this->assertSame($result[$key], JournalCoreService::getFormDefaults($key));
        }
    }

    /**
     * @testdox Checking getFormDefaults() Unknown field
     */
    public function testGetFormDefaultsInvalid()
    {
        try {
            $result = JournalCoreService::getFormDefaults('alien-field');
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\Exception::class, $error);
            $this->assertStringContainsString('Unknown field name', $error->getMessage());
            $this->assertStringContainsString('alien-field', $error->getMessage());
        }
    }

    /**
     * @testdox Checking create() with Empty Fields
     */
    public function testCreateEmptyFields()
    {
        $this->prepMockedPropertiesForProcessingEmptyFields(false);

        $fields = [];
        $journal = $this->journalCoreService->create($fields);
        $this->journalTestEmpty($journal);

        $this->assertSame(101, $journal->getId());

        $now = new \DateTime('now');
        $this->assertInstanceOf(\DateTime::class, $journal->getDetailsMetaCreatedOn());
        $this->assertSame($now->format('Y-m-d H'), $journal->getDetailsMetaCreatedOn()->format('Y-m-d H'));
        $this->assertSame('Unknown', $journal->getDetailsMetaCreatedBy());

        $this->assertNull($journal->getDetailsMetaModifiedOn());
        $this->assertSame(null, $journal->getDetailsMetaModifiedBy());
    }

    /**
     * @testdox Checking update() with Empty Fields
     */
    public function testUpdateEmptyFields()
    {
        $this->prepMockedPropertiesForProcessingEmptyFields(true);

        $journal = new \App\Entity\Journal;
        $reflection = new \ReflectionClass(\App\Entity\Journal::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($journal, 101);

        $fields = [];
        $journal = $this->journalCoreService->update($journal, $fields);
        $this->journalTestEmpty($journal);

        $this->assertSame(101, $journal->getId());

        $this->assertNull($journal->getDetailsMetaCreatedOn());
        $this->assertSame(null, $journal->getDetailsMetaCreatedBy());

        $now = new \DateTime('now');
        $this->assertInstanceOf(\DateTime::class, $journal->getDetailsMetaModifiedOn());
        $this->assertSame($now->format('Y-m-d H'), $journal->getDetailsMetaModifiedOn()->format('Y-m-d H'));
        $this->assertSame('Unknown', $journal->getDetailsMetaModifiedBy());
    }

    /**
     * @testdox Checking create() with $fields
     */
    public function testCreateWithFields()
    {
        /**
         * PREP Property
         */
        $this->prepMockedPropertiesForProcessingDataFields(false);

        /**
         * PREP Fields
         */
        $fields = $this->getFormData();

        /**
         * EXECUTE
         */
        $journal = $this->journalCoreService->create($fields);

        /**
         * CHECKING
         */
        $this->journalTestData($journal, false);

        return $journal;
    }

    /**
     * @testdox Checking create() but missing ID after persist
     */
    public function testCreateButMissingIdAfterPersist()
    {
        $this->prepMockedPropertiesForProcessingEmptyFields(false, false);

        try {
            $fields = [];
            $this->journalCoreService->create($fields);
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Journal entity failed to return new ID', $e->getMessage());
        }
    }

    /**
     * @testdox Checking create() with Fields but remote throw exception
     */
    public function testCreateWithFieldsButExceptionThrown()
    {
        /**
         * PREP Property
         */
        $this->prepMockedPropertiesForProcessingDataFields(false, true);

        /**
         * PREP Fields
         */
        $fields = $this->getFormData();

        /**
         * EXECUTE
         */
        try {
            $this->journalCoreService->create($fields);
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Testing dumping create process.', $e->getMessage());
        }
    }

    /**
     * @depends testCreateWithFields
     *
     * @testdox Checking update() with $fields
     */
    public function testUpdateWithFields($journal)
    {
        /**
         * SET Journal
         */
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);

        $specialty1 = new \App\Entity\SpecialtyTaxonomy;
        $reflProp->setValue($specialty1, 20);
        $specialty1->setSpecialty('Oncology');
        $journalSpecialy1 = new \App\Entity\JournalSpecialties;
        $journalSpecialy1->setJournal($journal);
        $journalSpecialy1->setSpecialty($specialty1);

        $specialty2 = new \App\Entity\SpecialtyTaxonomy;
        $reflProp->setValue($specialty2, 28);
        $specialty2->setSpecialty('Cancer Treatment');
        $journalSpecialy2 = new \App\Entity\JournalSpecialties;
        $journalSpecialy2->setJournal($journal);
        $journalSpecialy2->setSpecialty($specialty2);

        $specialty3 = new \App\Entity\SpecialtyTaxonomy;
        $reflProp->setValue($specialty3, 31);
        $specialty3->setSpecialty('Hypertension');
        $journalSpecialy3 = new \App\Entity\JournalSpecialties;
        $journalSpecialy3->setJournal($journal);
        $journalSpecialy3->setSpecialty($specialty3);

        $specialties = new \Doctrine\Common\Collections\ArrayCollection([$journalSpecialy1, $journalSpecialy2, $journalSpecialy3]);

        $reflection = new \ReflectionClass(\App\Entity\Journal::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($journal, 101);

        $reflProp = $reflection->getProperty('specialties');
        $reflProp->setAccessible(true);
        $reflProp->setValue($journal, $specialties);

        /**
         * PREP Property
         */
        $this->prepMockedPropertiesForProcessingDataFields(true);

        /**
         * CHANGE Fields
         */
        $fields = $this->getFormData();
        $fields['urltoupdate']        = '2025-08-01';
        $fields['urlviewedoncomment'] = 'Site Closed';
        $fields['deleted']            = 'yes';
        $fields['valstatus']          = '5';
        $fields['specialty']          = [20, 28, 'Cardiology'];

        /**
         * EXECUTE
         */
        $journal = $this->journalCoreService->update($journal, $fields);

        /**
         * CHECKING
         */
        $this->journalTestData($journal, true);

        return $journal;
    }

    /**
     * @depends testUpdateWithFields
     *
     * @testdox Checking delete()
     */
    public function testDelete($journal)
    {
        /**
         * PREP Property
         */
        $journalRemote = $this->createMock(JournalSourceService::class);
        $journalRemote->expects($this->once())->method('remove')->will($this->returnCallback(function($journal) {
            $this->assertInstanceOf(\App\Entity\Journal::class, $journal);
            $this->assertSame(101, $journal->getId());
            return true;
        }));

        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->at(0))->method('remove')->will($this->returnCallback(function($journalSpecialties) {
            $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $journalSpecialties);
            $this->assertSame('Oncology', $journalSpecialties->getSpecialty()->getSpecialty());
            $this->assertSame(20, $journalSpecialties->getSpecialty()->getId());
            return true;
        }));

        $em->expects($this->at(1))->method('remove')->will($this->returnCallback(function($journalSpecialties) {
            $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $journalSpecialties);
            $this->assertSame('Cancer Treatment', $journalSpecialties->getSpecialty()->getSpecialty());
            $this->assertSame(28, $journalSpecialties->getSpecialty()->getId());
            return true;
        }));

        $em->expects($this->at(2))->method('remove')->will($this->returnCallback(function($journalSpecialties) {
            $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $journalSpecialties);
            $this->assertSame('Cardiology', $journalSpecialties->getSpecialty()->getSpecialty());
            $this->assertSame(35, $journalSpecialties->getSpecialty()->getId());
            return true;
        }));

        $em->expects($this->at(4))->method('remove')->will($this->returnCallback(function($journal) {
            $this->assertInstanceOf(\App\Entity\Journal::class, $journal);
            $this->assertSame(101, $journal->getId());
            return true;
        }));

        $em->expects($this->exactly(1))->method('flush')->will($this->returnValue(true));

        $user = $this->createMock(\App\Security\User\LdapUser::class);
        $user->method('getUsername')->will($this->returnValue('John.Doe'));
        $symfonySecurity = $this->createMock(Security::class);
        $symfonySecurity->method('getUser')->will($this->returnValue($user));

        $reflection = new \ReflectionClass(JournalCoreService::class);

        $reflProp = $reflection->getProperty('journalRemote');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $journalRemote);

        $reflProp = $reflection->getProperty('em');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $em);

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) {
            $this->assertStringContainsStringIgnoringCase('Journal Deleted', $msg);
            $this->assertArrayHasKey('id', $args);
            $this->assertSame(101, $args['id']);
            $this->assertArrayHasKey('user', $args);
            $this->assertSame('John.Doe', $args['user']);
            return true;
        }));
        $reflProp = $reflection->getProperty('logger');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $logger);

        $reflProp = $reflection->getProperty('symfonySecurity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $symfonySecurity);

        /**
         * EXECUTE
         */
        $this->journalCoreService->delete($journal);
    }

    private function getFormData()
    {
        return [
            'name'                 => 'JournalCoreServiceUnitTest Test 1',
            'abbreviation'         => 'JournalCoreServiceUnitTestT1',
            'specialty'            => ['Oncology', 'Cancer Treatment', 'Hypertension'],
            'volume'               => 'Volume 1.2'  ,
            'issue'                => 'Test Issue 12'  ,
            'jr1'                  => 4.88,
            'jr2'                  => 1.23,
            'issn'                 => '3AXD-FF69',
            'ismedline'            => 'yes',
            'issecondline'         => 'no',
            'isdgabstract'         => 'yes',
            'notbeingupdated'      => 'no',
            'drugsmonitored'       => 'yes',
            'globaleditionjournal' => 'no',
            'manualcreation'       => 'yes',
            'deleted'              => 'no',
            'urlurl'               => 'http://superduper.org/angelica' ,
            'urlupdatedon'         => '2019-08-28',
            'urltoupdate'          => '10 Jan 2018' , // just
            'urlviewedon'          => '2019-08-28'  ,
            'urlviewedoncomment'   => 'Everything is Normal',
            'urlfrequency'         => '56/year',
            'urlnote'              => 'Nothing to note here' . PHP_EOL . 'Just multi line test',
            'urlusername'          => 'wonderWoman',
            'urlpassword'          => 'l3ftSpiderMan@l0ne',
            'publishername'        => 'Canada Mint',
            'publisherregurl'      => 'http://canadamint.can.gov/test-1/?register',
            'audiencelanguages'    => ['English', 'Chinese', 'Japanese', 'Slovak'],
            'audiencecountry'      => 'AW',
            'audienceregion'       => 103,
            'edition'              => ['English', 'Korean', 'Japanese', 'Swedish'],
            'copyright'            => 'All free for take, so enjoy!',
            'absabstract'          => 'yes',
            'absregforabstract'    => 'no',
            'absftavailable'       => 'yes',
            'absregforfulltext'    => 'yes',
            'absfeeforfulltext'    => 'no',
            'abscomments'          => 'Just need an email, use eatme+spam@gmail.com',
            'valstatus'            => '1',
            'valauthor'            => 'Eric Nelson',
            'valdatecomposed'      => '2017-03-10 13:02:31',
            'valclearedby'         => 'Jonathan Hurix',
            'valclearedon'         => '2018-01-03 16:45:30',
            'valmodified'          => 'All still working, use the updated password',
            'valexported'          => 'yes',
            'artodrhtmlcode'       => 'http://superduper.org/angelica?q=%term%&id=%username&specialty=%specialty%',
            'artodrcgival'         => '58200',
            'artodrprinter'        => 'Vatican Limited Printer',
        ];
    }

    private function prepMockedPropertiesForProcessingEmptyFields($updated = false, $persistWithId = true)
    {
        $journalRemote = $this->createMock(JournalSourceService::class);
        if ($persistWithId) {
            $journalRemote->expects($this->once())->method('upsync')->will($this->returnCallback(function($Journal) {
                $this->assertInstanceOf(\App\Entity\Journal::class, $Journal);
                return true;
            }));
        }

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->exactly($updated ? 1 : ($persistWithId ? 2 : 1)))->method('persist')->will($this->returnCallback(function($journal) use ($updated, $persistWithId) {
            $this->assertInstanceOf(\App\Entity\Journal::class, $journal);
            if (
                (!$updated) &&
                ($persistWithId)
            ) {
                $reflection = new \ReflectionClass(\App\Entity\Journal::class);
                $reflProp = $reflection->getProperty('id');
                $reflProp->setAccessible(true);
                $reflProp->setValue($journal, 101);
            }
            return true;
        }));
        $em->expects($this->exactly($updated ? 1 : 2))->method('flush')->will($this->returnValue(true));

        $reflection = new \ReflectionClass(JournalCoreService::class);

        $reflProp = $reflection->getProperty('journalRemote');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $journalRemote);

        $reflProp = $reflection->getProperty('em');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $em);

        $logger = $this->createMock(LoggerInterface::class);
        if ($persistWithId) {
            $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) use ($updated) {
                if (!$updated) {
                    $this->assertStringContainsStringIgnoringCase('Journal Created', $msg);
                }
                else {
                    $this->assertStringContainsStringIgnoringCase('Journal Updated', $msg);
                }
                $this->assertArrayHasKey('id', $args);
                $this->assertSame(101, $args['id']);
                $this->assertArrayHasKey('user', $args);
                $this->assertSame('Unknown', $args['user']);
                return true;
            }));
        }

        $reflProp = $reflection->getProperty('logger');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $logger);

        $symfonySecurity = $this->createMock(Security::class);
        $reflProp = $reflection->getProperty('symfonySecurity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $symfonySecurity);
    }

    private function prepMockedPropertiesForProcessingDataFields($update = false, $exception = false)
    {
        $journalRemote = $this->createMock(JournalSourceService::class);

        if (!$exception) {
            $journalRemote->expects($this->once())->method('upsync')->will($this->returnCallback(function($Journal) {
                $this->assertInstanceOf(\App\Entity\Journal::class, $Journal);
                return true;
            }));
        }
        else {
            $journalRemote->expects($this->once())->method('upsync')->will($this->throwException(new \Exception('Testing dumping create process.')));
        }

        $em = $this->createMock(EntityManagerInterface::class);

        // ___searchOrCreateNewSpecialtyTaxonomy #1 - #3
        if (!$update) {
            $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
            $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue(null));
            $repo->expects($this->at(1))->method('findOneBy')->will($this->returnValue(null));
            $em->method('getRepository')->will($this->returnValue($repo));

            if (!$exception) {
                // (3 x 2) + 2
                $em->expects($this->exactly(8))->method('persist')->will($this->returnCallback(function($entity) {
                    if ($entity instanceof \App\Entity\Journal) {
                        $reflection = new \ReflectionClass(\App\Entity\Journal::class);
                        $reflProp = $reflection->getProperty('id');
                        $reflProp->setAccessible(true);
                        $reflProp->setValue($entity, 101);
                    }
                    return true;
                }));
                // 1 + 1
                $em->expects($this->exactly(2))->method('flush')->will($this->returnValue(true));
            }
            else {
                // (3 x 1) + 1
                $em->expects($this->exactly(4))->method('remove')->will($this->returnValue(true));
                // (3 x 2) + 1
                $em->expects($this->exactly(7))->method('persist')->will($this->returnCallback(function($entity) {
                    if ($entity instanceof \App\Entity\Journal) {
                        $reflection = new \ReflectionClass(\App\Entity\Journal::class);
                        $reflProp = $reflection->getProperty('id');
                        $reflProp->setAccessible(true);
                        $reflProp->setValue($entity, 101);
                    }
                    return true;
                }));
                // 1 + 1
                $em->expects($this->exactly(2))->method('flush')->will($this->returnValue(true));
            }
        }
        else {
            $em->expects($this->exactly(1))->method('remove')->will($this->returnCallback(function($JournalSpecialties) {
                $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $JournalSpecialties);
                $this->assertSame('Hypertension', $JournalSpecialties->getSpecialty()->getSpecialty());
                $this->assertSame(31, $JournalSpecialties->getSpecialty()->getId());
                return true;
            }));

            $specialtyTax2 = $this->createConfiguredMock(\App\Entity\SpecialtyTaxonomy::class, [
                'getId'        => 35,
                'getSpecialty' => 'Cardiology',
            ]);
            $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
            $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue(null));
            $repo->expects($this->at(1))->method('findOneBy')->will($this->returnValue($specialtyTax2));
            $em->method('getRepository')->will($this->returnValue($repo));

            // (1) + 1
            $em->expects($this->exactly(2))->method('persist')->will($this->returnValue(true));
            // 0 + 1
            $em->expects($this->exactly(1))->method('flush')->will($this->returnValue(true));
        }

        // __getCurrentUserUsername
        $user = $this->createMock(\App\Security\User\LdapUser::class);
        $user->method('getUsername')->will($this->returnValue('John.Doe'));
        $symfonySecurity = $this->createMock(Security::class);
        $symfonySecurity->method('getUser')->will($this->returnValue($user));

        $reflection = new \ReflectionClass(JournalCoreService::class);

        $reflProp = $reflection->getProperty('journalRemote');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $journalRemote);

        $reflProp = $reflection->getProperty('em');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $em);

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(($exception ? $this->never() : $this->once()))->method('info')->will($this->returnCallback(function($msg, $args) use ($update) {
            if (!$update) {
                $this->assertStringContainsStringIgnoringCase('Journal Created', $msg);
            }
            else {
                $this->assertStringContainsStringIgnoringCase('Journal Updated', $msg);
            }
            $this->assertArrayHasKey('id', $args);
            $this->assertSame(101, $args['id']);
            $this->assertArrayHasKey('user', $args);
            $this->assertSame('John.Doe', $args['user']);
            return true;
        }));
        $reflProp = $reflection->getProperty('logger');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $logger);

        $reflProp = $reflection->getProperty('symfonySecurity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->journalCoreService, $symfonySecurity);
    }

    private function journalTestEmpty($journal)
    {
        $this->assertInstanceOf(\App\Entity\Journal::class, $journal);

        $this->assertSame('', $journal->getName());
        $this->assertSame(null, $journal->getAbbreviation());
        $this->assertSame(null, $journal->getVolume());
        $this->assertSame(null, $journal->getNumber());

        $this->assertSame(0, $journal->getStatus());
        $this->assertSame('None', $journal->getStatusReadable());
        $this->assertTrue($journal->isStatusNone());
        $this->assertFalse($journal->isStatusActive());
        $this->assertFalse($journal->isStatusArchived());

        $this->assertSame(0, $journal->getExported());
        $this->assertFalse($journal->isExported());

        $this->assertSame(0, $journal->getIsMedline());
        $this->assertFalse($journal->isMedLine());

        $this->assertSame(0, $journal->getIsSecondLine());
        $this->assertFalse($journal->isSecondLine());

        $this->assertSame(0, $journal->getIsDgAbstract());
        $this->assertFalse($journal->isDgAbstract());

        $this->assertSame(0, $journal->getNotBeingUpdated());
        $this->assertFalse($journal->isNotBeingUpdated());

        $this->assertSame(0, $journal->getDrugsMonitored());
        $this->assertFalse($journal->isDrugsMonitored());

        $this->assertSame(0, $journal->getGlobalEditionJournal());
        $this->assertFalse($journal->isGlobalEditionJournal());

        $this->assertSame(0, $journal->getDeleted());
        $this->assertFalse($journal->isDeleted());

        $this->assertSame(0, $journal->getManualCreation());
        $this->assertFalse($journal->isManualCreation());

        $this->assertNull($journal->getMedlineIssn());
        $this->assertSame(0.00, $journal->getJournalReport1());
        $this->assertSame(0.00, $journal->getJournalReport2());
        $this->assertNull($journal->getUnid());

        $this->assertSame('', $journal->getDetailsLanguage());
        $this->assertSame(null, $journal->getDetailsCountry());
        $this->assertSame(null, $journal->getDetailsRegion());
        $this->assertSame('', $journal->getDetailsEdition());
        $this->assertSame('', $journal->getDetailsCopyright());
        $this->assertSame('', $journal->getDetailsAutoPublishingSource());
        $this->assertSame('', $journal->getDetailsPreviousRowGuid());
        $this->assertSame(false, $journal->getDetailsAbstractAbstract());
        $this->assertSame(false, $journal->getDetailsAbstractRegForAbstract());
        $this->assertSame(false, $journal->getDetailsAbstractFullTextAvailablity());
        $this->assertSame(false, $journal->getDetailsAbstractRegForFullText());
        $this->assertSame(false, $journal->getDetailsAbstractFeeForFullText());
        $this->assertSame('', $journal->getDetailsAbstractComments());
        $this->assertSame('', $journal->getDetailsOrderingHtmlCode());
        $this->assertSame('', $journal->getDetailsOrderingCgiValue());
        $this->assertSame('', $journal->getDetailsOrderingPrinterName());
        $this->assertSame('', $journal->getDetailsPublisherName());
        $this->assertSame('', $journal->getDetailsPublisherRegUrl());
        $this->assertSame('', $journal->getDetailsUrlUrl());
        $this->assertSame(null, $journal->getDetailsUrlUpdatedOn());
        $this->assertSame(null, $journal->getDetailsUrlViewedOn());
        $this->assertSame('', $journal->getDetailsUrlViewedOnComment());
        $this->assertSame(null, $journal->getDetailsUrlToUpdate());
        $this->assertSame('', $journal->getDetailsUrlNote());
        $this->assertSame('', $journal->getDetailsUrlFrequency());
        $this->assertSame('', $journal->getDetailsUrlUsername());
        $this->assertSame('', $journal->getDetailsUrlPassword());
        $this->assertSame('', $journal->getDetailsValidationAuthor());
        $this->assertSame(null, $journal->getDetailsValidationDateComposed());
        $this->assertSame('', $journal->getDetailsValidationClearedBy());
        $this->assertSame(null, $journal->getDetailsValidationClearedOn());
        $this->assertSame('', $journal->getDetailsValidationModified());

        $this->assertInstanceOf(\Doctrine\Common\Collections\ArrayCollection::class, $journal->getSpecialties());
        $this->assertSame(0, $journal->getSpecialties()->count());
    }

    private function journalTestData($journal, $update = false)
    {
        $this->assertInstanceOf(\App\Entity\Journal::class, $journal);

        $this->assertSame(101, $journal->getId());

        $this->assertSame('JournalCoreServiceUnitTest Test 1', $journal->getName());
        $this->assertSame('JournalCoreServiceUnitTestT1', $journal->getAbbreviation());
        $this->assertSame('Volume 1.2', $journal->getVolume());
        $this->assertSame('Test Issue 12', $journal->getNumber());

        if (!$update) {
            $this->assertSame(1, $journal->getStatus());
            $this->assertSame('Active', $journal->getStatusReadable());
            $this->assertSame(false, $journal->isStatusNone());
            $this->assertSame(true, $journal->isStatusActive());
            $this->assertSame(false, $journal->isStatusArchived());
        }
        else {
            $this->assertSame(5, $journal->getStatus());
            $this->assertSame('Archived', $journal->getStatusReadable());
            $this->assertSame(false, $journal->isStatusNone());
            $this->assertSame(false, $journal->isStatusActive());
            $this->assertSame(true, $journal->isStatusArchived());
        }

        $this->assertSame(1, $journal->getExported());
        $this->assertSame(true, $journal->isExported());

        $this->assertSame(1, $journal->getIsMedline());
        $this->assertSame(true, $journal->isMedLine());

        $this->assertSame(0, $journal->getIsSecondLine());
        $this->assertSame(false, $journal->isSecondLine());

        $this->assertSame(1, $journal->getIsDgAbstract());
        $this->assertSame(true, $journal->isDgAbstract());

        $this->assertSame(0, $journal->getNotBeingUpdated());
        $this->assertSame(false, $journal->isNotBeingUpdated());

        $this->assertSame(1, $journal->getDrugsMonitored());
        $this->assertSame(true, $journal->isDrugsMonitored());

        $this->assertSame(0, $journal->getGlobalEditionJournal());
        $this->assertSame(false, $journal->isGlobalEditionJournal());

        if (!$update) {
            $this->assertSame(0, $journal->getDeleted());
            $this->assertSame(false, $journal->isDeleted());
        }
        else {
            $this->assertSame(1, $journal->getDeleted());
            $this->assertSame(true, $journal->isDeleted());
        }

        $this->assertSame(1, $journal->getManualCreation());
        $this->assertSame(true, $journal->isManualCreation());

        $this->assertSame('3AXD-FF69', $journal->getMedlineIssn());

        $this->assertSame(4.88, $journal->getJournalReport1());
        $this->assertSame(1.23, $journal->getJournalReport2());

        $this->assertSame(null, $journal->getUnid());

        $this->assertSame('English;Chinese;Japanese;Slovak', $journal->getDetailsLanguage());
        $this->assertSame('AW', $journal->getDetailsCountry());
        $this->assertSame(103, $journal->getDetailsRegion());
        $this->assertSame('English;Korean;Japanese;Swedish', $journal->getDetailsEdition());
        $this->assertSame('All free for take, so enjoy!', $journal->getDetailsCopyright());

        $this->assertSame('', $journal->getDetailsAutoPublishingSource());
        $this->assertSame('', $journal->getDetailsPreviousRowGuid());

        $this->assertSame(true, $journal->getDetailsAbstractAbstract());
        $this->assertSame(false, $journal->getDetailsAbstractRegForAbstract());
        $this->assertSame(true, $journal->getDetailsAbstractFullTextAvailablity());
        $this->assertSame(true, $journal->getDetailsAbstractRegForFullText());
        $this->assertSame(false, $journal->getDetailsAbstractFeeForFullText());
        $this->assertSame('Just need an email, use eatme+spam@gmail.com', $journal->getDetailsAbstractComments());

        $this->assertSame('http://superduper.org/angelica?q=%term%&id=%username&specialty=%specialty%', $journal->getDetailsOrderingHtmlCode());
        $this->assertSame('58200', $journal->getDetailsOrderingCgiValue());
        $this->assertSame('Vatican Limited Printer', $journal->getDetailsOrderingPrinterName());

        $this->assertSame('Canada Mint', $journal->getDetailsPublisherName());
        $this->assertSame('http://canadamint.can.gov/test-1/?register', $journal->getDetailsPublisherRegUrl());

        $this->assertSame('http://superduper.org/angelica', $journal->getDetailsUrlUrl());
        $this->assertInstanceOf(\DateTime::class, $journal->getDetailsUrlUpdatedOn());
        $this->assertSame('2019-08-28', $journal->getDetailsUrlUpdatedOn()->format('Y-m-d'));
        $this->assertInstanceOf(\DateTime::class, $journal->getDetailsUrlViewedOn());
        $this->assertSame('2019-08-28', $journal->getDetailsUrlViewedOn()->format('Y-m-d'));

        if (!$update) {
            $this->assertSame('Everything is Normal', $journal->getDetailsUrlViewedOnComment());
        }
        else {
            $this->assertSame('Site Closed', $journal->getDetailsUrlViewedOnComment());
        }

        if (!$update) {
            $this->assertSame(null, $journal->getDetailsUrlToUpdate());
        }
        else {
            $this->assertInstanceOf(\DateTime::class, $journal->getDetailsUrlToUpdate());
            $this->assertSame('2025-08-01', $journal->getDetailsUrlToUpdate()->format('Y-m-d'));
        }

        $this->assertSame('Nothing to note here' . PHP_EOL . 'Just multi line test', $journal->getDetailsUrlNote());
        $this->assertSame('56/year', $journal->getDetailsUrlFrequency());
        $this->assertSame('wonderWoman', $journal->getDetailsUrlUsername());
        $this->assertSame('l3ftSpiderMan@l0ne', $journal->getDetailsUrlPassword());

        $this->assertSame('Eric Nelson', $journal->getDetailsValidationAuthor());
        $this->assertInstanceOf(\DateTime::class, $journal->getDetailsValidationDateComposed());
        $this->assertSame('2017-03-10', $journal->getDetailsValidationDateComposed()->format('Y-m-d'));
        $this->assertSame('Jonathan Hurix', $journal->getDetailsValidationClearedBy());
        $this->assertInstanceOf(\DateTime::class, $journal->getDetailsValidationClearedOn());
        $this->assertSame('2018-01-03', $journal->getDetailsValidationClearedOn()->format('Y-m-d'));
        $this->assertSame('All still working, use the updated password', $journal->getDetailsValidationModified());

        $this->assertInstanceOf(\Doctrine\Common\Collections\ArrayCollection::class, $journal->getSpecialties());
        $this->assertSame(3, $journal->getSpecialties()->count());

        if (!$update) {
            $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $journal->getSpecialties()->toArray()[0]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,  $journal->getSpecialties()->toArray()[0]->getSpecialty());
            $this->assertSame('Oncology',                                  $journal->getSpecialties()->toArray()[0]->getSpecialty()->getSpecialty());

            $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $journal->getSpecialties()->toArray()[1]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,  $journal->getSpecialties()->toArray()[1]->getSpecialty());
            $this->assertSame('Cancer Treatment',                          $journal->getSpecialties()->toArray()[1]->getSpecialty()->getSpecialty());

            $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $journal->getSpecialties()->toArray()[2]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,  $journal->getSpecialties()->toArray()[2]->getSpecialty());
            $this->assertSame('Hypertension',                              $journal->getSpecialties()->toArray()[2]->getSpecialty()->getSpecialty());
        }
        else {
            $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $journal->getSpecialties()->toArray()[0]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,  $journal->getSpecialties()->toArray()[0]->getSpecialty());
            $this->assertSame('Oncology',                                  $journal->getSpecialties()->toArray()[0]->getSpecialty()->getSpecialty());
            $this->assertSame(20,                                          $journal->getSpecialties()->toArray()[0]->getSpecialty()->getId());

            $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $journal->getSpecialties()->toArray()[1]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,  $journal->getSpecialties()->toArray()[1]->getSpecialty());
            $this->assertSame('Cancer Treatment',                          $journal->getSpecialties()->toArray()[1]->getSpecialty()->getSpecialty());
            $this->assertSame(28,                                          $journal->getSpecialties()->toArray()[1]->getSpecialty()->getId());

            $this->assertInstanceOf(\App\Entity\JournalSpecialties::class, $journal->getSpecialties()->toArray()[3]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,  $journal->getSpecialties()->toArray()[3]->getSpecialty());
            $this->assertSame('Cardiology',                                $journal->getSpecialties()->toArray()[3]->getSpecialty()->getSpecialty());
            $this->assertSame(35,                                          $journal->getSpecialties()->toArray()[3]->getSpecialty()->getId());
        }

        $now = new \DateTime('now');
        $this->assertInstanceOf(\DateTime::class, $journal->getDetailsMetaCreatedOn());
        $this->assertSame($now->format('Y-m-d H'), $journal->getDetailsMetaCreatedOn()->format('Y-m-d H'));
        $this->assertSame('John.Doe', $journal->getDetailsMetaCreatedBy());

        if (!$update) {
            $this->assertNull($journal->getDetailsMetaModifiedOn());
            $this->assertSame(null, $journal->getDetailsMetaModifiedBy());
        }
        else {
            $now = new \DateTime('now');
            $this->assertInstanceOf(\DateTime::class, $journal->getDetailsMetaModifiedOn());
            $this->assertSame($now->format('Y-m-d H'), $journal->getDetailsMetaModifiedOn()->format('Y-m-d H'));
            $this->assertSame('John.Doe', $journal->getDetailsMetaModifiedBy());
        }
    }
}
