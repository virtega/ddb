<?php

namespace App\Tests\Functional\Service;

use App\Entity\Brand;
use App\Entity\Experimental;
use App\Entity\Generic;
use App\Exception\DrugCrudException;
use App\Service\CoreService;
use App\Tests\Fixtures\CountryFixtures;
use App\Tests\Fixtures\Drug\BrandDrugRepoFixtures;
use App\Tests\Fixtures\Drug\ExperimentalDrugRepoFixtures;
use App\Tests\Fixtures\Drug\GenericDrugRepoFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * @testdox FUNCTIONAL | Service | Core Service
 */
class CoreServiceFunctionalTest extends WebTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $entityManager;
    private $em;

    /**
     * @var CoreService
     */
    private $cs;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new BrandDrugRepoFixtures());
        $loader->addFixture(new ExperimentalDrugRepoFixtures());
        $loader->addFixture(new GenericDrugRepoFixtures());

        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        $isMsql = (self::$entityManager->getConnection()->getDatabasePlatform() instanceof \Doctrine\DBAL\Platforms\MySqlPlatform);
        if ($isMsql) {
            self::$entityManager->getConnection()
                ->prepare('SET FOREIGN_KEY_CHECKS=0;')
                ->execute();
        }

        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->purge();

        if ($isMsql) {
            self::$entityManager->getConnection()
                ->prepare('SET FOREIGN_KEY_CHECKS=1;')
                ->execute();
        }

        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->em = $client->getContainer()->get('doctrine')->getManager();
        $this->cs = $client->getContainer()->get('test.' . CoreService::class);

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->em->close();
        $this->em = null;
        $this->cs = null;

        parent::tearDown();
    }

    /**
     * @testdox Checking Properties
     */
    public function testBasic()
    {
        $this->assertNotEmpty(CoreService::VERSION);
    }

    /**
     * @testdox Checking Get Drug
     */
    public function testGetDrug()
    {
        try {
            $this->cs->getDrug('unknown', 0);
        } catch (DrugCrudException $e) {
            $this->assertStringContainsStringIgnoringCase('Unknown Drug Type:', $e->getMessage());
        }

        try {
            $this->cs->getDrug('experimental', 0);
        } catch (DrugCrudException $e) {
            $this->assertStringContainsStringIgnoringCase('Unknown experimental Drug', $e->getMessage());
        }

        $sql  = 'SELECT `id` FROM `experimental` LIMIT 1;';
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $drug = $stmt->fetchColumn();
        $drug = $this->cs->getDrug('experimental', $drug);
        $this->assertTrue($drug instanceof \App\Entity\Experimental);
    }

    /**
     * @testdox Checking Get Drug Family
     */
    public function testGetDrugFamily()
    {
        try {
            $this->cs->getDrugFamily('unknown', 0);
        } catch (DrugCrudException $e) {
            $this->assertStringContainsStringIgnoringCase('Unknown Drug Type:', $e->getMessage());
        }

        try {
            $this->cs->getDrugFamily('experimental', 0);
        } catch (DrugCrudException $e) {
            $this->assertStringContainsStringIgnoringCase('Unknown experimental Drug', $e->getMessage());
        }

        $sql  = 'SELECT `id` FROM `generic` LIMIT 1;';
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $drug = $stmt->fetchColumn();
        $drug = $this->cs->getDrugFamily('generic', $drug);
        $this->assertTrue(is_array($drug));
        $this->assertEquals(5, count($drug));
    }

    /**
     * @testdox Checking Experimental Drug Family Getter
     */
    public function testGetExperimentalDrug()
    {
        $sql  = 'SELECT * FROM `experimental` LIMIT 1;';
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $drug = $stmt->fetchColumn();

        $this->assertNotEmpty($drug);

        $family = $this->cs->getExperimentalDrug($drug);

        $this->assertEquals(1, count($family));
        $this->assertArrayHasKey('main', $family);
    }

    /**
     * @testdox Checking Generic Drug Family Getter
     */
    public function testGetGenericDrug()
    {
        $sql  = "SELECT * FROM `generic` WHERE `name` = 'test-1';";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $drug = $stmt->fetchColumn();

        $this->assertNotEmpty($drug);

        $family = $this->cs->getGenericDrug($drug);

        $this->assertEquals(5, count($family));

        $expects = array(
            'main',
            'synonyms',
            'synonyms_keys',
            'typos',
            'typos_keys',
        );
        $this->assertSame($expects, array_keys($family));

        $this->assertEquals(2, count($family['synonyms']));
        $this->assertEquals(2, count($family['synonyms_keys']));
        $this->assertEquals(3, count($family['typos']));
        $this->assertEquals(3, count($family['typos_keys']));
    }

    /**
     * @testdox Checking Brand Drug Family Getter
     */
    public function testGetBrandDrug()
    {
        $sql  = "SELECT * FROM `brand` WHERE `name` = 'test-1';";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $drug = $stmt->fetchColumn();

        $this->assertNotEmpty($drug);

        $family = $this->cs->getBrandDrug($drug);

        $this->assertEquals(5, count($family));

        $expects = array(
            'main',
            'synonyms',
            'synonyms_keys',
            'typos',
            'typos_keys',
        );
        $this->assertSame($expects, array_keys($family));

        $this->assertEquals(2, count($family['synonyms']));
        $this->assertEquals(2, count($family['synonyms_keys']));
        $this->assertEquals(3, count($family['typos']));
        $this->assertEquals(3, count($family['typos_keys']));
    }

    /**
     * @testdox Checking Generic Reference Levels Family Getter
     */
    public function testGetGenericRefLevels()
    {
        for ($i = 1; $i <= 4; ++$i) {
            $sql  = "SELECT COUNT(*) AS `found` FROM `generic_ref_level{$i}`;";
            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->execute();
            $found = (int) $stmt->fetchColumn();

            $this->assertNotEmpty($found);

            $result = $this->cs->getGenericRefLevel($i);

            $this->assertNotEmpty($result);

            $this->assertEquals(count($result), count($result));
        }

        try {
            $this->cs->getGenericRefLevel(5);
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Unknown Generic Reference Level:', $e->getMessage());
        }
    }

    /**
     * @testdox Checking Drugs Creation Failure
     */
    public function testUpdateDeleteDrugFailure()
    {
        $request = new ParameterBag(array());

        try {
            $this->cs->updateDrug('unknown', $request, false);
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Unknown Drug Type:', $e->getMessage());
        }

        try {
            $this->cs->deleteDrug('unknown', $request, false);
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Unknown Drug Type:', $e->getMessage());
        }

        try {
            $this->cs->updateDrug('experimental', $request, false);
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Drug Name is empty', $e->getMessage());
        }
    }

    /**
     * @dataProvider getNewDrugProperties
     *
     * @testdox Checking Drugs Creation and Deletion: Stacks
     */
    public function testUpdateDrug($type, $requestArr, $addRelation, $revMsg)
    {
        $request = new ParameterBag($requestArr);

        // add
        list($status, $msg, $dig, $drug) = $this->cs->updateDrug($type, $request, false);

        $this->assertTrue($status);
        $this->assertSame('Drug has been registered #' . $dig, $msg);
        $this->assertNotEmpty($dig);
        $this->assertSame('App\Entity\\' . ucwords($type), get_class($drug));

        if (!$addRelation) {
            // update to update keywords: #1, #2
            if ($type == 'generic') { // #1    - synonyms
                // query family
                $drugFam = $this->cs->getDrugFamily($type, $dig);

                // remove
                unset($requestArr['generic_synonyms'][0]);

                // change to id
                foreach ($drugFam['synonyms_keys'] as $k_index => $keyword_id) {
                    if ($k_index > 0) {
                        // replacing
                        $requestArr['generic_synonyms'][$k_index] = array(
                            $keyword_id => $drugFam['synonyms'][$keyword_id]->getName(),
                            'country'   => $drugFam['synonyms'][$keyword_id]->getCountry(),
                        );
                    }
                }

                // new
                $requestArr['generic_synonyms'][] = array(
                    'new'     => 'generic-s-cs-creation-04',
                    'country' => array('SG', 'PA'),
                );

                // missing
                $requestArr['generic_synonyms'][] = array(
                    'country' => array('MY', 'CH'),
                );

                $requestArr['generic_synonyms'] = array_values($requestArr['generic_synonyms']);
            }
            elseif ($type == 'brand') { // #2    - typos
                // query family
                $drugFam = $this->cs->getDrugFamily($type, $dig);

                // remove
                unset($requestArr['brand_typos'][0]);

                // change to id
                foreach ($drugFam['typos_keys'] as $k_index => $keyword_id) {
                    if ($k_index > 0) {
                        // replacing
                        $requestArr['brand_typos'][$k_index] = array(
                            $keyword_id => $drugFam['typos'][$keyword_id]->getName(),
                        );
                    }
                }

                // new
                $requestArr['brand_typos'][] = array(
                    'new' => 'brand-t-cs-creation-04',
                );

                // missing
                $requestArr['brand_typos'][] = array(
                    'new' => '',
                );

                $requestArr['brand_typos'] = array_values($requestArr['brand_typos']);
            }
            // repack
            $request = new ParameterBag($requestArr);

            // update
            list($statusExt, $msgExt, $digExt, $drugExt) = $this->cs->updateDrug($type, $request, $dig);
            $this->assertTrue($statusExt);
            $this->assertStringContainsStringIgnoringCase('Drug details has been updated', $msgExt);
            $this->assertNotEmpty($digExt);
            $this->assertSame('App\Entity\\' . ucwords($type), get_class($drugExt));
        }
        else {
            // add-relation: #3, #4
            switch ($type) {
                case 'generic':
                    $experimental = $this->em->getRepository(Experimental::class)->getDrug('test-3', 'name');
                    $status = $this->cs->updateDrugRelation($type, $dig, 'experimental', $experimental->getId());
                    $this->assertTrue($status);

                    // for delete - expect error
                    try {
                        $this->cs->updateDrugRelation($type, $dig, 'experimental', false);
                    }
                    catch (\Exception $e) {
                        $this->assertStringContainsStringIgnoringCase('Missing lower-entity ID', $e->getMessage());
                    }

                    // for delete
                    $status = $this->cs->updateDrugRelation($type, $dig, false, false);
                    $this->assertTrue($status);

                    // for re-add
                    $status = $this->cs->updateDrugRelation($type, $dig, 'experimental', $experimental->getId());
                    $this->assertTrue($status);

                    // for update
                    $status = $this->cs->updateDrugRelation($type, $dig, 'experimental', $experimental->getId());
                    $this->assertTrue($status);
                    break;

                case 'brand':
                    $generic = $this->em->getRepository(Generic::class)->getDrug('test-3', 'name');
                    $status = $this->cs->updateDrugRelation($type, $dig, 'generic', $generic->getId());
                    $this->assertTrue($status);

                    // for delete
                    $status = $this->cs->updateDrugRelation($type, $dig, false, false);
                    $this->assertTrue($status);

                    // for re-add
                    $status = $this->cs->updateDrugRelation($type, $dig, 'generic', $generic->getId());
                    $this->assertTrue($status);

                    // for update
                    $status = $this->cs->updateDrugRelation($type, $dig, 'generic', $generic->getId());
                    $this->assertTrue($status);
                    break;
            }
        }

        // delete
        list($status, $msg) = $this->cs->deleteDrug($type, $dig);
        $this->assertTrue($status);
        $this->assertSame($revMsg, $msg);

        // recheck relation still available
        if ($addRelation) {
            switch ($type) {
                case 'generic':
                        $experimental = $this->em->getRepository(Experimental::class)->getDrug('test-3', 'name');
                        $this->assertNotEmpty($experimental);
                        break;

                    case 'brand':
                        $generic = $this->em->getRepository(Generic::class)->getDrug('test-3', 'name');
                        $this->assertNotEmpty($generic);
                        break;
            }
        }
    }

    public function getNewDrugProperties()
    {
        return array(
            // #0
            array(
                'type'    => 'experimental',
                'request' => array(
                    'name'                => 'cs-test-create-experimental',
                    'type'                => 'experimental',
                    'valid'               => 0,
                    'comments'            => 'this-is-a-test-for-cs-service-on-drug-creation-stack',
                    'unid'                => 'OKH8HZ7E4U3C85383ZNS3K2DQNRU7873',
                    'creation_date'       => '2017-02-19 05:06:21',
                ),
                'relate' => false,
                'remove' => '1 Record Removed, meanwhile 0 detached.',
            ),
            // #1
            array(
                'type'    => 'generic',
                'request' => array(
                    'name'                => 'cs-test-create-generic',
                    'valid'               => 0,
                    'comments'            => 'this-is-a-test-for-cs-service-on-drug-creation-stack',
                    'level_1'             => 'AB',
                    'level_2'             => 'CD',
                    'level_3'             => 'EF',
                    'level_4'             => 'GH',
                    'level_5'             => 'IJ',
                    'unid'                => 'B5F3FFA7ER7NLM2LS1IBGOIT7PRU7873',
                    'auto_encode_explude' => 0,
                    'no_double_bounce'    => 1,
                    'creation_date'       => '',
                    'generic_synonyms'    => array(
                        array(
                            'new'     => 'generic-s-cs-creation-01',
                            'country' => array('US', 'CA', 'MY'),
                        ),
                        array(
                            'new'     => 'generic-s-cs-creation-02',
                            'country' => array('BN', 'BY'),
                        ),
                        array(
                            'new'     => 'generic-s-cs-creation-03',
                            'country' => array('PT', 'QA'),
                        ),
                    ),
                    'generic_typos'       => array(
                        array(
                            'new'     => 'generic-t-cs-creation-01',
                        ),
                    ),
                ),
                'relate' => false,
                'remove' => '5 Records Removed, meanwhile 0 detached.',
            ),
            // #2
            array(
                'type'    => 'brand',
                'request' => array(
                    'name'                => 'cs-test-create-brand',
                    'valid'               => 0,
                    'comments'            => 'this-is-a-test-for-cs-service-on-drug-creation-stack',
                    'unid'                => 'WB2VSRCK4XRU0T45SCYSF952CNRU7873',
                    'creation_date'       => '',
                    'auto_encode_explude' => 1,
                    'brand_synonyms'      => array(
                        array(
                            'new'     => 'brand-s-cs-creation-01',
                            'country' => array('RO'),
                        ),
                    ),
                    'brand_typos'         => array(
                        array(
                            'new'     => 'brand-t-cs-creation-01',
                        ),
                        array(
                            'new'     => 'brand-t-cs-creation-02',
                        ),
                        array(
                            'new'     => 'brand-t-cs-creation-03',
                        ),
                    ),
                ),
                'relate' => false,
                'remove' => '5 Records Removed, meanwhile 0 detached.',
            ),
            // #3
            array(
                'type'    => 'generic',
                'request' => array(
                    'name'                => 'cs-test-create-generic-02',
                    'valid'               => 1,
                    'comments'            => 'this-is-a-test-for-cs-service-on-drug-creation-stack',
                    'level_1'             => 'AB',
                    'level_2'             => 'CD',
                    'level_3'             => 'EF',
                    'level_4'             => 'GH',
                    'level_5'             => 'IJ',
                    'unid'                => 'R8826I204LT1FVJ3RRFJF8RXWPCQQPXQ',
                    'auto_encode_explude' => 1,
                    'no_double_bounce'    => 0,
                    'creation_date'       => '',
                    'generic_synonyms'    => array(),
                    'generic_typos'       => array(),
                ),
                'relate' => true,
                'remove' => '1 Record Removed, meanwhile 1 detached.',
            ),
            // #4
            array(
                'type'    => 'brand',
                'request' => array(
                    'name'                => 'cs-test-create-brand-02',
                    'valid'               => 1,
                    'comments'            => 'this-is-a-test-for-cs-service-on-drug-creation-stack',
                    'unid'                => '2KV2Z8NDGPU8HEEWEP56XY4WF5HUM122',
                    'creation_date'       => '2018-06-05 13:02:03',
                    'auto_encode_explude' => 0,
                    'brand_synonyms'      => array(),
                    'brand_typos'         => array(),
                ),
                'relate' => true,
                'remove' => '1 Record Removed, meanwhile 1 detached.',
            ),
        );
    }

    /**
     * @testdox Checking Trigger Command
     */
    public function testTriggerCommandProcess()
    {
        $proc = $this->cs->triggerCommandProcess('debug:container', 0);

        $this->assertArrayHasKey('STAT', $proc);
        $this->assertArrayHasKey('COMMAND', $proc);

        $this->assertStringContainsString('/php', $proc['COMMAND']);
        $this->assertStringContainsString('/bin/console', $proc['COMMAND']);
        $this->assertStringContainsString('debug:container', $proc['COMMAND']);
    }

    /**
     * @testdox Checking Drugs Advance Deletion
     */
    public function testDeleteDrugAdvance()
    {
        $generic = $this->em->getRepository(Generic::class)->getDrug('test-1', 'name');
        list($status, $msg, $removed, $untied) = $this->cs->deleteDrug('generic', $generic->getId());
        $this->assertTrue($status);
        $this->assertSame('6 Records Removed, meanwhile 2 detached.', $msg);
        $this->assertEquals(6, $removed);
        $this->assertEquals(2, $untied);

        $brand = $this->em->getRepository(Brand::class)->getDrug('test-1', 'name');
        list($msg, $msg, $removed, $untied) = $this->cs->deleteDrug('brand', $brand->getId());
        $this->assertTrue($status);
        $this->assertSame('6 Records Removed, meanwhile 1 detached.', $msg);
        $this->assertEquals(6, $removed);
        $this->assertEquals(1, $untied);
    }
}
