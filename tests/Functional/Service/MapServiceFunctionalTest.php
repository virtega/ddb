<?php

namespace App\Tests\Functional\Service;

use App\Entity\Experimental;
use App\Service\MapService;
use App\Tests\Fixtures\Drug\ExperimentalDrugRepoFixtures;
use App\Tests\Fixtures\Drug\GenericDrugRepoFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox FUNCTIONAL | Service | Map Service
 */
class MapServiceFunctionalTest extends WebTestCase
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $entityManager;
    private $em;

    /**
     * @var MapService
     */
    private $map;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        self::$entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $loader = new Loader();
        $loader->addFixture(new ExperimentalDrugRepoFixtures());
        $loader->addFixture(new GenericDrugRepoFixtures());

        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        $purger   = new ORMPurger(self::$entityManager, \App\Tests\Functional\BaseFunctionalTest::$ormPurgerExcludes);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->purge();

        self::$entityManager->close();
        self::$entityManager = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->em = $client->getContainer()->get('doctrine')->getManager();
        $this->map = $client->getContainer()->get('test.' . MapService::class);

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->em->close();
        $this->em = null;
        $this->map = null;

        parent::tearDown();
    }

    /**
     * @testdox Checking getDrugFamilyForMap(): Single Tree
     */
    public function testGetDrugFamilyForMapSingleTreee()
    {
        $repo = $this->em->getRepository(Experimental::class);
        $drug = $repo->getDrug('test-3', 'name');

        $results = $this->map->getDrugFamilyForMap('experimental', $drug->getId(), array());

        $expects = array(
            'operators' => array(
                'e_' . $drug->getId() => array(
                    'top'        => 20,
                    'left'       => 20,
                    'properties' => array(
                        'title'   => 'E: ' . $drug->getName(),
                        'class'   => 'experimental',
                        'inputs'  => null,
                        'outputs' => array(
                            'o_e_' . $drug->getId() => array(
                                'label'    => 'G: 0',
                                'multiple' => false,
                            ),
                        ),
                    ),
                    'drug' => array(
                        'id'         => $drug->getId(),
                        'name'       => $drug->getName(),
                        'type'       => 'experimental',
                    ),
                ),
            ),
            'links' => array(),
        );

        $this->assertSame($expects, $results);
    }

    /**
     * @testdox Checking getDrugFamilyForMap(): 2 Tree
     */
    public function testGetDrugFamilyForMapTwoTree()
    {
        $repo = $this->em->getRepository(Experimental::class);
        $drug = $repo->getDrug('test-1', 'name');

        $results = $this->map->getDrugFamilyForMap('experimental', $drug->getId(), array());

        $family = $repo->queryDrugFamily('experimental', $drug->getId(), false, true);

        $expects = array(
            'operators' => array(
                'e_' . $family['experimental'][0]['main']->getId() => array(
                    'top'        => 20,
                    'left'       => 20,
                    'properties' => array(
                        'title'   => 'E: ' . $family['experimental'][0]['main']->getName(),
                        'class'   => 'experimental',
                        'inputs'  => null,
                        'outputs' => array(
                            'o_e_' . $family['experimental'][0]['main']->getId() => array(
                                'label'    => 'G: 1',
                                'multiple' => false,
                            ),
                        ),
                    ),
                    'drug' => array(
                        'id'         => $family['experimental'][0]['main']->getId(),
                        'name'       => $family['experimental'][0]['main']->getName(),
                        'type'       => 'experimental',
                    ),
                ),
                'g_' . $family['generic'][0]['main']->getId() => array(
                    'top'        => 20,
                    'left'       => 330,
                    'properties' => array(
                        'title'  => 'G: ' . $family['generic'][0]['main']->getName(),
                        'class'  => 'generic',
                        'inputs' => array(
                            'i_g_' . $family['generic'][0]['main']->getId() => array(
                                'label'    => 'E: 1',
                                'multiple' => false,
                            ),
                        ),
                        'outputs' => array(
                            'o_g_' . $family['generic'][0]['main']->getId() => array(
                                'label'    => 'B: 0',
                                'multiple' => false,
                            ),
                        ),
                    ),
                    'drug' => array(
                        'id'         => $family['generic'][0]['main']->getId(),
                        'name'       => $family['generic'][0]['main']->getName(),
                        'type'       => 'generic',
                    ),
                ),
            ),
            'links' => array(
                'e_g_' . $family['experimental'][0]['main']->getId() . '_' . $family['generic'][0]['main']->getId() => array(
                    'fromOperator'  => 'e_' . $family['experimental'][0]['main']->getId(),
                    'fromConnector' => 'o_e_' . $family['experimental'][0]['main']->getId(),
                    'toOperator'    => 'g_' . $family['generic'][0]['main']->getId(),
                    'toConnector'   => 'i_g_' . $family['generic'][0]['main']->getId(),
                ),
            ),
        );

        $this->assertSame($expects, $results);
    }

    /**
     * @testdox Checking getDrugFamilyForMap(): Full Tree
     */
    public function testGetDrugFamilyForMapFullTree()
    {
        list($drug, $map) = $this->getMockFullMap();

        $results = $this->map->getDrugFamilyForMap('experimental', $drug->getId(), array());

        $this->assertSame($map, $results);

        return $results;
    }

    /**
     * @depends testGetDrugFamilyForMapFullTree
     *
     * @testdox Checking getDrugFamilyForMap(): Full Tree Mapped
     */
    public function testGetDrugFamilyForMapFullTreeMapped($fromPrevious)
    {
        $repo = $this->em->getRepository(Experimental::class);
        $drug = $repo->getDrug('test-downlink', 'name');

        $mapped = array(
            'experimental' => 1,
            'generic'      => 1,
            'brand'        => 1,
        );

        $results = $this->map->getDrugFamilyForMap('experimental', $drug->getId(), $mapped);

        foreach ($fromPrevious['operators'] as $key => $operator) {
            $operator['top']                 = 100;
            $fromPrevious['operators'][$key] = $operator;
        }

        $this->assertSame($fromPrevious, $results);

        return $results;
    }

    /**
     * @dataProvider getInvalidMapFormat
     *
     * @testdox Checking updateDrugRelations(); with invalid map format, stacks
     */
    public function testUpdateDrugFamilyForMapWithWrongFormat($operators, $links)
    {
        try {
            $results = $this->map->updateDrugRelations($operators, $links);
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Invalid "operators" Format', $e->getMessage());
        }
    }

    public function getInvalidMapFormat()
    {
        return array(
            // 0
            array(
                'operators' => array(
                    'any_id' => array(
                        'top'  => 0,
                        'left' => 0,
                        'no-properties' => array(
                        ),
                        'no-drug'       => array(
                        ),
                    ),
                ),
                'links'     => array(),
            ),
            // 1
            array(
                'operators' => array(
                    'any_id' => array(
                        'top'  => 0,
                        'left' => 0,
                        'properties' => array(
                            'title'   => 'any-title',
                            'class'   => 'experimental',
                            'inputs'  => null,
                            'outputs' => array(
                                'any_id' => array(
                                    'label'    => 'G: (:i)',
                                    'multiple' => false,
                                ),
                            ),
                        ),
                        'drug'       => array(
                        ),
                    ),
                ),
                'links'     => array(),
            ),
            // 2
            array(
                'operators' => array(
                    'any_id' => array(
                        'top'  => 0,
                        'left' => 0,
                        'properties' => array(
                        ),
                        'drug'       => array(
                            'id'         => 0,
                            'name'       => 'drug-name',
                            'type'       => 'experimental',
                        ),
                    ),
                ),
                'links'     => array(),
            ),
        );
    }

    /**
     * @testdox Checking updateDrugRelations(); with valid map format
     */
    public function testUpdateDrugFamilyForMap()
    {
        list($drug, $map) = $this->getMockFullMap();
        $operators = $map['operators'];
        $links     = $map['links'];

        // with no relation
        $oprem = $operators;
        foreach ($oprem as $key => $set) {
            $set['properties']['inputs']  = null;
            $set['properties']['outputs'] = null;
            $oprem[$key] = $set;
        }
        $results = $this->map->updateDrugRelations($oprem, array());
        $this->assertEquals(2, $results);

        // with relation
        $results = $this->map->updateDrugRelations($operators, $links);
        $this->assertEquals(2, $results);
    }

    private function getMockFullMap()
    {
        $repo = $this->em->getRepository(Experimental::class);
        $drug = $repo->getDrug('test-downlink', 'name');

        $family = $repo->queryDrugFamily('experimental', $drug->getId(), false, true);

        return array(
            $drug,
            array(
                'operators' => array(
                    'e_' . $family['experimental'][0]['main']->getId() => array(
                        'top'        => 20,
                        'left'       => 20,
                        'properties' => array(
                            'title'   => 'E: ' . $family['experimental'][0]['main']->getName(),
                            'class'   => 'experimental',
                            'inputs'  => null,
                            'outputs' => array(
                                'o_e_' . $family['experimental'][0]['main']->getId() => array(
                                    'label'    => 'G: 1',
                                    'multiple' => false,
                                ),
                            ),
                        ),
                        'drug' => array(
                            'id'         => $family['experimental'][0]['main']->getId(),
                            'name'       => $family['experimental'][0]['main']->getName(),
                            'type'       => 'experimental',
                        ),
                    ),
                    'g_' . $family['generic'][0]['main']->getId() => array(
                        'top'        => 20,
                        'left'       => 330,
                        'properties' => array(
                            'title'  => 'G: ' . $family['generic'][0]['main']->getName(),
                            'class'  => 'generic',
                            'inputs' => array(
                                'i_g_' . $family['generic'][0]['main']->getId() => array(
                                    'label'    => 'E: 1',
                                    'multiple' => false,
                                ),
                            ),
                            'outputs' => array(
                                'o_g_' . $family['generic'][0]['main']->getId() => array(
                                    'label'    => 'B: 1',
                                    'multiple' => false,
                                ),
                            ),
                        ),
                        'drug' => array(
                            'id'         => $family['generic'][0]['main']->getId(),
                            'name'       => $family['generic'][0]['main']->getName(),
                            'type'       => 'generic',
                        ),
                    ),
                    'b_' . $family['brand'][0]['main']->getId() => array(
                        'top'        => 20,
                        'left'       => 660,
                        'properties' => array(
                            'title'  => 'B: ' . $family['brand'][0]['main']->getName(),
                            'class'  => 'brand',
                            'inputs' => array(
                                'i_b_' . $family['brand'][0]['main']->getId() => array(
                                    'label'    => 'G: 1',
                                    'multiple' => false,
                                ),
                            ),
                            'outputs' => null,
                        ),
                        'drug' => array(
                            'id'         => $family['brand'][0]['main']->getId(),
                            'name'       => $family['brand'][0]['main']->getName(),
                            'type'       => 'brand',
                        ),
                    ),
                ),
                'links' => array(
                    'e_g_' . $family['experimental'][0]['main']->getId() . '_' . $family['generic'][0]['main']->getId() => array(
                        'fromOperator'  => 'e_' . $family['experimental'][0]['main']->getId(),
                        'fromConnector' => 'o_e_' . $family['experimental'][0]['main']->getId(),
                        'toOperator'    => 'g_' . $family['generic'][0]['main']->getId(),
                        'toConnector'   => 'i_g_' . $family['generic'][0]['main']->getId(),
                    ),
                    'g_b_' . $family['generic'][0]['main']->getId() . '_' . $family['brand'][0]['main']->getId() => array(
                        'fromOperator'  => 'g_' . $family['generic'][0]['main']->getId(),
                        'fromConnector' => 'o_g_' . $family['generic'][0]['main']->getId(),
                        'toOperator'    => 'b_' . $family['brand'][0]['main']->getId(),
                        'toConnector'   => 'i_b_' . $family['brand'][0]['main']->getId(),
                    ),
                ),
            ),
        );
    }
}
