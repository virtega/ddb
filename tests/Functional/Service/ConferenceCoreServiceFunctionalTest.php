<?php

namespace App\Tests\Functional\Service;

use App\Service\ConferenceCoreService;
use App\Service\ConferenceSourceService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Security;

/**
 * @testdox FUNCTIONAL | Service | ConferenceCoreService
 */
class ConferenceCoreServiceFunctionalTest extends WebTestCase
{
    /**
     * @var ConferenceCoreService
     */
    private $conferenceCoreService;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->conferenceCoreService = $client->getContainer()->get('test.' . ConferenceCoreService::class);

        parent::setUp();
    }

    /**
     * @testdox Checking Class Initialization
     */
    public function testInit()
    {
        $this->assertInstanceOf(ConferenceCoreService::class, $this->conferenceCoreService);
    }

    /**
     * @testdox Checking getFormDefaults() basic structure
     */
    public function testGetFormDefaults()
    {
        $result = ConferenceCoreService::getFormDefaults();

        $keys = [
            'name',
            'startdate',
            'enddate',
            'city',
            'cityguide',
            'state',
            'country',
            'region',
            'oncruise',
            'online',
            'keycongress',
            'discontinued',
            'specialty',
            'contactnote',
            'contactphone',
            'contactemail',
            'contactfax',
            'hasguide',
            'links',
            'uniquename',
        ];

        $this->assertTrue(is_array($result));
        $this->assertSame(count($keys), count($result));

        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
            $this->assertSame($result[$key], ConferenceCoreService::getFormDefaults($key));
        }
    }

    /**
     * @testdox Checking getFormDefaults() Unknown field
     */
    public function testGetFormDefaultsInvalid()
    {
        try {
            $result = ConferenceCoreService::getFormDefaults('alien-field');
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\Exception::class, $error);
            $this->assertStringContainsString('Unknown field name', $error->getMessage());
            $this->assertStringContainsString('alien-field', $error->getMessage());
        }
    }

    /**
     * @testdox Checking create() with Empty Fields
     */
    public function testCreateEmptyFields()
    {
        $this->prepMockedPropertiesForProcessingEmptyFields(false);

        $fields = [];
        $conference = $this->conferenceCoreService->create($fields);
        $this->conferenceTestEmpty($conference);

        $this->assertSame(101, $conference->getId());

        $now = new \DateTime('now');
        $this->assertInstanceOf(\DateTime::class, $conference->getDetailsMetaCreatedOn());
        $this->assertSame($now->format('Y-m-d H'), $conference->getDetailsMetaCreatedOn()->format('Y-m-d H'));
        $this->assertSame('Unknown', $conference->getDetailsMetaCreatedBy());

        $this->assertNull($conference->getDetailsMetaModifiedOn());
        $this->assertSame(null, $conference->getDetailsMetaModifiedBy());
    }

    /**
     * @testdox Checking update() with Empty Fields
     */
    public function testUpdateEmptyFields()
    {
        $this->prepMockedPropertiesForProcessingEmptyFields(true);

        $conference = new \App\Entity\Conference;
        $reflection = new \ReflectionClass(\App\Entity\Conference::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($conference, 101);

        $fields = [];
        $conference = $this->conferenceCoreService->update($conference, $fields);
        $this->conferenceTestEmpty($conference);

        $this->assertSame(101, $conference->getId());

        $this->assertNull($conference->getDetailsMetaCreatedOn());
        $this->assertSame(null, $conference->getDetailsMetaCreatedBy());

        $now = new \DateTime('now');
        $this->assertInstanceOf(\DateTime::class, $conference->getDetailsMetaModifiedOn());
        $this->assertSame($now->format('Y-m-d H'), $conference->getDetailsMetaModifiedOn()->format('Y-m-d H'));
        $this->assertSame('Unknown', $conference->getDetailsMetaModifiedBy());
    }

    /**
     * @testdox Checking create() with $fields
     */
    public function testCreateWithFields()
    {
        /**
         * PREP Property
         */
        $this->prepMockedPropertiesForProcessingDataFields(false);

        /**
         * PREP Fields
         */
        $fields = $this->getFormData();

        /**
         * EXECUTE
         */
        $conference = $this->conferenceCoreService->create($fields);

        /**
         * CHECKING
         */
        $this->conferenceTestData($conference, false);

        return $conference;
    }

    /**
     * @depends testCreateWithFields
     *
     * @testdox Checking update() with $fields
     */
    public function testUpdateWithFields($conference)
    {
        /**
         * SET Conference
         */
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);

        $specialty1 = new \App\Entity\SpecialtyTaxonomy;
        $reflProp->setValue($specialty1, 20);
        $specialty1->setSpecialty('Oncology');
        $confSpecialy1 = new \App\Entity\ConferenceSpecialties;
        $confSpecialy1->setConference($conference);
        $confSpecialy1->setSpecialty($specialty1);

        $specialty2 = new \App\Entity\SpecialtyTaxonomy;
        $reflProp->setValue($specialty2, 28);
        $specialty2->setSpecialty('Cancer Treatment');
        $confSpecialy2 = new \App\Entity\ConferenceSpecialties;
        $confSpecialy2->setConference($conference);
        $confSpecialy2->setSpecialty($specialty2);

        $specialty3 = new \App\Entity\SpecialtyTaxonomy;
        $reflProp->setValue($specialty3, 31);
        $specialty3->setSpecialty('Hypertension');
        $confSpecialy3 = new \App\Entity\ConferenceSpecialties;
        $confSpecialy3->setConference($conference);
        $confSpecialy3->setSpecialty($specialty3);

        $specialties = new \Doctrine\Common\Collections\ArrayCollection([$confSpecialy1, $confSpecialy2, $confSpecialy3]);

        $reflection = new \ReflectionClass(\App\Entity\Conference::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($conference, 101);

        $reflProp = $reflection->getProperty('specialties');
        $reflProp->setAccessible(true);
        $reflProp->setValue($conference, $specialties);

        /**
         * PREP Property
         */
        $this->prepMockedPropertiesForProcessingDataFields(true);

        /**
         * CHANGE Fields
         */
        $fields = $this->getFormData();
        $fields['enddate']    = '2019-06-25';
        $fields['country']    = 'MY';
        $fields['specialty']  = [20, 28, 'Cardiology'];
        $fields['contactfax'] = ['+60355112486'];

        /**
         * EXECUTE
         */
        $conference = $this->conferenceCoreService->update($conference, $fields);

        /**
         * CHECKING
         */
        $this->conferenceTestData($conference, true);

        return $conference;
    }

    /**
     * @depends testUpdateWithFields
     *
     * @testdox Checking delete()
     */
    public function testDelete($conference)
    {
        /**
         * PREP Property
         */
        $conferenceRemote = $this->createMock(ConferenceSourceService::class);
        $conferenceRemote->expects($this->once())->method('remove')->will($this->returnCallback(function($conference) {
            $this->assertInstanceOf(\App\Entity\Conference::class, $conference);
            $this->assertSame(101, $conference->getId());
            return true;
        }));

        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->at(0))->method('remove')->will($this->returnCallback(function($conferenceSpecialties) {
            $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conferenceSpecialties);
            $this->assertSame('Oncology', $conferenceSpecialties->getSpecialty()->getSpecialty());
            $this->assertSame(20, $conferenceSpecialties->getSpecialty()->getId());
            return true;
        }));

        $em->expects($this->at(1))->method('remove')->will($this->returnCallback(function($conferenceSpecialties) {
            $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conferenceSpecialties);
            $this->assertSame('Cancer Treatment', $conferenceSpecialties->getSpecialty()->getSpecialty());
            $this->assertSame(28, $conferenceSpecialties->getSpecialty()->getId());
            return true;
        }));

        $em->expects($this->at(2))->method('remove')->will($this->returnCallback(function($conferenceSpecialties) {
            $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conferenceSpecialties);
            $this->assertSame('Cardiology', $conferenceSpecialties->getSpecialty()->getSpecialty());
            $this->assertSame(35, $conferenceSpecialties->getSpecialty()->getId());
            return true;
        }));

        $em->expects($this->at(4))->method('remove')->will($this->returnCallback(function($conference) {
            $this->assertInstanceOf(\App\Entity\Conference::class, $conference);
            $this->assertSame(101, $conference->getId());
            return true;
        }));

        $em->expects($this->exactly(1))->method('flush')->will($this->returnValue(true));

        $user = $this->createMock(\App\Security\User\LdapUser::class);
        $user->method('getUsername')->will($this->returnValue('John.Doe'));
        $symfonySecurity = $this->createMock(Security::class);
        $symfonySecurity->method('getUser')->will($this->returnValue($user));

        $reflection = new \ReflectionClass(ConferenceCoreService::class);

        $reflProp = $reflection->getProperty('conferenceRemote');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $conferenceRemote);

        $reflProp = $reflection->getProperty('em');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $em);

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) {
            $this->assertStringContainsStringIgnoringCase('Conference Deleted', $msg);
            $this->assertArrayHasKey('id', $args);
            $this->assertSame(101, $args['id']);
            $this->assertArrayHasKey('user', $args);
            $this->assertSame('John.Doe', $args['user']);
            return true;
        }));
        $reflProp = $reflection->getProperty('logger');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $logger);

        $reflProp = $reflection->getProperty('symfonySecurity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $symfonySecurity);

        /**
         * EXECUTE
         */
        $this->conferenceCoreService->delete($conference);
    }

    /**
     * @testdox Checking create() but missing ID after persist
     */
    public function testCreateButMissingIdAfterPersist()
    {
        $this->prepMockedPropertiesForProcessingEmptyFields(false, false);

        try {
            $fields = [];
            $this->conferenceCoreService->create($fields);
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('Conference entity failed to return new ID', $e->getMessage());
        }
    }

    private function getFormData()
    {
        return [
            'name'         => 'ConferenceCoreServiceUnitTest Test 1',
            'startdate'    => '2019-06-01',
            'enddate'      => '25 Jun 2019', // on purpose
            'city'         => 'Kuala Lumpur',
            'cityguide'    => 'Next to Majestic Hotel',
            'state'        => 'W.P. KL',
            'country'      => 'SG', // on purpose
            'region'       => 203,
            'oncruise'     => 'yes',
            'online'       => 'no',
            'keycongress'  => 'yes',
            'discontinued' => 'no',
            'specialty'    => ['Oncology', 'Cancer Treatment', 'Hypertension'],
            'contactnote'  => 'Event Coordinator - Edward Lim',
            'contactphone' => ['+60123456789', '+60355112485'],
            'contactemail' => ['edward.lim@superdymm.com'],
            'contactfax'   => [],
            'hasguide'     => 'yes',
            'links'        => [
                [
                    'caption' => 'Homepage',
                    'url'     => 'http://superdymm.com/event-ConferenceCoreServiceUnitTest'
                ],
                [
                    'caption' => 'superdymm.com',
                    'url'     => 'http://superdymm.com/contact'
                ],
            ],
            'uniquename'   => 'ConferenceCoreServiceUnitTestABC1234',
        ];
    }

    private function prepMockedPropertiesForProcessingEmptyFields($updated = false, $persistWithId = true)
    {
        $conferenceRemote = $this->createMock(ConferenceSourceService::class);
        if ($persistWithId) {
            $conferenceRemote->expects($this->once())->method('upsync')->will($this->returnCallback(function($conference) {
                $this->assertInstanceOf(\App\Entity\Conference::class, $conference);
                return true;
            }));
        }

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->once())->method('persist')->will($this->returnCallback(function($conference) use ($persistWithId) {
            $this->assertInstanceOf(\App\Entity\Conference::class, $conference);
            if ($persistWithId) {
                $reflection = new \ReflectionClass(\App\Entity\Conference::class);
                $reflProp = $reflection->getProperty('id');
                $reflProp->setAccessible(true);
                $reflProp->setValue($conference, 101);
            }
            return true;
        }));
        $em->expects($this->once())->method('flush')->will($this->returnValue(true));

        $reflection = new \ReflectionClass(ConferenceCoreService::class);

        $reflProp = $reflection->getProperty('conferenceRemote');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $conferenceRemote);

        $reflProp = $reflection->getProperty('em');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $em);

        $logger = $this->createMock(LoggerInterface::class);
        if ($persistWithId) {
            $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) use ($updated) {
                if (!$updated) {
                    $this->assertStringContainsStringIgnoringCase('Conference Created', $msg);
                }
                else {
                    $this->assertStringContainsStringIgnoringCase('Conference Updated', $msg);
                }
                $this->assertArrayHasKey('id', $args);
                $this->assertSame(101, $args['id']);
                $this->assertArrayHasKey('user', $args);
                $this->assertSame('Unknown', $args['user']);
                return true;
            }));
        }

        $reflProp = $reflection->getProperty('logger');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $logger);

        $symfonySecurity = $this->createMock(Security::class);
        $reflProp = $reflection->getProperty('symfonySecurity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $symfonySecurity);
    }

    private function prepMockedPropertiesForProcessingDataFields($update = false)
    {
        $conferenceRemote = $this->createMock(ConferenceSourceService::class);
        $conferenceRemote->expects($this->once())->method('upsync')->will($this->returnCallback(function($conference) {
            $this->assertInstanceOf(\App\Entity\Conference::class, $conference);
            return true;
        }));

        $em = $this->createMock(EntityManagerInterface::class);

        // __getCountryByIsoCod
        if (!$update) {
            $country = [
                'getIsoCode'     => 'SG',
                'getName'        => 'Singapore',
                'getCapitalCity' => 'Singapore',
            ];
        }
        else {
            $country = [
                'getIsoCode'     => 'MY',
                'getName'        => 'Malaysia',
                'getCapitalCity' => 'Kuala Lumpur',
            ];
        }
        $country = $this->createConfiguredMock(\App\Entity\Country::class, $country);
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue($country));
        $em->expects($this->at(0))->method('getRepository')->will($this->returnValue($repo));

        // __getCountryTaxonomyById
        $country = $this->createConfiguredMock(\App\Entity\CountryTaxonomy::class, [
            'getId'     => 203,
            'getName'   => 'Southeast Asia',
            'getParent' => null,
        ]);
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue($country));
        $em->expects($this->at(1))->method('getRepository')->will($this->returnValue($repo));

        // ___searchOrCreateNewSpecialtyTaxonomy #1 - #3
        if (!$update) {
            $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
            $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue(null));
            $repo->expects($this->at(1))->method('findOneBy')->will($this->returnValue(null));
            $em->method('getRepository')->will($this->returnValue($repo));

            // (3 x 2) + 1
            $em->expects($this->exactly(7))->method('persist')->will($this->returnCallback(function($entity) {
                if ($entity instanceof \App\Entity\Conference) {
                    $reflection = new \ReflectionClass(\App\Entity\Conference::class);
                    $reflProp = $reflection->getProperty('id');
                    $reflProp->setAccessible(true);
                    $reflProp->setValue($entity, 101);
                }
                return true;
            }));
            $em->expects($this->exactly(1))->method('flush')->will($this->returnValue(true));
        }
        else {
            $em->expects($this->exactly(1))->method('remove')->will($this->returnCallback(function($conferenceSpecialties) {
                $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conferenceSpecialties);
                $this->assertSame('Hypertension', $conferenceSpecialties->getSpecialty()->getSpecialty());
                $this->assertSame(31, $conferenceSpecialties->getSpecialty()->getId());
                return true;
            }));

            $specialtyTax2 = $this->createConfiguredMock(\App\Entity\SpecialtyTaxonomy::class, [
                'getId'        => 35,
                'getSpecialty' => 'Cardiology',
            ]);
            $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
            $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue(null));
            $repo->expects($this->at(1))->method('findOneBy')->will($this->returnValue($specialtyTax2));
            $em->method('getRepository')->will($this->returnValue($repo));

            // (1) + 1
            $em->expects($this->exactly(2))->method('persist')->will($this->returnValue(true));
            $em->expects($this->exactly(1))->method('flush')->will($this->returnValue(true));
        }

        // __getCurrentUserUsername
        $user = $this->createMock(\App\Security\User\LdapUser::class);
        $user->method('getUsername')->will($this->returnValue('John.Doe'));
        $symfonySecurity = $this->createMock(Security::class);
        $symfonySecurity->method('getUser')->will($this->returnValue($user));

        $reflection = new \ReflectionClass(ConferenceCoreService::class);

        $reflProp = $reflection->getProperty('conferenceRemote');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $conferenceRemote);

        $reflProp = $reflection->getProperty('em');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $em);

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) use ($update) {
            if (!$update) {
                $this->assertStringContainsStringIgnoringCase('Conference Created', $msg);
            }
            else {
                $this->assertStringContainsStringIgnoringCase('Conference Updated', $msg);
            }
            $this->assertArrayHasKey('id', $args);
            $this->assertSame(101, $args['id']);
            $this->assertArrayHasKey('user', $args);
            $this->assertSame('John.Doe', $args['user']);
            return true;
        }));
        $reflProp = $reflection->getProperty('logger');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $logger);

        $reflProp = $reflection->getProperty('symfonySecurity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->conferenceCoreService, $symfonySecurity);
    }

    private function conferenceTestEmpty($conference)
    {
        $this->assertInstanceOf(\App\Entity\Conference::class, $conference);
        $this->assertNull($conference->getStartDate());
        $this->assertNull($conference->getEndDate());
        $this->assertNull($conference->getCity());
        $this->assertNull($conference->getCityGuide());
        $this->assertNull($conference->getCountry());
        $this->assertNull($conference->getRegion());
        $this->assertNull($conference->getPreviousId());
        $this->assertNull($conference->getUniqueName());

        $this->assertSame(0, $conference->getCruise());
        $this->assertFalse($conference->isCruise());
        $this->assertSame(0, $conference->getOnline());
        $this->assertFalse($conference->isOnline());
        $this->assertSame(0, $conference->getKeyEvent());
        $this->assertFalse($conference->isKeyEvent());
        $this->assertSame(0, $conference->getHasGuide());
        $this->assertFalse($conference->isHasGuide());
        $this->assertSame(0, $conference->getDiscontinued());
        $this->assertFalse($conference->isDiscontinued());

        $this->assertInstanceOf(\Doctrine\Common\Collections\ArrayCollection::class, $conference->getSpecialties());
        $this->assertSame(0, $conference->getSpecialties()->count());

        $this->assertSame('', $conference->getDetailsState());
        $this->assertSame('', $conference->getDetailsContactDesc());
        $this->assertSame([], $conference->getDetailsContactPhones());
        $this->assertSame([], $conference->getDetailsContactFaxes());
        $this->assertSame([], $conference->getDetailsContactEmails());
        $this->assertSame([], $conference->getDetailsContactLinks());
    }

    private function conferenceTestData($conference, $update = false)
    {
        $this->assertInstanceOf(\App\Entity\Conference::class, $conference);

        $this->assertSame(101, $conference->getId());

        $this->assertSame('ConferenceCoreServiceUnitTest Test 1', $conference->getName());

        $this->assertInstanceOf(\DateTime::class, $conference->getStartDate());
        $this->assertSame('2019-06-01', $conference->getStartDate()->format('Y-m-d'));

        if (!$update) {
            $this->assertNull($conference->getEndDate());
        }
        else {
            $this->assertInstanceOf(\DateTime::class, $conference->getEndDate());
            $this->assertSame('2019-06-25', $conference->getEndDate()->format('Y-m-d'));
        }

        $this->assertSame('Kuala Lumpur', $conference->getCity());
        $this->assertSame('Next to Majestic Hotel', $conference->getCityGuide());

        if (!$update) {
            $this->assertSame('SG', $conference->getCountry()->getIsoCode());
            $this->assertSame('Singapore', $conference->getCountry()->getName());
        }
        else {
            $this->assertSame('MY', $conference->getCountry()->getIsoCode());
            $this->assertSame('Malaysia', $conference->getCountry()->getName());
        }

        $this->assertSame('Southeast Asia', $conference->getRegion()->getName());

        $this->assertNull($conference->getPreviousId());
        $this->assertSame('ConferenceCoreServiceUnitTestABC1234', $conference->getUniqueName());

        $this->assertSame(1, $conference->getCruise());
        $this->assertTrue($conference->isCruise());

        $this->assertSame(0, $conference->getOnline());
        $this->assertFalse($conference->isOnline());

        $this->assertSame(1, $conference->getKeyEvent());
        $this->assertTrue($conference->isKeyEvent());

        $this->assertSame(1, $conference->getHasGuide());
        $this->assertTrue($conference->isHasGuide());

        $this->assertSame(0, $conference->getDiscontinued());
        $this->assertFalse($conference->isDiscontinued());

        $this->assertInstanceOf(\Doctrine\Common\Collections\ArrayCollection::class, $conference->getSpecialties());
        $this->assertSame(3, $conference->getSpecialties()->count());

        if (!$update) {
            $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conference->getSpecialties()->toArray()[0]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,     $conference->getSpecialties()->toArray()[0]->getSpecialty());
            $this->assertSame('Oncology',                                     $conference->getSpecialties()->toArray()[0]->getSpecialty()->getSpecialty());

            $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conference->getSpecialties()->toArray()[1]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,     $conference->getSpecialties()->toArray()[1]->getSpecialty());
            $this->assertSame('Cancer Treatment',                             $conference->getSpecialties()->toArray()[1]->getSpecialty()->getSpecialty());

            $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conference->getSpecialties()->toArray()[2]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,     $conference->getSpecialties()->toArray()[2]->getSpecialty());
            $this->assertSame('Hypertension',                                 $conference->getSpecialties()->toArray()[2]->getSpecialty()->getSpecialty());
        }
        else {
            $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conference->getSpecialties()->toArray()[0]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,     $conference->getSpecialties()->toArray()[0]->getSpecialty());
            $this->assertSame('Oncology',                                     $conference->getSpecialties()->toArray()[0]->getSpecialty()->getSpecialty());
            $this->assertSame(20,                                             $conference->getSpecialties()->toArray()[0]->getSpecialty()->getId());

            $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conference->getSpecialties()->toArray()[1]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,     $conference->getSpecialties()->toArray()[1]->getSpecialty());
            $this->assertSame('Cancer Treatment',                             $conference->getSpecialties()->toArray()[1]->getSpecialty()->getSpecialty());
            $this->assertSame(28,                                             $conference->getSpecialties()->toArray()[1]->getSpecialty()->getId());

            $this->assertInstanceOf(\App\Entity\ConferenceSpecialties::class, $conference->getSpecialties()->toArray()[3]);
            $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class,     $conference->getSpecialties()->toArray()[3]->getSpecialty());
            $this->assertSame('Cardiology',                                   $conference->getSpecialties()->toArray()[3]->getSpecialty()->getSpecialty());
            $this->assertSame(35,                                             $conference->getSpecialties()->toArray()[3]->getSpecialty()->getId());
        }

        $this->assertSame('W.P. KL', $conference->getDetailsState());
        $this->assertSame('Event Coordinator - Edward Lim', $conference->getDetailsContactDesc());

        $this->assertSame(['+60123456789', '+60355112485'], $conference->getDetailsContactPhones());

        if (!$update) {
            $this->assertSame([], $conference->getDetailsContactFaxes());
        }
        else {
            $this->assertSame(['+60355112486'], $conference->getDetailsContactFaxes());
        }

        $this->assertSame(['edward.lim@superdymm.com'], $conference->getDetailsContactEmails());

        $this->assertSame([
            [
                'caption' => 'Homepage',
                'url'     => 'http://superdymm.com/event-ConferenceCoreServiceUnitTest'
            ],
            [
                'caption' => 'superdymm.com',
                'url'     => 'http://superdymm.com/contact'
            ],
        ], $conference->getDetailsContactLinks());

        $now = new \DateTime('now');
        $this->assertInstanceOf(\DateTime::class, $conference->getDetailsMetaCreatedOn());
        $this->assertSame($now->format('Y-m-d H'), $conference->getDetailsMetaCreatedOn()->format('Y-m-d H'));
        $this->assertSame('John.Doe', $conference->getDetailsMetaCreatedBy());

        if (!$update) {
            $this->assertNull($conference->getDetailsMetaModifiedOn());
            $this->assertSame(null, $conference->getDetailsMetaModifiedBy());
        }
        else {
            $now = new \DateTime('now');
            $this->assertInstanceOf(\DateTime::class, $conference->getDetailsMetaModifiedOn());
            $this->assertSame($now->format('Y-m-d H'), $conference->getDetailsMetaModifiedOn()->format('Y-m-d H'));
            $this->assertSame('John.Doe', $conference->getDetailsMetaModifiedBy());
        }
    }
}
