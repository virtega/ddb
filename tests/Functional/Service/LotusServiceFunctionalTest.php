<?php

namespace App\Tests\Functional\Service;

use App\Entity\Brand;
use App\Entity\Experimental;
use App\Entity\Generic;
use App\Service\LotusService;
use App\Utility\Helpers as H;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox FUNCTIONAL | Service | Lotus Service
 */
class LotusServiceFunctionalTest extends WebTestCase
{
    /**
     * @var LotusService
     */
    private $ls;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->root = $client->getContainer()->get('kernel')->getProjectDir();

        $this->ls = $client->getContainer()->get('test.' . LotusService::class);

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->ls = null;

        parent::tearDown();
    }

    /**
     * @testdox Check private Update drug Keyword SQL
     */
    public function testPrivateUpdateKeywordSQL()
    {
        $res = $this->invokeLotusServiceMethod('_getKeywordUpdateSql', array(
            'typo',
            'experimental',
            10,
            array(),
            array(),
        ));

        $exp = 'DELETE FROM ExperimentalTypos WHERE ExperimentalID = 10;';
        $exp = H::removeExcessWhitespace($exp);
        $this->assertSame($exp, $res);

        $res = $this->invokeLotusServiceMethod('_getKeywordUpdateSql', array(
            'typo',
            'experimental',
            10,
            array(
                array(
                    'name' => 'test-a',
                ),
                array(
                    'name' => 'test-b',
                ),
            ),
            array(
                array(
                    'name' => 'test-c',
                ),
            ),
        ));

        $exp = 'DELETE FROM ExperimentalTypos WHERE ExperimentalID = 10;
            INSERT INTO ExperimentalTypos ( ExperimentalID, Typo )
            SELECT ExperimentalID, Typo FROM (
                SELECT 10, "test-a"
                UNION ALL SELECT 10, "test-b"
                UNION ALL SELECT 10, "test-c"
            ) AS temp (ExperimentalID, Typo);
        ';
        $exp = H::removeExcessWhitespace($exp);
        $this->assertSame($exp, $res);
    }

    /**
     * @dataProvider getPrivateUpdateRelationDrugSQL
     *
     * @testdox Check private Update drug Relation SQL
     */
    public function testPrivateUpdateRelationDrugSQL($set, $exp)
    {
        $res = $this->invokeLotusServiceMethod('_getDrugRelationSql', $set);
        $this->assertSame($exp, $res);
    }

    public function getPrivateUpdateRelationDrugSQL()
    {
        return array(
            array(
                array(true, 'experimental', 10, 'generic'),
                'IF NOT EXISTS (SELECT * FROM ExperimentalGeneric WHERE ExperimentalID = 10) INSERT INTO ExperimentalGeneric ( ExperimentalID, GenericID ) VALUES ( 10, 0 )',
            ),
            array(
                array(true, 'experimental', 10, 'generic', 11),
                'IF NOT EXISTS (SELECT * FROM ExperimentalGeneric WHERE ExperimentalID = 10 AND GenericID = 11) INSERT INTO ExperimentalGeneric ( ExperimentalID, GenericID ) VALUES ( 10, 11 )',
            ),
            array(
                array(true, 'generic', 11, 'brand', 12),
                'IF NOT EXISTS (SELECT * FROM GenericBrand WHERE GenericID = 11 AND BrandID = 12) INSERT INTO GenericBrand ( GenericID, BrandID ) VALUES ( 11, 12 )',
            ),
            array(
                array(false, 'experimental', 10, 'generic'),
                'DELETE FROM ExperimentalGeneric WHERE ExperimentalID = 10',
            ),
            array(
                array(false, 'experimental', 10, 'generic', 11),
                'DELETE FROM ExperimentalGeneric WHERE ExperimentalID = 10 AND GenericID = 11',
            ),
            array(
                array(false, 'generic', 11, 'brand', 12),
                'DELETE FROM GenericBrand WHERE GenericID = 11 AND BrandID = 12',
            ),
        );
    }

    /**
     * @dataProvider gettPrivateGetDeleteDrugSQL
     *
     * @testdox Check private get Delete drug SQL
     */
    public function testPrivateGetDeleteDrugSQL($set, $exp)
    {
        $res = $this->invokeLotusServiceMethod('_getDeleteDrugSql', $set);
        $this->assertSame($exp, $res);
    }

    public function gettPrivateGetDeleteDrugSQL()
    {
        return array(
            array(
                array('experimental', 10),
                'DELETE FROM Experimental WHERE ExperimentalID = 10',
            ),
            array(
                array('generic', 10),
                implode('; ', array(
                    'DELETE FROM GenericSynonyms WHERE GenericID = 10',
                    'DELETE FROM GenericTypos WHERE GenericID = 10',
                    'DELETE FROM BrandGeneric WHERE GenericID = 10',
                    'DELETE FROM Generic WHERE GenericID = 10',
                )),
            ),
            array(
                array('brand', 10),
                implode('; ', array(
                    'DELETE FROM BrandSynonyms WHERE BrandID = 10',
                    'DELETE FROM BrandTypos WHERE BrandID = 10',
                    'DELETE FROM BrandGeneric WHERE BrandID = 10',
                    'DELETE FROM Brand WHERE BrandID = 10',
                )),
            ),
        );
    }

    private function invokeLotusServiceMethod($methodName, array $parameters = array(), $statementSql = false)
    {
        $pdo = $this->createMock(\PDO::class);
        $pdo->method('quote')->will($this->returnCallback(function(string $string, $parameter_type = \PDO::PARAM_STR) {
            return '"' . addslashes($string) . '"';
        }));


        if ($statementSql) {
            $sttmt = $this->createMock(\PDOStatement::class);
            $sttmt->method('bindParam')->willReturn(true);
            $pdo->method('prepare')->will($this->returnCallback(function(string $statement, $driver_options = array()) use (&$sttmt) {
                $sttmt->argSqlString = $statement;
                return $sttmt;
            }));
        }

        $reflection = new \ReflectionClass(get_class($this->ls));

        $lotusProperty = $reflection->getProperty('lotus');
        $lotusProperty->setAccessible(true);
        $lotusProperty->setValue($this->ls, $pdo);

        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($this->ls, $parameters);
    }

    private function createMockDrug($entity, $mockedId)
    {
        $class = 'App\Entity\\' . ucwords($entity);
        $drug = new $class;

        $reflection = new \ReflectionClass($drug);
        $id = $reflection->getProperty('id');
        $id->setAccessible(true);
        $id->setValue($drug, $mockedId);

        if ($entity != 'brand') {
            $drug->setName('test-subject');
        }

        $drug->setValid(0);

        if ($entity != 'generic') {
            $drug->setUid('str-' . time());
        }

        return $drug;
    }
}
