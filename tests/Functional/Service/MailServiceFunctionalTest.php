<?php

namespace App\Tests\Functional\Service;

use App\Service\MailService;
use App\Tests\Functional\BaseFunctionalTest;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox FUNCTIONAL | Service | Mail Service
 */
class MailServiceFunctionalTest extends WebTestCase
{
    /**
     * @var MailService
     */
    private $ms;

    /**
     * @var string Site Log Path
     */
    private $logPath;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->logPath = $client->getContainer()->get('kernel')->getLogDir();

        $this->ms = $client->getContainer()->get('test.' . MailService::class);

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->ms = null;

        parent::tearDown();
    }

    /**
     * @testdox Check to Sent Basic Email
     */
    public function testSentBasicEmail()
    {
        if (!self::isServiceSetup()) {
            $this->markTestSkipped('Skip as Mail is not Setup.');
            return;
        }

        /**
         * Basic template & prefix
         */
        $this->assertEquals('email/lined.html.twig', $this->ms->getTemplate());

        $result = $this->ms->sentEmail(
            $this->getEmail(),
            'Test Subject 1',
            'Test Content'
        );
        $this->assertNotEmpty($result);

        $log = $this->getLogFileTail();
        $this->assertNotEmpty($log);
        $this->assertEquals(1, $log['count']);
        $this->assertSame($this->getLoggedEmail(), $log['data']['to']);
        $this->assertStringContainsStringIgnoringCase('[DrugsDb]', $log['data']['subject']);
        $this->assertStringContainsStringIgnoringCase('Test Subject 1', $log['data']['subject']);
        $this->assertEquals(0, $log['data']['attachment']);
        $this->assertEquals(0, $log['data']['cc']);
        $this->assertEquals(0, $log['data']['bcc']);

        /**
         * Defined template & prefix
         */
        $result = $this->ms
            ->setTemplate('email/boxed.html.twig')
            ->setSubjectPrefix('[testing-prefix]')
            ->sentEmail(
                $this->getEmail(),
                'Test Subject 1',
                [
                    'heading'            => 'Test Content',
                    'greeting'           => 'Howdy,',
                    'content_top'        => 'Content TOP here..',
                    'pre_content_before' => 'Pre Content PRE',
                    'pre_content'        => 'Pre Content Line 1' . PHP_EOL . 'Pre Content Line 2',
                    'pre_content_after'  => 'Pre Content POST',
                    'content_bottom'     => 'Content BOTTOM here..',
                ]
            );
        $this->assertNotEmpty($result);

        $log = $this->getLogFileTail();
        $this->assertNotEmpty($log);
        $this->assertEquals(1, $log['count']);
        $this->assertSame($this->getLoggedEmail(), $log['data']['to']);
        $this->assertStringNotContainsStringIgnoringCase('[DrugsDb]', $log['data']['subject']);
        $this->assertStringContainsStringIgnoringCase('[testing-prefix]', $log['data']['subject']);
        $this->assertStringContainsStringIgnoringCase('Test Subject 1', $log['data']['subject']);
        $this->assertEquals(0, $log['data']['attachment']);
        $this->assertEquals(0, $log['data']['cc']);
        $this->assertEquals(0, $log['data']['bcc']);
    }

    /**
     * @testdox Check to Sent Email with CC
     */
    public function testSentBasicEmailWithCc()
    {
        if (!self::isServiceSetup()) {
            $this->markTestSkipped('Skip as Mail is not Setup.');
            return;
        }

        $result = $this->ms->sentEmail(
            $this->getEmail(),
            'Test Subject 2',
            'Test Content',
            array(),
            $this->getEmail('two')
        );
        $this->assertNotEmpty($result);

        $log = $this->getLogFileTail();
        $this->assertNotEmpty($log);
        $this->assertEquals(1, $log['count']);
        $this->assertSame($this->getLoggedEmail(), $log['data']['to']);
        $this->assertStringContainsStringIgnoringCase('Test Subject 2', $log['data']['subject']);
        $this->assertEquals(0, $log['data']['attachment']);
        $this->assertEquals(1, $log['data']['cc']);
        $this->assertEquals(0, $log['data']['bcc']);
    }

    /**
     * @testdox Check to Sent Email with BCC
     */
    public function testSentBasicEmailWithBcc()
    {
        if (!self::isServiceSetup()) {
            $this->markTestSkipped('Skip as Mail is not Setup.');
            return;
        }

        $result = $this->ms->sentEmail(
            $this->getEmail(),
            'Test Subject 3',
            'Test Content',
            array(),
            null,
            $this->getEmail('four')
        );
        $this->assertNotEmpty($result);

        $log = $this->getLogFileTail();
        $this->assertNotEmpty($log);
        $this->assertEquals(1, $log['count']);
        $this->assertSame($this->getLoggedEmail(), $log['data']['to']);
        $this->assertStringContainsStringIgnoringCase('Test Subject 3', $log['data']['subject']);
        $this->assertEquals(0, $log['data']['attachment']);
        $this->assertEquals(0, $log['data']['cc']);
        $this->assertEquals(1, $log['data']['bcc']);
    }

    /**
     * @testdox Check to Sent Email with Attachments
     */
    public function testSentBasicEmailWithAttachments()
    {
        if (!self::isServiceSetup()) {
            $this->markTestSkipped('Skip as Mail is not Setup.');
            return;
        }

        $file1 = tempnam(null, 'testSentBasicEmailWithAttachments1');
        $file2 = tempnam(null, 'testSentBasicEmailWithAttachments2');

        $result = $this->ms->sentEmail(
            $this->getEmail(),
            'Test Subject 4',
            'Test Content',
            array(
                $file1,
                $file2,
            ),
            $this->getEmail('three'),
            $this->getEmail('four')
        );
        $this->assertNotEmpty($result);

        $log = $this->getLogFileTail();
        $this->assertNotEmpty($log);
        $this->assertEquals(1, $log['count']);
        $this->assertSame($this->getLoggedEmail(), $log['data']['to']);
        $this->assertStringContainsStringIgnoringCase('Test Subject 4', $log['data']['subject']);
        $this->assertEquals(2, $log['data']['attachment']);
        $this->assertEquals(1, $log['data']['cc']);
        $this->assertEquals(1, $log['data']['bcc']);
    }

    private static function isServiceSetup()
    {
        static $enable;

        do {
            if (null !== $enable) {
                break;
            }

            $mailer = getenv('MAILER_URL');
            if (empty($mailer)) {
                break;
            }

            $uri = parse_url($mailer);

            if ((empty($uri['host'])) || (empty($uri['port']))) {
                break;
            }

            exec("ping -c 1 -W 3 {$uri['host']}", $output, $status);
            if (0 != $status) {
                break;
            }

            $enable = true;
        } while (false);

        return $enable;
    }

    private function getLogFileTail()
    {
        $tail = array();

        do {
            $file = $this->logPath . '/test-app.log';
            if (!file_exists($file)) {
                break;
            }

            exec('tail ' . $file . ' --lines=1', $output);
            $output = end($output);

            if (empty($output)) {
                break;
            }

            preg_match("/app\.INFO\: MAIL\: ([0-9]+) Submitted (.*) \[\]/", $output, $found);
            if (($found[1] === '') || (empty($found[2]))) {
                break;
            }

            $tail = array(
                'count' => (int) $found[1],
                'data'  => json_decode($found[2], true),
            );
        } while (false);

        return $tail;
    }

    private function getEmail($label = false)
    {
        $email = BaseFunctionalTest::getTestEmail();

        if (!empty($label)) {
            $label = "+{$label}";
            $email = preg_replace('/(.*)@(.*)/i', '$1' . $label . '@$2', $email);
        }

        return $email;
    }

    private function getLoggedEmail()
    {
        $email = BaseFunctionalTest::getTestEmail();
        $email = preg_replace('/(.{1,3})(.*)@(.*)/i', '$1*@*', $email);
        return $email;
    }
}
