<?php

namespace App\Tests\Fixtures\Drug;

use App\Entity\Brand;
use App\Entity\BrandSynonym;
use App\Entity\BrandTypo;
use App\Entity\Experimental;
use App\Entity\Generic;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BrandDrugRepoFixtures extends Fixture
{
    public function load(ObjectManager $om)
    {
        $downlink1 = new Experimental();
        $downlink1->setName('test-downlink-1');
        $downlink1->setType('experimental');
        $downlink1->setValid('1');
        $downlink1->setUid('TEST123');
        $downlink1->setComments('test-TEST123-Comment');
        $om->persist($downlink1);

        $downlink2 = new Generic();
        $downlink2->setName('test-downlink-2');
        $downlink2->setValid('1');
        $downlink2->setLevel1('A1');
        $downlink2->setLevel2('A2');
        $downlink2->setLevel3('A3');
        $downlink2->setLevel4('A4');
        $downlink2->setLevel5('A5');
        $downlink2->setUid('ABC123');
        $downlink2->setNodoublebounce(1);
        $downlink2->setAutoencodeexclude(1);
        $downlink2->setExperimental($downlink1);
        $downlink2->setComments('test-1-ABC123-Comment');
        $om->persist($downlink2);

        $drug1 = new Brand();
        $drug1->setName('test-1');
        $drug1->setValid('0');
        $drug1->setUid('ABC123');
        $drug1->setAutoencodeexclude(1);
        $drug1->setComments('test-1-ABC123-Comment');
        $drug1->setGeneric($downlink2);
        $om->persist($drug1);

        $bs1 = new BrandSynonym();
        $bs1->setName('test-1S-A');
        $bs1->setCountry(array('AU', 'US'));
        $bs1->setBrand($drug1);
        $om->persist($bs1);

        $bs2 = new BrandSynonym();
        $bs2->setName('test-1S-B');
        $bs2->setCountry(array('MY', 'JP'));
        $bs2->setBrand($drug1);
        $om->persist($bs2);

        $bt1 = new BrandTypo();
        $bt1->setName('test-1T-A');
        $bt1->setBrand($drug1);
        $om->persist($bt1);

        $bt2 = new BrandTypo();
        $bt2->setName('test-1T-B');
        $bt2->setBrand($drug1);
        $om->persist($bt2);

        $bt3 = new BrandTypo();
        $bt3->setName('test-1T-C');
        $bt3->setBrand($drug1);
        $om->persist($bt3);

        $drug2 = new Brand();
        $drug2->setName('test-2');
        $drug2->setValid(1);
        $drug2->setUid('DEF456');
        $drug2->setAutoencodeexclude(0);
        $drug2->setComments('test-2-DEF456-Comment');
        $om->persist($drug2);

        $drug3 = new Brand();
        $drug3->setName('test-3');
        $drug3->setValid(0);
        $drug3->setUid('GHI789');
        $drug3->setAutoencodeexclude(1);
        $drug3->setComments('test-3-GHI789-Comment');
        $drug3->setCreationDate(new \DateTime());
        $om->persist($drug3);

        $drug4g = new Generic();
        $drug4g->setName('test-generic-b4');
        $drug4g->setValid('1');
        $drug4g->setLevel1('A1');
        $drug4g->setLevel2('C2');
        $drug4g->setLevel3('D3');
        $drug4g->setLevel4('E4');
        $drug4g->setLevel5('G5');
        $drug4g->setUid('1234567901-1234567901-12345679BB');
        $drug4g->setNodoublebounce(1);
        $drug4g->setAutoencodeexclude(1);
        $drug4g->setComments('test-3-1234567901-Comment');
        $om->persist($drug4g);

        $drug4 = new Brand();
        $drug4->setName('test-brand-4');
        $drug4->setValid(1);
        $drug4->setUid('1234567901-1234567901-12345679AA');
        $drug4->setAutoencodeexclude(1);
        $drug4->setComments('test-3-1234567901-Comment');
        $drug4->setCreationDate(new \DateTime());
        $drug4->setGeneric($drug4g);
        $om->persist($drug4);

        $drug5g = new Generic();
        $drug5g->setName('test-generic-b5');
        $drug5g->setValid('1');
        $drug5g->setLevel1('');
        $drug5g->setLevel2('');
        $drug5g->setLevel3('');
        $drug5g->setLevel4('');
        $drug5g->setLevel5('');
        $drug5g->setUid('1234567901-1234567901-12345679CC');
        $drug5g->setNodoublebounce(1);
        $drug5g->setAutoencodeexclude(1);
        $om->persist($drug5g);

        $drug5 = new Brand();
        $drug5->setName('test-brand-5');
        $drug5->setValid(1);
        $drug5->setUid('1234567901-1234567901-12345679CC');
        $drug5->setAutoencodeexclude(1);
        $drug5->setComments('test-3-1234567901-Comment');
        $drug5->setCreationDate(new \DateTime());
        $drug5->setGeneric($drug5g);
        $om->persist($drug4);

        $om->flush();
    }
}
