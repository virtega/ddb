<?php

namespace App\Tests\Fixtures\Drug;

use App\Entity\Experimental;
use App\Entity\Generic;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ExperimentalDrugRepoFixtures extends Fixture
{
    public function load(ObjectManager $om)
    {
        $drug1 = new Experimental();
        $drug1->setName('test-1');
        $drug1->setType('experimental');
        $drug1->setValid('1');
        $drug1->setUid('ABC123');
        $drug1->setComments('test-1-ABC123-Comment');
        $om->persist($drug1);

        $drug2 = new Experimental();
        $drug2->setName('test-2');
        $drug2->setType('generic');
        $drug2->setValid('1');
        $drug2->setUid('DEF456');
        $drug2->setComments('test-2-DEF456-Comment');
        $om->persist($drug2);

        $drug3 = new Experimental();
        $drug3->setName('test-3');
        $drug3->setType('brand');
        $drug3->setValid('1');
        $drug3->setUid('GHI789');
        $drug3->setComments('test-3-GHI789-Comment');
        $drug3->setCreationDate(new \DateTime());
        $om->persist($drug3);

        $uplink = new Generic();
        $uplink->setName('test-uplink');
        $uplink->setValid('1');
        $uplink->setLevel1('A1');
        $uplink->setLevel2('A2');
        $uplink->setLevel3('A3');
        $uplink->setLevel4('A4');
        $uplink->setLevel5('A5');
        $uplink->setUid('UPLINK');
        $uplink->setNodoublebounce(1);
        $uplink->setAutoencodeexclude(1);
        $uplink->setExperimental($drug1);
        $uplink->setComments('test-UPLINK-Comment');
        $om->persist($uplink);

        $om->flush();
    }
}
