<?php

namespace App\Tests\Fixtures\Drug;

use App\Entity\Generic;
use App\Entity\GenericSynonym;
use App\Entity\GenericTypo;
use App\Entity\GenericRefLevel1;
use App\Entity\GenericRefLevel2;
use App\Entity\GenericRefLevel3;
use App\Entity\GenericRefLevel4;
use App\Entity\Experimental;
use App\Entity\Brand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class GenericDrugRepoFixtures extends Fixture
{
    public function load(ObjectManager $om)
    {
        $downlink = new Experimental();
        $downlink->setName('test-downlink');
        $downlink->setType('experimental');
        $downlink->setValid('1');
        $downlink->setUid('DOWNLINK');
        $downlink->setComments('test-DOWNLINK-Comment');
        $om->persist($downlink);

        $drug1 = new Generic();
        $drug1->setName('test-1');
        $drug1->setValid('1');
        $drug1->setLevel1('A1');
        $drug1->setLevel2('A2');
        $drug1->setLevel3('A3');
        $drug1->setLevel4('A4');
        $drug1->setLevel5('A5');
        $drug1->setUid('ABC123');
        $drug1->setNodoublebounce(1);
        $drug1->setAutoencodeexclude(1);
        $drug1->setExperimental($downlink);
        $drug1->setComments('test-1-ABC123-Comment');
        $om->persist($drug1);

        $uplink = new Brand();
        $uplink->setName('test-uplink');
        $uplink->setValid(1);
        $uplink->setUid('UPLINK');
        $uplink->setAutoencodeexclude(0);
        $uplink->setComments('test-UPLINK-Comment');
        $uplink->setGeneric($drug1);
        $om->persist($uplink);

        $gs1 = new GenericSynonym();
        $gs1->setName('test-1S-A');
        $gs1->setCountry(array('AU', 'US'));
        $gs1->setGeneric($drug1);
        $om->persist($gs1);

        $gs2 = new GenericSynonym();
        $gs2->setName('test-1S-B');
        $gs2->setCountry(array('MY', 'JP'));
        $gs2->setGeneric($drug1);
        $om->persist($gs2);

        $gt1 = new GenericTypo();
        $gt1->setName('test-1T-A');
        $gt1->setGeneric($drug1);
        $om->persist($gt1);

        $gt2 = new GenericTypo();
        $gt2->setName('test-1T-B');
        $gt2->setGeneric($drug1);
        $om->persist($gt2);

        $gt3 = new GenericTypo();
        $gt3->setName('test-1T-C');
        $gt3->setGeneric($drug1);
        $om->persist($gt3);

        $drug2 = new Generic();
        $drug2->setName('test-2');
        $drug2->setValid('0');
        $drug2->setLevel1('B1');
        $drug2->setLevel2('B2');
        $drug2->setLevel3('B3');
        $drug2->setLevel4('B4');
        $drug2->setLevel5('B5');
        $drug2->setUid('DEF456');
        $drug2->setNodoublebounce(0);
        $drug2->setAutoencodeexclude(1);
        $drug2->setComments('test-2-DEF456-Comment');
        $om->persist($drug2);

        $drug3 = new Generic();
        $drug3->setName('test-3');
        $drug3->setValid(1);
        $drug3->setLevel1('C1');
        $drug3->setLevel2('C2');
        $drug3->setLevel3('C3');
        $drug3->setLevel4('C4');
        $drug3->setLevel5('C5');
        $drug3->setUid('GHI789');
        $drug3->setNodoublebounce(1);
        $drug3->setAutoencodeexclude(0);
        $drug3->setComments('test-3-GHI789-Comment');
        $drug3->setCreationDate(new \DateTime());
        $om->persist($drug3);

        $stmt = $om->getConnection()->prepare("INSERT INTO `generic_ref_level1` (`code`, `name`) VALUES ('A', 'Alimentary Tract And Metabolism'), ('B', 'Blood And Blood Forming Organs'), ('C', 'Cardiovascular System'), ('D', 'Dermatologicals'), ('G', 'Genito Urinary System And Sex Hormones'), ('H', 'Systemic Hormonal Preparations, Excl. Sex Hormones'), ('J', 'General Antiinfectives For Systemic Use'), ('L', 'Antineoplastic And Immunomodulating Agents'), ('M', 'Musculo-Skeletal System'), ('N', 'Nervous System'), ('P', 'Antiparasitic Products, Insecticides And Repellents'), ('R', 'Respiratory System'), ('S', 'Sensory Organs'), ('V', 'Various'), ('X', 'Class Not Available'), ('Y', 'Code Not available');");
        $stmt->execute();

        $stmt = $om->getConnection()->prepare("INSERT INTO `generic_ref_level2` (`code`, `name`) VALUES ('A01', 'Stomatological Preparations'), ('A02', 'Drugs for Acid Related Disorders'), ('A03', 'Drugs for Functional Gastrointestinal Disorders'), ('A04', 'Antiemetics And Antinauseants'), ('A05', 'Bile And Liver Therapy'), ('A06', 'Laxatives'), ('A07', 'Antidiarrheals, Intestinal Antiinflammatory/Antiinfective Agents'), ('A08', 'Antiobesity Preparations, Excl. Diet Products'), ('A09', 'Digestives, Incl. Enzymes'), ('A10', 'Drugs Used In Diabetes'), ('A11', 'Vitamins'), ('A12', 'Mineral Supplements'), ('A13', 'Tonics'), ('A14', 'Anabolic Agents For Systemic Use'), ('A15', 'Appetite Stimulants'), ('A16', 'Other Alimentary Tract And Metabolism Products');");
        $stmt->execute();

        $stmt = $om->getConnection()->prepare("INSERT INTO `generic_ref_level3` (`code`, `name`) VALUES ('A01A', 'Stomatological Preparations'), ('A02A', 'Antacids'), ('A02B', 'Drugs for Peptic Ulcer and Gastroesophageal Reflux Disease (GERD)'), ('A02X', 'Other drugs for acid related disorders'), ('A03A', 'Drugs for Functional Bowel Disorders'), ('A03B', 'Belladonna And Derivatives, Plain'), ('A03C', 'Antispasmodics In Combination With Psycholeptics'), ('A03D', 'Antispasmodics In Combination With Analgesics'), ('A03E', 'Antispasmodics And Anticholinergics In Combination With Other Drugs'), ('A03F', 'Propulsives'), ('A04A', 'Antiemetics And Antinauseants'), ('A05A', 'Bile Therapy'), ('A05B', 'Liver Therapy, Lipotropics'), ('A05C', 'Drugs For Bile Therapy And Lipotropics In Combination'), ('A06A', 'Laxatives'), ('A07A', 'Intestinal Antiinfectives'), ('A07B', 'Intestinal Adsorbents'), ('A07C', 'Electrolytes With Carbohydrates'), ('A07D', 'Antipropulsives'), ('A07E', 'Intestinal Antiinflammatory Agents'), ('A07F', 'Antidiarrheal Microorganisms'), ('A07X', 'Other Antidiarrheals'), ('A08A', 'Antiobesity Preparations, Excl. Diet Products'), ('A09A', 'Digestives, Incl. Enzymes'), ('A10A', 'Insulins And Analogues'), ('A10B', 'Oral Blood Glucose Lowering Drugs'), ('A10X', 'Other Drugs Used In Diabetes'), ('A11A', 'Multivitamins, Combinations'), ('A11B', 'Multivitamins, Plain'), ('A11C', 'Vitamin A And D, Incl. Combinations Of The Two'), ('A11D', 'Vitamin B1, Plain And In Combination With Vitamin B6 And B12'), ('A11E', 'Vitamin B-Complex, Incl. Combinations'), ('A11G', 'Ascorbic Acid (Vitamin C), Incl. Combinations'), ('A11H', 'Other Plain Vitamin Preparations'), ('A11J', 'Other Vitamin Products, Combinations'), ('A12A', 'Calcium'), ('A12B', 'Potassium'), ('A12C', 'Other Mineral Supplements'), ('A13A', 'Tonics'), ('A14A', 'Anabolic Steroids'), ('A14B', 'Other Anabolic Agents'), ('A16A', 'Other Alimentary Tract And Metabolism Products');");
        $stmt->execute();

        $stmt = $om->getConnection()->prepare("INSERT INTO `generic_ref_level4` (`code`, `name`) VALUES ('A01AA', 'Caries Prophylactic Agents'), ('A01AB', 'Antiinfectives For Local Oral Treatment'), ('A01AC', 'Corticosteroids For Local Oral Treatment'), ('A01AD', 'Other Agents For Local Oral Treatment'), ('A02AA', 'Magnesium Compounds'), ('A02AB', 'Aluminium Compounds'), ('A02AC', 'Calcium Compounds'), ('A02AD', 'Combinations And Complexes Of Aluminium, Calcium And Magnesium Compounds'), ('A02AF', 'Antacids With Antiflatulents'), ('A02AG', 'Antacids With Antispasmodics'), ('A02AH', 'Antacids With Sodium Bicarbonate'), ('A02AX', 'Antacids, Other Combinations'), ('A02BA', 'H2-Receptor Antagonists'), ('A02BB', 'Prostaglandins'), ('A02BC', 'Proton Pump Inhibitors'), ('A02BD', 'Combinations For Eradication Of Helicobacter Pylori'), ('A02BX', 'Other Drugs for Peptic Ulcer and Gastroesophageal Reflux Disease (GERD)'), ('A02DA', 'Antiflatulents'), ('A02EA', 'Antiregurgitants'), ('A03AA', 'Synthetic Anticholinergics, Esters With Tertiary Amino Group'), ('A03AB', 'Synthetic Anticholinergics, Quaternary Ammonium Compounds'), ('A03AC', 'Synthetic Antispasmodics, Amides With Tertiary Amines'), ('A03AD', 'Papaverine And Derivatives'), ('A03AE', 'Drugs acting on serotonin receptors'), ('A03AK', 'Other Drugs for Functional Bowel Disorders'), ('A03AX', 'Other Drugs For Functional Bowel Disorders'), ('A03BA', 'Belladonna Alkaloids, Tertiary Amines'), ('A03BB', 'Belladonna Alkaloids, Semisynthetic, Quaternary Ammonium Compounds'), ('A03CA', 'Synthetic Anticholinergic Agents In Combination With Psycholeptics'), ('A03CB', 'Belladonna And Derivatives In Combination With Psycholeptics'), ('A03CC', 'Other Antispasmodics In Combination With Psycholeptics'), ('A03DA', 'Synthetic Anticholinergic Agents In Combination With Analgesics'), ('A03DB', 'Belladonna And Derivatives In Combination With Analgesics'), ('A03DC', 'Other Antispasmodics In Combination With Analgesics'), ('A03EA', 'Antispasmodics, Psycholeptics And Analgesics In Combination');");
        $stmt->execute();

        $om->flush();
    }
}
