<?php

namespace App\Tests\Fixtures;

use App\Entity\Country;
use App\Entity\CountryTaxonomy;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CountryFixtures extends Fixture
{
    public function load(ObjectManager $om)
    {
        if ($om->getConnection()->getDatabasePlatform() instanceof \Doctrine\DBAL\Platforms\MySqlPlatform) {
            $om->getConnection()
                ->prepare('SET FOREIGN_KEY_CHECKS=0;')
                ->execute();
        }

        // reflect v1.0
        $om->getConnection()
            ->prepare( \App\Migrations\Version20180501000001::getCountryDataSQL() )
            ->execute();

        // reflect v1.2
        $om->getConnection()
            ->prepare( \App\Migrations\Version20190724000000::getCountryTaxonomyDataSQL() )
            ->execute();

        $om->getConnection()
            ->prepare( \App\Migrations\Version20190724000001::getUpdateCountryAndCityDataSQL() )
            ->execute();

        $om->getConnection()
            ->prepare( \App\Migrations\Version20190724000001::getCountryContinentDataSQL() )
            ->execute();

        $om->getConnection()
            ->prepare( \App\Migrations\Version20190724000001::getCountryContinentSpecialGroupEU5() )
            ->execute();

        if ($om->getConnection()->getDatabasePlatform() instanceof \Doctrine\DBAL\Platforms\MySqlPlatform) {
            $om->getConnection()
                ->prepare('SET FOREIGN_KEY_CHECKS=1;')
                ->execute();
        }

        $countryRepo = $om->getRepository(Country::class);
        $this->addReference('COUNTRY_MY', $countryRepo->findOneBy(['isoCode' => 'MY']));
        $this->addReference('COUNTRY_PH', $countryRepo->findOneBy(['isoCode' => 'PH']));
        $this->addReference('COUNTRY_RU', $countryRepo->findOneBy(['isoCode' => 'RU']));
        $this->addReference('COUNTRY_SG', $countryRepo->findOneBy(['isoCode' => 'SG']));
        $this->addReference('COUNTRY_US', $countryRepo->findOneBy(['isoCode' => 'US']));

        $countryTaxRepo = $om->getRepository(CountryTaxonomy::class);
        $this->addReference('REGION_MWA', $countryTaxRepo->findOneBy(['name' => 'Midwest America']));
        $this->addReference('REGION_NEU', $countryTaxRepo->findOneBy(['name' => 'Northern Europe']));
        $this->addReference('REGION_PLY', $countryTaxRepo->findOneBy(['name' => 'Polynesia']));
        $this->addReference('REGION_SEA', $countryTaxRepo->findOneBy(['name' => 'Southeast Asia']));
        $this->addReference('REGION_WAF', $countryTaxRepo->findOneBy(['name' => 'West Africa']));
    }
}
