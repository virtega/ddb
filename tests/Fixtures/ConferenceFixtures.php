<?php

namespace App\Tests\Fixtures;

use App\Entity\Conference;
use App\Entity\ConferenceSpecialties;
use App\Entity\Country;
use App\Entity\SpecialtyTaxonomy;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ConferenceFixtures extends Fixture
{
    public function load(ObjectManager $om)
    {
        $conference = new Conference();
        $conference->setName('Conference A Test S69RTU');
        $conference->setStartDate(new \DateTime('2019-02-19 08:00:00'));
        $conference->setEndDate(new \DateTime('2019-05-30 17:00:00'));
        $conference->setCity('Petaling Jaya');
        $conference->setCityGuide('Next to Hilton Hotel from Federal Highway');
        $conference->setCountry( $this->getReference('COUNTRY_MY') );
        $conference->setRegion( $this->getReference('REGION_SEA') );
        $conference->setPreviousId('12345678901234567890123456789012');
        $conference->setUniqueName('CAT000S69RTU');
        $conference->setCruise(false);
        $conference->setOnline(false);
        $conference->setKeyEvent(true);
        $conference->setHasGuide(true);
        $conference->setDiscontinued(false);
        $conference->setDetailsState('Selangor');
        $conference->setDetailsContactDesc('Call Regina mobile below,' . PHP_EOL . 'She will arrange everything including Hotel arrangement.');
        $conference->setDetailsContactPhones(['+60123456789', '+60123456788']);
        $conference->setDetailsContactFaxes([]);
        $conference->setDetailsContactEmails(['regina.phalange@buffay.ok.com']);
        $conference->setDetailsContactLinks(['https://w3.S69RTU.org.com', 'https://www.hilton.com.my/events/2019']);
        $conference->setDetailsMetaCreatedOn(new \DateTime('2018-11-25 16:35:33'));
        $conference->setDetailsMetaCreatedBy('king.author');

        $specialty = new ConferenceSpecialties;
        $specialty->setConference($conference);
        $specialty->setSpecialty( $this->getReference('SPECIALTY_NP') );
        $om->persist($specialty);
        $conference->addSpecialty($specialty);

        $specialty = new ConferenceSpecialties;
        $specialty->setConference($conference);
        $specialty->setSpecialty( $this->getReference('SPECIALTY_UR') );
        $om->persist($specialty);
        $conference->addSpecialty($specialty);

        $om->persist($conference);

        $conference = new Conference();
        $conference->setName('Conference B Test RD996G');
        $conference->setStartDate(new \DateTime('2018-03-19 09:00:00'));
        $conference->setEndDate(new \DateTime('2019-03-20 12:30:00'));
        $conference->setCity('Tondo');
        $conference->setCountry( $this->getReference('COUNTRY_PH') );
        $conference->setRegion( $this->getReference('REGION_SEA') );
        $conference->setPreviousId('39248056732985623495769234023344');
        $conference->setUniqueName('CAT000RD996G');
        $conference->setCruise(false);
        $conference->setOnline(false);
        $conference->setKeyEvent(false);
        $conference->setHasGuide(false);
        $conference->setDiscontinued(false);
        $conference->setDetailsState('Tagalog');
        $conference->setDetailsContactDesc('Teodora Alonso Realonda');
        $conference->setDetailsContactEmails(['Teodora.Realonda@gmail.com']);
        $conference->setDetailsMetaCreatedOn(new \DateTime('2016-06-16 12:26:21'));
        $conference->setDetailsMetaCreatedBy('king.author');
        $om->persist($conference);

        $conference = new Conference();
        $conference->setName('Conference C Test DF345F');
        $conference->setStartDate(new \DateTime('2020-08-10 08:00:00'));
        $conference->setEndDate(new \DateTime('2020-08-18 17:00:00'));
        $conference->setCity('Voronezh');
        $conference->setCountry( $this->getReference('COUNTRY_RU') );
        $conference->setRegion( $this->getReference('REGION_NEU') );
        $conference->setPreviousId('42745242459922345685638735468735');
        $conference->setUniqueName('CAT000DF345F');
        $conference->setCruise(false);
        $conference->setOnline(true);
        $conference->setKeyEvent(true);
        $conference->setHasGuide(true);
        $conference->setDiscontinued(false);
        $conference->setDetailsState('Воронеж');
        $conference->setDetailsContactPhones(['+60123456789', '+60123456788']);
        $conference->setDetailsContactFaxes([]);
        $conference->setDetailsContactEmails(['Stanislav.Lyubava@ninel.com', 'bogdan.arkady@afanasyveniamin.ru']);
        $conference->setDetailsContactLinks(['https://anton.org.com.ru', 'https://AfanasyVeniamin.ru']);
        $conference->setDetailsMetaCreatedOn(new \DateTime('2019-05-12 13:35:33'));
        $conference->setDetailsMetaCreatedBy('king.author');

        $specialty = new ConferenceSpecialties;
        $specialty->setConference($conference);
        $specialty->setSpecialty( $this->getReference('SPECIALTY_HO') );
        $om->persist($specialty);
        $conference->addSpecialty($specialty);

        $specialty = new ConferenceSpecialties;
        $specialty->setConference($conference);
        $specialty->setSpecialty( $this->getReference('SPECIALTY_PY') );
        $om->persist($specialty);
        $conference->addSpecialty($specialty);

        $specialty = new ConferenceSpecialties;
        $specialty->setConference($conference);
        $specialty->setSpecialty( $this->getReference('SPECIALTY_TS') );
        $om->persist($specialty);
        $conference->addSpecialty($specialty);

        $om->persist($conference);

        $conference = new Conference();
        $conference->setName('Conference D Test KF938S');
        $conference->setStartDate(new \DateTime('2020-03-03 08:00:00'));
        $conference->setEndDate(new \DateTime('2020-03-03 18:00:00'));
        $conference->setCity('South Gate Central');
        $conference->setCountry( $this->getReference('COUNTRY_SG') );
        $conference->setRegion( $this->getReference('REGION_WAF') );
        $conference->setPreviousId('34523908457230948572340934563457');
        $conference->setUniqueName('CAT000KF938S');
        $conference->setCruise(true);
        $conference->setOnline(false);
        $conference->setKeyEvent(true);
        $conference->setHasGuide(true);
        $conference->setDiscontinued(false);
        $conference->setDetailsContactPhones(['+6563524112']);
        $conference->setDetailsContactFaxes(['+6563524112']);
        $conference->setDetailsContactEmails(['irfan.mikail@nutc.com.sg']);
        $conference->setDetailsContactLinks(['https://anton.org.sg', 'https://nutc.sg']);
        $conference->setDetailsMetaCreatedOn(new \DateTime('2019-10-25 15:15:33'));
        $conference->setDetailsMetaCreatedBy('king.author');
        $om->persist($conference);

        $om->flush();
    }
}
