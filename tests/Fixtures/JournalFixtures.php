<?php

namespace App\Tests\Fixtures;

use App\Entity\Country;
use App\Entity\Journal;
use App\Entity\JournalSpecialties;
use App\Entity\SpecialtyTaxonomy;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class JournalFixtures extends Fixture
{
    public function load(ObjectManager $om)
    {
        $journal = new Journal();
        $journal->setName('Journal Test A RF346GH');
        $journal->setAbbreviation('JTA 6GH');
        $journal->setVolume('V100 -');
        $journal->setNumber('48');
        $journal->setStatus(Journal::STATUS_ACTIVE);
        $journal->setExported(1);
        $journal->setIsMedline(1);
        $journal->setIsSecondLine(0);
        $journal->setIsDgAbstract(0);
        $journal->setNotBeingUpdated(0);
        $journal->setDrugsMonitored(1);
        $journal->setGlobalEditionJournal(1);
        $journal->setDeleted(0);
        $journal->setManualCreation(1);
        $journal->setMedlineIssn('1234-X65A');
        $journal->setJournalReport1(0.5);
        $journal->setJournalReport2(0.1);
        $journal->setUnid(40127364512735601515476166103476);

        $specialty = new JournalSpecialties;
        $specialty->setJournal($journal);
        $specialty->setSpecialty( $this->getReference('SPECIALTY_IM') );
        $om->persist($specialty);
        $journal->addSpecialty($specialty);

        $specialty = new JournalSpecialties;
        $specialty->setJournal($journal);
        $specialty->setSpecialty( $this->getReference('SPECIALTY_UR') );
        $om->persist($specialty);
        $journal->addSpecialty($specialty);

        $journal->setDetailsLanguage('English;Cantonese;Malay');

        $country = $this->getReference('COUNTRY_SG');
        $journal->setDetailsCountry( $country->getIsoCode() );

        $region = $this->getReference('REGION_SEA');
        $journal->setDetailsRegion( $region->getId() );

        $journal->setDetailsEdition('English;Mandarin');
        $journal->setDetailsCopyright('All Rights Reserved by Bodega Industries.');
        $journal->setDetailsAutoPublishingSource('Unknown');
        $journal->setDetailsPreviousRowGuid('45637357-6D23-4DDB-93CF-8C78489AE448');
        $journal->setDetailsAbstractAbstract(true);
        $journal->setDetailsAbstractRegForAbstract(true);
        $journal->setDetailsAbstractFullTextAvailablity(true);
        $journal->setDetailsAbstractRegForFullText(true);
        $journal->setDetailsAbstractFeeForFullText(true);
        $journal->setDetailsAbstractComments('Complete RST available with Kevin.' . PHP_EOL . 'Please work with him to pull your resources needed from here.');
        $journal->setDetailsOrderingHtmlCode('HTML Order Attribute');
        $journal->setDetailsOrderingCgiValue(58600);
        $journal->setDetailsOrderingPrinterName('Bodega Industries');
        $journal->setDetailsPublisherName('Bodega Industries');
        $journal->setDetailsPublisherRegUrl('http://bodegaindustries.com/register');
        $journal->setDetailsUrlUrl('http://bodegaindustries.com/about-him');
        $journal->setDetailsUrlUpdatedOn(new \DateTime('2018-12-10 03:18:21'));
        $journal->setDetailsUrlViewedOn(new \DateTime('2018-12-10 04:18:21'));
        $journal->setDetailsUrlViewedOnComment('Everything is still make sense.' . PHP_EOL . 'No need to change anything.');
        $journal->setDetailsUrlToUpdate(new \DateTime('2019-01-3 16:18:21'));
        $journal->setDetailsUrlNote('Everything is still making sense.' . PHP_EOL . 'Contact Pamela for new URL.');
        $journal->setDetailsUrlFrequency('8-10 time per week');
        $journal->setDetailsUrlUsername('anton.vaklusc');
        $journal->setDetailsUrlPassword('3q4jkl34jnkqw34');
        $journal->setDetailsValidationAuthor('karam.nurul');
        $journal->setDetailsValidationDateComposed(new \DateTime('2018-06-25 12:20:15'));
        $journal->setDetailsValidationClearedBy('king.author');
        $journal->setDetailsValidationClearedOn(new \DateTime('2018-11-25 16:35:33'));
        $journal->setDetailsValidationModified('this mod is a string');
        $journal->setDetailsMetaCreatedOn(new \DateTime('2018-11-25 16:35:33'));
        $journal->setDetailsMetaCreatedBy('king.author');

        $om->persist($journal);

        $journal = new Journal();
        $journal->setName('Journal Test B HJ345X');
        $journal->setAbbreviation('JTB 45X');
        $journal->setVolume('Vol 5 Ser 08');
        $journal->setNumber('36');
        $journal->setStatus(Journal::STATUS_ARCHIVE);
        $journal->setExported(0);
        $journal->setIsMedline(0);
        $journal->setIsSecondLine(1);
        $journal->setIsDgAbstract(1);
        $journal->setNotBeingUpdated(1);
        $journal->setDrugsMonitored(0);
        $journal->setGlobalEditionJournal(2);
        $journal->setDeleted(1);
        $journal->setManualCreation(0);
        $journal->setMedlineIssn('345D-X65A');
        $journal->setJournalReport1(0.2);
        $journal->setJournalReport2(0.6);
        $journal->setUnid(40127364512735601515476166103476);

        $journal->setDetailsMetaCreatedOn(new \DateTime('2018-08-20 01:45:22'));
        $journal->setDetailsMetaCreatedBy('king.author');

        $om->persist($journal);

        $om->flush();
    }
}
