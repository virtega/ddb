<?php

namespace App\Tests\Fixtures;

use App\Entity\SpecialtyTaxonomy;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SpecialtyTaxonomyFixtures extends Fixture
{
    public function load(ObjectManager $om)
    {
        $specialty = new SpecialtyTaxonomy();
        $specialty->setSpecialty('Clinical Pharmacology');
        $om->persist($specialty);
        $this->addReference('SPECIALTY_CP', $specialty);

        $specialty = new SpecialtyTaxonomy();
        $specialty->setSpecialty('Nephrology');
        $om->persist($specialty);
        $this->addReference('SPECIALTY_NP', $specialty);

        $specialty = new SpecialtyTaxonomy();
        $specialty->setSpecialty('Immunology');
        $om->persist($specialty);
        $this->addReference('SPECIALTY_IM', $specialty);

        $specialty = new SpecialtyTaxonomy();
        $specialty->setSpecialty('Gastroenterology');
        $om->persist($specialty);
        $this->addReference('SPECIALTY_GS', $specialty);

        $specialty = new SpecialtyTaxonomy();
        $specialty->setSpecialty('Psychiatry');
        $om->persist($specialty);
        $this->addReference('SPECIALTY_PY', $specialty);

        $specialty = new SpecialtyTaxonomy();
        $specialty->setSpecialty('Urology');
        $om->persist($specialty);
        $this->addReference('SPECIALTY_UR', $specialty);

        $specialty = new SpecialtyTaxonomy();
        $specialty->setSpecialty('Haematology and Oncology');
        $om->persist($specialty);
        $this->addReference('SPECIALTY_HO', $specialty);

        $specialty = new SpecialtyTaxonomy();
        $specialty->setSpecialty('Oncology');
        $om->persist($specialty);
        $this->addReference('SPECIALTY_OC', $specialty);

        $specialty = new SpecialtyTaxonomy();
        $specialty->setSpecialty('Transplant Surgery');
        $om->persist($specialty);
        $this->addReference('SPECIALTY_TS', $specialty);

        $om->flush();
    }
}
