<?php

namespace App\Tests\Unit\Repository;

use App\Entity\Conference;
use App\Repository\ConferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Repository | ConferenceRepository
 */
class ConferenceRepositoryUnitTest extends WebTestCase
{
    use ConferenceJournalRepositoryTestTrait;

    /**
     * @var ConferenceRepository
     */
    private $repo;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        $this->repo = $em->getRepository(Conference::class);

        parent::setUp();
    }

    /**
     * @testdox Checking changeSearchConditionsBinder()
     */
    public function testChangeSearchConditionsBinder()
    {
        $this->__setupMocks(ConferenceRepository::class);

        $this->assertSame('AND', $this->getSearchConditionBinderValue(ConferenceRepository::class));

        $this->repo->changeSearchConditionsBinder('or');

        $this->assertSame('or', $this->getSearchConditionBinderValue(ConferenceRepository::class));

        try {
            $this->repo->changeSearchConditionsBinder('any');
            $this->assertFalse($this->getSearchConditionBinderValue(ConferenceRepository::class), 'This should be rejected.');
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\Exception::class, $error);
            $this->assertStringContainsStringIgnoringCase('Invalid search binder', $error->getMessage());
        }
    }

    /**
     * @dataProvider getSearchStacksProvider
     *
     * @testdox Checking search() Stacks
     */
    public function testSearch($binder = 'AND' , $terms, $offsetLimit, $expectedSql, $executeResult, $fetchResult, $expectedFindByArgs)
    {
        $em = $this->_search($expectedSql, $executeResult, $fetchResult, Conference::class, $expectedFindByArgs);

        $this->__setupMocks(ConferenceRepository::class, $em);

        $this->repo->changeSearchConditionsBinder($binder);

        $this->repo->search($terms, $offsetLimit[0], $offsetLimit[1]);
    }

    public function getSearchStacksProvider()
    {
        return [
            // #0
            [
                'AND',
                [
                    'name' => 'test1',
                ],
                [10, 0],
                "SELECT `c`.`id` FROM `conference` `c` WHERE (`c`.`name` LIKE '%test1%') ORDER BY `c`.`start_date` DESC, `c`.`end_date` ASC, `c`.`name` ASC LIMIT 10 OFFSET 0",
                true,
                [
                    ['id' => 10],
                    ['id' => 11],
                    ['id' => 12],
                    ['id' => 13],
                ],
                [
                    ['id' => [10, 11, 12, 13]],
                    [
                        'startDate' => 'DESC',
                        'endDate' => 'ASC',
                        'name' => 'ASC'
                    ],
                    null,
                    null,
                ]
            ],
            // #1
            [
                'AND',
                [],
                [10, 0],
                "SELECT `c`.`id` FROM `conference` `c` ORDER BY `c`.`start_date` DESC, `c`.`end_date` ASC, `c`.`name` ASC LIMIT 10 OFFSET 0",
                true,
                [],
                [
                    ['id' => []],
                    [
                        'startDate' => 'DESC',
                        'endDate' => 'ASC',
                        'name' => 'ASC'
                    ],
                    null,
                    null,
                ]
            ],
            // #2
            [
                'AND',
                [
                    'name'          => 'test$',
                    'city'          => ['Kuala Lumpur', 'Singapore', "any'de lux"],
                    'country'       => ['MY', 'SG'],
                    'region'        => [203],
                    'specialty'     => [42, 16, 'Emergency'],
                    'startdate'     => '2019-08-01',
                    'enddate'       => '2019-08-30',
                    'ids'           => '^843269*',
                    'keycongress'   => 'yes',
                    'oncruise'      => 'no',
                    'online'        => 'YES',
                    'hasguide'      => '1',
                    'discontinued'  => 'false',
                    'showcorrupted' => '0',
                ],
                [500, 100],
                "SELECT
                    `c`.`id`
                FROM
                    `conference` `c`
                    RIGHT JOIN `country_taxonomy` `ct` ON (`ct`.`id` = `c`.`region` OR `ct`.`parent` = `c`.`region`)
                    INNER JOIN `conference_specialties` `csid0` ON ( `csid0`.`conference_id` = `c`.`id` AND `csid0`.`specialty_id` = 42 )
                    INNER JOIN `conference_specialties` `csid1` ON ( `csid1`.`conference_id` = `c`.`id` AND `csid1`.`specialty_id` = 16 )
                    INNER JOIN `conference_specialties` `cs` ON `cs`.`conference_id` = `c`.`id`
                    INNER JOIN `specialty_taxonomy` `st` ON ((`st`.`id` = `cs`.`specialty_id`) AND ((`st`.`specialty` LIKE \"%Emergency%\")))
                WHERE
                    (`c`.`name` LIKE '%test')
                    AND
                    (
                        (`c`.`city` LIKE \"Kuala Lumpur\")
                        OR
                        (`c`.`city` LIKE \"Singapore\")
                        OR
                        (`c`.`city` LIKE \"any\'de lux\")
                    )
                    AND
                    (`c`.`country` IN (\"MY\",\"SG\"))
                    AND
                    (
                        (`ct`.`id`     IN (203))
                        OR
                        (`ct`.`parent` IN (203))
                    )
                    AND
                    (`c`.`start_date` >= \"2019-08-01\")
                    AND
                    (`c`.`end_date` <= \"2019-08-30\")
                    AND
                    (
                        (`c`.`id`          LIKE '843269%')
                        OR
                        (`c`.`previous_id` LIKE '843269%')
                        OR
                        (`c`.`unique_name` LIKE '843269%')
                    )
                    AND
                    (`c`.`key_event` = 1)
                    AND
                    (`c`.`cruise` = 0)
                    AND
                    (`c`.`online` = 1)
                    AND
                    (`c`.`has_guide` = 1)
                    AND
                    (`c`.`discontinued` = 0)
                    AND
                    (`c`.`name` IS NOT NULL)
                    AND
                    (`c`.`name` !=  \"\")
                ORDER BY
                    `c`.`start_date` DESC,
                    `c`.`end_date` ASC,
                    `c`.`name` ASC
                LIMIT
                    500 OFFSET 100",
                true,
                [
                    ['id' => 101],
                    ['id' => 111],
                    ['id' => 121],
                    ['id' => 131],
                ],
                [
                    ['id' => [101, 111, 121, 131]],
                    [
                        'startDate' => 'DESC',
                        'endDate' => 'ASC',
                        'name' => 'ASC'
                    ],
                    null,
                    null,
                ]
            ],
            // #3
            [
                'OR',
                [
                    'name'          => 'test$',
                    'ids'           => '^843269',
                    'discontinued'  => 'false',
                ],
                [10, 2200],
                "SELECT
                    `c`.`id`
                FROM
                    `conference` `c`
                WHERE
                    (`c`.`name` LIKE '%test')
                    OR
                    (
                        (`c`.`id` LIKE '843269%')
                        OR
                        (`c`.`previous_id` LIKE '843269%')
                        OR
                        (`c`.`unique_name` LIKE '843269%')
                    )
                    OR
                    (`c`.`discontinued` = 0)
                ORDER BY
                    `c`.`start_date` DESC,
                    `c`.`end_date` ASC,
                    `c`.`name` ASC
                LIMIT
                    10 OFFSET 2200",
                true,
                [
                    ['id' => 1311],
                    ['id' => 1301],
                    ['id' => 1321],
                ],
                [
                    ['id' => [1311, 1301, 1321]],
                    [
                        'startDate' => 'DESC',
                        'endDate' => 'ASC',
                        'name' => 'ASC'
                    ],
                    null,
                    null,
                ]
            ],
        ];
    }

    /**
     * @testdox Checking searchResultCount()
     */
    public function testSearchResultCount()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(*) AS `count` FROM `conference` `c` WHERE (`c`.`name` LIKE '%test1%')",
            true,
            4
        );

        $this->__setupMocks(ConferenceRepository::class, $em);

        $result = $this->repo->searchResultCount(['name' => 'test1'], 10, 0);

        $this->assertSame(4, $result);
    }

    /**
     * @testdox Checking countOverallCount()
     */
    public function testCountOverallCount()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(*) AS `count` FROM `conference`;",
            true,
            4
        );

        $this->__setupMocks(ConferenceRepository::class, $em);

        $result = $this->repo->countOverallCount();

        $this->assertSame(4, $result);
    }

    /**
     * @testdox Checking countCorruptedCount()
     */
    public function testCountCorruptedCount()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(*) AS `count` FROM `conference` WHERE `name` IS NULL OR `name` = \"\"  OR `start_date` IS NULL OR `start_date` = \"\"  OR `end_date` IS NULL OR `end_date` = \"\" ",
            true,
            4
        );

        $this->__setupMocks(ConferenceRepository::class, $em);

        $result = $this->repo->countCorruptedCount();

        $this->assertSame(4, $result);
    }

    /**
     * @testdox Checking countKeyedCount()
     */
    public function testCountKeyedCount()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(*) AS `count` FROM `conference` WHERE `isThisField` = 1 ",
            true,
            4
        );

        $this->__setupMocks(ConferenceRepository::class, $em);

        $result = $this->repo->countKeyedCount('isThisField', 1);

        $this->assertSame(4, $result);
    }

    /**
     * @testdox Checking countMaxSpecialtiesConnection()
     */
    public function testCountMaxSpecialtiesConnection()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(*) AS `count` FROM `conference_specialties` GROUP BY `conference_id` ORDER BY `count` DESC LIMIT 1;",
            true,
            18
        );

        $this->__setupMocks(ConferenceRepository::class, $em);

        $result = $this->repo->countMaxSpecialtiesConnection();

        $this->assertSame(18, $result);
    }
}
