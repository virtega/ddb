<?php

namespace App\Tests\Unit\Repository;

use App\Entity\Journal;
use App\Repository\JournalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Repository | JournalRepository
 */
class JournalRepositoryUnitTest extends WebTestCase
{
    use ConferenceJournalRepositoryTestTrait;

    /**
     * @var JournalRepository
     */
    private $repo;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        $this->repo = $em->getRepository(Journal::class);

        parent::setUp();
    }

    /**
     * @testdox Checking changeSearchConditionsBinder()
     */
    public function testChangeSearchConditionsBinder()
    {
        $this->__setupMocks(JournalRepository::class);

        $this->assertSame('AND', $this->getSearchConditionBinderValue(JournalRepository::class));

        $this->repo->changeSearchConditionsBinder('or');

        $this->assertSame('or', $this->getSearchConditionBinderValue(JournalRepository::class));

        try {
            $this->repo->changeSearchConditionsBinder('any');
            $this->assertFalse($this->getSearchConditionBinderValue(JournalRepository::class), 'This should be rejected.');
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\Exception::class, $error);
            $this->assertStringContainsStringIgnoringCase('Invalid search binder', $error->getMessage());
        }
    }

    /**
     * @dataProvider getSearchStacksProvider
     *
     * @testdox Checking search() Stacks
     */
    public function testSearch($binder = 'AND', $terms, $offsetLimit, $expectedSql, $executeResult, $fetchResult, $expectedFindByArgs)
    {
        $em = $this->_search($expectedSql, $executeResult, $fetchResult, Journal::class, $expectedFindByArgs);

        $this->__setupMocks(JournalRepository::class, $em);

        $this->repo->changeSearchConditionsBinder($binder);

        $this->repo->search($terms, $offsetLimit[0], $offsetLimit[1]);
    }

    public function getSearchStacksProvider()
    {
        return [
            // #0
            [
                'AND',
                ['name' => 'test1'],
                [10, 0],
                "SELECT DISTINCT(`j`.`id`) FROM `journal` `j` WHERE ((`j`.`name` LIKE '%test1%') OR (`j`.`abbreviation` LIKE '%test1%')) LIMIT 10 OFFSET 0",
                true,
                [
                    ['id' => 10],
                    ['id' => 11],
                    ['id' => 12],
                    ['id' => 13],
                ],
                [
                    ['id' => [10, 11, 12, 13]],
                    ['name' => 'ASC'],
                    null,
                    null,
                ]
            ],
            // #1
            [
                'AND',
                [],
                [10, 0],
                "SELECT DISTINCT(`j`.`id`) FROM `journal` `j` LIMIT 10 OFFSET 0",
                true,
                [],
                [
                    ['id' => []],
                    ['name' => 'ASC'],
                    null,
                    null,
                ]
            ],
            // #2 - advance search - binder AND
            [
                'AND',
                [
                    'name'               => '^test34',
                    'volume'             => '2017',
                    'status'             => 3,
                    'issn'               => 'AB%D-3669',
                    'specialty'          => [32, 31, 4, 'neuro'],
                    'ids'                => '3369$',
                    'ismedline'          => 'no',
                    'isdgabstract'       => 'yes',
                    'issecondline'       => 'false',
                    'notbeingupdated'    => 'NO',
                    'drugbeingmonitored' => '0',
                    'globaledition'      => 'YES',
                    'deleted'            => 'no',
                    'showcorrupted'      => 'no',
                ],
                [50, 60],
                "SELECT
                    DISTINCT(`j`.`id`)
                FROM
                    `journal` `j`
                    INNER JOIN `journal_specialties` `jsid0` ON (`jsid0`.`journal_id` = `j`.`id` AND `jsid0`.`specialty_id` = 32)
                    INNER JOIN `journal_specialties` `jsid1` ON (`jsid1`.`journal_id` = `j`.`id` AND `jsid1`.`specialty_id` = 31)
                    INNER JOIN `journal_specialties` `jsid2` ON (`jsid2`.`journal_id` = `j`.`id` AND `jsid2`.`specialty_id` = 4)
                    INNER JOIN `journal_specialties` `js` ON `js`.`journal_id` = `j`.`id`
                    INNER JOIN `specialty_taxonomy` `st` ON ((`st`.`id` = `js`.`specialty_id`) AND ((`st`.`specialty` LIKE \"%neuro%\")))
                WHERE
                    (
                        (`j`.`name` LIKE 'test34%')
                        OR
                        (`j`.`abbreviation` LIKE 'test34%')
                    )
                    AND
                    (
                        (`j`.`volume` LIKE '%2017%')
                        OR
                        (`j`.`number`LIKE'%2017%')
                    )
                    AND
                    (`j`.`status` = 3)
                    AND
                    (`j`.`medline_issn` LIKE '%AB%D%3669%')
                    AND
                    (
                        (`j`.`id` LIKE '%3369')
                        OR
                        (`j`.`unid` LIKE '%3369')
                    )
                    AND
                    (`j`.`is_medline` = 0)
                    AND
                    (`j`.`is_dgabstract` = 1)
                    AND
                    (`j`.`is_second_line` = 0)
                    AND
                    (`j`.`not_being_updated` = 0)
                    AND
                    (`j`.`drugs_monitored` = 0)
                    AND
                    (`j`.`global_edition_journal` = 1)
                    AND
                    (`j`.`deleted` = 0)
                    AND
                    (`j`.`name` IS NOT NULL)
                    AND
                    (`j`.`name` ! =\"\")
                LIMIT
                    50 OFFSET 60",
                true,
                [],
                [
                    ['id' => []],
                    ['name' => 'ASC'],
                    null,
                    null,
                ]
            ],
            // #3 - overlay search - binder OR
            [
                'OR',
                [
                    'name' => '^test34',
                    'issn' => '^AB*-3669$',
                ],
                [10, 0],
                "SELECT
                    DISTINCT(`j`.`id`)
                FROM
                    `journal` `j`
                WHERE
                    (
                        (`j`.`name` LIKE 'test34%')
                        OR
                        (`j`.`abbreviation` LIKE 'test34%')
                    )
                    OR
                    (`j`.`medline_issn` LIKE 'AB%3669')
                LIMIT
                    10 OFF SET 0",
                true,
                [
                    ['id' => 2236],
                    ['id' => 56],
                    ['id' => 6993],
                ],
                [
                    ['id' => [2236, 56, 6993]],
                    ['name' => 'ASC'],
                    null,
                    null,
                ]
            ],
        ];
    }

    /**
     * @testdox Checking searchResultCount()
     */
    public function testSearchResultCount()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(DISTINCT(`j`.`id`)) AS `count` FROM `journal` `j` WHERE ((`j`.`name` LIKE '%test1%') OR (`j`.`abbreviation` LIKE '%test1%'))",
            true,
            4
        );

        $this->__setupMocks(JournalRepository::class, $em);

        $result = $this->repo->searchResultCount(['name' => 'test1'], 10, 0);

        $this->assertSame(4, $result);
    }

    /**
     * @testdox Checking countOverallCount()
     */
    public function testCountOverallCount()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(*) AS `count` FROM `journal`;",
            true,
            4
        );

        $this->__setupMocks(JournalRepository::class, $em);

        $result = $this->repo->countOverallCount();

        $this->assertSame(4, $result);
    }

    /**
     * @testdox Checking countCorruptedCount()
     */
    public function testCountCorruptedCount()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(*) AS `count` FROM `journal` WHERE `name` IS NULL OR `name` = \"\"  OR `abbreviation` IS NULL OR `abbreviation` = \"\" ",
            true,
            4
        );

        $this->__setupMocks(JournalRepository::class, $em);

        $result = $this->repo->countCorruptedCount();

        $this->assertSame(4, $result);
    }

    /**
     * @testdox Checking countKeyedCount()
     */
    public function testCountKeyedCount()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(*) AS `count` FROM `journal` WHERE `isThisField` = 1 ",
            true,
            4
        );

        $this->__setupMocks(JournalRepository::class, $em);

        $result = $this->repo->countKeyedCount('isThisField', 1);

        $this->assertSame(4, $result);
    }

    /**
     * @testdox Checking countMaxSpecialtiesConnection()
     */
    public function testCountMaxSpecialtiesConnection()
    {
        $em = $this->_generalQueryAndFetchCollumn(
            "SELECT COUNT(*) AS `count` FROM `journal_specialties` GROUP BY `journal_id` ORDER BY `count` DESC LIMIT 1;",
            true,
            32
        );

        $this->__setupMocks(JournalRepository::class, $em);

        $result = $this->repo->countMaxSpecialtiesConnection();

        $this->assertSame(32, $result);
    }

    /**
     * @testdox Checking getJournalDetailsCountry()
     */
    public function testGetJournalDetailsCountry()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->exactly(2))->method('getRepository')->will($this->returnCallback(function($repoClass) {
            $this->assertSame(\App\Entity\Country::class, $repoClass);

            $repo = $this->createMock(\App\Repository\CountryRepository::class);
            $repo->expects($this->at(0))->method('findOneBy')->will($this->returnCallback(function($terms) {
                $this->assertArrayHasKey('isoCode', $terms);

                $country = null;

                if ('MY' == $terms['isoCode']) {
                    $country = new \App\Entity\Country;
                    $reflection = new \ReflectionClass(\App\Entity\Country::class);
                    $reflProp = $reflection->getProperty('isoCode');
                    $reflProp->setAccessible(true);
                    $reflProp->setValue($country, 'MY');
                    $country->setName('Malaysia');
                    $country->setCapitalCity('Kuala Lumpur');
                }

                return $country;
            }));
            return $repo;
        }));

        $this->__setupMocks(JournalRepository::class, $em);

        $journal = new Journal;

        // found
        $journal->setDetailsCountry('MY');
        $found = $this->repo->getJournalDetailsCountry($journal);
        $this->assertTrue(!empty($found));
        $this->assertInstanceOf(\App\Entity\Country::class, $found);
        $this->assertSame('MY', $found->getIsoCode());
        $this->assertSame('Malaysia', $found->getName());
        $this->assertSame('Kuala Lumpur', $found->getCapitalCity());

        // null by search
        $journal->setDetailsCountry('A5');
        $found = $this->repo->getJournalDetailsCountry($journal);
        $this->assertNull($found);

        // null by value
        $journal->setDetailsCountry(null);
        $found = $this->repo->getJournalDetailsCountry($journal);
        $this->assertNull($found);
    }

    /**
     * @testdox Checking getJournalDetailsCountryTaxonomy()
     */
    public function testGetJournalDetailsCountryTaxonomy()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->exactly(2))->method('getRepository')->will($this->returnCallback(function($repoClass) {
            $this->assertSame(\App\Entity\CountryTaxonomy::class, $repoClass);

            $repo = $this->createMock(\App\Repository\CountryTaxonomyRepository::class);
            $repo->expects($this->at(0))->method('findOneBy')->will($this->returnCallback(function($terms) {
                $this->assertArrayHasKey('id', $terms);

                $countryTax = null;

                if (203 == $terms['id']) {
                    $countryTax = new \App\Entity\CountryTaxonomy;
                    $reflection = new \ReflectionClass(\App\Entity\CountryTaxonomy::class);
                    $reflProp = $reflection->getProperty('id');
                    $reflProp->setAccessible(true);
                    $reflProp->setValue($countryTax, 203);
                    $countryTax->setName('Southeast Asia');
                }

                return $countryTax;
            }));
            return $repo;
        }));

        $this->__setupMocks(JournalRepository::class, $em);

        $journal = new Journal;

        // found
        $journal->setDetailsRegion(203);
        $found = $this->repo->getJournalDetailsCountryTaxonomy($journal);
        $this->assertTrue(!empty($found));
        $this->assertInstanceOf(\App\Entity\CountryTaxonomy::class, $found);
        $this->assertSame(203, $found->getId());
        $this->assertSame('Southeast Asia', $found->getName());
        $this->assertNull($found->getParent());

        // null by search
        $journal->setDetailsRegion(99);
        $found = $this->repo->getJournalDetailsCountryTaxonomy($journal);
        $this->assertNull($found);

        // null by value
        $journal->setDetailsRegion(0);
        $found = $this->repo->getJournalDetailsCountryTaxonomy($journal);
        $this->assertNull($found);
    }
}
