<?php

namespace App\Tests\Unit\Repository;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Shared methods for JournalRepositoryUnitTest and ConferenceRepositoryUnitTest
 */
trait ConferenceJournalRepositoryTestTrait
{
    private function __setupMocks($class, $em = null)
    {
        $reflection = new \ReflectionClass($class);

        if (null === $em) {
            $em = $this->createMock(EntityManagerInterface::class);
        }
        $reflProp = $reflection->getProperty('_em');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->repo, $em);
    }

    private function getSearchConditionBinderValue($class)
    {
        $reflection = new \ReflectionClass($class);

        $reflProp = $reflection->getProperty('searchConditionBinder');
        $reflProp->setAccessible(true);

        return $reflProp->getValue($this->repo);
    }

    private function _search($expectedSql, $executeResult, $fetchResult, $expectedPersistEntity, $expectedFindByArgs)
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->at(0))->method('getConnection')->will($this->returnCallback(function() use ($expectedSql, $executeResult, $fetchResult, $expectedFindByArgs) {
            $conn = $this->createMock(\Doctrine\DBAL\Connection::class);
            $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($expectedSql, $executeResult, $fetchResult)  {
                $sql         = (string) preg_replace('/\s/', '', $sql);
                $expectedSql = (string) preg_replace('/\s/', '', $expectedSql);
                $this->assertSame($expectedSql, $sql);

                $stmt = $this->createMock(\Doctrine\DBAL\Driver\Statement::class);
                $stmt->expects($this->at(0))->method('execute')->will($this->returnValue($executeResult));
                $stmt->expects($this->at(1))->method('fetchAll')->will($this->returnValue($fetchResult));

                return $stmt;
            }));
            return $conn;
        }));

        // EM findBy()
        $em->expects($this->at(1))->method('getUnitOfWork')->will($this->returnCallback(function() use ($expectedPersistEntity, $expectedFindByArgs) {
            $uow = $this->createMock(\Doctrine\ORM\UnitOfWork::class);
            $uow->expects($this->at(0))->method('getEntityPersister')->will($this->returnCallback(function($_entityName) use ($expectedPersistEntity, $expectedFindByArgs) {
                $this->assertSame($expectedPersistEntity, $_entityName);

                $persister = $this->createMock(\Doctrine\ORM\Persisters\Entity\EntityPersister::class);
                $persister->expects($this->at(0))->method('loadAll')->will($this->returnCallback(function($criteria, $orderBy, $limit, $offset) use ($expectedFindByArgs) {
                    $this->assertSame($expectedFindByArgs, [$criteria, $orderBy, $limit, $offset]);

                    return [];
                }));
                return $persister;
            }));
            return $uow;
        }));

        return $em;
    }

    private function _generalQueryAndFetchCollumn($expectedSql, $executeResult, $fetchResult)
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->at(0))->method('getConnection')->will($this->returnCallback(function() use ($expectedSql, $executeResult, $fetchResult) {
            $conn = $this->createMock(\Doctrine\DBAL\Connection::class);
            $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($expectedSql, $executeResult, $fetchResult)  {
                $this->assertSame($expectedSql, $sql);

                $stmt = $this->createMock(\Doctrine\DBAL\Driver\Statement::class);
                $stmt->expects($this->at(0))->method('execute')->will($this->returnValue($executeResult));
                $stmt->expects($this->at(1))->method('fetchColumn')->will($this->returnValue($fetchResult));

                return $stmt;
            }));
            return $conn;
        }));

        return $em;
    }
}
