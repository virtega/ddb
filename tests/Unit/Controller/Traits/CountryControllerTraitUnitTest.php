<?php

namespace App\Tests\Unit\Controller\Traits;

use App\Controller\Traits\CountryControllerTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Controller-Traits | CountryControllerTrait
 */
class CountryControllerTraitUnitTest extends WebTestCase
{
    use CountryControllerTrait;

    /**
     * @var Mocked EntityManager|null
     */
    private $em;

    /**
     * @dataProvider getSelectionCitiesStacksProvider
     *
     * @testdox Checking __getSelectionCities() Stacks
     */
    public function test__getSelectionCitiesStacks($selected, $foundCities, $expected)
    {
        $this->setupEmCountryGetDistictCapitalCities($foundCities);

        $result = $this->__getSelectionCities($selected);

        $this->assertSame($expected, $result);
    }

    public function getSelectionCitiesStacksProvider()
    {
        return [
            // #0
            [
                [],
                true,
                [
                    ['name' => 'Buenos Aires', 'selected' => false],
                    ['name' => 'Harare',       'selected' => false],
                    ['name' => 'Minsk',        'selected' => false],
                    ['name' => 'San Juan',     'selected' => false],
                ],
            ],
            // #1
            [
                [],
                false,
                [],
            ],
            // #2
            [
                ['minsk ', 'AQ'],
                true,
                [
                    ['name' => 'Buenos Aires', 'selected' => false],
                    ['name' => 'Harare',       'selected' => false],
                    ['name' => 'Minsk',        'selected' => true ],
                    ['name' => 'San Juan',     'selected' => false],
                    ['name' => 'AQ',           'selected' => true ],
                ],
            ],
            // #3
            [
                ['TQ'],
                true,
                [
                    ['name' => 'Buenos Aires', 'selected' => false],
                    ['name' => 'Harare',       'selected' => false],
                    ['name' => 'Minsk',        'selected' => false],
                    ['name' => 'San Juan',     'selected' => false],
                    ['name' => 'TQ',           'selected' => true ],
                ],
            ],
            // #4
            [
                ['TQ'],
                false,
                [
                    ['name' => 'TQ', 'selected' => true],
                ],
            ],
            // #5
            [
                ['Minsk', 'Neverland'],
                true,
                [
                    ['name' => 'Buenos Aires', 'selected' => false],
                    ['name' => 'Harare',       'selected' => false],
                    ['name' => 'Minsk',        'selected' => true ],
                    ['name' => 'San Juan',     'selected' => false],
                    ['name' => 'Neverland',    'selected' => true ],
                ],
            ],
            // #6
            [
                ['   mINSK    ', 'san juan'],
                true,
                [
                    ['name' => 'Buenos Aires', 'selected' => false],
                    ['name' => 'Harare',       'selected' => false],
                    ['name' => 'Minsk',        'selected' => true ],
                    ['name' => 'San Juan',     'selected' => true ],
                ],
            ],
            // #7
            [
                ['Buenos Aires'],
                true,
                [
                    ['name' => 'Buenos Aires', 'selected' => true ],
                    ['name' => 'Harare',       'selected' => false],
                    ['name' => 'Minsk',        'selected' => false],
                    ['name' => 'San Juan',     'selected' => false],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getSelectionCountriesStacksProvider
     *
     * @testdox Checking __getSelectionCountries() Stacks
     */
    public function test__getSelectionCountriesStacks($selected, $foundCountries, $expected)
    {
        $this->setupEmCountryGetCountryList($foundCountries);

        $result = $this->__getSelectionCountries($selected);

        $this->assertSame($expected, $result);
    }

    public function getSelectionCountriesStacksProvider()
    {
        return [
            // #0
            [
                [],
                true,
                [
                    ['isocode' => 'AR', 'name' => 'Argentina',   'selected' => false],
                    ['isocode' => 'AQ', 'name' => 'Antarctica',  'selected' => false],
                    ['isocode' => 'BY', 'name' => 'Belarus',     'selected' => false],
                    ['isocode' => 'PR', 'name' => 'Puerto Rico', 'selected' => false],
                    ['isocode' => 'ZW', 'name' => 'Zimbabwe',    'selected' => false],
                ],
            ],
            // #1
            [
                [],
                false,
                [],
            ],
            // #2
            [
                ['AQ', 'PR', 'TQ'],
                true,
                [
                    ['isocode' => 'AR', 'name' => 'Argentina',   'selected' => false],
                    ['isocode' => 'AQ', 'name' => 'Antarctica',  'selected' => true ],
                    ['isocode' => 'BY', 'name' => 'Belarus',     'selected' => false],
                    ['isocode' => 'PR', 'name' => 'Puerto Rico', 'selected' => true ],
                    ['isocode' => 'ZW', 'name' => 'Zimbabwe',    'selected' => false],
                ],
            ],
            // #3
            [
                ['TQ'],
                true,
                [
                    ['isocode' => 'AR', 'name' => 'Argentina',   'selected' => false],
                    ['isocode' => 'AQ', 'name' => 'Antarctica',  'selected' => false],
                    ['isocode' => 'BY', 'name' => 'Belarus',     'selected' => false],
                    ['isocode' => 'PR', 'name' => 'Puerto Rico', 'selected' => false],
                    ['isocode' => 'ZW', 'name' => 'Zimbabwe',    'selected' => false],
                ],
            ],
            // #4
            [
                ['TQ'],
                false,
                [],
            ],
        ];
    }

    /**
     * @dataProvider getSelectionRegionsStacksProvider
     *
     * @testdox Checking __getSelectionRegions() Stacks
     */
    public function test__getSelectionRegionsStacks($selected, $foundRegions, $expected)
    {
        $regions = [
            5   => ['Americas',               null],
            501 => ['America',                5],
            550 => ['Latin America',          5],
            7   => ['Asia',                   null],
            864 => ['Southeast Asia',         7],
            965 => ['Southeast Central Asia', 864],
        ];

        foreach ($regions as $ri => $region) {
            $regions[$ri] = new \App\Entity\CountryTaxonomy;
            $regions[$ri]->setName($region[0]);
            $regions[$ri]->setParent($regions[$region[1]] ?? null);

            $reflection = new \ReflectionClass(\App\Entity\CountryTaxonomy::class);
            $reflProp = $reflection->getProperty('id');
            $reflProp->setAccessible(true);
            $reflProp->setValue($regions[$ri], $ri);
        }

        // reorder by getAllLevelRegions()
        $regions = [
            [
                $regions[5],
                $regions[501],
            ],
            [
                $regions[5],
                $regions[550],
            ],
            [
                $regions[7],
                $regions[864],
                $regions[965],
            ],
        ];

        $repo = $this->createMock(\App\Repository\CountryTaxonomyRepository::class);
        $repo->method('getAllLevelRegions')
            ->will($this->returnValue(($foundRegions ? $regions : [])));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        $result = $this->__getSelectionRegions($selected);

        $this->assertSame($expected, $result);
    }

    public function getSelectionRegionsStacksProvider()
    {
        return [
            // #0
            [
                [],
                true,
                [
                    5   => ['id' => 5,   'name' => 'Americas',               'caption' => 'Americas',                  'top' => true,  'selected' => false],
                    501 => ['id' => 501, 'name' => 'America',                'caption' => '> America',                 'top' => false, 'selected' => false],
                    550 => ['id' => 550, 'name' => 'Latin America',          'caption' => '> Latin America',           'top' => false, 'selected' => false],
                    7   => ['id' => 7,   'name' => 'Asia',                   'caption' => 'Asia',                      'top' => true,  'selected' => false],
                    864 => ['id' => 864, 'name' => 'Southeast Asia',         'caption' => '> Southeast Asia',          'top' => false, 'selected' => false],
                    965 => ['id' => 965, 'name' => 'Southeast Central Asia', 'caption' => '>> Southeast Central Asia', 'top' => false, 'selected' => false],
                ],
            ],
            // #1
            [
                [],
                false,
                [],
            ],
            // #2
            [
                [550, 7],
                true,
                [
                    5   => ['id' => 5,   'name' => 'Americas',               'caption' => 'Americas',                  'top' => true,  'selected' => false],
                    501 => ['id' => 501, 'name' => 'America',                'caption' => '> America',                 'top' => false, 'selected' => false],
                    550 => ['id' => 550, 'name' => 'Latin America',          'caption' => '> Latin America',           'top' => false, 'selected' => true ],
                    7   => ['id' => 7,   'name' => 'Asia',                   'caption' => 'Asia',                      'top' => true,  'selected' => true ],
                    864 => ['id' => 864, 'name' => 'Southeast Asia',         'caption' => '> Southeast Asia',          'top' => false, 'selected' => false],
                    965 => ['id' => 965, 'name' => 'Southeast Central Asia', 'caption' => '>> Southeast Central Asia', 'top' => false, 'selected' => false],
                ],
            ],
            // #3
            [
                [864, 10],
                true,
                [
                    5   => ['id' => 5,   'name' => 'Americas',               'caption' => 'Americas',                  'top' => true,  'selected' => false],
                    501 => ['id' => 501, 'name' => 'America',                'caption' => '> America',                 'top' => false, 'selected' => false],
                    550 => ['id' => 550, 'name' => 'Latin America',          'caption' => '> Latin America',           'top' => false, 'selected' => false],
                    7   => ['id' => 7,   'name' => 'Asia',                   'caption' => 'Asia',                      'top' => true,  'selected' => false],
                    864 => ['id' => 864, 'name' => 'Southeast Asia',         'caption' => '> Southeast Asia',          'top' => false, 'selected' => true ],
                    965 => ['id' => 965, 'name' => 'Southeast Central Asia', 'caption' => '>> Southeast Central Asia', 'top' => false, 'selected' => false],
                ],
            ],
        ];
    }

    private function setupEmCountryGetCountryList($found)
    {
        $countries = [
            'AR' => ['Argentina',   'Buenos Aires'],
            'AQ' => ['Antarctica',   null],
            'BY' => ['Belarus',     'Minsk'],
            'PR' => ['Puerto Rico', 'San Juan'],
            'ZW' => ['Zimbabwe',    'Harare'],
        ];

        foreach ($countries as $ci => $city) {
            $countries[$ci] = new \App\Entity\Country;
            $countries[$ci]->setName($city[0]);
            $countries[$ci]->setCapitalCity($city[1]);

            $reflection = new \ReflectionClass(\App\Entity\Country::class);
            $reflProp = $reflection->getProperty('isoCode');
            $reflProp->setAccessible(true);
            $reflProp->setValue($countries[$ci], $ci);
        }

        $repo = $this->createMock(\App\Repository\CountryRepository::class);
        $repo->method('getCountryList')
            ->will($this->returnValue(($found ? $countries : [])));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));
    }

    private function setupEmCountryGetDistictCapitalCities($found)
    {
        $countries = [
            'Buenos Aires',
            'Harare',
            'Minsk',
            'San Juan',
        ];

        $repo = $this->createMock(\App\Repository\CountryRepository::class);
        $repo->method('getDistictCapitalCities')
            ->will($this->returnValue(($found ? $countries : [])));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));
    }
}
