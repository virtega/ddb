<?php

namespace App\Tests\Unit\Controller\Traits;

use App\Controller\Traits\BasicFormControllerTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Controller-Traits | BasicFormControllerTrait
 */
class BasicFormControllerTraitUnitTest extends WebTestCase
{
    use BasicFormControllerTrait;

    /**
     * @dataProvider getRadioboxSetSettingsStacksProvider
     *
     * @testdox Checking __getRadioboxSetSettings() Stacks
     */
    public function test__getRadioboxSetSettingsStacks($selected, $options, $expected)
    {
        $result = $this->__getRadioboxSetSettings($selected, $options);

        $this->assertSame($expected, $result);
    }

    public function getRadioboxSetSettingsStacksProvider()
    {
        return [
            // #0
            [
                '',
                ['' => 'Any', 'yes' => 'Yes', 'no' => 'No'],
                [
                    ['value' => '',    'caption' => 'Any', 'checked' => true ],
                    ['value' => 'yes', 'caption' => 'Yes', 'checked' => false],
                    ['value' => 'no',  'caption' => 'No',  'checked' => false],
                ]
            ],
            // #1
            [
                'yes',
                null,
                [
                    ['value' => 'yes', 'caption' => 'Yes', 'checked' => true ],
                    ['value' => 'no',  'caption' => 'No',  'checked' => false],
                ]
            ],
            // #2
            [
                'no',
                ['' => 'Any', 'yes' => 'Yes', 'no' => 'No'],
                [
                    ['value' => '',    'caption' => 'Any', 'checked' => false],
                    ['value' => 'yes', 'caption' => 'Yes', 'checked' => false],
                    ['value' => 'no',  'caption' => 'No',  'checked' => true ],
                ]
            ],
            // #3
            [
                '33',
                [0 => 'Any', 11 => 'Yes', 33 => 'No'],
                [
                    ['value' => 0,  'caption' => 'Any', 'checked' => false],
                    ['value' => 11, 'caption' => 'Yes', 'checked' => false],
                    ['value' => 33, 'caption' => 'No',  'checked' => true ],
                ]
            ],
            // #4
            [
                'injected',
                null,
                [
                    ['value' => 'yes', 'caption' => 'Yes', 'checked' => false],
                    ['value' => 'no',  'caption' => 'No',  'checked' => false],
                ]
            ],
            // #5
            [
                'soul',
                [
                    'soul' => 'Soul',
                    'trap' => 'Trap',
                ],
                [
                    ['value' => 'soul', 'caption' => 'Soul', 'checked' => true ],
                    ['value' => 'trap', 'caption' => 'Trap', 'checked' => false],
                ]
            ],
        ];
    }

    /**
     * @dataProvider getSelectionLanguagesStacksProvider
     *
     * @testdox Checking __getSelectionLanguages() Stacks
     */
    public function test__getSelectionLanguagesStacks($selected, $expected)
    {
        $result = $this->__getSelectionLanguages($selected);

        $this->assertSame($expected, $result);
    }

    public function getSelectionLanguagesStacksProvider()
    {
        return [
            // #0
            [
                [],
                [
                    ['value' => 'Afrikaans',      'selected' => false],
                    ['value' => 'Arabic',         'selected' => false],
                    ['value' => 'Bulgarian',      'selected' => false],
                    ['value' => 'Catalan',        'selected' => false],
                    ['value' => 'Chinese',        'selected' => false],
                    ['value' => 'Czech',          'selected' => false],
                    ['value' => 'Danish',         'selected' => false],
                    ['value' => 'Dutch',          'selected' => false],
                    ['value' => 'English',        'selected' => false],
                    ['value' => 'French',         'selected' => false],
                    ['value' => 'German',         'selected' => false],
                    ['value' => 'Greek',          'selected' => false],
                    ['value' => 'Hausa',          'selected' => false],
                    ['value' => 'Hebrew',         'selected' => false],
                    ['value' => 'Hindustani',     'selected' => false],
                    ['value' => 'Hungarian',      'selected' => false],
                    ['value' => 'Italian',        'selected' => false],
                    ['value' => 'Japanese',       'selected' => false],
                    ['value' => 'Korean',         'selected' => false],
                    ['value' => 'Malay',          'selected' => false],
                    ['value' => 'Norwegian',      'selected' => false],
                    ['value' => 'Persian',        'selected' => false],
                    ['value' => 'Polish',         'selected' => false],
                    ['value' => 'Portuguese',     'selected' => false],
                    ['value' => 'Quechua',        'selected' => false],
                    ['value' => 'Russian',        'selected' => false],
                    ['value' => 'Serbo-Croatian', 'selected' => false],
                    ['value' => 'Slovak',         'selected' => false],
                    ['value' => 'Sotho',          'selected' => false],
                    ['value' => 'Spanish',        'selected' => false],
                    ['value' => 'Swahili',        'selected' => false],
                    ['value' => 'Swedish',        'selected' => false],
                    ['value' => 'Tamil',          'selected' => false],
                    ['value' => 'Thai',           'selected' => false],
                    ['value' => 'Turkish',        'selected' => false],
                    ['value' => 'Ukrainian',      'selected' => false],
                    ['value' => 'Yoruba',         'selected' => false],
                ]
            ],
            // #1
            [
                ['Danish', 'Korean', 'Swedish'],
                [
                    ['value' => 'Afrikaans',      'selected' => false],
                    ['value' => 'Arabic',         'selected' => false],
                    ['value' => 'Bulgarian',      'selected' => false],
                    ['value' => 'Catalan',        'selected' => false],
                    ['value' => 'Chinese',        'selected' => false],
                    ['value' => 'Czech',          'selected' => false],
                    ['value' => 'Danish',         'selected' => true ],
                    ['value' => 'Dutch',          'selected' => false],
                    ['value' => 'English',        'selected' => false],
                    ['value' => 'French',         'selected' => false],
                    ['value' => 'German',         'selected' => false],
                    ['value' => 'Greek',          'selected' => false],
                    ['value' => 'Hausa',          'selected' => false],
                    ['value' => 'Hebrew',         'selected' => false],
                    ['value' => 'Hindustani',     'selected' => false],
                    ['value' => 'Hungarian',      'selected' => false],
                    ['value' => 'Italian',        'selected' => false],
                    ['value' => 'Japanese',       'selected' => false],
                    ['value' => 'Korean',         'selected' => true ],
                    ['value' => 'Malay',          'selected' => false],
                    ['value' => 'Norwegian',      'selected' => false],
                    ['value' => 'Persian',        'selected' => false],
                    ['value' => 'Polish',         'selected' => false],
                    ['value' => 'Portuguese',     'selected' => false],
                    ['value' => 'Quechua',        'selected' => false],
                    ['value' => 'Russian',        'selected' => false],
                    ['value' => 'Serbo-Croatian', 'selected' => false],
                    ['value' => 'Slovak',         'selected' => false],
                    ['value' => 'Sotho',          'selected' => false],
                    ['value' => 'Spanish',        'selected' => false],
                    ['value' => 'Swahili',        'selected' => false],
                    ['value' => 'Swedish',        'selected' => true ],
                    ['value' => 'Tamil',          'selected' => false],
                    ['value' => 'Thai',           'selected' => false],
                    ['value' => 'Turkish',        'selected' => false],
                    ['value' => 'Ukrainian',      'selected' => false],
                    ['value' => 'Yoruba',         'selected' => false],
                ]
            ],
            // #2
            [
                [' German   ', 'ITALIAN', 'swedish', '   ', 'Sowa  ', '  Klallam'],
                [
                    ['value' => 'Afrikaans',      'selected' => false],
                    ['value' => 'Arabic',         'selected' => false],
                    ['value' => 'Bulgarian',      'selected' => false],
                    ['value' => 'Catalan',        'selected' => false],
                    ['value' => 'Chinese',        'selected' => false],
                    ['value' => 'Czech',          'selected' => false],
                    ['value' => 'Danish',         'selected' => false],
                    ['value' => 'Dutch',          'selected' => false],
                    ['value' => 'English',        'selected' => false],
                    ['value' => 'French',         'selected' => false],
                    ['value' => 'German',         'selected' => true ],
                    ['value' => 'Greek',          'selected' => false],
                    ['value' => 'Hausa',          'selected' => false],
                    ['value' => 'Hebrew',         'selected' => false],
                    ['value' => 'Hindustani',     'selected' => false],
                    ['value' => 'Hungarian',      'selected' => false],
                    ['value' => 'Italian',        'selected' => true ],
                    ['value' => 'Japanese',       'selected' => false],
                    ['value' => 'Korean',         'selected' => false],
                    ['value' => 'Malay',          'selected' => false],
                    ['value' => 'Norwegian',      'selected' => false],
                    ['value' => 'Persian',        'selected' => false],
                    ['value' => 'Polish',         'selected' => false],
                    ['value' => 'Portuguese',     'selected' => false],
                    ['value' => 'Quechua',        'selected' => false],
                    ['value' => 'Russian',        'selected' => false],
                    ['value' => 'Serbo-Croatian', 'selected' => false],
                    ['value' => 'Slovak',         'selected' => false],
                    ['value' => 'Sotho',          'selected' => false],
                    ['value' => 'Spanish',        'selected' => false],
                    ['value' => 'Swahili',        'selected' => false],
                    ['value' => 'Swedish',        'selected' => true ],
                    ['value' => 'Tamil',          'selected' => false],
                    ['value' => 'Thai',           'selected' => false],
                    ['value' => 'Turkish',        'selected' => false],
                    ['value' => 'Ukrainian',      'selected' => false],
                    ['value' => 'Yoruba',         'selected' => false],
                    ['value' => 'Sowa',           'selected' => true ],
                    ['value' => 'Klallam',        'selected' => true ],
                ]
            ],
        ];
    }

    /**
     * @dataProvider getSelectionJournalStatusesStacksProvider
     *
     * @testdox Checking __getSelectionJournalStatuses() Stacks
     */
    public function test__getSelectionJournalStatusesStacks($selected, $expected)
    {
        $result = $this->__getSelectionJournalStatuses($selected, true);

        $this->assertSame($expected, $result);
    }

    /**
     * @dataProvider getSelectionContactsRepeaterFieldsStacksProvider
     *
     * @testdox Checking __getSelectionContactsRepeaterFields() Stacks
     */
    public function test__getSelectionContactsRepeaterFieldsStacks($input, $expected)
    {
        $result = $this->__getSelectionContactsRepeaterFields($input);

        $this->assertSame($expected, $result);
    }

    public function getSelectionContactsRepeaterFieldsStacksProvider()
    {
        return [
            // #0
            [
                [],
                [],
            ],
            // #1
            [
                [
                    ['input' => '123456  '],
                    ['input' => '  345456'],
                ],
                [
                    '123456',
                    '345456'
                ],
            ],
            // #2
            [
                [
                    ['value' => '123456   '],
                    ['value' => '  345456'],
                ],
                [],
            ],
            // #3
            [
                [
                    ['input' => 'a'],
                    ['input' => ' b'],
                    ['input' => 'a    '],
                    ['input' => 'c  '],
                ],
                ['a', 'b', 'a', 'c'],
            ],
        ];
    }

    /**
     * @dataProvider getSelectionLinksRepeaterFieldsStacksProvider
     *
     * @testdox Checking __getSelectionLinksRepeaterFields() Stacks
     */
    public function test__getSelectionLinksRepeaterFieldsStacks($input, $expected)
    {
        $result = $this->__getSelectionLinksRepeaterFields($input);

        $this->assertSame($expected, $result);
    }

    public function getSelectionLinksRepeaterFieldsStacksProvider()
    {
        return [
            // #0
            [
                [],
                [],
            ],
            // #1
            [
                [
                    [
                        'url'     => 'http://google.com/?q=stay1',
                        'caption' => 'Test 1',
                    ],
                    [
                        'url'     => 'http://google.com/?q=stay2',
                        'caption' => '',
                    ],
                    [
                        'url'     => 'ftp://google.com/?q=stay3',
                        'caption' => '',
                    ],
                    [
                        'url'     => 'Test 4',
                        'caption' => '',
                    ],
                ],
                [
                    [
                        'caption' => 'Test 1',
                        'url'     => 'http://google.com/?q=stay1',
                    ],
                    [
                        'caption' => 'google.com',
                        'url'     => 'http://google.com/?q=stay2',
                    ],
                    [
                        'caption' => 'google.com',
                        'url'     => 'ftp://google.com/?q=stay3',
                    ],
                    [
                        'caption' => 'Unknown',
                        'url'     => 'Test 4',
                    ],
                ],
            ],
        ];
    }

    public function getSelectionJournalStatusesStacksProvider()
    {
        return [
            // #0
            [
                '',
                [
                    ['value' => '',  'caption' => 'Any',      'selected' => true ],
                    ['value' => '0', 'caption' => 'None',     'selected' => false],
                    ['value' => '1', 'caption' => 'Active',   'selected' => false],
                    ['value' => '5', 'caption' => 'Archived', 'selected' => false],
                ]
            ],
            // #1
            [
                '0',
                [
                    ['value' => '',  'caption' => 'Any',      'selected' => false],
                    ['value' => '0', 'caption' => 'None',     'selected' => true ],
                    ['value' => '1', 'caption' => 'Active',   'selected' => false],
                    ['value' => '5', 'caption' => 'Archived', 'selected' => false],
                ]
            ],
            // #2
            [
                0,
                [
                    ['value' => '',  'caption' => 'Any',      'selected' => false],
                    ['value' => '0', 'caption' => 'None',     'selected' => true ],
                    ['value' => '1', 'caption' => 'Active',   'selected' => false],
                    ['value' => '5', 'caption' => 'Archived', 'selected' => false],
                ]
            ],
            // #3
            [
                '5',
                [
                    ['value' => '',  'caption' => 'Any',      'selected' => false],
                    ['value' => '0', 'caption' => 'None',     'selected' => false],
                    ['value' => '1', 'caption' => 'Active',   'selected' => false],
                    ['value' => '5', 'caption' => 'Archived', 'selected' => true ],
                ]
            ],
            // #4
            [
                'inject',
                [
                    ['value' => '',  'caption' => 'Any',      'selected' => false],
                    ['value' => '0', 'caption' => 'None',     'selected' => false],
                    ['value' => '1', 'caption' => 'Active',   'selected' => false],
                    ['value' => '5', 'caption' => 'Archived', 'selected' => false],
                ]
            ],
        ];
    }

    /**
     * @dataProvider getfilterSelectionSelectedStacksStacksProvider
     *
     * @testdox Checking __filterSelectionSelected() Stacks
     */
    public function test__filterSelectionSelectedStacks($set, $selected, $name, $expected)
    {
        $result = $this->__filterSelectionSelected($set, $selected, $name);

        $this->assertSame($expected, $result);
    }

    public function getfilterSelectionSelectedStacksStacksProvider()
    {
        return [
            // #0
            [
                [],
                'selected',
                'name',
                [],
            ],
            // #1
            [
                [
                    ['a' => 1, 'b' => 2, 'c' => true],
                    ['a' => 3, 'b' => 4, 'c' => true],
                    ['a' => 6, 'b' => 7, 'c' => true],
                ],
                'c',
                'a',
                [1, 3, 6],
            ],
            // #2
            [
                [
                    ['id' => 2, 'name' => 'Cat 001', 'selected' => true ],
                    ['id' => 4, 'name' => 'Cat 002', 'selected' => false],
                    ['id' => 7, 'name' => 'Cat 003', 'selected' => true ],
                ],
                'selected',
                'name',
                ['Cat 001', 'Cat 003'],
            ],
            // #3
            [
                [
                    ['id' => 2, 'name' => 'Cat 001', 'selected' => true ],
                    ['id' => 4, 'name' => 'Cat 002', 'selected' => false],
                    ['id' => 7, 'name' => 'Cat 003', 'selected' => true ],
                ],
                'selected',
                'name',
                ['Cat 001', 'Cat 003'],
            ],
            // #4
            [
                [
                    ['id' => 2, 'name' => 'Cat 001', 'selected' => true ],
                    ['id' => 4, 'name' => 'Cat 002', 'selected' => false],
                    ['id' => 7, 'name' => 'Cat 003', 'selected' => true ],
                ],
                'selected',
                'guid', // not exits
                [],
            ],
            // #5
            [
                [
                    ['id' => 2, 'name' => 'Cat 001', 'selected' => true ],
                    ['id' => 4, 'name' => 'Cat 002', 'selected' => false],
                    ['id' => 7, 'name' => 'Cat 003', 'selected' => true ],
                ],
                'masking', // not exits
                'id',
                [],
            ],
            // #6
            [
                [
                    ['id' => 2, 'name' => 'Cat 001', 'selected' => true ],
                    ['id' => 4, 'name' => 'Cat 002', 'selected' => false],
                    ['id' => 7, 'name' => 'Cat 003', 'selected' => true ],
                ],
                'id', // not boolean
                'name',
                ['Cat 001', 'Cat 002', 'Cat 003'],
            ],
            // #7
            [
                [
                    ['fid' => 'MCD', 'name' => 'Cat 001', 'selected' => true ],
                    ['fid' => 'BK',  'name' => 'Cat 002', 'selected' => false],
                    ['fid' => '',    'name' => 'Cat 003', 'selected' => true ],
                ],
                'fid', // not boolean
                'name',
                ['Cat 001', 'Cat 002'],
            ],
            // #8
            [
                [
                    ['id' => 2, 'name' => 'Cat 001', 'selected' => true ],
                    ['id' => 4, 'name' => 'Cat 002', 'selected' => false],
                    ['id' => 7, 'selected' => true ],
                ],
                'selected',
                'name', // 1 missing
                [],
            ],
            // #9
            [
                [
                    ['id' => 2, 'name' => 'Cat 001', 'selected' => true ],
                    ['id' => 4, 'name' => 'Cat 002', 'selected' => false],
                    ['id' => 7, 'name' => 'Cat 003'                     ],
                ],
                'selected', // 1 missing, considered as false
                'name',
                ['Cat 001'],
            ],
            // #10
            [
                [
                    ['ok' => false, 'name' => 'Cat 001', 'selected' => true ],
                    ['ok' => true , 'name' => 'Cat 002', 'selected' => false],
                    ['ok' => false, 'name' => 'Cat 003', 'selected' => true ],
                ],
                'selected',
                'ok', // boolean / non-unique
                [],
            ],
            // #11
            [
                [
                    ['fid' => 'MCD', 'name' => 'Cat 001', 'selected' => true ],
                    ['fid' => 'BK',  'name' => 'Cat 002', 'selected' => false],
                    ['fid' => '',    'name' => 'Cat 003', 'selected' => true ],
                    ['fid' => 'MCD', 'name' => 'Cat 004', 'selected' => true ],
                ],
                'selected',
                'fid', // not boolean / non-unique
                [],
            ],
        ];
    }

    /**
     * @dataProvider getParsingAndSanitizeUrlStacksStacksProvider
     *
     * @testdox Checking __parsingAndSanitizeUrl() Stacks
     */
    public function test__parsingAndSanitizeUrlStacks($uri, $expected)
    {
        $result = $this->__parsingAndSanitizeUrl($uri);

        $this->assertSame($expected, $result);
    }

    public function getParsingAndSanitizeUrlStacksStacksProvider()
    {
        return [
            // #0
            [
                '',
                null,
            ],
            // #1
            [
                null,
                null,
            ],
            // #2
            [
                'http://www.google.com/search?q=loop',
                [
                    'host' => 'google.com',
                    'path' => 'search',
                ],
            ],
            // #3
            [
                'https://google.com.au/?q=loop',
                [
                    'host' => 'google.com.au',
                    'path' => null,
                ],
            ],
            // #4
            [
                'http://google.com.nz/feature/wait/barrel-roll/',
                [
                    'host' => 'google.com.nz',
                    'path' => 'barrel-roll',
                ],
            ],
            // #5
            [
                'https://nz.gov.education.com.nz/feature/wait/barrel-roll/execute.js',
                [
                    'host' => 'nz.gov.education.com.nz',
                    'path' => 'execute.js',
                ],
            ],
            // #6
            [
                'https://w3.coca-cola.org/3-in-1-version',
                [
                    'host' => 'coca-cola.org',
                    'path' => '3-in-1-version',
                ],
            ],
            // #7
            [
                'ftp://cloud.storage.somewhere.tz/secure-location',
                [
                    'host' => 'cloud.storage.somewhere.tz',
                    'path' => 'secure-location',
                ],
            ],
            // #8
            [
                '://example.com/file',
                null,
            ],
            // #9
            [
                'example.com/file',
                [
                    'host' => 'example.com',
                    'path' => 'file',
                ],
            ],
            // #10
            [
                'http://example.com/file ; http://example.com/file',
                [
                    'host' => 'example.com',
                    'path' => 'file',
                ],
            ],
            // #11
            [
                'HTTP://WWW.EXAMPLE.COM/PATH1/PATH2/PATH3',
                [
                    'host' => 'example.com',
                    'path' => 'path3',
                ],
            ],
            // #12
            [
                'schema://dns3.EXAMPLE.COM/',
                [
                    'host' => 'dns3.example.com',
                    'path' => null,
                ],
            ],
            // #13
            [
                'error://dns3.example.com?gone=1#smile',
                [
                    'host' => 'dns3.example.com',
                    'path' => null,
                ],
            ],
            // #14
            [
                'www.cancer.orgh',
                [
                    'host' => 'cancer.orgh',
                    'path' => null,
                ],
            ],
            // #15
            [
                'marrieann.com.au',
                [
                    'host' => 'marrieann.com.au',
                    'path' => null,
                ],
            ],
        ];
    }
}
