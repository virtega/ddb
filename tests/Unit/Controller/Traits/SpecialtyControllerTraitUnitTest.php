<?php

namespace App\Tests\Unit\Controller\Traits;

use App\Controller\Traits\SpecialtyControllerTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Controller-Traits | SpecialtyControllerTrait
 */
class SpecialtyControllerTraitUnitTest extends WebTestCase
{
    use SpecialtyControllerTrait;

    /**
     * @var Mocked EntityManager|null
     */
    private $em;

    /**
     * @dataProvider getSelectionSpecialtiesStacksProvider
     *
     * @testdox Checking __getSelectionSpecialties() Stacks
     */
    public function test__getSelectionSpecialtiesStacks($selected, $foundSpecialties, $expected)
    {
        $specialties = [
            1  => "Anaesthesia",
            4  => "Dental and Oral Disorders",
            9  => "Emergency care",
            10 => "Genitourinary disorders",
            12 => "Gynaecology and obstetrics",
        ];
        foreach ($specialties as $si => $specialty) {
            $specialties[$si] = new \App\Entity\SpecialtyTaxonomy;
            $specialties[$si]->setSpecialty($specialty);

            $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
            $reflProp = $reflection->getProperty('id');
            $reflProp->setAccessible(true);
            $reflProp->setValue($specialties[$si], $si);
        }

        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->method('findBy')
            ->will($this->returnValue(($foundSpecialties ? $specialties : [])));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        $result = $this->__getSelectionSpecialties($selected);

        $this->assertSame($expected, $result);
    }

    public function getSelectionSpecialtiesStacksProvider()
    {
        return [
            // #0
            [
                [],
                true,
                [
                    ['id' => 1,  'name' => 'Anaesthesia',                'selected' => false],
                    ['id' => 4,  'name' => 'Dental and Oral Disorders',  'selected' => false],
                    ['id' => 9,  'name' => 'Emergency care',             'selected' => false],
                    ['id' => 10, 'name' => 'Genitourinary disorders',    'selected' => false],
                    ['id' => 12, 'name' => 'Gynaecology and obstetrics', 'selected' => false],
                ],
            ],
            // #1
            [
                ['1', 'Test1'],
                true,
                [
                    ['id' => 1,       'name' => 'Anaesthesia',                'selected' => true ],
                    ['id' => 4,       'name' => 'Dental and Oral Disorders',  'selected' => false],
                    ['id' => 9,       'name' => 'Emergency care',             'selected' => false],
                    ['id' => 10,      'name' => 'Genitourinary disorders',    'selected' => false],
                    ['id' => 12,      'name' => 'Gynaecology and obstetrics', 'selected' => false],
                    ['id' => 'Test1', 'name' => 'Test1',                      'selected' => true],
                ],
            ],
            // #2
            [
                ['1', 9, 'Test2'],
                true,
                [
                    ['id' => 1,       'name' => 'Anaesthesia',                'selected' => true ],
                    ['id' => 4,       'name' => 'Dental and Oral Disorders',  'selected' => false],
                    ['id' => 9,       'name' => 'Emergency care',             'selected' => true],
                    ['id' => 10,      'name' => 'Genitourinary disorders',    'selected' => false],
                    ['id' => 12,      'name' => 'Gynaecology and obstetrics', 'selected' => false],
                    ['id' => 'Test2', 'name' => 'Test2',                      'selected' => true],
                ],
            ],
            // #3
            [
                [1, 2, 3, 4],
                true,
                [
                    ['id' => 1,  'name' => 'Anaesthesia',                'selected' => true ],
                    ['id' => 4,  'name' => 'Dental and Oral Disorders',  'selected' => true ],
                    ['id' => 9,  'name' => 'Emergency care',             'selected' => false],
                    ['id' => 10, 'name' => 'Genitourinary disorders',    'selected' => false],
                    ['id' => 12, 'name' => 'Gynaecology and obstetrics', 'selected' => false],
                    ['id' => 2,  'name' => 2,                            'selected' => true ],
                    ['id' => 3,  'name' => 3,                            'selected' => true ],
                ],
            ],
            // #4
            [
                ['Test3'],
                true,
                [
                    ['id' => 1,       'name' => 'Anaesthesia',                'selected' => false],
                    ['id' => 4,       'name' => 'Dental and Oral Disorders',  'selected' => false],
                    ['id' => 9,       'name' => 'Emergency care',             'selected' => false],
                    ['id' => 10,      'name' => 'Genitourinary disorders',    'selected' => false],
                    ['id' => 12,      'name' => 'Gynaecology and obstetrics', 'selected' => false],
                    ['id' => 'Test3', 'name' => 'Test3',                      'selected' => true],
                ],
            ],
            // #5
            [
                [],
                false,
                [],
            ],
            // #6
            [
                ['1', 9, 'Test2'],
                false,
                [
                    ['id' => '1',     'name' => '1',     'selected' => true],
                    ['id' => 9,       'name' => 9,       'selected' => true],
                    ['id' => 'Test2', 'name' => 'Test2', 'selected' => true],
                ],
            ],
        ];
    }
}
