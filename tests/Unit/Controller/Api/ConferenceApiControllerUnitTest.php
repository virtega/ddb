<?php

namespace App\Tests\Unit\Controller\Api;

use App\Controller\Api\ConferenceApiController;
use App\Service\CoreService;
use Doctrine\ORM\EntityManagerInterface;
use Mockery as M;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @testdox UNIT | Controller | Api | ConferenceApiController
 */
class ConferenceApiControllerUnitTest extends TestCase
{
    /**
     * @var ConferenceApiController
     */
    private $controller;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->cs     = M::mock(CoreService::class);
        $this->em     = M::mock(EntityManagerInterface::class);
        $this->logger = M::mock(LoggerInterface::class);
    }

    /**
     * @testdox Checking Export and with no $_POST['email'] defined Exception
     *
     * @expectedException \App\Exception\ApiIncompleteException
     *
     * @expectedExceptionMessage Invalid Request, Missing Argument
     * @expectedExceptionCode 400
     */
    public function testExportMissingEmail()
    {
        $this->logger->shouldReceive('info');
        $this->logger->shouldReceive('error');

        $this->controller = new ConferenceApiController($this->cs, $this->em, $this->logger);

        $request = M::mock(Request::class);
        $request->request = M::mock(\Symfony\Component\DependencyInjection\ParameterBag::class);
        $request->request->shouldReceive('get')
            ->withArgs(function ($name) {
                self::assertSame('email', $name);
                return true;
            })
            ->andReturn(null);

        try {
            $this->controller->export($request);
        }
        catch (\Exception $e) {
            self::assertSame('email', $e->getApiArgument());
            throw $e;
        }
    }

    /**
     * @testdox Checking Export and with no $_POST['query'] defined Exception
     *
     * @expectedException \App\Exception\ApiIncompleteException
     *
     * @expectedExceptionMessage Invalid Request, Missing Argument
     * @expectedExceptionCode 400
     */
    public function testExportMissingQuery()
    {
        $this->logger->shouldReceive('info');
        $this->logger->shouldReceive('error');

        $this->controller = new ConferenceApiController($this->cs, $this->em, $this->logger);

        $request = M::mock(Request::class);
        $request->request = M::mock(\Symfony\Component\DependencyInjection\ParameterBag::class);
        $request->request->shouldReceive('get')
            ->withArgs(function ($name) {
                $found = in_array($name, ['email', 'query'], true);
                self::assertTrue($found);
                return $found;
            })
            ->andReturnValues([
                'dummy@google.com',
                null,
            ]);

        try {
            $this->controller->export($request);
        }
        catch (\Exception $e) {
            self::assertSame('query', $e->getApiArgument());
            throw $e;
        }
    }

    /**
     * @testdox Checking Export with Dev Response
     */
    public function testExportOkDevResponse()
    {
        $commandString = 'app:conferences-export-data "dummy@google.com" "Cardio" "" "" "" "" "" "" "" "" "" "" "" "" "no"';

        $commandExecStatus = [
            'USER'    => "apache",
            'PID'     => "3216",
            '%CPU'    => "13.5",
            '%MEM'    => "1.3",
            'VSZ'     => "321654",
            'RSS'     => "54562",
            'TTY'     => "?",
            'STAT'    => "S",
            'START'   => "06:38",
            'time'    => "0.00",
            'command' => "/usr/bin/php /var/html/bin/console app:conferences-export-data dummy@google.com Cardio no",
        ];

        $this->logger->shouldReceive('info');

        $this->cs->shouldReceive('triggerCommandProcess')
            ->withArgs(function ($commandWithAttrs, $checkAfterSec) use ($commandString) {
                self::assertSame($commandString, $commandWithAttrs);
                self::assertSame(1.5, $checkAfterSec);
                return true;
            })
            ->andReturn($commandExecStatus);

        $this->controller = new ConferenceApiController($this->cs, $this->em, $this->logger);

        $container = M::mock(\Symfony\Component\DependencyInjection\Container::class);
        $container->shouldReceive('has')->andReturnValues([
            // parameter_bag
            true,
            // serializer
            false,
        ]);

        $paramBag = M::mock(\Symfony\Component\DependencyInjection\ParameterBag::class);
        $paramBag->shouldReceive('get')->andReturn('dev');
        $container->shouldReceive('get')->andReturn($paramBag);
        $this->controller->setContainer($container);

        $request = M::mock(Request::class);
        $request->request = M::mock(\Symfony\Component\DependencyInjection\ParameterBag::class);
        $request->request->shouldReceive('get')
            ->withArgs(function ($name) {
                $found = in_array($name, ['email', 'query'], true);
                self::assertTrue($found);
                return $found;
            })
            ->andReturnValues([
                'dummy@google.com',
                [
                    'name' => 'Cardio',
                ],
            ]);

        $response = $this->controller->export($request);

        self::assertNotEmpty($response);
        self::assertInstanceOf(\Symfony\Component\HttpFoundation\JsonResponse::class, $response);
        self::assertSame(200, $response->getStatusCode());

        $content = $response->getContent();
        $content = json_decode($content, true);

        self::assertArrayHasKey('msg', $content);
        self::assertSame('Processing...', $content['msg']);

        self::assertArrayHasKey('data', $content);
        self::assertArrayHasKey('com', $content['data']);
        self::assertSame($commandString, $content['data']['com']);
        self::assertArrayHasKey('ps', $content['data']);
        self::assertSame($commandExecStatus, $content['data']['ps']);

        self::assertArrayHasKey('count', $content);
        self::assertSame(2, $content['count']);

        self::assertArrayHasKey('success', $content);
        self::assertSame(true, $content['success']);
    }

    /**
     * @testdox Checking Export with Prod Response
     */
    public function testExportOkProdResponse()
    {
        $this->logger->shouldReceive('info');

        $this->cs->shouldReceive('triggerCommandProcess')
            ->withArgs(function ($commandWithAttrs, $checkAfterSec) {
                self::assertSame('app:conferences-export-data "dummy@google.com" "Cardio" "10,11" "25,30" "35" "1,2" "" "" "" "" "" "" "" "" "no"', $commandWithAttrs);
                self::assertSame(1.5, $checkAfterSec);
                return true;
            })
            ->andReturn([]);

        $this->controller = new ConferenceApiController($this->cs, $this->em, $this->logger);

        $container = M::mock(\Symfony\Component\DependencyInjection\Container::class);
        $container->shouldReceive('has')->andReturnValues([
            // parameter_bag
            true,
            // serializer
            false,
        ]);

        $paramBag = M::mock(\Symfony\Component\DependencyInjection\ParameterBag::class);
        $paramBag->shouldReceive('get')->andReturn('prod');
        $container->shouldReceive('get')->andReturn($paramBag);
        $this->controller->setContainer($container);

        $request = M::mock(Request::class);
        $request->request = M::mock(\Symfony\Component\DependencyInjection\ParameterBag::class);
        $request->request->shouldReceive('get')
            ->withArgs(function ($name) {
                $found = in_array($name, ['email', 'query'], true);
                self::assertTrue($found);
                return $found;
            })
            ->andReturnValues([
                'dummy@google.com',
                [
                    'name'      => 'Cardio',
                    'city'      => [10, 11],
                    'country'   => [25, 30],
                    'region'    => [35],
                    'specialty' => [1, 2],
                ],
            ]);

        $response = $this->controller->export($request);

        self::assertNotEmpty($response);
        self::assertInstanceOf(\Symfony\Component\HttpFoundation\JsonResponse::class, $response);
        self::assertSame(200, $response->getStatusCode());

        $content = $response->getContent();
        $content = json_decode($content, true);

        self::assertArrayHasKey('msg', $content);
        self::assertSame('Probably in processing...', $content['msg']);

        self::assertArrayHasKey('data', $content);
        self::assertSame([], $content['data']);

        self::assertArrayHasKey('count', $content);
        self::assertSame(0, $content['count']);

        self::assertArrayHasKey('success', $content);
        self::assertSame(true, $content['success']);
    }
}
