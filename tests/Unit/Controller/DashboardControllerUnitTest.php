<?php

namespace App\Tests\Unit\Controller\Traits;

use App\Controller\DashboardController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Controller | DashboardController
 */
class DashboardControllerUnitTest extends WebTestCase
{
    /**
     * @var DashboardController
     */
    private $dashboardController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        $this->dashboardController = static::$kernel->getContainer()->get(DashboardController::class);

    }

    /**
     * @dataProvider getFillDrugsCountsDataStacksProvider
     *
     * @testdox Checking fillDrugsCountsData() Stacks
     */
    public function testFillDrugsCountsDataStacks($args, $expected)
    {
        $method = new \ReflectionMethod(DashboardController::class, 'fillDrugsCountsData');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->dashboardController, $args);

        $this->assertSame($expected, $result);
    }

    public function getFillDrugsCountsDataStacksProvider()
    {
        return [
            // #0
            [
                [
                    [
                        'widget-experimental-count'    => 0,
                        'widget-generic-count'         => 0,
                        'widget-brand-count'           => 0,
                        'widget-generic-synonym-count' => 0,
                        'widget-generic-typo-count'    => 0,
                        'widget-brand-synonym-count'   => 0,
                        'widget-brand-typo-count'      => 0,
                    ],
                ],
                [
                    'experimental_count' => 0,
                    'generic_count'      => 0,
                    'brand_count'        => 0,
                    'generic_keywords'   => 0,
                    'brand_keywords'     => 0,
                ],
            ],
            // #1
            [
                [
                    [
                        'widget-experimental-count'    => 65,
                        'widget-generic-count'         => 4562,
                        'widget-brand-count'           => 546,
                        'widget-generic-synonym-count' => 234,
                        'widget-generic-typo-count'    => 456,
                        'widget-brand-synonym-count'   => 789,
                        'widget-brand-typo-count'      => 858,
                    ],
                ],
                [
                    'experimental_count' => 65,
                    'generic_count'      => 4562,
                    'brand_count'        => 546,
                    'generic_keywords'   => 690,
                    'brand_keywords'     => 1647,
                ],
            ],
        ];
    }

    /**
     * @dataProvider getFillSpecificsDrugsDataStacksProvider
     *
     * @testdox Checking fillSpecificsDrugsData() Stacks
     */
    public function testFillSpecificsDrugsDataStacks($args, $expected)
    {
        $method = new \ReflectionMethod(DashboardController::class, 'fillSpecificsDrugsData');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->dashboardController, $args);

        $this->assertSame($expected, $result);
    }

    public function getFillSpecificsDrugsDataStacksProvider()
    {
        return [
            // #0
            [
                [
                    [
                        'widget-specific-complete-count'             => 0,
                        'widget-specific-experimental-generic-count' => 0,
                        'widget-specific-generic-brand-count'        => 0,
                        'widget-specific-brand-generic-count'        => 0,
                        'widget-specific-experimental-count'         => 0,
                        'widget-specific-generic-count'              => 0,
                        'widget-specific-brand-count'                => 0,
                    ],
                ],
                [
                    'sum' => [
                        'Drug Net Count',
                        '0',
                    ],
                    'all' => [
                        'Experimental related to Brand Drug',
                        'Number of relations between All THREE (3) Drug types',
                        '0',
                    ],
                    'e_g' => [
                        'Experimental related to Generic Drug',
                        'Number of relations between TWO (2) Drug types',
                        '0',
                    ],
                    'g_b' => [
                        'Generic related to Brand Drug',
                        'Number of relations between TWO (2) Drug types',
                        '0',
                    ],
                    'b_g' => [
                        'Brand related to Generic Drug',
                        'Number of relations between TWO (2) Drug types',
                        '0',
                    ],
                    'e' => [
                        'Experimental Drug',
                        'Number of Drug which have NO relations between other Drug type',
                        '0',
                    ],
                    'g' => [
                        'Generic Drug',
                        'Number of Drug which have NO relations between other Drug type',
                        '0',
                    ],
                    'b' => [
                        'Brand Drug',
                        'Number of Drug which have NO relations between other Drug type',
                        '0',
                    ],
                ],
            ],
            // #1
            [
                [
                    [
                        'widget-specific-complete-count'             => 4645,
                        'widget-specific-experimental-generic-count' => 56,
                        'widget-specific-generic-brand-count'        => 575,
                        'widget-specific-brand-generic-count'        => 7967,
                        'widget-specific-experimental-count'         => 345342326,
                        'widget-specific-generic-count'              => 453,
                        'widget-specific-brand-count'                => 574566,
                    ],
                ],
                [
                    'sum' => [
                        'Drug Net Count',
                        '345,930,588',
                    ],
                    'all' => [
                        'Experimental related to Brand Drug',
                        'Number of relations between All THREE (3) Drug types',
                        '4,645',
                    ],
                    'e_g' => [
                        'Experimental related to Generic Drug',
                        'Number of relations between TWO (2) Drug types',
                        '56',
                    ],
                    'g_b' => [
                        'Generic related to Brand Drug',
                        'Number of relations between TWO (2) Drug types',
                        '575',
                    ],
                    'b_g' => [
                        'Brand related to Generic Drug',
                        'Number of relations between TWO (2) Drug types',
                        '7,967',
                    ],
                    'e' => [
                        'Experimental Drugs',
                        'Number of Drug which have NO relations between other Drug type',
                        '345,342,326',
                    ],
                    'g' => [
                        'Generic Drugs',
                        'Number of Drug which have NO relations between other Drug type',
                        '453',
                    ],
                    'b' => [
                        'Brand Drugs',
                        'Number of Drug which have NO relations between other Drug type',
                        '574,566',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getFillDrugsBreakdownDataStacksProvider
     *
     * @testdox Checking fillDrugsBreakdownsData() Stacks
     */
    public function testFillDrugsBreakdownDataStacks($args, $expected)
    {
        $method = new \ReflectionMethod(DashboardController::class, 'fillDrugsBreakdownsData');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->dashboardController, $args);

        $this->assertSame($expected, $result);
    }

    public function getFillDrugsBreakdownDataStacksProvider()
    {
        return [
            // #0
            [
                [
                    [
                        'widget-experimental-count'                      => 0,
                        'widget-generic-count'                           => 0,
                        'widget-brand-count'                             => 0,
                        'widget-experimental-experimental-valid-count'   => 0,
                        'widget-experimental-experimental-invalid-count' => 0,
                        'widget-experimental-generic-valid-count'        => 0,
                        'widget-experimental-generic-invalid-count'      => 0,
                        'widget-experimental-brand-valid-count'          => 0,
                        'widget-experimental-brand-invalid-count'        => 0,
                        'widget-generic-valid-non_orphan-count'          => 0,
                        'widget-generic-invalid-non_orphan-count'        => 0,
                        'widget-generic-valid-orphan-count'              => 0,
                        'widget-generic-invalid-orphan-count'            => 0,
                        'widget-brand-valid-non_orphan-count'            => 0,
                        'widget-brand-invalid-non_orphan-count'          => 0,
                        'widget-brand-valid-orphan-count'                => 0,
                        'widget-brand-invalid-orphan-count'              => 0,
                        'widget-generic-synonym-count'                   => 0,
                        'widget-generic-typo-count'                      => 0,
                        'widget-brand-synonym-count'                     => 0,
                        'widget-brand-typo-count'                        => 0,
                        'widget-generic-synonym-orphan-count'            => 0,
                        'widget-generic-typo-orphan-count'               => 0,
                        'widget-brand-synonym-orphan-count'              => 0,
                        'widget-brand-typo-orphan-count'                 => 0,
                        'widget-experimental-creation-date-count'        => 0,
                        'widget-generic-creation-date-count'             => 0,
                        'widget-brand-creation-date-count'               => 0,
                        'widget-experimental-comments-count'             => 0,
                        'widget-generic-comments-count'                  => 0,
                        'widget-brand-comments-count'                    => 0,
                    ],
                ],
                [
                    [
                        'head'         => 'Overall Count',
                        'experimental' => '0 Drug',
                        'generic'      => '0 Drug',
                        'brand'        => '0 Drug',
                    ],
                    [
                        'head' => 'Experimental Type',
                        'experimental' => [
                            '0 Valid',
                            '0 Invalid',
                        ],
                        'generic' => [
                            '0 Valid',
                            '0 Invalid',
                        ],
                         'brand' => [
                            '0 Valid',
                            '0 Invalid',
                        ]
                    ],
                    [
                        'head' => 'Have Relation Count',
                        'experimental' => 'n/a',
                        'generic' => [
                            '0 Valid',
                            '0 Invalid',
                        ],
                         'brand' => [
                            '0 Valid',
                            '0 Invalid',
                        ]
                    ],
                    [
                        'head' => 'Orphan Drug Count',
                        'experimental' => 'n/a',
                        'generic' => [
                            '0 Valid',
                            '0 Invalid',
                        ],
                         'brand' => [
                            '0 Valid',
                            '0 Invalid',
                        ]
                    ],
                    [
                        'head'         => 'Synonym Count',
                        'experimental' => 'n/a',
                        'generic'      => '0 Record',
                        'brand'        => '0 Record'
                    ],
                    [
                        'head'         => 'Typo Count',
                        'experimental' => 'n/a',
                        'generic'      => '0 Record',
                        'brand'        => '0 Record'
                    ],
                    [
                        'head' => 'Orphan Keyword Count',
                        'experimental' => 'n/a',
                        'generic' => [
                            '0 Synonym',
                            '0 Typo',
                        ],
                         'brand' => [
                            '0 Synonym',
                            '0 Typo',
                        ]
                    ],
                    [
                        'head'         => 'Has Creation Date',
                        'experimental' => '0 Drug',
                        'generic'      => '0 Drug',
                        'brand'        => '0 Drug',
                    ],
                    [
                        'head'         => 'Has Comments',
                        'experimental' => '0 Drug',
                        'generic'      => '0 Drug',
                        'brand'        => '0 Drug',
                    ],
                ],
            ],
            // #0
            [
                [
                    [
                        'widget-experimental-count'                      => 14534,
                        'widget-generic-count'                           => 2,
                        'widget-brand-count'                             => 4563,
                        'widget-experimental-experimental-valid-count'   => 4,
                        'widget-experimental-experimental-invalid-count' => 455,
                        'widget-experimental-generic-valid-count'        => 6,
                        'widget-experimental-generic-invalid-count'      => 7,
                        'widget-experimental-brand-valid-count'          => 845,
                        'widget-experimental-brand-invalid-count'        => 9,
                        'widget-generic-valid-non_orphan-count'          => 10,
                        'widget-generic-invalid-non_orphan-count'        => 11,
                        'widget-generic-valid-orphan-count'              => 1456,
                        'widget-generic-invalid-orphan-count'            => 13,
                        'widget-brand-valid-non_orphan-count'            => 1454,
                        'widget-brand-invalid-non_orphan-count'          => 15,
                        'widget-brand-valid-orphan-count'                => 1456,
                        'widget-brand-invalid-orphan-count'              => 17,
                        'widget-generic-synonym-count'                   => 3455,
                        'widget-generic-typo-count'                      => 1,
                        'widget-brand-synonym-count'                     => 20,
                        'widget-brand-typo-count'                        => 54,
                        'widget-generic-synonym-orphan-count'            => 5675,
                        'widget-generic-typo-orphan-count'               => 0,
                        'widget-brand-synonym-orphan-count'              => 45645,
                        'widget-brand-typo-orphan-count'                 => 345,
                        'widget-experimental-creation-date-count'        => 345,
                        'widget-generic-creation-date-count'             => 0,
                        'widget-brand-creation-date-count'               => 345,
                        'widget-experimental-comments-count'             => 86456,
                        'widget-generic-comments-count'                  => 49856,
                        'widget-brand-comments-count'                    => 905,
                    ],
                ],
                [
                    [
                        'head'         => 'Overall Count',
                        'experimental' => '14,534 Drugs',
                        'generic'      => '2 Drugs',
                        'brand'        => '4,563 Drugs',
                    ],
                    [
                        'head' => 'Experimental Type',
                        'experimental' => [
                            '4 Valids',
                            '455 Invalids',
                        ],
                        'generic' => [
                            '6 Valids',
                            '7 Invalids',
                        ],
                         'brand' => [
                            '845 Valids',
                            '9 Invalids',
                        ]
                    ],
                    [
                        'head' => 'Have Relation Count',
                        'experimental' => 'n/a',
                        'generic' => [
                            '10 Valids',
                            '11 Invalids',
                        ],
                         'brand' => [
                            '1,454 Valids',
                            '15 Invalids',
                        ]
                    ],
                    [
                        'head' => 'Orphan Drug Count',
                        'experimental' => 'n/a',
                        'generic' => [
                            '1,456 Valids',
                            '13 Invalids',
                        ],
                         'brand' => [
                            '1,456 Valids',
                            '17 Invalids',
                        ]
                    ],
                    [
                        'head'         => 'Synonym Count',
                        'experimental' => 'n/a',
                        'generic'      => '3,455 Records',
                        'brand'        => '20 Records'
                    ],
                    [
                        'head'         => 'Typo Count',
                        'experimental' => 'n/a',
                        'generic'      => '1 Record',
                        'brand'        => '54 Records'
                    ],
                    [
                        'head' => 'Orphan Keyword Count',
                        'experimental' => 'n/a',
                        'generic' => [
                            '5,675 Synonyms',
                            '0 Typo',
                        ],
                         'brand' => [
                            '1,456 Synonyms',
                            '17 Typos',
                        ]
                    ],
                    [
                        'head'         => 'Has Creation Date',
                        'experimental' => '345 Drugs',
                        'generic'      => '0 Drug',
                        'brand'        => '345 Drugs',
                    ],
                    [
                        'head'         => 'Has Comments',
                        'experimental' => '86,456 Drugs',
                        'generic'      => '49,856 Drugs',
                        'brand'        => '905 Drugs',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getPrepareDetailsForRemoteSyncStacksProvider
     *
     * @testdox Checking prepareDetailsForRemoteSync() Stacks
     */
    public function testPrepareDetailsForRemoteSyncStacks($args, $expected)
    {
        $method = new \ReflectionMethod(DashboardController::class, 'prepareDetailsForRemoteSync');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->dashboardController, $args);

        $this->assertSame($expected, $result);
    }

    public function getPrepareDetailsForRemoteSyncStacksProvider()
    {
        return [
            // #0
            [
                [
                    [
                        'dbname' => '',
                        'host'   => '',
                        'error'  => true,
                    ],
                    false,
                    []
                ],
                [
                    'db'       => '? @ ?',
                    'status'   => 'muted',
                    'captions' => [
                        [
                            'muted',
                            'Up-Sync disabled'
                        ]
                    ],
                ],
            ],
            // #1
            [
                [
                    [
                        'dbname' => 'databasename',
                        'host'   => 'hostname',
                        'error'  => true,
                    ],
                    false,
                    []
                ],
                [
                    'db'       => 'databasename @ hostname',
                    'status'   => 'muted',
                    'captions' => [
                        ['muted', 'Up-Sync disabled'],
                    ],
                ],
            ],
            // #2
            [
                [
                    [
                        'dbname' => 'databasename',
                        'host'   => 'hostname',
                        'error'  => true,
                    ],
                    true,
                    []
                ],
                [
                    'db'       => 'databasename @ hostname',
                    'status'   => 'danger',
                    'captions' => [
                        ['danger', 'Unable to make connection.'],
                        ['danger', 'Up-Sync will fail.'],
                    ],
                ],
            ],
            // #3
            [
                [
                    [
                        'dbname' => 'databasename',
                        'host'   => 'hostname',
                        'error'  => false,
                    ],
                    true,
                    [
                        'First Table' => 'abc',
                        'Fourth Table' => 'def',
                    ]
                ],
                [
                    'db'       => 'databasename @ hostname',
                    'status'   => 'complete',
                    'captions' => [
                        ['success', 'Connection OK.'],
                        ['success', 'Up-Sync enabled.'],
                        ['info', 'First Table: "abc"'],
                        ['info', 'Fourth Table: "def"'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getFillDrugsInitialSyncDataStacksProvider
     *
     * @testdox Checking fillDrugsInitialSyncData() Stacks
     */
    public function testDrugsInitialSyncDataStacks($foundCountries, $args, $expected)
    {
        $repo = $this->createMock(\App\Repository\CountryRepository::class);
        $repo->method('getCountryGroupList')
            ->will($this->returnValue($foundCountries));

        $em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnValue($repo));

        $reflection = new \ReflectionClass(DashboardController::class);
        $property = $reflection->getProperty('em');
        $property->setAccessible(true);
        $property->setValue($this->dashboardController, $em);

        $method = new \ReflectionMethod(DashboardController::class, 'fillDrugsInitialSyncData');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->dashboardController, $args);

        $this->assertSame($expected, $result);
    }

    public function getFillDrugsInitialSyncDataStacksProvider()
    {
        return [
            // #0
            [
                [],
                [
                    [
                        'ext-script-import-creation-date-count' => 0,
                        'ext-script-import-creation-date-last'  => null,
                        'widget-experimental-comments-count'    => 0,
                        'widget-generic-comments-count'         => 0,
                        'widget-brand-comments-count'           => 0,
                    ],
                ],
                [
                    'status'   => 'danger',
                    'captions' => [
                        ['danger', '0 Country synced.'],
                        ['danger', '0 Comment synced.'],
                    ],
                ],
            ],
            // #1
            [
                [
                    'group1' => array_fill(0,  10,   ['dummy' => true]),
                    'group2' => array_fill(10, 50,   ['dummy' => true]),
                    'group3' => array_fill(156, 366, ['dummy' => true]),
                ],
                [
                    [
                        'ext-script-import-creation-date-count' => 345,
                        'ext-script-import-creation-date-last'  => new \DateTime('-62 days'),
                        'widget-experimental-comments-count'    => 5674,
                        'widget-generic-comments-count'         => 34,
                        'widget-brand-comments-count'           => 84,
                    ],
                ],
                [
                    'status'   => 'complete',
                    'captions' => [
                        ['info', '426 Countries synced.'],
                        ['info', '345 Creation dates synced (62 days ago)'],
                        ['info', '5,792 Comments synced.'],
                    ],
                ],
            ],
            // #2
            [
                [
                    'group1' => array_fill(0,  1,    ['dummy' => true]),
                ],
                [
                    [
                        'ext-script-import-creation-date-count' => 234,
                        'ext-script-import-creation-date-last'  => new \DateTime('-6655 hours'),
                        'widget-experimental-comments-count'    => 23,
                        'widget-generic-comments-count'         => 342,
                        'widget-brand-comments-count'           => 34,
                    ],
                ],
                [
                    'status'   => 'complete',
                    'captions' => [
                        ['info', '1 Country synced.'],
                        ['info', '234 Creation dates synced (277 days ago)'],
                        ['info', '399 Comments synced.'],
                    ],
                ],
            ],
            // #3
            [
                [],
                [
                    [
                        'ext-script-import-creation-date-count' => 234,
                        'ext-script-import-creation-date-last'  => 0,
                        'widget-experimental-comments-count'    => 0,
                        'widget-generic-comments-count'         => 0,
                        'widget-brand-comments-count'           => 1,
                    ],
                ],
                [
                    'status'   => 'warning',
                    'captions' => [
                        ['danger', '0 Country synced.'],
                        ['warning', '234 Creation dates synced (Unknown Sync Date)'],
                        ['info', '1 Comment synced.'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getFillJournalsInitialSyncDataStacksProvider
     *
     * @testdox Checking fillJournalsInitialSyncData() Stacks
     */
    public function testFillJournalsInitialSync($args, $expected)
    {
        $method = new \ReflectionMethod(DashboardController::class, 'fillJournalsInitialSyncData');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->dashboardController, $args);

        $this->assertSame($expected, $result);
    }

    public function getFillJournalsInitialSyncDataStacksProvider()
    {
        $date1 = new \DateTime('-19 days');

        return [
            // #0
            [
                [
                    [
                        'journal-sync-last-date'            => 0,
                        'journal-sync-last-id'              => 0,
                        'journal-sync-last-ignored-count'   => 0,
                        'journal-sync-last-processed-count' => 0,
                    ],
                ],
                [
                    'status'   => 'danger',
                    'captions' => [
                        ['danger', 'Journal data were never been synced before.'],
                    ],
                ],
            ],
            // #1
            [
                [
                    [
                        'journal-sync-last-date'            => $date1,
                        'journal-sync-last-id'              => 47436,
                        'journal-sync-last-ignored-count'   => 1,
                        'journal-sync-last-processed-count' => 234,
                    ],
                ],
                [
                    'status'   => 'complete',
                    'captions' => [
                        ['info', 'Last synced on ' . $date1->format('Y-m-d H:i:s') . ' (19 days ago)'],
                        ['info', '234 Processed Counts'],
                        ['info', '1 Ignored Count'],
                        ['info', 'Last ID #47,436'],
                    ],
                ],
            ],
            // #2
            [
                [
                    [
                        'journal-sync-last-date'            => 0,
                        'journal-sync-last-id'              => 345,
                        'journal-sync-last-ignored-count'   => 64,
                        'journal-sync-last-processed-count' => 573,
                    ],
                ],
                [
                    'status'   => 'danger',
                    'captions' => [
                        ['danger', 'Journal data were never been synced before.'],
                        ['info', '573 Processed Counts'],
                        ['info', '64 Ignored Counts'],
                        ['info', 'Last ID #345'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getFillConferencesInitialSyncDataStacksProvider
     *
     * @testdox Checking fillConferencesInitialSyncData() Stacks
     */
    public function testFillConferencesInitialSync($args, $expected)
    {
        $method = new \ReflectionMethod(DashboardController::class, 'fillConferencesInitialSyncData');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->dashboardController, $args);

        $this->assertSame($expected, $result);
    }

    public function getFillConferencesInitialSyncDataStacksProvider()
    {
        $date1 = new \DateTime('-6 days');

        return [
            // #0
            [
                [
                    [
                        'conference-sync-last-date'            => 0,
                        'conference-sync-last-processed-count' => 0,
                        'conference-sync-last-ignored-count'   => 0,
                    ],
                ],
                [
                    'status'   => 'danger',
                    'captions' => [
                        ['danger', 'Conference data were never been synced before.'],
                    ],
                ],
            ],
            // #1
            [
                [
                    [
                        'conference-sync-last-date'            => $date1,
                        'conference-sync-last-processed-count' => 234,
                        'conference-sync-last-ignored-count'   => 1,
                    ],
                ],
                [
                    'status'   => 'complete',
                    'captions' => [
                        ['info', 'Last synced on ' . $date1->format('Y-m-d H:i:s') . ' (6 days ago)'],
                        ['info', '234 Processed Counts'],
                        ['info', '1 Ignored Count'],
                    ],
                ],
            ],
            // #2
            [
                [
                    [
                        'conference-sync-last-date'            => 0,
                        'conference-sync-last-processed-count' => 573,
                        'conference-sync-last-ignored-count'   => 64,
                    ],
                ],
                [
                    'status'   => 'danger',
                    'captions' => [
                        ['danger', 'Conference data were never been synced before.'],
                        ['info', '573 Processed Counts'],
                        ['info', '64 Ignored Counts'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getFillJournalRecordsDataStacksProvider
     *
     * @testdox Checking fillJournalRecordsData() Stacks
     */
    public function testFillJournalRecordsDataStacks($args, $expected)
    {
        $method = new \ReflectionMethod(DashboardController::class, 'fillJournalRecordsData');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->dashboardController, $args);

        $this->assertSame($expected, $result);
    }

    public function getFillJournalRecordsDataStacksProvider()
    {
        return [
            // #0
            [
                [
                    [
                        'widget-journals-overall-count'                => 0,
                        'widget-journals-corrupted-count'              => 0,
                        'widget-journals-key-active-count'             => 0,
                        'widget-journals-key-archive-count'            => 0,
                        'widget-journals-key-is-medline-count'         => 0,
                        'widget-journals-key-is-secondline-count'      => 0,
                        'widget-journals-key-is-dgabstract-count'      => 0,
                        'widget-journals-key-is-notbeingupdated-count' => 0,
                        'widget-journals-key-is-deleted-count'         => 0,
                        'widget-journals-key-is-globaleditions-count'  => 0,
                    ],
                ],
                [
                    'status'   => 'danger',
                    'captions' => [
                        ['danger', 'No active record found.'],
                    ],
                ],
            ],
            // #1
            [
                [
                    [
                        'widget-journals-overall-count'                => 43,
                        'widget-journals-corrupted-count'              => 5,
                        'widget-journals-key-active-count'             => 4,
                        'widget-journals-key-archive-count'            => 3,
                        'widget-journals-key-is-medline-count'         => 7,
                        'widget-journals-key-is-secondline-count'      => 8,
                        'widget-journals-key-is-dgabstract-count'      => 2353,
                        'widget-journals-key-is-notbeingupdated-count' => 1,
                        'widget-journals-key-is-deleted-count'         => 57,
                        'widget-journals-key-is-globaleditions-count'  => 85,
                    ],
                ],
                [
                    'status'   => 'complete',
                    'captions' => [
                        ['info', '43 Overall Journals'],
                        ['info', '5 Corrupted Journals'],
                        ['info', '4 Active Journals'],
                        ['info', '3 Archived Journals'],
                        ['info', '7 MedLine Journals'],
                        ['info', '8 SecondLine Journals'],
                        ['info', '2,353 DgAbstract Journals'],
                        ['info', '1 Not Being Updated Journal'],
                        ['info', '85 Global Editions Journals'],
                        ['info', '57 Deleted Journals'],
                    ],
                ],
            ],
            // #2
            [
                [
                    [
                        'widget-journals-overall-count'                => 0,
                        'widget-journals-corrupted-count'              => 0,
                        'widget-journals-key-active-count'             => 0,
                        'widget-journals-key-archive-count'            => 0,
                        'widget-journals-key-is-medline-count'         => 0,
                        'widget-journals-key-is-secondline-count'      => 0,
                        'widget-journals-key-is-dgabstract-count'      => 0,
                        'widget-journals-key-is-notbeingupdated-count' => 0,
                        'widget-journals-key-is-deleted-count'         => 0,
                        'widget-journals-key-is-globaleditions-count'  => 3,
                    ],
                ],
                [
                    'status'   => 'complete',
                    'captions' => [
                        ['info', '3 Global Editions Journals'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getFillConferenceRecordsDataStacksProvider
     *
     * @testdox Checking fillConferenceRecordsData() Stacks
     */
    public function testFillConferenceRecordsDataStacks($args, $expected)
    {
        $method = new \ReflectionMethod(DashboardController::class, 'fillConferenceRecordsData');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->dashboardController, $args);

        $this->assertSame($expected, $result);
    }

    public function getFillConferenceRecordsDataStacksProvider()
    {
        return [
            // #0
            [
                [
                    [
                        'widget-conferences-overall-count'              => 0,
                        'widget-conferences-corrupted-count'            => 0,
                        'widget-conferences-key-discontinued-count'     => 0,
                        'widget-conferences-key-not-discontinued-count' => 0,
                        'widget-conferences-key-online-count'           => 0,
                        'widget-conferences-key-not-online-count'       => 0,
                        'widget-conferences-key-key-event-count'        => 0,
                        'widget-conferences-key-not-key-event-count'    => 0,
                    ],
                ],
                [
                    'status'   => 'danger',
                    'captions' => [
                        ['danger', 'No active record found.'],
                    ],
                ],
            ],
            // #1
            [
                [
                    [
                        'widget-conferences-overall-count'              => 45,
                        'widget-conferences-corrupted-count'            => 674,
                        'widget-conferences-key-discontinued-count'     => 6,
                        'widget-conferences-key-not-discontinued-count' => 1,
                        'widget-conferences-key-online-count'           => 46,
                        'widget-conferences-key-not-online-count'       => 74574,
                        'widget-conferences-key-key-event-count'        => 74,
                        'widget-conferences-key-not-key-event-count'    => 574,
                    ],
                ],
                [
                    'status'   => 'complete',
                    'captions' => [
                        ['info', '45 Overall Conferences'],
                        ['info', '674 Corrupted Conferences'],
                        ['info', '1 On Going Conference'],
                        ['info', '6 Discontinued Conferences'],
                        ['info', '46 Online Conferences'],
                        ['info', '74,574 Not Online Conferences'],
                        ['info', '74 Key Conferences'],
                        ['info', '574 Not Key Conferences'],
                    ],
                ],
            ],
            // #2
            [
                [
                    [
                        'widget-conferences-overall-count'              => 10,
                        'widget-conferences-corrupted-count'            => 0,
                        'widget-conferences-key-discontinued-count'     => 0,
                        'widget-conferences-key-not-discontinued-count' => 0,
                        'widget-conferences-key-online-count'           => 10,
                        'widget-conferences-key-not-online-count'       => 0,
                        'widget-conferences-key-key-event-count'        => 0,
                        'widget-conferences-key-not-key-event-count'    => 0,
                    ],
                ],
                [
                    'status'   => 'complete',
                    'captions' => [
                        ['info', '10 Overall Conferences'],
                        ['info', '10 Online Conferences'],
                    ],
                ],
            ],
        ];
    }
}
