<?php

namespace App\Tests\Unit\Service;

use App\Service\ConferenceSourceService;
use App\Utility\MsSqlRemoteConnection;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Security;

/**
 * @testdox UNIT | Service | ConferenceSourceService
 */
class ConferenceSourceServiceUnitTest extends WebTestCase
{
    /**
     * @var ConferenceSourceService
     */
    private $sourceService;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->sourceService = $client->getContainer()->get('test.' . ConferenceSourceService::class);

        parent::setUp();
    }

    /**
     * @testdox Checking Class Initialization and UpSync Disabled
     */
    public function testInitAndUpSyncDisabled()
    {
        $this->__setupMocks(null, null, false);

        $this->assertInstanceOf(ConferenceSourceService::class, $this->sourceService);
        $this->assertFalse($this->sourceService->isUpSync());
        $this->assertSame('"dbC"."tableOne"', $this->sourceService->getPrimaryTableName(true, true));
        $this->assertSame('"dbC"."tableTwo"', $this->sourceService->getSecondaryTableName(true, true));
        $this->assertSame('dbC.tableOne', $this->sourceService->getPrimaryTableName(true, false));
        $this->assertSame('dbC.tableTwo', $this->sourceService->getSecondaryTableName(true, false));
        $this->assertSame('"tableOne"', $this->sourceService->getPrimaryTableName(false, true));
        $this->assertSame('"tableTwo"', $this->sourceService->getSecondaryTableName(false, true));
        $this->assertSame('tableOne', $this->sourceService->getPrimaryTableName(false, false));
        $this->assertSame('tableTwo', $this->sourceService->getSecondaryTableName(false, false));

        $entity = $this->_loadEntity();

        $result = $this->sourceService->upsync($entity);
        $this->assertFalse($result);

        $result = $this->sourceService->remove($entity);
        $this->assertFalse($result);
    }

    /**
     * @testdox Checking upsync() create with no issue
     */
    public function testUpSyncCreateEmptyOk()
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $uniqueName = '';
        $conn = $this->_generateNewConference($uniqueName, null);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_createNewConferenceOnTableInsert(0, '"dbC"."tableTwo"', true);
        $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_updateConference(null, false);
        $remote->expects($this->at(3))->method('getConnection')->will($this->returnValue($conn));

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) {
            $this->assertStringContainsStringIgnoringCase('Remote Conference UpSync Complete', $msg);
            $this->assertArrayHasKey('P', $args);
            $this->assertSame('insert', $args['P']);
            $this->assertArrayHasKey('I', $args);
            $this->assertSame('123456789012345678901234567890AB', $args['I']);
            return true;
        }));

        $this->__setupMocks($remote, $logger);

        $entity = $this->_loadEntity();

        $result = $this->sourceService->upsync($entity);
        $this->assertTrue($result);

        $this->assertSame('123456789012345678901234567890AB', $entity->getPreviousId());

        return $entity;
    }

    /**
     * @dataProvider getEntityForUpdateProvider
     *
     * @testdox Checking upsync() update with no issue
     */
    public function testUpSyncUpdateOk($entity, $withData)
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $remote->expects($this->at(1))->method('getConnection')->will($this->returnCallback(function() {
            $conn = $this->createMock(\PDO::class);

            $conn = $this->_createNewConferenceOnTableSelectCount(0, '"dbC"."tableOne"', true, 0, $conn);

            $conn = $this->_createNewConferenceOnTableInsert(2, '"dbC"."tableOne"', true, $conn);

            return $conn;
        }));

        $conn = $this->_createNewConferenceOnTableSelectCount(0, '"dbC"."tableTwo"', true, 1);
        $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_updateConference(null, $withData);
        $remote->expects($this->at(3))->method('getConnection')->will($this->returnValue($conn));

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) {
            $this->assertStringContainsStringIgnoringCase('Remote Conference UpSync Complete', $msg);
            $this->assertArrayHasKey('P', $args);
            $this->assertSame('update', $args['P']);
            $this->assertArrayHasKey('I', $args);
            $this->assertSame('123456789012345678901234567890AB', $args['I']);
            return true;
        }));

        $this->__setupMocks($remote, $logger);

        $result = $this->sourceService->upsync($entity);
        $this->assertTrue($result);

        return $entity;
    }

    /**
     * @testdox Checking remove() with no issue
     */
    public function testUpSyncRemoveEmptyOk()
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $remote->expects($this->at(1))->method('getConnection')->will($this->returnCallback(function() {
            $conn = $this->createMock(\PDO::class);
            $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) {
                $this->assertSame(
                    'DELETE FROM "dbC"."tableTwo" WHERE CongressID = :CongressID;',
                    $sql
                );

                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                    $this->assertSame(':CongressID', $token);
                    $this->assertSame('123456789012345678901234567890AB', $value);
                    return true;
                }));
                $stmt->expects($this->at(1))->method('execute')->will($this->returnValue(true));
                return $stmt;
            }));
            $conn->expects($this->at(1))->method('errorInfo')->will($this->returnValue(['remove -> secondary -> empty']));

            $conn->expects($this->at(2))->method('prepare')->will($this->returnCallback(function($sql) {
                $this->assertSame(
                    'DELETE FROM "dbC"."tableOne" WHERE CongressID = :CongressID;',
                    $sql
                );

                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                    $this->assertSame(':CongressID', $token);
                    $this->assertSame('123456789012345678901234567890AB', $value);
                    return true;
                }));
                $stmt->expects($this->at(1))->method('execute')->will($this->returnValue(true));
                return $stmt;
            }));
            $conn->expects($this->at(3))->method('errorInfo')->will($this->returnValue(['remove -> primary -> empty']));
            return $conn;
        }));

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) {
            $this->assertStringContainsStringIgnoringCase('Remote Conference Removal Complete', $msg);
            $this->assertArrayHasKey('P', $args);
            $this->assertSame('delete', $args['P']);
            $this->assertArrayHasKey('primary', $args);
            $this->assertSame(true, $args['primary']);
            $this->assertArrayHasKey('secondary', $args);
            $this->assertSame(true, $args['secondary']);
            $this->assertArrayHasKey('V', $args);
            $this->assertSame('123456789012345678901234567890AB', $args['V']);
            return true;
        }));

        $this->__setupMocks($remote, $logger);

        $entity = $this->_loadEntity();
        $entity = $this->_loadEntityId($entity);
        $entity = $this->_loadEntityPreviousId($entity);

        $result = $this->sourceService->remove($entity);
        $this->assertTrue($result);
    }

    /**
     * @dataProvider getUpSyncCreateEmptyFailErrorLevelsProvider
     *
     * @testdox Checking upsync() create with failures
     */
    public function testUpSyncCreateEmptyFailures($errorLevel)
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $uniqueName = '';
        $conn = $this->_generateNewConference($uniqueName, $errorLevel);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        if (2 === $errorLevel) {
            $conn = $this->_createNewConferenceOnTableInsert(0, '"dbC"."tableTwo"', false);
            $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));
        }

        $this->__setupMocks($remote);

        $entity = $this->_loadEntity();

        try {
            $this->sourceService->upsync($entity);
        }
        catch (\Exception $error)  {
            $this->assertInstanceOf(\App\Exception\RemoteSyncException::class, $error);
            $this->assertStringContainsStringIgnoringCase('Fail to Generate new ID: ' . $uniqueName, $error->getMessage());
        }
    }

    /**
     * @dataProvider getUpSyncUpdateEmptyFailErrorLevelsProvider
     *
     * @testdox Checking upsync() update with failures
     */
    public function testUpSyncUpdateEmptyFailures($errorLevel, $errorMsg)
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $remote->expects($this->at(1))->method('getConnection')->will($this->returnCallback(function() {
            $conn = $this->createMock(\PDO::class);

            $conn = $this->_createNewConferenceOnTableSelectCount(0, '"dbC"."tableOne"', true, 0, $conn);

            $conn = $this->_createNewConferenceOnTableInsert(2, '"dbC"."tableOne"', true, $conn);
            return $conn;
        }));

        $conn = $this->_createNewConferenceOnTableSelectCount(0, '"dbC"."tableTwo"', true, 1);
        $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_updateConference($errorLevel, false);
        $remote->expects($this->at(3))->method('getConnection')->will($this->returnValue($conn));

        $this->__setupMocks($remote);

        $entity = $this->_loadEntity();
        $entity = $this->_loadEntityId($entity);
        $entity = $this->_loadEntityPreviousId($entity);

        try {
            $this->sourceService->upsync($entity);
        }
        catch (\Exception $error)  {
            $this->assertInstanceOf(\App\Exception\RemoteSyncException::class, $error);
            $this->assertStringContainsStringIgnoringCase($errorMsg, $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('"previous_id":"123456789012345678901234567890AB"', $error->getMessage());
        }
    }

    /**
     * @testdox Checking remove() Previous ID failures
     */
    public function testUpSyncRemoveEmptyPreviousIdFailure()
    {
        $entity = $this->_loadEntity();
        $entity = $this->_loadEntityId($entity);

        $this->__setupMocks();

        try {
            $this->sourceService->remove($entity);
        }
        catch (\Exception $error)  {
            $this->assertInstanceOf(\App\Exception\RemoteSyncException::class, $error);
            $this->assertStringContainsStringIgnoringCase('Conference has no record of Previous Site ID', $error->getMessage());
        }
    }

    /**
     * @testdox Checking remove() PDO failures
     */
    public function testUpSyncRemoveEmptyPdoFailure()
    {
        $entity = $this->_loadEntity();
        $entity = $this->_loadEntityId($entity);
        $entity = $this->_loadEntityPreviousId($entity);

        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $remote->expects($this->at(1))->method('getConnection')->will($this->returnCallback(function() {
            $conn = $this->createMock(\PDO::class);
            $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) {
                $this->assertSame(
                    'DELETE FROM "dbC"."tableTwo" WHERE CongressID = :CongressID;',
                    $sql
                );

                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                    $this->assertSame(':CongressID', $token);
                    $this->assertSame('123456789012345678901234567890AB', $value);
                    return true;
                }));
                $stmt->expects($this->at(1))->method('execute')->will($this->throwException(new \PDOException('SQL 1002 Error Line 1 at ...')));
                return $stmt;
            }));
            return $conn;
        }));

        $this->__setupMocks($remote);

        try {
            $this->sourceService->remove($entity);
        }
        catch (\Exception $error)  {
            $this->assertInstanceOf(\App\Exception\RemoteSyncException::class, $error);
            $this->assertStringContainsStringIgnoringCase('Remote Database Up-Sync failed at SQL level', $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('Your request has been canceled', $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('SQL 1002 Error Line 1 at', $error->getMessage());
        }
    }

    public function getUpSyncCreateEmptyFailErrorLevelsProvider()
    {
        return [
            [0], // 0 - Create ID on Primary Failure
            [1], // 1 - Re-query Entity by ID Failure
            [2], // 2 - Create ID on Secondary Failure
        ];
    }

    public function getUpSyncUpdateEmptyFailErrorLevelsProvider()
    {
        return [
            [0, 'Unable to update Primary Table.'],   // 0 - Update Primary Table
            [1, 'Unable to update Secondary Table.'], // 1 - Update Secondary Table
        ];
    }

    public function getEntityForUpdateProvider()
    {
        $entity1 = $this->_loadEntity();
        $entity1 = $this->_loadEntityId($entity1);
        $entity1 = $this->_loadEntityPreviousId($entity1);

        $entity2 = $this->_loadEntity();
        $entity2 = $this->_loadEntityId($entity2);
        $entity2 = $this->_loadEntityData($entity2);

        return [
            [$entity1, false],
            [$entity2, true],
        ];
    }

    public function _generateNewConference(&$uniqueName = '', $errorLevel = null)
    {
        $conn = $this->createMock(\PDO::class);

        // create primary table
        $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use (&$uniqueName, $errorLevel) {
            $this->assertSame(
                "INSERT INTO \"dbC\".\"tableOne\" (CongressID, Name) VALUES (CONVERT(VARCHAR(32), REPLACE(NEWID(),'-','')), :Name);",
                $sql
            );
            $stmt = $this->createMock(\PDOStatement::class);
            $stmt->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use (&$uniqueName) {
                $this->assertSame(':Name', $token);
                $this->assertStringContainsStringIgnoringCase('DrugDbTemporaryIDENTIFIER-', $value);
                preg_match('/(DrugDbTemporaryIDENTIFIER)-([a-z0-9]{32})-(\d{0,4})-(\d{10}\.\d{1,4})/', $value, $found);
                $this->assertSame(5, count($found));
                $uniqueName = $found[0];
                return true;
            }));
            $stmt->method('execute')->will($this->returnValue( (0 !== $errorLevel) ));
            return $stmt;
        }));
        $conn->expects($this->at(1))->method('errorInfo')->will($this->returnValue(['_generateNewConference -> create primary table']));

        if (in_array($errorLevel, [null, 1, 2], true)) {
            // get the id
            $conn->expects($this->at(2))->method('prepare')->will($this->returnCallback(function($sql) use (&$uniqueName, $errorLevel) {
                $this->assertSame(
                    'SELECT CongressID FROM "dbC"."tableOne" WHERE Name = :Name;',
                    $sql
                );
                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use (&$uniqueName) {
                    $this->assertSame(':Name', $token);
                    $this->assertSame($uniqueName, $value);
                    return true;
                }));

                if (1 === $errorLevel) {
                    $stmt->method('execute')->will($this->returnValue(false));
                    $stmt->method('fetchColumn')->will($this->returnValue(null));
                }
                else {
                    $stmt->method('execute')->will($this->returnValue(true));
                    $stmt->method('fetchColumn')->will($this->returnValue('123456789012345678901234567890AB'));
                }
                return $stmt;
            }));
            $conn->expects($this->at(3))->method('errorInfo')->will($this->returnValue(['_generateNewConference -> get the id']));
        }

        return $conn;
    }

    public function _createNewConferenceOnTableSelectCount(int $invokerStartsAt, string $table, bool $success = true, int $count = 1, $conn = null)
    {
        if (null === $conn) {
            $conn = $this->createMock(\PDO::class);
        }

        $conn->expects($this->at($invokerStartsAt))->method('prepare')->will($this->returnCallback(function($sql) use ($table, $success, $count) {
            $this->assertSame(
                "SELECT COUNT(*) AS count FROM {$table} WHERE CongressID = :CongressID;",
                $sql
            );

            $stmt = $this->createMock(\PDOStatement::class);
            $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                $this->assertSame(':CongressID', $token);
                $this->assertSame('123456789012345678901234567890AB', $value);
                return true;
            }));
            $stmt->expects($this->at(1))->method('execute')->will($this->returnValue($success));
            $stmt->expects($this->at(2))->method('fetchColumn')->will($this->returnValue($count));
            return $stmt;
        }));
        $conn->expects($this->at(($invokerStartsAt + 1)))->method('errorInfo')->will($this->returnValue(['_createNewConferenceOnTableSelectCount', $table, $success, $count]));

        return $conn;
    }

    public function _createNewConferenceOnTableInsert(int $invokerStartsAt, string $table, bool $success = true, $conn = null)
    {
        if (null === $conn) {
            $conn = $this->createMock(\PDO::class);
        }

        $conn->expects($this->at($invokerStartsAt))->method('prepare')->will($this->returnCallback(function($sql) use ($table, $success) {
            $this->assertSame(
                "INSERT INTO {$table} (CongressID) VALUES (:CongressID);",
                $sql
            );
            $stmt = $this->createMock(\PDOStatement::class);
            $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                $this->assertSame(':CongressID', $token);
                $this->assertSame('123456789012345678901234567890AB', $value);
                return true;
            }));
            $stmt->expects($this->at(1))->method('execute')->will($this->returnValue($success));
            return $stmt;
        }));
        $conn->expects($this->at(($invokerStartsAt + 1)))->method('errorInfo')->will($this->returnValue(['_createNewConferenceOnTableInsert', $table, $success]));

        return $conn;
    }

    public function _updateConference($errorLevel = null, $withData = false)
    {
        $primaryFields = [
            'Name'         => ['Test Conference 1', 2],
            'City'         => ['Kuala Lumpur', 2],
            'CityGuide'    => ['W.P. Kuala Lumpur', 2],
            'CountryCode'  => ['my', 2],
            'CountryName'  => ['Malaysia', 2],
            'State'        => ['Mid Valley City', 2],
            'Region'       => ['Southeast Asia', 2],
            'Discontinued' => [null, 0],
            'StartDate'    => ['2018-06-03 16:23:54', 2],
            'EndDate'      => ['2018-06-05 03:04:05', 2],
            'Contact'      => ['This is' . PHP_EOL . 'a Test', 2],
            'Email'        => ['moddy.gideon@gmail.com', 2],
            'Fax'          => ['+603 123 6456 / +603 123 6789', 2],
            'Phone'        => ['+6 019 1236 456 / +6 019 1236 789', 2],
            'Specialty'    => ['Oncology; Gideon', 2],
            'Cruise'       => ['yes', 2],
            'KeyCongress'  => ['yes', 2],
            'Online'       => [null, 0],
        ];

        $secondaryFields = [
            'IncludeCongressGuide' => "CONVERT(BINARY, 'I')",
            'CongressLink1'        => ['Homepage', 2],
            'CongressLink2'        => ['Registration Page', 2],
            'CongressLink3'        => [null, 0],
            'CongressLink4'        => [null, 0],
            'CongressLink5'        => [null, 0],
            'CongressLink6'        => [null, 0],
            'CongressURL1'         => ['http://google.com', 2],
            'CongressURL2'         => ['http://toyota.com', 2],
            'CongressURL3'         => [null, 0],
            'CongressURL4'         => [null, 0],
            'CongressURL5'         => [null, 0],
            'CongressURL6'         => [null, 0],
            'UniqueName'           => ['123456789132465', 2],
        ];

        if (!$withData) {
            $primaryFields = array_combine(array_keys($primaryFields), array_fill(0, count($primaryFields), [null, 0]));
            $secondaryFields = array_combine(array_keys($secondaryFields), array_fill(0, count($secondaryFields), [null, 0]));
        }

        $conn = $this->createMock(\PDO::class);

        // primary table
        $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($errorLevel, $primaryFields) {
            $fieldsStr = [];
            foreach (array_keys($primaryFields) as $fi => $field) {
                if (is_array($primaryFields[$field])) {
                    $fieldsStr[] = "{$field}=:{$field}";
                }
                else {
                    $fieldsStr[] = "{$field}={$primaryFields[$field]}";
                }
            }
            $fieldsStr = implode(', ', $fieldsStr);

            $this->assertSame("UPDATE \"dbC\".\"tableOne\" SET {$fieldsStr} WHERE CongressID = :PreviousID;", $sql);

            $stmt = $this->createMock(\PDOStatement::class);

            $invokedAt = 0;
            foreach (array_keys($primaryFields) as $fi => $field) {
                if (is_array($primaryFields[$field])) {
                    $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use ($field, $primaryFields) {
                        $this->assertSame(":{$field}", $token);
                        $this->assertSame($primaryFields[$field][0], $value);
                        $this->assertSame($primaryFields[$field][1], $type);
                        return true;
                    }));
                    $invokedAt++;
                }
            }

            $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                $this->assertSame(':PreviousID', $token);
                $this->assertSame('123456789012345678901234567890AB', $value);
                $this->assertSame(2, $type);
                return true;
            }));
            $invokedAt++;

            $stmt->expects($this->at($invokedAt))->method('execute')->will($this->returnValue( (0 !== $errorLevel) ));
            $invokedAt++;
            return $stmt;
        }));
        $conn->expects($this->at(1))->method('errorInfo')->will($this->returnValue(['_updateConference - primary table']));

        // secondary table
        if (
            (null === $errorLevel) ||
            (1 === $errorLevel)
        ) {
            $conn->expects($this->at(2))->method('prepare')->will($this->returnCallback(function($sql) use ($errorLevel, $secondaryFields) {
                $fieldsStr = [];
                foreach (array_keys($secondaryFields) as $fi => $field) {
                    if (is_array($secondaryFields[$field])) {
                        $fieldsStr[] = "{$field}=:{$field}";
                    }
                    else {
                        $fieldsStr[] = "{$field}={$secondaryFields[$field]}";
                    }
                }
                $fieldsStr = implode(', ', $fieldsStr);

                $this->assertSame("UPDATE \"dbC\".\"tableTwo\" SET {$fieldsStr} WHERE CongressID = :PreviousID;", $sql);

                $stmt = $this->createMock(\PDOStatement::class);

                $invokedAt = 0;
                foreach (array_keys($secondaryFields) as $fi => $field) {
                    if (is_array($secondaryFields[$field])) {
                        $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use ($field, $secondaryFields) {
                            $this->assertSame(":{$field}", $token);
                            $this->assertSame($secondaryFields[$field][0], $value);
                            $this->assertSame($secondaryFields[$field][1], $type);
                            return true;
                        }));
                        $invokedAt++;
                    }
                }

                $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                    $this->assertSame(':PreviousID', $token);
                    $this->assertSame('123456789012345678901234567890AB', $value);
                    $this->assertSame(2, $type);
                    return true;
                }));
                $invokedAt++;

                $stmt->expects($this->at($invokedAt))->method('execute')->will($this->returnValue( (1 !== $errorLevel) ));
                $invokedAt++;
                return $stmt;
            }));
            $conn->expects($this->at(3))->method('errorInfo')->will($this->returnValue(['_updateConference - secondary table']));
        }

        return $conn;
    }

    private function __setupMocks($remote = null, $logger = null, $upsync = true)
    {
        $reflection = new \ReflectionClass(ConferenceSourceService::class);

        if (null === $remote) {
            $remote = $this->createMock(MsSqlRemoteConnection::class);
        }
        $reflProp = $reflection->getProperty('remote');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, $remote);

        if (null === $logger) {
            $logger = $this->createMock(LoggerInterface::class);
        }
        $reflProp = $reflection->getProperty('logger');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, $logger);

        $reflProp = $reflection->getProperty('upSync');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, $upsync);

        $reflProp = $reflection->getProperty('dbSchema');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, 'dbC');

        $reflProp = $reflection->getProperty('tblPrimary');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, 'tableOne');

        $reflProp = $reflection->getProperty('tblSecondary');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, 'tableTwo');
    }

    private function _loadEntity()
    {

        return new \App\Entity\Conference;
    }

    private function _loadEntityId($entity = null)
    {
        $reflection = new \ReflectionClass(\App\Entity\Conference::class);

        if (null === $entity) {
            $entity = $this->_loadEntity();
        }

        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($entity, 101);

        return $entity;
    }

    private function _loadEntityPreviousId($entity = null)
    {
        $reflection = new \ReflectionClass(\App\Entity\Conference::class);

        if (null === $entity) {
            $entity = $this->_loadEntity();
        }

        $reflProp = $reflection->getProperty('previousId');
        $reflProp->setAccessible(true);
        $reflProp->setValue($entity, '123456789012345678901234567890AB');

        return $entity;
    }

    private function _loadEntityData($entity = null)
    {
        if (null === $entity) {
            $entity = $this->_loadEntity();
        }

        $start_date = \DateTime::createFromFormat('Y-m-d H:i:s', '2018-06-03 16:23:54');
        $end_date   = \DateTime::createFromFormat('Y-m-d H:i:s', '2018-06-05 03:04:05');

        $country = new \App\Entity\Country;
        $reflection = new \ReflectionClass(\App\Entity\Country::class);
        $reflProp = $reflection->getProperty('isoCode');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'MY');
        $reflProp = $reflection->getProperty('name');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'Malaysia');
        $reflProp = $reflection->getProperty('capitalCity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'Kuala Lumpur');

        $countryTaxonomy = new \App\Entity\CountryTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\CountryTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($countryTaxonomy, 6568);
        $reflProp = $reflection->getProperty('name');
        $reflProp->setAccessible(true);
        $reflProp->setValue($countryTaxonomy, 'Southeast Asia');

        $region = new \App\Entity\CountryRegions;
        $reflection = new \ReflectionClass(\App\Entity\CountryRegions::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($region, 34532);
        $reflProp = $reflection->getProperty('country');
        $reflProp->setAccessible(true);
        $reflProp->setValue($region, $country);
        $reflProp = $reflection->getProperty('region');
        $reflProp->setAccessible(true);
        $reflProp->setValue($region, $countryTaxonomy);

        $specialtyTax1 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax1, 40);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax1, 'Oncology');

        $specialtyTax2 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax2, 41);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax2, 'Gideon');

        $specialtyTax3 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax3, 42);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax3, 'Silver');

        $specialty1 = new \App\Entity\ConferenceSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\ConferenceSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, 5);
        $reflProp = $reflection->getProperty('conference');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, $entity);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, $specialtyTax1);

        $specialty2 = new \App\Entity\ConferenceSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\ConferenceSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, 65);
        $reflProp = $reflection->getProperty('conference');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, $entity);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, $specialtyTax2);

        $specialty3 = new \App\Entity\ConferenceSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\ConferenceSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty3, 1);

        $entity
            ->setName('Test Conference 1')
            ->setStartDate($start_date)
            ->setEndDate($end_date)
            ->setCity($country->getCapitalCity())
            ->setCityGuide('W.P. ' . $country->getCapitalCity())
            ->setCountry($country)
            ->setRegion($countryTaxonomy)
            ->setPreviousId('123456789012345678901234567890AB')
            ->setUniqueName('123456789132465')
            ->setCruise(1)
            ->setOnline(0)
            ->setKeyEvent(2)
            ->setHasGuide(1)
            ->setDiscontinued(0)
            ->addSpecialty($specialty1)
            ->addSpecialty($specialty3)
            ->addSpecialty($specialty2)
            ->removeSpecialty($specialty3)
            ->setDetails([
                'dummy'   => 'testing 123',
                'state'   => 'Mid Valley City',
                'contact' => [
                    'fax' => [
                        '+603 123 6456',
                        '+603 123 6789',
                    ],
                    'email' => [
                        'moddy.gideon@gmail.com',
                    ],
                ],
                'links' => [
                    [
                        'caption' => 'Homepage',
                        'url'     => 'http://google.com',
                    ]
                ],
                '_meta' => [
                    'created_on' => '2017-03-05 13:45:35',
                    'created_by' => 'jake.ajax',
                ],
            ])
            ->setDetailsMetaModifiedOn( \DateTime::createFromFormat('Y-m-d H:i:s', '2016-08-08 08:08:08') )
            ->addDetailsContactLink('Registration Page', 'http://toyota.com')
            ->setDetailsContactDesc('This is<br   />a Test')
            ->setDetailsContactPhones(['+6 019 1236 456', '+6 019 1236 789'])
        ;

        return $entity;
    }
}
