<?php

namespace App\Tests\Unit\Service\Factory;

use App\Service\Factory\SearchServiceFactory;
use App\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Service | Factory | SearchServiceFactory
 */
class SearchServiceFactoryUnitTest extends WebTestCase
{
    /**
     * @var SearchServiceFactory
     */
    private $ss;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->ss = $client->getContainer()->get('test.' . SearchService::class);

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->ss = null;

        parent::tearDown();
    }

    /**
     * @testdox Check defaulting values
     */
    public function testDefaulting()
    {
        $expects = array(
            'search-term'            => null,
            'opt-search-type'        => SearchServiceFactory::TYPE_ANY,
            'opt-export-complete'    => true,
            'opt-export-prefix'      => 'search-export',
            'opt-search-uid'         => false,
            'opt-has-code-only'      => true,
            'opt-has-given-list'     => false,
            'opt-given-count'        => 0,
            'opt-has-exclusion-list' => false,
            'opt-exclusion-count'    => 0,
            'output-is-autocomplete' => false,
            'output-is-export'       => false,
            'cfg-limit-main'         => 1000,
            'cfg-limit-sub'          => 5,
            'cfg-limits'             => array(
                'search-main'       => 1000,
                'search-sub'        => 5,
                'autocomplete-main' => 10,
                'autocomplete-sub'  => 3,
                'overridden'        => false,
            ),
        );

        $reflection = new \ReflectionClass(get_class($this->ss));
        $method = $reflection->getMethod('getDetails');
        $method->setAccessible(true);
        $details = $method->invoke($this->ss);

        $this->assertSame($expects, $details);
    }

    /**
     * @testdox Check Option Search Term & Query
     *
     * Functionality wise see @DrugRepositoryHelpersUnitTest.
     */
    public function testOptSearchTerm()
    {
        $this->assertSame(null, $this->ss->getTerm());
        $this->assertSame(null, $this->ss->getTermQuery());

        $expect = 'abc1234' . time();
        $this->ss->setTerm($expect);
        $this->ss->setTermQuery($expect);

        $this->assertSame($expect, $this->ss->getTerm());
        $this->assertSame($expect, $this->ss->getTermQuery());
    }

    /**
     * @testdox Check Option Search Type
     */
    public function testOptSearchType()
    {
        $this->assertSame(SearchServiceFactory::TYPE_ANY, $this->ss->getType());

        $expect = 'Generic ';
        $this->ss->setType($expect);

        $expect = strtolower($expect);
        $expect = trim($expect);
        $this->assertSame($expect, $this->ss->getType());

        $expect = 'any.drug.type.is.accepted';
        $this->ss->setType($expect);
        $this->assertSame($expect, $this->ss->getType());
    }

    /**
     * @testdox Check Option Auto Complete
     */
    public function testOptAutoComplete()
    {
        $this->assertSame(false, $this->ss->isAutoComplete());

        $expect = true;
        $this->ss->setAutoComplete($expect);

        $this->assertSame($expect, $this->ss->isAutoComplete());
    }

    /**
     * @testdox Check Option Export
     */
    public function testOptExport()
    {
        $this->assertSame(false, $this->ss->isExport());

        $expect = true;
        $this->ss->setExport($expect);

        $this->assertSame($expect, $this->ss->isExport());
    }

    /**
     * @testdox Check Option Export Complete
     */
    public function testOptExportComplete()
    {
        $this->assertSame(true, $this->ss->isExportComplete());

        $expect = false;
        $this->ss->setExportComplete($expect);

        $this->assertSame($expect, $this->ss->isExportComplete());
    }

    /**
     * @testdox Check Option Export Prefix
     */
    public function testOptExportPrefix()
    {
        $this->assertSame('search-export', $this->ss->getExportPrefix());

        $expect = 'export-example-test-' . time();
        $this->ss->setExportPrefix($expect);

        $this->assertSame($expect, $this->ss->getExportPrefix());
    }

    /**
     * @testdox Check Option UUID Only
     */
    public function testOptUnidOnly()
    {
        $this->assertSame(false, $this->ss->isUnidOnly());

        $expect = true;
        $this->ss->setUnidOnly($expect);

        $this->assertSame($expect, $this->ss->isUnidOnly());
    }

    /**
     * @testdox Check Option Has Code Only
     */
    public function testOptHasCodeOnly()
    {
        $this->assertSame(true, $this->ss->isHasCodeOnly());

        $expect = false;
        $this->ss->setHasCodeOnly($expect);

        $this->assertSame($expect, $this->ss->isHasCodeOnly());
    }

    /**
     * @testdox Check Option Given List
     */
    public function testOptGivenList()
    {
        $expectsDefault = array(
            'experimental' => array(),
            'generic'      => array(),
            'brand'        => array(),
        );
        $this->assertSame($expectsDefault, $this->ss->getGivenList());
        $this->assertSame(false,    $this->ss->hasGivenList());
        $this->assertSame(0,        $this->ss->getGivenListCount());

        $given = array(
            'brand'   => array(1, 2, 4, 6, 10),
            'unknown' => array(8, 9, 12),
        );
        $this->ss->setGivenList($given);
        $this->assertSame(true, $this->ss->hasGivenList());

        $expectsCount = (count($given['brand']) + count($given['unknown']));
        $this->assertSame($expectsCount, $this->ss->getGivenListCount());

        // $expectsList = ($given + $expectsDefault);
        $expectsList = array(
            'brand'        => array(1, 2, 4, 6, 10),
            'unknown'      => array(8, 9, 12),
            'experimental' => array(),
            'generic'      => array(),
        );
        $this->assertSame($expectsList, $this->ss->getGivenList());
    }

    /**
     * @testdox Check Option Exclusion List
     */
    public function testOptExclusionList()
    {
        $expectsDefault = array(
            'experimental'    => array(),
            'generic'         => array(),
            'generic_typo'    => array(),
            'generic_synonym' => array(),
            'brand'           => array(),
            'brand_typo'      => array(),
            'brand_synonym'   => array(),
        );
        $this->assertSame($expectsDefault, $this->ss->getExclusionList());
        $this->assertSame(false,    $this->ss->hasExclusionList());
        $this->assertSame(0,        $this->ss->getExclusionListCount());

        $given = array(
            'brand_typo' => array(1, 2, 4, 6, 10),
            'unknown'    => array(8, 9, 12),
        );
        $this->ss->setExclusionList($given);
        $this->assertSame(true, $this->ss->hasExclusionList());

        $expectsCount = (count($given['brand_typo']) + count($given['unknown']));
        $this->assertSame($expectsCount, $this->ss->getExclusionListCount());

        // $expectsList = ($given + $expectsDefault);
        $expectsList = array(
            'brand_typo'      => array(1, 2, 4, 6, 10),
            'unknown'         => array(8, 9, 12),
            'experimental'    => array(),
            'generic'         => array(),
            'generic_typo'    => array(),
            'generic_synonym' => array(),
            'brand'           => array(),
            'brand_synonym'   => array(),
        );
        $this->assertSame($expectsList, $this->ss->getExclusionList());
    }

    /**
     * @testdox Check Config limits
     */
    public function testConfigLimits()
    {
        $env = array(
            'SEARCH_LIMIT_SEARCHPAGE'       => 1000,
            'SEARCH_SUB_LIMIT_SEARCHPAGE'   => 5,
            'SEARCH_LIMIT_AUTOCOMPLETE'     => 10,
            'SEARCH_SUB_LIMIT_AUTOCOMPLETE' => 3,
        );

        $this->assertSame($env['SEARCH_LIMIT_SEARCHPAGE'], $this->ss->getLimitMain());
        $this->assertSame($env['SEARCH_SUB_LIMIT_SEARCHPAGE'], $this->ss->getLimitSubs());

        $this->ss->setAutoComplete(true);

        $this->assertSame($env['SEARCH_LIMIT_AUTOCOMPLETE'], $this->ss->getLimitMain());
        $this->assertSame($env['SEARCH_SUB_LIMIT_AUTOCOMPLETE'], $this->ss->getLimitSubs());

        $this->ss->setExport(true);

        $expectsZero = 0;
        $this->assertSame($expectsZero, $this->ss->getLimitMain());
        $this->assertSame($expectsZero, $this->ss->getLimitSubs());

        $expectsLimited = 10;
        $this->ss->setAutoComplete(false);
        $this->ss->setExport(false);
        $this->ss->setLimitMain($expectsLimited);
        $this->assertSame($expectsLimited, $this->ss->getLimitMain());

        $this->ss->setAutoComplete(true);
        $this->assertSame($expectsLimited, $this->ss->getLimitMain());
        $this->assertSame($env['SEARCH_SUB_LIMIT_AUTOCOMPLETE'], $this->ss->getLimitSubs());

        $this->ss->setExport(true);
        $this->assertSame($expectsLimited, $this->ss->getLimitMain());
        $this->assertSame($expectsZero, $this->ss->getLimitSubs());
    }
}
