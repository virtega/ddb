<?php

namespace App\Tests\Unit\Service\Traits;

use App\Service\Traits\EntityFormUpackServiceTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Service-Traits | EntityFormUpackServiceTrait
 */
class EntityFormUpackServiceTraitUnitTest extends WebTestCase
{
    use EntityFormUpackServiceTrait;

    /**
     * @var Mocked EntityManager|null
     */
    private $em;

    /**
     * @var Mocked \Symfony\Component\Security\Core\Security|null
     */
    private $symfonySecurity;

    /**
     * @dataProvider getCurrentUserUsernameStacksProvider
     *
     * @testdox Checking __getCurrentUserUsername() Stacks
     */
    public function test__getCurrentUserUsernameStacks($user, $expected)
    {
        $this->symfonySecurity = $this->createMock(\Symfony\Component\Security\Core\Security::class);
        $this->symfonySecurity->method('getUser')
            ->will($this->returnValue($user));

        $result = $this->__getCurrentUserUsername();

        $this->assertSame($expected, $result);
    }

    public function getCurrentUserUsernameStacksProvider()
    {
        $user1 = $this->createMock(\App\Security\User\LdapUser::class);
        $user1->method('getUsername')->will($this->returnValue('John.Doe'));

        return [
            // #0
            [null, 'Unknown'],
            // #1
            [$user1, 'John.Doe'],
        ];
    }

    /**
     * @dataProvider getCountryByIsoCodeStacksProvider
     *
     * @testdox Checking __getCountryByIsoCode() Stacks
     */
    public function test__getCountryByIsoCodeStacks($findOneByResult, $input, $expected)
    {
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->method('findOneBy')
            ->will($this->returnValue($findOneByResult));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        $result = $this->__getCountryByIsoCode($input);

        if ($expected) {
            $this->assertSame($findOneByResult, $result);
        }
        else {
            $this->assertNull($result);
        }
    }

    public function getCountryByIsoCodeStacksProvider()
    {
        return [
            // #0
            [
                new \App\Entity\Country,
                '',
                false,
            ],
            // #1
            [
                new \App\Entity\Country,
                'test',
                true,
            ],
            // #2
            [
                null,
                'test',
                false,
            ],
        ];
    }

    /**
     * @dataProvider getCountryTaxonomyByIdStacksProvider
     *
     * @testdox Checking __getCountryTaxonomyById() Stacks
     */
    public function test__getCountryTaxonomyByIdStacks($findOneByResult, $input, $expected)
    {
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->method('findOneBy')
            ->will($this->returnValue($findOneByResult));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        $result = $this->__getCountryTaxonomyById($input);

        if ($expected) {
            $this->assertSame($findOneByResult, $result);
        }
        else {
            $this->assertNull($result);
        }
    }

    public function getCountryTaxonomyByIdStacksProvider()
    {
        return [
            // #0
            [
                null,
                0,
                false,
            ],
            // #1
            [
                new \App\Entity\CountryTaxonomy,
                1,
                true,
            ],
            // #2
            [
                null,
                2,
                false,
            ],
        ];
    }

    /**
     * @testdox Checking __unpackAndAssignConferenceSpecilties()
     */
    public function test__unpackAndAssignConferenceSpecilties()
    {
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);

        $conference = new \App\Entity\Conference;

        $specialtyTaxonomyB41 = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomyB41->setSpecialty('Test 1');
        $reflProp->setValue($specialtyTaxonomyB41, 1);
        $conferenceSpecialtiesB41 = new \App\Entity\ConferenceSpecialties;
        $conferenceSpecialtiesB41->setConference($conference);
        $conferenceSpecialtiesB41->setSpecialty($specialtyTaxonomyB41);
        $conference->addSpecialty($conferenceSpecialtiesB41);

        $specialtyTaxonomyB42 = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomyB42->setSpecialty('Test 99');
        $reflProp->setValue($specialtyTaxonomyB42, 99);
        $conferenceSpecialtiesB42 = new \App\Entity\ConferenceSpecialties;
        $conferenceSpecialtiesB42->setConference($conference);
        $conferenceSpecialtiesB42->setSpecialty($specialtyTaxonomyB42);
        $conference->addSpecialty($conferenceSpecialtiesB42);

        $specialtyTaxonomyFD1 = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomyFD1->setSpecialty('Test 2');
        $reflProp->setValue($specialtyTaxonomyFD1, 2);

        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue($specialtyTaxonomyFD1));

        $specialtyTaxonomyFD2 = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomyFD2->setSpecialty('A Stored Specialty');
        $reflProp->setValue($specialtyTaxonomyFD2, 3);

        $repo->expects($this->at(1))->method('findOneBy')->will($this->returnValue(null));
        $repo->expects($this->at(2))->method('findOneBy')->will($this->returnValue($specialtyTaxonomyFD2));

        $repo->expects($this->at(3))->method('findOneBy')->will($this->returnValue(null));
        $repo->expects($this->at(4))->method('findOneBy')->will($this->returnValue(null));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        $this->em->expects($this->once())->method('remove')->will($this->returnValue(true));
        $this->em->method('persist')->will($this->returnValue(true));

        $this->assertSame(2, $conference->getSpecialties()->count());

        $this->__unpackAndAssignConferenceSpecilties($conference, ['1', '2', 'A Stored Specialty', 'A New Specialty']);

        $this->assertSame(4, $conference->getSpecialties()->count());

        $specialites = $conference->getSpecialties()->toArray();
        $specialites = array_values($specialites);

        $this->assertSame('Test 1', $specialites[0]->getSpecialty()->getSpecialty());
        $this->assertSame(1, $specialites[0]->getSpecialty()->getId());

        $this->assertSame('Test 2', $specialites[1]->getSpecialty()->getSpecialty());
        $this->assertSame(2, $specialites[1]->getSpecialty()->getId());

        $this->assertSame('A Stored Specialty', $specialites[2]->getSpecialty()->getSpecialty());
        $this->assertSame(3, $specialites[2]->getSpecialty()->getId());

        $this->assertSame('A New Specialty', $specialites[3]->getSpecialty()->getSpecialty());
        $this->assertSame(null, $specialites[3]->getSpecialty()->getId());
    }

    /**
     * @testdox Checking __unpackAndAssignJournalSpecilties()
     */
    public function test__unpackAndAssignJournalSpecilties()
    {
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);

        $journal = new \App\Entity\Journal;

        $specialtyTaxonomyB41 = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomyB41->setSpecialty('Test 1');
        $reflProp->setValue($specialtyTaxonomyB41, 1);
        $journalSpecialtiesB41 = new \App\Entity\JournalSpecialties;
        $journalSpecialtiesB41->setJournal($journal);
        $journalSpecialtiesB41->setSpecialty($specialtyTaxonomyB41);
        $journal->addSpecialty($journalSpecialtiesB41);

        $specialtyTaxonomyB42 = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomyB42->setSpecialty('Test 99');
        $reflProp->setValue($specialtyTaxonomyB42, 99);
        $journalSpecialtiesB42 = new \App\Entity\JournalSpecialties;
        $journalSpecialtiesB42->setJournal($journal);
        $journalSpecialtiesB42->setSpecialty($specialtyTaxonomyB42);
        $journal->addSpecialty($journalSpecialtiesB42);

        $specialtyTaxonomyFD1 = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomyFD1->setSpecialty('Test 2');
        $reflProp->setValue($specialtyTaxonomyFD1, 2);

        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue($specialtyTaxonomyFD1));

        $specialtyTaxonomyFD2 = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomyFD2->setSpecialty('A Stored Specialty');
        $reflProp->setValue($specialtyTaxonomyFD2, 3);

        $repo->expects($this->at(1))->method('findOneBy')->will($this->returnValue(null));
        $repo->expects($this->at(2))->method('findOneBy')->will($this->returnValue($specialtyTaxonomyFD2));

        $repo->expects($this->at(3))->method('findOneBy')->will($this->returnValue(null));
        $repo->expects($this->at(4))->method('findOneBy')->will($this->returnValue(null));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        $this->em->expects($this->once())->method('remove')->will($this->returnValue(true));
        $this->em->method('persist')->will($this->returnValue(true));

        $this->assertSame(2, $journal->getSpecialties()->count());

        $this->__unpackAndAssignJournalSpecilties($journal, ['1', '2', 'A Stored Specialty', 'A New Specialty']);

        $this->assertSame(4, $journal->getSpecialties()->count());

        $specialites = $journal->getSpecialties()->toArray();
        $specialites = array_values($specialites);

        $this->assertSame('Test 1', $specialites[0]->getSpecialty()->getSpecialty());
        $this->assertSame(1, $specialites[0]->getSpecialty()->getId());

        $this->assertSame('Test 2', $specialites[1]->getSpecialty()->getSpecialty());
        $this->assertSame(2, $specialites[1]->getSpecialty()->getId());

        $this->assertSame('A Stored Specialty', $specialites[2]->getSpecialty()->getSpecialty());
        $this->assertSame(3, $specialites[2]->getSpecialty()->getId());

        $this->assertSame('A New Specialty', $specialites[3]->getSpecialty()->getSpecialty());
        $this->assertSame(null, $specialites[3]->getSpecialty()->getId());
    }

    /**
     * @dataProvider getSearchOrCreateNewSpecialtyTaxonomyStacksProvider
     *
     * @testdox Checking ___searchOrCreateNewSpecialtyTaxonomy() Stacks
     */
    public function test__searchOrCreateNewSpecialtyTaxonomy($firstfindOneByResult, $secondfindOneByResult, $input, $willPersist, $expected)
    {
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);

        $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue($firstfindOneByResult));

        if (false !== $secondfindOneByResult) {
            $repo->expects($this->at(1))->method('findOneBy')->will($this->returnValue($secondfindOneByResult));
        }
        else {
            $repo->expects($this->exactly(1))->method('findOneBy');
        }

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        if ($willPersist) {
            $this->em->method('persist')->will($this->returnValue(true));
        }
        else {
            $this->em->expects($this->never())->method('persist');
        }

        $result = $this->___searchOrCreateNewSpecialtyTaxonomy($input);
        $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class, $result);
        $this->assertSame($expected, $result->getSpecialty());
    }

    public function getSearchOrCreateNewSpecialtyTaxonomyStacksProvider()
    {
        $speTax1 = new \App\Entity\SpecialtyTaxonomy;
        $speTax1->setSpecialty('test');

        return [
            // #0
            [
                $speTax1,
                false,
                'test',
                false,
                'test',
            ],
            // #1
            [
                null,
                $speTax1,
                'test',
                false,
                'test',
            ],
            // #2
            [
                null,
                null,
                'test',
                true,
                'test',
            ],
        ];
    }

    /**
     * @dataProvider getUnpackDateTimePickerFieldStacksProvider
     *
     * @testdox Checking __unpackDateTimePickerField() Stacks
     */
    public function test__unpackDateTimePickerFieldStacks($input, $opt, $expected)
    {
        $result = $this->__unpackDateTimePickerField($input, $opt);

        $this->assertSame($expected, ($result ? $result->format('Y-m-d H:i:s') : $result));
    }

    public function getUnpackDateTimePickerFieldStacksProvider()
    {
        return [
            // #0
            [
                '',
                'Y-m-d',
                null,
            ],
            // #1
            [
                '2019-08-08 08:08:08',
                'Y-m-d H:i:s',
                '2019-08-08 08:08:08',
            ],
            // #2
            [
                '08 2019 08 08:08 08',
                'Y-m-d H:i:s',
                null,
            ],
            // #3
            [
                '08 2019 08 08:08 08',
                'm Y d H:i s',
                '2019-08-08 08:08:08',
            ],
            // #4 - reset to 0hr
            [
                '2019-08-08',
                'Y-m-d',
                '2019-08-08 00:00:00',
            ],
            // #5 - reset to 0hr
            [
                '3 May 2016',
                'j M Y',
                '2016-05-03 00:00:00',
            ],
            // #6
            [
                '2016 12 6',
                'j M Y',
                null,
            ],
        ];
    }

    /**
     * @dataProvider getUnpackYesNoToIntRadioFieldStacksProvider
     *
     * @testdox Checking __unpackYesNoToIntRadioField() Stacks
     */
    public function test__unpackYesNoToIntRadioFieldStacks($input, $expected)
    {
        $result = $this->__unpackYesNoToIntRadioField($input);

        $this->assertSame($expected, $result);
    }

    public function getUnpackYesNoToIntRadioFieldStacksProvider()
    {
        return [
            // #0
            ['', 0],
            // #1
            ['yes', 1],
            // #2
            ['no', 0],
            // #3
            ['YES', 0],
            // #4
            ['BETUL', 0],
        ];
    }

    /**
     * @dataProvider getUnpackYesNoToBoolRadioFieldStacksProvider
     *
     * @testdox Checking __unpackYesNoToBoolRadioField() Stacks
     */
    public function test__unpackYesNoToBoolRadioFieldStacks($input, $expected)
    {
        $result = $this->__unpackYesNoToBoolRadioField($input);

        $this->assertSame($expected, $result);
    }

    public function getUnpackYesNoToBoolRadioFieldStacksProvider()
    {
        return [
            // #0
            ['', false],
            // #1
            ['yes', true],
            // #2
            ['no', false],
            // #3
            ['YES', false],
            // #4
            ['BETUL', false],
        ];
    }

    /**
     * @dataProvider getUnpackStringBaseListFieldStacksProvider
     *
     * @testdox Checking __unpackStringBaseListField() Stacks
     */
    public function test__unpackStringBaseListFieldStacks($input, $expected)
    {
        $result = $this->__unpackStringBaseListField($input);

        $this->assertSame($expected, $result);
    }

    public function getUnpackStringBaseListFieldStacksProvider()
    {
        return [
            // #0
            [
                [],
                '',
            ],
            // #1
            [
                ['a', 'b', 'c', 'd'],
                'a;b;c;d'
            ],
            // #2
            [
                ['a', 'b', 'c', 'd', 'c'],
                'a;b;c;d;c'
            ],
        ];
    }
}
