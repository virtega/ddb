<?php

namespace App\Tests\Unit\Service\Traits;

use App\Service\Traits\SourceDataConverterServiceTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Service-Traits | SourceDataConverterServiceTrait
 */
class SourceDataConverterServiceTraitUnitTest extends WebTestCase
{
    use SourceDataConverterServiceTrait;

    /**
     * @dataProvider getConvertInCountryIsoStacksProvider
     *
     * @testdox Checking ___convertInCountryIso() Stacks
     */
    public function test__convertInCountryIsoStacks($input, $expected, $expectedInput)
    {
        $result = self::___convertInCountryIso($input);

        $this->assertSame($expected, $result);
        $this->assertSame($expectedInput, $input);
    }

    public function getConvertInCountryIsoStacksProvider()
    {
        return [
            // #0
            [null, false, null],
            // #1
            ['    ', true, '    '],
            // #2
            ['ANY', true, 'ANY'],
            // #3
            ['@Error', false, '@Error'],
            // #4
            ['EU', false, 'EU'],
            // #5
            ['AA', true, 'AW'],
            // #6
            ['AV', true, 'AI'],
            // #7
            ['KO', true, 'KR'],
            // #8
            ['LO', true, 'LS'],
            // #9
            ['USA', true, 'US'],
            // #10
            ['75', true, 'US'],
            // #11
            ['UK', true, 'GB'],
        ];
    }

    /**
     * @dataProvider getConvertOutCountryIsoStacksProvider
     *
     * @testdox Checking ___convertOutCountryIso() Stacks
     */
    public function test__convertOutCountryIsoStacks($input, $expected)
    {
        $result = self::___convertOutCountryIso($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertOutCountryIsoStacksProvider()
    {
        return [
            // #0
            [null,      null],
            // #1
            ['    ',    '    '],
            // #2
            ['ANY',     'ANY'],
            // #3
            ['  Error ',   '  Error '],
            // #4
            ['EU',      'EU'],
            // #5
            ['AW',      'aa'],
            // #6
            ['AI',      'av'],
            // #7
            ['KR',      'ko'],
            // #8
            ['LS',      'lo'],
            // #9
            ['US',      'usa'],
            // #10
            ['GB',      'uk'],
            // #11
            ['SG',      'SG'],
        ];
    }

    /**
     * @dataProvider getConvertCountryInNameStacksProvider
     *
     * @testdox Checking __convertInCountryName() Stacks
     */
    public function test__convertInCountryNameStacks($input, $expected, $expectedInput)
    {
        $result = $this->__convertInCountryName($input);

        $this->assertSame($expected, $result);
        $this->assertSame($expectedInput, $input);
    }

    public function getConvertCountryInNameStacksProvider()
    {
        return [
            // #0
            [null, false, null],
            // #1
            ['    ', true, '    '],
            // #2
            ['ANY', true, 'ANY'],
            // #3
            ['Error', false, 'Error'],
            // #4
            ['Czeck Republic', true, 'Czech Republic'],
            // #5
            ['Czechia', true, 'Czech Republic'],
            // #6
            ['Korea (SOUTH)', true, 'South Korea'],
            // #7
            ['Slovak Republic', true, 'Slovakia'],
            // #8
            ['Czechoslovakia(former)', true, 'Slovakia'],
            // #9
            ['Singapore', true, 'Singapore'],
        ];
    }

    /**
     * @dataProvider getConvertOutCountryNameStacksProvider
     *
     * @testdox Checking ___convertOutCountryName() Stacks
     */
    public function test__convertOutCountryNameStacks($input, $expected)
    {
        $result = self::___convertOutCountryName($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertOutCountryNameStacksProvider()
    {
        return [
            // #0
            [null, null],
            // #1
            ['    ', '    '],
            // #2
            ['ANY', 'ANY'],
            // #3
            ['Error', 'Error'],
            // #4
            ['Czech Republic', 'Czeck Republic'],
            // #5
            ['South Korea', 'Korea (SOUTH)'],
            // #6
            ['Slovakia', 'Slovak Republic'],
            // #7
            ['Singapore', 'Singapore'],
        ];
    }

    /**
     * @dataProvider getConvertInRegionNameStacksProvider
     *
     * @testdox Checking __convertInRegionName() Stacks
     */
    public function test__convertInRegionNameStacks($input, $expected, $expectedInput)
    {
        $result = $this->__convertInRegionName($input);

        $this->assertSame($expected, $result);
        $this->assertSame($expectedInput, $input);
    }

    public function getConvertInRegionNameStacksProvider()
    {
        return [
            // #0
            [null, false, null],
            // #1
            ['    ', true, '    '],
            // #2
            ['Error', false, 'Error'],
            // #3
            ['@error 17412:', false, '@error 17412:'],
            // #4
            ['any error la', false, 'any error la'],
            // #5
            ['Cruises', false, 'Cruises'],
            // #6
            ['ON LINE', false, 'ON LINE'],
            // #7
            ['Australia & Oceania', true, 'Australia / Oceania'],
            // #8
            ['Latin America and The Caribbean', true, 'Latin America and the Caribbean'],
            // #9
            ['Latin America & the Caribbean', true, 'Latin America and the Caribbean'],
            // #10
            ['Indian Ocean', true, 'Indian Ocean'],
        ];
    }

    /**
     * @dataProvider getConvertOutRegionNameStacksProvider
     *
     * @testdox Checking ___convertOutRegionName() Stacks
     */
    public function test__convertOutRegionNameStacks($input, $expected)
    {
        $result = self::___convertOutRegionName($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertOutRegionNameStacksProvider()
    {
        return [
            // #0
            [null, null],
            // #1
            ['    ', '    '],
            // #2
            ['ANY', 'ANY'],
            // #3
            ['Error', 'Error'],
            // #4
            ['Australia / Oceania', 'Australia & Oceania'],
            // #5
            ['Latin America and the Caribbean', 'Latin America & the Caribbean'],
            // #6
            ['Southeast Asia', 'Southeast Asia'],
        ];
    }

    /**
     * @dataProvider getConvertInSpecialtyNameStacksProvider
     *
     * @testdox Checking __convertInSpecialtyName() Stacks
     */
    public function test__convertInSpecialtyNameStacks($input, $expected, $expectedInput)
    {
        $result = $this->__convertInSpecialtyName($input);

        $this->assertSame($expected, $result);
        $this->assertSame($expectedInput, $input);
    }

    public function getConvertInSpecialtyNameStacksProvider()
    {
        return [
            // #0
            [null, false, null],
            // #1
            ['    ', true, '    '],
            // #2
            ['Error', false, 'Error'],
            // #3
            ['   @error 17412:', false, '   @error 17412:'],
            // #4
            ['Aids/hiv', true, 'HIV/AIDS'],
            // #5
            ['Clinical Pharmacology', true, 'Clinical Pharmacology'],
            // #6
            ['Gynaecology and obstetrics', true, 'Gynaecology and Obstetrics'],
            // #7
            ['Oncology', true, 'Oncology'],
            // #8
            ['Hiv/aids', true, 'HIV/AIDS'],
        ];
    }
}
