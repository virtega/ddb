<?php

namespace App\Tests\Unit\Service\Traits;

use App\Service\Traits\SqlQueryBuildHelperServiceTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Service-Traits | SqlQueryBuildHelperServiceTrait
 */
class SqlQueryBuildHelperServiceTraitUnitTest extends WebTestCase
{
    use SqlQueryBuildHelperServiceTrait;

    /**
     * @dataProvider getTableNameFormatterStacksProvider
     *
     * @testdox Checking __tableNameFormatter() Stacks
     */
    public function test__tableNameFormatterStacks($tableName, $schema, $qoute, $expected)
    {
        $result = $this->__tableNameFormatter($tableName, $schema, $qoute);

        $this->assertSame($expected, $result);
    }

    public function getTableNameFormatterStacksProvider()
    {
        return [
            // #0
            ['example', 'sch', true, '"sch"."example"'],
            // #1
            ['example', 'sch', false, 'sch.example'],
            // #2
            ['example', '', false, 'example'],
            // #3
            ['example', null, false, 'example'],
            // #4
            ['example', null, true, '"example"'],
        ];
    }

    /**
     * @dataProvider getMappingIntoFieldsTokensAndBindersStacksProvider
     *
     * @testdox Checking __convertMappingIntoFieldsTokensAndBinders() Stacks
     */
    public function test__convertMappingIntoFieldsTokensAndBindersStacks($input, $expected)
    {
        $result = $this->__convertMappingIntoFieldsTokensAndBinders($input);

        $this->assertSame($expected, $result);
    }

    public function getMappingIntoFieldsTokensAndBindersStacksProvider()
    {
        return [
            // #0
            [
                [],
                ['', '', []],
            ],
            // #1
            [
                [
                    [
                        'field' => 'a',
                        'token' => ':a',
                        'bind'  => 'one',
                    ],
                    [
                        'field' => 'b',
                        'token' => ':b',
                        'bind'  => 'two',
                    ],
                ],
                [
                    'a,b',
                    ':a,:b',
                    [
                        ':a' => 'one',
                        ':b' => 'two',
                    ]
                ],
            ],
            // #2
            [
                [
                    [
                        'token' => ':a',
                        'bind'  => 'one',
                    ],
                    [
                        'token' => ':b',
                        'bind'  => 'two',
                    ],
                ],
                [
                    '',
                    ':a,:b',
                    [
                        ':a' => 'one',
                        ':b' => 'two',
                    ]
                ],
            ],
            // #3
            [
                [
                    [
                        'field' => 'a',
                        'bind'  => 'one',
                    ],
                    [
                        'field' => 'b',
                        'bind'  => 'two',
                    ],
                ],
                [
                    'a,b',
                    '',
                    [
                        0 => 'one',
                        1 => 'two',
                    ]
                ],
            ],
            // #4
            [
                [
                    [
                        'field' => 'a',
                    ],
                    [
                        'field' => 'b',
                    ],
                ],
                [
                    'a,b',
                    '',
                    []
                ],
            ],
        ];
    }

    /**
     * @dataProvider getMappingIntoSettersAndBindersStacksProvider
     *
     * @testdox Checking __convertMappingIntoSettersAndBinders() Stacks
     */
    public function test__convertMappingIntoSettersAndBindersStacks($input, $expected)
    {
        $result = $this->__convertMappingIntoSettersAndBinders($input);

        $this->assertSame($expected, $result);
    }

    public function getMappingIntoSettersAndBindersStacksProvider()
    {
        return [
            // #0
            [
                [],
                ['', []],
            ],
            // #1
            [
                [
                    [
                        'field' => 'a',
                        'token' => ':a',
                        'bind'  => 'one',
                    ],
                    [
                        'field' => 'b',
                        'token' => ':b',
                        'bind'  => 'two',
                    ],
                ],
                [
                    'a=:a, b=:b',
                    [
                        ':a' => 'one',
                        ':b' => 'two',
                    ]
                ],
            ],
            // #2
            [
                [
                    [
                        'token' => ':a',
                        'bind'  => 'one',
                    ],
                    [
                        'token' => ':b',
                        'bind'  => 'two',
                    ],
                ],
                [
                    '0=:a, 1=:b',
                    [
                        ':a' => 'one',
                        ':b' => 'two',
                    ]
                ],
            ],
            // #3
            [
                [
                    [
                        'field' => 'a',
                        'bind'  => 'one',
                    ],
                    [
                        'field' => 'b',
                        'bind'  => 'two',
                    ],
                ],
                [
                    '',
                    [
                        0 => 'one',
                        1 => 'two',
                    ]
                ],
            ],
            // #4
            [
                [
                    [
                        'field' => 'a',
                    ],
                    [
                        'field' => 'b',
                    ],
                ],
                [
                    '',
                    []
                ],
            ],
        ];
    }

    /**
     * @testdox Checking __bindingValues()
     */
    public function test__bindingValues()
    {
        $mapping = [
            ':a' => null,
            ':c' => false,
            ':e' => '',
            ':d' => 'one',
            ':f' => '0',
            ':g' => 0,
        ];

        $statement = $this->createMock(\PDOStatement::class);

        $statement->expects($this->exactly(5))->method('bindValue');

        $statement->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
            $this->assertSame(':a',  $token);
            $this->assertNull($value);
            $this->assertSame(\PDO::PARAM_NULL, $type);
        }));

        $statement->expects($this->at(1))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
            $this->assertSame(':e',  $token);
            $this->assertSame('', $value);
            $this->assertSame(\PDO::PARAM_STR, $type);
        }));

        $statement->expects($this->at(2))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
            $this->assertSame(':d',  $token);
            $this->assertSame('one', $value);
            $this->assertSame(\PDO::PARAM_STR, $type);
        }));

        $statement->expects($this->at(3))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
            $this->assertSame(':f',  $token);
            $this->assertSame('0', $value);
            $this->assertSame(\PDO::PARAM_STR, $type);
        }));

        $statement->expects($this->at(4))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
            $this->assertSame(':g',  $token);
            $this->assertSame(0, $value);
            $this->assertSame(\PDO::PARAM_INT, $type);
        }));

        $this->__bindingValues($statement, $mapping);
    }
}
