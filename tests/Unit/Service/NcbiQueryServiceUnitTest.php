<?php

namespace App\Tests\Unit\Service;

use App\Service\NcbiQueryService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Service | NcbiQueryService
 */
class NcbiQueryServiceUnitTest extends WebTestCase
{
    private $logger;
    private $client;
    private $ncbiService;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->logger = $this->createMock(\Psr\Log\LoggerInterface::class);
        $this->client = $this->createMock(\Symfony\Contracts\HttpClient\HttpClientInterface::class);
    }

    /**
     * @testdox Checking Initialization & Constants
     */
    public function testInitialize()
    {
        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);

        $this->assertInstanceOf(NcbiQueryService::class, $this->ncbiService);

        $this->assertSame(NcbiQueryService::SEARCH_FIELD_TITLE, 'JOURNAL');
        $this->assertSame(NcbiQueryService::SEARCH_FIELD_ISSN, 'ISSN');

        $class = new \ReflectionClass(NcbiQueryService::class);

        $this->assertSame('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/', $class->getConstant('NCBI_UTILS_PATH'));

        $this->assertSame('esearch.fcgi', $class->getConstant('NCBI_UTILS_ESEARCH'));

        $this->assertSame('esummary.fcgi', $class->getConstant('NCBI_UTILS_ESUMMARY'));

        $this->assertSame('nlmcatalog', $class->getConstant('SEARCH_DB'));

        $property = $class->getProperty('queryPaginationPage');
        $property->setAccessible(true);
        $this->assertSame(1, $property->getValue($this->ncbiService));

        $property = $class->getProperty('queryPaginationMaxPerPage');
        $property->setAccessible(true);
        $this->assertSame(20, $property->getValue($this->ncbiService));
    }

    /**
     * @testdox Checking setCurrentPaginationPage()
     */
    public function testSetCurrentPaginationPage()
    {
        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);
        $result = $this->ncbiService->setCurrentPaginationPage();
        $this->assertSame($result, 1);

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);
        $result = $this->ncbiService->setCurrentPaginationPage(10);
        $this->assertSame($result, 10);

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);
        try {
            $result = $this->ncbiService->setCurrentPaginationPage(-2);
            $this->assertFalse(true, 'This should not be executed, Exception should be thrown.');
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('invalid current-page value', $e->getMessage());
        }
    }

    /**
     * @testdox Checking setMaximumPerPaginationPage()
     */
    public function testSetMaximumPerPaginationPage()
    {
        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);
        $result = $this->ncbiService->setMaximumPerPaginationPage();
        $this->assertSame($result, 20);

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);
        $result = $this->ncbiService->setMaximumPerPaginationPage(10);
        $this->assertSame($result, 10);

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);
        try {
            $result = $this->ncbiService->setMaximumPerPaginationPage(-2);
            $this->assertFalse(true, 'This should not be executed, Exception should be thrown.');
        }
        catch (\Exception $e) {
            $this->assertStringContainsStringIgnoringCase('invalid maximum-per-page value', $e->getMessage());
        }
    }

    /**
     * @todo May have for future
     *
     * @testdox Checking restructureJournalDetails()
     */
    public function testRestructureJournalDetails()
    {
        $array = [];
        $result = NcbiQueryService::restructureJournalDetails($array);
        $this->assertSame($array, $result);

        $array = [
            0,
            'a' => 'b',
            'r' => 43,
        ];
        $result = NcbiQueryService::restructureJournalDetails($array);
        $this->assertSame($array, $result);
    }

    /**
     * @testdox Checking eSearch() with no issue
     */
    public function testESearchOK()
    {
        $sentQuery = [
            'db'       => 'nlmcatalog',
            'field'    => 'test-field',
            'term'     => 'test-term',
            'rettype'  => 'uilist',
            'retmode'  => 'json',
            'retmax'   => 50,
            'retstart' => 100,
        ];

        $responseContent = [
            'headers' => [
                'type'    => 'esearch',
                'version' => '0.3',
            ],
            'esearchresult' => [
                'count'    => '2',
                'retmax'   => '2',
                'retstart' => '0',
                'idlist'   => [
                    '101223209',
                    '100897065',
                ],
                'translationset'   => [],
                'translationstack' => [
                    [
                        'term'    => 'AAPS PharmSci[Title]',
                        'field'   => 'Title',
                        'count'   => '2',
                        'explode' => 'N',
                    ],
                    'GROUP',
                ],
                'querytranslation' => 'AAPS PharmSci[Title]',
                'warninglist'      => [
                    'phrasesignored'        => [],
                    'quotedphrasesnotfound' => [],
                    'outputmessages'        => [],
                ],
            ],
        ];

        $this->client->expects($this->at(0))->method('request')->will($this->returnCallback(function($method, $url, $payload) use ($sentQuery, $responseContent) {
            $this->assertSame('GET', $method);
            $this->assertSame('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi', $url);
            $this->assertSame([
                'query'   => $sentQuery,
                'headers' => [
                    'Content-Type' => 'application/json; charset=UTF-8',
                ],
            ], $payload);

            $response = $this->createMock(\Symfony\Contracts\HttpClient\ResponseInterface::class);
            $response->expects($this->at(0))->method('toArray')->will($this->returnCallback(function($throw) use ($responseContent) {
                $this->assertFalse($throw);
                return $responseContent;
            }));

            $response->expects($this->at(1))->method('getInfo')->will($this->returnCallback(function($attr) {
                $this->assertSame('http_method', $attr);
                return 'GET';
            }));

            $response->expects($this->at(2))->method('getInfo')->will($this->returnCallback(function($attr) use ($sentQuery) {
                $this->assertSame('url', $attr);

                $query = http_build_query($sentQuery);
                return 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?' . $query;
            }));

            $response->expects($this->at(3))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            $response->expects($this->at(4))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            return $response;
        }));

        $this->logger->expects($this->at(0))->method('debug')->will($this->returnCallback(function($message, $context) use ($sentQuery, $responseContent) {
            $this->assertSame('NCBI Query', $message);

            $query = http_build_query($sentQuery);
            $this->assertSame([
                'method'  => 'GET',
                'path'    => 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?' . $query,
                'code'    => 200,
                'content' => $responseContent,
            ], $context);

            return true;
        }));

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);
        $this->ncbiService->setCurrentPaginationPage(3);
        $this->ncbiService->setMaximumPerPaginationPage(50);

        $result = $this->ncbiService->eSearch('test-field', 'test-term');

        $this->assertSame($responseContent['esearchresult'], $result);
    }

    /**
     * @testdox Checking eSearch() with Invalid Status Code
     */
    public function testESearchWithInvalidStatusCode()
    {
        $this->client->expects($this->at(0))->method('request')->will($this->returnCallback(function($method, $url, $payload) {
            $response = $this->createMock(\Symfony\Contracts\HttpClient\ResponseInterface::class);
            $response->expects($this->at(0))->method('toArray')->will($this->returnCallback(function($throw) {
                return [];
            }));

            $response->expects($this->at(1))->method('getInfo')->will($this->returnCallback(function($attr) {
                return 'GET';
            }));

            $response->expects($this->at(2))->method('getInfo')->will($this->returnCallback(function($attr) {
                return 'https://google.com';
            }));

            $response->expects($this->at(3))->method('getStatusCode')->will($this->returnCallback(function() {
                return 400;
            }));

            $response->expects($this->at(4))->method('getStatusCode')->will($this->returnCallback(function() {
                return 400;
            }));

            return $response;
        }));

        $this->logger->expects($this->at(0))->method('debug')->will($this->returnCallback(function($message, $context) {
            $this->assertSame('NCBI Query', $message);

            $this->assertSame([
                'method'  => 'GET',
                'path'    => 'https://google.com',
                'code'    => 400,
                'content' => [],
            ], $context);

            return true;
        }));

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);

        try {
            $result = $this->ncbiService->eSearch('test-field', 'test-term');
            $this->assertFalse(true, 'This should not be executed, Exception should be thrown.');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\TrirdPartyException::class, $e);
            $this->assertStringContainsStringIgnoringCase('NCBI Error: esearch.fcgi - Unknown Error', $e->getMessage());
        }
    }

    /**
     * @testdox Checking eSearch() with Error in 200
     */
    public function testESearchWithErrorMessage()
    {
        $sentQuery = [
            'db'       => 'unknown-db',
            'field'    => 'test-field',
            'term'     => 'test-term',
            'rettype'  => 'uilist',
            'retmode'  => 'json',
            'retmax'   => 20,
            'retstart' => 0,
        ];

        $responseContent = [
            'headers' => [
                'type'    => 'esearch',
                'version' => '0.3',
            ],
            'esearchresult' => [
                'ERROR' => 'Invalid db name specified: unknown-db',
            ],
        ];

        $this->client->expects($this->at(0))->method('request')->will($this->returnCallback(function($method, $url, $payload) use ($sentQuery, $responseContent) {
            $this->assertSame('GET', $method);
            $this->assertSame('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi', $url);
            // skip check $payload - constant value

            $response = $this->createMock(\Symfony\Contracts\HttpClient\ResponseInterface::class);
            $response->expects($this->at(0))->method('toArray')->will($this->returnCallback(function($throw) use ($responseContent) {
                $this->assertFalse($throw);
                return $responseContent;
            }));

            $response->expects($this->at(1))->method('getInfo')->will($this->returnCallback(function($attr) {
                $this->assertSame('http_method', $attr);
                return 'GET';
            }));

            $response->expects($this->at(2))->method('getInfo')->will($this->returnCallback(function($attr) use ($sentQuery) {
                $this->assertSame('url', $attr);

                $query = http_build_query($sentQuery);
                return 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?' . $query;
            }));

            $response->expects($this->at(3))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            $response->expects($this->at(4))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            return $response;
        }));

        $this->logger->expects($this->at(0))->method('debug')->will($this->returnCallback(function($message, $context) use ($sentQuery, $responseContent) {
            $this->assertSame('NCBI Query', $message);

            $query = http_build_query($sentQuery);
            $this->assertSame([
                'method'  => 'GET',
                'path'    => 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?' . $query,
                'code'    => 200,
                'content' => $responseContent,
            ], $context);

            return true;
        }));

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);

        try {
            $result = $this->ncbiService->eSearch('test-field', 'test-term');
            $this->assertFalse(true, 'This should not be executed, Exception should be thrown.');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\TrirdPartyException::class, $e);
            $this->assertStringContainsStringIgnoringCase('NCBI Error: E-Search - Invalid db name specified: unknown-db', $e->getMessage());
        }
    }

    /**
     * @testdox Checking eSearch() with Corrupted Results
     */
    public function testESearchWithCorruptedResults()
    {
        $responseContent = [
            'headers' => [
                'type'    => 'esearch',
                'version' => '0.3',
            ],
            'alien-attribute' => [],
        ];

        $this->client->expects($this->at(0))->method('request')->will($this->returnCallback(function($method, $url, $payload) use ($responseContent) {
            $response = $this->createMock(\Symfony\Contracts\HttpClient\ResponseInterface::class);
            $response->expects($this->at(0))->method('toArray')->will($this->returnCallback(function($throw) use ($responseContent) {
                return $responseContent;
            }));

            $response->expects($this->at(1))->method('getInfo')->will($this->returnCallback(function($attr) {
                return 'GET';
            }));

            $response->expects($this->at(2))->method('getInfo')->will($this->returnCallback(function($attr) {
                return 'https://google.com';
            }));

            $response->expects($this->at(3))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            $response->expects($this->at(4))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            return $response;
        }));

        $this->logger->expects($this->at(0))->method('debug')->will($this->returnCallback(function($message, $context) use ($responseContent) {
            $this->assertSame('NCBI Query', $message);

            $this->assertSame([
                'method'  => 'GET',
                'path'    => 'https://google.com',
                'code'    => 200,
                'content' => $responseContent,
            ], $context);

            return true;
        }));

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);

        try {
            $result = $this->ncbiService->eSearch('test-field', 'test-term');
            $this->assertFalse(true, 'This should not be executed, Exception should be thrown.');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\TrirdPartyException::class, $e);
            $this->assertStringContainsStringIgnoringCase('NCBI Error: E-Search - Invalid Return structure.', $e->getMessage());
        }
    }

    /**
     * @testdox Checking eSummary() with no issue
     */
    public function testESummaryOK()
    {
        $sentQuery = [
            'db'       => 'nlmcatalog',
            'id'       => '101223209,100897065',
            'retmode'  => 'json',
            'retmax'   => 50,
            'retstart' => 100,
        ];

        $responseContent = json_decode('{
            "header": {
                "type": "esummary",
                "version": "0.3"
            },
            "result": {
                "uids": [
                    "101223209",
                    "100897065"
                ],
                "101223209": {
                    "uid": "101223209",
                    "host": "",
                    "nlmuniqueid": "101223209",
                    "daterevised": "2019-06-22",
                    "authorlist": [
                        {
                            "collectivename": "American Association of Pharmaceutical Scientists.",
                            "lastname": "",
                            "forename": "",
                            "namequalifier": "",
                            "otherinformation": "",
                            "titleassociatedwithname": "",
                            "aiid": ""
                        }
                    ],
                    "publicationinfolist": [
                        {
                            "imprint": "Arlington, Va., USA : American Association of Pharmaceutical Scientists, [2004]-",
                            "place": "Arlington, Va., USA :",
                            "publisher": "American Association of Pharmaceutical Scientists,",
                            "dateissued": "[2004]-",
                            "edition": "",
                            "datesofserialpublication": "Vol. 6, issue 03-"
                        }
                    ],
                    "resourceinfolist": [
                        {
                            "typeofresource": "Serial",
                            "resourceunit": "Remote electronic resource, Text"
                        }
                    ],
                    "titlemainlist": [
                        {
                            "sorttitle": "aaps journal",
                            "title": "The AAPS journal.",
                            "aiid": ""
                        }
                    ],
                    "titlemainsort": "aaps journal",
                    "titleotherlist": [
                        {
                            "titlealternate": "AAPSJ",
                            "otherinformation": "",
                            "aiid": ""
                        },
                        {
                            "titlealternate": "American Association of Pharmaceutical Scientists journal",
                            "otherinformation": "",
                            "aiid": ""
                        },
                        {
                            "titlealternate": "AAPS annual meeting abstracts.",
                            "otherinformation": "",
                            "aiid": ""
                        },
                        {
                            "titlealternate": "AAPS journal. Annual meeting abstracts",
                            "otherinformation": "",
                            "aiid": ""
                        },
                        {
                            "titlealternate": "AAPS PharmSci",
                            "otherinformation": "",
                            "aiid": ""
                        }
                    ],
                    "issnlist": [
                        {
                            "issn": "1550-7416",
                            "validyn": "Y",
                            "issntype": "Electronic"
                        },
                        {
                            "issn": "1550-7416",
                            "validyn": "Y",
                            "issntype": "Linking"
                        }
                    ],
                    "isbn": "",
                    "country": "United States",
                    "currentindexingstatus": "Y",
                    "medlineta": "AAPS J",
                    "isoabbreviation": "AAPS J",
                    "startyear": "2004",
                    "jrid": "32024",
                    "endyear": "9999",
                    "language": "eng",
                    "broadheading": [
                        "Drug Therapy",
                        "Pharmacology"
                    ],
                    "continuationnotes": "Title from issue caption (publisher Web site, viewed Aug. 16, 2004).; Issues for vol. 6, issue 3- called also vol. 6, no. 3-; Vols. have an extra no. designated \"S1\" which contains abstracts for that year\'s AAPS annual meeting.; AAPS PharmSci"
                },
                "100897065": {
                    "uid": "100897065",
                    "host": "",
                    "nlmuniqueid": "100897065",
                    "daterevised": "2019-06-22",
                    "authorlist": [
                        {
                            "collectivename": "American Association of Pharmaceutical Scientists.",
                            "lastname": "",
                            "forename": "",
                            "namequalifier": "",
                            "otherinformation": "",
                            "titleassociatedwithname": "",
                            "aiid": ""
                        }
                    ],
                    "publicationinfolist": [
                        {
                            "imprint": "Alexandria, Va. : American Association of Pharmaceutical Scientists, [1999-2004]",
                            "place": "Alexandria, Va. :",
                            "publisher": "American Association of Pharmaceutical Scientists,",
                            "dateissued": "[1999-2004]",
                            "edition": "",
                            "datesofserialpublication": "Vol. 1, issue 1 (Jan.-Mar. 1999)-v. 6, issue 2."
                        }
                    ],
                    "resourceinfolist": [
                        {
                            "typeofresource": "Serial",
                            "resourceunit": "Remote electronic resource, Text"
                        }
                    ],
                    "titlemainlist": [
                        {
                            "sorttitle": "aaps pharmsci",
                            "title": "AAPS pharmSci.",
                            "aiid": ""
                        }
                    ],
                    "titlemainsort": "aaps pharmsci",
                    "titleotherlist": [
                        {
                            "titlealternate": "AAPS PharmSci",
                            "otherinformation": "",
                            "aiid": ""
                        },
                        {
                            "titlealternate": "PharmSci",
                            "otherinformation": "",
                            "aiid": ""
                        },
                        {
                            "titlealternate": "Pharmacogenetics/pharmacogenomics virtual journal.",
                            "otherinformation": "",
                            "aiid": ""
                        },
                        {
                            "titlealternate": "AAPS PharmSci. Supplement",
                            "otherinformation": "",
                            "aiid": ""
                        },
                        {
                            "titlealternate": "AAPS PharmSci. Supplement (CD-ROM)",
                            "otherinformation": "",
                            "aiid": ""
                        },
                        {
                            "titlealternate": "AAPS journal",
                            "otherinformation": "",
                            "aiid": ""
                        }
                    ],
                    "issnlist": [
                        {
                            "issn": "1522-1059",
                            "validyn": "Y",
                            "issntype": "Electronic"
                        },
                        {
                            "issn": "1522-1059",
                            "validyn": "Y",
                            "issntype": "Linking"
                        }
                    ],
                    "isbn": "",
                    "country": "United States",
                    "currentindexingstatus": "N",
                    "medlineta": "AAPS PharmSci",
                    "isoabbreviation": "AAPS PharmSci",
                    "startyear": "1999",
                    "jrid": "22454",
                    "endyear": "2004",
                    "language": "eng",
                    "broadheading": [
                        "Drug Therapy",
                        "Pharmacology"
                    ],
                    "continuationnotes": "Title from title screen (viewed Jan. 24, 2000).; Earlier versions of publisher\'s website carried slightly different numbering, e.g., v. 1, issue 1 (Jan./Mar. 1999) now is designated v. 1, no. 1 (1998).; Vol. 1, no. 1 (1998) contains abstracts of the 1998 AAPS annual meeting; vols. for 1999- have an extra no. designated \"S1\" which contains abstracts for that year\'s meeting.; Publisher\'s website contains a page of links to PubMed (National Library of Medicine) for topics in pharmacogenetics and pharmacogenomics, titled: Pharmacogenetics/pharmacogenomics virtual journal.; AAPS journal"
                }
            }
        }', true);

        $this->client->expects($this->at(0))->method('request')->will($this->returnCallback(function($method, $url, $payload) use ($sentQuery, $responseContent) {
            $this->assertSame('GET', $method);
            $this->assertSame('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi', $url);
            $this->assertSame([
                'query'   => $sentQuery,
                'headers' => [
                    'Content-Type' => 'application/json; charset=UTF-8',
                ],
            ], $payload);

            $response = $this->createMock(\Symfony\Contracts\HttpClient\ResponseInterface::class);
            $response->expects($this->at(0))->method('toArray')->will($this->returnCallback(function($throw) use ($responseContent) {
                $this->assertFalse($throw);
                return $responseContent;
            }));

            $response->expects($this->at(1))->method('getInfo')->will($this->returnCallback(function($attr) {
                $this->assertSame('http_method', $attr);
                return 'GET';
            }));

            $response->expects($this->at(2))->method('getInfo')->will($this->returnCallback(function($attr) use ($sentQuery) {
                $this->assertSame('url', $attr);

                $query = http_build_query($sentQuery);
                return 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?' . $query;
            }));

            $response->expects($this->at(3))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            $response->expects($this->at(4))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            return $response;
        }));

        $this->logger->expects($this->at(0))->method('debug')->will($this->returnCallback(function($message, $context) use ($sentQuery, $responseContent) {
            $this->assertSame('NCBI Query', $message);

            $query = http_build_query($sentQuery);
            $this->assertSame([
                'method'  => 'GET',
                'path'    => 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?' . $query,
                'code'    => 200,
                'content' => $responseContent,
            ], $context);

            return true;
        }));

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);
        $this->ncbiService->setCurrentPaginationPage(3);
        $this->ncbiService->setMaximumPerPaginationPage(50);

        $result = $this->ncbiService->eSummary([101223209, 100897065]);

        $this->assertSame($responseContent['result'], $result);
    }

    /**
     * @testdox Checking eSummary() with CorruptedResults
     */
    public function testESummaryWithCorruptedResults()
    {
        $sentQuery = [
            'db'       => 'nlmcatalog',
            'id'       => '101223209,100897065',
            'retmode'  => 'json',
            'retmax'   => 25,
            'retstart' => 50,
        ];

        $responseContent = json_decode('{
            "header": {
                "type": "esummary",
                "version": "0.3"
            },
            "esummaryresult": [
                "This is an example error message"
            ]
        }', true);

        $this->client->expects($this->at(0))->method('request')->will($this->returnCallback(function($method, $url, $payload) use ($sentQuery, $responseContent) {
            $this->assertSame('GET', $method);
            $this->assertSame('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi', $url);
            $this->assertSame([
                'query'   => $sentQuery,
                'headers' => [
                    'Content-Type' => 'application/json; charset=UTF-8',
                ],
            ], $payload);

            $response = $this->createMock(\Symfony\Contracts\HttpClient\ResponseInterface::class);
            $response->expects($this->at(0))->method('toArray')->will($this->returnCallback(function($throw) use ($responseContent) {
                $this->assertFalse($throw);
                return $responseContent;
            }));

            $response->expects($this->at(1))->method('getInfo')->will($this->returnCallback(function($attr) {
                $this->assertSame('http_method', $attr);
                return 'GET';
            }));

            $response->expects($this->at(2))->method('getInfo')->will($this->returnCallback(function($attr) use ($sentQuery) {
                $this->assertSame('url', $attr);

                $query = http_build_query($sentQuery);
                return 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?' . $query;
            }));

            $response->expects($this->at(3))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            $response->expects($this->at(4))->method('getStatusCode')->will($this->returnCallback(function() {
                return 200;
            }));

            return $response;
        }));

        $this->logger->expects($this->at(0))->method('debug')->will($this->returnCallback(function($message, $context) use ($sentQuery, $responseContent) {
            $this->assertSame('NCBI Query', $message);

            $query = http_build_query($sentQuery);
            $this->assertSame([
                'method'  => 'GET',
                'path'    => 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?' . $query,
                'code'    => 200,
                'content' => $responseContent,
            ], $context);

            return true;
        }));

        $this->ncbiService = new NcbiQueryService($this->logger, $this->client);
        $this->ncbiService->setCurrentPaginationPage(3);
        $this->ncbiService->setMaximumPerPaginationPage(25);

        try {
            $result = $this->ncbiService->eSummary([101223209, 100897065]);
            $this->assertFalse(true, 'This should not be executed, Exception should be thrown.');
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\TrirdPartyException::class, $e);
            $this->assertStringContainsStringIgnoringCase('NCBI Error: E-Summary - This is an example error message', $e->getMessage());
        }
    }
}
