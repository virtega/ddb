<?php

namespace App\Tests\Unit\Service;

use App\Service\JournalSourceService;
use App\Utility\MsSqlRemoteConnection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Security;

/**
 * @testdox UNIT | Service | JournalSourceService
 */
class JournalSourceServiceUnitTest extends WebTestCase
{
    /**
     * @var JournalSourceService
     */
    private $sourceService;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();

        $this->sourceService = $client->getContainer()->get('test.' . JournalSourceService::class);

        parent::setUp();
    }

    /**
     * @testdox Checking Class Initialization and UpSync Disabled
     */
    public function testInitAndUpSyncDisabled()
    {
        $this->__setupMocks(null, null, null, false);

        $this->assertInstanceOf(JournalSourceService::class, $this->sourceService);
        $this->assertFalse($this->sourceService->isUpSync());
        $this->assertSame('"dbJ"."tableOne"',   $this->sourceService->getPrimaryTableName(true, true));
        $this->assertSame('"dbJ"."tableTwo"',   $this->sourceService->getSecondaryTableName(true, true));
        $this->assertSame('"dbJ"."tableThree"', $this->sourceService->getSpecialtiesTableName(true, true));
        $this->assertSame('dbJ.tableOne',   $this->sourceService->getPrimaryTableName(true, false));
        $this->assertSame('dbJ.tableTwo',   $this->sourceService->getSecondaryTableName(true, false));
        $this->assertSame('dbJ.tableThree', $this->sourceService->getSpecialtiesTableName(true, false));
        $this->assertSame('"tableOne"',   $this->sourceService->getPrimaryTableName(false, true));
        $this->assertSame('"tableTwo"',   $this->sourceService->getSecondaryTableName(false, true));
        $this->assertSame('"tableThree"', $this->sourceService->getSpecialtiesTableName(false, true));
        $this->assertSame('tableOne',   $this->sourceService->getPrimaryTableName(false, false));
        $this->assertSame('tableTwo',   $this->sourceService->getSecondaryTableName(false, false));
        $this->assertSame('tableThree', $this->sourceService->getSpecialtiesTableName(false, false));

        $entity = $this->_loadEntity();

        $result = $this->sourceService->upsync($entity);
        $this->assertFalse($result);

        $result = $this->sourceService->remove($entity);
        $this->assertFalse($result);
    }

    /**
     * @testdox Checking upsync() create with no issue
     */
    public function testUpSyncCreateEmptyOk()
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableOne"', [101, 1], 0);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_createNewJournalOnTable('"dbJ"."tableOne"', [101, 1], true);
        $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_copyRemoteIds(['UNID' => '123456789012345678901234567890AB', 'rowguid' => 'DC7A21D1-28E1-4D08-BBDC-8D0610E4AA88']);
        $remote->expects($this->at(3))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableTwo"', ['123456789012345678901234567890AB', 2], 0);
        $remote->expects($this->at(4))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_createNewJournalOnTable('"dbJ"."tableTwo"', ['123456789012345678901234567890AB', 2], true);
        $remote->expects($this->at(5))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_updateJournal(false, true, true);
        $remote->expects($this->at(6))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_updateJournalSpecialties(true, false);
        $remote->expects($this->at(7))->method('getConnection')->will($this->returnValue($conn));

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) {
            $this->assertStringContainsStringIgnoringCase('Remote Journal UpSync Complete', $msg);
            $this->assertArrayHasKey('P', $args);
            $this->assertSame('insert', $args['P']);
            $this->assertArrayHasKey('I', $args);
            $this->assertSame(101, $args['I']);
            return true;
        }));

        $this->__setupMocks($remote, $logger);

        $entity = $this->_loadEntityId();

        $result = $this->sourceService->upsync($entity);
        $this->assertTrue($result);

        $this->assertSame(101, $entity->getId());
        $this->assertSame('123456789012345678901234567890AB', $entity->getUnid());
        $this->assertSame('DC7A21D1-28E1-4D08-BBDC-8D0610E4AA88', $entity->getDetailsPreviousRowGuid());

        return $entity;
    }

    /**
     * @depends testUpSyncCreateEmptyOk
     *
     * @testdox Checking upsync() update with no issue
     */
    public function testUpSyncUpdateDataOk($entity)
    {
        $entity = $this->_loadEntityData($entity);

        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableOne"', [101, 1], 1);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableTwo"', ['123456789012345678901234567890AB', 2], 1);
        $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_updateJournal(true, true, true);
        $remote->expects($this->at(3))->method('getConnection')->will($this->returnValue($conn));

        // _updateJournal > _getMappingForSecondaryTable
        $em = $this->_getMappingForSecondaryTable(0, null, true, true);

        $conn = $this->_updateJournalSpecialties(true, true, true);
        $remote->expects($this->at(4))->method('getConnection')->will($this->returnValue($conn));

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) {
            $this->assertStringContainsStringIgnoringCase('Remote Journal UpSync Complete', $msg);
            $this->assertArrayHasKey('P', $args);
            $this->assertSame('update', $args['P']);
            $this->assertArrayHasKey('I', $args);
            $this->assertSame(101, $args['I']);
            return true;
        }));

        $this->__setupMocks($remote, $logger, $em);

        $result = $this->sourceService->upsync($entity);
        $this->assertTrue($result);

        $this->assertSame(101, $entity->getId());
        $this->assertSame('123456789012345678901234567890AB', $entity->getUnid());
        $this->assertSame('DC7A21D1-28E1-4D08-BBDC-8D0610E4AA88', $entity->getDetailsPreviousRowGuid());
    }

    /**
     * @testdox Checking upsync() update with on un-even ok
     */
    public function testUpSyncUpdateDataUnEvenOk()
    {
        $entity = $this->_loadEntityId();
        $entity = $this->_loadEntityData($entity);

        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableOne"', [101, 1], 1);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_createJournalUnid(true);
        $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_copyRemoteIds(['UNID' => '123456789012345678901234567890AB', 'rowguid' => 'DC7A21D1-28E1-4D08-BBDC-8D0610E4AA88']);
        $remote->expects($this->at(3))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableTwo"', ['123456789012345678901234567890AB', 2], 1);
        $remote->expects($this->at(4))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_updateJournal(true, true, true);
        $remote->expects($this->at(5))->method('getConnection')->will($this->returnValue($conn));

        // _updateJournal > _getMappingForSecondaryTable
        $em = $this->_getMappingForSecondaryTable(0, null, true, true);

        $conn = $this->_updateJournalSpecialties(true, true, true);
        $remote->expects($this->at(6))->method('getConnection')->will($this->returnValue($conn));

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) {
            $this->assertStringContainsStringIgnoringCase('Remote Journal UpSync Complete', $msg);
            $this->assertArrayHasKey('P', $args);
            $this->assertSame('update', $args['P']);
            $this->assertArrayHasKey('I', $args);
            $this->assertSame(101, $args['I']);
            return true;
        }));

        $this->__setupMocks($remote, $logger, $em);

        $result = $this->sourceService->upsync($entity);
        $this->assertTrue($result);

        $this->assertSame(101, $entity->getId());
        $this->assertSame('123456789012345678901234567890AB', $entity->getUnid());
        $this->assertSame('DC7A21D1-28E1-4D08-BBDC-8D0610E4AA88', $entity->getDetailsPreviousRowGuid());
    }

    /**
     * @testdox Checking upsync() update with on un-even failure
     */
    public function testUpSyncUpdateDataUnEvenFailure()
    {
        $entity = $this->_loadEntityId();
        $entity = $this->_loadEntityData($entity);

        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableOne"', [101, 1], 1);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_createJournalUnid(false);
        $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_copyRemoteIds(['UNID' => '', 'rowguid' => '']);
        $remote->expects($this->at(3))->method('getConnection')->will($this->returnValue($conn));

        $this->__setupMocks($remote);

        try {
            $this->sourceService->upsync($entity);
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\Exception::class, $error);
            $this->assertStringContainsStringIgnoringCase('Journal has no UNID', $error->getMessage());
        }
    }

    /**
     * @testdox Checking upsync() create with ID failures
     */
    public function testUpSyncCreateEmptyIdFailures()
    {
        $entity = $this->_loadEntity();

        $this->__setupMocks();

        try {
            $this->sourceService->upsync($entity);
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\Exception::class, $error);
            $this->assertStringContainsStringIgnoringCase('Missing Journal ID', $error->getMessage());
        }
    }

    /**
     * @testdox Checking upsync() create with UNID failures
     */
    public function testUpSyncCreateEmptyUnidFailures()
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableOne"', [101, 1], 0);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_createNewJournalOnTable('"dbJ"."tableOne"', [101, 1], true);
        $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_copyRemoteIds(null);
        $remote->expects($this->at(3))->method('getConnection')->will($this->returnValue($conn));

        $this->__setupMocks($remote);

        $entity = $this->_loadEntityId();

        try {
            $this->sourceService->upsync($entity);
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\App\Exception\RemoteSyncException::class, $error);
            $this->assertStringContainsStringIgnoringCase('Remote Database Up-Sync failed at PHP level', $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('Your request has been canceled', $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('Unable to get Journal UNID', $error->getMessage());
        }

        $this->assertSame(101, $entity->getId());
        $this->assertSame(null, $entity->getUnid());
        $this->assertSame('', $entity->getDetailsPreviousRowGuid());
    }

    /**
     * @dataProvider getUpSyncUpdateEmptyUpdateFailuresProvider
     *
     * @testdox Checking upsync() update with Update failures
     */
    public function testUpSyncUpdateEmptyUpdateFailures($entity, $primary, $secondary)
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableOne"', [101, 1], 1);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_checkIdExitsOnTable('"dbJ"."tableTwo"', ['123456789012345678901234567890AB', 2], 1);
        $remote->expects($this->at(2))->method('getConnection')->will($this->returnValue($conn));

        $conn = $this->_updateJournal(true, $primary, $secondary);
        $remote->expects($this->at(3))->method('getConnection')->will($this->returnValue($conn));

        $em = null;
        if (null !== $secondary) {
            // _updateJournal > _getMappingForSecondaryTable
            $em = $this->_getMappingForSecondaryTable(0, null, true, true);
        }

        $this->__setupMocks($remote, null, $em);

        try {
            $this->sourceService->upsync($entity);
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\App\Exception\RemoteSyncException::class, $error);
            $this->assertStringContainsStringIgnoringCase('Remote Database Up-Sync failed at PHP level', $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('Your request has been canceled', $error->getMessage());
            if (false === $primary) {
                $this->assertStringContainsStringIgnoringCase('Unable to update Primary Table', $error->getMessage());
            }
            if (false === $secondary) {
                $this->assertStringContainsStringIgnoringCase('Unable to update Secondary Table', $error->getMessage());
            }

            $this->assertSame(101, $entity->getId());
            $this->assertSame('123456789012345678901234567890AB', $entity->getUnid());
        }
    }

    /**
     * @testdox Checking remove() with no issue
     */
    public function testRemoveOk()
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $conn = $this->_remove(true, true, true, 2);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('info')->will($this->returnCallback(function($msg, $args) {
            $this->assertStringContainsStringIgnoringCase('Remote Journal Removal Complete', $msg);
            $this->assertArrayHasKey('P', $args);
            $this->assertSame('delete', $args['P']);
            $this->assertArrayHasKey('primary', $args);
            $this->assertSame(true, $args['primary']);
            $this->assertArrayHasKey('secondary', $args);
            $this->assertSame(true, $args['secondary']);
            $this->assertArrayHasKey('specialties', $args);
            $this->assertSame(true, $args['specialties']);
            $this->assertArrayHasKey('specialties-count', $args);
            $this->assertSame(2, $args['specialties-count']);
            $this->assertArrayHasKey('I', $args);
            $this->assertSame(101, $args['I']);
            $this->assertArrayHasKey('V', $args);
            $this->assertSame('123456789012345678901234567890AB', $args['V']);
            return true;
        }));

        $this->__setupMocks($remote, $logger);

        $entity = $this->_loadEntityId();
        $entity = $this->_loadEntityUnid($entity);

        $result = $this->sourceService->remove($entity);
        $this->assertTrue($result);
    }

    /**
     * @testdox Checking remove() with IDs Failure
     */
    public function testRemoveIdFailure()
    {
        $this->__setupMocks();

        $entity = $this->_loadEntityId();

        try {
            $this->sourceService->remove($entity);
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\App\Exception\RemoteSyncException::class, $error);
            $this->assertStringContainsStringIgnoringCase('Remote Database Up-Sync failed at PHP level', $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('Your request has been canceled', $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('Journal has no record of Previous Site ID', $error->getMessage());
        }
    }

    /**
     * @testdox Checking remove() with PDO Failure
     */
    public function testRemovePdoFailure()
    {
        $remote = $this->createMock(MsSqlRemoteConnection::class);
        $remote->expects($this->at(0))->method('makeConnection')->will($this->returnValue(true));

        $exception = new \PDOException('SQL Error DELETE ...');
        $conn = $this->_remove(null, $exception, true, 2);
        $remote->expects($this->at(1))->method('getConnection')->will($this->returnValue($conn));

        $this->__setupMocks($remote);

        $entity = $this->_loadEntityId();
        $entity = $this->_loadEntityUnid($entity);

        try {
            $this->sourceService->remove($entity);
        }
        catch (\Exception $error) {
            $this->assertInstanceOf(\App\Exception\RemoteSyncException::class, $error);
            $this->assertStringContainsStringIgnoringCase('Remote Database Up-Sync failed at SQL level', $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('Your request has been canceled', $error->getMessage());
            $this->assertStringContainsStringIgnoringCase('SQL Error DELETE', $error->getMessage());
        }
    }

    public function getUpSyncUpdateEmptyUpdateFailuresProvider()
    {
        $entity = $this->_loadEntityId();
        $entity = $this->_loadEntityUnid($entity);
        $entity = $this->_loadEntityData($entity);

        return [
            [$entity, false, null],
            [$entity, true, false],
        ];
    }

    private function _checkIdExitsOnTable($tableName, $binder, $result)
    {
        $conn = $this->createMock(\PDO::class);

        $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($tableName, $binder, $result) {
            $this->assertSame(
                "SELECT COUNT(*) AS count FROM {$tableName} WHERE JournalID = :JournalID;",
                $sql
            );
            $stmt = $this->createMock(\PDOStatement::class);
            $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use ($binder) {
                $this->assertSame(':JournalID', $token);
                $this->assertSame($binder[0], $value);
                $this->assertSame($binder[1], $type);
                return true;
            }));
            $stmt->expects($this->at(1))->method('execute')->will($this->returnValue(true));
            $stmt->expects($this->at(2))->method('fetchColumn')->will($this->returnValue($result));
            return $stmt;
        }));

        $conn->expects($this->at(1))->method('errorInfo')->will($this->returnValue(['_checkIdExitsOnTable', $tableName, $binder, $result]));

        return $conn;
    }

    private function _createJournalUnid($result)
    {
        $conn = $this->createMock(\PDO::class);

        $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($result) {
            $this->assertSame(
                "UPDATE \"dbJ\".\"tableOne\" SET (UNID = CONVERT(VARCHAR(32), REPLACE(NEWID(),'-',''))) WHERE JournalID = :JournalID;",
                $sql
            );
            $stmt = $this->createMock(\PDOStatement::class);
            $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                $this->assertSame(':JournalID', $token);
                $this->assertSame(101, $value);
                $this->assertSame(1, $type);
                return true;
            }));
            $stmt->expects($this->at(1))->method('execute')->will($this->returnValue($result));
            return $stmt;
        }));

        $conn->expects($this->at(1))->method('errorInfo')->will($this->returnValue(['_createJournalUnid', $result]));

        return $conn;
    }

    private function _createNewJournalOnTable($tableName, $binder, $result)
    {
        $conn = $this->createMock(\PDO::class);

        if ('"dbJ"."tableOne"' == $tableName) {
            $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($tableName) {
                $this->assertSame(
                    "SET IDENTITY_INSERT {$tableName} ON;",
                    $sql
                );
                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnValue(true));
                return $stmt;
            }));

            $conn->expects($this->at(1))->method('prepare')->will($this->returnCallback(function($sql) use ($tableName, $binder, $result) {
                $this->assertSame(
                    "INSERT INTO {$tableName} (JournalID, Name, UNID, rowguid) VALUES (:JournalID, '', CONVERT(VARCHAR(32), REPLACE(NEWID(),'-','')), NEWID());",
                    $sql
                );

                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use ($binder) {
                    $this->assertSame(':JournalID', $token);
                    $this->assertSame($binder[0], $value);
                    $this->assertSame($binder[1], $type);
                    return true;
                }));
                $stmt->expects($this->at(1))->method('execute')->will($this->returnValue($result));
                return $stmt;
            }));
            $conn->expects($this->at(2))->method('errorInfo')->will($this->returnValue(['_createNewJournalOnTable', $tableName, $binder, $result]));

            $conn->expects($this->at(3))->method('prepare')->will($this->returnCallback(function($sql) use ($tableName) {
                $this->assertSame(
                    "SET IDENTITY_INSERT {$tableName} OFF;",
                    $sql
                );
                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnValue(true));
                return $stmt;
            }));
        }
        else {
            $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($tableName, $binder, $result) {
                $this->assertSame(
                    "INSERT INTO {$tableName} (JournalID) VALUES (:JournalID);",
                    $sql
                );

                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use ($binder) {
                    $this->assertSame(':JournalID', $token);
                    $this->assertSame($binder[0], $value);
                    $this->assertSame($binder[1], $type);
                    return true;
                }));
                $stmt->expects($this->at(1))->method('execute')->will($this->returnValue($result));
                return $stmt;
            }));

            $conn->expects($this->at(1))->method('errorInfo')->will($this->returnValue(['_createNewJournalOnTable', $tableName, $binder, $result]));
        }

        return $conn;
    }

    private function _copyRemoteIds($result)
    {
        $conn = $this->createMock(\PDO::class);

        $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($result) {
            $this->assertSame(
                "SELECT UNID, CAST(rowguid AS VARCHAR(36)) AS rowguid FROM \"dbJ\".\"tableOne\" WHERE JournalID = :JournalID;",
                $sql
            );
            $stmt = $this->createMock(\PDOStatement::class);
            $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                $this->assertSame(':JournalID', $token);
                $this->assertSame(101, $value);
                $this->assertSame(1, $type);
                return true;
            }));
            $stmt->expects($this->at(1))->method('execute')->will($this->returnValue(true));
            $stmt->expects($this->at(2))->method('fetch')->will($this->returnValue($result));
            return $stmt;
        }));

        $conn->expects($this->at(1))->method('errorInfo')->will($this->returnValue(['_copyRemoteIds', $result]));

        return $conn;
    }

    private function _updateJournal($withData, $resultPrimary, $resultSecondary = null)
    {
        if (!$withData) {
            $primaryFields = [
                'Name'         => [null, 0],
                'Abbreviation' => [null, 0],
                'URL'          => [null, 0],
                'Status'       => [null, 0],
                'BeginDate'    => [null, 0],
                'LastUpdate'   => [null, 0],
                'Deleted'      => [0, 1],
                'JR1'          => [0.0, 2],
                'JR2'          => [0.0, 2],
                'ISSN'         => [null, 0],
                'DGAbstract'   => [0, 1],
                'SecondLine'   => [0, 1],
            ];

            $secondaryFields = [
                'JournalVolume'        => [null, 0],
                'JournalNumber'        => [null, 0],
                'LastViewed'           => [null, 0],
                'Notes'                => [null, 0],
                'Frequency'            => [null, 0],
                'UserName'             => [null, 0],
                'Password'             => [null, 0],
                'Publisher'            => [null, 0],
                'regurl'               => [null, 0],
                'Language'             => [null, 0],
                'Country'              => [null, 0],
                'Region'               => [null, 0],
                'Editions'             => [null, 0],
                'Copyright'            => [null, 0],
                'NotBeingUpdated'      => "CONVERT(BINARY, 'N')",
                'DrugsMonitored'       => "CONVERT(BINARY, 'N')",
                'GlobalEditionJournal' => "CONVERT(BINARY, 'N')",
                'abstract'             => "CONVERT(BINARY, 'N')",
                'regabstract'          => "CONVERT(BINARY, 'N')",
                'fulltxt'              => "CONVERT(BINARY, 'N')",
                'regfulltxt'           => "CONVERT(BINARY, 'N')",
                'feefulltxt'           => "CONVERT(BINARY, 'N')",
                'comments'             => [null, 0],
                'DocAuthor'            => [null, 0],
                'cleared_by'           => [null, 0],
                'cleared_on'           => [null, 0],
                'Copied'               => ['No', 2],
                'Modified'             => [null, 0],
                'JournalHTMLCode'      => [null, 0],
                'CGIValue'             => [null, 0],
                'PrinterName'          => [null, 0],
            ];
        }
        else {
            $primaryFields = [
                'Name'         => ['Histology Today', 2],
                'Abbreviation' => ['Hist2day', 2],
                'URL'          => ['http://Hist2day.register.com/register?mmbrid=345234523', 2],
                'Status'       => ['Archive', 2],
                'BeginDate'    => ['2016-06-06 06:06:06', 2],
                'LastUpdate'   => [null, 0],
                'Deleted'      => [0, 1],
                'JR1'          => [166.67, 2],
                'JR2'          => [0.0, 2],
                'ISSN'         => ['234A-3463', 2],
                'DGAbstract'   => [1, 1],
                'SecondLine'   => [1, 1],
            ];

            $secondaryFields = [
                'JournalVolume'        => ['Vol 2018', 2],
                'JournalNumber'        => ['2 JA2X', 2],
                'LastViewed'           => ['2016-08-08 08:08:08; Don\'t need to update this', 2],
                'Notes'                => [null, 0],
                'Frequency'            => ['3-4 times a week, trice a year', 2],
                'UserName'             => ['lovelyDay', 2],
                'Password'             => ['Bi||With3rs', 2],
                'Publisher'            => ['APU - Africa Printer Union', 2],
                'regurl'               => ['http://chitan.com/west-register', 2],
                'Language'             => ['Cantonese;Mandarin;Halsey', 2],
                'Country'              => ['Micro Gangrenous', 2],
                'Region'               => ['Fabus Indi', 2],
                'Editions'             => ['RE #36', 2],
                'Copyright'            => ['Copyright by Top Alliance Papers', 2],
                'NotBeingUpdated'      => "CONVERT(BINARY, 'N')",
                'DrugsMonitored'       => "CONVERT(BINARY, 'Y')",
                'GlobalEditionJournal' => "CONVERT(BINARY, 'Y')",
                'abstract'             => "CONVERT(BINARY, 'Y')",
                'regabstract'          => "CONVERT(BINARY, 'N')",
                'fulltxt'              => "CONVERT(BINARY, 'Y')",
                'regfulltxt'           => "CONVERT(BINARY, 'Y')",
                'feefulltxt'           => "CONVERT(BINARY, 'N')",
                'comments'             => ['Yes this is a another test' . PHP_EOL . 'still', 2],
                'DocAuthor'            => ['Kim.Joung/WEB3', 2],
                'cleared_by'           => ['Lisa.Hal/SA', 2],
                'cleared_on'           => ['26 Mar 2015 00:00:02', 2],
                'Copied'               => ['Yes', 2],
                'Modified'             => ['Add membership token on register link', 2],
                'JournalHTMLCode'      => ['http://google?q=%tsearch%&pa=%member_id%#!show-diagram', 2],
                'CGIValue'             => ['58000', 2],
                'PrinterName'          => ['Africa Printer Union', 2],
            ];
        }

        $conn = $this->createMock(\PDO::class);

        $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($primaryFields, $resultPrimary) {
            $fieldsStr = [];
            foreach (array_keys($primaryFields) as $fi => $field) {
                if (is_array($primaryFields[$field])) {
                    $fieldsStr[] = "{$field}=:{$field}";
                }
                else {
                    $fieldsStr[] = "{$field}={$primaryFields[$field]}";
                }
            }
            $fieldsStr = implode(', ', $fieldsStr);

            $this->assertSame("UPDATE \"dbJ\".\"tableOne\" SET {$fieldsStr} WHERE JournalID = :JournalID;", $sql);

            $stmt = $this->createMock(\PDOStatement::class);

            $invokedAt = 0;
            foreach (array_keys($primaryFields) as $fi => $field) {
                if (is_array($primaryFields[$field])) {
                    $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use ($field, $primaryFields) {
                        $this->assertSame(":{$field}", $token);
                        $this->assertSame($primaryFields[$field][0], $value);
                        $this->assertSame($primaryFields[$field][1], $type);
                        return true;
                    }));
                    $invokedAt++;
                }
            }

            $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                $this->assertSame(':JournalID', $token);
                $this->assertSame(101, $value);
                $this->assertSame(1, $type);
                return true;
            }));
            $invokedAt++;

            $stmt->expects($this->at($invokedAt))->method('execute')->will($this->returnValue($resultPrimary));
            $invokedAt++;
            return $stmt;
        }));

        $conn->expects($this->at(1))->method('errorInfo')->will($this->returnValue(['_updateJournal - primary', $withData, $resultPrimary]));

        if ($resultPrimary) {
            $conn->expects($this->at(2))->method('prepare')->will($this->returnCallback(function($sql) use ($secondaryFields, $resultSecondary) {
                $fieldsStr = [];
                foreach (array_keys($secondaryFields) as $fi => $field) {
                    if (is_array($secondaryFields[$field])) {
                        $fieldsStr[] = "{$field}=:{$field}";
                    }
                    else {
                        $fieldsStr[] = "{$field}={$secondaryFields[$field]}";
                    }
                }
                $fieldsStr = implode(', ', $fieldsStr);

                $this->assertSame("UPDATE \"dbJ\".\"tableTwo\" SET {$fieldsStr} WHERE JournalID = :JournalUNID;", $sql);

                $stmt = $this->createMock(\PDOStatement::class);

                $invokedAt = 0;
                foreach (array_keys($secondaryFields) as $fi => $field) {
                    if (is_array($secondaryFields[$field])) {
                        $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use ($field, $secondaryFields) {
                            $this->assertSame(":{$field}", $token);
                            $this->assertSame($secondaryFields[$field][0], $value);
                            $this->assertSame($secondaryFields[$field][1], $type);
                            return true;
                        }));
                        $invokedAt++;
                    }
                }

                $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                    $this->assertSame(':JournalUNID', $token);
                    $this->assertSame('123456789012345678901234567890AB', $value);
                    $this->assertSame(2, $type);
                    return true;
                }));
                $invokedAt++;

                $stmt->expects($this->at($invokedAt))->method('execute')->will($this->returnValue($resultSecondary));
                $invokedAt++;
                return $stmt;
            }));

            $conn->expects($this->at(3))->method('errorInfo')->will($this->returnValue(['_updateJournal - secondary', $withData, $resultSecondary]));
        }

        return $conn;
    }

    private function _updateJournalSpecialties($deleteResult, $haveSpecialty, $insertResult = null)
    {
        $conn = $this->createMock(\PDO::class);

        $conn->expects($this->at(0))->method('prepare')->will($this->returnCallback(function($sql) use ($deleteResult) {
            $this->assertSame(
                "DELETE FROM \"dbJ\".\"tableThree\" WHERE JournalID = :JournalID;",
                $sql
            );
            $stmt = $this->createMock(\PDOStatement::class);
            $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                $this->assertSame(':JournalID', $token);
                $this->assertSame(101, $value);
                $this->assertSame(1, $type);
                return true;
            }));
            $stmt->expects($this->at(1))->method('execute')->will($this->returnValue($deleteResult));
            return $stmt;
        }));

        $conn->expects($this->at(1))->method('errorInfo')->will($this->returnValue(['_updateJournalSpecialties - delete', $deleteResult]));

        if ($haveSpecialty) {
            $conn->expects($this->at(2))->method('prepare')->will($this->returnCallback(function($sql) use ($insertResult) {
                $tokens = [
                    'SELECT :JournalID40,:Specialty40',
                    'SELECT :JournalID41,:Specialty41',
                ];
                $tokens = implode(' UNION ALL ', $tokens);
                $this->assertSame(
                    "INSERT INTO \"dbJ\".\"tableThree\" (JournalID,Specialty) {$tokens};",
                    $sql
                );
                $stmt = $this->createMock(\PDOStatement::class);

                $specialties = [
                    '40' => 'Oncology',
                    '41' => 'Gideon',
                ];

                $invokedAt = 0;
                foreach ($specialties as $key => $specilty) {
                    $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use ($key) {
                        $this->assertSame(":JournalID{$key}", $token);
                        $this->assertSame(101, $value);
                        $this->assertSame(1, $type);
                        return true;
                    }));
                    $invokedAt++;

                    $stmt->expects($this->at($invokedAt))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) use ($key, $specilty) {
                        $this->assertSame(":Specialty{$key}", $token);
                        $this->assertSame($specilty, $value);
                        $this->assertSame(2, $type);
                        return true;
                    }));
                    $invokedAt++;
                }
                $stmt->expects($this->at($invokedAt))->method('execute')->will($this->returnValue($insertResult));
                return $stmt;
            }));

            $conn->expects($this->at(3))->method('errorInfo')->will($this->returnValue(['_updateJournalSpecialties - insert', $insertResult]));
        }

        return $conn;
    }

    private function _getMappingForSecondaryTable($invokedAt = 0, $em = null, $countryResult = false, $regionResult = false)
    {
        if (null === $em) {
            $em = $this->createMock(EntityManagerInterface::class);
        }

        if (false !== $countryResult) {
            $repo = $this->createMock(\App\Repository\CountryRepository::class);
            if (null === $countryResult) {
                $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue($countryResult));
            }
            else {
                $repo->expects($this->at(0))->method('findOneBy')->will($this->returnCallback(function() {
                    $country = new \App\Entity\Country;
                    $reflection = new \ReflectionClass(\App\Entity\Country::class);
                    $reflProp = $reflection->getProperty('isoCode');
                    $reflProp->setAccessible(true);
                    $reflProp->setValue($country, 'MG');
                    $country->setName('Micro Gangrenous');
                    $country->setCapitalCity('Central Micro Gangrenous');
                    return $country;
                }));
            }
            $em->expects($this->at($invokedAt))->method('getRepository')->will($this->returnValue($repo));
            $invokedAt++;
        }

        if (false !== $regionResult) {
            $repo = $this->createMock(\App\Repository\CountryTaxonomyRepository::class);
            if (null === $regionResult) {
                $repo->expects($this->at(0))->method('findOneBy')->will($this->returnValue($regionResult));
            }
            else {
                $repo->expects($this->at(0))->method('findOneBy')->will($this->returnCallback(function() {
                    $countryTax = new \App\Entity\CountryTaxonomy;
                    $reflection = new \ReflectionClass(\App\Entity\CountryTaxonomy::class);
                    $reflProp = $reflection->getProperty('id');
                    $reflProp->setAccessible(true);
                    $reflProp->setValue($countryTax, 59);
                    $reflProp = $reflection->getProperty('name');
                    $reflProp->setAccessible(true);
                    $reflProp->setValue($countryTax, 'Fabus Indi');
                    return $countryTax;
                }));
            }
            $em->expects($this->at($invokedAt))->method('getRepository')->will($this->returnValue($repo));
            $invokedAt++;
        }

        return $em;
    }

    private function _remove($tableOneResult, $tableTwoResult, $tableThreeResult, $tableThreeCountResult)
    {
        $conn = $this->createMock(\PDO::class);

        $invokedAt = 0;

        if (null !== $tableThreeResult) {
            $conn->expects($this->at($invokedAt))->method('prepare')->will($this->returnCallback(function($sql) use ($tableThreeResult, $tableThreeCountResult) {
                $this->assertSame(
                    "DELETE FROM \"dbJ\".\"tableThree\" WHERE JournalID = :JournalID;",
                    $sql
                );
                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                    $this->assertSame(':JournalID', $token);
                    $this->assertSame(101, $value);
                    $this->assertSame(1, $type);
                    return true;
                }));
                $func = 'returnValue';
                if ($tableThreeResult instanceof \Exception) {
                    $func = 'throwException';
                }
                $stmt->expects($this->at(1))->method('execute')->will($this->$func($tableThreeResult));
                if (null !== $tableThreeCountResult) {
                    $func = 'returnValue';
                    if ($tableThreeCountResult instanceof \Exception) {
                        $func = 'throwException';
                    }
                    $stmt->expects($this->at(2))->method('rowCount')->will($this->$func($tableThreeCountResult));
                }
                return $stmt;
            }));
            $invokedAt++;

            if (
                (!($tableThreeResult instanceof \Exception)) &&
                (!($tableThreeCountResult instanceof \Exception))
            ) {
                $conn->expects($this->at($invokedAt))->method('errorInfo')->will($this->returnValue(['_remove - tableThree', $tableThreeResult, $tableThreeCountResult]));
                $invokedAt++;
            }
        }

        if (null !== $tableTwoResult) {
            $conn->expects($this->at($invokedAt))->method('prepare')->will($this->returnCallback(function($sql) use ($tableTwoResult) {
                $this->assertSame(
                    "DELETE FROM \"dbJ\".\"tableTwo\" WHERE JournalID = :JournalID;",
                    $sql
                );
                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                    $this->assertSame(':JournalID', $token);
                    $this->assertSame('123456789012345678901234567890AB', $value);
                    $this->assertSame(2, $type);
                    return true;
                }));
                $func = 'returnValue';
                if ($tableTwoResult instanceof \Exception) {
                    $func = 'throwException';
                }
                $stmt->expects($this->at(1))->method('execute')->will($this->$func($tableTwoResult));
                return $stmt;
            }));
            $invokedAt++;

            if (!($tableTwoResult instanceof \Exception)) {
                $conn->expects($this->at($invokedAt))->method('errorInfo')->will($this->returnValue(['_remove - tableTwo', $tableTwoResult]));
                $invokedAt++;
            }
        }

        if (null !== $tableOneResult) {
            $conn->expects($this->at($invokedAt))->method('prepare')->will($this->returnCallback(function($sql) use ($tableOneResult) {
                $this->assertSame(
                    "DELETE FROM \"dbJ\".\"tableOne\" WHERE JournalID = :JournalID;",
                    $sql
                );
                $stmt = $this->createMock(\PDOStatement::class);
                $stmt->expects($this->at(0))->method('bindValue')->will($this->returnCallback(function($token, $value, $type) {
                    $this->assertSame(':JournalID', $token);
                    $this->assertSame(101, $value);
                    $this->assertSame(1, $type);
                    return true;
                }));
                $func = 'returnValue';
                if ($tableOneResult instanceof \Exception) {
                    $func = 'throwException';
                }
                $stmt->expects($this->at(1))->method('execute')->will($this->$func($tableOneResult));
                return $stmt;
            }));
            $invokedAt++;

            if (!($tableOneResult instanceof \Exception)) {
                $conn->expects($this->at($invokedAt))->method('errorInfo')->will($this->returnValue(['_remove - tableOne', $tableOneResult]));
                $invokedAt++;
            }
        }

        return $conn;
    }

    private function __setupMocks($remote = null, $logger = null, $em = null, $upsync = true)
    {
        $reflection = new \ReflectionClass(JournalSourceService::class);

        if (null === $remote) {
            $remote = $this->createMock(MsSqlRemoteConnection::class);
        }
        $reflProp = $reflection->getProperty('remote');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, $remote);

        if (null === $logger) {
            $logger = $this->createMock(LoggerInterface::class);
        }
        $reflProp = $reflection->getProperty('logger');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, $logger);

        if (null === $em) {
            $em = $this->createMock(EntityManagerInterface::class);
        }
        $reflProp = $reflection->getProperty('em');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, $em);

        $reflProp = $reflection->getProperty('upSync');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, $upsync);

        $reflProp = $reflection->getProperty('dbSchema');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, 'dbJ');

        $reflProp = $reflection->getProperty('tblPrimary');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, 'tableOne');

        $reflProp = $reflection->getProperty('tblSecondary');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, 'tableTwo');

        $reflProp = $reflection->getProperty('tblSpecialties');
        $reflProp->setAccessible(true);
        $reflProp->setValue($this->sourceService, 'tableThree');
    }

    private function _loadEntity()
    {

        return new \App\Entity\Journal;
    }

    private function _loadEntityId($entity = null)
    {
        $reflection = new \ReflectionClass(\App\Entity\Journal::class);

        if (null === $entity) {
            $entity = $this->_loadEntity();
        }

        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($entity, 101);

        return $entity;
    }

    private function _loadEntityUnid($entity = null)
    {
        $reflection = new \ReflectionClass(\App\Entity\Journal::class);

        if (null === $entity) {
            $entity = $this->_loadEntity();
        }

        $reflProp = $reflection->getProperty('unid');
        $reflProp->setAccessible(true);
        $reflProp->setValue($entity, '123456789012345678901234567890AB');

        return $entity;
    }

    private function _loadEntityData($entity = null)
    {
        if (null === $entity) {
            $entity = $this->_loadEntity();
        }

        $specialtyTax1 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax1, 40);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax1, 'Oncology');

        $specialtyTax2 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax2, 41);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax2, 'Gideon');

        $specialtyTax3 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax3, 42);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax3, 'Silver');

        $specialty1 = new \App\Entity\JournalSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\JournalSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, 5);
        $reflProp = $reflection->getProperty('journal');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, $entity);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, $specialtyTax1);

        $specialty2 = new \App\Entity\JournalSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\JournalSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, 65);
        $reflProp = $reflection->getProperty('journal');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, $entity);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, $specialtyTax2);

        $specialty3 = new \App\Entity\JournalSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\JournalSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty3, 1);

        $entity
            ->setName('Histology Today')
            ->setAbbreviation('Hist2day')
            ->setVolume('Vol 2018')
            ->setNumber('2 JA2X')
            ->setStatus(5)
            ->setExported(1)
            ->setIsMedline(1)
            ->setIsSecondLine(2)
            ->setIsDgAbstract(1)
            ->setNotBeingUpdated(0)
            ->setDrugsMonitored(1)
            ->setGlobalEditionJournal(1)
            ->setDeleted(0)
            ->setManualCreation(1)
            ->setMedlineIssn('234A-3463')
            ->setJournalReport1(166.6693636566912102)
            ->setJournalReport2(0)
            ->addSpecialty($specialty1)
            ->addSpecialty($specialty2)
            ->addSpecialty($specialty3)
            ->removeSpecialty($specialty3)
            ->setDetailsLanguage('Cantonese;Mandarin;Halsey')
            ->setDetailsCountry('MG')
            ->setDetailsRegion(59)
            ->setDetailsEdition('RE #36')
            ->setDetailsCopyright('Copyright by Top Alliance Papers')
            ->setDetailsAutoPublishingSource('Yes this correct')
            ->setDetailsPreviousRowGuid('DC7A21D1-28E1-4D08-BBDC-8D0610E4AA88')
            ->setDetailsAbstractAbstract(true)
            ->setDetailsAbstractRegForAbstract(false)
            ->setDetailsAbstractFullTextAvailablity(true)
            ->setDetailsAbstractRegForFullText(true)
            ->setDetailsAbstractFeeForFullText(false)
            ->setDetailsAbstractComments("Yes this is a another test\r\nstill")
            ->setDetailsOrderingHtmlCode('http://google?q=%tsearch%&pa=%member_id%#!show-diagram')
            ->setDetailsOrderingCgiValue(58000)
            ->setDetailsOrderingPrinterName('Africa Printer Union')
            ->setDetailsPublisherName('APU - Africa Printer Union')
            ->setDetailsPublisherRegUrl('http://chitan.com/west-register')
            ->setDetailsUrlUrl('http://Hist2day.register.com/register?mmbrid=345234523')
            ->setDetailsUrlUpdatedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2016-06-06 06:06:06'))
            ->setDetailsUrlViewedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2016-08-08 08:08:08'))
            ->setDetailsUrlViewedOnComment('Don\'t need to update this')
            ->setDetailsUrlToUpdate(null)
            ->setDetailsUrlNote('')
            ->setDetailsUrlFrequency('3-4 times a week, trice a year')
            ->setDetailsUrlUsername('lovelyDay')
            ->setDetailsUrlPassword('Bi||With3rs')
            ->setDetailsValidationAuthor('Kim.Joung/WEB3')
            ->setDetailsValidationDateComposed(\DateTime::createFromFormat('Y-m-d H:i:s', '2015-03-26 00:00:01'))
            ->setDetailsValidationClearedBy('Lisa.Hal/SA')
            ->setDetailsValidationClearedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2015-03-26 00:00:02'))
            ->setDetailsValidationModified('Add membership token on register link')
            ->setDetailsMetaCreatedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2015-03-26 00:00:05'))
            ->setDetailsMetaCreatedBy('Halsey.Duo')
            ->setDetailsMetaModifiedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2017-05-26 13:25:05'))
            ->setDetailsMetaModifiedBy('Halsey.Trio')
        ;

        return $entity;
    }
}
