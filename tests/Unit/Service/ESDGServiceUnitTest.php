<?php

use App\Service\ESDGService;
use App\Utility\ESQueryBuilder;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Mockery as M;

/**
 * @testdox UNIT | Service | ESDGService
 */
class ESDGServiceUnitTest extends TestCase
{
    private $ESDGService;
    private $loggerMock;
    private $clientMock;
    private $clientBuilderMock;
    private $ESQB;

    public function setUp(): void
    {
        $this->loggerMock = M::mock(LoggerInterface::class);
        $this->clientMock = M::mock(Client::class);
        $this->clientBuilderMock = M::mock(ClientBuilder::class);
        $this->ESQB = new ESQueryBuilder('fb_articles');

        $this->ESDGService = new ESDGService($this->loggerMock, $this->clientMock, 'fb_articles', 1000);
    }

    public function testBuildPapersOnlyQuery()
    {
        $expectedQuery = json_decode('{"scroll":"1m","size":100,"index":"fb_articles","body":{"_source":["title","updated","status"],"query":{"bool":{"should":{"match":{"type":"DGAbstract"}}}},"sort":{"updated":{"order":"desc"}}},"client":{"curl":{"10036":"GET"}}}', 1);
        $query = $this->ESQB->buildPapersOnlyQuery();
        $this->assertSame($expectedQuery, $query);
    }

    public function testParsePapersOnly()
    {
        $resultMock = '{"took":11,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":362515,"max_score":null,"hits":[{"_index":"fb_articles","_type":"_doc","_id":"4546919","_score":null,"_source":{"updated":"1578956401","title":"Surgical management of primary thyroid tumours","status":"1"},"sort":[1578956401]},{"_index":"fb_articles","_type":"_doc","_id":"4548711","_score":null,"_source":{"updated":"1578956401","title":"2017 Versus 2012 Appropriate Use Criteria for Percutaneous Coronary Interventions: Impact on Appropriateness Ratings","status":"1"},"sort":[1578956401]}]}}';
        $expexted = [];
        $article['title'] = 'Surgical management of primary thyroid tumours';
        $article['status'] = '1';
        $article['updated'] = '2020-01-13 23:00:01';
        $expexted[] = $article;
        $article['title'] = '2017 Versus 2012 Appropriate Use Criteria for Percutaneous Coronary Interventions: Impact on Appropriateness Ratings';
        $article['status'] = '1';
        $article['updated'] = '2020-01-13 23:00:01';
        $expexted[] = $article;
        $result = $this->ESDGService->parsePapersOnly(json_decode($resultMock, 1));
        $this->assertSame($expexted, $result);
    }

    public function testGetPapersOnly()
    {
        $resultMock = '{"took":11,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":362515,"max_score":null,"hits":[{"_index":"fb_articles","_type":"_doc","_id":"4546919","_score":null,"_source":{"updated":"1578956401","title":"Surgical management of primary thyroid tumours","status":"1"},"sort":[1578956401]},{"_index":"fb_articles","_type":"_doc","_id":"4548711","_score":null,"_source":{"updated":"1578956401","title":"2017 Versus 2012 Appropriate Use Criteria for Percutaneous Coronary Interventions: Impact on Appropriateness Ratings","status":"1"},"sort":[1578956401]}]}}';
        $expexted = [];
        $article['title'] = 'Surgical management of primary thyroid tumours';
        $article['status'] = '1';
        $article['updated'] = '2020-01-13 23:00:01';
        $expexted[] = $article;
        $article['title'] = '2017 Versus 2012 Appropriate Use Criteria for Percutaneous Coronary Interventions: Impact on Appropriateness Ratings';
        $article['status'] = '1';
        $article['updated'] = '2020-01-13 23:00:01';
        $expexted[] = $article;
        $this->clientMock->expects('search')->andReturn(json_decode($resultMock, 1));
        $result = $this->ESDGService->getPapersOnly();
        $this->assertSame($expexted, $result);
    }

    public function testBuildTimeStampQuery()
    {
        $expectedQuery = json_decode('{"scroll":"1m","size":100,"index":"fb_articles","body":{"_source":["title","updated","type"],"query":{"bool":{"must":[{"term":{"status":1}},{"terms":{"type":["DGNews","DGDispatch"]}}]}},"sort":{"updated":{"order":"desc"}}},"client":{"curl":{"10036":"GET"}}}', 1);
        $query = $this->ESQB->buildTimeStampQuery();
        $this->assertSame($expectedQuery, $query);
    }

    public function testParseTimeStamp()
    {
        $resultMock = '{"took":145,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":14120,"max_score":null,"hits":[{"_index":"fb_articles","_type":"_doc","_id":"4548671","_score":null,"_source":{"updated":"1579042801","title":"Alirocumab Reduces Rates of Major CV Events in Patients With Persistently High LDL Cholesterol","type":"DGDispatch"},"sort":[1579042801]},{"_index":"fb_articles","_type":"_doc","_id":"4701608","_score":null,"_source":{"updated":"1579042801","title":"NL test 8.1.2020","type":"DGDispatch"},"sort":[1579042801]}]}}';
        $expexted = [];
        $article['title'] = 'Alirocumab Reduces Rates of Major CV Events in Patients With Persistently High LDL Cholesterol';
        $article['type'] = 'DGDispatch';
        $article['updated'] = '2020-01-14 23:00:01';
        $expexted[] = $article;
        $article['title'] = 'NL test 8.1.2020';
        $article['type'] = 'DGDispatch';
        $article['updated'] = '2020-01-14 23:00:01';
        $expexted[] = $article;
        $result = $this->ESDGService->parseTimeStamp(json_decode($resultMock, 1));
        $this->assertSame($expexted, $result);
    }

    public function testGetTimeStamp()
    {
        $resultMock = '{"took":145,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":14120,"max_score":null,"hits":[{"_index":"fb_articles","_type":"_doc","_id":"4548671","_score":null,"_source":{"updated":"1579042801","title":"Alirocumab Reduces Rates of Major CV Events in Patients With Persistently High LDL Cholesterol","type":"DGDispatch"},"sort":[1579042801]},{"_index":"fb_articles","_type":"_doc","_id":"4701608","_score":null,"_source":{"updated":"1579042801","title":"NL test 8.1.2020","type":"DGDispatch"},"sort":[1579042801]}]}}';
        $expexted = [];
        $article['title'] = 'Alirocumab Reduces Rates of Major CV Events in Patients With Persistently High LDL Cholesterol';
        $article['type'] = 'DGDispatch';
        $article['updated'] = '2020-01-14 23:00:01';
        $expexted[] = $article;
        $article['title'] = 'NL test 8.1.2020';
        $article['type'] = 'DGDispatch';
        $article['updated'] = '2020-01-14 23:00:01';
        $expexted[] = $article;
        $this->clientMock->expects('search')->andReturn(json_decode($resultMock, 1));
        $result = $this->ESDGService->getTimeStamp();
        $this->assertSame($expexted, $result);
    }

    public function testbuildAbstractCountsESQuery()
    {
        $date = '2019-12-23';
        $expectedQuery = json_decode('{"size":0,"index":"fb_articles","body":{"query":{"bool":{"must":[{"term":{"status":1}},{"term":{"type":"DGAbstract"}}],"filter":{"range":{"updated":{"gt":1577059200,"lt":1577145600}}}}},"sort":{"updated":{"order":"desc"}}},"client":{"curl":{"10036":"GET"}}}', 1);
        $query = $this->ESQB->buildAbstractCountsESQuery($date);
        $this->assertSame($expectedQuery, $query);
    }

    public function testParseAbstractsReadOnly()
    {
        $resultMock = '{"took":3,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":1,"max_score":null,"hits":[{"_index":"fb_articles","_type":"_doc","_id":"4702387","_score":null,"_source":{"id":"4702387", "title":"Abstract 1"},"sort":[1580407803]}]}}';
        $expected[] = ['id' => '4702387', 'title' => 'Abstract 1', 'updated' => date('Y-m-d')];
        $result = $this->ESDGService->parseAbstractsReadOnly(date('Y-m-d'), json_decode($resultMock, 1));
        $this->assertSame($expected, $result);
    }

    public function testGetAbstractsReadOnly()
    {
        $date = '2019-12-23';
        $resultMock = '{"took":3,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":1,"max_score":null,"hits":[{"_index":"fb_articles","_type":"_doc","_id":"4702387","_score":null,"_source":{"id":"4702387", "title":"Abstract 1"},"sort":[1580407803]}]}}';
        $this->clientMock->expects('search')->andReturn(json_decode($resultMock, 1));
        $expected['data'][] = ['id' => '4702387', 'title' => 'Abstract 1', 'updated' => $date];
        $expected['message'] = '';
        $result = $this->ESDGService->getAbstractsReadOnly($date);
        $this->assertSame($expected, $result);
    }

    public function testGetFBArticles()
    {
        $resultMock = '{"took":18,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":636510,"max_score":null,"hits":[{"_index":"fb_articles_fbes","_type":"_doc","_id":"4709524","_score":null,"_source":{"id":"4709524","title":"A case of Trypanosoma evansi in a German Shepherd dog in Vietnam.","type":"DGME","updated":"1600360502","status":"1"},"sort":[1600360502]}]}}';
        $expected['id'] = '4709524';
        $expected['title'] = 'A case of Trypanosoma evansi in a German Shepherd dog in Vietnam.';
        $expected['status'] = '1';
        $expected['updated'] = '2020-09-17 16:35:02';
        $expected['type'] = 'DGME';
        $this->clientMock->expects('search')->andReturn(json_decode($resultMock, 1));
        $result = $this->ESDGService->getFBArticles();
        $this->assertSame([$expected], $result);
    }

    public function testBuildFBArticlesQueryWithoutDate()
    {
        $expectedQuery = json_decode('{"scroll":"1m","size":100,"index":"fb_articles","body":{"_source":["id","title","updated","type","status"],"sort":{"updated":{"order":"desc"}}},"client":{"curl":{"10036":"GET"}}}', 1);
        $query = $this->ESQB->buildFBArticlesQuery();
        $this->assertSame($expectedQuery, $query);
    }

    public function testBuildFBArticlesQueryWithDate()
    {
        $date = '2020-09-17';
        $expectedQuery = json_decode('{"scroll":"1m","size":100,"index":"fb_articles","body":{"_source":["id","title","updated","type","status"],"sort":{"updated":{"order":"desc"}},"query":{"bool":{"filter":{"range":{"updated":{"gt":1600300800,"lt":1600387200}}}}}},"client":{"curl":{"10036":"GET"}}}', 1);
        $query = $this->ESQB->buildFBArticlesQuery($date);
        $this->assertSame($expectedQuery, $query);
    }

    public function testParseFBArticles()
    {
        $resultMock = '{"took":15,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":29,"max_score":null,"hits":[{"_index":"fb_articles_fbes","_type":"_doc","_id":"4705191","_score":null,"_source":{"id":"4705191","title":"Tenue d un essai clinique sur le recours au plasma de convalescent pour le traitement de la COVID-19","type":"DGAlerts","updated":"1588348803","status":"1"},"sort":[1588348803]}]}}';
        $resultMock = '{"took":21,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":37,"max_score":null,"hits":[{"_index":"fb_articles_fbes","_type":"_doc","_id":"4711037","_score":null,"_source":{"id":"4711037","title":"Coronavirus (COVID-19) Update: Daily Roundup September 17, 2020","type":"DGAlerts","updated":"1600383303","status":"1"},"sort":[1600383303]},{"_index":"fb_articles_fbes","_type":"_doc","_id":"4711036","_score":null,"_source":{"id":"4711036","title":"title 1 for vadym  tylenol","type":"DGDispatch","updated":"1600350603","status":"1"},"sort":[1600350603]},{"_index":"fb_articles_fbes","_type":"_doc","_id":"4711032","_score":null,"_source":{"id":"4711032","title":"A case of Trypanosoma evansi in a German Shepherd dog in Vietnam.","type":"DGCasesText","updated":"1600327208","status":"1"},"sort":[1600327208]}]}}';
        $expected[] = [
            'id' => '4711037',
            'title' => 'Coronavirus (COVID-19) Update: Daily Roundup September 17, 2020',
            'status' => '1',
            'updated' => '2020-09-17 22:55:03',
            'type' => 'DGAlerts',
        ];
        $expected[] = [
            'id' => '4711036',
            'title' => 'title 1 for vadym  tylenol',
            'status' => '1',
            'updated' => '2020-09-17 13:50:03',
            'type' => 'DGDispatch',
        ];
        $expected[] = [
            'id' => '4711032',
            'title' => 'A case of Trypanosoma evansi in a German Shepherd dog in Vietnam.',
            'status' => '1',
            'updated' => '2020-09-17 07:20:08',
            'type' => 'DGCasesText',
        ];

        $result = $this->ESDGService->parseFBArticles(json_decode($resultMock, 1));
        $this->assertSame($expected, $result);
    }

    public function testSearchFBArticlesByDate()
    {
        $date = '2020-09-18';
        $resultMock = '{"took":2,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":38,"max_score":null,"hits":[{"_index":"fb_articles_fbes","_type":"_doc","_id":"4711037","_score":null,"_source":{"id":"4711037","title":"Coronavirus (COVID-19) Update: Daily Roundup September 17, 2020","type":"DGAlerts","updated":"1600383303","status":"1"},"sort":[1600383303]}]}}';
        $this->clientMock->expects('search')->andReturn(json_decode($resultMock, 1));
        $expected = [
            'data' => [[
                'id' => '4711037',
                'title' => 'Coronavirus (COVID-19) Update: Daily Roundup September 17, 2020',
                'status' => '1',
                'updated' => '2020-09-17 22:55:03',
                'type' => 'DGAlerts',
            ]],
            'message' => ''
        ];
        $result = $this->ESDGService->searchFBArticlesByDate($date);
        $this->assertSame($expected, $result);
    }

    public function testSearchFBArticlesByDateLargeResultsMsg()
    {
        $date = '2020-09-18';
        $resultMock = '{"took":2,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":3800,"max_score":null,"hits":[{"_index":"fb_articles_fbes","_type":"_doc","_id":"4711037","_score":null,"_source":{"id":"4711037","title":"Coronavirus (COVID-19) Update: Daily Roundup September 17, 2020","type":"DGAlerts","updated":"1600383303","status":"1"},"sort":[1600383303]}]}}';
        $this->clientMock->expects('search')->andReturn(json_decode($resultMock, 1));
        $expected = [
            'data' => [[
                'id' => '4711037',
                'title' => 'Coronavirus (COVID-19) Update: Daily Roundup September 17, 2020',
                'status' => '1',
                'updated' => '2020-09-17 22:55:03',
                'type' => 'DGAlerts',
            ]],
            'message' => 'Results size is too large, displaying only 1000 records'
        ];
        $result = $this->ESDGService->searchFBArticlesByDate($date);
        $this->assertSame($expected, $result);
    }

    public function testGetFBArticleById()
    {
        $responseMock = '{"took":3,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":1,"max_score":1,"hits":[{"_index":"fb_articles_fbes","_type":"_doc","_id":"4710908","_score":1,"_source":{"id":"4710908","title":"title empty body limit test test test test test test","body":"","type":"Grey8News","status":"1","publication_date":"1600142400","created":"1600198502","updated":"1600269902","updated_for_es":"1600269902","lima_parser":"1","human_url":"title-empty-body","md5_human_url":"f75f800ff37583bf76c77af8e3ea2313","extra_fields":{"name":"empty body NAME","update_type":"Treatment Update","article_url":"https://academic.oup.com/cid/advance-article/doi/10.1093/cid/ciaa1258/5898121","teaser":"teaser empty body","source":"test test"},"tags":{"external_ids":{"woodwingid":"1017573"}}}}]}}';
        $expected = [
            'publication_date' => 'September 15, 2020 - 04:00:00',
            'created' => 'September 15, 2020 - 19:35:02',
            'updated' => 'September 16, 2020 - 15:25:02',
            'extra_fields' => [
                'Name' => 'empty body NAME',
                'Update Type' => 'Treatment Update',
                'Article Url' => 'https://academic.oup.com/cid/advance-article/doi/10.1093/cid/ciaa1258/5898121',
                'Teaser' => 'teaser empty body',
                'Source' => 'test test',
            ],
            'tags' => [
                'External Ids' => [
                    0 => '1017573'
                ]
            ],
            'title' => 'title empty body limit test test test test test test',
            'body' => '',
            'type' => 'Grey8News',
            'status' => '1',
            'lima_parser' => '1',
            'human_url' => 'title-empty-body',
            'md5_human_url' => 'f75f800ff37583bf76c77af8e3ea2313',
            'score' => '',

        ];
        $this->clientMock->expects('search')->andReturn(json_decode($responseMock, 1));
        $result = $this->ESDGService->getFBArticleById(4710908);
        $this->assertSame($expected, $result);
    }

    public function testParseFBArticle()
    {
        $responseMock = '{"took":3,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":1,"max_score":1,"hits":[{"_index":"fb_articles_fbes","_type":"_doc","_id":"4710908","_score":1,"_source":{"id":"4710908","title":"title empty body limit test test test test test test","body":"","type":"Grey8News","status":"1","publication_date":"1600142400","created":"1600198502","updated":"1600269902","updated_for_es":"1600269902","lima_parser":"1","human_url":"title-empty-body","md5_human_url":"f75f800ff37583bf76c77af8e3ea2313","extra_fields":{"name":"empty body NAME","update_type":"Treatment Update","article_url":"https://academic.oup.com/cid/advance-article/doi/10.1093/cid/ciaa1258/5898121","teaser":"teaser empty body","source":"test test"},"tags":{"external_ids":{"woodwingid":"1017573"}}}}]}}';
        $expected = [
            'publication_date' => 'September 15, 2020 - 04:00:00',
            'created' => 'September 15, 2020 - 19:35:02',
            'updated' => 'September 16, 2020 - 15:25:02',
            'extra_fields' => [
                'Name' => 'empty body NAME',
                'Update Type' => 'Treatment Update',
                'Article Url' => 'https://academic.oup.com/cid/advance-article/doi/10.1093/cid/ciaa1258/5898121',
                'Teaser' => 'teaser empty body',
                'Source' => 'test test',
            ],
            'tags' => [
                'External Ids' => [
                    0 => '1017573'
                ]
            ],
            'title' => 'title empty body limit test test test test test test',
            'body' => '',
            'type' => 'Grey8News',
            'status' => '1',
            'lima_parser' => '1',
            'human_url' => 'title-empty-body',
            'md5_human_url' => 'f75f800ff37583bf76c77af8e3ea2313',
            'score' => '',

        ];
        $result = $this->ESDGService->parseFBArticle(json_decode($responseMock, 1));
        $this->assertSame($expected, $result);
    }

    public function testSnakeToCamelCase()
    {
        $strings = [
            'this_is_a_snake_string',
            '_other_snake',
            '_another_snake_',
            'eeeeeeeh_',
        ];
        $expected = [
            'This Is A Snake String',
            'Other Snake',
            'Another Snake',
            'Eeeeeeeh'
        ];

        $reflectMethod = new \ReflectionMethod(ESDGService::class, 'snakeToCamelCase');
        $reflectMethod->setAccessible(true);

        foreach ($strings as $item => $str) {
            $result = $reflectMethod->invokeArgs($this->ESDGService, [$str]);
            $this->assertSame($expected[$item], $result);
        }
    }
}
