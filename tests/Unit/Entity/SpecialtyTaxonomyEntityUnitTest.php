<?php

namespace App\Tests\Unit\Entity;

use App\Entity\SpecialtyTaxonomy;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox UNIT | Entity | SpecialtyTaxonomy
 */
class SpecialtyTaxonomyEntityUnitTest extends KernelTestCase
{
    /**
     * @testdox Checking SpecialtyTaxonomy basic values
     */
    public function testBasicFields(): void
    {
        $specialtyTax = new SpecialtyTaxonomy;

        $reflection = new \ReflectionClass(SpecialtyTaxonomy::class);

        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax, 69);

        $specialtyTax->setSpecialty('Nessun dorma');

        $this->assertSame(69, $specialtyTax->getId());
        $this->assertSame('Nessun dorma', $specialtyTax->getSpecialty());
    }
}
