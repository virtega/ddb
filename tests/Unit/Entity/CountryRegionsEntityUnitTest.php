<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Country;
use App\Entity\CountryRegions;
use App\Entity\CountryTaxonomy;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox UNIT | Entity | CountryRegions
 */
class CountryRegionsEntityUnitTest extends KernelTestCase
{
    /**
     * @testdox Checking CountryRegions basic values
     */
    public function testBasicFields(): void
    {
        $countryRg = new CountryRegions;

        $CRreflection = new \ReflectionClass(CountryRegions::class);

        $reflProp = $CRreflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($countryRg, 10);

        $country = new Country;
        $reflection = new \ReflectionClass(Country::class);
        $reflProp = $reflection->getProperty('isoCode');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'IG');
        $reflProp = $reflection->getProperty('name');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'Intra-Gangrenous');
        $reflProp = $reflection->getProperty('capitalCity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'Central Gangrenous');
        $countryRg->setCountry($country);

        $countryTax = new CountryTaxonomy;
        $reflection = new \ReflectionClass(CountryTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($countryTax, 59);
        $reflProp = $reflection->getProperty('name');
        $reflProp->setAccessible(true);
        $reflProp->setValue($countryTax, 'Fabus Indi');
        $countryRg->setRegion($countryTax);

        $this->assertSame(10, $countryRg->getId());
        $this->assertSame('Intra-Gangrenous', $countryRg->getCountry()->getName());
        $this->assertSame('Central Gangrenous', $countryRg->getCountry()->getCapitalCity());
        $this->assertSame(59, $countryRg->getRegion()->getId());
        $this->assertSame('Fabus Indi', $countryRg->getRegion()->getName());
        $this->assertSame(null, $countryRg->getRegion()->getParent());
    }
}
