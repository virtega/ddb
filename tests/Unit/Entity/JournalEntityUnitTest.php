<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Journal;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox UNIT | Entity | Journal
 */
class JournalEntityUnitTest extends KernelTestCase
{
    /**
     * @testdox Checking Journal defaulting field values
     */
    public function testDefaults(): void
    {
        $this->assertSame(0, Journal::STATUS_NONE);
        $this->assertSame(1, Journal::STATUS_ACTIVE);
        $this->assertSame(5, Journal::STATUS_ARCHIVE);

        $journal = new Journal();

        $reflection = new \ReflectionClass(Journal::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($journal, 234);

        $this->assertSame(234, $journal->getId());

        $this->assertNull($journal->getName());
        $this->assertNull($journal->getAbbreviation());
        $this->assertNull($journal->getVolume());
        $this->assertNull($journal->getNumber());
        $this->assertSame(0, $journal->getStatus());
        $this->assertSame('None', $journal->getStatusReadable());
        $this->assertSame(0, $journal->getExported());
        $this->assertSame(0, $journal->getIsMedline());
        $this->assertSame(0, $journal->getIsSecondLine());
        $this->assertSame(0, $journal->getIsDgAbstract());
        $this->assertSame(0, $journal->getNotBeingUpdated());
        $this->assertSame(0, $journal->getDrugsMonitored());
        $this->assertSame(0, $journal->getGlobalEditionJournal());
        $this->assertSame(0, $journal->getDeleted());
        $this->assertSame(0, $journal->getManualCreation());
        $this->assertNull($journal->getMedlineIssn());
        $this->assertSame(0.00, $journal->getJournalReport1());
        $this->assertSame(0.00, $journal->getJournalReport2());
        $this->assertNull($journal->getUnid());
        $this->assertSame([], $journal->getSpecialties()->toArray());

        $this->assertTrue($journal->isStatusNone());
        $this->assertFalse($journal->isStatusActive());
        $this->assertFalse($journal->isStatusArchived());
        $this->assertFalse($journal->isExported());
        $this->assertFalse($journal->isMedLine());
        $this->assertFalse($journal->isSecondLine());
        $this->assertFalse($journal->isDgAbstract());
        $this->assertFalse($journal->isNotBeingUpdated());
        $this->assertFalse($journal->isDrugsMonitored());
        $this->assertFalse($journal->isGlobalEditionJournal());
        $this->assertFalse($journal->isDeleted());
        $this->assertFalse($journal->isManualCreation());

        $this->assertSame([
            'language'               => '',
            'country'                => null,
            'region'                 => null,
            'edition'                => '',
            'copyright'              => '',
            'auto_publishing_source' => '',
            'previous_rowguid'       => '',
            'abstract'               => [
                'abstract'             => false,
                'reg_for_abstract'     => false,
                'fulltext_availablity' => false,
                'reg_for_fulltext'     => false,
                'fee_for_fulltext'     => false,
                'comments'             => '',
            ],
            'ordering' => [
                'html_code'    => '',
                'cgi_value'    => '',
                'printer_name' => '',
            ],
            'publisher' => [
                'name'    => '',
                'reg_url' => '',
            ],
            'url' => [
                'url'               => '',
                'updated_on'        => null,
                'viewed_on'         => null,
                'viewed_on_comment' => '',
                'to_update'         => null,
                'note'              => '',
                'frequency'         => '',
                'username'          => '',
                'password'          => '',
            ],
            'validation' => [
                'author'        => '',
                'date_composed' => null,
                'cleared_by'    => '',
                'cleared_on'    => null,
                'modified'      => '',
            ],
            '_meta' => [
                'created_on'  => null,
                'created_by'  => null,
                'modified_on' => null,
                'modified_by' => null,
            ],
        ], $journal->getDetails());

        $this->assertSame('', $journal->getDetailsLanguage());
        $this->assertSame(null, $journal->getDetailsCountry());
        $this->assertSame(null, $journal->getDetailsRegion());
        $this->assertSame('', $journal->getDetailsEdition());
        $this->assertSame('', $journal->getDetailsCopyright());
        $this->assertSame('', $journal->getDetailsAutoPublishingSource());
        $this->assertSame('', $journal->getDetailsPreviousRowGuid());
        $this->assertSame(false, $journal->getDetailsAbstractAbstract());
        $this->assertSame(false, $journal->getDetailsAbstractRegForAbstract());
        $this->assertSame(false, $journal->getDetailsAbstractFullTextAvailablity());
        $this->assertSame(false, $journal->getDetailsAbstractRegForFullText());
        $this->assertSame(false, $journal->getDetailsAbstractFeeForFullText());
        $this->assertSame('', $journal->getDetailsAbstractComments());
        $this->assertSame('', $journal->getDetailsOrderingHtmlCode());
        $this->assertSame('', $journal->getDetailsOrderingCgiValue());
        $this->assertSame('', $journal->getDetailsOrderingPrinterName());
        $this->assertSame('', $journal->getDetailsPublisherName());
        $this->assertSame('', $journal->getDetailsPublisherRegUrl());
        $this->assertSame('', $journal->getDetailsUrlUrl());
        $this->assertSame(null, $journal->getDetailsUrlUpdatedOn());
        $this->assertSame(null, $journal->getDetailsUrlViewedOn());
        $this->assertSame('', $journal->getDetailsUrlViewedOnComment());
        $this->assertSame(null, $journal->getDetailsUrlToUpdate());
        $this->assertSame('', $journal->getDetailsUrlNote());
        $this->assertSame('', $journal->getDetailsUrlFrequency());
        $this->assertSame('', $journal->getDetailsUrlUsername());
        $this->assertSame('', $journal->getDetailsUrlPassword());
        $this->assertSame('', $journal->getDetailsValidationAuthor());
        $this->assertSame(null, $journal->getDetailsValidationDateComposed());
        $this->assertSame('', $journal->getDetailsValidationClearedBy());
        $this->assertSame(null, $journal->getDetailsValidationClearedOn());
        $this->assertSame('', $journal->getDetailsValidationModified());

        $this->assertNull($journal->getDetailsMetaCreatedOn());
        $this->assertNull($journal->getDetailsMetaCreatedBy());
        $this->assertNull($journal->getDetailsMetaModifiedOn());
        $this->assertNull($journal->getDetailsMetaModifiedBy());
    }

    /**
     * @testdox Checking Journal with data field values
     */
    public function testBasicAssignments(): void
    {
        $journal = new Journal();

        $reflection = new \ReflectionClass(Journal::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($journal, 345);

        $this->assertSame(345, $journal->getId());

        $specialtyTax1 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax1, 40);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax1, 'Oncology');

        $specialtyTax2 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax2, 41);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax2, 'Gideon');

        $specialtyTax3 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax3, 42);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax3, 'Silver');

        $specialty1 = new \App\Entity\JournalSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\JournalSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, 5);
        $reflProp = $reflection->getProperty('journal');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, $journal);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, $specialtyTax1);

        $specialty2 = new \App\Entity\JournalSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\JournalSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, 65);
        $reflProp = $reflection->getProperty('journal');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, $journal);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, $specialtyTax2);

        $specialty3 = new \App\Entity\JournalSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\JournalSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty3, 1);

        $journal
            ->setName('Histology Today')
            ->setAbbreviation('Hist2day')
            ->setVolume('Vol 2018')
            ->setNumber('2 JA2X')
            ->setStatus(5)
            ->setExported(0)
            ->setIsMedline(1)
            ->setIsSecondLine(2)
            ->setIsDgAbstract(1)
            ->setNotBeingUpdated(0)
            ->setDrugsMonitored(1)
            ->setGlobalEditionJournal(1)
            ->setDeleted(0)
            ->setManualCreation(1)
            ->setMedlineIssn('234A-3463')
            ->setJournalReport1(166.6693636566912102)
            ->setJournalReport2(0)
            ->setUnid('097S69VAFBG9SD6FBAFN0557A34SDASD')
            ->addSpecialty($specialty1)
            ->addSpecialty($specialty2)
            ->addSpecialty($specialty3)
            ->removeSpecialty($specialty3)
            ->setDetailsLanguage('Cantonese Mandarin Halsey')
            ->setDetailsCountry('MG')
            ->setDetailsRegion(101)
            ->setDetailsEdition('RE #36')
            ->setDetailsCopyright('Copyright by Top Alliance Papers')
            ->setDetailsAutoPublishingSource('Yes this correct')
            ->setDetailsPreviousRowGuid('DC7A21D1-28E1-4D08-BBDC-8D0610E4AA88')
            ->setDetailsAbstractAbstract(true)
            ->setDetailsAbstractRegForAbstract(false)
            ->setDetailsAbstractFullTextAvailablity(true)
            ->setDetailsAbstractRegForFullText(true)
            ->setDetailsAbstractFeeForFullText(false)
            ->setDetailsAbstractComments("Yes this is a another test\r\nstill")
            ->setDetailsOrderingHtmlCode('http://google?q=%tsearch%&pa=%member_id%#!show-diagram')
            ->setDetailsOrderingCgiValue(58000)
            ->setDetailsOrderingPrinterName('Africa Printer Union')
            ->setDetailsPublisherName('APU - Africa Printer Union')
            ->setDetailsPublisherRegUrl('http://chitan.com/west-register')
            ->setDetailsUrlUrl('http://Hist2day.register.com/register?mmbrid=345234523')
            ->setDetailsUrlUpdatedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2016-06-06 06:06:06'))
            ->setDetailsUrlViewedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2016-08-08 08:08:08'))
            ->setDetailsUrlViewedOnComment('Don\'t need to update this')
            ->setDetailsUrlToUpdate(null)
            ->setDetailsUrlNote('')
            ->setDetailsUrlFrequency('3-4 times a week, trice a year')
            ->setDetailsUrlUsername('lovelyDay')
            ->setDetailsUrlPassword('Bi||With3rs')
            ->setDetailsValidationAuthor('Kim.Joung/WEB3')
            ->setDetailsValidationDateComposed(\DateTime::createFromFormat('Y-m-d H:i:s', '2015-03-26 00:00:01'))
            ->setDetailsValidationClearedBy('Lisa.Hal/SA')
            ->setDetailsValidationClearedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2015-03-26 00:00:02'))
            ->setDetailsValidationModified('Add membership token on register link')
            ->setDetailsMetaCreatedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2015-03-26 00:00:05'))
            ->setDetailsMetaCreatedBy('Halsey.Duo')
            ->setDetailsMetaModifiedOn(\DateTime::createFromFormat('Y-m-d H:i:s', '2017-05-26 13:25:05'))
            ->setDetailsMetaModifiedBy('Halsey.Trio')
        ;

        $this->assertSame('Histology Today', $journal->getName());
        $this->assertSame('Hist2day', $journal->getAbbreviation());
        $this->assertSame('Vol 2018', $journal->getVolume());
        $this->assertSame('2 JA2X', $journal->getNumber());
        $this->assertSame(Journal::STATUS_ARCHIVE, $journal->getStatus());
        $this->assertSame('Archived', $journal->getStatusReadable());
        $this->assertSame(0, $journal->getExported());
        $this->assertSame(1, $journal->getIsMedline());
        $this->assertSame(2, $journal->getIsSecondLine());
        $this->assertSame(1, $journal->getIsDgAbstract());
        $this->assertSame(0, $journal->getNotBeingUpdated());
        $this->assertSame(1, $journal->getDrugsMonitored());
        $this->assertSame(1, $journal->getGlobalEditionJournal());
        $this->assertSame(0, $journal->getDeleted());
        $this->assertSame(1, $journal->getManualCreation());
        $this->assertSame('234A-3463', $journal->getMedlineIssn());
        $this->assertEquals(166.67, $journal->getJournalReport1());
        $this->assertEquals("0", $journal->getJournalReport2());
        $this->assertSame('097S69VAFBG9SD6FBAFN0557A34SDASD', $journal->getUnid());
        $this->assertSame(2, $journal->getSpecialties()->count());

        foreach ($journal->getSpecialties() as $gettSpecial) {
            $this->assertSame($journal, $gettSpecial->getJournal());

            switch ($gettSpecial->getId()) {
                case 5:
                    $this->assertSame('Oncology', $gettSpecial->getSpecialty()->getSpecialty());
                    break;

                case 65:
                    $this->assertSame('Gideon', $gettSpecial->getSpecialty()->getSpecialty());

                    $gettSpecial->setSpecialty($specialtyTax3);
                    $this->assertSame('Silver', $gettSpecial->getSpecialty()->getSpecialty());
                    $this->assertSame(42, $gettSpecial->getSpecialty()->getId());
                    break;
            }
        }

        $this->assertFalse($journal->isStatusNone());
        $this->assertFalse($journal->isStatusActive());
        $this->assertTrue($journal->isStatusArchived());

        $this->assertFalse($journal->isExported());
        $this->assertTrue($journal->isMedLine());
        $this->assertTrue($journal->isSecondLine());
        $this->assertTrue($journal->isDgAbstract());
        $this->assertFalse($journal->isNotBeingUpdated());
        $this->assertTrue($journal->isDrugsMonitored());
        $this->assertTrue($journal->isGlobalEditionJournal());
        $this->assertFalse($journal->isDeleted());
        $this->assertTrue($journal->isManualCreation());

        $this->assertSame([
            'language'               => 'Cantonese Mandarin Halsey',
            'country'                => 'MG',
            'region'                 => 101,
            'edition'                => 'RE #36',
            'copyright'              => 'Copyright by Top Alliance Papers',
            'auto_publishing_source' => 'Yes this correct',
            'previous_rowguid'       => 'DC7A21D1-28E1-4D08-BBDC-8D0610E4AA88',
            'abstract'               => [
                'abstract'             => true,
                'reg_for_abstract'     => false,
                'fulltext_availablity' => true,
                'reg_for_fulltext'     => true,
                'fee_for_fulltext'     => false,
                'comments'             => 'Yes this is a another test<br/>still',
            ],
            'ordering' => [
                'html_code'    => 'http://google?q=%tsearch%&pa=%member_id%#!show-diagram',
                'cgi_value'    => '58000',
                'printer_name' => 'Africa Printer Union',
            ],
            'publisher' => [
                'name'    => 'APU - Africa Printer Union',
                'reg_url' => 'http://chitan.com/west-register',
            ],
            'url' => [
                'url'               => 'http://Hist2day.register.com/register?mmbrid=345234523',
                'updated_on'        => '2016-06-06 06:06:06',
                'viewed_on'         => '2016-08-08 08:08:08',
                'viewed_on_comment' => 'Don\'t need to update this',
                'to_update'         => null,
                'note'              => '',
                'frequency'         => '3-4 times a week, trice a year',
                'username'          => 'lovelyDay',
                'password'          => 'Bi||With3rs',
            ],
            'validation' => [
                'author'        => 'Kim.Joung/WEB3',
                'date_composed' => '2015-03-26 00:00:01',
                'cleared_by'    => 'Lisa.Hal/SA',
                'cleared_on'    => '2015-03-26 00:00:02',
                'modified'      => 'Add membership token on register link',
            ],
            '_meta' => [
                'created_on'  => '2015-03-26 00:00:05',
                'created_by'  => 'Halsey.Duo',
                'modified_on' => '2017-05-26 13:25:05',
                'modified_by' => 'Halsey.Trio',
            ],
        ], $journal->getDetails());

        $this->assertSame('Cantonese Mandarin Halsey', $journal->getDetailsLanguage());
        $this->assertSame('MG', $journal->getDetailsCountry());
        $this->assertSame(101, $journal->getDetailsRegion());
        $this->assertSame('RE #36', $journal->getDetailsEdition());
        $this->assertSame('Copyright by Top Alliance Papers', $journal->getDetailsCopyright());
        $this->assertSame('Yes this correct', $journal->getDetailsAutoPublishingSource());
        $this->assertSame('DC7A21D1-28E1-4D08-BBDC-8D0610E4AA88', $journal->getDetailsPreviousRowGuid());

        $this->assertSame(true, $journal->getDetailsAbstractAbstract());
        $this->assertSame(false, $journal->getDetailsAbstractRegForAbstract());
        $this->assertSame(true, $journal->getDetailsAbstractFullTextAvailablity());
        $this->assertSame(true, $journal->getDetailsAbstractRegForFullText());
        $this->assertSame(false, $journal->getDetailsAbstractFeeForFullText());
        $this->assertSame('Yes this is a another test' . PHP_EOL .'still', $journal->getDetailsAbstractComments());

        $this->assertSame('http://google?q=%tsearch%&pa=%member_id%#!show-diagram', $journal->getDetailsOrderingHtmlCode());
        $this->assertSame('58000', $journal->getDetailsOrderingCgiValue());
        $this->assertSame('Africa Printer Union', $journal->getDetailsOrderingPrinterName());

        $this->assertSame('APU - Africa Printer Union', $journal->getDetailsPublisherName());
        $this->assertSame('http://chitan.com/west-register', $journal->getDetailsPublisherRegUrl());

        $this->assertSame('http://Hist2day.register.com/register?mmbrid=345234523', $journal->getDetailsUrlUrl());
        $this->assertSame('2016-06-06', $journal->getDetailsUrlUpdatedOn()->format('Y-m-d'));
        $this->assertSame('2016-08-08', $journal->getDetailsUrlViewedOn()->format('Y-m-d'));
        $this->assertSame('Don\'t need to update this', $journal->getDetailsUrlViewedOnComment());
        $this->assertSame(null, $journal->getDetailsUrlToUpdate());
        $this->assertSame('', $journal->getDetailsUrlNote());
        $this->assertSame('3-4 times a week, trice a year', $journal->getDetailsUrlFrequency());
        $this->assertSame('lovelyDay', $journal->getDetailsUrlUsername());
        $this->assertSame('Bi||With3rs', $journal->getDetailsUrlPassword());

        $this->assertSame('Kim.Joung/WEB3', $journal->getDetailsValidationAuthor());
        $this->assertSame('00:00:01', $journal->getDetailsValidationDateComposed()->format('H:i:s'));
        $this->assertSame('Lisa.Hal/SA', $journal->getDetailsValidationClearedBy());
        $this->assertSame('00:00:02', $journal->getDetailsValidationClearedOn()->format('H:i:s'));
        $this->assertSame('Add membership token on register link', $journal->getDetailsValidationModified());

        $this->assertSame('05', $journal->getDetailsMetaCreatedOn()->format('s'));
        $this->assertSame('Halsey.Duo', $journal->getDetailsMetaCreatedBy());
        $this->assertSame('13:25', $journal->getDetailsMetaModifiedOn()->format('H:i'));
        $this->assertSame('Halsey.Trio', $journal->getDetailsMetaModifiedBy());
    }
}
