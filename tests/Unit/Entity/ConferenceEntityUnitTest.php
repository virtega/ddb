<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Conference;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox UNIT | Entity | Conference
 */
class ConferenceEntityUnitTest extends KernelTestCase
{
    /**
     * @testdox Checking Conference defaulting field values
     */
    public function testDefaults(): void
    {
        $conference = new Conference();

        $reflection = new \ReflectionClass(Conference::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($conference, 123);

        $this->assertSame(123, $conference->getId());

        $this->assertNull($conference->getName());
        $this->assertNull($conference->getStartDate());
        $this->assertNull($conference->getEndDate());
        $this->assertNull($conference->getCity());
        $this->assertNull($conference->getCityGuide());
        $this->assertNull($conference->getCityGuide());
        $this->assertNull($conference->getCountry());
        $this->assertNull($conference->getRegion());
        $this->assertNull($conference->getPreviousId());
        $this->assertNull($conference->getUniqueName());

        $this->assertSame(0, $conference->getCruise());
        $this->assertFalse($conference->isCruise());
        $this->assertSame(0, $conference->getOnline());
        $this->assertFalse($conference->isOnline());
        $this->assertSame(0, $conference->getKeyEvent());
        $this->assertFalse($conference->isKeyEvent());
        $this->assertSame(0, $conference->getHasGuide());
        $this->assertFalse($conference->isHasGuide());
        $this->assertSame(0, $conference->getDiscontinued());
        $this->assertFalse($conference->isDiscontinued());

        $this->assertSame([], $conference->getSpecialties()->toArray());

        $this->assertSame([
            'state'   => '',
            'contact' => [
                'desc'  => '',
                'phone' => [],
                'fax'   => [],
                'email' => [],
            ],
            'links' => [],
            '_meta' => [
                'created_on'  => null,
                'created_by'  => null,
                'modified_on' => null,
                'modified_by' => null,
            ],
        ], $conference->getDetails());

        $this->assertSame('', $conference->getDetailsState());
        $this->assertSame('', $conference->getDetailsContactDesc());
        $this->assertSame([], $conference->getDetailsContactPhones());
        $this->assertSame([], $conference->getDetailsContactEmails());
        $this->assertSame([], $conference->getDetailsContactLinks());

        $this->assertNull($conference->getDetailsMetaCreatedOn());
        $this->assertNull($conference->getDetailsMetaCreatedBy());
        $this->assertNull($conference->getDetailsMetaModifiedOn());
        $this->assertNull($conference->getDetailsMetaModifiedBy());
    }

    /**
     * @testdox Checking Conference with data field values
     */
    public function testBasicAssignments(): void
    {
        $conference = new Conference();

        $reflection = new \ReflectionClass(Conference::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($conference, 123);

        $this->assertSame(123, $conference->getId());

        $start_date = \DateTime::createFromFormat('Y-m-d H:i:s', '2018-06-03 16:23:54');
        $end_date   = \DateTime::createFromFormat('Y-m-d H:i:s', '2018-06-05 03:04:05');

        $country = new \App\Entity\Country;
        $reflection = new \ReflectionClass(\App\Entity\Country::class);
        $reflProp = $reflection->getProperty('isoCode');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'MY');
        $reflProp = $reflection->getProperty('name');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'Malaysia');
        $reflProp = $reflection->getProperty('capitalCity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'Kuala Lumpur');

        $countryTaxonomy = new \App\Entity\CountryTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\CountryTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($countryTaxonomy, 6568);
        $reflProp = $reflection->getProperty('name');
        $reflProp->setAccessible(true);
        $reflProp->setValue($countryTaxonomy, 'Southeast Asia');

        $region = new \App\Entity\CountryRegions;
        $reflection = new \ReflectionClass(\App\Entity\CountryRegions::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($region, 34532);
        $reflProp = $reflection->getProperty('country');
        $reflProp->setAccessible(true);
        $reflProp->setValue($region, $country);
        $reflProp = $reflection->getProperty('region');
        $reflProp->setAccessible(true);
        $reflProp->setValue($region, $countryTaxonomy);

        $specialtyTax1 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax1, 40);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax1, 'Oncology');

        $specialtyTax2 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax2, 41);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax2, 'Gideon');

        $specialtyTax3 = new \App\Entity\SpecialtyTaxonomy;
        $reflection = new \ReflectionClass(\App\Entity\SpecialtyTaxonomy::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax3, 42);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialtyTax3, 'Silver');

        $specialty1 = new \App\Entity\ConferenceSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\ConferenceSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, 5);
        $reflProp = $reflection->getProperty('conference');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, $conference);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty1, $specialtyTax1);

        $specialty2 = new \App\Entity\ConferenceSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\ConferenceSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, 65);
        $reflProp = $reflection->getProperty('conference');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, $conference);
        $reflProp = $reflection->getProperty('specialty');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty2, $specialtyTax2);

        $specialty3 = new \App\Entity\ConferenceSpecialties;
        $reflection = new \ReflectionClass(\App\Entity\ConferenceSpecialties::class);
        $reflProp = $reflection->getProperty('id');
        $reflProp->setAccessible(true);
        $reflProp->setValue($specialty3, 1);

        $conference
            ->setName('Test Conference 1')
            ->setStartDate($start_date)
            ->setEndDate($end_date)
            ->setCity($country->getCapitalCity())
            ->setCityGuide('W.P. ' . $country->getCapitalCity())
            ->setCountry($country)
            ->setRegion($countryTaxonomy)
            ->setPreviousId('34634534745674564565645643436783')
            ->setUniqueName('123456789132465')
            ->setCruise(1)
            ->setOnline(0)
            ->setKeyEvent(2)
            ->setHasGuide(1)
            ->setDiscontinued(0)
            ->addSpecialty($specialty1)
            ->addSpecialty($specialty2)
            ->addSpecialty($specialty3)
            ->removeSpecialty($specialty3)
            ->setDetails([
                'dummy'   => 'testing 123',
                'state'   => 'Mid Valley City',
                'contact' => [
                    'fax' => [
                        '+603 123 6456',
                        '+603 123 6789',
                    ],
                    'email' => [
                        'moddy.gideon@gmail.com',
                    ],
                ],
                'links' => [
                    [
                        'caption' => 'Homepage',
                        'url'     => 'http://google.com',
                    ]
                ],
                '_meta' => [
                    'created_on' => '2017-03-05 13:45:35',
                    'created_by' => 'jake.ajax',
                ],
            ])
            ->setDetailsMetaModifiedOn( \DateTime::createFromFormat('Y-m-d H:i:s', '2016-08-08 08:08:08') )
            ->addDetailsContactLink('Registration Page', 'http://toyota.com')
            ->setDetailsContactDesc('This is<br   />a Test')
            ->setDetailsContactPhones(['+6 019 1236 456', '+6 019 1236 789'])
        ;

        $this->assertSame('Test Conference 1', $conference->getName());
        $this->assertSame('2018-06-03 16:23:54', $conference->getStartDate()->format('Y-m-d H:i:s'));
        $this->assertSame('2018-06-05', $conference->getEndDate()->format('Y-m-d'));
        $this->assertSame('Kuala Lumpur', $conference->getCity());
        $this->assertSame('W.P. Kuala Lumpur', $conference->getCityGuide());
        $this->assertSame($country, $conference->getCountry());
        $this->assertSame('MY', $conference->getCountry()->getIsoCode());
        $this->assertSame('Malaysia', $conference->getCountry()->getName());
        $this->assertSame('Kuala Lumpur', $conference->getCountry()->getCapitalCity());
        $this->assertSame($countryTaxonomy, $conference->getRegion());
        $this->assertSame(6568, $conference->getRegion()->getId());
        $this->assertSame('Southeast Asia', $conference->getRegion()->getName());
        $this->assertSame(null, $conference->getRegion()->getParent());
        $this->assertSame('34634534745674564565645643436783', $conference->getPreviousId());
        $this->assertSame('123456789132465', $conference->getUniqueName());

        $this->assertSame(1, $conference->getCruise());
        $this->assertSame(true, $conference->isCruise());
        $this->assertSame(0, $conference->getOnline());
        $this->assertSame(false, $conference->isOnline());
        $this->assertSame(2, $conference->getKeyEvent());
        $this->assertSame(true, $conference->isKeyEvent());
        $this->assertSame(1, $conference->getHasGuide());
        $this->assertSame(true, $conference->isHasGuide());
        $this->assertSame(0, $conference->getDiscontinued());
        $this->assertSame(false, $conference->isDiscontinued());

        $this->assertSame(2, $conference->getSpecialties()->count());

        foreach ($conference->getSpecialties() as $gettSpecial) {
            $this->assertSame($conference, $gettSpecial->getConference());

            switch ($gettSpecial->getId()) {
                case 5:
                    $this->assertSame('Oncology', $gettSpecial->getSpecialty()->getSpecialty());
                    break;

                case 65:
                    $this->assertSame('Gideon', $gettSpecial->getSpecialty()->getSpecialty());

                    $gettSpecial->setSpecialty($specialtyTax3);
                    $this->assertSame('Silver', $gettSpecial->getSpecialty()->getSpecialty());
                    $this->assertSame(42, $gettSpecial->getSpecialty()->getId());
                    break;
            }
        }

        $this->assertInstanceOf(\DateTime::class, $conference->getDetailsMetaCreatedOn());
        $this->assertSame('2017-03-05 13:45:35', $conference->getDetailsMetaCreatedOn()->format('Y-m-d H:i:s'));
        $this->assertSame('jake.ajax', $conference->getDetailsMetaCreatedBy());

        $this->assertInstanceOf(\DateTime::class, $conference->getDetailsMetaModifiedOn());
        $this->assertSame('08:08:08', $conference->getDetailsMetaModifiedOn()->format('H:i:s'));
        $this->assertNull($conference->getDetailsMetaModifiedBy());

        $conference->setDetailsMetaCreatedBy('Daniel.Lam');
        $this->assertSame('Daniel.Lam', $conference->getDetailsMetaCreatedBy());

        $conference->setDetailsMetaModifiedBy('Daniel.Lam');
        $this->assertSame('Daniel.Lam', $conference->getDetailsMetaModifiedBy());

        $conference->setDetailsMetaCreatedOn( $conference->getDetailsMetaModifiedOn() );
        $this->assertSame('08:08:08', $conference->getDetailsMetaCreatedOn()->format('H:i:s'));

        $this->assertSame('testing 123', $conference->getDetails()['dummy']);

        $this->assertSame('Mid Valley City', $conference->getDetailsState());
        $this->assertSame('This is' . PHP_EOL . 'a Test', $conference->getDetailsContactDesc());
        $this->assertStringNotContainsString("<br   />", $conference->getDetailsContactDesc());

        $this->assertSame(['+6 019 1236 456', '+6 019 1236 789'], $conference->getDetailsContactPhones());
        $this->assertSame(['+603 123 6456', '+603 123 6789'], $conference->getDetailsContactFaxes());
        $this->assertSame(['moddy.gideon@gmail.com'], $conference->getDetailsContactEmails());

        $this->assertSame([
            [
                'caption' => 'Homepage',
                'url'     => 'http://google.com',
            ],
            [
                'caption' => 'Registration Page',
                'url'     => 'http://toyota.com',
            ]
        ], $conference->getDetailsContactLinks());

        $conference->setDetailsState('Dayna City');
        $this->assertSame('Dayna City', $conference->getDetailsState());

        $conference->setDetailsContactFaxes(['+60191236456']);
        $this->assertSame(['+60191236456'], $conference->getDetailsContactFaxes());

        $conference->setDetailsContactEmails(['William.One@rocket.mail', 'Darren.Loi@about.me']);
        $this->assertSame(['William.One@rocket.mail', 'Darren.Loi@about.me'], $conference->getDetailsContactEmails());

        $conference->setDetailsContactLinks([]);
        $this->assertSame([], $conference->getDetailsContactLinks());

        $this->assertSame([
            'state'   => 'Dayna City',
            'contact' => [
                'desc'  => 'This is<br   />a Test',
                'phone' => [
                    '+6 019 1236 456',
                    '+6 019 1236 789',
                ],
                'fax'   => [
                    '+60191236456',
                ],
                'email' => [
                    'William.One@rocket.mail',
                    'Darren.Loi@about.me',
                ],
            ],
            'links' => [],
            '_meta' => [
                'created_on'  => '2016-08-08 08:08:08',
                'created_by'  => 'Daniel.Lam',
                'modified_on' => '2016-08-08 08:08:08',
                'modified_by' => 'Daniel.Lam',
            ],
            'dummy' => 'testing 123',
        ], $conference->getDetails());
    }
}
