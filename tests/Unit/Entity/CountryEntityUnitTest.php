<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Country;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox UNIT | Entity | Country
 */
class CountryEntityUnitTest extends KernelTestCase
{
    /**
     * @testdox Checking Country defaulting field values
     */
    public function testDefaults(): void
    {
        $country = new Country;

        $this->assertNull($country->getIsoCode());
        $this->assertNull($country->getName());
        $this->assertNull($country->getCapitalCity());
    }

    /**
     * @testdox Checking Country basic values
     */
    public function testBasicFields(): void
    {
        $country = new Country;

        $reflection = new \ReflectionClass(Country::class);

        $reflProp = $reflection->getProperty('isoCode');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'IG');

        $reflProp = $reflection->getProperty('name');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'Intra-Gangrenous');

        $reflProp = $reflection->getProperty('capitalCity');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'Central Gangrenous');

        $this->assertSame('IG', $country->getIsoCode());
        $this->assertSame('Intra-Gangrenous', $country->getName());
        $this->assertSame('Central Gangrenous', $country->getCapitalCity());
    }

    /**
     * @testdox Checking Country set values
     */
    public function testSetFields(): void
    {
        $country = new Country;

        $reflection = new \ReflectionClass(Country::class);

        $reflProp = $reflection->getProperty('isoCode');
        $reflProp->setAccessible(true);
        $reflProp->setValue($country, 'IG');

        $this->assertSame('IG', $country->getIsoCode());
        $this->assertNull($country->getName());
        $this->assertNull($country->getCapitalCity());

        $country->setName('Intra-Gangrenous');
        $this->assertSame('Intra-Gangrenous', $country->getName());

        $country->setCapitalCity('Central Gangrenous');
        $this->assertSame('Central Gangrenous', $country->getCapitalCity());
    }
}
