<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Variable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @testdox UNIT | Entity | Variable
 */
class VariableEntityUnitTest extends KernelTestCase
{
    /**
     * @testdox Handling TEXT values
     */
    public function testVariableValueText()
    {
        $var = new Variable('test');
        $var->setType('text');
        $var->setValue('hello', true);

        $this->assertSame('test', $var->getName());
        $this->assertSame('hello', $var->getValue());
        $this->assertSame('hello', $var->getValue(true));

        $var = new Variable('test');
        $var->setType('text');
        $var->setValue('hello');

        $this->assertSame('hello', $var->getValue());
    }

    /**
     * @testdox Handling INTEGER values
     */
    public function testVariableValueInteger()
    {
        $var = new Variable('test');
        $var->setType('int');
        $var->setValue('1000');

        $this->assertSame('test', $var->getName());

        $this->assertSame(1000, $var->getValue());
        $this->assertEquals('integer', $var->getType());

        $this->assertSame(1000, $var->getValue());
    }

    /**
     * @testdox Handling FLOAT values
     */
    public function testVariableValueFloat()
    {
        $var = new Variable('test');
        $var->setType('float');
        $var->setValue('19.85');

        $this->assertSame(19.85, $var->getValue());

        $var->setValue('3.14159265359', true);
        $this->assertSame(3.14159265359, $var->getValue());
    }

    /**
     * @testdox Handling ARRAY values
     */
    public function testVariableValueArray()
    {
        $json = array(
            'test001' => 1,
            'test002' => 2,
            'test003' => 3,
        );
        $str = json_encode($json);

        $var = new Variable('test');
        $var->setType('array');

        $var->setValue($json);
        $this->assertSame($json, $var->getValue());
        $this->assertEquals('json', $var->getType());

        $var->setValue($str, true);
        $this->assertSame($json, $var->getValue());

        $this->assertSame($str, $var->getValue(true));
    }

    /**
     * @testdox Handling OBJECT values
     */
    public function testVariableValueObject()
    {
        $var = new Variable('test');
        $var->setType('object');

        $dt = new \DateTime();
        $var->setValue($dt);

        $this->assertSame(
        $dt->format('Y-m-d H:i:s.u'),
        $var->getValue()->date
    );

        $this->assertSame(
        $dt->getTimezone()->getName(),
        $var->getValue()->timezone
    );
    }

    /**
     * @testdox Handling DATETIME values
     */
    public function testVariableValueDateTime()
    {
        $date = '1981-04-30 03:01:17';
        $time = (int) strtotime($date);
        $datO = \DateTime::createFromFormat('Y-m-d G:i:s', $date);
        $frmt = 'Y-m-d H:i:s';

        $var = new Variable('test');
        $var->setType('datetime');

        $var->setValue($datO);
        $this->assertSame(
        $date,
        $var->getValue()->format($frmt)
    );

        $var->setValue($time);
        $this->assertSame(
        $datO->format($frmt),
        $var->getValue()->format($frmt)
    );

        $var->setValue($date, true);
        $this->assertSame(
        $date,
        $var->getValue()->format($frmt)
    );
    }

    /**
     * @testdox Handling BOOLEAN values
     */
    public function testVariableValueBoolean()
    {
        $var = new Variable('test');
        $var->setType('bool');
        $var->setValue('1', true);

        $this->assertSame(true, $var->getValue());

        $var->setValue(true);
        $this->assertSame(true, $var->getValue());
        $this->assertEquals('boolean', $var->getType());

        $var->setValue('0', true);
        $this->assertSame(false, $var->getValue());

        $var->setValue('', true);
        $this->assertSame(false, $var->getValue());

        $var->setValue(null, true);
        $this->assertSame(false, $var->getValue());

        $var->setValue(array());
        $this->assertSame(false, $var->getValue());

        $var->setValue('confirmed');
        $this->assertSame(true, $var->getValue());

        $var->setValue(array('this-is-a-test'));
        $this->assertSame(true, $var->getValue());

        $var->setValue(new Variable('test-object'));
        $this->assertSame(true, $var->getValue());
    }

    /**
     * @testdox Handling No-Type DateTime values
     */
    public function testVariableValueDateTimeNoType()
    {
        $var = new Variable('test');

        $dt = new \DateTime();
        $var->setValue($dt);

        $this->assertSame(
        $dt->format('Y-m-d H:i:s'),
        $var->getValue()->format('Y-m-d H:i:s')
    );
    }

    /**
     * @testdox Handling DateTime string values
     */
    public function testVariableValueDateTimeStringDt()
    {
        $var = new Variable('test');
        $var->setType('datetime');

        $var->setValue('1925-07-10');

        $this->assertSame(
        '1925-07-10',
        $var->getValue()->format('Y-m-d')
    );
    }

    /**
     * @testdox Handling DateTime annotation values
     */
    public function testVariableValueDateTimeStringAnnotation()
    {
        $var = new Variable('test');
        $var->setType('datetime');

        $var->setValue('+2 days');

        $this->assertSame(
        date('Y-m-d', strtotime('+2 days')),
        $var->getValue()->format('Y-m-d')
    );
    }
}
