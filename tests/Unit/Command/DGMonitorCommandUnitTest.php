<?php

namespace App\Tests\Unit\Command;

use App\Command\DGMonitorCommand;
use App\Service\ESDGService;
use App\Repository\DGMonitorRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @testdox UNIT | Command | DGMonitorCommand
 */
class DGMonitorCommandUnitTest extends KernelTestCase
{
    /**
     * @var CommandTester
     */
    private $commandTester;

    /**
     * @var DGMonitorCommand
     */
    private $command;

    /**
     * @var ESDGService
     */
    private $dgServiceMock;

    /**
     * @var DGMonitorRepository
     */
    private $dgMonitorRepositoryMock;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        self::bootKernel();
        $kernel = static::createKernel();

        $this->dgServiceMock = $this->getMockBuilder(ESDGService::class)
            ->disableOriginalConstructor()
            ->setMethods(['getAbstractsReadOnly'])
            ->getMock();

        $this->dgMonitorRepositoryMock = $this->getMockBuilder(DGMonitorRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['bulkInsertIds', 'delete'])
            ->getMock();

        $command = new DGMonitorCommand(
            self::$container->get('doctrine.orm.default_entity_manager'),
            self::$container->get('monolog.logger'),
            $this->dgServiceMock,
            $this->dgMonitorRepositoryMock
        );

        $app = new Application($kernel);
        $app->add($command);
        $this->command = $app->find('app:dgmonitor-fetch-stats');
        $this->commandTester = new CommandTester($this->command);
    }

    public function testExecuteWithInvalidDates()
    {
        $dates = [
            'invalid',
            '2010-22-01',
            '01-02-2020',
            '2019-01-32',
            '2019-01-02 11:12:13'
        ];
        foreach ($dates as $date) {
            $this->expectException("InvalidArgumentException");
            $this->expectExceptionMessage("Specified date not valid. Format should be: Y-m-d");
            $this->commandTester->execute(['--date'  => $date]);
        }
    }

    public function testExecuteWithNoInputAndNoData()
    {
        $this->dgServiceMock->method('getAbstractsReadOnly')->willReturn([]);
        $this->dgMonitorRepositoryMock->method('delete')->willReturn(0);
        $this->commandTester->execute([]);
        $output = $this->commandTester->getDisplay();
        $date = date('Y-m-d');
        $this->assertContains("No data found for date '" . $date ."'", $output);
    }

    public function testExecuteWithInputAndNoData()
    {
        $date = '2018-02-03';
        $this->dgServiceMock->method('getAbstractsReadOnly')->willReturn([]);
        $this->dgMonitorRepositoryMock->method('delete')->willReturn(0);
        $this->commandTester->execute(['--date'  => $date]);
        $output = $this->commandTester->getDisplay();
        $this->assertContains("No data found for date '" . $date ."'", $output);
    }

    public function testExecuteWithInputAndReturnData()
    {
        $date = '2018-02-03';
        $ids = range(100001, 100125);
        $this->dgServiceMock->method('getAbstractsReadOnly')->willReturn($ids);
        $this->dgMonitorRepositoryMock->method('delete')->willReturn(125);
        $this->dgMonitorRepositoryMock->method('bulkInsertIds')->willReturn(true);
        $this->commandTester->execute(['--date'  => $date]);
        $output = $this->commandTester->getDisplay();
        $this->assertContains("Deleted 125 ids for '2018-02-03'", $output);
        $this->assertContains("Inserted 125 ids for '2018-02-03'", $output);
    }

    public function testExecuteWithNoInputAndReturnData()
    {
        $ids = range(100001, 100125);
        $this->dgServiceMock->method('getAbstractsReadOnly')->willReturn($ids);
        $this->dgMonitorRepositoryMock->method('delete')->willReturn(125);
        $this->dgMonitorRepositoryMock->method('bulkInsertIds')->willReturn(true);
        $this->commandTester->execute([]);
        $output = $this->commandTester->getDisplay();
        $date = date('Y-m-d');
        $this->assertContains("Deleted 125 ids for '{$date}'", $output);
        $this->assertContains("Inserted 125 ids for '{$date}'", $output);
    }
}
