<?php

namespace App\Tests\Unit\Command;

use App\Command\JournalsSyncDownCommand;
use App\Service\JournalSourceService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Command | JournalsSyncDownCommand
 */
class JournalsSyncDownCommandUnitTest extends WebTestCase
{
    /**
     * @var JournalsSyncDownCommand
     */
    private $command;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        self::bootKernel();

        $this->command = new JournalsSyncDownCommand(
            self::$container->get('kernel'),
            self::$container->get('doctrine.orm.default_entity_manager'),
            self::$container->get('monolog.logger'),
            self::$container->get(JournalSourceService::class)
        );
    }

    /**
     * @testdox Checking Journal Welcome Db Table info
     */
    public function testGetWelcomeTable()
    {
        $method = new \ReflectionMethod(JournalsSyncDownCommand::class, 'getWelcomeTablesInfo');
        $method->setAccessible(true);

        $info = $method->invoke($this->command);
        $this->assertTrue(is_array($info));
        $this->assertEquals(3, count($info));
    }

    /**
     * @testdox Checking Journal Entity Name
     */
    public function testGetReadbleEntityName()
    {
        $method = new \ReflectionMethod(JournalsSyncDownCommand::class, 'getReadbleEntityName');
        $method->setAccessible(true);

        $name = $method->invoke($this->command);
        $this->assertSame('Journals', $name);

        $name = $method->invokeArgs ($this->command, [true]);
        $this->assertSame('Journals', $name);

        $name = $method->invokeArgs ($this->command, [false]);
        $this->assertSame('Journal', $name);
    }

    /**
     * @testdox Checking Journal Db Info
     */
    public function testGetRemoteDbInfo()
    {
        $method = new \ReflectionMethod(JournalsSyncDownCommand::class, 'getRemoteDbInfo');
        $method->setAccessible(true);

        $info = $method->invoke($this->command);
        $this->assertTrue(is_array($info));

        $this->assertArrayHasKey('dsn', $info);
        $this->assertArrayHasKey('host', $info);
        $this->assertArrayHasKey('port', $info);
        $this->assertArrayHasKey('dbname', $info);
        $this->assertArrayHasKey('options', $info);
        $this->assertArrayHasKey('version', $info['options']);
        $this->assertArrayHasKey('charset', $info['options']);
        $this->assertArrayHasKey('username', $info);
        $this->assertArrayHasKey('password', $info);
        $this->assertArrayHasKey('connected', $info);
        $this->assertArrayHasKey('error', $info);
        $this->assertArrayHasKey('error-msg', $info);
    }
}
