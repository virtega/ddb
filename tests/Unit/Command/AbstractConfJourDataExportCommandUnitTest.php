<?php

namespace App\Tests\Unit\Command;

use App\Command\AbstractConfJourDataExportCommand;
use App\Entity\Conference;
use App\Entity\Journal;
use App\Service\MailService;
use Mockery as M;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class dummyEmpty01Class extends AbstractConfJourDataExportCommand
{
}

final class dummyEmpty02Class extends AbstractConfJourDataExportCommand
{

    public static $type = 'dummy';
}

final class dummyEmpty03Class extends AbstractConfJourDataExportCommand
{

    public static $type = 'dummy';

    public static $cfg = [
        'name' => '',
    ];

    public function __construct($repo, $mailSrc, $logger)
    {
        $this->repo = $repo;

        parent::__construct($mailSrc, $logger);
    }

    protected function getInputsToConfig(): ?array
    {
        return [
            'name' => '',
        ];
    }
}

/**
 * @testdox UNIT | Command | AbstractConfJourDataExportCommand
 */
class AbstractConfJourDataExportCommandUnitTest extends TestCase
{
    /**
     * @var Mockery|MailService
     */
    private $mailServiceMock;

    /**
     * @var Mockery|LoggerInterface
     */
    private $loggerMock;

    /**
     * @var Mockery|InputInterface
     */
    private $inputInterfaceMock;

    /**
     * @var Mockery|OutputInterface
     */
    private $outputInterfaceMock;

    /**
     * @var AbstractConfJourDataExportCommand
     */
    private $command;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->mailServiceMock     = M::mock(MailService::class);
        $this->loggerMock          = M::mock(LoggerInterface::class);
        $this->inputInterfaceMock  = M::mock(InputInterface::class);
        $this->outputInterfaceMock = M::mock(OutputInterface::class);
    }

    /**
     * @testdox Checking Class Construction and with no $type defined Exception
     *
     * @expectedException \Exception
     *
     * @expectedExceptionMessage AbstractConfJourDataExportCommand $type is empty.
     * @expectedExceptionCode 1
     */
    public function testConstructWithNoType()
    {

        $this->command = new dummyEmpty01Class($this->mailServiceMock, $this->loggerMock);
    }

    /**
     * @testdox Checking Class Construction and Base Method
     */
    public function testConstructAndBasedMethods()
    {
        $this->command = new dummyEmpty02Class($this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);
        self::assertSame('dummy', $this->command->getType());
        self::assertSame([], $this->command->getArguments());

        $method = new \ReflectionMethod(AbstractConfJourDataExportCommand::class, 'getInputsToConfig');
        $method->setAccessible(true);
        $result = $method->invoke($this->command);
        self::assertSame(null, $result);

        $method = new \ReflectionMethod(AbstractConfJourDataExportCommand::class, 'getHeaders');
        $method->setAccessible(true);
        $result = $method->invoke($this->command);
        self::assertSame([], $result);

        $method = new \ReflectionMethod(AbstractConfJourDataExportCommand::class, 'convertJournalToRow');
        $method->setAccessible(true);
        $journal = new Journal;
        $result = $method->invokeArgs($this->command, [$journal]);
        self::assertSame([], $result);

        $method = new \ReflectionMethod(AbstractConfJourDataExportCommand::class, 'convertConferenceToRow');
        $method->setAccessible(true);
        $conference = new Conference;
        $result = $method->invokeArgs($this->command, [$conference]);
        self::assertSame([], $result);
    }

    /**
     * @testdox Checking Command Class execute() with no Email on InputInterface Exception
     *
     * @expectedException \Exception
     *
     * @expectedExceptionMessage Missing Email address on export-command argument.
     * @expectedExceptionCode 1
     */
    public function testClassExecuteWithNoEmail()
    {
        $this->command = new dummyEmpty02Class($this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);

        $this->inputInterfaceMock
            ->shouldReceive('getArgument')
            ->once()
            ->withArgs(function ($arg) {
                self::assertSame('email', $arg);
                return true;
            })
            ->andReturn(null);

        $this->outputInterfaceMock->shouldReceive('getVerbosity')->andReturn(OutputInterface::VERBOSITY_QUIET);
        $this->outputInterfaceMock->shouldReceive('isDecorated')->andReturn(false);
        $this->outputInterfaceMock->shouldReceive('getFormatter')->andReturn(null);

        $method = new \ReflectionMethod(AbstractConfJourDataExportCommand::class, 'execute');
        $method->setAccessible(true);
        $method->invokeArgs($this->command, [$this->inputInterfaceMock, $this->outputInterfaceMock]);
    }

    public function testClassExecuteWithFailedInputToConfig()
    {
        $this->command = new dummyEmpty02Class($this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);

        $this->inputInterfaceMock
            ->shouldReceive('getArgument')
            ->once()
            ->withArgs(function ($arg) {
                self::assertSame('email', $arg);
                return true;
            })
            ->andReturn('dummy@mockery.com');

        $this->outputInterfaceMock->shouldReceive('getVerbosity')->andReturn(OutputInterface::VERBOSITY_QUIET);
        $this->outputInterfaceMock->shouldReceive('isDecorated')->andReturn(false);
        $this->outputInterfaceMock->shouldReceive('getFormatter')->andReturn(null);

        $method = new \ReflectionMethod(AbstractConfJourDataExportCommand::class, 'execute');
        $method->setAccessible(true);
        $result = $method->invokeArgs($this->command, [$this->inputInterfaceMock, $this->outputInterfaceMock]);

        self::assertSame(1, $result);
    }

    public function testClassExecuteTryFailure()
    {
        $repo = M::mock(\Doctrine\ORM\EntityRepository::class);
        $repo->shouldReceive('searchResultCount')->once()->andThrow(new \Exception('Dummy Error', 1));

        $this->mailServiceMock->shouldReceive('setTemplate')
            ->once()
            ->andReturnSelf();
        $this->mailServiceMock->shouldReceive('sentEmail')->once()
            ->withArgs(function ($email, $subject, $content, $files) {
                $date = new \DateTime('now');
                $date = $date->format('Y-m-d');

                self::assertSame('dummy@mockery.com', $email);

                self::assertStringContainsStringIgnoringCase('Your dummy Export on ' . $date, $subject);

                self::assertSame([
                    'heading'            => 'dummy Export Data Failed',
                    'greeting'           => 'Hi,',
                    'content_top'        => 'Export Incomplete, an error has occur.',
                    'pre_content_before' => 'Error below were recorded:',
                    'pre_content'        => '',
                ], $content);

                self::assertIsArray($files);
                self::assertEmpty($files);

                return true;
            })
            ->andReturn(1);

        $this->loggerMock->shouldReceive('info');
        $this->loggerMock->shouldReceive('error');

        $this->command = new dummyEmpty03Class($repo, $this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);
        self::assertSame(['name' => ''], $this->command->getArguments());

        $this->inputInterfaceMock
            ->shouldReceive('getArgument')
            ->once()
            ->withArgs(function ($arg) {
                self::assertSame('email', $arg);
                return true;
            })
            ->andReturn('dummy@mockery.com');

        $this->outputInterfaceMock->shouldReceive('getVerbosity')->andReturn(OutputInterface::VERBOSITY_QUIET);
        $this->outputInterfaceMock->shouldReceive('isDecorated')->andReturn(false);
        $this->outputInterfaceMock->shouldReceive('getFormatter')->andReturn(null);
        $this->outputInterfaceMock->shouldReceive('write');

        $method = new \ReflectionMethod(AbstractConfJourDataExportCommand::class, 'execute');
        $method->setAccessible(true);
        $method->invokeArgs($this->command, [$this->inputInterfaceMock, $this->outputInterfaceMock]);
    }

    public function testClassExecuteOk()
    {
        $repo = M::mock(\Doctrine\ORM\EntityRepository::class);
        $repo->shouldReceive('searchResultCount')
            ->once()
            ->withArgs(function ($configs) {
                self::assertSame([
                    'name' => '',
                ], $configs);
                return true;
            })
            ->andReturn(4569);

        $loop = 0;
        $repo->shouldReceive('search')
            ->withArgs(function ($configs, $limit, $offset) use (&$loop) {
                self::assertSame([
                    'name' => '',
                ], $configs);

                self::assertSame(1000, $limit);

                self::assertSame(($loop * 1000), $offset);

                $loop++;
                return true;
            })
            ->andReturn([]);

        $this->mailServiceMock->shouldReceive('setTemplate')
            ->once()
            ->andReturnSelf();
        $this->mailServiceMock->shouldReceive('sentEmail')->once()
            ->withArgs(function ($email, $subject, $content, $files) {
                $date = new \DateTime('now');
                $date = $date->format('Y-m-d');

                self::assertSame('dummy@mockery.com', $email);

                self::assertStringContainsStringIgnoringCase('Your dummy Export on ' . $date, $subject);

                self::assertIsArray($content);
                self::assertArrayHasKey('heading', $content);
                self::assertSame('dummy Export Data Attached', $content['heading']);

                self::assertArrayHasKey('greeting', $content);
                self::assertSame('Hi,', $content['greeting']);

                self::assertArrayHasKey('content_top', $content);
                self::assertSame('Export complete, file attached.', $content['content_top']);

                self::assertArrayHasKey('pre_content_before', $content);
                self::assertSame('Recorded process:', $content['pre_content_before']);

                self::assertArrayHasKey('pre_content', $content);
                self::assertStringContainsStringIgnoringCase('COMMAND: Exporting dummy Data', $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('dummy@mockery.com', $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('- name', $content['pre_content']);
                self::assertStringContainsStringIgnoringCase($date, $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('Export complete.', $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('Export Size:', $content['pre_content']);

                self::assertIsArray($files);
                self::assertNotEmpty($files);
                self::assertStringContainsStringIgnoringCase('DrugDb-dummy-export-data-' . $date, $files[0]);
                self::assertStringContainsStringIgnoringCase('.csv', $files[0]);

                return true;
            })
            ->andReturn(1);

        $this->loggerMock->shouldReceive('info');

        $this->command = new dummyEmpty03Class($repo, $this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);

        $this->inputInterfaceMock
            ->shouldReceive('getArgument')
            ->once()
            ->withArgs(function ($arg) {
                self::assertSame('email', $arg);
                return true;
            })
            ->andReturn('dummy@mockery.com');

        $this->outputInterfaceMock->shouldReceive('getVerbosity')->andReturn(OutputInterface::VERBOSITY_VERY_VERBOSE);
        $this->outputInterfaceMock->shouldReceive('isDecorated')->andReturn(false);
        $this->outputInterfaceMock->shouldReceive('getFormatter')->andReturn(null);
        $this->outputInterfaceMock
            ->shouldReceive('write')
            ->once()
            ->withArgs(function ($buffered) {
                self::assertTrue(is_string($buffered));
                return true;
            });

        $method = new \ReflectionMethod(AbstractConfJourDataExportCommand::class, 'execute');
        $method->setAccessible(true);
        $method->invokeArgs($this->command, [$this->inputInterfaceMock, $this->outputInterfaceMock]);
    }
}
