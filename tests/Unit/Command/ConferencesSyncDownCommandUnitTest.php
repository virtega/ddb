<?php

namespace App\Tests\Unit\Command;

use App\Command\ConferencesSyncDownCommand;
use App\Service\ConferenceSourceService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Command | ConferencesSyncDownCommand
 */
class ConferencesSyncDownCommandUnitTest extends WebTestCase
{
    /**
     * @var ConferencesSyncDownCommand
     */
    private $command;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        self::bootKernel();

        $this->command = new ConferencesSyncDownCommand(
            self::$container->get('kernel'),
            self::$container->get('doctrine.orm.default_entity_manager'),
            self::$container->get('monolog.logger'),
            self::$container->get(ConferenceSourceService::class)
        );
    }

    /**
     * @testdox Checking Conference Welcome Db Table info
     */
    public function testGetWelcomeTable()
    {
        $method = new \ReflectionMethod(ConferencesSyncDownCommand::class, 'getWelcomeTablesInfo');
        $method->setAccessible(true);

        $info = $method->invoke($this->command);
        $this->assertTrue(is_array($info));
        $this->assertEquals(2, count($info));
    }

    /**
     * @testdox Checking Conference Entity Name
     */
    public function testGetReadbleEntityName()
    {
        $method = new \ReflectionMethod(ConferencesSyncDownCommand::class, 'getReadbleEntityName');
        $method->setAccessible(true);

        $name = $method->invoke($this->command);
        $this->assertSame('Conferences', $name);

        $name = $method->invokeArgs ($this->command, [true]);
        $this->assertSame('Conferences', $name);

        $name = $method->invokeArgs ($this->command, [false]);
        $this->assertSame('Conference', $name);
    }

    /**
     * @testdox Checking Conference Db Info
     */
    public function testGetRemoteDbInfo()
    {
        $method = new \ReflectionMethod(ConferencesSyncDownCommand::class, 'getRemoteDbInfo');
        $method->setAccessible(true);

        $info = $method->invoke($this->command);
        $this->assertTrue(is_array($info));

        $this->assertArrayHasKey('dsn', $info);
        $this->assertArrayHasKey('host', $info);
        $this->assertArrayHasKey('port', $info);
        $this->assertArrayHasKey('dbname', $info);
        $this->assertArrayHasKey('options', $info);
        $this->assertArrayHasKey('version', $info['options']);
        $this->assertArrayHasKey('charset', $info['options']);
        $this->assertArrayHasKey('username', $info);
        $this->assertArrayHasKey('password', $info);
        $this->assertArrayHasKey('connected', $info);
        $this->assertArrayHasKey('error', $info);
        $this->assertArrayHasKey('error-msg', $info);
    }
}
