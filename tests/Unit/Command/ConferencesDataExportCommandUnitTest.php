<?php

namespace App\Tests\Unit\Command;

use App\Command\ConferencesDataExportCommand;
use App\Entity\Conference;
use App\Entity\Country;
use App\Entity\CountryTaxonomy;
use App\Entity\SpecialtyTaxonomy;
use App\Repository\ConferenceRepository;
use App\Service\MailService;
use Doctrine\ORM\EntityManagerInterface;
use Mockery as M;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @testdox UNIT | Command | ConferencesDataExportCommand
 */
class ConferencesDataExportCommandUnitTest extends TestCase
{
    /**
     * @var Mockery|EntityManagerInterface
     */
    private $entityManagerInterfaceMock;

    /**
     * @var Mockery|ConferenceRepository
     */
    private $repositoryMock;

    /**
     * @var Mockery|\Doctrine\ORM\EntityRepository<CountryTaxonomy>
     */
    private $repoRegionMock;

    /**
     * @var Mockery|\Doctrine\ORM\EntityRepository<SpecialtyTaxonomy>
     */
    private $repoSpecialtyMock;

    /**
     * @var Mockery|MailService
     */
    private $mailServiceMock;

    /**
     * @var Mockery|LoggerInterface
     */
    private $loggerMock;

    /**
     * @var Mockery|InputInterface
     */
    private $inputInterfaceMock;

    /**
     * @var Mockery|OutputInterface
     */
    private $outputInterfaceMock;

    /**
     * @var ConferencesDataExportCommand
     */
    private $command;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->entityManagerInterfaceMock = M::mock(EntityManagerInterface::class);
        $this->repositoryMock             = M::mock(ConferenceRepository::class);
        $this->repoRegionMock             = M::mock(\Doctrine\ORM\EntityRepository::class);
        $this->repoSpecialtyMock          = M::mock(\Doctrine\ORM\EntityRepository::class);
        $this->mailServiceMock            = M::mock(MailService::class);
        $this->loggerMock                 = M::mock(LoggerInterface::class);
        $this->inputInterfaceMock         = M::mock(InputInterface::class);
        $this->outputInterfaceMock        = M::mock(OutputInterface::class);

        $this->entityManagerInterfaceMock
            ->shouldReceive('getRepository')
            ->once()->ordered()
            ->withArgs(function ($class) {
                self::assertSame(Conference::class, $class);
                return true;
            })
            ->andReturn($this->repositoryMock);

        $this->entityManagerInterfaceMock
            ->shouldReceive('getRepository')
            ->once()->ordered()
            ->withArgs(function ($class) {
                self::assertSame(CountryTaxonomy::class, $class);
                return true;
            })
            ->andReturn($this->repoRegionMock);

        $this->entityManagerInterfaceMock
            ->shouldReceive('getRepository')
            ->once()->ordered()
            ->withArgs(function ($class) {
                self::assertSame(SpecialtyTaxonomy::class, $class);
                return true;
            })
            ->andReturn($this->repoSpecialtyMock);
    }

    public function test__construct()
    {
        $this->command = new ConferencesDataExportCommand($this->entityManagerInterfaceMock, $this->mailServiceMock, $this->loggerMock);

        self::assertNotNull($this->command);
        self::assertSame('Conferences', $this->command->getType());
    }

    public function testClassExecuteWithFailedInputToConfig()
    {
        $this->repositoryMock->shouldReceive('searchResultCount')
            ->once()
            ->andReturn(1);

        $this->loggerMock->shouldReceive('error')->withArgs(function ($arg) {
                self::assertSame('Invalid \'name\' multiple, expecting single value only.', $arg);
                return true;
            });

        $this->command = new ConferencesDataExportCommand($this->entityManagerInterfaceMock, $this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);

        $this->inputInterfaceMock->shouldReceive('getArgument')
            ->withArgs(function ($arg) {
                $found = array_search($arg, [
                    'email',
                    'name',
                ], true);

                $found = ($found !== false);
                self::assertTrue($found);

                return $found;
            })
            ->andReturnValues([
                'dummy@mockery.com',
                ['name1', 'name2'],
            ]);

        $this->outputInterfaceMock->shouldReceive('getVerbosity')->andReturn(OutputInterface::VERBOSITY_VERY_VERBOSE);
        $this->outputInterfaceMock->shouldReceive('isDecorated')->andReturn(false);
        $this->outputInterfaceMock->shouldReceive('getFormatter')->andReturn(null);

        $method = new \ReflectionMethod(ConferencesDataExportCommand::class, 'execute');
        $method->setAccessible(true);
        $result = $method->invokeArgs($this->command, [$this->inputInterfaceMock, $this->outputInterfaceMock]);

        self::assertSame(1, $result);
    }

    public function testClassExecuteWithInvalidDateInputToConfig()
    {
        $this->repositoryMock->shouldReceive('searchResultCount')
            ->once()
            ->andReturn(1);

        $this->loggerMock->shouldReceive('error')->withArgs(function ($arg) {
                self::assertSame('Invalid \'enddate\', expecting date format YYYY-MM-DD.', $arg);
                return true;
            });

        $this->command = new ConferencesDataExportCommand($this->entityManagerInterfaceMock, $this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);

        $this->inputInterfaceMock->shouldReceive('getArgument')
            ->withArgs(function ($arg) {
                $found = array_search($arg, [
                    'email',
                    'name',
                    'city',
                    'country',
                    'region',
                    'specialty',
                    'startdate',
                    'enddate',
                ], true);

                $found = ($found !== false);
                self::assertTrue($found);

                return $found;
            })
            ->andReturnValues([
                'dummy@mockery.com',
                '',
                '',
                '',
                '',
                '',
                '2019-05-12',
                '12 May 2019',
            ]);

        $this->outputInterfaceMock->shouldReceive('getVerbosity')->andReturn(OutputInterface::VERBOSITY_VERY_VERBOSE);
        $this->outputInterfaceMock->shouldReceive('isDecorated')->andReturn(false);
        $this->outputInterfaceMock->shouldReceive('getFormatter')->andReturn(null);

        $method = new \ReflectionMethod(ConferencesDataExportCommand::class, 'execute');
        $method->setAccessible(true);
        $result = $method->invokeArgs($this->command, [$this->inputInterfaceMock, $this->outputInterfaceMock]);

        self::assertSame(1, $result);
    }

    public function testClassExecuteOk()
    {
        $receivedConfigs = [
            'name'          => 'Oncology',
            'city'          => ['Cleveland'],
            'country'       => ['US', 'MY'],
            'region'        => ['300'],
            'specialty'     => ['1', '2', '3', 'new specialty'],
            'startdate'     => '',
            'enddate'       => '2030-12-03',
            'ids'           => '',
            'keycongress'   => 'yes',
            'oncruise'      => '',
            'online'        => 'no',
            'hasguide'      => '',
            'discontinued'  => 'no',
            'showcorrupted' => 'yes',
        ];

        $receivedConfigsReadable = [
            'region'    => 'North America (#300)',
            'specialty' => 'Oncology (#1), 2, Cardiology (#3), new specialty',
        ];

        $this->repositoryMock->shouldReceive('searchResultCount')
            ->once()
            ->withArgs(function ($configs) use ($receivedConfigs) {
                self::assertSame($receivedConfigs, $configs);
                return true;
            })
            ->andReturn(1);

        $this->repositoryMock->shouldReceive('countMaxSpecialtiesConnection')
            ->once()
            ->andReturn(10);

        $this->repoRegionMock->shouldReceive('findOneBy')
            ->once()->ordered()
            ->withArgs(function ($criteria) {
                self::assertSame(['id' => '300'], $criteria);
                return true;
            })
            ->andReturnUsing(function() {
                $region = new CountryTaxonomy;
                $region->setName('North America');
                return $region;
            });

        $this->repoSpecialtyMock->shouldReceive('findOneBy')
            ->once()->ordered()
            ->withArgs(function ($criteria) {
                self::assertSame(['id' => '1'], $criteria);
                return true;
            })
            ->andReturnUsing(function() {
                $specialty = new SpecialtyTaxonomy;
                $specialty->setSpecialty('Oncology');
                return $specialty;
            });

        $this->repoSpecialtyMock->shouldReceive('findOneBy')
            ->once()->ordered()
            ->withArgs(function ($criteria) {
                self::assertSame(['id' => '2'], $criteria);
                return true;
            })
            ->andReturn(null);

        $this->repoSpecialtyMock->shouldReceive('findOneBy')
            ->once()->ordered()
            ->withArgs(function ($criteria) {
                self::assertSame(['id' => '3'], $criteria);
                return true;
            })
            ->andReturnUsing(function() {
                $specialty = new SpecialtyTaxonomy;
                $specialty->setSpecialty('Cardiology');
                return $specialty;
            });

        $generalDate = new \DateTime('now');

        $country = new \App\Entity\Country;
        $country
            ->setName('Denmark')
            ->setCapitalCity('Copenhagen');

        $region = new \App\Entity\CountryTaxonomy;
        $region->setName('Western Europe');

        $conference = new Conference();
        $conference
            ->setName('Oncology Experts Weekends')

            ->setStartDate(new \DateTime('2020-02-22'))
            ->setEndDate(null)

            ->setCity('1718 København')
            ->setCityGuide('Next to Cafe Sommersted')
            ->setDetailsState('Køpmannæhafn')
            ->setCountry($country)
            ->setRegion($region)

            ->setPreviousId('C37C3E5F8842C7AD8525770B006848B9')
            ->setUniqueName('NT003463CS')

            ->setCruise(0)
            ->setOnline(0)
            ->setKeyEvent(1)
            ->setHasGuide(1)
            ->setDiscontinued(0)

            ->setDetailsContactDesc('Call Mick he will explains about Cabbage')
            ->setDetailsContactPhones(['+123-123-456-789', '+123-123-456-790'])
            ->setDetailsContactFaxes(['+123-123-456-791'])
            ->setDetailsContactEmails(['mick.dagger@digger.com', 'bit@digger.com'])
            ->setDetailsContactLinks([
                [
                    'url'     => 'http://google1.com',
                    'caption' => 'Google 1',
                ],
                [
                    'url'     => 'http://google2.com',
                ],
                [
                    'caption' => 'Google 3',
                ],
            ])

            ->setDetailsMetaCreatedOn($generalDate)
            ->setDetailsMetaCreatedBy('mick.dagger')
            ->setDetailsMetaModifiedOn($generalDate)
            ->setDetailsMetaModifiedBy('mick.dagger')

            ->addSpecialty($this->createConferenceSpecialties('Specialty 65'))
            ->addSpecialty($this->createConferenceSpecialties('Specialty 59'))
            ->addSpecialty($this->createConferenceSpecialties('Specialty 81'))
            ;

        $this->repositoryMock->shouldReceive('search')
            ->withArgs(function ($configs, $limit, $offset) use ($receivedConfigs) {
                self::assertSame($receivedConfigs, $configs);

                self::assertSame(1000, $limit);

                self::assertSame(0, $offset);
                return true;
            })
            ->andReturn([$conference]);

        $this->mailServiceMock->shouldReceive('setTemplate')
            ->once()
            ->andReturnSelf();
        $this->mailServiceMock->shouldReceive('sentEmail')->once()
            ->withArgs(function ($email, $subject, $content, $files) use ($receivedConfigs, $receivedConfigsReadable, $generalDate) {
                $date = new \DateTime('now');
                $date = $date->format('Y-m-d');

                self::assertSame('dummy@mockery.com', $email);

                self::assertStringContainsStringIgnoringCase('Your Conferences Export on ' . $date, $subject);

                self::assertIsArray($content);
                self::assertArrayHasKey('heading', $content);
                self::assertSame('Conferences Export Data Attached', $content['heading']);

                self::assertArrayHasKey('greeting', $content);
                self::assertSame('Hi,', $content['greeting']);

                self::assertArrayHasKey('content_top', $content);
                self::assertSame('Export complete, file attached.', $content['content_top']);

                self::assertArrayHasKey('pre_content_before', $content);
                self::assertSame('Recorded process:', $content['pre_content_before']);

                self::assertArrayHasKey('pre_content', $content);
                self::assertStringContainsStringIgnoringCase('COMMAND: Exporting Conferences Data', $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('dummy@mockery.com', $content['pre_content']);
                foreach ($receivedConfigs as $receivedConfigKey => $receivedConfigValue) {
                    self::assertStringContainsStringIgnoringCase('- ' . $receivedConfigKey, $content['pre_content']);

                    if (isset($receivedConfigsReadable[$receivedConfigKey])) {
                        self::assertStringContainsStringIgnoringCase($receivedConfigsReadable[$receivedConfigKey], $content['pre_content']);
                        continue;
                    }

                    if (!empty($receivedConfigValue)) {
                        if (is_array($receivedConfigValue)) {
                            self::assertStringContainsStringIgnoringCase('| ' . implode(', ', $receivedConfigValue), $content['pre_content']);
                        }
                        else {
                            self::assertStringContainsStringIgnoringCase('| ' . $receivedConfigValue, $content['pre_content']);
                        }
                    }
                }

                self::assertStringContainsStringIgnoringCase($date, $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('Export complete.', $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('Export Size:', $content['pre_content']);

                self::assertIsArray($files);
                self::assertNotEmpty($files);
                self::assertStringContainsStringIgnoringCase('DrugDb-Conferences-export-data-' . $date, $files[0]);
                self::assertStringContainsStringIgnoringCase('.csv', $files[0]);

                $csv = file_get_contents($files[0]);
                $csv = explode(PHP_EOL, $csv);
                $csv = array_filter($csv);
                $csv = array_map('str_getcsv', $csv);
                self::assertSame(2, count($csv));

                // BOM removal
                $csv[0] = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $csv[0]);

                self::assertSame(36, count($csv[0]));
                self::assertEquals([
                    'Id',
                    'Name',
                    'Start Date',
                    'End Date',
                    'City',
                    'City Guide',
                    'State',
                    'Country',
                    'Region',
                    'Previous ID',
                    'Unique Name',
                    'Is Cruise',
                    'Is Online',
                    'Is Key Event',
                    'Is Has Guide',
                    'Is Discontinued',
                    'Contact: Details Desc',
                    'Contact: Phones',
                    'Contact: Faxes',
                    'Contact: Emails',
                    'Contact: Links',
                    'Meta: Created on',
                    'Meta: Created by',
                    'Meta: Modified on',
                    'Meta: Modified by',
                    'Specialty Count',
                    'Specialty 1',
                    'Specialty 2',
                    'Specialty 3',
                    'Specialty 4',
                    'Specialty 5',
                    'Specialty 6',
                    'Specialty 7',
                    'Specialty 8',
                    'Specialty 9',
                    'Specialty 10',
                ], $csv[0]);

                $generalDate = $generalDate->format('Y-m-d H:i:s');
                self::assertSame(29, count($csv[1]));
                self::assertEquals([
                    '',
                    'Oncology Experts Weekends',
                    '2020-02-22',
                    '',
                    '1718 København',
                    'Next to Cafe Sommersted',
                    'Køpmannæhafn',
                    'Denmark',
                    'Western Europe',
                    'C37C3E5F8842C7AD8525770B006848B9',
                    'NT003463CS',
                    'No',
                    'No',
                    'Yes',
                    'Yes',
                    'No',
                    'Call Mick he will explains about Cabbage',
                    '+123-123-456-789; +123-123-456-790',
                    '+123-123-456-791',
                    'mick.dagger@digger.com; bit@digger.com',
                    '[Google 1](http://google1.com); [Unknown](http://google2.com); [Google 3]()',
                    $generalDate,
                    'mick.dagger',
                    $generalDate,
                    'mick.dagger',
                    '3',
                    'Specialty 65',
                    'Specialty 59',
                    'Specialty 81',
                ], $csv[1]);

                return true;
            })
            ->andReturn(1);

        $this->loggerMock->shouldReceive('info');

        $this->command = new ConferencesDataExportCommand($this->entityManagerInterfaceMock, $this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);

        $this->inputInterfaceMock->shouldReceive('getArgument')
            ->withArgs(function ($arg) {
                $found = array_search($arg, [
                    'email',
                    'name',
                    'city',
                    'country',
                    'region',
                    'specialty',
                    'startdate',
                    'enddate',
                    'ids',
                    'keycongress',
                    'oncruise',
                    'online',
                    'hasguide',
                    'discontinued',
                    'showcorrupted',
                ], true);

                $found = ($found !== false);
                self::assertTrue($found);

                return $found;
            })
            ->andReturnValues([
                'dummy@mockery.com',
                'Oncology',
                'Cleveland',
                'US , MY',
                '300',
                '1, 2, 3, , new specialty ',
                '',
                '2030-12-03',
                '',
                'yes',
                '',
                'no',
                '',
                'no',
                'yes',
            ]);

        $this->outputInterfaceMock->shouldReceive('getVerbosity')->andReturn(OutputInterface::VERBOSITY_VERY_VERBOSE);
        $this->outputInterfaceMock->shouldReceive('isDecorated')->andReturn(false);
        $this->outputInterfaceMock->shouldReceive('getFormatter')->andReturn(null);
        $this->outputInterfaceMock
            ->shouldReceive('write')
            ->once()
            ->withArgs(function ($buffered) {
                self::assertTrue(is_string($buffered));
                return true;
            });

        $method = new \ReflectionMethod(ConferencesDataExportCommand::class, 'execute');
        $method->setAccessible(true);
        $method->invokeArgs($this->command, [$this->inputInterfaceMock, $this->outputInterfaceMock]);
    }

    private function createConferenceSpecialties(string $specialtyName): \App\Entity\ConferenceSpecialties
    {
        $specialtyTaxonomy = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomy->setSpecialty($specialtyName);

        $conferenceSpecialties = new \App\Entity\ConferenceSpecialties;
        $conferenceSpecialties->setSpecialty($specialtyTaxonomy);

        return $conferenceSpecialties;
    }
}
