<?php

namespace App\Tests\Unit\Command\Traits;

use App\Command\Traits\SourceImportSanitizerCommandsTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Command-Traits | SourceImportSanitizerCommandsTrait
 */
class SourceImportSanitizerCommandsTraitUnitTest extends WebTestCase
{
    use SourceImportSanitizerCommandsTrait;

    /**
     * @var Mocked EntityManager|null
     */
    private $em;

    /**
     * @dataProvider getTrimStringStacksProvider
     *
     * @testdox Checking __trimString() Stacks
     */
    public function test__trimStringStacks($input, $expected)
    {
        $result = $this->__trimString($input);

        $this->assertSame($expected, $result);
    }

    public function getTrimStringStacksProvider()
    {
        return [
            // #0
            ['    ', ''],
            // #1
            [null, null],
            // #2
            ['asd ', 'asd'],
            // #3
            ['123', '123'],
            // #4
            [123, '123'],
        ];
    }

    /**
     * @dataProvider getSafeStringStacksProvider
     *
     * @testdox Checking __safeString() Stacks
     */
    public function test__safeStringStacks($input, $expected)
    {
        $result = $this->__safeString($input);

        $this->assertSame($expected, $result);
    }

    public function getSafeStringStacksProvider()
    {
        return [
            // #0
            ['    ', ''],
            // #1
            [null, null],
            // #2
            ['asd ', 'asd'],
            // #3
            ['123', '123'],
            // #4
            [123, '123'],
            // #5
            ['"test"', 'test'],
            // #6
            ['A \'quote\' "is" <b>bold</b>', 'A \'quote\' is <b>bold</b>'],
            // #7
            ['"t/e/s[\t"', 't/e/s[\t'],
            // #8
            ['the `back tick`', 'the back tick'],
        ];
    }

    /**
     * @dataProvider getConvertDateStacksProvider
     *
     * @testdox Checking __convertDate() Stacks
     */
    public function test__convertDateStacks($input, $format, $expected)
    {
        $result = $this->__convertDate($input, $format);

        if (null === $expected) {
            $this->assertSame($expected, $result);
        }
        else {
            $this->assertSame($expected, $result->format('Y-m-d H:i:s'));
        }
    }

    public function getConvertDateStacksProvider()
    {
        return [
            // #0
            [
                '    ',
                'Y-m-d H:i:s',
                null,
            ],
            // #1
            [
                null,
                'Y-m-d H:i:s',
                null,
            ],
            // #2
            [
                'asd ',
                'Y-m-d H:i:s',
                null,
            ],
            // #3
            [
                '1985-06-04 16:45:32',
                'Y-m-d H:i:s',
                '1985-06-04 16:45:32',
            ],
            // #4
            [
                '6/4/1985 08:35:59',
                'm/d/Y H:i:s',
                '1985-06-04 08:35:59',
            ],
        ];
    }

    /**
     * @dataProvider getLookUpCountryIsoCodeStacksProvider
     *
     * @testdox Checking __lookUpCountryIsoCode() Stacks
     */
    public function test___lookUpCountryIsoCode($query, $findOneByResults, $expected)
    {
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->method('findOneBy')
            ->will($this->returnValue($findOneByResults));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        try {
            $result = $this->__lookUpCountryIsoCode($query);
            $this->assertSame($expected, $result);
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\CommandSourceImportSanitizerException::class, $e);
            $this->assertStringContainsStringIgnoringCase('Invalid country code: \'' . $expected . '\'', $e->getMessage());
        }
    }

    public function getLookUpCountryIsoCodeStacksProvider()
    {
        $country = new \App\Entity\Country;
        $country->setName('Malaysia');
        $country->setCapitalCity('Kuala Lumpur');

        return [
            // #0
            ['Error', [], null],
            // #1
            ['   ', [], null],
            // #2
            [null, [], null],
            // #3 - alias
            ['KO', [], 'KR'],
            // #4
            ['MY', [], 'MY'],
            // #5
            ['MY', $country, $country],
        ];
    }

    /**
     * @dataProvider getLookUpCountryNameStacksProvider
     *
     * @testdox Checking __lookUpCountryName() Stacks
     */
    public function test___lookUpCountryName($query, $findOneByResults, $expected)
    {
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->method('findOneBy')
            ->will($this->returnValue($findOneByResults));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        try {
            $result = $this->__lookUpCountryName($query);
            $this->assertSame($expected, $result);
        }
        catch (\Exception $e) {
            $this->assertInstanceOf(\App\Exception\CommandSourceImportSanitizerException::class, $e);
            $this->assertStringContainsStringIgnoringCase('Invalid country name: \'' . $expected . '\'', $e->getMessage());
        }
    }

    public function getLookUpCountryNameStacksProvider()
    {
        $country = new \App\Entity\Country;
        $country->setName('Malaysia');
        $country->setCapitalCity('Kuala Lumpur');

        return [
            // #0
            ['Error', [], null],
            // #1
            ['   ', [], null],
            // #2
            [null, [], null],
            // #3 - alias
            ['Korea (South)', [], 'South Korea'],
            // #4
            ['Dynasty', [], 'Dynasty'],
            // #5
            ['Malaysia', $country, $country],
        ];
    }

    /**
     * @dataProvider getLookUpRegionNullifyStacksProvider
     *
     * @testdox Checking __lookUpRegion() Null Result Stacks
     */
    public function test__lookUpRegionNullifyStacks($country, $regionStr, $fresh, $expected)
    {
        $result = $this->__lookUpRegion($country, $regionStr, $fresh);

        $this->assertSame($expected, $result);
    }

    public function getLookUpRegionNullifyStacksProvider()
    {
        return [
            // #0
            [
                null,
                null,
                true,
                null
            ],
            // #1
            [
                null,
                '    ',
                true,
                null
            ],
            // #2
            [
                null,
                'Error',
                true,
                null
            ],
            // #3
            [
                null,
                'Cruises',
                false,
                null
            ],
            // #4
            [
                null,
                'Cruises',
                true,
                null
            ],
        ];
    }

    /**
     * @dataProvider getLookUpRegionStacksProvider
     *
     * @testdox Checking __lookUpRegion() Stacks
     */
    public function test__lookUpRegionStacks($country, $regionStr, $countryTaxonomy, $regionTaxonomy, $expected)
    {
        $ts = 1;
        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);

        $countryTax = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $countryTax->method('findOneBy')->will($this->returnValue($countryTaxonomy));
        $this->em->expects($this->at(0))->method('getRepository')->will($this->returnValue($countryTax));
        $ts++;

        if ($country && $countryTaxonomy) {
            $cRegions = $this->createMock(\Doctrine\ORM\EntityRepository::class);
            $cRegions->method('findOneBy')->will($this->returnValue($regionTaxonomy));
            $this->em->expects($this->at(1))->method('getRepository')->will($this->returnValue($cRegions));
            $ts++;

            if ($regionTaxonomy) {
                $this->em->method('persist')->will($this->returnValue(true));
                $ts++;
            }
        }

        try {
            $result = $this->__lookUpRegion($country, $regionStr, true);
            $ts++;
            $this->assertTrue($expected[0]);
            $this->assertInstanceOf(\App\Entity\CountryTaxonomy::class, $result);
        }
        catch (\Exception $e) {
            $this->assertFalse($expected[0]);
        }

        $this->assertSame($expected[1], $ts);
    }

    public function getLookUpRegionStacksProvider()
    {
        $country         = new \App\Entity\Country;
        $countryTaxonomy = new \App\Entity\CountryTaxonomy;
        $regionTaxonomy  = new \App\Entity\CountryRegions;

        return [
            // #0 - Search & Create
            [
                $country,
                'Example 1',
                $countryTaxonomy,
                $regionTaxonomy,
                [true, 5],
            ],
            // #1 - Search only
            [
                $country,
                'Example 2',
                $countryTaxonomy,
                null,
                [true, 4],
            ],
            // #2
            [
                null,
                'Example 2',
                $countryTaxonomy,
                null,
                [true, 3],
            ],
            // #3
            [
                null,
                'Example 2',
                null,
                null,
                [false, 2],
            ],
            // #4
            [
                $country,
                'Example 2',
                null,
                null,
                [false, 2],
            ],
        ];
    }

    /**
     * @dataProvider getLookFilterIdsStacksProvider
     *
     * @testdox Checking __lookFilterIds() Stacks
     */
    public function test__lookFilterIdsStacks($count, $ids, $expected, $error = null, $errorMsg = null)
    {
        try {
            $result = $this->__lookFilterIds($count, $ids);
        }
        catch (\Exception $e) {
            $this->assertNull($expected);
            $this->assertInstanceOf($error, $e);
            $this->assertStringContainsStringIgnoringCase($errorMsg, $e->getMessage());

            return;
        }

        $this->assertSame($expected, $result);
    }

    public function getLookFilterIdsStacksProvider()
    {
        return [
            // #0
            [
                2,
                [],
                null,
                \App\Exception\CommandSourceImportSanitizerException::class,
                'Has no relevant IDs',
            ],
            // #1
            [
                2,
                [0, 1, 1, 2, 3],
                null,
                \App\Exception\CommandSourceImportSanitizerException::class,
                'Has too many IDs: ["1","2","3"]',
            ],
            // #2
            [
                2,
                [1, 2, 2, 1],
                ['1', '2'],
            ],
            // #3
            [
                2,
                [1, null, 0, false, 2],
                ['1', '2'],
            ],
            // #4
            [
                2,
                [1, null, 0, false, 0],
                ['1', null],
            ],
            // #5
            [
                3,
                [1, null, 0, false, 2],
                ['1', '2', null],
            ],
        ];
    }

    /**
     * @dataProvider getConvertIntFromYesStringStacksProvider
     *
     * @testdox Checking __convertIntFromYesString() Stacks
     */
    public function test___convertIntFromYesStringStacks($input, $expected)
    {
        $result = $this->__convertIntFromYesString($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertIntFromYesStringStacksProvider()
    {
        return [
            // #0
            ['    ', 0],
            // #1
            [null, 0],
            // #2
            ['Y', 0],
            // #3
            ['YES', 1],
            // #4
            ['Yes', 1],
            // #5
            ['No', 0],
            // #6
            ['Other', 0],
        ];
    }

    /**
     * @dataProvider getConvertIntFromBitIStacksProvider
     *
     * @testdox Checking __convertIntFromBitI() Stacks
     */
    public function test__convertIntFromBitIStacks($input, $expected)
    {
        $result = $this->__convertIntFromBitI($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertIntFromBitIStacksProvider()
    {
        return [
            // #0
            ['    ', 0],
            // #1
            [null, 0],
            // #2
            ['Y', 0],
            // #3
            ['YES', 0],
            // #4
            ['I', 1],
            // #5
            ['i', 1],
            // #6
            ['x', 0],
        ];
    }

    /**
     * @dataProvider getConvertIntFromBitYNStacksProvider
     *
     * @testdox Checking __convertIntFromBitYN() Stacks
     */
    public function test__convertIntFromBitYNStacks($input, $expected)
    {
        $result = $this->__convertIntFromBitYN($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertIntFromBitYNStacksProvider()
    {
        return [
            // #0
            ['    ', 0],
            // #1
            [null, 0],
            // #2
            ['Y', 1],
            // #3
            ['YES', 1],
            // #4
            ['I', 0],
            // #5
            ['i', 0],
            // #6
            ['x', 0],
            // #7
            ['No', 0],
            // #8
            ['N', 0],
        ];
    }

    /**
     * @dataProvider getConvertIntFromBitYNStacksProvider
     *
     * @testdox Checking __convertBoolFromBitYN() Stacks
     */
    public function test__convertBoolFromBitYNStacks($input, $expected)
    {
        $result = $this->__convertBoolFromBitYN($input);

        $this->assertSame((!empty($expected)), $result);
    }

    /**
     * @dataProvider getConvertIntFromBitIntStacksProvider
     *
     * @testdox Checking __convertIntFromBitInt() Stacks
     */
    public function test__convertIntFromBitIntStacks($input, $expected)
    {
        $result = $this->__convertIntFromBitInt($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertIntFromBitIntStacksProvider()
    {
        return [
            // #0
            [null, 0],
            // #1
            [0, 0],
            // #2
            [1, 1],
            // #3
            [7, 1],
        ];
    }

    /**
     * @dataProvider getConvertSpecialtyStacksProvider
     *
     * @testdox Checking __convertSpecialty() Stacks
     */
    public function test__convertSpecialty($input, $expectedCount, $expected)
    {
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->method('findOneBy')->will($this->returnValue(null));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('persist')->will($this->returnValue(true));
        $this->em->method('getRepository')->will($this->returnValue($repo));

        foreach ([new \App\Entity\Conference, new \App\Entity\Journal] as $entity) {
            $this->__convertSpecialty($entity, $input);

            $specialties = $entity->getSpecialties();
            $this->assertSame($expectedCount, count($specialties));

            $names = [];
            foreach ($specialties as $si => $specialty) {
                $names[] = $specialty->getSpecialty()->getSpecialty();
            }
            $this->assertSame($expected, $names);
        }
    }

    public function getConvertSpecialtyStacksProvider()
    {
        return [
            // #0
            [
                null,
                0,
                []
            ],
            // #1
            [
                '    ',
                0,
                []
            ],
            // #2
            [
                'ABC-CEF;;ASD-234;  ; GFY-TED;',
                3,
                ['Abc-cef', 'Asd-234', 'Gfy-ted']
            ],
            // #3
            [
                'ABC-CEF;ABC-CEF;;ABC-CEG',
                2,
                ['Abc-cef', 'Abc-ceg']
            ],
            // #4
            [
                'ABC; CEF; ABC-CEF; ABC-CEG;',
                4,
                ['Abc', 'Cef', 'Abc-cef', 'Abc-ceg']
            ],
            // #5
            [
                'ABC; AIDS/HIV; HIV/AIDS; ABC-CEF; ABC-CEG;',
                4,
                ['Abc', 'HIV/AIDS', 'Abc-cef', 'Abc-ceg']
            ],
            // #6 - alias
            [
                'Gynaecology/Obstetrics; AIDS/HIV; error; Clinical pharmacology;',
                3,
                ['Gynaecology and Obstetrics', 'HIV/AIDS', 'Clinical Pharmacology']
            ],
            // #7 - case
            [
                'Hematology;Hematology and Oncology;Hematology and oncology;hematology and oncology;HEMATOLOGY AND ONCOLOGY',
                2,
                ['Hematology', 'Hematology and Oncology']
            ],
            // #8 - case 2 "and" & spaces
            [
                'A AND Z; a and z;   a andz ; andz an;zand a;aanda;;andz  an;',
                5,
                ['A and Z', 'A Andz', 'Andz An', 'Zand A', 'Aanda']
            ],
            // #9 - case 2 "and" & spaces
            [
                'Hematology;Hematology and oncology;immunology;Infectious deceases;Infectious Deceases;Intensive Care;;intensive care;Musculoskeletal and connective tissue disorders;Sexually related disorders',
                7,
                ['Hematology', 'Hematology and Oncology', 'Immunology', 'Infectious Deceases', 'Intensive Care','Musculoskeletal and Connective Tissue Disorders', 'Sexually Related Disorders']
            ],
        ];
    }

    /**
     * @dataProvider getLookUpSpecialtyCreateStacksProvider
     *
     * @testdox Checking __lookUpSpecialtyCreate() Stacks
     */
    public function test__lookUpSpecialtyCreate($specialTax, $name)
    {
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->method('findOneBy')
            ->will($this->returnValue($specialTax));

        $this->em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $this->em->method('getRepository')
            ->will($this->returnValue($repo));

        $result = $this->__lookUpSpecialtyCreate($name);

        $this->assertInstanceOf(\App\Entity\SpecialtyTaxonomy::class, $result);
        $this->assertSame($name, $result->getSpecialty());
    }

    public function getLookUpSpecialtyCreateStacksProvider()
    {
        $specialTax = new \App\Entity\SpecialtyTaxonomy;
        $specialTax->setSpecialty('ABC');

        return [
            // #0 - Search
            [
                $specialTax,
                'ABC'
            ],
            // #2 - Create
            [
                null,
                'EFG'
            ],
        ];
    }

    /**
     * @dataProvider getConvertPhonesFaxesStacksProvider
     *
     * @testdox Checking __convertPhonesFaxes() Stacks
     */
    public function test__convertPhonesFaxesStacks($input, $expected)
    {
        $result = $this->__convertPhonesFaxes($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertPhonesFaxesStacksProvider()
    {
        return [
            // #0
            [null, []],
            // #1
            ['    ', []],
            // #2
            ['', []],
            // #3
            ['  123456789   ', ['123456789']],
            // #4
            ['  123456789 / 123423  ', ['123456789', '123423']],
            // #5
            [' + 123456789 / ext 123423  ', ['+ 123456789', 'ext 123423']],
            // #6
            [' +123456789/123423/987654  ', ['+123456789', '123423', '987654']],
            // #7
            [' +123456789/123423/987654 //123423  ', ['+123456789', '123423', '987654']],
            // #8
            [' +123456789/123423a/987654 //123423b  ', ['+123456789', '123423a', '987654', '123423b']],
        ];
    }

    /**
     * @dataProvider getConvertEmailsStacksProvider
     *
     * @testdox Checking __convertEmails() Stacks
     */
    public function test__convertEmailsStacks($input, $expected)
    {
        $result = $this->__convertEmails($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertEmailsStacksProvider()
    {
        return [
            // #0
            [null, []],
            // #1
            ['    ', []],
            // #2
            ['', []],
            // #3
            ['  test@auth.com   ', ['test@auth.com']],
            // #4
            ['  test@auth.com ; @auth.com  ', ['test@auth.com', '@auth.com']],
            // #5
            ['test+dat@auth.com; test+da2t@auth.com  ', ['test+dat@auth.com', 'test+da2t@auth.com']],
            // #6
            ['   test+dat1@auth.com;  ;test+dat2@auth.com;        test+dat3@auth.com;test+dat2@auth.com', ['test+dat1@auth.com', 'test+dat2@auth.com', 'test+dat3@auth.com']],
        ];
    }

    /**
     * @dataProvider getConvertConferenceLinksStacksProvider
     *
     * @testdox Checking __convertConferenceLinks() Stacks
     */
    public function test__convertConferenceLinksStacks($input, $expected)
    {
        $result = $this->__convertConferenceLinks($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertConferenceLinksStacksProvider()
    {
        return [
            // #0
            [[], []],
            // #1
            [
                [
                    'd_CongressLink1' => 'Home   ',
                    'd_CongressLink2' => '  Register',
                    'd_CongressURL1'  => 'http://google.com?q=1',
                    'd_CongressURL2'  => 'http://google.com?q=2',
                    'd_CongressURL3'  => 'http://google.com?q=3     ',
                ],
                [
                    [
                        'caption' => 'Home',
                        'url'     => 'http://google.com?q=1'
                    ],
                    [
                        'caption' => 'Register',
                        'url'     => 'http://google.com?q=2'
                    ],
                    [
                        'caption' => 'Unknown',
                        'url'     => 'http://google.com?q=3'
                    ],
                ]
            ],
            // #2
            [
                [
                    'd_CongressLink1' => 'Home',
                    'd_CongressLink2' => 'Songs',
                    'd_CongressLink5' => 'Register    ',
                    'd_CongressLink3' => null,
                    'd_CongressLink4' => 'Letter',
                    'd_CongressURL1'  => 'http://google.com?q=1',
                    'd_CongressURL2'  => '   ',
                    'd_CongressURL3'  => null,
                    'd_CongressURL5'  => 'http://google.com?q=2    ',
                    'd_CongressURL6'  => '   http://google.com?q=3',
                ],
                [
                    [
                        'caption' => 'Home',
                        'url'     => 'http://google.com?q=1'
                    ],
                    [
                        'caption' => 'Register',
                        'url'     => 'http://google.com?q=2'
                    ],
                    [
                        'caption' => 'Unknown',
                        'url'     => 'http://google.com?q=3'
                    ],
                ]
            ],
        ];
    }

    /**
     * @dataProvider getConvertJournalStatusStacksProvider
     *
     * @testdox Checking __convertJournalStatus() Stacks
     */
    public function test__convertJournalStatusStacks($input, $expected)
    {
        $result = $this->__convertJournalStatus($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertJournalStatusStacksProvider()
    {
        return [
            // #0
            [null, 0],
            // #1
            ['   ', 0],
            // #2
            ['ACTIVE', 1],
            // #3
            ['Active', 1],
            // #4
            ['Archived', 0],
            // #5
            ['Archive', 5],
            // #6
            ['archive', 5],
            // #6
            ['Others', 0],
        ];
    }

    /**
     * @dataProvider getConvertJournalIsMedLineStacksProvider
     *
     * @testdox Checking __convertJournalIsMedLine() Stacks
     */
    public function test__convertJournalIsMedLineStacks($input, $expected)
    {
        $result = $this->__convertJournalIsMedLine($input);

        $this->assertSame($expected, $result);
    }

    public function getConvertJournalIsMedLineStacksProvider()
    {
        return [
            // #0
            [null, 0],
            // #1
            ['   ', 0],
            // #2
            ['', 0],
            // #3
            ['X123', 1],
            // #4
            ['X123-3215', 1],
            // #5
            ['Archive', 1],
        ];
    }

    /**
     * @dataProvider getSanitizeDecimalValueStacksProvider
     *
     * @testdox Checking __sanitizeDecimalValue() Stacks
     */
    public function test__sanitizeDecimalValueStacks($input, $expected)
    {
        $result = $this->__sanitizeDecimalValue($input);

        $this->assertSame($expected, $result);
    }

    public function getSanitizeDecimalValueStacksProvider()
    {
        return [
            // #0
            [null, 0.00],
            // #1
            [0, 0.00],
            // #2
            [0.00, 0.00],
            // #3
            [123, 123.0],
            // #4
            [0.366, 0.366],
            // #5
            [.335, 0.335],
        ];
    }

    /**
     * @dataProvider getSanitizeJournalPublisherStacksProvider
     *
     * @testdox Checking __sanitizeJournalPublisher() Stacks
     */
    public function test__sanitizeJournalPublisherStacks($pub, $url, $expected)
    {
        $result = $this->__sanitizeJournalPublisher($pub, $url);

        $this->assertSame($expected, $result);
    }

    public function getSanitizeJournalPublisherStacksProvider()
    {
        return [
            // #0
            [null, null, ['', '']],
            // #1
            ['     ', 'http://google.com', ['', 'http://google.com']],
            // #1
            ['Homepage', 'http://google.com', ['Homepage', 'http://google.com']],
            // #2
            ['Registry', 'https://google.com', ['Registry', 'https://google.com']],
            // #3
            ['https://google.com', null, ['google.com', 'https://google.com']],
            // #4
            ['http://paste.com', null, ['paste.com', 'http://paste.com']],
            // #5
            ['hp://cc.com/23', null, ['hp://cc.com/23', '']],
            // #6
            ['http://paste.com', 'http://copy.com', ['http://paste.com', 'http://copy.com']],
        ];
    }

    /**
     * @dataProvider getSanitizeJournalLastViewedStacksProvider
     *
     * @testdox Checking __sanitizeJournalLastViewed() Stacks
     */
    public function test__sanitizeJournalLastViewedStacks($input, $expectedResult, $expected = null)
    {
        $result = $this->__sanitizeJournalLastViewed($input);

        if (false === $expectedResult) {
            $this->assertSame([null, ''], $result);
        }
        else {
            if ($result[0] instanceof \DateTime) {
                $this->assertSame($expected[0], $result[0]->format('Y-m-d'));
            }
            else {
                $this->assertSame($expected[0], $result[0]);
            }
            $this->assertSame($expected[1], $result[1]);
        }
    }

    public function getSanitizeJournalLastViewedStacksProvider()
    {
        return [
            // #0
            [null, false],
            // #1
            ['   ', false],
            // #2
            ['3/6/99', true, ['1999-03-06', '']],
            // #3
            ['11/12/2109', true, ['2109-11-12', '']],
            // #4
            ['3/6/2019 Pizza', true, ['2019-03-06', '3/6/2019 Pizza']],
            // #5
            ['  Pizza ', true, [null, 'Pizza']],
            // #6
            ['03/06/99 Pizza, then 4/6/2019 Taco', true, ['1999-03-06', '03/06/99 Pizza, then 4/6/2019 Taco']],
            // #7
            ['03/99 just Pizza, then 4/6/2019 Taco', true, ['2019-04-06', '03/99 just Pizza, then 4/6/2019 Taco']],
        ];
    }
}
