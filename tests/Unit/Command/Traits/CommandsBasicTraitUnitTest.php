<?php

namespace App\Tests\Unit\Command\Traits;

use App\Command\Traits\CommandsBasicTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Command-Traits | CommandsBasicTrait
 */
class CommandsBasicTraitUnitTest extends WebTestCase
{
    use CommandsBasicTrait;

    /**
     * Simulate \Symfony\Component\Console\Command\Command::$application
     *
     * @var  \Symfony\Component\Console\Application|null
     */
    private $application;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->logger  = $this->createMock(\Psr\Log\LoggerInterface::class);
        $this->cInput  = $this->createMock(\Symfony\Component\Console\Input\InputInterface::class);
        $this->cOutput = $this->createMock(\Symfony\Component\Console\Output\OutputInterface::class);
    }

    /**
     * @testdox Checking _markCommandStarts()
     */
    public function testChecking_markCommandStarts()
    {
        $this->assertSame(0, $this->timedStart);

        $this->_markCommandStarts();

        $this->assertNotSame(0, $this->timedStart);

        $this->assertTrue(($this->timedStart > 0));

        sleep(1);
        $this->assertTrue(time() > $this->timedStart);
    }

    /**
     * @testdox Checking _markCommandEnds() Completed
     */
    public function testChecking_markCommandEndsCompleted()
    {
        $this->timedStart = (time() - 300);

        $this->cOutput->expects($this->at(0))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertSame(['',''], $messages);
            }));

        $this->cOutput->expects($this->at(1))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertStringContainsStringIgnoringCase('Total Execution Time: 5 minutes', $messages);
            }));

        $this->cOutput->expects($this->at(2))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertStringContainsStringIgnoringCase('Command End', $messages);
            }));

        $this->_markCommandEnds(true);
    }

    /**
     * @testdox Checking _markCommandEnds() Not Completed
     */
    public function testChecking_markCommandEndsIncompleted()
    {
        $this->timedStart = (time() - 300);

        $this->cOutput->expects($this->at(0))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertSame(['',''], $messages);
            }));

        $this->cOutput->expects($this->at(1))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertStringContainsStringIgnoringCase('But nothing were processed', $messages);
            }));

        $this->cOutput->expects($this->at(2))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertStringContainsStringIgnoringCase('Command End', $messages);
            }));

        $this->_markCommandEnds(false);
    }

    /**
     * @testdox Checking _setWelcome()
     */
    public function testChecking_setWelcome()
    {
        $this->timedStart = (time() - 300);

        $date = new \DateTime();
        $date = $date->setTimestamp($this->timedStart);

        $set = [
            'Testing Command Heading',
            ['attr 1', 'value A'],
            'string row here',
            ['attr 2', 'value B'],
        ];

        $this->cOutput->expects($this->exactly(26))->method('getFormatter')
            ->will($this->returnCallback(function() {
                return new \Symfony\Component\Console\Formatter\OutputFormatter;
            }));

        $expected = [
            '',
            '+---------------------+---------------------+',
            '|<info>  ==> COMMAND: Testing Command Heading     </info>|',
            '+---------------------+---------------------+',
            '| attr 1              | value A             |',
            '| string row here     |                     |',
            '| attr 2              | value B             |',
            '| Received            | ' . $date->format('Y-m-d H:i:s') . ' |',
            '| DrugDb Ver          | ' . \App\Service\CoreService::VERSION . '                 |',
            '+---------------------+---------------------+',
            '',
        ];

        $this->cOutput->expects($this->exactly( count($expected) ))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) use (&$expected) {
                $index = array_search($messages, $expected, true);

                $this->assertNotFalse($index);
                $this->assertArrayHasKey($index, $expected);
                $this->assertSame($expected[$index], $messages);

                unset($expected[$index]);
            }));

        $this->_setWelcome($set);
    }

    /**
     * @testdox Checking _showStatsTable()
     */
    public function testChecking_showStatsTable()
    {
        $this->timedStart = (time() - 300);

        $stats = [
            'stats heading here',
            ['attr 1', 'value A'],
            'string row here',
            ['attr 2', 'value B'],
            ['attr 3', 563665669],
        ];

        $this->cOutput->expects($this->exactly(34))->method('getFormatter')
            ->will($this->returnCallback(function() {
                return new \Symfony\Component\Console\Formatter\OutputFormatter;
            }));

        // cut off - variables value length
        $expected = [
            [true,  ''],
            [false, '+---------------------+---------------'],
            [false, '|<info>  ==> stats heading here       '],
            [false, '+---------------------+---------------'],
            [false, '| attr 1              | value A       '],
            [false, '| string row here     |               '],
            [false, '| attr 2              | value B       '],
            [false, '| attr 3              | 563,665,669   '],
            [false, '| CPU Load            | '],
            [false, '| Memory Usage / Peak | '],
            [false, '| Time Lapsed         | 5 minutes     '],
            [false, '+---------------------+---------------'],
            [true,  ''],
        ];

        $index = 0;
        $this->cOutput->expects($this->exactly( count($expected) ))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) use (&$index, $expected) {
                if ($expected[$index][0]) {
                    $this->assertSame($expected[$index][1], $messages);
                }
                else {
                    $this->assertStringContainsStringIgnoringCase($expected[$index][1], $messages);
                }
                $index++;
            }));

        $this->_showStatsTable($stats);
    }

    /**
     * @testdox Checking _executeWidgetUpdatesCommand() with Application Null
     */
    public function testChecking_executeWidgetUpdatesCommandApplicationNull()
    {
        $this->timedStart = (time() - 300);

        $this->cOutput->expects($this->at(0))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertSame(['','Executing "update-widgets" command...'], $messages);
            }));

        $this->cOutput->expects($this->at(1))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertSame('Unable to load Application.', $messages);
            }));

        $this->cOutput->expects($this->at(2))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertSame(' ', $messages);
            }));

        $this->_executeWidgetUpdatesCommand();
    }

    /**
     * @testdox Checking _executeWidgetUpdatesCommand() with Mocked Application
     */
    public function testChecking_executeWidgetUpdatesCommandMockedApplication()
    {
        $this->application = $this->createMock(\Symfony\Component\Console\Application::class);
        $this->application->expects($this->once())->method('find')
            ->will($this->returnCallback(function($name) {
                $this->assertSame('app:update-widgets', $name);

                $command = $this->createMock(\Symfony\Component\Console\Command\Command::class);
                $command->expects($this->once())->method('run')
                    ->will($this->returnCallback(function($input, $output) {
                        $this->assertInstanceOf(\Symfony\Component\Console\Input\Input::class, $input);
                        $this->assertTrue(!empty($output));
                    }));

                return $command;
            }));

        $this->timedStart = (time() - 300);

        $this->cOutput->expects($this->at(0))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertSame(['','Executing "update-widgets" command...'], $messages);
            }));

        $this->cOutput->expects($this->at(1))->method('writeln')
            ->will($this->returnCallback(function($messages, $options) {
                $this->assertSame(' ', $messages);
            }));

        $this->_executeWidgetUpdatesCommand();
    }

    /**
     * Simulate \Symfony\Component\Console\Command\Command::getApplication()
     */
    private function getApplication()
    {

        return $this->application;
    }
}
