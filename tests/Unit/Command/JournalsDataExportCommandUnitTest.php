<?php

namespace App\Tests\Unit\Command;

use App\Command\JournalsDataExportCommand;
use App\Entity\Journal;
use App\Entity\SpecialtyTaxonomy;
use App\Repository\JournalRepository;
use App\Service\MailService;
use Doctrine\ORM\EntityManagerInterface;
use Mockery as M;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @testdox UNIT | Command | JournalsDataExportCommand
 */
class JournalsDataExportCommandUnitTest extends TestCase
{
    /**
     * @var Mockery|EntityManagerInterface
     */
    private $entityManagerInterfaceMock;

    /**
     * @var Mockery|JournalRepository
     */
    private $repositoryMock;

    /**
     * @var Mockery|\Doctrine\ORM\EntityRepository<SpecialtyTaxonomy>
     */
    private $repoSpecialtyMock;

    /**
     * @var Mockery|MailService
     */
    private $mailServiceMock;

    /**
     * @var Mockery|LoggerInterface
     */
    private $loggerMock;

    /**
     * @var Mockery|InputInterface
     */
    private $inputInterfaceMock;

    /**
     * @var Mockery|OutputInterface
     */
    private $outputInterfaceMock;

    /**
     * @var JournalsDataExportCommand
     */
    private $command;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->entityManagerInterfaceMock = M::mock(EntityManagerInterface::class);
        $this->repositoryMock             = M::mock(JournalRepository::class);
        $this->repoSpecialtyMock          = M::mock(\Doctrine\ORM\EntityRepository::class);
        $this->mailServiceMock            = M::mock(MailService::class);
        $this->loggerMock                 = M::mock(LoggerInterface::class);
        $this->inputInterfaceMock         = M::mock(InputInterface::class);
        $this->outputInterfaceMock        = M::mock(OutputInterface::class);

        $this->entityManagerInterfaceMock
            ->shouldReceive('getRepository')
            ->once()->ordered()
            ->withArgs(function ($class) {
                self::assertSame(Journal::class, $class);
                return true;
            })
            ->andReturn($this->repositoryMock);

        $this->entityManagerInterfaceMock
            ->shouldReceive('getRepository')
            ->once()->ordered()
            ->withArgs(function ($class) {
                self::assertSame(SpecialtyTaxonomy::class, $class);
                return true;
            })
            ->andReturn($this->repoSpecialtyMock);
    }

    public function test__construct()
    {
        $this->command = new JournalsDataExportCommand($this->entityManagerInterfaceMock, $this->mailServiceMock, $this->loggerMock);

        self::assertNotNull($this->command);
        self::assertSame('Journals', $this->command->getType());
    }

    public function testClassExecuteWithFailedInputToConfig()
    {
        $this->repositoryMock->shouldReceive('searchResultCount')
            ->once()
            ->andReturn(1);

        $this->loggerMock->shouldReceive('error')->withArgs(function ($arg) {
                self::assertSame('Invalid \'name\' multiple, expecting single value only.', $arg);
                return true;
            });

        $this->command = new JournalsDataExportCommand($this->entityManagerInterfaceMock, $this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);

        $this->inputInterfaceMock->shouldReceive('getArgument')
            ->withArgs(function ($arg) {
                $found = array_search($arg, [
                    'email',
                    'name',
                ], true);

                $found = ($found !== false);
                self::assertTrue($found);

                return $found;
            })
            ->andReturnValues([
                'dummy@mockery.com',
                ['name1', 'name2'],
            ]);

        $this->outputInterfaceMock->shouldReceive('getVerbosity')->andReturn(OutputInterface::VERBOSITY_VERY_VERBOSE);
        $this->outputInterfaceMock->shouldReceive('isDecorated')->andReturn(false);
        $this->outputInterfaceMock->shouldReceive('getFormatter')->andReturn(null);

        $method = new \ReflectionMethod(JournalsDataExportCommand::class, 'execute');
        $method->setAccessible(true);
        $result = $method->invokeArgs($this->command, [$this->inputInterfaceMock, $this->outputInterfaceMock]);

        self::assertSame(1, $result);
    }

    public function testClassExecuteOk()
    {
        $receivedConfigs = [
            'name'               => 'Cardio',
            'volume'             => '',
            'status'             => 5,
            'specialty'          => ['1', '2', '3', 'new specialty'],
            'ids'                => '',
            'ismedline'          => 'yes',
            'isdgabstract'       => '',
            'issecondline'       => 'yes',
            'notbeingupdated'    => '',
            'drugbeingmonitored' => 'no',
            'deleted'            => '',
            'showcorrupted'      => 'no',
        ];

        $receivedConfigsReadable = [
            'status'    => 'Archived (5)',
            'specialty' => 'Oncology (#1), 2, Cardiology (#3), new specialty',
        ];

        $this->repositoryMock->shouldReceive('searchResultCount')
            ->once()
            ->withArgs(function ($configs) use ($receivedConfigs) {
                self::assertSame($receivedConfigs, $configs);
                return true;
            })
            ->andReturn(1);

        $this->repositoryMock->shouldReceive('countMaxSpecialtiesConnection')
            ->once()
            ->andReturn(3);

        $this->repoSpecialtyMock->shouldReceive('findOneBy')
            ->once()->ordered()
            ->withArgs(function ($criteria) {
                self::assertSame(['id' => '1'], $criteria);
                return true;
            })
            ->andReturnUsing(function() {
                $specialty = new SpecialtyTaxonomy;
                $specialty->setSpecialty('Oncology');
                return $specialty;
            });

        $this->repoSpecialtyMock->shouldReceive('findOneBy')
            ->once()->ordered()
            ->withArgs(function ($criteria) {
                self::assertSame(['id' => '2'], $criteria);
                return true;
            })
            ->andReturn(null);

        $this->repoSpecialtyMock->shouldReceive('findOneBy')
            ->once()->ordered()
            ->withArgs(function ($criteria) {
                self::assertSame(['id' => '3'], $criteria);
                return true;
            })
            ->andReturnUsing(function() {
                $specialty = new SpecialtyTaxonomy;
                $specialty->setSpecialty('Cardiology');
                return $specialty;
            });

        $generalDate = new \DateTime('now');

        $journal = new Journal(1902);
        $journal
            ->setName('Cardiotherapy Weekly')
            ->setAbbreviation('CDIOW')
            ->setVolume('E125')
            ->setNumber('#45')
            ->setStatus(Journal::STATUS_ACTIVE)
            ->setExported(0)
            ->setIsMedline(1)
            ->setIsSecondLine(1)
            ->setIsDgAbstract(0)
            ->setNotBeingUpdated(1)
            ->setDrugsMonitored(0)
            ->setGlobalEditionJournal(1)
            ->setDeleted(0)
            ->setManualCreation(1)
            ->setMedlineIssn('3216-4584')
            ->setJournalReport1(2.32)
            ->setJournalReport2(12.86)
            ->setUnid('24670451690125668078960235178903')
            ->setDetailsLanguage('DE')
            ->setDetailsCountry('DEN')
            ->setDetailsRegion(3)
            ->setDetailsEdition('This is edition number')
            ->setDetailsCopyright('This is the copyrights text here')
            ->setDetailsAutoPublishingSource('APS String')
            ->setDetailsPreviousRowGuid('ROW GUID String')
            ->setDetailsAbstractAbstract(true)
            ->setDetailsAbstractRegForAbstract(false)
            ->setDetailsAbstractFullTextAvailablity(true)
            ->setDetailsAbstractRegForFullText(false)
            ->setDetailsAbstractFeeForFullText(false)
            ->setDetailsAbstractComments('Abs Comments String')
            ->setDetailsOrderingHtmlCode('html tag code string')
            ->setDetailsOrderingCgiValue('CGI value here')
            ->setDetailsOrderingPrinterName('Printer Name String')
            ->setDetailsPublisherName('My Publisher')
            ->setDetailsPublisherRegUrl('https://david.coderlogy.com')
            ->setDetailsUrlUrl('http://plex.david.coderlogy.com')
            ->setDetailsUrlUpdatedOn($generalDate)
            ->setDetailsUrlViewedOn($generalDate)
            ->setDetailsUrlViewedOnComment('Site is up')
            ->setDetailsUrlToUpdate($generalDate)
            ->setDetailsUrlNote('Site robots.txt disable indexing')
            ->setDetailsUrlFrequency('Twice a year')
            ->setDetailsUrlUsername('northwestern')
            ->setDetailsUrlPassword('p4$$w0rd')
            ->setDetailsValidationAuthor('mike.dagger')
            ->setDetailsValidationDateComposed($generalDate)
            ->setDetailsValidationClearedBy('ben.howard')
            ->setDetailsValidationClearedOn($generalDate)
            ->setDetailsValidationModified('bon.iver')
            ->setDetailsMetaCreatedOn($generalDate)
            ->setDetailsMetaCreatedBy('mick.dagger')
            ->setDetailsMetaModifiedOn($generalDate)
            ->setDetailsMetaModifiedBy('mick.dagger')
            ->addSpecialty($this->createJournalSpecialties('Specialty 1'))
            ->addSpecialty($this->createJournalSpecialties('Specialty 2'))
            ->addSpecialty($this->createJournalSpecialties('Specialty 3'))
            ;

        $this->repositoryMock->shouldReceive('search')
            ->withArgs(function ($configs, $limit, $offset) use ($receivedConfigs) {
                self::assertSame($receivedConfigs, $configs);

                self::assertSame(1000, $limit);

                self::assertSame(0, $offset);
                return true;
            })
            ->andReturn([$journal]);

        $country = new \App\Entity\Country;
        $country
            ->setName('Denmark')
            ->setCapitalCity('Copenhagen');

        $this->repositoryMock->shouldReceive('getJournalDetailsCountry')
            ->once()
            ->andReturn($country);

        $region = new \App\Entity\CountryTaxonomy;
        $region->setName('Western Europe');

        $this->repositoryMock->shouldReceive('getJournalDetailsCountryTaxonomy')
            ->once()
            ->andReturn($region);

        $this->mailServiceMock->shouldReceive('setTemplate')
            ->once()
            ->andReturnSelf();
        $this->mailServiceMock->shouldReceive('sentEmail')->once()
            ->withArgs(function ($email, $subject, $content, $files) use ($receivedConfigs, $receivedConfigsReadable, $generalDate) {
                $date = new \DateTime('now');
                $date = $date->format('Y-m-d');

                self::assertSame('dummy@mockery.com', $email);

                self::assertStringContainsStringIgnoringCase('Your Journals Export on ' . $date, $subject);

                self::assertIsArray($content);
                self::assertArrayHasKey('heading', $content);
                self::assertSame('Journals Export Data Attached', $content['heading']);

                self::assertArrayHasKey('greeting', $content);
                self::assertSame('Hi,', $content['greeting']);

                self::assertArrayHasKey('content_top', $content);
                self::assertSame('Export complete, file attached.', $content['content_top']);

                self::assertArrayHasKey('pre_content_before', $content);
                self::assertSame('Recorded process:', $content['pre_content_before']);

                self::assertArrayHasKey('pre_content', $content);
                self::assertStringContainsStringIgnoringCase('COMMAND: Exporting Journals Data', $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('dummy@mockery.com', $content['pre_content']);
                foreach ($receivedConfigs as $receivedConfigKey => $receivedConfigValue) {
                    self::assertStringContainsStringIgnoringCase('- ' . $receivedConfigKey, $content['pre_content']);

                    if (isset($receivedConfigsReadable[$receivedConfigKey])) {
                        self::assertStringContainsStringIgnoringCase($receivedConfigsReadable[$receivedConfigKey], $content['pre_content']);
                        continue;
                    }

                    if (!empty($receivedConfigValue)) {
                        if (is_array($receivedConfigValue)) {
                            self::assertStringContainsStringIgnoringCase('| ' . implode(', ', $receivedConfigValue), $content['pre_content']);
                        }
                        else {
                            self::assertStringContainsStringIgnoringCase('| ' . $receivedConfigValue, $content['pre_content']);
                        }
                    }
                }

                self::assertStringContainsStringIgnoringCase($date, $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('Export complete.', $content['pre_content']);
                self::assertStringContainsStringIgnoringCase('Export Size:', $content['pre_content']);

                self::assertIsArray($files);
                self::assertNotEmpty($files);
                self::assertStringContainsStringIgnoringCase('DrugDb-Journals-export-data-' . $date, $files[0]);
                self::assertStringContainsStringIgnoringCase('.csv', $files[0]);

                $csv = file_get_contents($files[0]);
                $csv = explode(PHP_EOL, $csv);
                $csv = array_filter($csv);
                $csv = array_map('str_getcsv', $csv);
                self::assertSame(2, count($csv));

                // BOM removal
                $csv[0] = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $csv[0]);

                self::assertSame(59, count($csv[0]));
                self::assertEquals([
                    'Id',
                    'Name',
                    'Abbreviation',
                    'Volume/Issue',
                    'Number',
                    'Status',
                    'Exported',
                    'Medline',
                    'SecondLine',
                    'DG Abstract',
                    'Not being Updated',
                    'Drugs being Monitored',
                    'Global',
                    'Deleted',
                    'Manual Creation',
                    'Medline ISSN',
                    'Journal Report 1',
                    'Journal Report 2',
                    'UNID',
                    'Language',
                    'Country',
                    'Region',
                    'Edition',
                    'Copyright',
                    'Auto Publishing Source',
                    'Previous Row GUID',
                    'Abstract: With Abstract',
                    'Abstract: Register for Abstract',
                    'Abstract: FullText Availability',
                    'Abstract: Register for FullText',
                    'Abstract: Fee for FullText',
                    'Abstract: Comments',
                    'Ordering: HTML Code',
                    'Ordering: CGI Value',
                    'Ordering: Printer Name',
                    'Publisher: Name',
                    'Publisher: Registration URL',
                    'URL: URL',
                    'URL: Updated On',
                    'URL: Viewed On',
                    'URL: Viewed On Comment',
                    'URL: To Update',
                    'URL: Note',
                    'URL: Frequency',
                    'URL: Username',
                    'URL: Password',
                    'Validation: Author',
                    'Validation: Date Composed',
                    'Validation: Cleared by',
                    'Validation: Cleared on',
                    'Validation: Modified',
                    'Meta: Created on',
                    'Meta: Created by',
                    'Meta: Modified on',
                    'Meta: Modified by',
                    'Specialty Count',
                    'Specialty 1',
                    'Specialty 2',
                    'Specialty 3',
                ], $csv[0]);

                $generalDate = $generalDate->format('Y-m-d H:i:s');
                self::assertSame(59, count($csv[1]));
                self::assertEquals([
                    '1902',
                    'Cardiotherapy Weekly',
                    'CDIOW',
                    'E125',
                    '#45',
                    'Active',
                    'No',
                    'Yes',
                    'Yes',
                    'No',
                    'Yes',
                    'No',
                    'Yes',
                    'No',
                    'Yes',
                    '3216-4584',
                    '2.32',
                    '12.86',
                    '24670451690125668078960235178903',
                    'DE',
                    'Denmark',
                    'Western Europe',
                    'This is edition number',
                    'This is the copyrights text here',
                    'APS String',
                    'ROW GUID String',
                    'Yes',
                    'No',
                    'Yes',
                    'No',
                    'No',
                    'Abs Comments String',
                    'html tag code string',
                    'CGI value here',
                    'Printer Name String',
                    'My Publisher',
                    'https://david.coderlogy.com',
                    'http://plex.david.coderlogy.com',
                    $generalDate,
                    $generalDate,
                    'Site is up',
                    $generalDate,
                    'Site robots.txt disable indexing',
                    'Twice a year',
                    'northwestern',
                    'p4$$w0rd',
                    'mike.dagger',
                    $generalDate,
                    'ben.howard',
                    $generalDate,
                    'bon.iver',
                    $generalDate,
                    'mick.dagger',
                    $generalDate,
                    'mick.dagger',
                    '3',
                    'Specialty 1',
                    'Specialty 2',
                    'Specialty 3',
                ], $csv[1]);

                return true;
            })
            ->andReturn(1);

        $this->loggerMock->shouldReceive('info');

        $this->command = new JournalsDataExportCommand($this->entityManagerInterfaceMock, $this->mailServiceMock, $this->loggerMock);
        self::assertNotEmpty($this->command);

        $this->inputInterfaceMock->shouldReceive('getArgument')
            ->withArgs(function ($arg) {
                $found = array_search($arg, [
                    'email',
                    'name',
                    'volume',
                    'status',
                    'specialty',
                    'ids',
                    'ismedline',
                    'isdgabstract',
                    'issecondline',
                    'notbeingupdated',
                    'drugbeingmonitored',
                    'deleted',
                    'showcorrupted',
                ], true);

                $found = ($found !== false);
                self::assertTrue($found);

                return $found;
            })
            ->andReturnValues([
                'dummy@mockery.com',
                'Cardio',
                '',
                '5',
                '1, 2, 3, , new specialty ',
                '',
                'yes',
                '',
                'yes',
                '',
                'no',
                '',
                null,
            ]);

        $this->outputInterfaceMock->shouldReceive('getVerbosity')->andReturn(OutputInterface::VERBOSITY_VERY_VERBOSE);
        $this->outputInterfaceMock->shouldReceive('isDecorated')->andReturn(false);
        $this->outputInterfaceMock->shouldReceive('getFormatter')->andReturn(null);
        $this->outputInterfaceMock
            ->shouldReceive('write')
            ->once()
            ->withArgs(function ($buffered) {
                self::assertTrue(is_string($buffered));
                return true;
            });

        $method = new \ReflectionMethod(JournalsDataExportCommand::class, 'execute');
        $method->setAccessible(true);
        $method->invokeArgs($this->command, [$this->inputInterfaceMock, $this->outputInterfaceMock]);
    }

    private function createJournalSpecialties(string $specialtyName): \App\Entity\JournalSpecialties
    {
        $specialtyTaxonomy = new \App\Entity\SpecialtyTaxonomy;
        $specialtyTaxonomy->setSpecialty($specialtyName);

        $journalSpecialties = new \App\Entity\JournalSpecialties;
        $journalSpecialties->setSpecialty($specialtyTaxonomy);

        return $journalSpecialties;
    }
}
