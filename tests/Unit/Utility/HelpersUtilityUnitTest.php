<?php

namespace App\Tests\Unit\Utility;

use App\Utility\Helpers;
use PHPUnit\Framework\TestCase;

/**
 * @testdox UNIT | Utility | Helpers
 */
class HelpersUtilityUnitTest extends TestCase
{
    /**
     * @dataProvider getStringNumProvider
     *
     * @testdox stringNumFormat(): Stacks
     */
    public function testStringNumFormat($input, $postfix, $plural, $expect)
    {
        $value = Helpers::stringNumFormat($input, $postfix, $plural);
        $this->assertSame($expect, $value);
    }

    public function getStringNumProvider()
    {
        return array(
            // 0 Check Data Type to confirmed
            array(2, '', 's', '2'),
            // 1 Check on plural post-fix
            array(9, 'cat', 's', '9 cats'),
            // 2 Check none plural post-fix, with integer value
            array(1, 'dog', 's', '1 dog'),
            // 3 Check none plural post-fix, with floating value
            array((1 / 4), 'position', 's', '0 position'),
        );
    }

    /**
     * @dataProvider getStringPluralProvider
     *
     * @testdox stringPluralFormat(): Stacks
     */
    public function testStringPluralFormat($input, $singular, $plural, $expect)
    {
        $value = Helpers::stringPluralFormat($input, $singular, $plural);
        $this->assertSame($expect, $value);
    }

    public function getStringPluralProvider()
    {
        return array(
            // 0
            array(1, 'car', 's', 'car'),
            // 1
            array(2, 'car', 's', 'cars'),
            // 2
            array(1, 'annoy', 's', 'annoy'),
            // 3
            array(2, 'donkey', 'donkeys', 'donkeys'),
            // 4
            array(1, 'enemy', 'enemies', 'enemy'),
            // 5
            array(3, 'enemy', 'enemies', 'enemies'),
            // 6
            array(5, 'soliloquy', 'soliloquies', 'soliloquies'),
        );
    }

    /**
     * @dataProvider getHumanReadableSizeProvider
     *
     * @testdox convertHumanReadableSize(): Stacks
     */
    public function testConvertHumanReadableSize($input, $decimal, $expect)
    {
        $value = Helpers::convertHumanReadableSize($input, $decimal);
        $this->assertEquals($expect, $value);
    }

    public function getHumanReadableSizeProvider()
    {
        return array(
            // 0 Check small value
            array(1024, 2, '1.00Kb'),
            // 1 Check huge value
            array(((1024 * 1024) * 1024), 2, '1.00Gb'),
            // 2 Check specific value
            array(64264, 2, '62.76Kb'),
        );
    }

    /**
     * @dataProvider getHumanReadableTimeProvider
     *
     * @testdox convertHumanReadbleTime(): Stacks
     */
    public function testConvertHumanReadbleTime($input, $inital, $specific, $expect)
    {
        $value = Helpers::convertHumanReadbleTime($input, $inital, $specific);
        $this->assertEquals($expect, $value);
    }

    public function getHumanReadableTimeProvider()
    {
        $now = time();
        return array(
            // 0 Check to-time, with no initial-time, short-gap
            array(strtotime('1970-01-01 01:00:00'), 0, 0, '1 hour'),
            // 1 Check to-time, with no initial-time, long-gap
            array(strtotime('1981-04-30 03:01:17'), 0, 0, '4137 days, 3 hours, 1 minute, 17 seconds'),
            // 2 Check to-time, with initial-time
            array($now, ($now - (60 * 60 * 3)), 0, '3 hours'),
            // 3 Check to-time, with initial-time note limited by specific
            array($now, ($now - (60 * 60 * 3.52)), 0, '3 hours, 31 minutes, 12 seconds'),
            // 4 Check to-time, with initial-time limited by specific 2
            array($now, ($now - (60 * 60 * 3.52)), 2, '3 hours, 31 minutes'),
            // 5 Check to-time, with initial-time limited by specific 1
            array($now, ($now - (60 * 60 * 3.52)), 1, '3 hours'),
            // 6 Millisecond
            array($now, $now, 1, 'Just now'),
        );
    }

    /**
     * @dataProvider getExcessWhitespaceProvider
     *
     * @testdox removeExcessWhitespace(): Stacks
     */
    public function testRemoveExcessWhitespace($input, $expect)
    {
        $value = Helpers::removeExcessWhitespace($input);
        $this->assertEquals($expect, $value);
    }

    public function getExcessWhitespaceProvider()
    {
        return array(
            // 0 Check basic L/R trimming
            array(
                ' this should be simple ',
                'this should be simple',
            ),
            // 1 Check basic carriage-return with spaces trimming
            array(
                '
                    Christmas night,
                    it clutched         the light,
                    the hallow bright
                ',
                'Christmas night, it clutched the light, the hallow bright',
            ),
            // 2 Check multi carriage-return trimming
            array(
                '
                    And
                    at
                    once
                    I
                    knew
                    I
                    was
                    not
                    magnificent
                ',
                'And at once I knew I was not magnificent',
            ),
            // 3 Check extreme multi-type of whitespace-s' trimming
            array(
                '                                                                                                                                                '
                . "\t\f\f\r\n\x0B"
                . 'Staring'
                . "\t\f\f\t\x0B"
                . null
                . null
                . ' at the'
                . PHP_EOL
                . '                         '
                . PHP_EOL
                . 'sink     of'
                . "\r\r\r"
                . 'blood'
                . "\n\n\n"
                . 'and    crushed veneer'
                . "\r\n\r\n\r\n",
                'Staring at the sink of blood and crushed veneer',
            ),
        );
    }

    /**
     * @dataProvider getEqualArraySetProvider
     *
     * @testdox validateEqualArrayValueSet(): Stacks
     */
    public function testValidateEqualArrayValueSet($inputA, $inputB, $expect)
    {
        $value = Helpers::validateEqualArrayValueSet($inputA, $inputB);
        $this->assertEquals($expect, $value);
    }

    public function getEqualArraySetProvider()
    {
        return array(
            // #0
            array(
                array('a', 'b', 'c'),
                array('a', 'b', 'c'),
                true
            ),
            // #1
            array(
                array('a', 'b', 'c'),
                array('b', 'c', 'a'),
                true
            ),
            // #2
            array(
                array('b', 'a', 'c'),
                array('b', 'c', 'a'),
                true
            ),
            // #3
            array(
                array('c'),
                array('b', 'c', 'a'),
                false
            ),
            // #4
            array(
                array('a' => '1', 'b' => '2'),
                array('b' => '2', 'a' => '1'),
                true
            ),
            // #5
            array(
                array('a' => 1, 'b' => 2),
                array('b' => 2, 'a' => 1),
                true
            ),
            // #6
            array(
                array('a' => 1, 'b' => 2),
                array('c' => 2, 'z' => 1),
                true
            ),
            // #7
            array(
                array('a' => 1, 'b' => 2),
                array(2, 1),
                true
            ),
            // #8
            array(
                array('a' => 1, 'b' => 2),
                array('b' => 2),
                false
            ),
            // #9
            array(
                array('a' => array(1), 'b' => array(2)),
                array(array(1), array(2)),
                true
            ),
            // #10
            array(
                array('a' => array(1), 'b' => array(2)),
                array(1, 2),
                false
            ),
            // #11 - see inner / deeper content is ignored
            array(
                array('a' => array(1), 'b' => array(2)),
                array(array(1), array(3)),
                true
            ),
            // #12
            array(
                array('a' => 1, 'b' => 3, 'z' => 2),
                array(2, 1, 3),
                true
            ),
        );
    }

    /**
     * @dataProvider getKeyOfSameArraySetProvider
     *
     * @testdox getKeyOfSameArraySet(): Stacks
     */
    public function testGetKeyOfSameArraySet($tree, $subset, $in, $expect)
    {
        $value = Helpers::getKeyOfSameArraySet($tree, $subset, $in);
        $this->assertEquals($expect, $value);
    }

    public function getKeyOfSameArraySetProvider()
    {
        return array(
            // #0
            array(
                array(
                    array(1, 2, 3)
                ),
                array(1, 2, 3),
                null,
                0
            ),
            // #1
            array(
                array(
                    array(3),
                    array(2, 3),
                    array(1, 2, 3),
                    array(1, 2, 3),
                ),
                array(1, 2, 3),
                null,
                2
            ),
            // #2
            array(
                array(
                    array(1, 2, 3)
                ),
                array(3),
                null,
                null
            ),
            // #3
            array(
                array(
                    array('a' => array(1, 2, 3))
                ),
                array(1, 2, 3),
                'a',
                0
            ),
            // #4
            array(
                array(
                    array('a' => array(3)),
                    array('a' => array(1, 2, 3))
                ),
                array(1, 2, 3),
                'a',
                1
            ),
            // #5
            array(
                array(
                    array('a' => array(3)),
                    array('a' => array(1, 2, 3))
                ),
                array(1, 2, 3),
                'b',
                null
            ),
            // #6
            array(
                array(
                    array('a' => array(3)),
                    array('a' => array(array(1), 2, 3))
                ),
                array(3, 2, array(1)),
                'a',
                1
            ),
            // #7
            array(
                array(
                    array('a' => array(3)),
                    array('a' => array(array(1), array(2), 3))
                ),
                array(3, 2, array(1)),
                'a',
                null
            ),
        );
    }

    /**
     * @dataProvider getEventReadSpanBetweenDatesProvider
     *
     * @testdox static getEventReadSpanBetweenDates(): Stacks
     */
    public function testEventReadSpanBetweenDatesStacks($dateOne, $dateTwo, $expects)
    {
        foreach ($expects as $ei => $expect) {
            $result = Helpers::getEventReadSpanBetweenDates($dateOne, $dateTwo, $expect[0]);

            $this->assertEquals($expect[1], $result, "{$ei} - {$expect[0]}");
        }
    }

    public function getEventReadSpanBetweenDatesProvider()
    {
        return [
            // #0 - basic
            [
                null,
                null,
                [
                    [null, null],
                ],
            ],
            // #1 - with start date
            [
                new \DateTime('2017-02-19 15:30:00'),
                null,
                [
                    [null,     'Begins Sunday, 19th Feb 2017'],
                    ['strong', 'Begins <strong>Sunday, 19th Feb 2017</strong>'],
                ],
            ],
            // #2 - with end date + same month
            [
                new \DateTime('2017-02-19 15:30:00'),
                new \DateTime('2017-02-25 15:30:00'),
                [
                    [null, 'Between Sunday, 19th and Saturday, 25th Feb 2017'],
                    ['b',  'Between <b>Sunday, 19th</b> and <b>Saturday, 25th Feb 2017</b>'],
                ],
            ],
            // #3 - with month diff
            [
                new \DateTime('2017-02-19 15:30:00'),
                new \DateTime('2017-05-30 15:30:00'),
                [
                    [null,     'Between Sunday, 19th Feb and Tuesday, 30th May 2017'],
                    ['strong', 'Between <strong>Sunday, 19th Feb</strong> and <strong>Tuesday, 30th May 2017</strong>'],
                ],
            ],
            // #4- with year diff
            [
                new \DateTime('2017-02-19 15:30:00'),
                new \DateTime('2025-05-30 15:30:00'),
                [
                    [null,   'Between Sunday, 19th Feb 2017 and Friday, 30th May 2025'],
                    ['span', 'Between <span>Sunday, 19th Feb 2017</span> and <span>Friday, 30th May 2025</span>'],
                ],
            ],
            // #5 - no start date
            [
                null,
                new \DateTime('2025-05-30 15:30:00'),
                [
                    [null, 'Until Friday, 30th May 2025'],
                    ['u',  'Until <u>Friday, 30th May 2025</u>'],
                ],
            ],
            // #6 - same date
            [
                new \DateTime('2025-05-30 15:30:00'),
                new \DateTime('2025-05-30 15:30:00'),
                [
                    [null, 'On Friday, 30th May 2025'],
                    ['em', 'On <em>Friday, 30th May 2025</em>'],
                ],
            ],
            // #7 - reversed - will just provide, without validate them
            [
                new \DateTime('2017-02-25 15:30:00'),
                new \DateTime('2017-02-03 15:30:00'),
                [
                    [null,  'Between Saturday, 25th and Friday, 3rd Feb 2017'],
                    ['tag', 'Between <tag>Saturday, 25th</tag> and <tag>Friday, 3rd Feb 2017</tag>'],
                ],
            ],
        ];
    }
}
