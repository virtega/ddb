<?php

namespace App\Tests\Unit\Utility;

use App\Utility\Crypto;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CryptoExtendsOverloaded extends Crypto
{
    public function __construct(?string $cipher = 'AES-128-CBC', ?string $salt = null)
    {
        if (false === getenv('APP_SECRET')) {
            (new \Symfony\Component\Dotenv\Dotenv(true))->loadEnv(dirname(__DIR__) . '/../../.env');
        }

        parent::$cipher = $cipher;

        $salt = $salt ?? getenv('APP_SECRET');

        parent::__construct( $salt );
    }

    public function getCipher()
    {
        return parent::$cipher;
    }
}

/**
 * @testdox UNIT | Utility | Crypto
 */
class CryptoUtilityUnitTest extends WebTestCase
{
    /**
     * @testdox Check with Service DI AutoWired APP_SECRET
     */
    public function testWithWiredAppSecret()
    {
        $test_string = implode('', [
            '4SZjFWZwBCZuFGIu4iLzNXasJGIsFGdvRHIulGIlNWYwNHIodWdvJHa0ByZulGbpF2cgUGa0Byck5WZwNHIlhGIk5WQg4yYpNXdt',
            'BycyFWZoBSZIBiLlJ3btlnbhByZul2ajlGdgIXYlhGI0dibzV2bkBSZIBiLtVGa0BycuVGcvBSZoBiblhGdgQmbhBCLu9Wa0Fmbp',
            'dWYtlGIzlGag8GdulGIzV2bnBSZoBCZuFGIu4iLzVWelBycphGIzV2cvx2YgUGag82Ug4CZuV3bzBycphGdggGdpdHIlZ3bsBibp',
            'BCbsFmZg8GdgMXag4iLukHdp5WYzBycphGIlZXYzByb0BSehdHI5xmbvBSZoRHIu4iLzVGZpNWZkBCd1Fmbv12cvNGIlhGdg82U',
        ]);

        self::bootKernel();

        $cryto = self::$container->get('test.' . Crypto::class);

        $encrypted = $cryto::encrypt($test_string);
        $this->assertTrue(is_string($encrypted));

        $decrypted = $cryto::decrypt($encrypted);
        $this->assertSame($decrypted, $test_string);

        $decrypted = base64_decode(strrev($decrypted));
        $this->assertStringContainsString('cosmonaut', $decrypted);
        $this->assertStringContainsString('sailing', $decrypted);
    }

    /**
     * @testdox Check basic encryption
     */
    public function testEncrypt()
    {
        $encrypted = (new CryptoExtendsOverloaded())::encrypt('test_secret_plaintext');
        $this->assertTrue(is_string($encrypted));

        $encrypted = (new CryptoExtendsOverloaded(null, '12345678901234567890123456789012'))::encrypt('Standing In The Open Light><Within The Swelter Of The Night><I Found Myself Staring At You');
        $this->assertTrue(is_string($encrypted));
        $this->assertSame(implode('', [
            'fiHmZN3dngP3VOUEAcp5ALrWXOl7FsAp',
            'h0Du74z0UDoMwCxJwkZKGoJksBVDBKlC',
            'TgBrr0tRf+MdthyfMxW9ohBBg5AOZEqt',
            'l86vUIm4Cu7n6X3DVZEiEGtJWZhUe4Gg',
        ]), $encrypted);

        $encrypted = (new CryptoExtendsOverloaded('BF-CBC', '12345678901234567890123456789012'))::encrypt('Standing In The Open Light><Within The Swelter Of The Night><I Found Myself Staring At You');
        $this->assertTrue(is_string($encrypted));
        $this->assertSame(implode('', [
            'k60nrfWg7aXNV8kdTnReNBnHDP41+x03',
            'o8/n0MdS4YW6XquLJUrzIM3jEIdpCJVs',
            'kVS6ensC+9HSI24Si4yUOq5p+cogfAN2',
            'foiYzgAKcDpUkbvbbxsS2c0ntjpo5laO',
        ]), $encrypted);
    }

    /**
     * @testdox Check basic decryption
     */
    public function testDecrypt()
    {
        $decrypted = (new CryptoExtendsOverloaded())::decrypt('gfJlNQ1yQjzorDVXAadgndY5QRmfHEgg/ria6lL7HSQ');
        $this->assertTrue(is_string($decrypted));

        $decrypted = (new CryptoExtendsOverloaded(null, '12345678901234567890123456789012'))::decrypt(implode('', [
            'QfRE6gdD3xBd5cd/C1diorElH0Rpso00cJjX9cdROh68bNS0CNGJvgBqTECfJVeOW06h3MJ3jvZfFvsM8DkR8DG9ARLoCg0G/A5S6ErD/AJ'
        ]));
        $this->assertTrue(is_string($decrypted));
        $this->assertSame('AndIWalkedOffYou<>AndIWalkedOffAnOldMe<>OhMeOhMyIThoughtItWasADream<>SoItSeemed', $decrypted);

        $decrypted = (new CryptoExtendsOverloaded('AES-1024', '12345678901234567890123456789012'))::decrypt(implode('', [
            'A57EqcB4yfD/8iTnuM3sfkOLFpdxDHVOoZKGWtmCJVCdl349e61Ua1nCDxGwWeo9Bx6Frz+wvfPjKlGZ8t9hQZOHt+8iSZqC+F92xUg9nZk',
            '6Nu/eBgBOkNnuRldOKxJ2je9BXSO+01ba8FOyDcY08F96rx+cyJxkVM3GDkPwVBPexatKojG4BbfHtbwrrwQgb0pHC9ghYFbaepRv5BNme2',
        ]));
        $this->assertTrue(is_string($decrypted));
        $this->assertSame('And At Once I Knew I Was Not Magnificent--Strayed Above The Highway Aisle--Jagged Vacance, Thick With Ice--And I Could See For Miles, Miles, Miles', $decrypted);
    }

    /**
     * @testdox Check different Cipher
     */
    public function testDifferentCipher()
    {
        $crypto = new CryptoExtendsOverloaded('BF-CBC');
        $this->assertRegExp('/(BF|AES-[0-9]{2,3})-CBC/i', $crypto->getCipher());

        $crypto = new CryptoExtendsOverloaded('AES-1024-CBC');
        $this->assertRegExp('/AES-[0-9]{2,3}-CBC/i', $crypto->getCipher());

        $crypto = new CryptoExtendsOverloaded('AES-1024');
        $this->assertRegExp('/AES-[0-9]{2,3}-CBC/i', $crypto->getCipher());

        $crypto = new CryptoExtendsOverloaded('AES-1024-GCM');
        $this->assertRegExp('/AES-[0-9]{2,3}-(GCM|CBC)/i', $crypto->getCipher());

        $crypto  = new CryptoExtendsOverloaded('PSL-9999-CRY');
        $ciphers = openssl_get_cipher_methods();
        $ciphers = $ciphers[0];
        $this->assertSame($ciphers, $crypto->getCipher());
    }
}
