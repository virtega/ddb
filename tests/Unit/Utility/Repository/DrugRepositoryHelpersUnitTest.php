<?php

namespace App\Tests\Unit\Utility\Repository;

use App\Entity\BrandSynonym;
use App\Service\SearchService;
use App\Utility\Repository\DrugRepositoryHelpers as DRHelpers;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @testdox UNIT | Utility-Repository | DrugRepositoryHelpers
 *
 * Tests for the following, moved to GenericDrugRepositoryFunctionalTest::testDownloadWriteDrugCollumnsForBoth(); handling with Fixture
 * - downloadWriteDrugCollumnsForComplete()
 * - downloadWriteDrugCollumnsForLess()
 */
class DrugRepositoryHelpersUnitTest extends WebTestCase
{
    /**
     * @testdox Checking reSortEntityKeywords(): Basic
     */
    public function testReSortEntityKeywords_001()
    {
        $results = array();
        DRHelpers::reSortEntityKeywords($results, 'synonyms');
        $this->assertEmpty($results);
    }

    /**
     * @testdox Checking reSortEntityKeywords(): Empty
     */
    public function testReSortEntityKeywords_002()
    {
        $results = array('synonyms' => array());
        DRHelpers::reSortEntityKeywords($results, 'synonyms');
        $this->assertEmpty($results['synonyms']);
    }

    /**
     * @testdox Checking reSortEntityKeywords(): Sorting
     */
    public function testReSortEntityKeywords_003()
    {
        $results = array('synonyms' => array());
        for ($i = 9; $i > 0; --$i) {
            $b = new BrandSynonym();
            $b->setName($i);
            $results['synonyms'][] = $b;
        }
        DRHelpers::reSortEntityKeywords($results, 'synonyms', 'getName');

        $expects = array_combine(range(0, 9), range(9, 0));
        foreach ($results['synonyms_keys'] as $index => $key) {
            $this->assertEquals($expects[$index], $key);
            $this->assertEquals($expects[$index], $results['synonyms'][$key]->getName());
        }
    }

    /**
     * @testdox Checking translateQueryToSql(): Returning Value
     */
    public function testTranslateQueryToSql()
    {
        $query  = 'test';
        $result = DRHelpers::translateQueryToSql($query);
        $this->assertEquals($result, $query);
    }

    /**
     * @dataProvider translateQueryToSqlProvider
     *
     * @testdox Checking translateQueryToSql(): Stacks
     */
    public function testTranslateQueryToSqlStack($query, $result)
    {
        DRHelpers::translateQueryToSql($query);
        $this->assertEquals($result, $query);
    }

    public function translateQueryToSqlProvider()
    {
        return array(
            array('test',            "LIKE '%test%'"),                        // 0
            array('test   ',         "LIKE '%test%'"),                        // 1
            array('test tea-coffee', "LIKE '%test%tea%coffee%'"),             // 2
            array('test%',           "LIKE '%test%'"),                        // 3
            array('^test%',          "LIKE 'test%'"),                         // 4
            array('^test',           "LIKE 'test%'"),                         // 5
            array('%test$',          "LIKE '%test'"),                         // 6
            array('test$',           "LIKE '%test'"),                         // 7
            array('^test$',          "= 'test'"),                             // 8
            array('^test^',          "LIKE 'test%'"),                         // 9
            array('$test$',          "LIKE '%test'"),                         // 10
            array('^:numeric',       "REGEXP '^([0-9]|[[:space:]]?[0-9])'"),  // 11
            array(':numeric',        "LIKE '%:numeric%'"),                    // 12
            array('^:symbol',        "REGEXP '^[^a-zA-Z0-9 ]'"),              // 13
            array(':symbol',         "LIKE '%:symbol%'"),                     // 14
            array('^A',              "REGEXP '^[[:space:]]?[A]'"),            // 15
            array('^5',              "REGEXP '^[[:space:]]?[5]'"),            // 16
            array('C',               "LIKE '%C%'"),                           // 17
            array('test+coffee',     "LIKE '%test coffee%'"),                 // 18
            array('^test+coffee',    "LIKE 'test coffee%'"),                  // 19
            array('test+coffee$',    "LIKE '%test coffee'"),                  // 20
            array('percent\%',       "LIKE '%percent\\\%'"),                  // 21
            array('any*',            "LIKE '%any%'"),                         // 22
            array('la\'+fame',       "LIKE '%la\' fame%'"),                   // 23
            array('"monster" truck', 'LIKE \'%\\"monster\\"%truck%\''),       // 24
            array('^any*',           "LIKE 'any%'"),                          // 25
            array('^any*-haul',      "LIKE 'any%haul%'"),                     // 26
            array('^any*-haul$',     "LIKE 'any%haul'"),                      // 27
        );
    }

    /**
     * @dataProvider getDrugSearchSqlProvider
     *
     * @testdox Checking getDrugSearchSql(): Stacks
     */
    public function testGetDrugSearchSql($i, $src, $overallCount, $hashed)
    {
        $sql = DRHelpers::getDrugSearchSql($src, $overallCount);

        // for troubleshooting the MD5 raw file:
        // $i = str_pad($i, 2, STR_PAD_LEFT, '0');
        // if (!is_dir(__DIR__ . '/data')) {
        //     mkdir(__DIR__ . '/data');
        // }
        // file_put_contents(__DIR__ . "/data/DrugRepositoryHelpersUnitTest-testGetDrugSearchSql-{$i}.sql", $sql);

        $sql = hash('sha256', $sql);
        $this->assertEquals($sql, $hashed);
    }

    public function getDrugSearchSqlProvider()
    {
        self::bootKernel();

        $src0 = self::$container->get('test.' . SearchService::class);
        $src0->setTerm('^test1$')
            ->setAutoComplete(true)
            ->setLimitMain(10)
            ->setType('experimental');
        self::$container->reset();

        $src1 = self::$container->get('test.' . SearchService::class);
        $src1->setTerm('test')
            ->setType('generic')
            ->setAutoComplete(true)
            ->setLimitMain(10);
        self::$container->reset();

        $src2 = self::$container->get('test.' . SearchService::class);
        $src2->setTerm('test1$')
            ->setType('generic_synonym')
            ->setAutoComplete(true)
            ->setLimitMain(10);
        self::$container->reset();

        $src3 = self::$container->get('test.' . SearchService::class);
        $src3->setTerm('test')
            ->setType('generic_typo')
            ->setExclusionList(array(
                'generic_typo' => array(5, 6, 7)
            ))
            ->setAutoComplete(true)
            ->setLimitMain(10);
        self::$container->reset();

        $src4 = self::$container->get('test.' . SearchService::class);
        $src4->setTerm('test')
            ->setType('brand')
            ->setAutoComplete(true)
            ->setLimitMain(10);
        self::$container->reset();

        $src5 = self::$container->get('test.' . SearchService::class);
        $src5->setTerm('test')
            ->setType('brand_synonym')
            ->setAutoComplete(true)
            ->setExclusionList(array(
                'brand'         => array(7, 8, 9),
                'brand_synonym' => array(1, 3, 5),
            ))
            ->setLimitMain(10);
        self::$container->reset();

        $src6 = self::$container->get('test.' . SearchService::class);
        $src6->setTerm('test')
            ->setAutoComplete(true)
            ->setLimitMain(10)
            ->setType('brand_typo');
        self::$container->reset();

        $src7 = self::$container->get('test.' . SearchService::class);
        $src7->setTerm('^test')
            ->setAutoComplete(true)
            ->setLimitMain(10);
        self::$container->reset();

        $src8 = self::$container->get('test.' . SearchService::class);
        $src8->setTerm('test')
            ->setAutoComplete(true)
            ->setLimitMain(10)
            ->setExclusionList(array(
                'experimental'    => array(1, 2, 3),
                'generic'         => array(1, 2, 3),
                'generic_synonym' => array(1, 2, 3),
                'generic_typo'    => array(1, 2, 3),
                'brand'           => array(1, 2, 3),
                'brand_synonym'   => array(1, 2, 3),
                'brand_typo'      => array(1, 2, 3),
            ));
        self::$container->reset();

        $src9 = self::$container->get('test.' . SearchService::class);
        $src9->setTerm('test')
            ->setAutoComplete(false)
            ->setLimitMain(10)
            ->setExclusionList(array(
                'experimental'    => array(1, 2, 3),
                'generic'         => array(1, 2, 3),
                'generic_synonym' => array(1, 2, 3),
                'generic_typo'    => array(1, 2, 3),
                'brand'           => array(1, 2, 3),
                'brand_synonym'   => array(1, 2, 3),
                'brand_typo'      => array(1, 2, 3),
            ));
        self::$container->reset();

        $src10 = clone $src9;

        $src11 = self::$container->get('test.' . SearchService::class);
        $src11->setTerm('810BFBEEC2E7728C85256B5800619206')
            ->setUnidOnly(true)
            ->setLimitMain(10);
        self::$container->reset();

        $src12 = self::$container->get('test.' . SearchService::class);
        $src12->setTerm('^810')
            ->setUnidOnly(true)
            ->setLimitMain(10);
        self::$container->reset();

        $src13 = self::$container->get('test.' . SearchService::class);
        $src13->setTerm('619206$')
            ->setUnidOnly(true)
            ->setLimitMain(10);
        self::$container->reset();

        return array(
            array( // 0
                0,
                $src0,
                false,
                '2570e67c8d07cff399e6ac37480f0c4fb0bcd473056e511b73603030cba2bded',
            ),
            array( // 1
                1,
                $src1,
                false,
                '0de414eedd8da77462199225c46b07b17508a5ac2add442561221461880f0610',
            ),
            array( // 2
                2,
                $src2,
                false,
                'aa52d21c919a46c11791e874623eb8a2ba8ffe267be93580b6154e07ffbe6b54',
            ),
            array( // 3
                3,
                $src3,
                false,
                '49c4c4b5f83dae54c09f5b7007451eb6524a493f4706b985641f59014b1c6677',
            ),
            array( // 4
                4,
                $src4,
                false,
                '9478fe09122c13f3bb1a1221dd6a1bd8a3b9b51e20560339359c19990c7caf47',
            ),
            array( // 5
                5,
                $src5,
                false,
                'f4e3b46611ab1004c15fb20a26f0a96e29762fa559ae85f6bdfca5ebe612552c',
            ),
            array( // 6
                6,
                $src6,
                false,
                'e4e0d02b8af74a5cbc394de17f5e1887b0fa31fa74f55a262b89c236f3d8e4ab',
            ),
            array( // 7
                7,
                $src7,
                false,
                'd519a3e454ae6c25e4fe0d47b567a460a0efd4ad5c040bd768ba43c84e207cb8',
            ),
            array( // 8
                8,
                $src8,
                false,
                'c13c0d6214ebd36c071515b5235114aeb247fc5953f62f58dd028b156c2e689f',
            ),
            array( // 9
                9,
                $src9,
                false,
                '4a5f29cb7da3ebada90cd68f4f29040e2158c0475fb64aae04d3b1ab7df3bb6d',
            ),
            array( // 10
                10,
                $src10,
                true,
                '573d82fa36f95e3f9c9c926c60298688b7d9a64dbe54038dd304634135e06896',
            ),
            array( // 11
                11,
                $src11,
                true,
                'e00df9dd3c2447070e508941ff103fc9a26b1b5347314d7facd8fa47a7336e98',
            ),
            array( // 12
                12,
                $src12,
                true,
                '70cedda8c1adb2658174a00f4c709d2fbe8c59a6d1e693d1c260734953997498',
            ),
            array( // 13
                13,
                $src13,
                true,
                '18c3dd56b318140eafc1342f8b4859966c78f12a5ca80fcf1d633a7f72b8495f',
            ),
        );
    }

    /**
     * @dataProvider getProcessSearchResultForAnyProvider
     *
     * @testdox Checking processSearchResultForAutoComplete(): Stacks
     */
    public function testProcessSearchResultForAutoComplete($lmt, $row, $autoc, $scrh, $dwnld)
    {
        $result = DRHelpers::processSearchResultForAutoComplete($row, $lmt);
        $this->assertSame($autoc, $result);
    }

    /**
     * @dataProvider getProcessSearchResultForAnyProvider
     *
     * @testdox Checking processSearchResultForSearchPage(): Stacks
     */
    public function testProcessSearchResultForSearchPage($lmt, $row, $autoc, $scrh, $dwnld)
    {
        $result = DRHelpers::processSearchResultForSearchPage($row, $lmt);
        $this->assertSame($scrh, $result);
    }

    /**
     * @dataProvider getProcessSearchResultForAnyProvider
     *
     * @testdox Checking processSearchResultForDownload(): Stacks
     */
    public function testProcessSearchResultForDownload($lmt, $row, $autoc, $scrh, $dwnld)
    {
        $result = array();
        DRHelpers::processSearchResultForDownload($result, $row);
        $this->assertSame($dwnld, $result);
    }

    public function getProcessSearchResultForAnyProvider()
    {
        return array(
            array( // 0
                5,
                array(
                    'name'         => 'test1',
                    'experimental' => '1',
                    'generic'      => 'THIS-IS-GENERATED-UUID',
                    'brand'        => 'THIS-IS-GENERATED-UUID',
                    'source'       => '100e',
                    'locked'       => '3',
                ),
                array(
                    'id'     => 1,
                    'family' => 'experimental',
                    'name'   => 'test1',
                    'desc'   => 'an Experimental Drug',
                ),
                array(
                    'experimental' => array(
                        array(
                            'id'   => 1,
                            'name' => 'test1',
                        ),
                    ),
                    'generic'         => array(),
                    'generic_typo'    => array(),
                    'generic_synonym' => array(),
                    'brand'           => array(),
                    'brand_typo'      => array(),
                    'brand_synonym'   => array(),
                ),
                array(
                    'experimental' => array(1 => 0),
                    'generic'      => array(),
                    'brand'        => array(),
                ),
            ),
            array( // 1
                5,
                array(
                    'name'         => 'test1',
                    'experimental' => 'THIS-IS-GENERATED-UUID',
                    'generic'      => '1',
                    'brand'        => '2',
                    'source'       => '200bt',
                    'locked'       => '660',
                ),
                array(
                    'id'     => 2,
                    'family' => 'brand',
                    'name'   => 'test1',
                    'desc'   => 'a reference to Brand Drug',
                ),
                array(
                    'experimental'    => array(),
                    'generic'         => array(),
                    'generic_typo'    => array(),
                    'generic_synonym' => array(),
                    'brand'           => array(),
                    'brand_typo'      => array(
                        array(
                            'id'   => 2,
                            'name' => 'test1',
                        ),
                    ),
                    'brand_synonym'   => array(),
                ),
                array(
                    'experimental' => array(),
                    'generic'      => array(1 => 0),
                    'brand'        => array(2 => 0),
                ),
            ),
            array( // 2
                3,
                array(
                    'name'         => 'test1ACB||test2||test3||test4||test5',
                    'experimental' => 'THIS-IS-GENERATED-UUID',
                    'generic'      => '5||1',
                    'brand'        => '2||3||4',
                    'source'       => '100g||200bs',
                    'locked'       => '2',
                ),
                array(
                    'id'     => 5,
                    'family' => 'generic',
                    'name'   => 'test1ACB / test2 / test3...',
                    'desc'   => 'a Generic Drug',
                ),
                array(
                    'experimental' => array(),
                    'generic'      => array(
                        array(
                            'id'   => 5,
                            'name' => 'test1ACB',
                        ),
                    ),
                    'generic_typo'    => array(),
                    'generic_synonym' => array(),
                    'brand'           => array(),
                    'brand_typo' => array(),
                    'brand_synonym'   => array(
                        array(
                            'id'   => 2,
                            'name' => 'test2',
                        ),
                    ),
                ),
                array(
                    'experimental' => array(),
                    'generic'      => array(5 => 0, 1 => 0),
                    'brand'        => array(2 => 0, 3 => 0, 4 => 0),
                ),
            ),
            array( // 3
                2,
                array(
                    'name'         => 'test1ACB||test2||test3||test4||test5||test6',
                    'experimental' => 'THIS-IS-GENERATED-UUID',
                    'generic'      => '5||1',
                    'brand'        => '2||3||4',
                    'source'       => '200gt||200bs||100b||100b||100b||100b',
                    'locked'       => '2',
                ),
                array(
                    'id'     => 5,
                    'family' => 'generic',
                    'name'   => 'test1ACB / test2...',
                    'desc'   => 'a reference to Generic Drug',
                ),
                array(
                    'experimental'    => array(),
                    'generic'         => array(),
                    'generic_typo'    => array(
                        array(
                            'id'   => 5,
                            'name' => 'test1ACB',
                        ),
                    ),
                    'generic_synonym' => array(),
                    'brand' => array(
                        array(
                            'id'   => 3,
                            'name' => 'test3',
                        ),
                        array(
                            'id'   => 4,
                            'name' => 'test4 / test5',
                        ),
                    ),
                    'brand_typo' => array(),
                    'brand_synonym' => array(
                        array(
                            'id'   => 2,
                            'name' => 'test2',
                        ),
                    ),
                ),
                array(
                    'experimental' => array(),
                    'generic'      => array(5 => 0, 1 => 0),
                    'brand'        => array(2 => 0, 3 => 0, 4 => 0),
                ),
            ),
            array( // 4
                2,
                array(
                    'name'         => 'test1ACB||test2||test3||test4||test5',
                    'experimental' => 'THIS-IS-GENERATED-UUID',
                    'generic'      => '5||1',
                    'brand'        => '2||3||4',
                    'source'       => '200gs',
                    'locked'       => '2',
                ),
                array(
                    'id'     => 5,
                    'family' => 'generic',
                    'name'   => 'test1ACB / test2...',
                    'desc'   => 'a reference to Generic Drug',
                ),
                array(
                    'experimental'    => array(),
                    'generic'         => array(),
                    'generic_typo'  => array(),
                    'generic_synonym' => array(
                        array(
                            'id'   => 5,
                            'name' => 'test1ACB',
                        ),
                    ),
                    'brand'         => array(),
                    'brand_typo'    => array(),
                    'brand_synonym' => array(),
                ),
                array(
                    'experimental' => array(),
                    'generic'      => array(5 => 0, 1 => 0),
                    'brand'        => array(2 => 0, 3 => 0, 4 => 0),
                ),
            ),
            array( // 5
                2,
                array(
                    'name'         => 'test1ACB||test2||test3||test4||test5',
                    'experimental' => 'THIS-IS-GENERATED-UUID',
                    'generic'      => '5||1',
                    'brand'        => '2||3||4',
                    'source'       => '100b',
                    'locked'       => '2',
                ),
                array(
                    'id'     => 2,
                    'family' => 'brand',
                    'name'   => 'test1ACB / test2...',
                    'desc'   => 'a Brand Drug',
                ),
                array(
                    'experimental'    => array(),
                    'generic'         => array(),
                    'generic_typo'    => array(),
                    'generic_synonym' => array(),
                    'brand'           => array(
                        array(
                            'id'   => 2,
                            'name' => 'test1ACB',
                        ),
                    ),
                    'brand_typo'    => array(),
                    'brand_synonym' => array(),
                ),
                array(
                    'experimental' => array(),
                    'generic'      => array(5 => 0, 1 => 0),
                    'brand'        => array(2 => 0, 3 => 0, 4 => 0),
                ),
            ),
            array( // 6
                2,
                array(
                    'name'         => 'test1ACB||test2||test3||test4||test5',
                    'experimental' => 'THIS-IS-GENERATED-UUID',
                    'generic'      => 'ASBC-AADDB-23451234',
                    'brand'        => '2||3||4',
                    'source'       => '',
                    'locked'       => '2',
                ),
                array(),
                array(),
                array(
                    'experimental' => array(),
                    'generic'      => array(),
                    'brand'        => array(2 => 0, 3 => 0, 4 => 0),
                ),
            ),
            array( // 7
                1,
                array(
                    'name'         => '||||test32',
                    'experimental' => 'THIS-IS-GENERATED-UUID',
                    'generic'      => 'ASBC-AADDB-23451234',
                    'brand'        => '2||3||4',
                    'source'       => '100b',
                    'locked'       => '2',
                ),
                array(
                    'id'     => 2,
                    'family' => 'brand',
                    'name'   => '...',
                    'desc'   => 'a Brand Drug',
                ),
                array(),
                array(
                    'experimental' => array(),
                    'generic'      => array(),
                    'brand'        => array(2 => 0, 3 => 0, 4 => 0),
                ),
            ),
            array( // 8
                2,
                array(
                    'name'         => 'test1||test2||test3||test4||test5||test6',
                    'experimental' => 'THIS-IS-GENERATED-UUID',
                    'generic'      => 'ASBC-AADDB-23451234',
                    'brand'        => '1||9||3||5||4||7',
                    'source'       => '100b||200bs||200bs||100b||200bs||100b',
                    'locked'       => '1',
                ),
                array(
                    'id'     => 1,
                    'family' => 'brand',
                    'name'   => 'test1 / test2...',
                    'desc'   => 'a Brand Drug',
                ),
                array(
                    'experimental'    => array(),
                    'generic'         => array(),
                    'generic_typo'    => array(),
                    'generic_synonym' => array(),
                    'brand'           => array(
                        array(
                            'id'   => 1,
                            'name' => 'test1',
                        ),
                        array(
                            'id'   => 5,
                            'name' => 'test4',
                        ),
                        array(
                            'id'   => 7,
                            'name' => 'test6',
                        ),
                    ),
                    'brand_typo' => array(),
                    'brand_synonym' => array(
                        array(
                            'id'   => 9,
                            'name' => 'test2',
                        ),
                        array(
                            'id'   => 3,
                            'name' => 'test3',
                        ),
                        array(
                            'id'   => 4,
                            'name' => 'test5',
                        ),
                    ),
                ),
                array(
                    'experimental' => array(),
                    'generic'      => array(),
                    'brand'        => array(1 => 0, 9 => 0, 3 => 0, 5 => 0, 4 => 0, 7 => 0),
                ),
            ),
        );
    }

    /**
     * @dataProvider getExportHeadingsProvider
     *
     * @testdox Checking prepareExportHeadings(): Stacks
     */
    public function testPrepareExportHeadings($complete, $maxKeywordCount, $expects)
    {
        $result = DRHelpers::prepareExportHeadings($complete, $maxKeywordCount);
        $this->assertSame($expects, $result);
    }

    public function getExportHeadingsProvider()
    {
        return array(
            array( // 0
                false, array(),
                array(
                    'Experimental-Id',
                    'Experimental-Name',
                    'Generic-Id',
                    'Generic-Name',
                    'Brand-Id',
                    'Brand-Name',
                ),
            ),
            array( // 1
                true, array(
                    'generic' => array('synonym' => 0, 'typo' => 0),
                    'brand'   => array('synonym' => 0, 'typo' => 0),
                ),
                array(
                    'Experimental-Id',
                    'Experimental-Name',
                    'Experimental-Type',
                    'Experimental-Valid',
                    'Experimental-UUID',
                    'Experimental-Comments',
                    'Experimental-Creation-Date',
                    'Generic-Id',
                    'Generic-Name',
                    'Generic-Valid',
                    'Generic-UUID',
                    'Generic-Comments',
                    'Generic-Creation-Date',
                    'Generic-Level-1',
                    'Generic-Level-2',
                    'Generic-Level-3',
                    'Generic-Level-4',
                    'Generic-Level-5',
                    'Generic-No-Double-Bounce',
                    'Generic-Auto-Encode-Exclude',
                    'Brand-Id',
                    'Brand-Name',
                    'Brand-Valid',
                    'Brand-UUID',
                    'Brand-Comments',
                    'Brand-Creation-Date',
                    'Brand-Auto-Encode-Exclude',
                ),
            ),
            array( // 2
                true, array(
                    'generic' => array('synonym' => 3, 'typo' => 5),
                    'brand'   => array('synonym' => 1, 'typo' => 7),
                ),
                array(
                    'Experimental-Id',
                    'Experimental-Name',
                    'Experimental-Type',
                    'Experimental-Valid',
                    'Experimental-UUID',
                    'Experimental-Comments',
                    'Experimental-Creation-Date',
                    'Generic-Id',
                    'Generic-Name',
                    'Generic-Valid',
                    'Generic-UUID',
                    'Generic-Comments',
                    'Generic-Creation-Date',
                    'Generic-Level-1',
                    'Generic-Level-2',
                    'Generic-Level-3',
                    'Generic-Level-4',
                    'Generic-Level-5',
                    'Generic-No-Double-Bounce',
                    'Generic-Auto-Encode-Exclude',
                    'Generic-Synonym-1',
                    'Generic-Synonym-Country-1',
                    'Generic-Synonym-2',
                    'Generic-Synonym-Country-2',
                    'Generic-Synonym-3',
                    'Generic-Synonym-Country-3',
                    'Generic-Typo-1',
                    'Generic-Typo-2',
                    'Generic-Typo-3',
                    'Generic-Typo-4',
                    'Generic-Typo-5',
                    'Brand-Id',
                    'Brand-Name',
                    'Brand-Valid',
                    'Brand-UUID',
                    'Brand-Comments',
                    'Brand-Creation-Date',
                    'Brand-Auto-Encode-Exclude',
                    'Brand-Synonym-1',
                    'Brand-Synonym-Country-1',
                    'Brand-Typo-1',
                    'Brand-Typo-2',
                    'Brand-Typo-3',
                    'Brand-Typo-4',
                    'Brand-Typo-5',
                    'Brand-Typo-6',
                    'Brand-Typo-7',
                ),
            ),
        );
    }

    /**
     * @dataProvider getDataSetKeysProvider
     *
     * @testdox Checking readDataSetKeys(): Stacks
     */
    public function testReadDataSetKeys($string, $expects)
    {
        $result = DRHelpers::readDataSetKeys($string);
        $this->assertSame($expects, $result);
    }

    public function getDataSetKeysProvider()
    {
        return array(
            array( // 0
                '1|2|3',
                array(
                    'experimental' => 1,
                    'generic'      => 2,
                    'brand'        => 3,
                ),
            ),
            array( // 1
                '1||3',
                array(
                    'experimental' => 1,
                    'generic'      => 0,
                    'brand'        => 3,
                ),
            ),
            array( // 2
                '1||',
                array(
                    'experimental' => 1,
                    'generic'      => 0,
                    'brand'        => 0,
                ),
            ),
            array( // 3
                '|2|',
                array(
                    'experimental' => 0,
                    'generic'      => 2,
                    'brand'        => 0,
                ),
            ),
            array( // 4
                '5|2|2',
                array(
                    'experimental' => 5,
                    'generic'      => 2,
                    'brand'        => 2,
                ),
            ),
        );
    }
}
