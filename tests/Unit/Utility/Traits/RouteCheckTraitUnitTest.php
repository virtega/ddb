<?php

namespace App\Tests\Unit\Utility\Traits;

use App\Utility\Traits\RouteCheckTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @testdox UNIT | Utility Traits | RouteCheckTrait
 */
class RouteCheckTraitUnitTest extends TestCase
{
    use RouteCheckTrait;

    /**
     * @dataProvider getApiPathProvider
     *
     * @testdox validateRequestOnApi(): Stacks
     */
    public function testValidateRequestOnApi($result, $args, ?array $attributes = null)
    {
        $request = Request::create($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6]);

        if (!empty($attributes)) {
            foreach ($attributes as $key => $val) {
                $request->attributes->set($key, $val);
            }
        }

        $this->assertSame($result, self::validateRequestOnApi($request));
    }

    /**
     * param[0] string               $uri        The URI
     * param[1] string               $method     The HTTP method
     * param[2] array                $parameters The query (GET) or request (POST) parameters
     * param[3] array                $cookies    The request cookies ($_COOKIE)
     * param[4] array                $files      The request files ($_FILES)
     * param[5] array                $server     The server parameters ($_SERVER)
     * param[6] string|resource|null $content    The raw body data
     */
    public function getApiPathProvider()
    {
        return array(
            array(true,     array('/v1/hello',          'GET', [], [], [], [], null), []),
            array(true,     array('/v1.0/hello',        'GET', [], [], [], [], null), []),
            array(true,     array('/v1.1',              'GET', [], [], [], [], null), []),
            array(true,     array('/v1.123',            'GET', [], [], [], [], null), []),
            array(true,     array('/v3.23',             'GET', [], [], [], [], null), []),
            array(true,     array('/v4123.2342346.123', 'GET', [], [], [], [], null), []),
            array(false,    array('/validate/hello',    'GET', [], [], [], [], null), []),
            array(false,    array('/validate1.0/hello', 'GET', [], [], [], [], null), []),
            array(true,     array('/v1/hello',          'GET', [], [], [], [], null), ['_route' => 'api_dummy']),
            array(true,     array('/validate1.0/hello', 'GET', [], [], [], [], null), ['_route' => 'api_dummy']),
        );
    }
}
