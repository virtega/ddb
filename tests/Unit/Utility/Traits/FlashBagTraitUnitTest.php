<?php

namespace App\Tests\Unit\Utility\Traits;

use App\Utility\Traits\FlashBagTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

/**
 * @testdox UNIT | Utility Traits | FlashBagTrait
 */
class FlashBagTraitUnitTest extends TestCase
{
    use FlashBagTrait;

    /**
     * @testdox Set off without Session on Request
     */
    public function testNonSessionOnRequest()
    {
        $request = Request::create('/v1/hello', 'GET');

        self::setOffFlashMsgByRequest($request, 'info', 'dummy-info');

        $this->assertFalse($request->hasSession());
    }

    /**
     * @testdox Set off with Session Request
     */
    public function testWithSessionOnRequest()
    {

        $request = Request::create('/v1/hello', 'GET');

        $session = new Session(new MockArraySessionStorage());
        $request->setSession($session);

        self::setOffFlashMsgByRequest($request, 'info', 'dummy-info');

        $this->assertTrue($request->hasSession());

        $flashBag = $request->getSession()->getFlashBag();
        $flashes  = $flashBag->peekAll();

        $this->assertNotEmpty($flashes);
        $this->assertNotEmpty($flashes['info']);
        $this->assertSame('dummy-info', $flashes['info'][0]);
    }
}
