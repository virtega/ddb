parameters:
    locale: 'en'
    APP_ENV: '%env(string:APP_ENV)%'
    env(APP_DEV_NICE_EXCEPTION): false
    env(SEARCH_LIMIT_SEARCHPAGE): 1000
    env(SEARCH_SUB_LIMIT_SEARCHPAGE): 5
    env(SEARCH_LIMIT_AUTOCOMPLETE): 10
    env(SEARCH_SUB_LIMIT_AUTOCOMPLETE): 3
    env(REMOTESPAWNPROC_TIMEOUT): NULL
    env(MSSQLREMOTECONN_TIMEOUT): 300
    env(CONFERENCE_DB_SCHEMA): 'dbo'
    env(CONFERENCE_DB_PRIMARY_TABLE): 'CRC'
    env(CONFERENCE_DB_SECONDARY_TABLE): 'CRC_Detail'
    env(CONFERENCE_DB_UPSYNC): true
    env(JOURNAL_DB_SCHEMA): 'dbo'
    env(JOURNAL_DB_PRIMARY_TABLE): 'Journal'
    env(JOURNAL_DB_SECONDARY_TABLE): 'Journal_Detail'
    env(JOURNAL_DB_SPECIALTIES_TABLE): 'JournalSpecialties'
    env(JOURNAL_DB_UPSYNC): true
    env(ES_QUERY_MAX_RESULTS): 10000
    env(FB_X_DAYS_SEARCH): 7
services:
    _defaults:
        autowire: true
        autoconfigure: true
        public: false
        bind:
            $appEnv: '%env(string:APP_ENV)%'
            $appDevNiceExp: '%env(bool:APP_DEV_NICE_EXCEPTION)%'
    Symfony\Component\Ldap\Adapter\ExtLdap\Adapter:
        arguments:
            -   host: '%env(string:LDAP_HOST)%'
                port: '%env(int:LDAP_PORT)%'
                encryption: '%env(string:LDAP_ENCRYPT)%'
                options:
                    protocol_version: 3
                    referrals: 0
    Symfony\Component\Ldap\Ldap:
        arguments: [ '@Symfony\Component\Ldap\Adapter\ExtLdap\Adapter' ]
    app_remote_mssql.conference:
        class: App\Utility\MsSqlRemoteConnection
        autowire: true
        arguments:
            $dbDsn: '%env(string:CONFERENCE_DB_DSN)%'
            $dbUser: '%env(string:CONFERENCE_DB_USER)%'
            $dbPwd: '%env(string:CONFERENCE_DB_PASSWORD)%'
            $optDriver: !php/const App\Utility\MsSqlRemoteConnection::DRIVER
            $optTimeout: '%env(int:MSSQLREMOTECONN_TIMEOUT)%'
    app_remote_mssql.journal:
        class: App\Utility\MsSqlRemoteConnection
        autowire: true
        arguments:
            $dbDsn: '%env(string:JOURNAL_DB_DSN)%'
            $dbUser: '%env(string:JOURNAL_DB_USER)%'
            $dbPwd: '%env(string:JOURNAL_DB_PASSWORD)%'
            $optDriver: !php/const App\Utility\MsSqlRemoteConnection::DRIVER
            $optTimeout: '%env(int:MSSQLREMOTECONN_TIMEOUT)%'
    App\:
        resource: '../src/*'
        exclude: '../src/{Entity,EventListener,Migrations,Tests,Kernel.php,*/Factory}'
    App\Controller\:
        resource: '../src/Controller'
        tags: [ 'controller.service_arguments' ]
    App\Command\ConferencesSyncDownCommand:
        arguments:
            $spawnTimeout: '%env(int:REMOTESPAWNPROC_TIMEOUT)%'
    App\Command\JournalsSyncDownCommand:
        arguments:
            $spawnTimeout: '%env(int:REMOTESPAWNPROC_TIMEOUT)%'
    App\EventListener\CacheWarmerListener:
        calls:
            - method: setKernel
              arguments:
                    - '@kernel'
        tags:
            - { name: kernel.cache_warmer, priority: -10 }
    App\EventListener\MaintenanceListener:
       arguments:
           - "%kernel.project_dir%/.maintenance.lock"
       tags:
           - { name: kernel.event_listener, event: kernel.request, method: onKernelRequest }
    App\EventListener\ExceptionListener:
        tags:
            - { name: kernel.event_listener, event: kernel.exception, priority: 1 }
    App\Service\ConferenceSourceService:
        arguments:
            $remote: '@app_remote_mssql.conference'
            $upSync: '%env(bool:CONFERENCE_DB_UPSYNC)%'
            $dbSchema: '%env(string:CONFERENCE_DB_SCHEMA)%'
            $tblPrimary: '%env(string:CONFERENCE_DB_PRIMARY_TABLE)%'
            $tblSecondary: '%env(string:CONFERENCE_DB_SECONDARY_TABLE)%'
    App\Service\JournalSourceService:
        arguments:
            $remote: '@app_remote_mssql.journal'
            $upSync: '%env(bool:JOURNAL_DB_UPSYNC)%'
            $dbSchema: '%env(string:JOURNAL_DB_SCHEMA)%'
            $tblPrimary: '%env(string:JOURNAL_DB_PRIMARY_TABLE)%'
            $tblSecondary: '%env(string:JOURNAL_DB_SECONDARY_TABLE)%'
            $tblSpecialties: '%env(string:JOURNAL_DB_SPECIALTIES_TABLE)%'
    App\Service\MailService:
        arguments:
            $mailFrom: '%env(string:MAILER_FROM)%'
    App\Service\LotusService:
        arguments:
            $lotusDbDsn: '%env(string:LOTUS_DB_DSN)%'
            $lotusDbUser: '%env(string:LOTUS_DB_USER)%'
            $lotusDbPwd: '%env(string:LOTUS_DB_PASSWORD)%'
            $lotusDbSync: '%env(bool:LOTUS_DB_UPSYNC)%'
    App\Service\SearchService:
        arguments:
            $searchPgLimit: '%env(int:SEARCH_LIMIT_SEARCHPAGE)%'
            $searchPgSubLimit: '%env(int:SEARCH_SUB_LIMIT_SEARCHPAGE)%'
            $searchAcLimit: '%env(int:SEARCH_LIMIT_AUTOCOMPLETE)%'
            $searchAcSubLimit: '%env(int:SEARCH_SUB_LIMIT_AUTOCOMPLETE)%'
    App\Security\Http\UserSessionLongevityEventListener:
        tags:
           - { name: kernel.event_listener, event: kernel.request, method: onKernelRequest, priority: 0 }
    App\Security\RestLdapAuthenticator:
        arguments:
            $securityApiToken: '%env(string:SECURITY_API_TOKEN)%'
    App\Security\User\LdapUserProvider:
        arguments:
            $ldap: '@Symfony\Component\Ldap\Ldap'
            $additionalQuery: '%env(string:LDAP_EXTRA_QUERY)%'
            $baseDn: '%env(string:LDAP_BASE_DN)%'
    App\Utility\Crypto:
        arguments:
            $appSecret: '%env(string:APP_SECRET)%'
    App\Utility\LotusConnection:
        arguments:
            $lotusDbDsn: '%env(string:LOTUS_DB_DSN)%'
            $lotusDbUser: '%env(string:LOTUS_DB_USER)%'
            $lotusDbPwd: '%env(string:LOTUS_DB_PASSWORD)%'
            $lotusDbSync: '%env(bool:LOTUS_DB_UPSYNC)%'
    App\Service\ESDGService:
        factory: ['App\Service\Factory\ESDGServiceFactory', create]
        arguments:
            $awsKey: '%env(string:AWS_KEY)%'
            $awsSecret: '%env(string:AWS_SECRET)%'
            $awsRegion: '%env(string:AWS_REGION)%'
            $hosts: '%env(string:ES_HOSTS)%'
            $ESIndex: '%env(string:ES_INDEX)%'
            $ESMaxResults: '%env(int:ES_QUERY_MAX_RESULTS)%'
            $FBXDaysSearch: '%env(int:FB_X_DAYS_SEARCH)%'