(function($) {
    'use strict';

    var ConJourSearch = function (exportElement) {
        this.exportElement = exportElement;
        this.searchType = '';
        this.searchEndpoint = {
            journal   : $.ajaxSetup()['url'] + '/v1/journal-search-export-csv',
            conference: $.ajaxSetup()['url'] + '/v1/conference-search-export-csv',
        };
        this.searchAttrs = {};
        this.init();
    };

    ConJourSearch.prototype = {
        init: function() {
            var that = this;
            $(that.exportElement).on('click', function(e) {
                e.preventDefault();
                var fields = $('.search-form').find('select,input');
                $(fields).each(function(idx, ele) {
                    var theValue = that.getValue(ele);
                    if (null !== theValue) {
                        that.searchAttrs[that.getName(ele)] = that.getValue(ele)
                    }
                    if ((idx + 1) == fields.length) {
                        that.submitExportRequest();
                    }
                });
            });

            if ($('form.search-form').length) {
                this.searchType = $('form.search-form').last().attr('id');
                this.searchType = this.searchType.replace('s-search', '');
                if (typeof this.searchEndpoint[this.searchType] != 'undefined')
                $(this.exportElement).show();
            }
        },
        getName: function(element) {
            var fallback = ['name', 'id', 'type'];
            var name;

            for (var i = 0; i < fallback.length; i++) {
                if (typeof name == 'undefined') {
                    name = $(element).attr(fallback[i]);
                }
            }

            name = name.replace('[]', '');

            return name;
        },
        getValue: function(element) {
            var value = null;

            if (
                ($(element).prop('nodeName') == 'INPUT') &&
                ($(element).prop('type') == 'text')
            ) {
                value = $(element).val();
            }
            else if (
                ($(element).prop('nodeName') == 'INPUT') &&
                ($(element).prop('type') == 'radio') &&
                ($(element).is(':checked'))
            ) {
                value = $(element).val();
            }
            else if ($(element).prop('nodeName') == 'SELECT') {
                value = $(element).val();
            }

            return value;
        },
        submitExportRequest: function() {
            var that = this;
            Swal.fire({
                title: 'The export file will be sent to your email',
                html: [
                    'Provide your email',
                ].join(''),
                input: 'email',
                inputPlaceholder: 'Your email address',
                inputValue: (CONST_USERMAIL || ''),
                inputValidator: function(result) {
                    if (result.trim().length == 0) {
                        return 'Missing email';
                    }
                },
                showCancelButton: true,
            }).then(function (result) {
                if (result.value) {
                    Swal.fire({
                        title: 'Sending export request...',
                        type: 'info',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showCloseButton: false,
                        allowEscapeKey: false,
                        onBeforeOpen: function() {
                            Swal.showLoading();
                            $.ajax({
                                url    : that.searchEndpoint[that.searchType],
                                method : "POST",
                                dataType: 'json',
                                data   : {
                                    email : result.value.trim(),
                                    query : that.searchAttrs,
                                },
                                success: function(data, text, response) {
                                    Swal.fire(
                                        'Export requested!',
                                        'Please check your Email Inbox.',
                                        'success'
                                    );
                                },
                            });
                        },
                    });
                }
            });
        },
    };

    $(document).ready(function($) {
        $('#help-text-toggle').on('click', function(e) {
            e.preventDefault();
            $('.help-text').toggle();
        });

        $('#search-reset').bind('click', function(event) {
            // text box
            $(this).parents('form').find('input[type=text]')
                .val('')
                .attr('value', '');
            // selectbox
            $(this).parents('form').find('select[data-init-plugin=select2]').each(function(idx, ele) {
                $(ele).find('option')
                    .attr('selected', false);
                $(ele).val(null).trigger('change');
            });
        });

        new ConJourSearch($('#export-csv'));
    });
})(window.jQuery);