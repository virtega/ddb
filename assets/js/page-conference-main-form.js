$(document).ready(function() {
    /**
     * DATES
     */
    var datetimepickercfg = {
        allowInputToggle: true,
        buttons: {
            showClear: true,
        },
        format: 'YYYY-MM-DD',
        sideBySide: true,
        useCurrent: 'day',
        viewMode: 'days',
        icons: {
            clear: 'far fa-trash-alt'
        }
    };
    $('#startdate-wrapper').datetimepicker(
        Object.assign(datetimepickercfg, {widgetParent: $('#start-datetimepicker-area')})
    );
    $('#enddate-wrapper').datetimepicker(
        Object.assign(datetimepickercfg, {widgetParent: $('#end-datetimepicker-area')})
    );
    $("#startdate-wrapper").on("change.datetimepicker", function (event) {
        $('#enddate-wrapper').datetimepicker('minDate', event.date);
    });
    $("#enddate-wrapper").on("change.datetimepicker", function (event) {
        $('#startdate-wrapper').datetimepicker('maxDate', event.date);
    });

    /**
     * LOCALITY with auto-fill control
     */
    /**
     * Reset Country selection box options.
     *
     * @param array countryList  If empty, it will query ajax for complete list.
     */
    function replenishCountries(countryList)
    {
        countryList = countryList || [];
        var selectedCountry = $('#country-select').val() || '';

        if (countryList.length == 0) {
            $.ajax({
                url: $.ajaxSetup()['url'] + '/v1/get-country-list',
                type: 'GET',
                suppressErrors: true,
                async: false,
                success: function(data) {
                    replenishCountries(data.data);
                }
            });
        }
        else {
            $('#country-select').select2('destroy');
            $('#country-select').find('option').remove();
            $('#country-select').append(
                $('<option></option>').attr('value', '')
            );
            $.each(countryList, function(idx, country) {
                $('#country-select').append(
                    $('<option></option>')
                        .attr('value', (country.iso_code || country.isoCode))
                        .prop('selected', ((country.iso_code || country.isoCode) == selectedCountry))
                        .html(country.name)
                );

                if (idx == (countryList.length - 1)) {
                    $('#country-select').select2();
                }
            });
        }
    }

    /**
     * Method handling as City Selection
     * - Short-list Countries to selected City had multiple Country.
     * - Apply Country, which will trigger update Region.
     */
    $('#city-select').on('change', function(event) {
        var selectObject = $(this);

        if ($(selectObject).data('update') === '#ignore') {
            $(selectObject).data('update', false);
        }
        else if ($('#country-auto').is(':checked')) {
            $.ajax({
                url: $.ajaxSetup()['url'] + '/v1/get-a-city-countries',
                type: 'POST',
                data: {
                    city: $(selectObject).val(),
                },
                suppressErrors: true,
                async: false,
                success: function(data) {
                    if (data.count == 1) {
                        $(selectObject).data('update', '#ignore');
                        $('#country-select')
                            .val(data.data[0].iso_code)
                            .trigger('change');
                    }
                    else if (data.count > 1) {
                        // more than 1, then short list country
                        $(selectObject).data('update', '#ignore');
                        replenishCountries(data.data);
                        setTimeout(function() {
                            $('#country-select').select2('open');
                        }, 50);
                    }
                }
            });
        }
    });

    /**
     * Method handling as Country Selection
     * - Apply City and Region, if flow checked
     */
    var initialCountryCount = $('#country-select option').length;
    $('#country-select').on('change', function(event) {
        var selectObject = $(this);

        if ($(selectObject).data('update') === '#ignore') {
            $(selectObject).data('update', false);
        }
        else if ($('#country-auto').is(':checked')) {
            $.ajax({
                url: $.ajaxSetup()['url'] + '/v1/get-a-country-details',
                type: 'POST',
                data: {
                    country_iso: $(selectObject).val(),
                },
                suppressErrors: true,
                async: false,
                success: function(data) {
                    if (data.data.country.iso_code !== undefined) {
                        $('#city-select')
                            .val(data.data.country.capital_city)
                            .data('update', '#ignore')
                            .trigger('change');
                    }

                    if ($(selectObject).data('update') === '#ignore-region') {
                        $(selectObject).data('update', false);
                    }
                    else if (
                        (data.data.regions.length > 0) &&
                        (data.data.regions[0][ (data.data.regions[0].length - 1) ].id !== undefined)
                    ) {
                        $('#region-select')
                            .val(data.data.regions[0][ (data.data.regions[0].length - 1) ].id)
                            .data('update', '#ignore')
                            .trigger('change');
                    }

                    if ($('#country-select option').length != initialCountryCount) {
                        // wait, let select2 settle first
                        setTimeout(function() {
                            replenishCountries([]);
                        }, 100);
                    }
                }
            });
        }
    });

    /**
     * Method handling as Region change.
     * - Short-list Countries to selected Region.
     * - Apply Country, which will trigger update City.
     */
    $('#region-select').on('change', function(event) {
        if ($(this).data('update') === '#ignore') {
            $(this).data('update', false);
        }
        else if ($('#country-auto').is(':checked')) {
            $.ajax({
                url: $.ajaxSetup()['url'] + '/v1/get-a-region-countries',
                type: 'POST',
                data: {
                    region_id: $(this).val(),
                },
                suppressErrors: true,
                success: function(data) {
                    $('#country-select').data('update', '#ignore-region');
                    replenishCountries(data.data);
                    setTimeout(function() {
                        $('#country-select').select2('open');
                    }, 50);
                }
            });
        }
    });

    /**
     * CONTACTS
     */
    $('#contact-contactphone-repeater, #contact-contactemail-repeater, #contact-contactfax-repeater')
        .find('.contact-repeater-action button[data-repeater-create]')
        .on('click', function(event) {
            event.preventDefault();
        });
    $('#contact-contactphone-repeater, #contact-contactemail-repeater, #contact-contactfax-repeater')
        .repeater({
            initEmpty: false,
            isFirstItemUndeletable: true
        });

    /**
     * LINKS
     */
    $('#guide-links-repeater')
        .find('.guide-links-repeater-action button[data-repeater-create]')
        .on('click', function(event) {
            event.preventDefault();
        });
    $('#guide-links-repeater')
        .repeater({
            initEmpty: false,
            isFirstItemUndeletable: true,
        });

    /**
     * REMOVAL
     */
    $('#remove-conference').on('click', function(event) {
        event.preventDefault();
        var href = $(this).attr('href');
        Swal.fire({
            title: "Removing this Conference",
            html: [
                '<h4 class="m-t-0 m-b-1">You are sure to remove this record?</h4>',
                '<span class="text-muted">There are no recovery from this.</span>',
            ].join(''),
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, remove it!",
            showLoaderOnConfirm: true,
            preConfirm: (function(value) {
                if (value !== false) {
                    window.location.href = href;
                }
            })
        });
    });
});