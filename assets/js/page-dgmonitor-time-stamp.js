(function($) {
    'use strict';
    $(document).ready(function () {
        $('#date-search').datetimepicker({
            'format': 'YYYY-MM-DD',
            'maxDate': 'now',
        });

        var dataTable = $('#dgmonitor-time-stamp').DataTable({
            "order":[[0, 'desc']],
            "oLanguage": {
                "sSearch": "Find in results:"
            }
        });

        $('#searchByDateBtn').on('click', function(){
            var dateValid = isValidDate();
            if (false === dateValid) {
                Toast.fire('Error', 'Please enter a valid date.', 'error');
                return false;
            }
            else if (null === dateValid) {
                Swal.fire({
                    title: 'Invalid Date',
                    html: 'Date should be past or today.',
                    type: 'warning',
                });
                return false;
            }
            dataTable.destroy();
            dataTable = $('#dgmonitor-time-stamp').DataTable({
                "ajax": {
                    "url": $.ajaxSetup()['url'] + '/v1/dgmonitor-time-stamp',
                    "type": "POST",
                    "data": {'date': $('#date-search').datetimepicker('viewDate').format('YYYY-MM-DD')},
                },
                "columns": [
                    {
                        "title"    : "Updated",
                        "data"     : "updated",
                        "orderable": true,
                        "width"    : "20%",
                    },
                    {
                        "title"    : "Title",
                        "data"     : (function (row, type, val, meta) {
                            return '<span title="' + row.title + '">' + row.title + '</span>';
                        }),
                        "orderable": true,
                    },
                    {
                        "title"    : "Type",
                        "style"    : "text-align: center;",
                        "data"     : "type",
                        "orderable": true,
                        "width"    : "10%",
                    }
                ],
                "order":[[0, 'desc']],
                "oLanguage": {
                    "sSearch": "Find in results:"
                },
                "pagingType": "full_numbers",
                "language"  : {
                    "paginate": {
                        "first"   : "&laquo;",
                        "previous": "&lt;",
                        "next"    : "&gt;",
                        "last"    : "&raquo;"
                    }
                }
            });
            dataTable.on( 'xhr', function () {
                var json = dataTable.ajax.json();
                if (typeof json.message !== 'undefined'
                    && json.message !== '') {
                    Swal.fire({
                        title: 'Warning',
                        html: json.message,
                        type: 'warning',
                    });
                }
            });
        });

        function isValidDate()
        {
            if (typeof $('#date-search').datetimepicker('viewDate')._isValid !== "undefined") {
                if ($('#date-search').datetimepicker('viewDate')._isValid) {
                    var now = moment();
                    if (now >= $('#date-search').datetimepicker('viewDate')) {
                        return true;
                    }
                    return null;
                }
                return false;
            }
            return false;
        }
    });
})(window.jQuery);