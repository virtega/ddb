function init_dashboard() {
    $('#generic-with-fullcode-switch').on('change', function() {
        var newUri = $.ajaxSetup()['url'] + '/dashboard?has-code=' + ($(this).is(':checked') ? 'true' : 'false');
        var message = ($(this).is(':checked') ? 'with' : 'without');
        Pace.track(function() {
            $.ajax({
                url     : newUri,
                type    : 'GET',
                dataType: 'text',
                accepts : { html: 'text/html' },
                success: function(response) {
                    var newDashboard = $(response).find('.page-content-wrapper > .content > .jumbotron');
                    if (newDashboard) {
                        $('.page-content-wrapper > .content > .jumbotron').html(newDashboard);
                    }
                },
                error: function(xhr, status, error) {
                    window.location.href = newUri;
                },
                complete: function(jqXHR, textStatus) {
                    pagesReInitializeAll(false, init_dashboard());
                    Toast.success('Widgets Changed', 'Count ' + message + ' Full code');
                }
            });
        });
        Pace.restart();
    });
    $('#refesh-dashboard-widgets').on('click', function(e) {
        e.preventDefault();
        var secToWait = 5;
        Pace.track(function() {
            $.ajax({
                url     : $.ajaxSetup()['url']  + '/v1/widgets-update',
                type    : 'GET',
                beforeSend: function() {
                    var timerInterval;
                    Swal.fire({
                        title: 'Sending recalculate request',
                        html: 'Please wait <span id="swal-request-timer" data-time="' + secToWait + '">' + secToWait + '</span> seconds...',
                        type: 'info',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showCloseButton: false,
                        allowEscapeKey: false,
                        onBeforeOpen: function() {
                            Swal.showLoading();
                            timerInterval = setInterval(function() {
                                var indicator = Swal.getContent().querySelector('#swal-request-timer');
                                var timed = ($(indicator).data('time') - 1);
                                $(indicator).html(timed).data('time', timed);
                            }, 1000);
                        },
                        onClose: function() {
                            clearInterval(timerInterval);
                        }
                    });
                },
                complete: function(jqXHR, textStatus) {
                    var newUri = $.ajaxSetup()['url'] + '/dashboard?fresh&has-code=' + ($('#generic-with-fullcode-switch').is(':checked') ? 'true' : 'false');
                    setTimeout(function() {
                        $.ajax({
                            url     : newUri,
                            type    : 'GET',
                            dataType: 'text',
                            accepts : { html: 'text/html' },
                            success: function(response) {
                                Swal.close();
                                var newDashboard = $(response).find('.page-content-wrapper > .content > .jumbotron');
                                if (newDashboard) {
                                    $('.page-content-wrapper > .content > .jumbotron').html(newDashboard);
                                }
                            },
                            error: function(xhr, status, error) {
                                window.location.href = newUri;
                            },
                            complete: function(jqXHR, textStatus) {
                                pagesReInitializeAll(false, init_dashboard());
                                Toast.success('Widgets Updated', 'Data has been recalculated.');
                            }
                        });
                    }, (secToWait * 1000));
                }
            });
        });
        Pace.restart();
    });
}
$(document).ready(function() {
    init_dashboard();
});