$(document).ready(function() {
    // get templates
    var user_actions = $('#user-action-temp').html();
    $('#user-action-temp').remove();
    var role_selector = $('#role-selector-temp').html();
    $('#role-selector-temp').remove();
    var roles = {};
    $(role_selector).find('option').each(function(idx, ele) {
        roles[$(ele).attr('value')] = $(ele).html();
    });

    // set data table ajax
    var dataTable = $('#user-list').DataTable({
        "ajax"      : {
            "url" : $.ajaxSetup()['url'] + '/v1/user-roles-list',
            "type": "GET",
        },
        "columns": [
            {
                "title"    : "ID",
                "data"     : "id",
                "orderable": false,
                "width"    : "10%",
                "className": "id-cell"
            },
            {
                "title"    : "Username",
                "data"     : "username",
                "orderable": true,
                "width"    : "35%",
                "className": "username-cell"
            },
            {
                "title"    : "Role",
                "data"     : (function (row, type, val, meta) {
                    return row.role.substring(0, 1).toUpperCase() + row.role.substring(1, row.role.length);
                }),
                "orderable": true,
                "width"    : "25%",
                "className": "role-cell"
            },
            {
                "title"         : "Actions",
                "data"          : null,
                "defaultContent": user_actions,
                "orderable"     : false,
                "searchable"    : false,
                "width"         : "30%",
                "className"     : "action-cell"
            }
        ],
        "order": [1, "asc"],
        "pagingType": "full_numbers",
        "language"  : {
            "paginate": {
                "first"   : "&laquo;",
                "previous": "&lt;",
                "next"    : "&gt;",
                "last"    : "&raquo;"
            }
        }
    });

    // clean up table on draw
    dataTable.on('draw', function() {
        $('#user-list thead th.username-cell').attr('aria-label', 'Username');
        $('#user-list thead th.role-cell').attr('aria-label', 'Role');
    });

    // action on add new button click
    $('#add-new-role').on('click', function(e) {
        Swal.mixin({
            input: 'text',
            confirmButtonText: "Continue",
            progressSteps: ['1', '2'],
            onBeforeOpen: function() {
                Swal.hideProgressSteps();
            },
        }).queue([
            {
                title: "Adding new User",
                    html: [
                    '<h4 class="m-t-0 m-b-1">Please enter Username, ie: "John.Doe"</h4>',
                    '<span class="text-muted">No Domain prefix and case-insensitive.</span>',
                ].join(''),
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                inputPlaceholder: "Username",
                showCancelButton: true,
                inputValidator: (function(username) {
                    if (!username) {
                        return 'Username is required'
                    }

                    username = username.trim();

                    if (username.indexOf("@") >= 0) {
                        return 'Username only, remove email domain';
                    }

                    var invalid = username.match(/[^a-zA-Z0-9._]/);
                    if (null != invalid) {
                        return 'Contains invalid character: "' + invalid + '"';
                    }
                }),
            },
            {
                title: "Assigning Role",
                input: 'select',
                inputOptions: roles,
                inputPlaceholder: "Select Role",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                inputValidator: (function(role) {
                    if (!role) {
                        return 'Role is required'
                    }
                }),
            }
        ]).then(function(result) {
            if (result.value) {
                return new Promise(function(resolve) {
                    $.ajax({
                        url: $.ajaxSetup()['url'] + '/v1/set-user-role',
                        method: 'POST',
                        data: {
                            "username": result.value[0],
                            "role"    : result.value[1]
                        },
                        success: function(response) {
                            resolve((
                                (response || false) &&
                                (response.success || false)
                            ));
                        },
                    });
                });
            }
            return false;
        }).then(function(complete) {
            if (complete) {
                Toast.success('User Created', 'Role has been assigned');
                dataTable.ajax.reload();
            }
        });
    });

    // action on User Action Edit button click
    $('#user-list tbody').on('click', 'button.edit-role', function () {
        var data = dataTable.row( $(this).parents('tr') ).data();
        Swal.fire({
            title: 'Edit User Role',
            html: [
                '<h4 class="m-t-0 m-b-1">You are changing <strong>' + data.username + '</strong>\'s Role</h4>',
            ].join(''),
            input: 'select',
            inputOptions: roles,
            inputValue: data.role,
            inputPlaceholder: "Select Role",
            confirmButtonText: "Update User Role Record",
            showLoaderOnConfirm: true,
            inputValidator: (function(role) {
                if (!role) {
                    return 'Role is required'
                }
            }),
            preConfirm: (function(value) {
                if (value !== false) {
                    return new Promise(function(resolve) {
                        $.ajax({
                            url: $.ajaxSetup()['url'] + '/v1/set-user-role',
                            method: 'POST',
                            data: {
                                "username": data.username,
                                "role"    : value
                            },
                            success: function(response) {
                                resolve(response);
                            },
                        });
                    });
                }
            })
        }).then(function(result) {
            if (
                (result.value || false) &&
                (result.value.success || false)
            ) {
                Toast.success('Success', 'User Role has been updated');
                dataTable.ajax.reload();
            }
        });
    });

    // action on User Action Remove button click
    $('#user-list tbody').on('click', 'button.remove-role', function () {
        var data = dataTable.row( $(this).parents('tr') ).data();
        Swal.fire({
            title: "Are you sure?",
            html: [
                '<h4 class="m-t-0 m-b-1">You are removing <strong>' + data.username + '</strong></h4>',
                '<span class="text-muted">User will be a Visitor on next login.</span>',
            ].join(''),
            type: "question",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, remove it!",
            showLoaderOnConfirm: true,
            preConfirm: (function(value) {
                if (value !== false) {
                    return new Promise(function(resolve) {
                        $.ajax({
                            url: $.ajaxSetup()['url'] + '/v1/remove-user-role',
                            data  : {
                                username: data.username
                            },
                            success: function(response) {
                                resolve(response);
                            },
                        });
                    });
                }
            })
        }).then(function(result) {
            if (
                (result.value || false) &&
                (result.value.success || false)
            ) {
                Toast.success('User Removed', 'User record has been removed');
                dataTable.ajax.reload();
            }
        });
    });
});