(function($) {
    'use strict';

    var NcbiSearch = (function () {
        /**
         * @type String  API Endpoint to search by Title / Abbreviation
         */
        this.queryByNamePath  = $.ajaxSetup()['url'] + '/v1/ncbi-search-journal-by-name';

        /**
         * @type String  API Endpoint to search by ISSN
         */
        this.queryByIssnPath  = $.ajaxSetup()['url'] + '/v1/ncbi-search-journal-by-issn';

        /**
         * @type String  NCBI page URL with trailing for NLM UID page-view
         */
        this.ncbiViewPath     = 'https://www.ncbi.nlm.nih.gov/nlmcatalog/';

        /**
         * @type String  NCBI page URL to manually search Journal; if result return empty
         */
        this.ncbiManualSearch = 'https://www.ncbi.nlm.nih.gov/nlmcatalog/journals';

        /**
         * @type String  Search Field expects "title" / "abbreviation" / "issn"
         */
        this.searchField      = '';

        /**
         * @type String  Search Term
         */
        this.searchTerm       = '';

        /**
         * @type Number  Current Search Page
         */
        this.searchPage       = 1;

        /**
         * @type Number Maximum Results per Page
         */
        this.searchMaxPerPage = 10;

        /**
         * @type Array  API Endpoint response
         */
        this.searchResults    = {};

        /**
         * @type Number  Pagination listing count
         */
        this.paginationBuffer = 5;

        this.init();
    });
    NcbiSearch.prototype = {
        /**
         * Initiate plugin, assign action on search buttons.
         *
         * @return void
         */
        init: function() {
            var that = this;
            /**
             * FORM FIELDS AND SEARCH BUTTONS
             */
            $('a.ncbi-search').each(function(index, ele) {
                var input = $(ele).parents('.input-group').find('input');

                $(ele).on('click', function(e) {
                    that.beginSearch($(this).data('ncbi-field'), $(input).val());
                });

                if ('' != $(input).val()) {
                    that._enableFormBtn(ele);
                }
                else {
                    that._disableFormBtn(ele);
                }

                $(input).on('keyup', function(e) {
                    if ('' != $(this).val()) {
                        that._enableFormBtn( $(this).parent().find('a.ncbi-search') );
                    }
                    else {
                        that._disableFormBtn( $(this).parent().find('a.ncbi-search') );
                    }
                });

                $(input).on('keypress', function(e) {
                    if (
                        ('' != $(this).val()) &&
                        (13 == e.which)
                    ) {
                        e.preventDefault();
                        $(this).parent().find('a.ncbi-search').trigger('click');
                    }
                });
            });

            /**
             * SEARCH MODAL
             */
            $('#ncbi-search-modal')
                .on('shown.bs.modal', function (e) {
                    $('body').addClass('ncbi-search-modal-shown');
                })
                .on('hidden.bs.modal', function (e) {
                    $('body').removeClass('ncbi-search-modal-shown');
                });
        },
        /**
         * Method to kick off Search Process
         *
         * @param String  field
         * @param string  term
         *
         * @return void
         */
        beginSearch: function(field, term) {
            field = field || '';
            field = field.trim();
            if (-1 == $.inArray(field, ['title', 'abbreviation', 'issn'])) {
                Swal.fire('Not Allowed', 'Unknown search field.', 'error').then(function() {
                    $('#ncbi-search-modal').modal('hide');
                });
                return;
            }

            term = term || '';
            term = term.trim();
            if (term.length < 1) {
                Swal.fire('Not Allowed', 'Search term is empty.', 'error').then(function() {
                    $('#ncbi-search-modal').modal('hide');
                });
                return;
            }

            if (
                (this.searchField == field) &&
                (this.searchTerm == term)
            ) {
                // stop, just show loaded results
                return;
            }

            this.resetVarsHard();
            this.resetSearchOverlay();

            this.searchField = field;
            this.searchTerm  = term;

            this.doSearch();
        },
        /**
         * Method to start API EP query process
         *
         * @return void
         */
        doSearch: function() {
            $('#ncbi-search-modal .loading .found').hide();
            this.removePagination();
            this.removeResults();

            $('#ncbi-search-modal .info').show();
            $('#ncbi-search-field').html(this.searchField);
            $('#ncbi-search-term').html(this.searchTerm);
            if ('' == $('#ncbi-search-modal .loading .wait span').html()) {
                $('#ncbi-search-modal .loading .wait span').html('Searching...');
            }
            $('#ncbi-search-modal .loading .wait').show();

            var that = this;
            $.ajax({
                url   : ('issn' == this.searchField ? this.queryByIssnPath : this.queryByNamePath),
                method: "POST",
                data  : {
                    term: this.searchTerm,
                    page: this.searchPage,
                    maxperpage: this.searchMaxPerPage,
                },
                success: function(data, text, response) {
                    that.searchResults = data;

                    if (0 == data.count) {
                        that.__setManualSearchElementOnFound('No Journal found with given Term.');
                        return;
                    }

                    that.setupPagination();
                    that.setupResults();

                    if (1 == data.count) {
                        setTimeout(function() {
                            $('#ncbi-search-modal .results .list-group .list-group-item-action')
                                .eq(0)
                                .trigger('click');
                        }, 100);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.__setManualSearchElementOnFound('An expected error occur.');
                },
                complete: function(jqXHR, textStatus) {
                    $('#ncbi-search-modal .loading .wait span').html('');
                    $('#ncbi-search-modal .loading .wait').hide();
                },
            });
        },
        /**
         * Method to remove Pagination listing
         *
         * @return void
         */
        removePagination: function() {
            $('#ncbi-search-modal .loading .pagination .btn-group a.btn')
                .remove();
        },
        /**
         * Method to setup new Pagination listing, based on collected result
         *
         * @return void
         */
        setupPagination: function() {
            var that = this;
            if (that.searchResults.pagination.maxpage > 1) {
                var step = 1;
                var dotted = false;
                while (step <= that.searchResults.pagination.maxpage) {
                    // pagination splitter logic
                    if (
                        // first
                        (step < that.paginationBuffer) ||
                        // previous/next
                        (Math.abs(that.searchResults.pagination.page - step) < that.paginationBuffer) ||
                        // last
                        (Math.abs(that.searchResults.pagination.maxpage - step) < that.paginationBuffer)
                    ) {
                        var pgBtn = $('<a></a>')
                            .attr('href', '')
                            .addClass('btn')
                            .addClass('btn-secondary')
                            .attr('data-ncbi-page', step)
                            .html( that.__numberFormating(step) );

                        if (step == that.searchResults.pagination.page) {
                            $(pgBtn).addClass('active');
                        }

                        $(pgBtn).on('click', function(e) {
                            e.preventDefault();
                            that.searchPage = $(this).data('ncbi-page');
                            $('#ncbi-search-modal .loading .wait span').html('Loading page #' + that.searchPage + '...');
                            that.doSearch();
                        });

                        $('#ncbi-search-modal .loading .pagination .btn-group')
                            .append(pgBtn);

                        dotted = false;
                    }
                    else if (false === dotted) {
                        var pgBtn = $('<a></a>')
                            .attr('href', '')
                            .addClass('btn')
                            .addClass('btn-secondary')
                            .addClass('disabled')
                            .addClass('active')
                            .attr('disabled', true)
                            .html('...');

                        $('#ncbi-search-modal .loading .pagination .btn-group')
                            .append(pgBtn);

                        dotted = true;
                    }

                    step++;
                }

                $('#ncbi-search-modal .loading .pagination')
                    .show();

                $('#ncbi-search-modal .loading .found')
                    .html('Found '
                            + that.__numberFormating(that.searchResults.pagination.overall)
                            + ' related Journals, showing '
                            + that.searchResults.count
                            + ', on page '
                            + that.__numberFormating(that.searchResults.pagination.page)
                            + ' from '
                            + that.__numberFormating(that.searchResults.pagination.maxpage)
                            + ' pages.'
                    )
                    .show();
            }
            else {
                $('#ncbi-search-modal .loading .found')
                    .html('Found '
                            + that.__numberFormating(that.searchResults.pagination.overall)
                            + ' related Journals.'
                    )
                    .show();
            }
        },
        /**
         * Method to remove Journal listing on result listing
         *
         * @return void
         */
        removeResults: function() {
            $('#ncbi-search-modal .results .list-group .list-group-item-action')
                .remove();

            $('#ncbi-search-modal .loading .onncbi')
                .hide();
        },
        /**
         * Method to setup Journal on result listing
         *
         * @return void
         */
        setupResults: function() {
            var that = this;
            $.each(this.searchResults.data, function(uid, journal) {
                var resultRow = $('#result-list-template .list-group-item-action').clone();

                $(resultRow).attr('data-ncbi-uid', uid);

                $(resultRow).find('.j-ncbi-link')
                    .attr('href', that.ncbiViewPath + uid)
                    .show();

                var name = that._getJournalName(journal);
                $(resultRow).find('.j-name').html(name);
                if ('' == name) {
                    $(resultRow).find('.j-action-copy[data-pull-field=name]').attr('disabled', true);
                }

                var abbrv = that._getJournalAbbreviation(journal);
                $(resultRow).find('.j-abbr').html(abbrv);
                if ('' == abbrv) {
                    $(resultRow).find('.j-action-copy[data-pull-field=abbreviation]').attr('disabled', true);
                }

                var issn = that._getJournalIssn(journal);
                $(resultRow).find('.j-issn').html(issn);
                if ('' == issn) {
                    $(resultRow).find('.j-issn').parent().remove();
                    $(resultRow).find('.j-action-copy[data-pull-field=issn]').attr('disabled', true);
                }

                var publ = that._getJournalPublisher(journal);
                $(resultRow).find('.j-pub').html(publ);
                if ('' == publ) {
                    $(resultRow).find('.j-pub').parent().remove();
                    $(resultRow).find('.j-action-copy[data-pull-field=publisher]').attr('disabled', true);
                }

                var detailsList = {
                    1: {
                        'NLM UID'     : {
                            value  : journal.uid || '',
                            disable: false,
                        },
                        'Start Year'  : {
                            value  : journal.startyear || '',
                            disable: false,
                        },
                        'End Year'    : {
                            value  : journal.endyear || '',
                            disable: false,
                        },
                        'Date Revised': {
                            value  : journal.daterevised || '',
                            disable: false,
                        },
                    },
                    2: {
                        'Specialties' : {
                            value  : (journal.broadheading || []).join(', '),
                            disable: '.j-action-copy[data-pull-field=specialty]',
                        },
                        'Languages'   : {
                            value  : that._getJournalLanguages(journal).join(', '),
                            disable: '.j-action-copy[data-pull-field=language]',
                        },
                        'Country'     : {
                            value  : journal.country || '',
                            disable: '.j-action-copy[data-pull-field=country]',
                        },
                    }
                };
                $.each(detailsList, function(listId, details) {
                    $.each(details, function(caption, set) {
                        if (set.value.length > 0) {
                            $(resultRow).find('.detail .detail-list .list-' + listId).append(
                                $('<li></li>').html('<strong>' + caption + ':</strong> ' + set.value)
                            );
                        }
                        else if (false !== set.disable) {
                            $(resultRow).find(set.disable).attr('disabled', true);
                        }
                    });
                });

                $(resultRow).on('click', function(e) {
                    if ($(this).find('.detail .detail-list').is('visible')) {
                        $(this).find('.detail .detail-list').hide();
                    }
                    else {
                        $('#ncbi-search-modal .results .list-group .list-group-item-action')
                            .not(this)
                            .removeClass('bg-master-lighter')
                            .find('.detail .detail-list')
                            .hide()
                            .find('.copy-extras')
                            .hide();

                        $(this)
                            .addClass('bg-master-lighter')
                            .find('.detail .detail-list')
                            .show();
                    }
                });

                $(resultRow).find('.j-action-copy').on('click', function(e) {
                    e.preventDefault();
                    that.validateAndInsertJournalToForm($(this).parents('.list-group-item-action').data('ncbi-uid'), $(this).data('pull-field'));
                });

                $(resultRow).find('.j-action-copy-others').on('click', function(e) {
                    e.preventDefault();
                    $(this).parents('.detail-list').find('.copy-extras').toggle();
                });

                $('#ncbi-search-modal .results .list-group')
                    .append(resultRow);
            });

            $('#ncbi-search-modal .results').show();

            if (this.searchResults.ncbi_query) {
                var searchTerms = $.param({
                    term: [
                        this.searchResults.ncbi_query['term'] || '',
                        '[',
                        this.searchResults.ncbi_query['field'] || '',
                        ']',
                    ].join('')
                });
                $('#ncbi-search-modal .loading .onncbi')
                    .show()
                    .find('a.onncbi-view-result')
                    .attr('href', this.ncbiViewPath + '?' + searchTerms);
            }
        },
        /**
         * Method to result Search variable back to defaulting values
         *
         * @return void
         */
        resetVarsHard: function() {
            this.searchPage    = 1;
            this.searchField   = '';
            this.searchTerm    = '';
            this.searchResults = {};

            $('#ncbi-search-modal .loading .onncbi')
                .hide()
                .find('a.onncbi-view-result')
                .attr('href', '');
        },
        /**
         * Method to reset entire search overlay, for new search
         *
         * @return void
         */
        resetSearchOverlay: function() {
            $('#ncbi-search-modal .info').hide();
            $('#ncbi-search-modal #ncbi-search-field').html('');
            $('#ncbi-search-modal #ncbi-search-term').html('');

            $('#ncbi-search-modal .loading .wait span').html('');
            $('#ncbi-search-modal .loading .wait').hide();
            $('#ncbi-search-modal .loading .found').html('').hide();
            $('#ncbi-search-modal .loading .pagination').hide();
            $('#ncbi-search-modal .loading .onncbi')
                .hide()
                .find('a.onncbi-view-result')
                .attr('href', '');
            $('#ncbi-search-modal .loading .onncbi a.onncbi-manual')
                .attr('href', this.ncbiManualSearch);

            this.removePagination();
            this.removeResults();
        },
        /**
         * Method to validate then process Journal selection to form-fields
         *
         * @param String       uid    Selected Journal UID
         * @param String|null  field  Which of Journal data will be pull
         *
         * @return void
         */
        validateAndInsertJournalToForm: function(uid, field) {
            // validate template
            if (
                (!$('body').hasClass('page_journal_registry')) &&
                (!$('body').hasClass('page_journal_modify'))
            ) {
                Swal.fire('Error', 'Unknown destination TEMPLATE', 'error');
                return;
            }

            // validate loaded data
            if (
                (undefined === uid) ||
                (undefined === this.searchResults.data[uid])
            ) {
                Swal.fire('Error', 'Unable to read UID: ' + (uid || 'missing'), 'error');
                return;
            }

            // validate field
            field = field || 'all';

            if (true == this._validateFieldToAskBeforeInsert(field)) {
                var msg = 'all relevant values';

                switch (field) {
                    case 'name':
                        msg = 'Journal Name value';
                        break;

                    case 'abbreviation':
                        msg = 'Journal Abbreviation value';
                        break;

                    case 'specialty':
                        msg = 'Journal Specialties selections';
                        break;

                    case 'issn':
                        msg = 'Journal ISSN value';
                        break;

                    case 'publisher':
                        msg = 'Journal Publisher name';
                        break;

                    case 'language':
                        msg = 'Journal Languages selections';
                        break;

                    case 'country':
                        msg = 'Journal Country selection';
                        break;
                }

                var that = this;
                Swal.fire({
                    title: "Are you sure?",
                    html: [
                        '<h4 class="m-t-0 m-b-1">',
                            'This will overwrite ',
                            '<strong>',
                                msg,
                            '</strong>',
                            ' on form fields, and this not reversible.',
                        '</h4>',
                    ].join(''),
                    type: "question",
                    showCancelButton: true,
                    confirmButtonClass: "btn-primary",
                    confirmButtonText: "Yes, copy it!",
                    position: 'center-end',
                    customContainerClass: 'ncbi-overwrite-question',
                }).then(function(result) {
                    if (result.value) {
                        that.executeInsertJournalToForm(field, uid);
                    }
                });
            }
            else {
                this.executeInsertJournalToForm(field, uid);
            }
        },
        /**
         * Method that kick start form fill process
         *
         * @param String field
         * @param String uid
         *
         * @return void
         */
        executeInsertJournalToForm: function(field, uid) {
            // by template
            switch (true) {
                case $('body').hasClass('page_journal_registry'):
                case $('body').hasClass('page_journal_modify'):
                    this._insertToJournalMainForms(field, this.searchResults.data[uid]);
                    break;
            }

            // done
            Toast.fire('Done', 'Journal details has been Copied', 'success');
            if ('all' == field) {
                $('#ncbi-search-modal').modal('hide');
                this.resetVarsHard();
                this.resetSearchOverlay();
            }
        },
        /**
         * Method to ask User before start pull data, if form has value. Then move pan form background to field.
         *
         * @param String field
         *
         * @return Boolean
         */
        _validateFieldToAskBeforeInsert: function(field) {
            var ask = true;
            var move = false;

            if ('all' != field) {
                // by template
                switch (true) {
                    case $('body').hasClass('page_journal_registry'):
                    case $('body').hasClass('page_journal_modify'):

                        // by field
                        switch (field) {
                            case 'name':
                                ask = ('' != $('.input-group #name').val());
                                move = $('.input-group #name').offset()['top'] || false;
                                break;

                            case 'abbreviation':
                                ask = ('' != $('.input-group #abbreviation').val());
                                move = $('.input-group #abbreviation').offset()['top'] || false;
                                break;

                            case 'specialty':
                                ask = ($('#specialty-selector').val().length > 0);
                                move = $('#specialty-selector').offset()['top'] || false;
                                break;

                            case 'issn':
                                ask = ('' != $('#issn-wrapper #issn').val());
                                move = $('#issn-wrapper #issn').offset()['top'] || false;
                                break;

                            case 'publisher':
                                ask = ('' != $('#publishername').val());
                                move = $('#publishername').offset()['top'] || false;
                                break;

                            case 'language':
                                ask = ($('#audiencelanguages-select').val().length > 0);
                                move = $('#audiencelanguages-select').offset()['top'] || false;
                                break;

                            case 'country':
                                ask = ($('#audiencecountry-select').val().length > 0);
                                move = $('#audiencecountry-select').offset()['top'] || false;
                                break;
                        }
                        break;
                }
            }

            if (false !== move) {
                $('html, body').scrollTop(move - (($('nav.page-sidebar').height() - 60) / 3));
            }

            return ask;
        },
        /**
         * Method to populate Journal data to Journal Register / Modify form; same template.
         *
         * @param String field
         * @param Array  journal
         *
         * @return void
         */
        _insertToJournalMainForms: function(field, journal) {
            // Start: Revert .good-data
            $('.form-group.good-data, .form-group-default.good-data, .radio.good-data').each(function(index, element) {
                $(element).removeClass('good-data');
            });

            // Name
            if (['all', 'name'].indexOf(field) >= 0) {
                this.___formInjectNormalTextField('.input-group #name', this._getJournalName(journal));
            }

            // Abbreviation
            if (['all', 'abbreviation'].indexOf(field) >= 0) {
                this.___formInjectNormalTextField('.input-group #abbreviation', this._getJournalAbbreviation(journal));
            }

            // Specialty
            if (['all', 'specialty'].indexOf(field) >= 0) {
                this.___formInjectSelect2Field('#specialty-selector', journal.broadheading);
            }

            // ISSN
            if (['all', 'issn'].indexOf(field) >= 0) {
                var issn = this._getJournalIssn(journal, true);
                if (issn.length) {
                    $('#opt-is-medline.radio label[for=opt-is-medline-yes]')
                        .trigger('click')
                        .parents('.radio')
                        .addClass('good-data');
                }
                else {
                     $('#opt-is-medline.radio label[for=opt-is-medline-no]')
                        .trigger('click');
                }
                this.___formInjectNormalTextField('#issn-wrapper #issn', (issn[0] || ''), '.form-group-default');
            }

            // Publisher
            if (['all', 'publisher'].indexOf(field) >= 0) {
                this.___formInjectNormalTextField('#publishername', this._getJournalPublisher(journal));
            }

            // Language
            if (['all', 'language'].indexOf(field) >= 0) {
                this.___formInjectSelect2Field('#audiencelanguages-select', this._getJournalLanguages(journal), '.form-group-default');
            }

            // Country
            if (['all', 'country'].indexOf(field) >= 0) {
                this.___formInjectSelect2Field('#audiencecountry-select', [journal.country], '.form-group-default');

                // Region if Selected by Country
                if (
                    (journal.country) &&
                    ($('#audiencecountry-select').val() != journal.country) &&
                    ($('#audienceregion-select').val())
                ) {
                    $('#audienceregion-select')
                        .parents('.form-group-default')
                        .addClass('good-data');
                }
                else if ('' == $('#audiencecountry-select').val()) {
                    $('#audienceregion-select').val(null).trigger('change');
                }
            }

            // Closing: Apply .good-data
            $.Pages.initFormGroupDefault();
        },
        /**
         * Form filler helper: Basic text fields
         *
         * @param String       fieldSelector
         * @param String|null  newValue
         * @param String|null  parent         Default ".form-group"
         *
         * @return void
         */
        ___formInjectNormalTextField: function(fieldSelector, newValue, parent) {
            if (0 == $(fieldSelector).length) {
                return;
            }

            newValue = $("<div/>").html((newValue || '')).text();
            parent = parent || '.form-group';

            $(fieldSelector)
                .val(newValue)
                .trigger('keyup');

            if (0 == newValue.length) {
                return;
            }

            $(fieldSelector)
                .parents(parent)
                .addClass('good-data');
        },
        /**
         * Form filler helper: Select2 select box fields
         *
         * @param String       fieldSelector
         * @param Array|null   newValues
         * @param String|null  parent         Default ".form-group"
         *
         * @return void
         */
        ___formInjectSelect2Field: function(fieldSelector, newValues, parent) {
            if (0 == $(fieldSelector).length) {
                return;
            }

            newValues = newValues || [];
            newValues = newValues.filter(function (el) {
                return (
                    (el != null) &&
                    (el != false) &&
                    (el.length > 0)
                );
            });
            parent = parent || '.form-group';

            $(fieldSelector).val(null).trigger('change');

            if (0 == newValues.length) {
                return;
            }

            var newValueSelector = [];
            for (var i = 0; i < newValues.length; i++) {
                var found = false;

                for (var j = 0; j < $(fieldSelector).find('option').length; j++) {
                    if ($(fieldSelector).find('option').eq(j).html().toLowerCase() == newValues[i].toLowerCase()) {
                        newValueSelector.push($(fieldSelector).find('option').eq(j).attr('value'));
                        found = true;
                        break;
                    }
                }

                if (false == found) {
                    var newValue = $("<div/>").html((newValues[i] || '')).text();
                    var newOption = new Option(newValue, newValue);
                    $(fieldSelector).append(newOption);
                    newValueSelector.push(newValues[i]);
                }
            }

            $(fieldSelector).val(newValueSelector).trigger('change');

            $(fieldSelector)
                .parents(parent)
                .addClass('good-data');
        },
        /**
         * Helper method to enable form-field search button
         *
         * @param DOM-Object element
         *
         * @return void
         */
        _enableFormBtn: function(element) {
            $(element)
                .removeClass('btn-secondary')
                .addClass('btn-primary')
                .removeClass('disabled')
                .attr('disabled', false);
        },
        /**
         * Helper method to disable form-field search button
         *
         * @param DOM-Object element
         *
         * @return void
         */
        _disableFormBtn: function(element) {
            $(element)
                .removeClass('btn-primary')
                .addClass('btn-secondary')
                .addClass('disabled')
                .attr('disabled', true);
        },
        /**
         * Helper method to extract a Journal's Name
         *
         * @param Array journal
         *
         * @return String
         */
        _getJournalName: function(journal) {
            var result = journal.uic;

            if (
                (journal.titlemainlist.length) &&
                (journal.titlemainlist[0].title)
            ) {
                result = journal.titlemainlist[0].title;
            }
            else if (
                (journal.authorlist.length) &&
                (journal.authorlist[0].collectivename)
            ) {
                result = journal.authorlist[0].collectivename;
            }

            return result;
        },
        /**
         * Helper method to extract a Journal's Abbreviation
         *
         * @param Array journal
         *
         * @return String
         */
        _getJournalAbbreviation: function(journal) {
            var result = 'NLM UID #' + journal.uid;

            if (journal.isoabbreviation.length) {
                result = journal.isoabbreviation;
            }
            else if (journal.titlemainsort.length) {
                result = journal.titlemainsort;
            }

            return result;
        },
        /**
         * Helper method to extract a Journal's uniques ISSN(s).
         * A Journal may have more than 1 ISSN, and most of them possibly same.
         * Print ISSN priority over Electronic ISSN number.
         *
         * @param Array        journal
         * @param Boolean|null inArray    Flag to Return value in Array
         *
         * @return String|Array
         */
        _getJournalIssn: function(journal, inArray) {
            inArray = inArray || false;
            var result = [];

            if (journal.issnlist.length) {
                result = [];
                for (var i = (journal.issnlist.length - 1); i >= 0; i--) {
                    if (journal.issnlist[i].issn) {
                        if ('print' == journal.issnlist[i].issntype.toLowerCase()) {
                            result.unshift(journal.issnlist[i].issn);
                        }
                        else {
                            result.push(journal.issnlist[i].issn);
                        }
                    }
                }
                result = Array.from(new Set(result));
            }

            if (false == inArray) {
                result = result.join(', ');
            }

            return result;
        },
        /**
         * Helper method to extract a Journal's Publication Imprint value
         *
         * @param Array journal
         *
         * @return String
         */
        _getJournalPublisher: function(journal) {
            var result = '';

            if (
                (journal.publicationinfolist.length) &&
                (journal.publicationinfolist[0].imprint)
            ) {
                result = journal.publicationinfolist[0].imprint;
            }

            return result;
        },
        /**
         * Method to convert Language attr string into Language Names
         *
         * @param Array journal
         *
         * @return Array
         */
        _getJournalLanguages: function(journal) {
            var result = [];

            if (
                (typeof MarcLanguageCode === 'function') &&
                (journal.language.length)
            ) {
                var marc = new MarcLanguageCode;
                var lang = journal.language.split(' ');
                for (var i = 0; i < lang.length; i++) {
                    var name = marc.codeToName(lang[i]);
                    if (false !== name) {
                        result.push(name);
                    }
                }
            }

            return result;
        },
        /**
         * Helper method to format Number to comma-separated String value
         *
         * @param String|Number nStr
         *
         * @return String
         */
        __numberFormating: function(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        },
        /**
         * Method to set notification, and show Manual search button.
         *
         * @param String|null msg  Message to appear above Search button, has default
         *
         * @return void
         */
        __setManualSearchElementOnFound: function(msg) {
            msg = msg || 'No Journal found with given Term.';

            $('#ncbi-search-modal .loading .found')
                .html(msg)
                .append(
                    $('<div></div>')
                        .addClass('mt-2')
                        .append(
                            $('<a></a>')
                                .attr('href', this.ncbiManualSearch)
                                .attr('target', '_blank')
                                .addClass('btn btn-primary')
                                .html('Manually Search at NCBI')
                        )
                )
                .show();
        },
    };

    $(document).ready(function() {
        /**
         * JR
         */
        $('#jr1, #jr2').each(function(idx, ele) {
            updateJRsValue($(ele));
        });
        $('#jr1, #jr2').on('blur', function() {
            updateJRsValue($(this));
        });
        function updateJRsValue(inputElement) {
            $(inputElement).val(
                parseFloat( $(inputElement).val() ).toFixed(2)
            );
        }

        /**
         * ISSN
         */
        if ($('#issn-wrapper input').val().length) {
            $('#issn-wrapper').show();
        }
        $('#opt-is-medline-yes, #opt-is-medline-no').on('change', function(event) {
            if ('yes' == $(this).val()) {
                $('#issn-wrapper').show();
            }
            else {
                $('#issn-wrapper').hide();
            }
        });

        /**
         * LOCALITY
         */
        /**
         * Reset Country selection box options.
         *
         * @param array countryList  If empty, it will query ajax for complete list.
         */
        function replenishCountries(countryList)
        {
            countryList = countryList || [];
            var selectedCountry = $('#audiencecountry-select').val() || '';

            if (countryList.length == 0) {
                $.ajax({
                    url: $.ajaxSetup()['url'] + '/v1/get-country-list',
                    type: 'GET',
                    suppressErrors: true,
                    async: false,
                    success: function(data) {
                        replenishCountries(data.data);
                    }
                });
            }
            else {
                $('#audiencecountry-select').select2('destroy');
                $('#audiencecountry-select').find('option').remove();
                $('#audiencecountry-select').append(
                    $('<option></option>').attr('value', '')
                );
                $.each(countryList, function(idx, country) {
                    $('#audiencecountry-select').append(
                        $('<option></option>')
                            .attr('value', (country.iso_code || country.isoCode))
                            .prop('selected', ((country.iso_code || country.isoCode) == selectedCountry))
                            .html(country.name)
                    );

                    if (idx == (countryList.length - 1)) {
                        $('#audiencecountry-select').select2();
                    }
                });
            }
        }

        /**
         * Method handling as Country Selection
         * - With Country (if), "first" Region will be selected.
         */
        $('#audiencecountry-select').on('change', function(event) {
            var selectObject = $(this);

            if ($(selectObject).data('update') === '#ignore') {
                $(selectObject).data('update', false);
            }
            else if ($(selectObject).val()) {
                $.ajax({
                    url: $.ajaxSetup()['url'] + '/v1/get-a-country-details',
                    type: 'POST',
                    data: {
                        country_iso: $(selectObject).val(),
                    },
                    suppressErrors: true,
                    async: false,
                    success: function(data) {
                        if (
                            ($(selectObject).attr('id') == 'audiencecountry-select') &&
                            ($(selectObject).data('update') === '#ignore-region')
                        ) {
                            // wait, let select2 settle first
                            setTimeout(function() {
                                replenishCountries([]);
                            }, 100);
                            $(selectObject).data('update', false);
                        }
                        else if (
                            (data.data.regions.length > 0) &&
                            (data.data.regions[0][ (data.data.regions[0].length - 1) ].id !== undefined)
                        ) {
                            $('#audienceregion-select')
                                .val(data.data.regions[0][ (data.data.regions[0].length - 1) ].id)
                                .data('update', '#ignore')
                                .trigger('change');
                        }
                    }
                });
            }
        });

        /**
         * Method handling as Region change.
         * - Short-list Countries to selected Region.
         * - Prep - so once Country selected, update City updated, not Region.
         */
        $('#audienceregion-select').on('change', function(event) {
            if ($(this).data('update') === '#ignore') {
                $(this).data('update', false);
            }
            else if ($(this).val()) {
                $.ajax({
                    url: $.ajaxSetup()['url'] + '/v1/get-a-region-countries',
                    type: 'POST',
                    data: {
                        region_id: $(this).val(),
                    },
                    suppressErrors: true,
                    success: function(data) {
                        $('#audiencecountry-select').data('update', '#ignore-region');
                        replenishCountries(data.data);
                        setTimeout(function() {
                            $('#audiencecountry-select').select2('open');
                        }, 50);
                    }
                });
            }
        });

        /**
         * DATES
         */
        var datetimepickercfg = {
            allowInputToggle: true,
            buttons: {
                showClear: true,
            },
            format: 'YYYY-MM-DD',
            sideBySide: true,
            useCurrent: 'day',
            viewMode: 'days',
            icons: {
                clear: 'far fa-trash-alt'
            }
        };
        $('#urlupdatedon-wrapper').datetimepicker(
            Object.assign(datetimepickercfg, {widgetParent: $('#urlupdatedon-datetimepicker-area')})
        );
        $('#urltoupdate-wrapper').datetimepicker(
            Object.assign(datetimepickercfg, {widgetParent: $('#urltoupdate-datetimepicker-area')})
        );
        $('#urlviewedon-wrapper').datetimepicker(
            Object.assign(datetimepickercfg, {widgetParent: $('#urlviewedon-datetimepicker-area')})
        );
        $('#valdatecomposed-wrapper').datetimepicker(
            Object.assign(datetimepickercfg, {widgetParent: $('#valdatecomposed-datetimepicker-area'), format: 'YYYY-MM-DD HH:mm:ss', useCurrent: true})
        );
        $('#valclearedon-wrapper').datetimepicker(
            Object.assign(datetimepickercfg, {widgetParent: $('#valclearedon-datetimepicker-area'), format: 'YYYY-MM-DD HH:mm:ss', useCurrent: true})
        );

        /**
         * REMOVAL
         */
        $('#remove-journal').on('click', function(event) {
            event.preventDefault();
            var href = $(this).attr('href');
            Swal.fire({
                title: "Removing this Journal",
                html: [
                    '<h4 class="m-t-0 m-b-1">You are sure to remove this record?</h4>',
                    '<span class="text-muted">There are no recovery from this.</span>',
                ].join(''),
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, remove it!",
                showLoaderOnConfirm: true,
                preConfirm: (function(value) {
                    if (value !== false) {
                        window.location.href = href;
                    }
                })
            });
        });

        /**
         * NCBI SEACH PLUGIN INITIATE
         */
        var search = new NcbiSearch;
    });
})(window.jQuery);