$(document).ready(function($) {
    // result table
    $('#search-results').DataTable({
        'columnDefs': [
            {
                'targets': [1, 2, 3],
                'orderable': false,
            },
            {
                'targets': [3],
                'searchable': false,
            },
        ],
    });

    $('#search-reset').bind('click', function(event) {
        // specific checkboxes
        $.each([
            ['#opt-is-medline', ''],
            ['#opt-is-dgabstract', ''],
            ['#opt-is-secondline', ''],
            ['#opt-is-not-being-updated', ''],
            ['#opt-drug-monitored', ''],
            ['#opt-deleted', ''],
            ['#opt-showcorrupted', 'no'],
        ], function(idx, set) {
            $(set[0] + ' input[type=radio]:not([value="' + set[1] + '"])').attr('checked', false);
            $(set[0] + ' input[type=radio][value="' + set[1] + '"]').attr('checked', true);
        });
    });
});