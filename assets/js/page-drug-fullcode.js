$(document).ready(function($) {
    // GENERIC-DRUG PAGE - DRUG-REF
    if (CONST_DRUGGENREF != undefined) {
        function update_gen_full_code() {
            var output = '';

            var genset = [
                [$('#gen_ref_lvl_1').val().trim(), 1],
                [$('#gen_ref_lvl_2').val().trim(), 2],
                [$('#gen_ref_lvl_3').val().trim(), 1],
                [$('#gen_ref_lvl_4').val().trim(), 1],
                [$('#gen_ref_lvl_5').val().trim(), 2]
            ];

            $(genset).each(function(index, set) {
                if (set[0] !== '') {
                    output += set[0];
                }
                else if (set[1]) {
                    output += '_'.repeat(set[1]);
                }
            });

            $('.gen-drug-full-code').html(output);

            setTimeout(function() {
                $('body').removeClass('js-input');
            }, 300);
        }

        function populate_gen_selects(level, options, value, family) {
            var found_value = false;
            // remove all options
            $('#sel_gen_ref_lvl_' + level + ' option').remove();
            // add empty selection
            $('#sel_gen_ref_lvl_' + level).append(
                $('<option>')
                    .attr('value', '')
                    .html('')
            );

            // build list based on option list
            var preffered = new Array();
            var normal    = new Array();
            $.each(options, function(key, lvl) {
                var opt = $('<option>')
                    .attr('value', lvl.code)
                    .html(lvl.code + " : " + lvl.name);

                if (value !== '') {
                    if (lvl.code == value) {
                        // if value and code is match
                        $(opt).attr('selected', true);
                        found_value = true;
                    }
                    if (family !== false) {
                        if (lvl.code.substring(0, family.length) != family) {
                            // if not in family
                            if (level == 1) {
                                // if lvl1 only
                                $(opt).addClass('disabled')
                                    .attr('disabled', true);
                                normal.push(opt);
                            }
                        }
                        else {
                            // if in family
                            preffered.push(opt);
                        }
                    }
                    else {
                        // no family - lvl1 only
                        normal.push(opt);
                    }
                }
                else if (level == 1) {
                    // if empty for lvl1
                    normal.push(opt);
                }
            });

            // push options
            $.each(preffered, function(index, opt) {
                $('#sel_gen_ref_lvl_' + level).append(opt);
            });
            $.each(normal, function(index, opt) {
                $('#sel_gen_ref_lvl_' + level).append(opt);
            });

            // update
            $('#sel_gen_ref_lvl_' + level).select2();

            if (
                // has value
                (value !== '') &&
                // not found yet
                (!found_value)
            ) {
                // not lvl2 with 1 char
                if (
                    (level != 2) ||
                    ((level == 2) && (value.length != 2))
                ) {
                    $('#gen_ref_lvl_' + level).val('');
                }
            }
        }

        function update_gen_lvl_by_input() {
            // LEVEL 1
            populate_gen_selects(
                1,
                CONST_DRUGGENREF.level_1,
                $('#gen_ref_lvl_1').val().trim(),
                false
            );

            // LEVEL 2
            populate_gen_selects(
                2,
                CONST_DRUGGENREF.level_2,
                $('#gen_ref_lvl_1').val().trim()
                    + $('#gen_ref_lvl_2').val().trim(),
                $('#gen_ref_lvl_1').val().trim()
            );

            // LEVEL 3
            populate_gen_selects(
                3,
                CONST_DRUGGENREF.level_3,
                $('#gen_ref_lvl_1').val().trim()
                    + $('#gen_ref_lvl_2').val().trim()
                    + $('#gen_ref_lvl_3').val().trim(),
                $('#gen_ref_lvl_1').val().trim()
                    + $('#gen_ref_lvl_2').val().trim()
            );

            // LEVEL 4
            populate_gen_selects(
                4,
                CONST_DRUGGENREF.level_4,
                $('#gen_ref_lvl_1').val().trim()
                    + $('#gen_ref_lvl_2').val().trim()
                    + $('#gen_ref_lvl_3').val().trim()
                    + $('#gen_ref_lvl_4').val().trim(),
                $('#gen_ref_lvl_1').val().trim()
                    + $('#gen_ref_lvl_2').val().trim()
                    + $('#gen_ref_lvl_3').val().trim()
            );
        }

        $('#gen_ref_lvl_1, #gen_ref_lvl_2, #gen_ref_lvl_3, #gen_ref_lvl_4').on('keyup', function(event) {
            $(this).val( $(this).val().toUpperCase() );
            update_gen_lvl_by_input();
            update_gen_full_code();
        });

        $('#gen_ref_lvl_5').on('keyup', function(event) {
            update_gen_full_code();
        });

        $('#sel_gen_ref_lvl_1, #sel_gen_ref_lvl_2, #sel_gen_ref_lvl_3, #sel_gen_ref_lvl_4').on('select2:opening', function(event) {
            $('body').addClass('js-input');
        });

        $('#sel_gen_ref_lvl_1, #sel_gen_ref_lvl_2, #sel_gen_ref_lvl_3, #sel_gen_ref_lvl_4').on('change', function(event) {
            var index = parseInt($(this).attr('id').substring(($(this).attr('id').length) - 1));
            var selval = $(this).val();

            if (index == 2) {
                selval = selval.substring(1);
            }
            else if (index == 3) {
                selval = selval.substring(3);
            }
            else if (index == 4) {
                selval = selval.substring(4);
            }

            $('#gen_ref_lvl_' + index).val(selval);

            update_gen_lvl_by_input();
            update_gen_full_code();
        });

        update_gen_lvl_by_input();
        update_gen_full_code();
    }
    else {
        console.error('Missing "CONST_DRUGGENREF", load failed.');
        Toast.fire('Loading Failed', 'Unable to process full-code entry.', 'error');
    }
});