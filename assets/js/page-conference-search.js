$(document).ready(function($) {
    // result table
    $('#search-results').DataTable({
        'order': [],
        'columnDefs': [
            {
                'targets': [1, 2, 3],
                'orderable': false,
            },
            {
                'targets': [3],
                'searchable': false,
            },
        ],
    });

    $('#search-reset').bind('click', function(event) {
        // specific checkboxes
        $.each([
            ['#opt-key-congress', ''],
            ['#opt-cruise', ''],
            ['#opt-online', ''],
            ['#opt-has-guide', ''],
            ['#opt-discontinued', ''],
            ['#opt-showcorrupted', ''],
        ], function(idx, set) {
            $(set[0] + ' input[type=radio]:not([value="' + set[1] + '"])').attr('checked', false);
            $(set[0] + ' input[type=radio][value="' + set[1] + '"]').attr('checked', true);
        });

    });

    var datetimepickercfg = {
        allowInputToggle: true,
        buttons: {
            showClear: true,
        },
        format: 'YYYY-MM-DD',
        sideBySide: true,
        useCurrent: 'day',
        viewMode: 'days',
        icons: {
            clear: 'far fa-trash-alt'
        }
    };
    $('#startdate-wrapper').datetimepicker(
        Object.assign(datetimepickercfg, {widgetParent: $('#start-datetimepicker-area')})
    );
    $('#enddate-wrapper').datetimepicker(
        Object.assign(datetimepickercfg, {widgetParent: $('#end-datetimepicker-area')})
    );
    $("#startdate-wrapper").on("change.datetimepicker", function (e) {
        $('#enddate-wrapper').datetimepicker('minDate', e.date);
    });
    $("#enddate-wrapper").on("change.datetimepicker", function (e) {
        $('#startdate-wrapper').datetimepicker('maxDate', e.date);
    });
});