$(document).ready(function() {
    $('#search-query').on('blur', function(e) {
        $(this).val($(this).val().trim().replace(/\s+/g, " "));
    });

    if ($('#search-results-data').length) {

        function setResultDisplay(drug, row, meta) {
            var display = [];

            for (var i = 0; i < row[drug].length; i++) {
                display.push(
                    '<a href="#" class="btn btn-sm mr-1 drug-' + drug + '" data-toggle="drug-view-overlay" data-type="' + drug + '" data-id="'
                        + row[drug][i].id
                        + '">'
                        + row[drug][i].name
                        + '</a>');

            }

            if ('experimental' != drug) {
                ['typo', 'synonym'].forEach(function(type, idx) {
                    if (row[drug + '_' + type].length) {
                        for (var i = 0; i < row[drug + '_' + type].length; i++) {
                            display.push(
                                '<a href="#" class="btn btn-sm mr-1 drug-' + drug + ' ' + type + '" data-toggle="drug-view-overlay" data-type="' + drug + '" data-id="'
                                    + row[drug + '_' + type][i].id
                                    + '">'
                                    + row[drug + '_' + type][i].name
                                    + '</a>');
                        }
                    }
                });
            }

            return display.join('');
        }

        function setResultText(drug, row, meta) {
            var filter = [];

            for (var i = 0; i < row[drug].length; i++) {
                filter.push(row[drug][i].name);
            }

            if ('experimental' != drug) {
                ['typo', 'synonym'].forEach(function(type, idx) {
                    if (row[drug + '_' + type].length) {
                        for (var i = 0; i < row[drug + '_' + type].length; i++) {
                            filter.push(row[drug + '_' + type][i].name);
                        }
                    }
                });
            }

            return filter.join(' ');
        }

        var drugToCol = ['experimental', 'generic', 'brand'];

        $('#search-results').DataTable({
            data: $.parseJSON($('#search-results-data').val()),
            ordering: false,
            pageLength: 25,
            columnDefs: [{
                targets: '_all',
                render: function (data, type, row, meta) {
                    switch (type) {
                        case 'display':
                            return setResultDisplay(drugToCol[meta.col], row, meta);
                            break;

                        case 'filter':
                        case 'type':
                        case 'sort':
                            return setResultText(drugToCol[meta.col], row, meta);
                            break;
                    }
                },
            }],
            drawCallback: function(settings) {
                $('.table.table-sm tbody tr td a.btn[data-toggle="drug-view-overlay"]').drugViewOverlay();
            },
        });
    }

    if ($('#export-csv').length) {
        $('#export-csv').on('click', function(e) {
            e.preventDefault();

            Swal.mixin({
                input: 'text',
                confirmButtonText: 'Next &rarr;',
                showCancelButton: true,
                progressSteps: ['1', '2'],
                onBeforeOpen: function() {
                    Swal.hideProgressSteps();
                },
            }).queue([
                {
                    title: 'Which Export type you would like?',
                    input: 'select',
                    inputOptions: {
                        'complete': 'Complete Drugs Data',
                        'names'   : 'Drugs Name Only',
                    },
                    inputValue: 'complete',
                    showCancelButton: true
                },
                {
                    title: 'The export will be email to you',
                    html: [
                        'But you can leave this field empty,<br/>',
                        'to download the export instead.<br/><br/>',
                        '<a href="#" id="reset-swal-email-input" class="btn btn-danger">',
                        'Clear Email &amp; Download instead',
                        '</a>',
                    ].join(''),
                    onOpen: function() {
                        $('#reset-swal-email-input').on('click', function(e) {
                            e.preventDefault();
                            $('input.swal2-input[type="email"]').val('');
                            Swal.clickConfirm();
                        });
                    },
                    input: 'email',
                    inputPlaceholder: 'Your email address',
                    inputValidator: function(result) {
                    },
                    inputValue: (CONST_USERMAIL || ''),
                    showCancelButton: true,
                }
            ]).then(function (result) {
                if (result.value) {
                    Swal.fire({
                        title: 'Sending export request...',
                        type: 'info',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showCloseButton: false,
                        allowEscapeKey: false,
                        onBeforeOpen: function() {
                            Swal.showLoading();
                            var isEmail = (result.value[1].trim().length > 0);
                            $.ajax({
                                url    : $.ajaxSetup()['url'] + '/v1/export-name-search',
                                method : "POST",
                                dataType: (isEmail ? 'json' : 'text'),
                                data   : {
                                    q       : $('#search-query').val().trim(),
                                    type    : $('#search-type').val(),
                                    less    : ('complete' == result.value[0] ? 0 : 1),
                                    email   : result.value[1].trim(),
                                    fullcode: $('#opt-has-code').is(':checked'),
                                    uuid    : $('#opt-uid-search').is(':checked'),
                                },
                                success: function(data, text, response) {
                                    if (isEmail) {
                                        Swal.fire(
                                            'Export requested!',
                                            'Please check your Email Inbox.',
                                            'success'
                                        );
                                    }
                                    else {
                                        var content_type = response.getResponseHeader('Content-Type');
                                        if (content_type.indexOf('text/csv') >= 0) {
                                            var header   = response.getResponseHeader('Content-Disposition');
                                            var filename = header.match(/filename="?(.+)"?/)[1];
                                            __saveData(data, filename);

                                            Swal.fire(
                                                'Export complete!',
                                                'Browser will ask/show download in progress..',
                                                'success'
                                            );
                                        }
                                        else {
                                            Swal.fire('Export Failed', 'Content Type unknown: ' + content_type, 'error');
                                        }
                                    }
                                },
                            });
                        },
                    });
                }
            })
        });
    }
});