(function($) {
    'use strict';
    $(document).ready(function () {
        $('#date-search').datetimepicker({
            'format': 'YYYY-MM-DD',
            'maxDate': 'now',
            'defaultDate': new Date(),
        });

        var dataTable = $('#feebuilder-article-list').DataTable({
            "order":[[0, 'desc']],
            "oLanguage": {
                "sSearch": "Find in results:",
                "sEmptyTable": "No articles available for today."
            },
            "pagingType": "full_numbers",
            "language"  : {
                "paginate": {
                    "first"   : "&laquo;",
                    "previous": "&lt;",
                    "next"    : "&gt;",
                    "last"    : "&raquo;"
                }
            }
        });

        $('#searchByDateBtn').on('click', function(){
            var dateValid = isValidDate();
            if (false === dateValid) {
                Toast.fire('Error', 'Please enter a valid date.', 'error');
                return false;
            }
            else if (null === dateValid) {
                Swal.fire({
                    title: 'Invalid Date',
                    html: 'Date should be past or today.',
                    type: 'warning',
                });
                return false;
            }
            dataTable.destroy();
            dataTable = $('#feebuilder-article-list').DataTable({
                "ajax": {
                    "url": $.ajaxSetup()['url'] + '/v1/feedbuilder-articles',
                    "type": "POST",
                    "data": {'date': $('#date-search').datetimepicker('viewDate').format('YYYY-MM-DD')},
                },
                "columns": [
                    {
                        "title"    : "Updated",
                        "data"     : "updated",
                        "orderable": true,
                        "width"    : "20%",
                    },
                    {
                        "title"    : "Type",
                        "data"     : "type",
                        "orderable": true,
                        "width"    : "20%",
                    },
                    {
                        "title"    : "Title",
                        "data"     : (function (row, type, val, meta) {
                            return '<a href="' + $.ajaxSetup()['url'] + '/feedbuilder/article/' + row.id + '"><span title="' + row.title + '">' + row.title + '</span></a>';
                        }),
                        "orderable": true,
                    },
                    {
                        "title"    : "Status",
                        "style"    : "text-align: center;",
                        "data"     : (
                            function (row, type, val, meta) {
                                var wrapper = $('<div/>');
                                var data = $('<div/>').attr({style: 'text-align: center;'});
                                var icon = $('<i/>');
                                icon.attr({
                                    class: row.status ? 'fa fa-check' : 'fa fa-times'
                                });
                                data.append(icon);
                                wrapper.append(data);
                                return wrapper.html();
                        }),
                        "orderable": true,
                        "width"    : "10%",
                    }
                ],
                "order":[[0, 'desc']],
                "oLanguage": {
                    "sSearch": "Find in results:",
                    "sEmptyTable": "No articles available for specified date."
                },
                "pagingType": "full_numbers",
                "language"  : {
                    "paginate": {
                        "first"   : "&laquo;",
                        "previous": "&lt;",
                        "next"    : "&gt;",
                        "last"    : "&raquo;"
                    }
                }
            });
            dataTable.on( 'xhr', function () {
                var json = dataTable.ajax.json();
                if (typeof json.message !== 'undefined'
                    && json.message !== '') {
                    Swal.fire({
                        title: 'Warning',
                        html: json.message,
                        type: 'warning',
                    });
                }
            });
        });
        $('#searchXdays').on('click', function() {
            var dateValid = isValidDate();
            if (false === dateValid) {
                Toast.fire('Error', 'Please enter a valid date.', 'error');
                return false;
            }
            else if (null === dateValid) {
                Swal.fire({
                    title: 'Invalid Date',
                    html: 'Date should be past or today.',
                    type: 'warning',
                });
                return false;
            }
            dataTable.destroy();
            dataTable = $('#feebuilder-article-list').DataTable({
                "ajax": {
                    "url": $.ajaxSetup()['url'] + '/v1/feedbuilder-articles-x-days',
                    "type": "POST",
                    "data": {'date': $('#date-search').datetimepicker('viewDate').format('YYYY-MM-DD')},
                },
                "columns": [
                    {
                        "title"    : "Updated",
                        "data"     : "updated",
                        "orderable": true,
                        "width"    : "20%",
                    },
                    {
                        "title"    : "Type",
                        "data"     : "type",
                        "orderable": true,
                        "width"    : "20%",
                    },
                    {
                        "title"    : "Title",
                        "data"     : (function (row, type, val, meta) {
                            return '<a href="' + $.ajaxSetup()['url'] + '/feedbuilder/article/' + row.id + '"><span title="' + row.title + '">' + row.title + '</span></a>';
                        }),
                        "orderable": true,
                    },
                    {
                        "title"    : "Status",
                        "style"    : "text-align: center;",
                        "data"     : (
                            function (row, type, val, meta) {
                                var wrapper = $('<div/>');
                                var data = $('<div/>').attr({style: 'text-align: center;'});
                                var icon = $('<i/>');
                                icon.attr({
                                    class: row.status ? 'fa fa-check' : 'fa fa-times'
                                });
                                data.append(icon);
                                wrapper.append(data);
                                return wrapper.html();
                            }),
                        "orderable": true,
                        "width"    : "10%",
                    }
                ],
                "order":[[0, 'desc']],
                "oLanguage": {
                    "sSearch": "Find in results:",
                    "sEmptyTable": "No articles available found."
                },
                "pagingType": "full_numbers",
                "language"  : {
                    "paginate": {
                        "first"   : "&laquo;",
                        "previous": "&lt;",
                        "next"    : "&gt;",
                        "last"    : "&raquo;"
                    }
                }
            });
            dataTable.on( 'xhr', function () {
                var json = dataTable.ajax.json();
                if (typeof json.message !== 'undefined'
                    && json.message !== '') {
                    Swal.fire({
                        title: 'Warning',
                        html: json.message,
                        type: 'warning',
                    });
                }
            });
        });
        function isValidDate()
        {
            if (typeof $('#date-search').datetimepicker('viewDate')._isValid !== "undefined") {
                if ($('#date-search').datetimepicker('viewDate')._isValid) {
                    var now = moment();
                    if (now >= $('#date-search').datetimepicker('viewDate')) {
                        return true;
                    }
                    return null;
                }
                return false;
            }
            return false;
        }
    });
})(window.jQuery);