(function($) {
    'use strict';
    $(document).ready(function () {
        $('#date-search').datetimepicker({
            'format': 'YYYY-MM-DD',
            'maxDate': 'now',
        });

        $('#dgmonitor-abstracts-read-only').DataTable({
            "order": [[0, 'desc']],
            "oLanguage": {
                "sSearch": "Find in results:"
            }
        });

        $('#searchByDateBtn').on('click', function(){
            var dateValid = isValidDate();
            if (false === dateValid) {
                Toast.fire('Error', 'Please enter a valid date.', 'error');
                return false;
            }
            else if (null === dateValid) {
                Swal.fire({
                    title: 'Invalid Date',
                    html: 'Date should be past or today.',
                    type: 'warning',
                });
                return false;
            }
            $('#dgmonitor-abstracts-read-only').DataTable().clear();
            $('#dgmonitor-abstracts-read-only').DataTable().destroy();
            $('#dgmonitor-abstracts-read-only').empty();
            var dataTable = $('#dgmonitor-abstracts-read-only').DataTable({
                "ajax": {
                    "url": $.ajaxSetup()['url'] + '/v1/dgmonitor-abstracts-read-only',
                    "type": "POST",
                    "data": {'date': $('#date-search').datetimepicker('viewDate').format('YYYY-MM-DD')},
                },
                "columns": [
                    {
                        "title"    : "Date",
                        "data"     : "updated",
                        "orderable": true,
                        "width"    : "20%",
                    },
                    {
                        "title"    : "Title",
                        "data"     : (function (row, type, val, meta) {
                            return '<span title="' + row.title + '">' + row.title + '</span>';
                        }),
                        "orderable": true,
                    },
                ],
                "order":[[0, 'desc']],
                "oLanguage": {
                    "sSearch": "Find in results:"
                },
                "pagingType": "full_numbers",
                "language"  : {
                    "paginate": {
                        "first"   : "&laquo;",
                        "previous": "&lt;",
                        "next"    : "&gt;",
                        "last"    : "&raquo;"
                    }
                }
            });
            dataTable.on( 'xhr', function () {
                var json = dataTable.ajax.json();
                if (typeof json.message !== 'undefined'
                    && json.message !== '') {
                    Swal.fire({
                        title: 'Warning',
                        html: json.message,
                        type: 'warning',
                    });
                }
            });
            $('#dgmonitor-abstracts-read-only').children('thead').addClass('thead-dark');
        });

        function isValidDate()
        {
            if (typeof $('#date-search').datetimepicker('viewDate')._isValid !== "undefined") {
                if ($('#date-search').datetimepicker('viewDate')._isValid) {
                    var now = moment();
                    if (now >= $('#date-search').datetimepicker('viewDate')) {
                        return true;
                    }
                    return null;
                }
                return false;
            }
            return false;
        }
    });
})(window.jQuery);