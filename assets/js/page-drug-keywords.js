$(document).ready(function() {
    // DRUG-KEYWORD-ACTION
    if ($('.drug-section').length) {
        function keywordSectionActionChange(secType, status) {
            var section = $('.section-' + secType);

            // field
            $(section)
                .find('input')
                .first()
                .val( status );

            // menu
            $('.section-' + secType + ' .pmbb-header .dropdown-menu li').each(function(index, element) {
                $(element).removeClass('active');
                if (index == 0) {
                    // enabled
                    if (status == 1) {
                        $(element).addClass('active');
                    }
                }
                else {
                    // disable
                    if (status == 0) {
                        $(element).addClass('active');
                    }
                }
            });

            // heading
            if (status == 0) {
                $('.section-' + secType + ' .type-section-' + secType).slideUp();
                $('.section-' + secType + ' .keyword-section-' + secType).slideUp();
                $('.section-' + secType + ' .pmb-disabled-note').slideDown();
                $('.section-' + secType + ' input[name="' + secType + '_name"]').prop('required', false);
            }
            else {
                $('.section-' + secType + ' .type-section-' + secType).slideDown();
                $('.section-' + secType + ' .keyword-section-' + secType).slideDown();
                $('.section-' + secType + ' .pmb-disabled-note').slideUp();
                $('.section-' + secType + ' input[name="' + secType + '_name"]').prop('required', true);
            }
        }
        $.each(["generic", "brand"], function(index, sectName) {
            keywordSectionActionChange(sectName, $('.section-' + sectName + ' > input').first().val());
            $('.section-' + sectName + ' .action-btn-enable').on('click', function(event) {
                event.preventDefault();
                keywordSectionActionChange(sectName, 1);
            });
            $('.section-' + sectName + ' .action-btn-disable').on('click', function(event) {
                event.preventDefault();
                keywordSectionActionChange(sectName, 0  );
            });
        });
    }

    // DRUG-KEYWORD-SYNONYM-COUNTRIES
    if ($('.sub-drugs').length) {
        // set repeater items first, based on bare template
        $('.drug-section .repeater').repeater({
            initEmpty: false,
            show: function (event) {
                // disable the button
                $(this).find('.btn-danger').on('click', function(event) {
                    event.preventDefault();
                });

                // activate select2 - Synonyms Only
                var select2 = $(this).find('.select2');

                // show
                $(this).slideDown(function() {
                    synonymsSelectPickerCountryInit(select2);
                });
            },
            hide: function (deleteElement) {
                // animate, as the plugin will remove, and reindex
                $(this).slideUp(deleteElement);
            },
            isFirstItemUndeletable: false
        });

        var countryOptions = $('<div>');
        if (CONST_CONTRIES != undefined) {
            $.each(CONST_CONTRIES, function(index, group) {
                var group_options = $('<div>');
                $.each(group, function(index, country) {
                    var optionstr = $('<option>')
                        .attr('value', country.iso_code)
                        .attr('data-subtext', ' (' + country.iso_code + ')')
                        .html(country.name);
                    $(group_options).append(optionstr);
                });
                $(countryOptions).prepend(
                    $('<optgroup>')
                        .attr('label', index)
                        .append($(group_options).children())
                );
            });
        }

        // add action to remove save button; z-index bug
        function synonymsSelectPickerCountryInit(elements) {
            $(elements).each(function(index, element) {
                $(element).html(function() {
                    // options set
                    var defaultOpts = $(countryOptions).children().clone();

                    // manually set selected
                    var selectedpre = $(this).data('preselected');
                    if (
                        (selectedpre != undefined) &&
                        (selectedpre.length)
                    ) {
                        // convert from json
                        selectedpre = selectedpre.split(',');
                        // selecting
                        $(defaultOpts).find('option').each(function(index, opt) {
                            if  ($.inArray($(opt).attr('value'), selectedpre) >= 0) {
                                $(opt).attr('selected', 'selected');
                            }
                        });
                    }

                    // passing
                    return $(defaultOpts);
                })
                .select2().on('select2:open', function() {
                    if (0 == $('.select2-results > p').length) {
                        $('.select2-results').append('<p class="small p-0 mb-0 mt-2 text-muted hint-text">*Holds CTRL / CMD key to select multiple entries.</p>');
                    }
                })
            });
        }

        // disable repeater button
        $('.drug-section .repeater .btn-danger').on('click', function(event) {
            event.preventDefault();
        });

        // disable repeater button
        $('.repeater-add-btn').on('click', function(event) {
            event.preventDefault();
        });

        // apply to loaded
        if ($('.drug-section .repeater .select2').length) {
            synonymsSelectPickerCountryInit($('.drug-section .repeater .select2'));
        }
    }
});