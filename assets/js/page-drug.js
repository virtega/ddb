$(document).ready(function($) {
    if ($('#creation-dtpicker').length) {
        $('#creation-dtpicker').datetimepicker({
            viewMode: 'days',
            format: 'YYYY-MM-DD HH:mm:ss',
            icons: {
                time: "fa fa-clock",
                date: "fa fa-calendar-alt",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
            },
            sideBySide: true,
        });
    }

    if ($('.drug-page-icon-list').length) {
        var id   = $('.drug-page-icon-list').data('id');
        var type = $('.drug-page-icon-list').data('type');

        $.ajax({
            url: $.ajaxSetup()['url'] + '/v1/get-drug-entire-family',
            type: 'POST',
            data: {
                id: id,
                type: type,
            },
            success: function(data) {
                if (0 == data.count) {
                    $('.drug-page-icon-list').remove();
                }

                $('.drug-page-icon-list')
                    .find('.drug-icon-' + type + ' a')
                    .removeClass('text-muted')
                    .addClass('text-primary')
                    .find('.drug-icon-name')
                    .html(data.data[type][0].main.name);

                $.each(['experimental', 'brand', 'generic'], function(lidx, ltype) {
                   if (data.data[ltype][0] != undefined)  {
                        $('.drug-page-icon-list')
                            .find('.drug-icon-' + ltype + ' .drug-icon-name')
                            .html(data.data[ltype][0].main.name)
                            .parents('.drug-icon-' + ltype)
                            .find('a')
                                .attr('data-type', ltype)
                                .attr('data-id', data.data[ltype][0].main.id)
                                .removeClass('cursor-initial')
                                .drugViewOverlay();
                    }
                });

                $('.drug-page-icon-list-wrapper').fadeIn();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.drug-page-icon-list').remove();
            }
        });
    }

    if ($('#action-delete').length) {
        $('#action-delete').on('click', function(e) {
            e.preventDefault();
            var id   = $(this).data('id');
            var type = $(this).data('type');

            Swal.fire({
                title: 'Confirmed to remove this Drug?',
                html: 'This will remove Drug details,<br/>and detached any Drug(s) relation.',
                type: "question",
                confirmButtonClass : "btn-danger",
                confirmButtonText : "Yes, remove it!",
                showCancelButton : true,
                showLoaderOnConfirm: true,
                preConfirm: (function(value) {
                    if (value !== false) {
                        return new Promise(function(resolve) {
                            $.ajax({
                                url: $.ajaxSetup()['url'] + '/v1/remove-drug',
                                data: {
                                    type: type,
                                    id: id,
                                },
                                success: function(response) {
                                    Swal.fire({
                                        title: 'Drug Removed',
                                        text: response.msg,
                                        type: 'success',
                                        timer: 10000,
                                        showConfirmButton: true,
                                    }).then(function() {
                                        window.location.href = $.ajaxSetup()['url'] + '/drug/search';
                                        resolve(response);
                                    });
                                },
                            });
                        });
                    }
                }),
            });
        });
    }
});