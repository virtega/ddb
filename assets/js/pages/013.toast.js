/**
 * SWEETALERT TOAST OR PGNOTIFICATION
 */
(function($) {

    'use strict';

    var Toast = function() {
        this.type = 'info';
        this.title = '';
        this.msg = '';
        this.defaultOpts = {
            notifier: 'page',
            icon: 'fa fa-bell',
            timeout: 3800,
        };
        this.options = $.extend(true, {}, this.defaultOpts);
    };

    Toast.prototype = {
        fire: function(title, msg, type, options) {
            this.title = title || 'Successful';
            this.msg   = msg || '';
            this.type  = type || 'info';
            this.options = $.extend(true, {}, this.defaultOpts, options);

            this.type = this.getType(this.type);

            if (this.isSweet()) {
                Swal.fire({
                    type: this.type,
                    title: this.title + ', ' + this.msg,
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: this.options.timeout,
                });
            }
            else {
                $('.page-content-wrapper').pgNotification({
                    type: this.type,
                    title: this.title,
                    message: this.msg,
                    position: 'top-right',
                    style: 'circle',
                    thumbnail: '<i class="' + this.options.icon + '"></i>',
                    timeout: this.options.timeout,
                }).show();
            }
        },
        isSweet: function() {
            return ('sweet' == this.options.notifier);
        },
        getType: function(type) {
            switch (this.type) {
                case 'green':
                case 'success':
                    if (this.isSweet()) {
                        return 'success';
                    }
                    else {
                        return 'info';
                    }

                case 'red':
                case 'error':
                case 'danger':
                    if (this.isSweet()) {
                        return 'error';
                    }
                    else {
                        return 'danger';
                    }

                case 'gray':
                case 'question':
                    if (this.isSweet()) {
                        return 'question';
                    }
                    else {
                        return 'info';
                    }

                case 'yellow':
                case 'warning':
                    return 'warning';

                case 'blue':
                case 'info':
                default:
                    if (this.isSweet()) {
                        return 'info';
                    }
                    else {
                        return 'success';
                    }
            }
        },
        success: function(title, msg, options) {
            this.fire(title, msg, 'success', options);
        },
        error: function(title, msg, options) {
            this.fire(title, msg, 'error', options);
        },
        warning: function(title, msg, options) {
            this.fire(title, msg, 'warning', options);
        },
        info: function(title, msg, options) {
            this.fire(title, msg, 'info', options);
        },
        question: function(title, msg, options) {
            this.fire(title, msg, 'question', options);
        },
        danger: function(title, msg, options) {
            this.fire(title, msg, 'danger', options);
        },
        clear: function() {
            if (this.isSweet()) {
                Swal.close();
            }
            else {
                $('.page-content-wrapper .pgn-wrapper[data-position="top-right"] button.close').trigger('click');
            }
        }
    };

    window.Toast = new Toast();
})(window.jQuery);