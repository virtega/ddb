/**
 * 901. TempusDominusBootstrap4 Override
 */
if ($.isFunction($.fn.datetimepicker)) {
    /**
     * @link https://github.com/tempusdominus/bootstrap-4/issues/34#issuecomment-538589256
     */
    $.fn.datetimepicker.Constructor.prototype._origSetValue = $.fn.datetimepicker.Constructor.prototype._setValue;
    $.fn.datetimepicker.Constructor.prototype._setValue = function _setValue(targetMoment, index) {
        var oldDate = this.unset ? null : this._dates[index];
        var outpValue = '';
        if (!targetMoment && (!this._options.allowMultidate || this._dates.length === 1)) {
            this.unset = true;
            this._dates = [this.getMoment()];
            this._datesFormatted = [];
            this._viewDate = this.getMoment().locale(this._options.locale).clone();
            if (this.input !== undefined) {
                this.input.val('');
                this.input.trigger('input');
            }
            this._element.data('date', outpValue);
            this._notifyEvent({
                type: $.fn.datetimepicker.Constructor.Event.CHANGE,
                date: false,
                oldDate: oldDate
            });
            this._update();
        }
        else
        {
            // added: missing indexes - change to null
            if (typeof index == 'undefined') {
                index = null;
                // or reset to previous set date - if any
                if ((this._dates.length === 1) && (typeof this._dates[0] != 'undefined')) {
                    index = 0;
                }
            }
            // added: check for validity, or reset to now()
            if (false == targetMoment._isValid) {
                targetMoment = this.getMoment();
            }
            this._origSetValue(targetMoment, index);
        }
    };
}