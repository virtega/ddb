(function($) {
    'use strict';

    $(document).ready(function() {

        /**
         * Template Switcher on Cookie
         */
        var sct_cookieKey = 'drugdb-theme';
        var sct_themes = {
            'white-theme': {
                name: 'White Theme',
            },
            'dark-theme': {
                name: 'Dark Theme',
            }
        };

        // read cookie
        var sct_current = Cookies.get(sct_cookieKey);
        if (sct_current == undefined) {
            for (var theme in sct_themes) {
                if ($('body').hasClass(theme)) {
                    sct_current = theme;
                }
            }
            // none, apply default
            if (sct_current == undefined) {
                sct_current = 'white-theme';
                $('body').addClass(sct_current);
            }
        }
        else if (!$('body').hasClass(sct_current)) {
            // controller should have this changed, but anyway...
            for (var theme in sct_themes) {
                $('body').removeClass(theme);
            }
            $('body').addClass(sct_current);
        }

        // plan for switch button
        var currTheme = '';
        var newTheme = '';
        for (var theme in sct_themes) {
            if (sct_current == theme) {
                currTheme = theme;
            }
            else {
                newTheme = theme;
            }
        }

        // set switch button
        $('#switch-color-theme')
            .show()
            .find('.theme-caption')
            .html(sct_themes[newTheme].name);

        // set switch button action
        $('#switch-color-theme').on('click', function(e) {
            e.preventDefault();

            $('body')
                .toggleClass(currTheme)
                .toggleClass(newTheme);

            $(this).find('.theme-caption').html(sct_themes[currTheme].name);

            sct_current = currTheme;
            currTheme   = newTheme;
            newTheme    = sct_current;

            Cookies.remove(sct_cookieKey);
            Cookies.set(sct_cookieKey, currTheme);
        });
    });
})(window.jQuery);