// DRUG VIEW OVERLAY
// ===================
(function($) {

    'use strict';

    var DrugViewOverlay = function (element, options) {
        this.element = ($(element).length ? $(element) : null);
        this.options = $.extend(true, {}, $.fn.drugViewOverlay.defaults, options);
        this.modal   = '#drug-view-overlay';
        this.id      = 0;
        this.type    = '';
        this.getter  = null;
        this.drug    = {};
        this.click   = {};
        if (this.element) {
            this.init();
        }
    };

    DrugViewOverlay.prototype = {
        init: function() {
            var me = this;
            $(me.element).unbind('click');
            $(me.element).on('click', function(e) {
                e.preventDefault();
                me.click = e;

                me.id   = $(this).data('id');
                me.type = $(this).data('type');

                me.openTarget();
            });
        },
        setTarget: function(id, type) {
            this.id = id;
            this.type = type;
        },
        openTarget: function() {
            if (this.preFlightCheck()) {
                var me = this;
                this.getDrug().then(function() {
                    me.prepareModal().then(function() {
                        me.show();
                    });
                });
            }
        },
        preFlightCheck: function() {
            if (0 == $(this.modal).length) {
                console.error('DrugViewOverlay: Cannot find the modal: ' + this.modal);
                Toast.error('Drug View Failed', 'Preflight check failed.');
                return false;
            }

            this.template = $(this.modal).children().clone(false, false);

            Toast.info('Please wait...', 'Loading drug details', {'icon':'fas fa-spinner fa-pulse'});
            return true;
        },
        getDrug: function() {
            var me = this;
            return new Promise(function(resolve, reject) {
                if (null != me.getter) {
                    me.getter.abort();
                }
                me.getter = $.ajax({
                    url: $.ajaxSetup()['url'] + '/v1/get-drug-entire-family',
                    type: 'POST',
                    data: {
                        id: me.id,
                        type: me.type,
                    },
                    success: function(data) {
                        if (data.success || false) {
                            me.drug = data.data;
                            resolve(true);
                        }
                        else {
                            Toast.clear();
                            Toast.error('Loading failed', 'Unable to get drug details');
                            reject(data);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        Toast.clear();
                        reject(errorThrown);
                    }
                });
            });
        },
        prepareModal: function() {
            var me = this;
            return new Promise(function(resolve, reject) {
                /**
                 * Main
                 */
                $(me.modal)
                    .find('.drug-icon-list .drug-icon-' + me.type + ' a')
                    .removeClass('text-muted')
                    .removeClass('cursor-initial')
                    .addClass('text-primary')
                    .find('.drug-icon-heading')
                    .addClass('text-primary')
                    .parent()
                    .find('.drug-icon-name')
                    .html(me.drug[me.type][0].main.name);

                /**
                 * Other Details
                 */
                $.each(['experimental', 'brand', 'generic'], function(lidx, ltype) {
                   if (ltype != me.type)  {
                        me._modalAddIconSubLink(ltype);
                   }
                });

                $(me.modal).find('.drug-detail-specific-' + me.type).show();

                $(me.modal).find('.drug-detail-name').html(me.drug[me.type][0].main.name);
                $(me.modal).find('.drug-detail-id').html('#' + me.drug[me.type][0].main.id);
                $(me.modal).find('.drug-detail-uuid').html(me.drug[me.type][0].main.uid);
                $(me.modal).find('.drug-detail-valid').html((me.drug[me.type][0].main.valid ? 'Yes' : 'No'));

                if (me.drug[me.type][0].main.comments) {
                    var comment = me.drug[me.type][0].main.comments;
                    if (comment.length > me.options.comment_trim) {
                        comment = comment.substring(0, me.options.comment_trim) + '...';
                    }
                    $(me.modal).find('.drug-detail-comment').html( comment );
                }
                else {
                    $(me.modal).find('.drug-detail-comment').parents('.row').hide();
                }

                /**
                 * Buttons
                 */
                // edit
                if (me.options.btn_edit) {
                    $(me.modal)
                        .find('.drug-link-edit')
                        .attr('href', $.ajaxSetup()['url'] + __getDrugPageUri('modify', me.type, me.drug[me.type][0].main.id));
                }
                else {
                    $(me.modal)
                        .find('.drug-link-edit')
                        .remove();
                }

                // map
                if (me.options.btn_map) {
                    $(me.modal)
                        .find('.drug-link-map')
                        .attr('href', $.ajaxSetup()['url'] + __getDrugPageUri('map', me.type, me.drug[me.type][0].main.id));
                }
                else {
                    $(me.modal)
                        .find('.drug-link-map')
                        .remove();
                }

                // close
                if (!me.options.btn_close) {
                    $(me.modal)
                        .find('.drug-link-close')
                        .remove();
                }
                else {
                    $(me.modal)
                        .find('.drug-link-close')
                        .addClass(me.options.btn_close_cls);
                }

                // other specific to drug
                switch (me.type) {
                    case 'experimental':
                        $(me.modal).find('.drug-detail-type').html(me.drug.experimental[0].main.type).css('textTransform', 'capitalize');
                        break;

                    case 'generic':
                        for (var i = 1; i <= 5; i++) {
                            $(me.modal).find('.drug-detail-fullcode').append(
                                $('<span></span>')
                                    .html( (me.drug.generic[0].main['level' + i] || '-') )
                                    .addClass('bold pr-1')
                            );
                        }

                        me._modalAddKeyword('typos');
                        me._modalAddKeyword('synonyms');
                        break;

                    case 'brand':
                        me._modalAddKeyword('typos');
                        me._modalAddKeyword('synonyms');
                        break;
                }

                resolve(true);
            });
        },
        show: function() {
            var me = this;
            Toast.clear();

            $(me.modal).on('hidden.bs.modal', function (e) {
                me.hardReset();
            });

            $(me.modal).modal('show')
        },
        hardReset: function() {
            $(this.modal).html(this.template);
        },
        softReset: function() {
            var me = this;
            return new Promise(function(resolve, reject) {
                $(me.modal).find('.drug-icon-list > div')
                    .find('a')
                    .unbind('click')
                    .addClass('text-muted')
                    .addClass('cursor-initial')
                    .removeClass('text-primary')
                    .unbind('click')
                    .attr('data-type', false)
                    .attr('data-id', false)
                    .find('.drug-icon-heading')
                    .removeClass('text-primary');

                $(me.modal).find('.drug-icon-list .drug-icon-name').html('');

                $(me.modal).find('.drug-detail-name').html('');
                $(me.modal).find('.drug-detail-id').html('');
                $(me.modal).find('.drug-detail-uuid').html('');
                $(me.modal).find('.drug-detail-valid').html('');
                $(me.modal).find('.drug-detail-comment').html('');
                $(me.modal).find('.drug-detail-comment').parents('.row').show();
                $(me.modal).find('.drug-detail-fullcode').html('');
                $(me.modal).find('.drug-detail-specific').hide();
                $(me.modal).find('.drug-detail-typos').html('');
                $(me.modal).find('.drug-detail-synonyms').html('');

                $(me.modal).find('.drug-link-edit').attr('href', '');
                $(me.modal).find('.drug-link-map').attr('href', '');

                setTimeout(function() {
                    resolve(true);
                }, 150);
            });
        },
        _modalAddIconSubLink: function(type) {
            if (this.drug[type][0]) {
                var me = this;
                $(me.modal)
                    .find('.drug-icon-list .drug-icon-' + type + ' .drug-icon-name')
                    .html(me.drug[type][0].main.name)
                    .parents('.drug-icon-' + type)
                    .find('a')
                        .attr('data-type', type)
                        .attr('data-id', me.drug[type][0].main.id)
                        .removeClass('cursor-initial')
                        .on('click', function(e) {
                            e.preventDefault();
                            me.id = me.drug[type][0].main.id;
                            me.type = type;
                            if (me.options.switch_recall) {
                                Toast.info('Please wait...', 'Loading drug details', {'icon':'fas fa-spinner fa-pulse'});
                                me.getDrug().then(function() {
                                    me.softReset().then(function() {
                                        me.prepareModal();
                                        Toast.clear();
                                    });
                                });
                            }
                            else {
                                me.softReset().then(function() {
                                    me.prepareModal();
                                });
                            }
                        });
            }
        },
        _modalAddKeyword: function(keyword) {
            if (this.drug[this.type][0][keyword + '_keys'].length) {
                for (var i = 0; i < this.drug[this.type][0][keyword + '_keys'].length; i++) {
                    var tag = false;
                    if ((i + 1) == this.options.keyword_limit) {
                        tag = '...';
                    }
                    else if ((i + 1) <= this.options.keyword_limit) {
                        tag = this.drug[this.type][0][keyword][
                            this.drug[this.type][0][keyword + '_keys'][i]
                        ].name;
                    }
                    if (tag) {
                        $(this.modal).find('.drug-detail-' + keyword).append(
                            $('<span></span>')
                                .html(tag)
                                .attr('title', tag)
                                .attr('data-toggle', 'tooltip')
                                .addClass('tag label text-truncate mx-' + this.options.keyword_width + 'px')
                        );
                    }
                }
                $(this.modal)
                    .find('.drug-detail-' + keyword + '-wrapper')
                    .show();
                $(this.modal).find('.drug-detail-' + keyword + ' span').tooltip();
            }
            else {
                $(this.modal)
                    .find('.drug-detail-' + keyword + '-wrapper')
                    .hide();
            }
        }
    };

    window.DrugViewOverlay = DrugViewOverlay;

    function Plugin(options) {
        return this.each(function() {
            var $this = $(this);

            var id = $this.data('id');
            var type = $this.data('type');

            return new DrugViewOverlay($this, options);
        });
    }

    var old = $.fn.drugViewOverlay;

    $.fn.drugViewOverlay = Plugin;
    $.fn.drugViewOverlay.Constructor = DrugViewOverlay;

    $.fn.drugViewOverlay.defaults = {
        comment_trim: 630,
        keyword_width: 100,
        keyword_limit: 15,
        switch_recall: false,
        btn_edit: true,
        btn_map: true,
        btn_close: true,
        btn_close_cls: 'btn-default',
    };

    $.fn.drugViewOverlay.noConflict = function() {
        $.fn.drugViewOverlay = old;
        return this;
    };

    $(document).ready(function() {
        $('#drug-view-overlay')
            .on('show.bs.modal', function (e) {
                if ($('body').hasClass('map-modal-shown')) {
                    $('#drug-view-overlay').addClass('double-overlay');
                }
            })
            .on('shown.bs.modal', function (e) {
                $('body').addClass('drug-overlay-shown');
            })
            .on('hidden.bs.modal', function (e) {
                if ($('body').hasClass('map-modal-shown')) {
                    $('body').addClass('modal-open');
                    $('#drug-view-overlay').removeClass('double-overlay');
                }

                $('body').removeClass('drug-overlay-shown');
            });
    });
})(window.jQuery);