/**
 * Register External Plug-ins
 */

/**
 * Helpers
 */

function __getDrugIconClass(type, others) {
    others = others || '';
    var cName = '';

    switch (type) {
        case 'experimental':
            cName = 'fa fa-microscope';
            break;

        case 'generic':
            cName = 'fa fa-mortar-pestle';
            break;

        case 'brand':
            cName = 'fa fa-pills';
            break;
    }

    return (cName + ' ' + others).trim();
}

function __getDrugPageUri(page, type, id) {
    var uri = '';

    switch(page) {
        case 'new':
        case 'create':
        case 'register':
            return '/register/' + (type || 'experimental');

        case 'edit':
        case 'modify':
            return '/modify/' + (type || 'experimental') + '/' + (id || 0);

        case 'map':
            return '/drug/map/' + (type || 'open') + '/' + (id || '');

        case 'search':
            return '/drug/search';

        default:
            return null
    }
}

function __getJournalPageUri(page, id) {
    var uri = '';

    switch(page) {
        case 'new':
        case 'create':
        case 'register':
            return '/journal/registry';

        case 'edit':
        case 'modify':
            return '/journal/modify/' + (id || 0);

        case 'search':
            return '/journal/search';

        case 'view':
        default:
            return '/journal/view/' + (id || 0);
            return null
    }
}

function __getConferencePageUri(page, id) {
    var uri = '';

    switch(page) {
        case 'new':
        case 'create':
        case 'register':
            return '/conference/registry';

        case 'edit':
        case 'modify':
            return '/conference/modify/' + (id || 0);

        case 'search':
            return '/conference/search';

        case 'view':
        default:
            return '/conference/view/' + (id || 0);
            return null
    }
}