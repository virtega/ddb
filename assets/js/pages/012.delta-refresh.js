// DELTA REFRESH, JS helper for Meta-Refresh
// - no additional time added, just reset on AJAX requests.
// - will enforce logout.
// - plus ajaxError() with 401 in placed to redirect user @ 101.setup.js
// ===================
(function($) {

    'use strict';

    var DeltaRefresh = function () {
        // seconds; 1440 php default, see config\packages\framework.yaml framework.session.cookie_lifetime
        this.sessionTime   = (typeof CONST_MREFRESH != 'undefined' ? CONST_MREFRESH : 0);
        this.enabled       = false;
        this.useLogout     = false;
        this.timedSec      = 0;
        this.timerInterval = null;
        this.warned        = false;
        this.expiries      = (this.sessionTime + 60); // (1440 + (1*60))
        this.checkCycle    = 300; // every 5m
    };

    DeltaRefresh.prototype = {
        start: function() {
            if (this.sessionTime > 0) {
                this.enabled = true;
                this.reset();

                var me = this;
                this.timerInterval = setInterval(function() {
                    me.timedSec -= me.checkCycle;

                    if (0 >= me.timedSec) {
                        me.timeUp();
                    }
                    else if (me.checkCycle >= me.timedSec) {
                        me.warnUser((me.checkCycle / 60) + ' minutes');
                    }
                }, (1000 * me.checkCycle));
            }
        },
        reset: function() {
            if (this.enabled) {
                this.timedSec = this.expiries;
            }
        },
        warnUser: function(secToExpired) {
            if (
                (this.enabled) &&
                (!this.warned)
            ) {
                this.warned = true;
                Toast.warning('No Activity', 'Session Expiring in ' + secToExpired + '.', {timeout: 10});
            }
        },
        timeUp: function() {
            if (this.enabled) {
                if (this.timerInterval) {
                    clearInterval(this.timerInterval);
                }

                if (this.useLogout) {
                    window.location.href = $.ajaxSetup()['url'] + '/logout?msg=Session+expired';
                }
                else {
                    window.location.href = window.location.href;
                }
            }
        }
    }

    /**
     * Set off Delta Refresh, and reset on AJAX requests
     */
    var dRsh = new DeltaRefresh();
    dRsh.start();
    $(document).ajaxComplete(function(event, xhr, settings) {
        dRsh.reset();
    });
})(window.jQuery);