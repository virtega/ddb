/**
 * SIMPLE PLUGINS
 */
var __saveData = (function () {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    return function (data, fileName) {
        var blob = new Blob([data], {type: "octet/stream"}),
                    url = window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = fileName;
                    a.click();
        window.URL.revokeObjectURL(url);
    };
}());


/**
 * Method to create random string
 *
 * @param  int  length
 *
 * @return string
 */
function makeRandomString(length)
{
    if (length == undefined) {
        length = 5;
    }
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@$#$^%*";
    for (var i = 0; i < length; i++)
    {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}


/**
 * AJAX SETUP
 */
CONST_ROOT = (typeof CONST_ROOT != 'undefined' ?  CONST_ROOT : '');
$.ajaxSetup({
    url     : CONST_ROOT.replace(/\/$/, ''),
    type    : 'POST',
    dataType: 'json',
    accepts : {
        json: 'application/json'
    },
});

$(document).ajaxError(function(event, jqXHR, settings, errorThrown) {
    if (settings.suppressErrors) {
        return;
    }

    if (401 == jqXHR.status) {
        // handle expired session
        // see DeltaRefresh
        Swal.fire({
            title: 'Session expired',
            text: 'Redirecting to login page...',
            type: 'error',
            timer: 5000,
        }).then(function() {
            window.location.href = $.ajaxSetup()['url'] + '/login?error=Session+expired&redirect=' + window.location.href;
        });
    }
    else if ('abort' != errorThrown) {
        var capt = 'Code ' +  jqXHR.status + ' ' + errorThrown;
        var msg = errorThrown;
        if ((jqXHR.responseJSON !== undefined) && (jqXHR.responseJSON.msg !== undefined)) {
            msg = jqXHR.responseJSON.msg;
        }

        Swal.fire(capt, msg, 'error');
        console.error([jqXHR, settings, errorThrown]);
    }
});