// SEARCH PROCESSOR
// ===================
(function($) {

    'use strict';

    var OverlaySearchProcessor = function () {
        this.suggestions       = '#overlay-suggestions';
        this.sectionDrug       = '#search-overlay-search-results-drug';
        this.sectionJournal    = '#search-overlay-search-results-journal';
        this.sectionConference = '#search-overlay-search-results-conference';

        this.searchTerm = '';
        this.onSubmit = false;

        this.topResult = {
            caption: null,
            href: null,
        };

        this.progressDrug = null;
        this.progressJournal = null;
        this.progressConference = null;
    };

    OverlaySearchProcessor.prototype = {
        search: function(searchTerm, onSubmit) {
            this.searchTerm = searchTerm || '';
            this.onSubmit = onSubmit || false;
            this.searchTerm = this.searchTerm.trim();

            if (this.onSubmit) {
                this.executeSuggestion();
            }
            else {
                this.searchConference();
                this.searchJournal();
                this.searchDrug();
            }
        },
        executeSuggestion: function() {
            if (
                (this.onSubmit) &&
                (null !== this.topResult.href)
            ) {
                window.location.href = this.topResult.href;
            }

            if (null !== this.topResult.href) {
               $(this.suggestions).html(
                    $('<a></a>')
                        .attr({
                            'href': this.topResult.href,
                            'title': 'Hit ENTER to view the suggestion item',
                        })
                        .html(this.topResult.caption)
                );
            }
        },
        searchDrug: function() {
            var me = this;

            if (null !== me.progressDrug) {
                me.progressDrug.abort();
            }

            me.setCaption(me.sectionDrug, 'Searching...');
            me.setProgressSearching(me.sectionDrug);
            $(me.sectionDrug).find('.result-results').children().remove();

            if (me.searchTerm.length > 0) {
                var count = 0;
                me.progressDrug = $.ajax({
                    url: $.ajaxSetup()['url'] + '/v1/search-by-name',
                    type: 'GET',
                    data: {
                        q: me.searchTerm,
                        auct: 1,
                    },
                    success: function(data) {
                        count = data.length;
                        for (var idx = 0; idx < count; idx++) {
                            if (0 == idx) {
                                me.topResult.caption = 'Drug: ' + data[idx].name + ', ' + data[idx].desc;
                                me.topResult.href    = __getDrugPageUri('modify', data[idx].family, data[idx].id);
                                me.executeSuggestion();
                            }

                            var row = $(me.sectionDrug).find('.result-template .template').children().clone(false, false);

                            $(row).find('.drug-icon').addClass(__getDrugIconClass(data[idx].family, 'fa-2x'));
                            $(row).find('.drug-name').html(data[idx].name);
                            $(row).find('.drug-desc').html(data[idx].desc);

                            $(row).attr('id', ['drug', data[idx].family, data[idx].id].join('-'))
                                .attr('data-id', data[idx].id)
                                .attr('data-type', data[idx].family)
                                .drugViewOverlay();

                            $(me.sectionDrug).find('.result-results').append(row);
                        }
                    },
                    complete: function(jqXHR, textStatus) {
                        if (count > 0) {
                            me.setProgressDone(me.sectionDrug);
                            me.setCaption(
                                me.sectionDrug,
                                'Found ' + count + ' Drug' + (count > 1 ? 's' : ''),
                                __getDrugPageUri('search') + '?q=' + me.searchTerm
                            );
                        }
                        else {
                            me.setProgressEmpty(me.sectionDrug);
                            me.setCaption(me.sectionDrug, 'No Drug found');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        me.setProgressError(me.sectionDrug);
                    }
                });
            }
            else {
                me.setProgressNull(me.sectionDrug);
                me.setCaption(me.sectionDrug);
            }
        },
        searchJournal: function() {
            var me = this;

            if (null !== me.progressJournal) {
                me.progressJournal.abort();
            }

            me.setCaption(me.sectionJournal, 'Searching...');
            me.setProgressSearching(me.sectionJournal);
            $(me.sectionJournal).find('.result-results').children().remove();

            if (me.searchTerm.length > 0) {
                var count = 0;
                me.progressJournal = $.ajax({
                    url: $.ajaxSetup()['url'] + '/v1/search-journal-by-name',
                    type: 'GET',
                    data: {
                        q: me.searchTerm,
                    },
                    success: function(data) {
                        count = data.overall_count;
                        for (var idx = 0; idx < data.count; idx++) {
                            if (0 == idx) {
                                me.topResult.caption = 'Journal: ' + data.data[idx].name;
                                me.topResult.href    = __getJournalPageUri('view', data.data[idx].id);
                                me.executeSuggestion();
                            }

                            var row = $(me.sectionJournal).find('.result-template .template').children().clone(false, false);

                            $(row)
                                .find('.journal-name')
                                .html(data.data[idx].name)
                                .attr('title', 'Abbv: ' + data.data[idx].abbrevation);

                            $(row).find('.journal-desc').append(
                                me.getDescAttrSpan(
                                    (data.data[idx].status == 'None' ? 'danger' : (data.data[idx].status == 'Archived' ? 'primary' : 'info')),
                                    'Status: ' + data.data[idx].status,
                                    data.data[idx].status
                                )
                            );

                            if (data.data[idx].is_medline) {
                                $(row).find('.journal-desc').append(
                                    me.getDescAttrSpan('info', 'Is a MedLine Journal', 'MedLine')
                                );
                            }

                            if (data.data[idx].is_secondline) {
                                $(row).find('.journal-desc').append(
                                    me.getDescAttrSpan('info', 'Is SecondLine Journal', 'SecondLine')
                                );
                            }

                            if (data.data[idx].is_dgabstract) {
                                $(row).find('.journal-desc').append(
                                    me.getDescAttrSpan('info', 'Is DgAbstract Journal', 'DgAbstract')
                                );
                            }

                            if (data.data[idx].medline_issn.length > 0) {
                                $(row).find('.journal-desc').append(
                                    me.getDescAttrSpan('info', 'ISSN: ' + data.data[idx].medline_issn, data.data[idx].medline_issn)
                                );
                            }

                            if (data.data[idx].specialties.length > 0) {
                                var specCount = data.data[idx].specialties.length;
                                var specialtiesStr = data.data[idx].specialties.join(', ');
                                $(row).find('.journal-desc').append(
                                    me.getDescAttrSpan('info',
                                        (specCount > 1 ? ' Specialties' : ' Specialty') + ': ' + specialtiesStr,
                                        (specCount + (specCount > 1 ? ' Specialties' : ' Specialty'))
                                    )
                                );
                            }

                            $(row).attr('id', 'journal-' + data.data[idx].id)
                                .attr('data-type', 'journal')
                                .attr('data-id', data.data[idx].id)
                                .on('click', function() {
                                    window.location.href = __getJournalPageUri('view', $(this).data('id'));
                                });

                            $(me.sectionJournal).find('.result-results').append(row);
                        }
                    },
                    complete: function(jqXHR, textStatus) {
                        if (count > 0) {
                            me.setProgressDone(me.sectionJournal);
                            me.setCaption(
                                me.sectionJournal,
                                'Found ' + count + ' Journal' + (count > 1 ? 's' : ''),
                                __getJournalPageUri('search') + '?name=' + me.searchTerm
                            );
                        }
                        else {
                            me.setProgressEmpty(me.sectionJournal);
                            me.setCaption(me.sectionJournal, 'No Journal found');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        me.setProgressError(me.sectionJournal);
                    }
                });
            }
            else {
                me.setProgressNull(me.sectionJournal);
                me.setCaption(me.sectionJournal);
            }
        },
        searchConference: function() {
            var me = this;

            if (null !== me.progressConference) {
                me.progressConference.abort();
            }

            me.setCaption(me.sectionConference, 'Searching...');
            me.setProgressSearching(me.sectionConference);
            $(me.sectionConference).find('.result-results').children().remove();

            if (me.searchTerm.length > 0) {
                var count = 0;
                me.progressConference = $.ajax({
                    url: $.ajaxSetup()['url'] + '/v1/search-conference-by-name',
                    type: 'GET',
                    data: {
                        q: me.searchTerm,
                    },
                    success: function(data) {
                        count = data.overall_count;
                        for (var idx = 0; idx < data.count; idx++) {
                            if (0 == idx) {
                                me.topResult.caption = 'Conference: ' + data.data[idx].name;
                                me.topResult.href    = __getConferencePageUri('view', data.data[idx].id);
                                me.executeSuggestion();
                            }

                            var row = $(me.sectionConference).find('.result-template .template').children().clone(false, false);

                            $(row)
                                .find('.conference-name')
                                .html(data.data[idx].name)
                                .attr('title', data.data[idx].name);

                            $(row).find('.conference-desc .date-locality').html(
                                data.data[idx].event_span
                                    + ((data.data[idx].event_span != null && data.data[idx].event_span.length > 0)
                                            ? (data.data[idx].locality.length > 0 ? ', at ' : '')
                                            : (data.data[idx].locality.length > 0 ? 'At ' : '')
                                    )
                                    + data.data[idx].locality

                            );

                            if (data.data[idx].is_online) {
                                $(row).find('.conference-desc .attrs').append(
                                    me.getDescAttrSpan('info', 'Is an Online Conference', 'Online')
                                );
                            }

                            if (data.data[idx].is_key_conference) {
                                $(row).find('.conference-desc .attrs').append(
                                    me.getDescAttrSpan('info', 'Is a Key Conference', 'Key Conference')
                                );
                            }

                            if (data.data[idx].is_cruise) {
                                $(row).find('.conference-desc .attrs').append(
                                    me.getDescAttrSpan('info', 'Is on Cruise Conference', 'Cruise')
                                );
                            }

                            if (data.data[idx].specialties.length > 0) {
                                var specCount = data.data[idx].specialties.length;
                                var specialtiesStr = data.data[idx].specialties.join(', ');
                                $(row).find('.conference-desc .attrs').append(
                                    me.getDescAttrSpan(
                                        'info',
                                        (specCount > 1 ? ' Specialties' : ' Specialty') + ': ' + specialtiesStr,
                                        specCount + (specCount > 1 ? ' Specialties' : ' Specialty')
                                    )
                                );
                            }

                            if (data.data[idx].is_discontinued) {
                                $(row).find('.conference-desc .attrs').append(
                                    me.getDescAttrSpan('danger', 'Conference has Discontinued', 'Discontinued')
                                );
                            }

                            $(row).attr('id', 'conference-' + data.data[idx].id)
                                .attr('data-type', 'conference')
                                .attr('data-id', data.data[idx].id)
                                .on('click', function() {
                                    window.location.href = __getConferencePageUri('view', $(this).data('id'));
                                });

                            $(me.sectionConference).find('.result-results').append(row);
                        }
                    },
                    complete: function(jqXHR, textStatus) {
                        if (count > 0) {
                            me.setProgressDone(me.sectionConference);
                            me.setCaption(
                                me.sectionConference,
                                'Found ' + count + ' Conference' + (count > 1 ? 's' : ''),
                                __getConferencePageUri('search') + '?name=' + me.searchTerm
                            );
                        }
                        else {
                            me.setProgressEmpty(me.sectionConference);
                            me.setCaption(me.sectionConference, 'No Conference found');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        me.setProgressError(me.sectionConference);
                    }
                });
            }
            else {
                me.setProgressNull(me.sectionConference);
                me.setCaption(me.sectionConference);
            }
        },
        setCaption: function(section, caption, link) {
            caption = caption || '';
            if (link !== undefined) {
                $(section).find('.result-heading-caption')
                    .html('')
                    .append(
                        $('<a></a>')
                            .html(caption)
                            .attr('href', link)
                    );
            }
            else {
                $(section).find('.result-heading-caption').html(caption);
            }
        },
        setComingSoon: function(section) {
            var caption = $(section).find('.result-heading-caption');
            if ('' == $(caption).html()) {
                $(caption).html('Coming soon');
            }
        },
        setProgressSearching: function(section) {
            this.setProgressNull(section);
            $(section).find('.result-progress').html(
                $('<div/>')
                    .addClass('progress-bar-indeterminate')
            );
        },
        setProgressDone: function(section) {
            this.setProgressNull(section);
            $(section).find('.result-progress').html(
                $('<div/>')
                    .addClass('progress-bar progress-bar-success')
                    .css({'width': '100%'})
            );
        },
        setProgressEmpty: function(section) {
            this.setProgressNull(section);
            $(section).find('.result-progress').html(
                $('<div/>')
                    .addClass('progress-bar progress-bar-warning')
                    .css({'width': '100%'})
            );
        },
        setProgressError: function(section) {
            this.setProgressNull(section);
            $(section).find('.result-progress').html(
                $('<div/>')
                    .addClass('progress-bar progress-bar-danger')
                    .css({'width': '100%'})
            );
        },
        setProgressNull: function(section) {
            $(section).find('.result-progress > div').remove();
        },
        getDescAttrSpan: function(type, title, caption) {
            type    = type || 'info';
            title   = title || '';
            caption = caption || '';

            var desc = $('<span></span>')
                .addClass('badge badge-' + type + ' mb-1 mr-1')
                .attr('title', title)
                .text(caption)

            return desc;
        }
    };

    window.OverlaySearchProcessor = OverlaySearchProcessor;
})(window.jQuery);