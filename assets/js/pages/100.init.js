var pagesReInitializeAll;

var paceOptions = {
    target: '.jumbotron',
    ajax: {
        trackMethods: ['GET', 'POST'],
    },
};

(function($) {
    'use strict';

    /**
     * Reinitialize all Pages related plugin
     *
     * @param {boolean}|null  restartPace  Just replay Pace, best any sub-page-Ajax tracked by individual instances
     * @param {Function}|null callback     Hook, method to call once this is complete
     *
     * @return {void}
     */
    pagesReInitializeAll = function (restartPace, callback) {
        restartPace = restartPace || false;
        if (true === restartPace) {
            // reanimate pace
            Pace.restart();
        }

        /**
         * PAGES PLUGINS
         */
        $.Pages.init();

        /**
         * CARD
         */
        $('[data-pages="card"]').each(function() {
            var $card = $(this)
            // create card
            $card.card($card.data());

            // add header event
            if (
                ($(this).find('.card-header').length) &&
                ($(this).find('.card-controls li a[data-toggle="collapse"]').length)
            ) {
                $(this).find('.card-header').addClass('cursor');
            }
        });

        /**
         * SEARCH OVERLAY
         */
        var searchProcessor = new OverlaySearchProcessor();
        $('[data-pages="search"]').search({
            searchField: '#overlay-search',
            closeButton: '.overlay-close',
            suggestions: '#overlay-suggestions',
            brand: '.brand',
            onSearchSubmit: function(searchString) {
                $('.search-results').fadeIn('fast');
                searchProcessor.search(searchString, true);
            },
            onKeyEnter: function(searchString) {
                $('.search-results').fadeIn('fast');
                searchProcessor.search(searchString, false);
            }
        });

        /**
         * DRUG VIEW OVERLAY
         */
        $('[data-toggle="drug-view-overlay"]').drugViewOverlay({'start': 'new'});

        /**
         * EXTERNAL HOOK
         */
        if (callback && typeof callback === 'function') {
            callback();
        }

        /**
         * FORM HACK
         */
        // form-auto fill / complete
        // manage to disable few of Chromes autofill, but not all
        $('form[autocomplete="off"]').each(function(index, el) {
            $(el).attr('autocomplete', makeRandomString(10));
            $(el).find('input, select, textarea').each(function(sin, sel) {
                $(sel).attr('autocomplete', makeRandomString(10));
            });
        });
    };

    $(document).ready(function() {
        pagesReInitializeAll(false, null);
    });
})(window.jQuery);