(function($) {
    'use strict';
    var DrugMap = function (mapData) {
        this.drugOverlay = new DrugViewOverlay();

        this.mapCanvas = '#drug-map';
        this.mapCover  = '#drug-map-cover';

        this.btnSave        = '#map-save';
        this.btnAutoSave    = '#autosave-map';
        this.btnAuyoSaveInd = '#autosave-map-indicator';

        this.btnExport    = '#export-drug';

        this.mapAct       = '#drug-map-actions';
        this.mapActRevRel = '#drug-map-action-remove-relation';

        this.mapDump      = '#drug-map-data';

        this.mapData = mapData;

        this.linkHistory = {
            created: [],
            removed: [],
        };

        this.mapLastKey = Object.keys((this.mapData.operators || []));
        this.mapCalculated = (this.mapLastKey.length == 0);

        this.mappedExperimental = 0;
        this.mappedGeneric      = 0;
        this.mappedBrand        = 0;

        this.savingProcess = null;
        this.mapCoverMaxSavingSec = 5000; // cycle of 0.5s

        this.init();
    };
    DrugMap.prototype = {
        init: function() {
            if (!this.mapCalculated) {
                this.mapLastKey = this.mapLastKey[this.mapLastKey.length - 1];
            }

            this.initFlowChart();
        },
        initFlowChart: function() {
            var dm = this;
            $(dm.mapCanvas).flowchart({
                data: dm.mapData,
                defaultLinkColor: '#3366ff',
                defaultSelectedLinkColor: '#46d4f5',
                multipleLinksOnInput: true,
                multipleLinksOnOutput: true,
                onOperatorCreate: function(operatorId, operatorData) {
                    if (operatorData.title_changed || false) {
                        return true;
                    }

                    // last item, then run this
                    if (
                        (!dm.mapCalculated) ||
                        (dm.mapLastKey == operatorId)
                    ) {
                        dm.recalculateDrugOnMap(true, operatorData.drug.type);
                        dm.recalcAndResizeMap();

                        // stop enforce run on every invoke next time
                        dm.mapCalculated = true;
                    }

                    // change to icon
                    operatorData.properties.title = '<i class="' + __getDrugIconClass(operatorData.drug.type, 'mr-1') + '"></i>&nbsp;' + operatorData.drug.name;
                    operatorData.title_changed = true;

                    // recall
                    $(dm.mapCanvas).flowchart('createOperator', operatorId, operatorData);

                    // reject this one
                    return false;
                },
                onLinkCreate: function(linkId, linkData) {
                    // check if given loaded map_data or generated one.
                    // generated one in INTEGER
                    if (Math.floor(linkId) == linkId && $.isNumeric(linkId)) {
                        // read from link data
                        var from = linkData.fromOperator.split('_');
                        var to = linkData.toOperator.split('_');

                        // check relation basic
                        if (
                            // avoid self inflict gun wound
                            (linkData.fromOperator == linkData.toOperator) ||
                            // e must to g only
                            ((from[0] == 'e') && (to[0] != 'g')) ||
                            // g must to b only
                            ((from[0] == 'g') && (to[0] != 'b')) ||
                            // no b to anything
                            (from[0] == 'b')
                        ) {
                            Swal.fire('Not Allowed', 'Such relationship is not allowed.', 'error');
                            return false;
                        }

                        // get current data
                        var currdata = dm.getCurrentData();

                        // relation check advance
                        if (to[0] == 'g') {
                            // generic connection from experimental
                            for (var link_key in currdata.links) {
                                link_key = link_key.split('_');
                                if (
                                    (link_key[0] == 'e') &&
                                    (link_key[1] == 'g') &&
                                    (link_key[3] == to[1])
                                ) {
                                    // found previous connection to brand
                                    Swal.fire('Not Allowed', 'Generic drug can only have One (1) Experimental relation.', 'error');
                                    return false;
                                }
                            }
                        }
                        else if (to[0] == 'b') {
                            // brand connection from generic
                            for (var link_key in currdata.links) {
                                link_key = link_key.split('_');
                                if (
                                    (link_key[0] == 'g') &&
                                    (link_key[1] == 'b') &&
                                    (link_key[3] == to[1])
                                ) {
                                    // found previous connection to brand
                                    Swal.fire('Not Allowed', 'Brand drug can only have One (1) Generic relation.', 'error');
                                    return false;
                                }
                            }
                        }

                        // generate link name
                        linkId = from[0] + '_' + to[0] + '_' + from[1] + '_' + to[1];

                        // if defined on current data, reject it!
                        if (currdata.links[linkId] !== undefined) {
                            Swal.fire('Not Allowed', 'Duplicated relationship is not allowed.', 'error');
                            return false;
                        }

                        // creating new one instead; sending another createlink
                        // this will be rejected
                        linkData.color = 'red';
                        $(dm.mapCanvas).flowchart('createLink', linkId, linkData);

                        // recalculate operator-labels
                        dm.actionAsLinkChangedAfterLoad();

                        // show button
                        dm.saveMap();

                        // push to history
                        dm.linkHistory.created.push([linkId, linkData]);

                        // reject defaults
                        return false;
                    }

                    // all id in string is from data
                    return true
                },
                onOperatorSelect: function(operatorId) {
                    // get current data
                    var currdata = dm.getCurrentData();
                    if (currdata.operators[operatorId].drug) {
                        dm.drugOverlay.setTarget(currdata.operators[operatorId].drug.id, currdata.operators[operatorId].drug.type);
                        dm.drugOverlay.openTarget();
                    }
                    return true;
                },
                onLinkSelect: function(linkId) {
                    // prep remove link button
                    var currdata = dm.getCurrentData();
                    dm.showActionUnlinkRel(
                        linkId,
                        currdata.operators[ currdata.links[linkId].toOperator ].top,
                        currdata.operators[ currdata.links[linkId].toOperator ].left
                    );
                    return true;
                },
                onOperatorMoved: function(operatorId, position) {
                    var openActLink = $(dm.mapActRevRel).attr('data-link-id');
                    if (openActLink != undefined) {
                        var currLink = openActLink.split('_');
                        if (operatorId == (currLink[1] + '_' + currLink[3])) {
                            $(dm.mapAct)
                                .css({
                                    'top'  : (position.top + 30) + 'px',
                                    'left' : (position.left - 5) + 'px',
                                    'right': 'unset',
                                });
                        }
                    }
                    return true;
                }
            });
        },
        getCurrentData: function() {
            return $(this.mapCanvas).flowchart('getData');
        },
        enableActionButton: function(id, newClass) {
            newClass = 'btn-' + (newClass || 'primary');
            if (!$(id).hasClass(newClass)) {
                $(id)
                    .addClass(newClass)
                    .removeClass('btn-secondary')
                    .removeClass('disabled')
                    .attr('disabled', false);
            }
        },
        disableActionButton: function(id) {
            if (!$(id).hasClass('btn-secondary')) {
                $(id)
                    .addClass('btn-secondary')
                    .addClass('disabled')
                    .removeClass('btn-primary')
                    .removeClass('btn-danger')
                    .removeClass('btn-warning')
                    .removeClass('btn-info')
                    .removeClass('btn-success')
                    .removeClass('active')
                    .attr('disabled', true);
            }
        },
        showActionUnlinkRel: function(linkid, ntop, nleft)  {
            var ptop = ntop || 0;
            var pleft = nleft || 30;
            $(this.mapActRevRel).attr('data-link-id', linkid);
            $(this.mapAct)
                .css({
                    'top'  : (ptop + 30) + 'px',
                    'left' : (pleft - 5) + 'px',
                    'right': 'unset',
                })
                .show();
        },
        hideActionUnlinkRel: function()  {
            $(this.mapActRevRel).attr('data-link-id', null);
            $(this.mapAct)
                .hide()
                .css({
                    'top'  : '45px',
                    'right': '30px',
                    'left' : 'unset',
                });
        },
        /**
         * method to count by type, drug on Map
         */
        recalculateDrugOnMap: function(showExport, extra_entity) {
            // reset
            this.mappedExperimental = 0;
            this.mappedGeneric = 0;
            this.mappedBrand = 0;

            // get on map counts
            var currdata = this.getCurrentData();
            for (var operatorKey in currdata.operators) {
                operatorKey = operatorKey.split('_');
                switch(operatorKey[0]) {
                    case 'e': this.mappedExperimental++; break;
                    case 'g': this.mappedGeneric++; break;
                    case 'b': this.mappedBrand++; break;
                }
            }

            // add extra-entity - see map event
            if (extra_entity !== undefined) {
                // avoid using eval
                // One thing  I love about, PHP for variable-variables.
                switch (extra_entity) {
                    case 'experimental': this.mappedExperimental++; break;
                    case 'generic'     : this.mappedGeneric++; break;
                    case 'brand'       : this.mappedBrand++; break;
                }
            }

            // show export button
            if (true === showExport) {
                var renderedDrugCount = (this.mappedExperimental + this.mappedGeneric + this.mappedBrand);
                if (renderedDrugCount > 0) {
                    this.enableActionButton(this.btnExport);
                }
            }
        },
        /**
         * change map size dynamically,
         * - only Brand have can render sideways..
         * - so others to bottom
         */
        recalcAndResizeMap: function() {
            // initial padding given - border
            var initpadding = 20;
            // additional view padding
            var viewpadding = 200;
            // sigle operator height ~ 1xOutput + 1xInput
            var peroperator = ((initpadding / 2) + 2 + 69);
            // as on css, minimum allowed
            var minheight   = 250;

            // get max from calculation
            var maxcount = Math.max(this.mappedExperimental, this.mappedGeneric, this.mappedBrand);
            // calculate
            var newheight = (initpadding + viewpadding + (peroperator * maxcount));
            // minimum check
            if (newheight < minheight)  {
                newheight = minheight;
            }
            // apply
            $(this.mapCanvas).parent().css('height', newheight + 'px');
        },
        /**
         * method to recalculate map link number
         * Map service is sending the calculated Links on Operators
         * This is just handling for User link Creation / Deletion only,
         * depending on this alone do effect the performance
         */
        actionAsLinkChangedAfterLoad: function() {
            var flowdata = this.getCurrentData();
            var updateLabel = {};
            $.each(flowdata.links, function(linkId, linkData) {
                var ls = linkId.split('_');
                if (updateLabel['o_' + ls[0] + '_' + ls[2]] == undefined) {
                    updateLabel['o_' + ls[0] + '_' + ls[2]] = 0;
                }
                updateLabel['o_' + ls[0] + '_' + ls[2]]++;

                if (updateLabel['i_' + ls[1] + '_' + ls[3]] == undefined) {
                    updateLabel['i_' + ls[1] + '_' + ls[3]] = 0;
                }
                updateLabel['i_' + ls[1] + '_' + ls[3]]++;
            });

            $.each(flowdata.operators, function(operatorId, operatorData) {
                var changed = false;
                $.each(operatorData.properties.inputs, function(intKey, intData) {
                    var newVal = 0;
                    if (updateLabel[intKey] != undefined) {
                        newVal = updateLabel[intKey];
                    }

                    if (
                        (operatorData.properties.class == 'generic') &&
                        (intData.label != 'E: ' + newVal)
                    ) {
                        operatorData.properties.inputs[intKey].label = 'E: ' + newVal;
                        changed = true;
                    }
                    else if (
                        (operatorData.properties.class == 'brand') &&
                        (intData.label != 'G: ' + newVal)
                    ) {
                        operatorData.properties.inputs[intKey].label = 'G: ' + newVal;
                        changed = true;
                    }
                });
                $.each(operatorData.properties.outputs, function(outKey, outData) {
                    var newVal = 0;
                    if (updateLabel[outKey] != undefined) {
                        newVal = updateLabel[outKey];
                    }

                    if (
                        (operatorData.properties.class == 'experimental') &&
                        (outData.label != 'G: ' + newVal)
                    ) {
                        operatorData.properties.outputs[outKey].label = 'G: ' + newVal;
                        changed = true;
                    }
                    else if (
                        (operatorData.properties.class == 'generic') &&
                        (outData.label != 'B: ' + newVal)
                    ) {
                        operatorData.properties.outputs[outKey].label = 'B: ' + newVal;
                        changed = true;
                    }
                });
                if (changed) {
                    $(this.mapCanvas).flowchart('setOperatorData', operatorId, operatorData);
                }
            });
        },
        /**
         * removing relation on UI only, use save-changes button so store changes
         */
        removeRelation: function(linkid) {
            var link = linkid.split('_');

            // push to history
            this.linkHistory.removed.push([linkid, link]);

            $(this.mapCanvas).flowchart('deleteLink', linkid);

            // recalculate operator-labels
            this.actionAsLinkChangedAfterLoad();

            this.hideActionUnlinkRel();

            this.saveMap();
        },
        /**
         * return list of Drugs ID's in current Map, keyed by Drug Type
         */
        getDrugIdList: function() {
            var currdata = this.getCurrentData();

            var excludes = {};
            for (var operator_key in currdata.operators) {
                // create key by type
                if (excludes[ currdata.operators[operator_key].drug.type ] == undefined) {
                    excludes[ currdata.operators[operator_key].drug.type ] = [];
                }
                // push id
                excludes[ currdata.operators[operator_key].drug.type ].push( currdata.operators[operator_key].drug.id );
            }

            return excludes;
        },
        /**
         * Method to process Add Drug / Search Drug map request.
         * Take raw provided data for Map presentation use
         */
        processAddNewDrugAjaxReponse: function(data) {
            var me = this;
            var currdata = this.getCurrentData();

            $.each(data.data.operators, function(operatorId, operatorData) {
                if (currdata.operators[operatorId] == undefined) {
                    $(me.mapCanvas).flowchart('createOperator', operatorId, operatorData);
                }
            });

            $.each(data.data.links, function(linkId, linkData) {
                if (currdata.links[linkId] == undefined) {
                    $(me.mapCanvas).flowchart('createLink', linkId, linkData);
                }
            });

            me.recalculateDrugOnMap(true);
            me.recalcAndResizeMap();
        },
        /**
         * Method to trigger save action, depending on which save method selected.
         */
        saveMap: function(forceSave) {
            forceSave = forceSave || false;
            var dm = this;

            if ($(dm.btnAutoSave).is(':checked') || forceSave) {
                dm.savingMap();
            }
            else {
                dm.enableActionButton(dm.btnSave, 'danger');
            }
        },
        /**
         * Method to actually saving the map.
         * All interaction purposes should use saveMap();
         * This method return Promise.
         */
        savingMap: function() {
            var dm = this;

            dm.disableActionButton(dm.btnSave);
            $(dm.btnAutoSave).bootstrapToggle('disable');

            $(dm.btnAuyoSaveInd).stop().show();

            return new Promise(function(resolve) {
                var currdata = dm.getCurrentData();
                dm.savingProcess = $.ajax({
                    url   : $.ajaxSetup()['url'] + '/v1/map-update-drug-relations',
                    method: "POST",
                    data  : {
                        operators: JSON.stringify(currdata.operators),
                        links: JSON.stringify(currdata.links)
                    },
                    beforeSend: function(xhr) {
                        if (
                            (dm.savingProcess !== null) &&
                            (dm.savingProcess.readyState != 4)
                        ) {
                            dm.savingProcess.abort();
                        }
                    },
                    success: function(data) {
                        dm.savedUpdateMap();

                        Toast.fire('Map Saved', 'Drugs has been updated', 'success');

                        resolve(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        if ('abort' != errorThrown) {
                            // revert changes
                            $.each(dm.linkHistory.created, function(index, set) {
                                if (typeof set[0] !== undefined) {
                                    $(dm.mapCanvas).flowchart('deleteLink', set[0]);
                                }
                            });

                            $.each(dm.linkHistory.removed, function(index, rev) {
                                if (typeof rev[0] !== undefined) {
                                    $(dm.mapCanvas).flowchart('createLink', rev[0], {
                                        fromOperator :        rev[1][0] + '_' + rev[1][2],
                                        fromConnector: 'o_' + rev[1][0] + '_' + rev[1][2],
                                        toOperator   :        rev[1][1] + '_' + rev[1][3],
                                        toConnector  : 'i_' + rev[1][1] + '_' + rev[1][3],
                                    });
                                }
                            });

                            // recalculate operator-labels
                            dm.actionAsLinkChangedAfterLoad();
                        }
                    },
                }).always(function(jqXHR, textStatus, errorThrown) {
                    if ('abort' != errorThrown) {
                        $(dm.btnAutoSave).bootstrapToggle('enable');
                        $(dm.btnAuyoSaveInd).stop().hide();

                        // always; clear history
                        dm.linkHistory.created = [];
                        dm.linkHistory.removed = [];
                    }
                });

                dm.savingTimeCheck();
            });
        },
        /**
         * Method to reset map, and fix Link calculation
         */
        savedUpdateMap: function() {
            var currdata = this.getCurrentData();

            var labels      = ['inputs', 'outputs'];
            var linkOpertor = ['fromConnector', 'toConnector'];
            var linkCounts  = {};

            for (var link_key in currdata.links) {
                // change red to default
                currdata.links[link_key].color = '#3366ff';

                // get count
                for (var lidx = 0; lidx < linkOpertor.length; lidx++) {

                    if (linkCounts[ currdata.links[link_key][ linkOpertor[lidx] ] ] == undefined) {
                        linkCounts[ currdata.links[link_key][ linkOpertor[lidx] ] ] = 0;
                    }

                    linkCounts[ currdata.links[link_key][ linkOpertor[lidx] ] ]++;
                }
            }

            // initiate label recalculation
            for (var operator_key in currdata.operators) {
                var drug = {
                    inputs: '',
                    outputs: '',
                };
                switch (currdata.operators[operator_key].drug.type) {
                    case 'experimental':
                        drug.outputs = 'G';
                        break;

                    case 'generic':
                        drug.inputs = 'E';
                        drug.outputs = 'B';
                        break;

                    case 'brand':
                        drug.inputs = 'B';
                        break;
                }

                for (var tidx = 0; tidx < labels.length; tidx++) {
                    if (currdata.operators[operator_key].properties[labels[tidx]] != null) {
                        for (var linkKey in currdata.operators[operator_key].properties[labels[tidx]]) {
                            var count = linkCounts[linkKey] || 0;
                            currdata.operators[operator_key].properties[labels[tidx]][linkKey].label = drug[labels[tidx]] + ': ' + count;
                        }
                    }
                }
            }

            // redraw
            $(this.mapCanvas).flowchart('setData', currdata);
        },
        /**
         * Method checking ajax saving process
         */
        savingTimeCheck: function() {
            var dm = this;

            var timed = 0;
            var cycle = 500;

            var checkingIntv = setInterval(function() {
                timed += cycle;

                if (
                    (dm.savingProcess === null) ||
                    (dm.savingProcess.readyState == 0) // abort too
                ) {
                    // reset
                    timed = 0;
                }
                else if (dm.savingProcess.readyState == 4) {
                    // stop
                    clearInterval(checkingIntv);
                    if ($(dm.mapCover).is(':visible')) {
                        $(dm.mapCover).fadeOut();
                    }
                }
                else if (timed >= dm.mapCoverMaxSavingSec) {
                    // cover now
                    if (!$(dm.mapCover).is(':visible')) {
                        $(dm.mapCover).fadeIn();
                    }
                }
            }, cycle);
        },
        /**
         * Method to handle export process
         */
        exportCsv: function() {
            var typeIds = this.getDrugIdList();

            Swal.mixin({
                input: 'text',
                confirmButtonText: 'Next &rarr;',
                showCancelButton: true,
                progressSteps: ['1'],
                onBeforeOpen: function() {
                    Swal.hideProgressSteps();
                },
            }).queue([
                {
                    title: 'Which Export type you would like?',
                    input: 'select',
                    inputOptions: {
                        'complete': 'Complete Drugs Data',
                        'names'   : 'Drugs Name Only',
                    },
                    inputValue: 'complete',
                    showCancelButton: true
                },
            ]).then(function (result) {
                if (result.value) {
                    Swal.fire({
                        title: 'Sending export request...',
                        type: 'info',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showCloseButton: false,
                        allowEscapeKey: false,
                        onBeforeOpen: function() {
                            Swal.showLoading();
                            $.ajax({
                                url    : $.ajaxSetup()['url'] + '/v1/export-id-search',
                                method : "POST",
                                dataType: 'text',
                                data   : {
                                    experimental: typeIds.experimental,
                                    generic     : typeIds.generic,
                                    brand       : typeIds.brand,
                                    less        : ('complete' == result.value[0] ? 0 : 1)
                                },
                                success: function(data, text, response) {
                                    var content_type = response.getResponseHeader('Content-Type');
                                    if (content_type.indexOf('text/csv') >= 0) {
                                        var header   = response.getResponseHeader('Content-Disposition');
                                        var filename = header.match(/filename="?(.+)"?/)[1];
                                        __saveData(data, filename);

                                        Swal.fire(
                                            'Export complete!',
                                            'Browser will ask/show download in progress..',
                                            'success'
                                        );
                                    }
                                    else {
                                        Swal.fire('Export Failed', 'Content Type unknown: ' + content_type, 'error');
                                    }
                                },
                            });
                        },
                    });
                }
            });
        },
    };

    var DrugMapModalOpt = function () {
        this.toastOpts = {
            timeout: 5000,
            icon: 'fa fa-exclamation'
        };
    };
    DrugMapModalOpt.prototype = {
        getToastOpt: function() {
            return this.toastOpts;
        },
    };

    var DrugMapNewDrug = function (drugMap) {
        this.opts    = new DrugMapModalOpt();
        this.drugMap = drugMap;
        this.type    = '';
        this.name    = '';
        this.valid   = false;
    };
    DrugMapNewDrug.prototype = {
        search: function() {
            this.type = $('.inp-new-type:checked').val();
            this.name = $('#inp-new-name').val().trim();
            this.valid = $('#inp-new-valid').is(':checked');

            if (this.type.length == 0) {
                Toast.fire('Not Allowed', 'New drug Type is required', 'error', this.opts.getToastOpt());
            }
            else if (this.name.length == 0) {
                Toast.fire('Not Allowed', 'New drug Name is required', 'error', this.opts.getToastOpt());
            }
            else {
                var dm = this.drugMap;
                dm.recalculateDrugOnMap(false);
                $.ajax({
                    url: $.ajaxSetup()['url'] + '/v1/add-new-map-drug',
                    method: "POST",
                    data: {
                        type: this.type,
                        name: this.name,
                        valid: (this.valid ? 1 : 0),
                        format: 'map',
                        mapped_experimental: dm.mappedExperimental,
                        mapped_generic: dm.mappedGeneric,
                        mapped_brand: dm.mappedBrand,
                    },
                    beforeSend: function(jqXHR, settings) {
                        Swal.fire({
                            title: 'Creating Drug...',
                            type: 'info',
                            allowOutsideClick: false,
                            showCancelButton: false,
                            showCloseButton: false,
                            allowEscapeKey: false,
                            onBeforeOpen: function() {
                                Swal.showLoading();
                            }
                        });
                    },
                    success: function(data, text, response) {
                        dm.processAddNewDrugAjaxReponse(data);
                        Swal.fire({
                            title: 'Drug creation Complete',
                            text: 'The Drug should appear on the map...',
                            type: 'success',
                            timer: 1500,
                        }).then(function() {
                            $('#drug-map-add-new-modal').modal('hide');
                            $('#clear-new-drug').trigger('click');
                        });
                    }
                });
            }
        },
        clear: function() {
            $('.inp-new-type').prop('checked', false);
            $('#new-type-experimental').prop('checked', true);
            $('#inp-new-name').val('');
            $('#inp-new-valid').bootstrapToggle('off');
        }
    };

    var DrugMapExitstingDrugSearch = function (drugMap) {
        this.opts      = new DrugMapModalOpt();
        this.drugMap   = drugMap;
        this.type      = '';
        this.name      = '';
        this.result    = '#existing-drug-search-results';
        this.searching = null;
    };
    DrugMapExitstingDrugSearch.prototype = {
        search: function(submit) {
            submit = submit || false;
            this.type = $('.inp-existing-type:checked').val();
            this.name = $('#inp-existing-name').val().trim();

            if (this.type.length == 0) {
                Toast.fire('Not Allowed', 'Drug Type is required', 'error', this.opts.getToastOpt());
            }
            else if (
                (submit) &&
                (this.name.length == 0)
            ) {
                Toast.fire('Not Allowed', 'Drug Name is required', 'error', this.opts.getToastOpt());
            }
            else if (this.name.length > 0) {
                var src = this;
                src.searching = $.ajax({
                    type: 'GET',
                    data: {
                        q: this.name,
                        type: this.type,
                        excludes: src.drugMap.getDrugIdList(),
                        auct: 1,
                    },
                    url: $.ajaxSetup()['url']  + '/v1/search-by-name',
                    beforeSend: function(xhr) {
                        if(src.searching != null) {
                            src.searching.abort();
                        }
                        src.prepareResultContainer();
                    },
                    success: function(data) {
                        if (data.length == 0) {
                            src.resultEmpty();
                        }
                        else {
                            src.resultPresent(data);
                        }
                    }
                });
            }
        },
        prepareResultContainer: function() {
            if ($(this.result).find('.row .waiting').length == 0) {
                $(this.result)
                    .find('.row .drug-pull-link')
                    .tooltip('dispose');
                $(this.result)
                    .find('.row > div')
                    .remove();
                $(this.result)
                    .find('.row')
                    .append(
                        $(this.result).find('.wait').children().clone(false, false)
                    );
                $(this.result)
                    .show();
            }
        },
        resultEmpty: function() {
            $(this.result)
                .find('.row > div')
                .remove();
            $(this.result)
                .find('.row')
                .append(
                    $(this.result).find('.empty').children().clone(false, false)
                );
            $(this.result)
                .show();
        },
        resultPresent: function(results) {
            $(this.result)
                .find('.row > div')
                .remove();

            var src = this;
            $.each(results, function(idx, res) {
                var template = $(src.result).find('.template').children().clone(false, false);
                $(template)
                    .addClass('drug-result-' + res.family + '-' + res.id);

                $(template)
                    .find('.drug-pull-link')
                    .attr('title', 'Pull ' + res.desc + ' to map, ' + res.name)
                    .attr('data-id', res.id)
                    .attr('data-type', res.family)
                    .tooltip();

                $(template)
                    .find('.drug-pull-link .drug-icon')
                    .addClass( __getDrugIconClass(res.family) )
                    .removeClass('drug-icon');

                $(template)
                    .find('.drug-pull-link .drug-name')
                    .html( res.name );

                $(template)
                    .find('.drug-pull-link .drug-desc')
                    .html( res.desc );

                $(template)
                    .find('.drug-view-link')
                    .attr('title', 'View Drug #' + res.id + ' details')
                    .attr('data-id', res.id)
                    .attr('data-type', res.family)
                    .tooltip();

                src.assignPullLink( $(template).find('.drug-pull-link'), res );
                src.assignViewLink( $(template).find('.drug-view-link'), res );

                $(src.result)
                    .find('.row')
                    .append(template);
            });


            $(this.result)
                .show();
        },
        clear: function() {
            $('.inp-existing-type').prop('checked', false);
            $('#existing-type-any').prop('checked', true);
            $('#inp-existing-name').val('');
            $(this.result).find('.row > div').remove();
            $(this.result).hide();
        },
        assignPullLink: function(ele, drug) {
            var dm = this.drugMap;

            $(ele).on('click', function(e) {
                e.preventDefault();

                dm.recalculateDrugOnMap(false);
                $.ajax({
                    url: $.ajaxSetup()['url'] + '/v1/get-map-drug',
                    method: "POST",
                    data: {
                        id: drug.id,
                        type: drug.family,
                        format: 'map',
                        mapped_experimental: dm.mappedExperimental,
                        mapped_generic: dm.mappedGeneric,
                        mapped_brand: dm.mappedBrand,
                    },
                    beforeSend: function(jqXHR, settings) {
                        $(ele).tooltip('dispose');
                        $(ele).find('.drug-pull-icon').hide();
                        $(ele).find('.drug-pull-wait').show();
                    },
                    success: function(data, text, response) {
                        dm.processAddNewDrugAjaxReponse(data);
                        $(ele).parents('.res-col').animate({
                            left: '-400px',
                            opacity: '0.2',
                        }, 600, function() {
                            $(ele).parents('.res-col').remove();
                        })
                    }
                });
            });
        },
        assignViewLink: function(ele, drug) {
            $(ele)
                .drugViewOverlay({
                    btn_edit: false,
                    btn_map: false,
                    btn_close_cls: 'btn-primary',
                });
        },
    };

    $(document).ready(function() {
        if (map_data == undefined) {
            console.error('Fail to load map_data.');
            Swal.fire('Error', 'Something when wrong...', 'error');

            $('#add-new-drug, #add-existing-drug')
                .addClass('disabled')
                .prop('disabled', true);
            // drugMap.btnAutoSave
            $('#autosave-map').bootstrapToggle('disable');
        }
        else {
            var drugMap = new DrugMap(map_data);

            /**
             * MAP ACTION: AUTO/MANUAL SAVE
             */
            $(drugMap.btnSave).on('click', function() {
                drugMap.saveMap(true);
            });

            $(drugMap.btnAutoSave).on('change', function(e) {
                e.preventDefault();
                if ($(this).is(':checked')) {
                    $(this).parents('span.btn').addClass('rounded-left');
                    $(drugMap.btnSave).hide();
                    if ($(drugMap.btnSave).parent().hasClass('legend')) {
                        $(drugMap.btnSave).parent().hide();
                    }
                }
                else {
                    $(this).parents('span.btn').removeClass('rounded-left');
                    $(drugMap.btnSave).show();
                    if ($(drugMap.btnSave).parent().hasClass('legend')) {
                        $(drugMap.btnSave).parent().show();
                    }
                }
            });

            /**
             * MAP ACTION: REV-REL
             */
            $(drugMap.mapActRevRel).on('click', function(e) {
                e.preventDefault();
                if ($(this).attr('data-link-id')) {
                    drugMap.removeRelation($(this).attr('data-link-id'));
                }
            });

            $('#drug-map-action-remove-relation-cancel').on('click', function(e) {
                e.preventDefault();
                drugMap.hideActionUnlinkRel();
            });

            /**
             * NEW DRUG SEARCH MODAL
             */
            var newDrug = new DrugMapNewDrug(drugMap);
            $('#submit-new-drug').on('click', function(e) {
                e.preventDefault();
                newDrug.search();
            });

            $('#clear-new-drug').on('click', function(e) {
                e.preventDefault();
                newDrug.clear();
            });

            /**
             * EXITSTING DRUG SEARCH MODAL
             */
            var exitstingSearch = new DrugMapExitstingDrugSearch(drugMap);
            $('#submit-existing-drug').on('click', function(e) {
                e.preventDefault();
                exitstingSearch.search(true);
            });

            $('#inp-existing-name').on('keyup', function(e) {
                e.preventDefault();
                exitstingSearch.search();
            });

            $('.inp-existing-type').on('change', function(e) {
                e.preventDefault();
                if ($('#inp-existing-name').val().trim().length) {
                    exitstingSearch.search();
                }
            });

            $('#clear-existing-drug').on('click', function(e) {
                e.preventDefault();
                exitstingSearch.clear();
            });

            /**
             * SAVING
             */
            $(drugMap.btnSave).on('click', function(e) {
                e.preventDefault();
                drugMap.savingMap();
            });

            /**
             * EXPORT
             */
            $(drugMap.btnExport).on('click', function(e) {
                e.preventDefault();
                drugMap.exportCsv();
            });


            /**
             * DEV
             */
            if (
                ($('#map-viewdata').length) &&
                ($('#drug-map-data').length)
            ) {
                $('#map-viewdata').on('click', function(e) {
                    e.preventDefault();
                    $('#drug-map-data').html(JSON.stringify(drugMap, null, 2));
                });
                $('#map-viewdata').trigger('click');
            }
        }

        /**
         * MODAL
         */
        $('#drug-map-add-new-modal, #drug-map-add-existing-modal')
            .on('shown.bs.modal', function (e) {
                $('body').addClass('map-modal-shown');
            })
            .on('hidden.bs.modal', function (e) {
                $('body').removeClass('map-modal-shown');
            });

         /**
          * LEGEND
          */
        $('#toggle-legend').on('click', function(e) {
            var togg = $('#toggle-legend').data('toggle');

            // drugMap.btnAutoSave
            $('#autosave-map').bootstrapToggle('destroy');

            if ($(togg).is(':visible')) {

                var btnClones = $('#drug-map-action-btns .btn-group-legend')
                    .addClass('btn-group pull-right')
                    .removeClass('btn-group-legend m-t-35')
                    .find('.btn')
                    .clone(true, true);
                $('#drug-map-action-btns .btn-group .legend').remove();
                $('#drug-map-action-btns .btn-group').append(btnClones);

                $(togg).slideUp();
            }
            else {
                $('#drug-map-action-btns .btn-group')
                    .addClass('btn-group-legend m-t-35')
                    .removeClass('btn-group pull-right')
                    .find('.btn')
                    .wrap(
                        $('<div></div>')
                            .addClass('legend my-1')
                    )
                    .each(function(idx, ele) {
                        $(ele)
                            .parent()
                            .append(
                                $('<em></em>')
                                    .addClass('legend ml-2 mt-1 mb-2 d-block d-md-inline-block hint-text')
                                    .html( $(ele).data('help') )
                            );
                        if (!$(ele).is(':visible')) {
                            $(ele).parent().hide();
                        }
                    });
                $(togg).slideDown();
            }

            // drugMap.btnAutoSave
            $('#autosave-map').bootstrapToggle();
        });
    });
})(window.jQuery);