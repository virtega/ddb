var currentBackground = 0;
var backgrounds = [
    {
        'img': '/assets/images/login/adult-career-clipboard-1919236-lowres.jpg',
        'capt': 'Photo by <a href="https://www.pexels.com/photo/clinician-writing-medical-report-1919236">rawpixel.com, Clinician Writing Medical Report at Pexels</a>',
    },
    {
        'img': '/assets/images/login/athletes-endurance-energy-685534-lowres.jpg',
        'capt': 'Photo by <a href="https://www.pexels.com/photo/people-in-gym-685534">Victor Freitas, People in Gym at Pexels</a>',
    },
    {
        'img': '/assets/images/login/care-check-checkup-905874-lowres.jpg',
        'capt': 'Photo by <a href="https://www.pexels.com/photo/person-using-black-blood-pressure-monitor-905874">rawpixel.com, Person using BP Monitor at Pexels</a>',
    },
];
function changeBackground() {
    $('.login-wrapper .bg-pic').fadeOut(1500, function() {
        $('.login-wrapper .bg-pic').css({
            'background-image' : "url('" + backgrounds[currentBackground].img + "')"
        });
        $('.login-wrapper .bg-pic-caption').html(
            $('<span></span>')
                .html(backgrounds[currentBackground].capt)
                .find('a')
                .addClass('text-' + ($('.login-wrapper').hasClass('status-error') ? 'danger' : 'success'))
                .attr('target', '_blank')
                .parents()
        );
        $('.login-wrapper .bg-pic').fadeIn(1500);
    });
    currentBackground++;
    if(currentBackground >= backgrounds.length) {
        currentBackground = 0;
    }
}
// changeBackground();
// $(document).ready(function() {
//     var bgChange = setInterval(changeBackground, 10000);
// });