module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-assets-versioning');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-file-exists');

    var target_env = grunt.option('env') || 'prod';

    var uglify_options = {
        mangle: true,
        beautify: false,
        sourceMap: false
    };

    var cssmin_options = {
        shorthandCompacting: false,
        roundingPrecision: -1,
        keepSpecialComments: 0,
        level: { 1: { specialComments: 0 } },
        sourceMap: false
    };

    if (target_env == 'dev') {
        // change "mangle" & "beautify" for unminified version
        uglify_options.sourceMap = {
            root: 'public/assets/js/',
            includeSources: true
        };

        uglify_options.sourceMapName = (function(name) {
            return name + '.map';
        })

        cssmin_options.sourceMap = true;
    }

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            vendor: [
                'public/assets/vendor',
            ],
            base: [
                'public/assets/base',
            ],
            build: [
                'public/assets/build',
            ],
            resources: [
                'public/assets/fonts',
                'public/assets/images',
            ],
            // remove previous location, @todo to be remove once v1.2 deployed
            previous: [
                'public/build',
                'public/favicon.ico',
            ],
        },
        copy: {
            vendor: {
                files: [
                    // bootstrap
                    {
                        expand: true,
                        cwd: 'node_modules/bootstrap/dist/',
                        src: [
                            'css/bootstrap.min.css',
                            'js/bootstrap.bundle.min.js'
                        ],
                        dest: 'public/assets/vendor/bootstrap/'
                    },
                    // bootstrap4-toggle
                    {
                        expand: true,
                        cwd: 'node_modules/bootstrap4-toggle/',
                        src: [
                            // @ v3.6.0-beta overriden
                            //  assets/css/pages/991.plugins-bootstrap4-toggle.css
                            //'css/bootstrap4-toggle.min.css',
                            'js/bootstrap4-toggle.min.js'
                        ],
                        dest: 'public/assets/vendor/bootstrap4-toggle/'
                    },
                    // clipboard
                    {
                        src: 'node_modules/clipboard/dist/clipboard.min.js',
                        dest: 'public/assets/vendor/clipboard/clipboard.min.js'
                    },
                    // datatable+bootstrap
                    {
                        src: 'node_modules/datatables.net/js/jquery.dataTables.min.js',
                        dest: 'public/assets/vendor/datatables/jquery.dataTables.min.js'
                    },
                    {
                        src: 'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
                        dest: 'public/assets/vendor/datatables/dataTables.bootstrap4.min.js'
                    },
                    {
                        src: 'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
                        dest: 'public/assets/vendor/datatables/dataTables.bootstrap4.min.css'
                    },
                    // desandro-classie
                    {
                        src: 'node_modules/desandro-classie/classie.js',
                        dest: 'public/assets/vendor/desandro-classie/classie.js'
                    },
                    // font-awesome v5
                    {
                        expand: true,
                        cwd: 'node_modules/@fortawesome/fontawesome-free/',
                        src: [
                            'css/all.min.css',
                            'js/all.min.js',
                            'svgs/*',
                            'webfonts/*',
                        ],
                        dest: 'public/assets/vendor/font-awesome/'
                    },
                    // moment
                    {
                        src: 'node_modules/moment/min/moment.min.js',
                        dest: 'public/assets/vendor/moment/moment.min.js'
                    },
                    // pace-js
                    {
                        src: 'node_modules/pace-js/pace.min.js',
                        dest: 'public/assets/vendor/pace-js/pace.min.js'
                    },
                    // jquery
                    {
                        src: 'node_modules/jquery/dist/jquery.min.js',
                        dest: 'public/assets/vendor/jquery/jquery.min.js'
                    },
                    // jquery.actual
                    {
                        src: 'node_modules/jquery.actual/jquery.actual.min.js',
                        dest: 'public/assets/vendor/jquery.actual/jquery.actual.min.js'
                    },
                    // jquery.easing
                    {
                        src: 'node_modules/jquery.easing/jquery.easing.min.js',
                        dest: 'public/assets/vendor/jquery.easing/jquery.easing.min.js'
                    },
                    // jquery.flowchart
                    {
                        expand: true,
                        cwd: 'node_modules/jquery.flowchart/',
                        src: [
                            'jquery.flowchart.min.css',
                            'jquery.flowchart.min.js'
                        ],
                        dest: 'public/assets/vendor/jquery.flowchart/'
                    },
                    // jquery.ioslist
                    {
                        expand: true,
                        cwd: 'node_modules/jquery.ioslist/dist/',
                        src: [
                            'css/jquery.ioslist.css',
                            'js/jquery.ioslist.min.js'
                        ],
                        dest: 'public/assets/vendor/jquery.ioslist/'
                    },
                    // jquery.repeater
                    {
                        src: 'node_modules/jquery.repeater/jquery.repeater.min.js',
                        dest: 'public/assets/vendor/jquery.repeater/jquery.repeater.min.js'
                    },
                    // jquery.scrollbar
                    {
                        expand: true,
                        cwd: 'node_modules/jquery.scrollbar/',
                        src: [
                            'jquery.scrollbar.css',
                            'jquery.scrollbar.min.js'
                        ],
                        dest: 'public/assets/vendor/jquery.scrollbar/'
                    },
                    // jquery-ui-dist
                    {
                        src: 'node_modules/jquery-ui-dist/jquery-ui.min.js',
                        dest: 'public/assets/vendor/jquery-ui-dist/jquery-ui.min.js'
                    },
                    // jquery-unveil
                    {
                        src: 'node_modules/jquery-unveil/jquery.unveil.js',
                        dest: 'public/assets/vendor/jquery-unveil/jquery.unveil.js'
                    },
                    // jquery-validation
                    {
                        src: 'node_modules/jquery-validation/dist/jquery.validate.min.js',
                        dest: 'public/assets/vendor/jquery-validation/jquery.validate.min.js'
                    },
                    // js-cookie
                    {
                        src: 'node_modules/js-cookie/src/js.cookie.js',
                        dest: 'public/assets/vendor/js-cookie/js.cookie.js'
                    },
                    // popper.js
                    {
                        src: 'node_modules/popper.js/dist/umd/popper.min.js',
                        dest: 'public/assets/vendor/popper.js/popper.min.js'
                    },
                    // select2
                    {
                        expand: true,
                        cwd: 'node_modules/select2/dist/',
                        src: [
                            'css/select2.min.css',
                            'js/select2.full.min.js'
                        ],
                        dest: 'public/assets/vendor/select2/'
                    },
                    // simplebar
                    {
                        expand: true,
                        cwd: 'node_modules/simplebar/dist/',
                        src: [
                            'simplebar.min.css',
                            'simplebar.min.js'
                        ],
                        dest: 'public/assets/vendor/simplebar/'
                    },
                    // sweetalert2
                    {
                        expand: true,
                        cwd: 'node_modules/sweetalert2/dist/',
                        src: [
                            'sweetalert2.min.css',
                            'sweetalert2.min.js',
                        ],
                        dest: 'public/assets/vendor/sweetalert2/'
                    },
                    // tempusdominus-bootstrap-4
                    {
                        expand: true,
                        cwd: 'node_modules/tempusdominus-bootstrap-4/build/',
                        src: [
                            'css/tempusdominus-bootstrap-4.min.css',
                            'js/tempusdominus-bootstrap-4.min.js'
                        ],
                        dest: 'public/assets/vendor/tempusdominus-bootstrap-4/'
                    },
                ]
            },
            resources: {
                files: [
                    {
                        src: 'assets/images/favicon/favicon.ico',
                        dest: 'public/favicon.ico'
                    },
                    // see README https://github.com/JulietaUla/Montserrat
                    {
                        expand: true,
                        cwd: 'assets/fonts/montserrat/fonts/webfonts/',
                        src: [
                            'Montserrat-Light*.*',
                            'Montserrat-Regular*.*',
                            'Montserrat-Italic*.*',
                            'Montserrat-Medium*.*',
                            'Montserrat-SemiBold*.*',
                        ],
                        dest: 'public/assets/fonts/montserrat/'
                    },
                    {
                        expand: true,
                        cwd: 'assets/images/',
                        src: ['**'],
                        dest: 'public/assets/images/'
                    },
                ]
            }
        },
        uglify: {
            options: uglify_options,
            base: {
                files: {
                    'public/assets/base/template-pages.min.js': [
                        // localized plugin
                        'assets/js/pages/901.modernizr.custom.min.js',
                        // template + custom plugin
                        'assets/js/pages/000.imports.js',
                        'assets/js/pages/001.core.js',
                        'assets/js/pages/002.selectfx.js',
                        'assets/js/pages/004.circular-progress.js',
                        'assets/js/pages/005.notifications.js',
                        'assets/js/pages/006.cards.js',
                        'assets/js/pages/007.mobile-view.js',
                        'assets/js/pages/009.sidebar.js',
                        'assets/js/pages/010.search-overlay.js',
                        'assets/js/pages/011.search-overlay-processor.js',
                        'assets/js/pages/012.delta-refresh.js',
                        'assets/js/pages/013.toast.js',
                        'assets/js/pages/014.drug-overlay.js',
                        'assets/js/pages/015.theme-switcher.js',
                        // setup and init
                        'assets/js/pages/100.init.js',
                        'assets/js/pages/101.setup.js',
                    ],
                    'public/assets/base/template-pages-post.min.js': [
                        'assets/js/pages/901.tempusdominusbootstrap4-override.js',
                    ]
                }
            },
            build: {
                files: [
                    {
                        expand: true,
                        cwd: 'assets/js/helpers/',
                        src: '*.js',
                        dest: 'public/assets/build/helpers/',
                        ext: '.min.js'
                    },
                    {
                        expand: true,
                        cwd: 'assets/js/',
                        src: ['layout-*.js', 'page-*.js'],
                        dest: 'public/assets/build/',
                        ext: '.min.js'
                    }
                ]
            }
        },
        cssmin: {
            options: cssmin_options,
            base: {
                files: {
                    'public/assets/base/template-pages.min.css': [
                        // localized plugin
                        // import script
                        'assets/css/pages/000.imports.css',
                        // template + custom
                        'assets/css/pages/001.core.css',
                        'assets/css/pages/005.typography.css',
                        'assets/css/pages/006.buttons.css',
                        'assets/css/pages/007.alerts.css',
                        'assets/css/pages/008.notifications.css',
                        'assets/css/pages/009.progressbar.css',
                        'assets/css/pages/011.modals.css',
                        'assets/css/pages/016.cards.css',
                        'assets/css/pages/017.form-elements.css',
                        'assets/css/pages/018.tables-standard.css',
                        'assets/css/pages/018.tables-datatable.css',
                        'assets/css/pages/020.lists.css',
                        'assets/css/pages/021.socials.css',
                        'assets/css/pages/024.app-login-screen.css',
                        'assets/css/pages/098.helpers.css',
                        'assets/css/pages/099.print.css',
                        'assets/css/pages/102.pace-theme-flash.css',
                        'assets/css/pages/200.ie-warning-overlays.css',
                        // theme
                        'assets/css/pages/500.dark-theme.css',
                    ],
                    'public/assets/base/template-pages-post.min.css': [
                        'assets/css/pages/990.plugins-override.css',
                        'assets/css/pages/991.plugins-bootstrap4-toggle.css',
                    ]
                }
            },
            build: {
                files: [{
                    expand: true,
                    cwd: 'assets/css/',
                    src: ['layout-*.css', 'page-*.css'],
                    dest: 'public/assets/build/',
                    ext: '.min.css'
                }]
            }
        },
        assets_versioning: {
            options: {
                tag: 'hash',
                hashLength: 7,
                versionsMapTemplate: 'scripts/001-grunt-mock-webpack/manifest-json-template.lodash',
                versionsMapTrimPath: 'public/',
                versionsMapFile: 'public/manifest.json'
            },
            src: {
                options: {
                    post: true,
                    tasks: ['cssmin:base', 'uglify:base', 'cssmin:build', 'uglify:build']
                }
            }
        },
        watch: {
            base: {
                files: [
                    'assets/css/pages/*.css',
                    'assets/js/pages/*.js',
                ],
                tasks: [
                    'clean:base',
                    'cssmin:base',
                    'uglify:base',
                    'assets_versioning'
                ],
                options: {
                    interrupt: true
                }
            },
            build: {
                files: [
                    'assets/css/*.css',
                    'assets/js/*.js',
                ],
                tasks: [
                    'clean:build',
                    'cssmin:build',
                    'uglify:build',
                    'assets_versioning'
                ],
                options: {
                    interrupt: true
                }
            }
        }
    });

    grunt.registerTask('all', [
        'clean',
        'copy',
        'cssmin',
        'uglify',
        'assets_versioning'
    ]);

    grunt.registerTask('template', [
        'clean:build',
        'cssmin:build',
        'uglify:build',
        'assets_versioning'
    ]);

    grunt.registerTask('production', [
        'clean:previous',
        'clean:vendor',
        'clean:resources',
        'copy:vendor',
        'copy:resources',
    ]);
};