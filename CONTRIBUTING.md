# Contributing

## Dev features
* `env`.`APP_DEV_NICE_EXCEPTION` [boolean]
    * if true, during error on dev, instead Symfony shows error page (with Xdebug if any), it will invoke Prod/Stage view - without changing `env` value.
* UI Tokens Debugging
    * on `dev`, you may view all processed twig tokens by append the URI with `(?|&)debug=tokens`.
* Map Dump
    * Map using special `operator` (drug), and `link` (relation) structure.
    * Drug Map under dev environment, it allows user to dump entire MAP module data to UI for quick review.
    
## Unit Testing
```sh
# Initialize, Install and run all PHPUnit additional libraries & recipe
$ php bin/phpunit --version

# Create test env settings; note that if certain attribute were not set, i.e. Lotus/MsSQL (remote) the test will be skipped.
$ cp .env .env.test
$ nano .env.test

# make sure to remove cache-env (if any)
$ rm .env.local.php

# activate phpunit config, and update `LDAP_LOGIN_*` config only
$ cp phpunit.xml.dist phpunit.xml
$ nano phpunit.xml

# Check env settings
$ php bin/console about --env=test

# [OPT] Functional Test will create, drop and mount fixture to Db, check if Db User capable to do so;
$ php bin/console doctrine:database:create --env=test
$ php bin/console doctrine:migrations:migrate -n --env=test
$ php bin/console doctrine:database:drop --force --env=test

# Check Migration status.
$ php bin/console doctrine:migrations:status --env=test

# Run test - do check phpunit.xml for specific suites
$ php bin/phpunit
```

## Code Standard
Ensure to include `--dev` option during composer install.
Note that .phpneon has preset to max level.
```sh
$ vendor/bin/phpstan analyse
```

## Login without Official LDAP, or Mocking LDAP Authentication
Without LDAP you never be able to login to the site. Developer physical adjustment is needed;

###### Files
* `src/Security/LdapAuthenticator.php` for Accessing FE site.
* `src/Security/RestLdapAuthenticator.php` for Accessing API endpoint

###### Changes
```php
?? public function getUser(...) {
??     ...  
??     try {
 +         return new \App\Security\User\LdapUser(
 +             'john.doe',                  // username
 +             'dummy.johndoe!pa$$word',    // password (dummy)
 +             [ 
 +                 'ROLE_ADMIN',            // roles - all; super admin
 +                 'ROLE_EDITOR',
 +                 'ROLE_USER',
 +                 'ROLE_VISITOR',
 +             ],
 +             new \Symfony\Component\Ldap\Entry(
 +                 'DC=pslgroup,DC=com',    // dummy DN
 +                 [
 +                     'sAMAccountName' => ['John.Dow'],          // Immitate LDAP query result here;
 +                     'name'           => ['John Doe'],          // Notice value in Array
 +                     'givenName'      => ['John Doe'],
 +                     'mail'           => ['john.doe@email.com'],
 +                     'title'          => ['Example User'],
 +                 ]
 +             )
 +         );
??        $userProvider->bindingLdapForQuery( ...
??        return $userProvider->loadUserByUsername( ...
??     } 
??     catch (\Exception $e) {
??         throw new AuthenticationException( ...    
??     }
??     ...
?? }
```

## Template License
* [#9694847] Pages - Admin Dashboard Template with Angular 6, Bootstrap 4 & HTML by Revox Pvt Ltd (ace)
* Envato Market, themeforest.net
* Regular License, 6 months support, expires on 31 Dec 2019
* Purchase code: e16541ef-c66c-4a77-8af7-fb384f9b6681
* Billed to C********, ODR: 107876608, INV: IVBF32809383, IVHF18383635, IVIP33004196