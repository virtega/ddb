FROM php:7.4-apache
#CMD mkdir /usr/src/drugdb
COPY . /var/www/html/

WORKDIR /var/www/html/

RUN apt-get update && apt-get install -y \
    git \
    nodejs \
    npm \
    curl \
    wget \
    vim \
    unzip 


RUN npm install -g grunt-cli 
RUN npm install --global yarn


# CMD apt install curl php-cli php-mbstring git unzip -y

RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN wget https://get.symfony.com/cli/installer -O - | bash

#RUN composer update
#RUN composer install

RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony





ARG APP_ENV
ARG MAILER_URL
ARG MAILER_FROM
ARG DATABASE_URL
ARG LDAP_HOST
ARG LDAP_PORT
ARG LDAP_ENCRYPT
ARG LDAP_BASE_DN
ARG LDAP_EXTRA_QUERY
ARG APP_SECRET
ARG SECURITY_API_TOKEN

ARG LOTUS_DB_DSN
ARG LOTUS_DB_USER
ARG LOTUS_DB_PASSWORD
ARG LOTUS_DB_UPSYNC

ARG CONFERENCE_DB_DSN
ARG CONFERENCE_DB_USER
ARG CONFERENCE_DB_PASSWORD
ARG JOURNAL_DB_DSN
ARG JOURNAL_DB_USER
ARG JOURNAL_DB_PASSWORD

ARG ES_HOSTS
ARG ES_INDEX

ARG AWS_KEY
ARG AWS_SECRET
ARG AWS_REGION

ARG AWS_REGION



CMD [ "php", "./public/index.php" ]






#php --ini
#    8  curl -sS https://get.symfony.com/cli/installer | bash
#   11  cp /root/.symfony/bin/symfony /usr/local/bin/

