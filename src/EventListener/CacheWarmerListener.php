<?php

namespace App\EventListener;

use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Process\Process;

/**
 * Class execute during clearing cache process
 *
 * @codeCoverageIgnore
 *
 * @since 1.2.0
 */
class CacheWarmerListener implements CacheWarmerInterface
{
    /**
     * @var Kernel
     */
    private $kernel;
    /**
     * @param Kernel $kernel
     */
    public function setKernel(Kernel $kernel): void
    {
        $this->kernel = $kernel;
    }

    /**
     * {@inheritdoc}
     */
    public function warmUp($cacheDir)
    {
        $output = new ConsoleOutput();

        foreach ($this->getCommands() as $command => $caption) {
            $output->writeln('');
            $output->writeln("<info>{$caption}</info>");

            // append
            $command .= ' --env=' . $this->kernel->getEnvironment();
            $success = $this->executeCommand($command, $output);

            if (!$success) {
                $output->writeln(sprintf('<info>An error occurs when running: %s</info>', $command));
                exit(1);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isOptional()
    {
        return true;
    }

    /**
     * @param string        $command
     * @param ConsoleOutput $output
     *
     * @return bool
     */
    private function executeCommand(string $command, ConsoleOutput $output): bool
    {
        $p = Process::fromShellCommandline($command);
        $p->setTimeout(null);
        $p->run(
            function ($type, $data) use ($output) {
                // clean up
                $done = preg_replace('/Command end\.\s?/i', '', $data);
                if (null === $done) {
                    $done = $data;
                }

                // output
                if (Process::OUT == $type) {
                    $output->write((string) $done, false, OutputInterface::OUTPUT_RAW);
                }
                else {
                    $output->write('.', false, OutputInterface::OUTPUT_RAW);
                }
            }
        );

        return $p->isSuccessful();
    }

    /**
     * @return array
     */
    private function getCommands(): array
    {
        $commands = [
            \App\Command\WidgetUpdatesCommand::getDefaultName() => 'Rebuild Dashboard Widgets cache',
        ];

        $console = 'php ' . $this->kernel->getProjectDir() . '/bin/console';

        $comms = [];
        foreach ($commands as $comm => $capt) {
            $comms["{$console} {$comm}"] = $capt;
        }

        return $comms;
    }
}