<?php

namespace App\EventListener;

use App\Entity\UserRoles;
use App\Exception\MaintenanceException;
use App\Security\User\LdapUser;
use App\Utility\Traits\FlashBagTrait;
use App\Utility\Traits\RouteCheckTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class handling Maintenance Flag to Stop all request and API.
 */
class MaintenanceListener
{
    use FlashBagTrait;
    use RouteCheckTrait;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var boolean
     */
    private $isLocked = false;

    /**
     * @param string                 $lockFilePath  Maintenance file file name on Root
     * @param RouterInterface        $router
     * @param TokenStorageInterface  $tokenStorage
     * @param string                 $appEnv
     */
    public function __construct(string $lockFilePath, RouterInterface $router, TokenStorageInterface $tokenStorage, string $appEnv)
    {
        $this->isLocked     = file_exists($lockFilePath);
        $this->router       = $router;
        $this->tokenStorage = $tokenStorage;

        FlashBagTrait::setEnv($appEnv);
    }

    /**
     * Tagged to response to every kernel request.
     * Event should hold information about where the request is going to.
     * This include if expected and error
     *
     * @param GetResponseEvent  $event
     *
     * @throws MaintenanceException  During maintenance, on API Request
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        /**
         * @var \Symfony\Component\HttpFoundation\Request
         */
        $request = $event->getRequest();

        // no lock
        if (!$this->isLocked) {
            // its ok now.. transferring
            if ($request->attributes->get('_route') == 'maintenance') {
                $url = $this->router->generate('login');
                $response = new RedirectResponse($url);
                $event->setResponse($response);
                $event->stopPropagation();
            }
            return;
        }

        // on the page
        if ($request->attributes->get('_route') == 'maintenance') {
            return;
        }

        // admin knocking on the login page
        if (
            ($request->attributes->get('_route') == 'login') &&
            ($request->query->has('admin'))
        ) {
            return;
        }

        // for admin only
        if ($token = $this->tokenStorage->getToken()) {
            $user = $token->getUser();
            if (
                (is_object($user)) &&
                ($user instanceof LdapUser) &&
                (in_array(UserRoles::ADMIN, $user->getRoles()))
            ) {
                return;
            }
        }

        // for API Request
        if (RouteCheckTrait::validateRequestOnApi($request)) {
            throw new MaintenanceException();
        }

        // force log out
        if ($request->hasSession()) {
            if ($session = $request->getSession()) {
                $this->tokenStorage->setToken(null);
                $session->invalidate();

                FlashBagTrait::setOffFlashMsgByRequest($request, 'info', 'You have been logged out.');
            }
        }

        // redirect all to maintenance page
        $url = $this->router->generate('maintenance');
        $response = new RedirectResponse($url);
        $event->setResponse($response);
        $event->stopPropagation();
    }
}
