<?php

namespace App\EventListener;

use App\Exception\ApiDataCrudException;
use App\Exception\ApiIncompleteException;
use App\Exception\DrugCrudException;
use App\Exception\LotusConnectionException;
use App\Exception\LotusSyncException;
use App\Exception\MaintenanceException;
use App\Exception\TrirdPartyException;
use App\Security\LdapAuthenticator;
use App\Service\CoreService;
use App\Utility\Traits\FlashBagTrait;
use App\Utility\Traits\RouteCheckTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Twig\Environment as Twig;

/**
 * Class handling Exception for 404 / Unknown PAGE & API Request.
 *
 * @since 1.0.0
 */
class ExceptionListener
{
    use RouteCheckTrait;

    /**
     * @var Twig
     */
    protected $twig;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var LdapAuthenticator
     */
    protected $ldapAuth;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * env(string:APP_ENV)
     *
     * @var string
     */
    private $appEnv;

    /**
     * env(string:APP_DEV_NICE_EXCEPTION)
     *
     * @var boolean
     */
    private $appDevNiceExp;

    /**
     * @param Twig                   $twig
     * @param TokenStorageInterface  $tokenStorage
     * @param LdapAuthenticator      $ldapAuth
     * @param RouterInterface        $router
     * @param string                 $appEnv
     * @param boolean                $appDevNiceExp
     */
    public function __construct(Twig $twig, TokenStorageInterface $tokenStorage, LdapAuthenticator $ldapAuth, RouterInterface $router, string $appEnv, bool $appDevNiceExp)
    {
        $this->twig          = $twig;
        $this->tokenStorage  = $tokenStorage;
        $this->ldapAuth      = $ldapAuth;
        $this->router        = $router;
        $this->appEnv        = $appEnv;
        $this->appDevNiceExp = $appDevNiceExp;

        FlashBagTrait::setEnv($this->appEnv);
    }

    /**
     * Event Handler during Exception Handling.
     * Method separate for API request for proper output process.
     *
     * @param GetResponseForExceptionEvent  $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (!RouteCheckTrait::validateRequestOnApi($event->getRequest())) {
            $this->processForUI($event);
            return;
        }

        // API controller request
        $exception = $event->getException();

        $date    = new \DateTime();
        $payload = array(
            'msg'     => $exception->getMessage(),
            'data'    => array(),
            'count'   => 0,
            'success' => false,
            'drugdb'  => array(
                'version' => CoreService::VERSION,
                'date'    => $date->format('c'),
            ),
        );

        // extracting the right status code
        $response = new JsonResponse();
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
        }
        elseif (!empty($exception->getCode())) {
            $response->setStatusCode($exception->getCode());
        }

        // extracting / choose how to process output
        if ($exception instanceof HttpException) {
            $payload['msg'] = 'Unknown API Request';
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
        }
        elseif (
            ($exception instanceof LotusSyncException) ||
            ($exception instanceof DrugCrudException) ||
            ($exception instanceof ApiDataCrudException) ||
            ($exception instanceof TrirdPartyException)
        ) {
            // @codeCoverageIgnoreStart
            $response->setStatusCode($exception->getCode());
            // @codeCoverageIgnoreEnd
        }
        elseif ($exception instanceof ApiIncompleteException) {
            $arg = $exception->getApiArgument();
            $payload['msg'] .= ": '{$arg}'.";
        }
        elseif (
            ($exception instanceof AccessDeniedHttpException) ||
            ($exception instanceof AccessDeniedException) ||
            ($exception instanceof AuthenticationException)
        ) {
            $payload['msg'] = 'Permission Denied: ' . $exception->getMessage();
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
        }
        elseif ($exception instanceof MaintenanceException) {
            $response->headers->set('Retry-After', MaintenanceException::RETRY_AFTER);
        }
        else {
            // This is to catch Syntax / Fatal error, not able to test
            // @codeCoverageIgnoreStart
            $payload['error'] = array(
                'class' => get_class($exception),
                'msg'   => (method_exists($exception, 'getMessage') ? $exception->getMessage() : false),
                'file'  => (method_exists($exception, 'getFile') ? $exception->getFile() : false),
                'line'  => (method_exists($exception, 'getLine') ? $exception->getLine() : false),
                'code'  => (method_exists($exception, 'getCode') ? $exception->getCode() : false),
                'trace' => (method_exists($exception, 'getTrace') ? $exception->getTrace() : false),
                'str'   => (string) $exception,
            );

            // tracing can be expensive, and may result JSON to failed - Recursive
            if (!empty($payload['error']['trace'])) {
                $payload['error']['trace'] = json_encode($payload['error']['trace'], JSON_PARTIAL_OUTPUT_ON_ERROR);
            }

            if (!in_array($this->appEnv, ['dev','staging'])) {
                $payload['msg']   = 'Error Occur: ' . md5($payload['msg']);
                $payload['error'] = (string) json_encode($payload['error']);
                $payload['error'] = base64_encode($payload['error']);
                $payload['error'] = str_split($payload['error'], 128);
            }

            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            // @codeCoverageIgnoreEnd
        }

        $response->setData($payload);

        $event->setResponse($response);
    }

    /**
     * Method to process UI Pages Exception
     * We not able to get TokenStorage populated for 404 error; So we depending on cookie as secondary.
     * Ultimately this will be use to identify to which template to be use.
     *
     * @param GetResponseForExceptionEvent  $event
     */
    private function processForUI(GetResponseForExceptionEvent $event)
    {
        if (
            ('dev' != $this->appEnv) ||
            ($this->appDevNiceExp)
        ) {
            $user   = null;
            $logged = false;

            $request = $event->getRequest();

            // check if user login still active
            // need to use cookie, ie in 404 error; did not initialize TokenStorage.
            if ($token = $this->tokenStorage->getToken()) {
                $user = $token->getUser();
                if (
                    (
                        (is_object($user)) &&
                        // @codeCoverageIgnoreStart
                        ($user instanceof AnonymousToken)
                        // @codeCoverageIgnoreEnd
                    ) ||
                    ('anon.' == $user)
                ) {
                    $user = null;
                }
            }
            // @codeCoverageIgnoreStart
            elseif (!empty($request->cookies)) {
                $logged = (!empty($request->cookies->get('PHPSESSID')));
            }
            // @codeCoverageIgnoreEnd

            $exception = $event->getException();
            // generic HTTP Exception, redirect them to dashboard
            if ($exception instanceof HttpException) {
                // expected
                if (
                    (strripos($request->getUri(), '.js.map') !== false) ||
                    (strripos($request->getUri(), '.css.map') !== false)
                ) {
                    // @codeCoverageIgnoreStart
                    $response = new Response();
                    $response->setContent($exception->getStatusCode());
                    $response->setStatusCode($exception->getStatusCode());
                    $event->setResponse($response);
                    return;
                    // @codeCoverageIgnoreEnd
                }
                FlashBagTrait::setOffFlashMsgByRequest($request, 'danger', $exception->getMessage());
                $response = $this->router->generate('dashboard');
                // masking error as 302
                $response = new RedirectResponse($response, Response::HTTP_FOUND);
                $event->setResponse($response);
                return;
            }

            // if user login has expired - ie during meta/delta refresh reload the page
            if (
                ((empty($user)) && (empty($logged))) &&
                (
                    ($exception instanceof AccessDeniedHttpException) ||
                    ($exception instanceof AccessDeniedException) ||
                    ($exception instanceof AuthenticationException)
                )
            ) {
                $response = $this->ldapAuth->start($request, null);
                $event->setResponse($response);
                return;
            }

            // known - push back to the original page
            if (
                ($exception instanceof LotusSyncException) ||
                ($exception instanceof LotusConnectionException) ||
                ($exception instanceof DrugCrudException)
            ) {
                // @codeCoverageIgnoreStart
                FlashBagTrait::setOffFlashMsgByRequest($request, 'danger', $exception->getMessage());
                $response = $this->router->generate(
                   $request->attributes->get('_route'),
                   $request->attributes->get('_route_params')
                );
                // masking error as 302
                $response = new RedirectResponse($response, Response::HTTP_FOUND);
                $event->setResponse($response);
                return;
                // @codeCoverageIgnoreEnd
            }

            // have/no user, on different error
            $code = $exception->getCode();
            if (method_exists($exception, 'getStatusCode')) {
                // @codeCoverageIgnoreStart
                $code = $exception->getStatusCode();
                // @codeCoverageIgnoreEnd
            }

            $content = array(
                'user'        => $user,
                'loggedin'    => ($logged ? $logged : (!empty($user))),
                'status_code' => $code,
                'status_text' => (Response::$statusTexts[$code] ?? 'Unknown'),
                'exception'   => $exception,
            );
            $content = $this->twig->render('bundles/TwigBundle/Exception/error.html.twig', $content);

            $response = new Response();
            $response->setContent($content);
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

            if (
                ($exception instanceof AccessDeniedHttpException) ||
                ($exception instanceof AccessDeniedException) ||
                ($exception instanceof AuthenticationException)
            ) {
                $response->setStatusCode($code);
            }

            if (method_exists($exception, 'getHeaders')) {
                // @codeCoverageIgnoreStart
                $response->headers->replace($exception->getHeaders());
                // @codeCoverageIgnoreEnd
            }

            $event->setResponse($response);
        }
    }
}
