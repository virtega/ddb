<?php

namespace App\Service;

use App\Entity\Conference;
use App\Exception\RemoteSyncException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Service: Conference Core Service.
 * Process Basic CRUD for Conference entity.
 *
 * @since  1.2.0
 */
class ConferenceCoreService
{
    use \App\Service\Traits\EntityFormUpackServiceTrait;

    /**
     * @var ConferenceSourceService
     */
    protected $conferenceRemote;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Security
     */
    private $symfonySecurity;

    /**
     * @param ConferenceSourceService  $conferenceRemote
     * @param EntityManagerInterface   $em
     * @param LoggerInterface          $logger
     * @param Security                 $symfonySecurity
     */
    public function __construct(ConferenceSourceService $conferenceRemote, EntityManagerInterface $em, LoggerInterface $logger, Security $symfonySecurity)
    {
        $this->conferenceRemote = $conferenceRemote;
        $this->em               = $em;
        $this->logger           = $logger;
        $this->symfonySecurity  = $symfonySecurity;
    }

    /**
     * Method to get Form defaults in set or single per field name.
     *
     * @param string|null  $fieldName
     *
     * @throws \Exception  If key is unknown.
     *
     * @return mixed
     */
    public static function getFormDefaults(string $fieldName = null)
    {
        $fields = [
            'name'         =>  ''  ,
            'startdate'    =>  ''  ,
            'enddate'      =>  ''  ,
            'city'         =>  ''  ,
            'cityguide'    =>  ''  ,
            'state'        =>  ''  ,
            'country'      =>  ''  ,
            'region'       =>  0   ,
            'oncruise'     =>  'no',
            'online'       =>  'no',
            'keycongress'  =>  'no',
            'discontinued' =>  'no',
            'specialty'    =>  []  ,
            'contactnote'  =>  ''  ,
            'contactphone' =>  []  ,
            'contactemail' =>  []  ,
            'contactfax'   =>  []  ,
            'hasguide'     =>  'no',
            'links'        =>  []  ,
            'uniquename'   =>  ''  ,
        ];

        if (null === $fieldName) {
            return $fields;
        }

        if (!isset($fields[$fieldName])) {
            throw new \Exception("Unknown field name: '{$fieldName}'.");
        }

        return $fields[$fieldName];
    }

    /**
     * Method to create new Conference from Form.
     *
     * @param array $fields
     *
     * @throws \Exception  If ID is still null after EM flush.
     *
     * @return Conference
     */
    public function create(array $fields): Conference
    {
        $conference = new Conference;
        $this->entityFormSetter($conference, $fields);

        $conference->setDetailsMetaCreatedOn(new \DateTime('now'));
        $conference->setDetailsMetaCreatedBy( $this->__getCurrentUserUsername() );

        $this->conferenceRemote->upsync($conference);

        $this->em->persist($conference);
        $this->em->flush();

        if (empty($conference->getId())) {
            throw new \Exception('Conference entity failed to return new ID, please check with Administrator.');
        }

        $this->logger->info('Conference Created', [
            'id'   => $conference->getId(),
            'user' => $this->__getCurrentUserUsername(),
        ]);

        return $conference;
    }

    /**
     * Method to update Conference from Form.
     *
     * @param Conference $conference
     * @param array      $fields
     *
     * @return Conference
     */
    public function update(Conference $conference, array $fields): Conference
    {
        $this->entityFormSetter($conference, $fields);

        $conference->setDetailsMetaModifiedOn(new \DateTime('now'));
        $conference->setDetailsMetaModifiedBy( $this->__getCurrentUserUsername() );

        $this->conferenceRemote->upsync($conference);

        $this->em->persist($conference);
        $this->em->flush();

        $this->logger->info('Conference Updated', [
            'id'   => $conference->getId(),
            'user' => $this->__getCurrentUserUsername(),
        ]);

        return $conference;
    }

    /**
     * Method to remove Conference both local and remote.
     *
     * @param Conference  $conference
     */
    public function delete(Conference $conference): void
    {
        $this->conferenceRemote->remove($conference);

        foreach ($conference->getSpecialties() as $conferenceSpecialties) {
            $this->em->remove($conferenceSpecialties);
        }

        $id = $conference->getId();

        $this->em->remove($conference);
        $this->em->flush();

        $this->logger->info('Conference Deleted', [
            'id'   => $id,
            'user' => $this->__getCurrentUserUsername(),
        ]);
    }

    /**
     * Method to set Forms value to a Conference
     *
     * @param Conference  $conference  Note by reference
     * @param array       $fields
     */
    private function entityFormSetter(Conference &$conference, array $fields): void
    {
        $fields = $fields + self::getFormDefaults(null);

        $conference->setName($fields['name']);

        $value = $this->__unpackDateTimePickerField($fields['startdate'], 'Y-m-d');
        $conference->setStartDate($value);

        $value = $this->__unpackDateTimePickerField($fields['enddate'], 'Y-m-d');
        $conference->setEndDate($value);

        $value = (!empty($fields['city']) ? $fields['city'] : null);
        $conference->setCity($value);

        $value = (!empty($fields['cityguide']) ? $fields['cityguide'] : null);
        $conference->setCityGuide($value);

        $value = null;
        if (!empty($fields['country'])) {
            $country = $this->__getCountryByIsoCode($fields['country']);
            if ($country) {
                $value = $country;
            }
        }
        $conference->setCountry($value);

        $value = $this->__getCountryTaxonomyById($fields['region']);
        $conference->setRegion($value);

        $value = (!empty($fields['uniquename']) ? $fields['uniquename'] : null);
        $conference->setUniqueName($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['oncruise']);
        $conference->setCruise($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['online']);
        $conference->setOnline($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['keycongress']);
        $conference->setKeyEvent($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['hasguide']);
        $conference->setHasGuide($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['discontinued']);
        $conference->setDiscontinued($value);

        $conference->setDetailsState($fields['state']);

        $conference->setDetailsContactDesc($fields['contactnote']);
        $conference->setDetailsContactPhones($fields['contactphone']);
        $conference->setDetailsContactFaxes($fields['contactfax']);
        $conference->setDetailsContactEmails($fields['contactemail']);
        $conference->setDetailsContactLinks($fields['links']);

        $this->__unpackAndAssignConferenceSpecilties($conference, $fields['specialty']);
    }
}
