<?php
namespace App\Service\Factory;

use App\Service\ESDGService;
use Aws\Signature\SignatureV4;
use Aws\Credentials\Credentials;
use Elasticsearch\ClientBuilder;
use Psr\Log\LoggerInterface;

/**
 * Service: ESDG Service Factory
 */
class ESDGServiceFactory
{
    /**
     * @param LoggerInterface $logger
     * @param string $awsKey
     * @param string $awsSecret
     * @param string $awsRegion
     * @param string $hosts
     * @param string $ESIndex
     * @param int $ESMaxResults
     * @param int $FBXDaysSearch
     *
     * @return ESDGService
     */
    public function create(LoggerInterface $logger, string $awsKey, string $awsSecret, string $awsRegion, string $hosts, string $ESIndex, int $ESMaxResults, int $FBXDaysSearch): ESDGService
    {
        $client = null;
        if (!empty($awsKey) && !empty($awsSecret) && !empty($awsRegion)) {
            // Signed ES Client
            $credentials = new Credentials($awsKey, $awsSecret);
            $signature = new SignatureV4('es', $awsRegion);

            $middleware = new AwsSignatureMiddleware($credentials, $signature);
            $defaultHandler = ClientBuilder::defaultHandler();
            $awsHandler = $middleware($defaultHandler);

            $client = ClientBuilder::create()
                ->setHandler($awsHandler)
                ->setHosts(explode('|', $hosts))
                ->build();
        } else {
            // Default ES Client with no AWS Auth
            $client = ClientBuilder::create()
                ->setHosts(explode('|', $hosts))
                ->build();
        }

        return new ESDGService($logger, $client, $ESIndex, $ESMaxResults, $FBXDaysSearch);
    }
}
