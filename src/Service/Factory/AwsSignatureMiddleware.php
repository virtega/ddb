<?php

namespace App\Service\Factory;

use Aws\Credentials\CredentialsInterface;
use Aws\Signature\SignatureInterface;
use GuzzleHttp\Psr7\Request;

class AwsSignatureMiddleware
{
    /**
     * @var \Aws\Credentials\CredentialsInterface
     */
    protected $credentials;

    /**
     * @var \Aws\Signature\SignatureInterface
     */
    protected $signature;

    /**
     * AwsSignatureMiddleware constructor.
     * @param CredentialsInterface $credentials
     * @param SignatureInterface $signature
     */
    public function __construct(CredentialsInterface $credentials, SignatureInterface $signature)
    {
        $this->credentials = $credentials;
        $this->signature = $signature;
    }

    /**
     * @param \Closure|callable  $handler
     *
     * @return callable
     */
    public function __invoke(callable $handler)
    {
        return function ($request)  use ($handler) {
            $headers = $request['headers'];

            $method = isset($request['client']['curl'][CURLOPT_CUSTOMREQUEST]) ? $request['client']['curl'][CURLOPT_CUSTOMREQUEST] : 'POST';

            $psrRequest = new Request($method, $request['uri'], $headers, $request['body']);

            $psrRequest = $this->signature->signRequest($psrRequest, $this->credentials);

            $request['headers'] = array_merge($psrRequest->getHeaders(), $request['headers']);

            return $handler($request);
        };
    }
}
