<?php

namespace App\Service\Factory;

use App\Entity\Family\DrugTypeFamily;

/**
 * Service: Search.
 *
 * @since  1.1.0
 */
abstract class SearchServiceFactory
{
    /**
     * $optType defaulting reference.
     *
     * @var string
     */
    const TYPE_ANY = 'any';

    /**
     * Search Term
     *
     * @var ?string
     */
    private $searchTerm;

    /**
     * Search Term Query
     *
     * @var ?string
     */
    private $searchTermQuery;

    /**
     * Option: Limit Search on Specific Drug Type
     *
     * @var string
     */
    private $optType = self::TYPE_ANY;

    /**
     * Output: Search for Auto-Complete use flag
     *
     * @var boolean
     */
    private $optIsAutoComplete = false;

    /**
     * Output: Search for Export use flag
     *
     * @var boolean
     */
    private $optIsExport       = false;

    /**
     * Option: Export in Complete Data flag
     *
     * @var boolean
     */
    private $optExportComplete = true;

    /**
     * Option: Export File Prefix "CoreService::BRANDING-{?}-{date}-" (App\Utility\ExportCsv)
     *
     * @var string
     */
    private $optExportPrefix = 'search-export';

    /**
     * Option: Use Term Search for Unid look up Only flag
     *
     * @var boolean
     */
    private $optUnidOnly     = false;

    /**
     * Option: Limit search for Drug that has Full-Code flag
     * - Even incomplete
     * - Only apply for Generic Drug; has Full-Code
     *
     * @var boolean
     */
    private $optHasCodeOnly  = true;

    /**
     * List of Drugs ID with Associative-Array Keyed by Type
     *
     * @var array
     */
    private $optGivenList    = array();

    /**
     * Option: Exclusion list in Associative Array of Index Array listing ID with Type key
     *
     * @var array
     */
    private $optExcludeList  = array();

    /**
     * Search Result Main Drug Limit.
     * @uses $_ENV['SEARCH_LIMIT_SEARCHPAGE']
     *
     * @var integer
     */
    private $cfgSearchLimit       = 1000;

    /**
     * Search Result Drugs' Keywords Limit
     * @uses $_ENV['SEARCH_SUB_LIMIT_SEARCHPAGE']
     *
     * @var integer
     */
    private $cfgSearchSubLimit    = 10;

    /**
     * Auto-complete Drug Limit
     * @uses $_ENV['SEARCH_LIMIT_AUTOCOMPLETE']
     *
     * @var integer
     */
    private $cfgAutoCmpltLimit    = 10;

    /**
     * Auto-complete Drug's Keywords Limit
     * @uses $_ENV['SEARCH_SUB_LIMIT_AUTOCOMPLETE']
     *
     * @var integer
     */
    private $cfgAutoCmpltSubLimit = 13;

    /**
     * Search Result Main Drug Limit Override Flag.
     *
     * @var boolean
     */
    private $cfgSearchLimitOveridden = false;

    /**
     * Preset configuration
     *
     * Sub- is referring to Synonyms & Typo count; sub to the Drug.
     *
     * @param int $searchPgLimit     Search Page results count limit
     * @param int $searchPgSubLimit  Search Page sub-results count limit
     * @param int $searchAcLimit     Auto Complete results count limit
     * @param int $searchAcSubLimit  Auto Complete sub-results count limit
     */
    public function __construct(int $searchPgLimit, int $searchPgSubLimit, int $searchAcLimit, int $searchAcSubLimit)
    {
        $this->cfgSearchLimit       = $searchPgLimit;
        $this->cfgSearchSubLimit    = $searchPgSubLimit;
        $this->cfgAutoCmpltLimit    = $searchAcLimit;
        $this->cfgAutoCmpltSubLimit = $searchAcSubLimit;
    }

    abstract public function execute();

    /**
     * Method to return all private property in array
     *
     * @return array
     */
    protected function getDetails(): array
    {
        return array(
            // main
            'search-term'            => $this->searchTerm,
            // options
            'opt-search-type'        => $this->optType,
            'opt-export-complete'    => $this->optExportComplete,
            'opt-export-prefix'      => $this->optExportPrefix,
            'opt-search-uid'         => $this->optUnidOnly,
            'opt-has-code-only'      => $this->optHasCodeOnly,
            'opt-has-given-list'     => $this->hasGivenList(),
            'opt-given-count'        => $this->getGivenListCount(),
            'opt-has-exclusion-list' => $this->hasExclusionList(),
            'opt-exclusion-count'    => $this->getExclusionListCount(),
            // output
            'output-is-autocomplete' => $this->optIsAutoComplete,
            'output-is-export'       => $this->optIsExport,
            // configs
            'cfg-limit-main'         => $this->getLimitMain(),
            'cfg-limit-sub'          => $this->getLimitSubs(),
            'cfg-limits'             => array(
                'search-main'       => $this->cfgSearchLimit,
                'search-sub'        => $this->cfgSearchSubLimit,
                'autocomplete-main' => $this->cfgAutoCmpltLimit,
                'autocomplete-sub'  => $this->cfgAutoCmpltSubLimit,
                'overridden'        => $this->cfgSearchLimitOveridden,
            ),
        );
    }

    /**
     * @param string $searchTerm
     *
     * @return self
     */
    public function setTerm(string $searchTerm): self
    {
        $this->searchTerm = $searchTerm;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerm(): ?string
    {

        return $this->searchTerm;
    }

    /**
     * @param string $searchTermQuery
     *
     * @return self
     */
    public function setTermQuery(string $searchTermQuery): self
    {
        $this->searchTermQuery = $searchTermQuery;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTermQuery(): ?string
    {

        return $this->searchTermQuery;
    }

    /**
     * @param string $optType
     *
     * @return self
     */
    public function setType(string $optType): self
    {
        $optType = trim($optType);
        $optType = strtolower($optType);
        $this->optType = $optType;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {

        return $this->optType;
    }

    /**
     * @param boolean $optIsAutoComplete
     *
     * @return self
     */
    public function setAutoComplete(bool $optIsAutoComplete): self
    {
        $this->optIsAutoComplete = $optIsAutoComplete;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isAutoComplete(): bool
    {

        return $this->optIsAutoComplete;
    }

    /**
     * @param boolean $optIsExport
     *
     * @return self
     */
    public function setExport(bool $optIsExport): self
    {
        $this->optIsExport = $optIsExport;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isExport(): bool
    {

        return $this->optIsExport;
    }

    /**
     * @param boolean $optExportComplete
     *
     * @return self
     */
    public function setExportComplete(bool $optExportComplete): self
    {
        $this->optExportComplete = $optExportComplete;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isExportComplete(): bool
    {

        return $this->optExportComplete;
    }

    /**
     * @param string $optExportPrefix
     *
     * @return self
     */
    public function setExportPrefix(string $optExportPrefix): self
    {
        $this->optExportPrefix = $optExportPrefix;

        return $this;
    }

    /**
     * @return string
     */
    public function getExportPrefix(): string
    {

        return $this->optExportPrefix;
    }

    /**
     * @param boolean $optUnidOnly
     *
     * @return self
     */
    public function setUnidOnly(bool $optUnidOnly): self
    {
        $this->optUnidOnly = $optUnidOnly;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isUnidOnly(): bool
    {

        return $this->optUnidOnly;
    }

    /**
     * @param boolean $optHasCodeOnly
     *
     * @return self
     */
    public function setHasCodeOnly(bool $optHasCodeOnly): self
    {
        $this->optHasCodeOnly = $optHasCodeOnly;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isHasCodeOnly(): bool
    {

        return $this->optHasCodeOnly;
    }

    /**
     * Method to set Given List
     *
     * @param array $optGivenList
     */
    public function setGivenList(array $optGivenList): self
    {
        if (!empty($optGivenList)) {
            $optGivenList = $this->_restructureGivenList($optGivenList);
        }

        $this->optGivenList = $optGivenList;

        return $this;
    }

    /**
     * Method to get Given List
     *
     * @return array
     */
    public function getGivenList(): array
    {

        return $this->_restructureGivenList($this->optGivenList);
    }

    /**
     * Method to count Total List Size from Given List
     *
     * @return integer
     */
    public function getGivenListCount(): int
    {
        $count = array_map('count', $this->getGivenList());
        $count = (int) array_sum($count);

        return $count;
    }

    /**
     * Method to check if any Given List were set
     *
     * @return boolean
     */
    public function hasGivenList(): bool
    {

        return (!empty($this->getGivenListCount()));
    }

    /**
     * @param array $optExcludeList
     *
     * @return self
     */
    public function setExclusionList(array $optExcludeList): self
    {
        if (!empty($optExcludeList)) {
            $optExcludeList = $this->_restructureExclusionList($optExcludeList);
        }

        $this->optExcludeList = $optExcludeList;

        return $this;
    }

    /**
     * @return array
     */
    public function getExclusionList(): array
    {

        return $this->_restructureExclusionList($this->optExcludeList);
    }

    /**
     * Method to count Total List Size from Exclusion List
     *
     * @return integer
     */
    public function getExclusionListCount(): int
    {
        $count = array_map('count', $this->getExclusionList());
        $count = (int) array_sum($count);

        return $count;
    }

    /**
     * Method to check if Exclusion List has any ID
     *
     * @return boolean
     */
    public function hasExclusionList(): bool
    {

        return (!empty($this->getExclusionListCount()));
    }

    /**
     * Override Drug Limit Count
     *
     * @return self
     */
    public function setLimitMain(int $limit): self
    {
        $this->cfgSearchLimitOveridden = true;

        $this->cfgSearchLimit = $limit;

        return $this;
    }

    /**
     * Get Main Drug Limit Count
     *
     * @return integer
     */
    public function getLimitMain(): int
    {
        if (false == $this->cfgSearchLimitOveridden) {
            if ($this->isExport()) {
                return 0;
            }

            if ($this->isAutoComplete()) {
                return $this->cfgAutoCmpltLimit;
            }
        }

        return $this->cfgSearchLimit;
    }

    /**
     * Get Sub/Keyword Drugs Limit Count
     *
     * @return integer
     */
    public function getLimitSubs(): int
    {
        if ($this->isExport()) {
            return 0;
        }

        if ($this->isAutoComplete()) {
            return $this->cfgAutoCmpltSubLimit;
        }

        return $this->cfgSearchSubLimit;
    }

    /**
     * Method to fill Given list with DrugTypeFamily::$mainDrugs array; list of ID, with keyed drug type
     *
     * @param  array  $set
     *
     * @return array
     */
    private function _restructureGivenList(array $set): array
    {
        static $givenFamilyStructure;

        if (is_null($givenFamilyStructure)) {
            // @codeCoverageIgnoreStart
            $givenFamilyStructure = array_combine(
                DrugTypeFamily::$mainDrugs,
                array_fill(0, count(DrugTypeFamily::$mainDrugs), array())
            );
            // @codeCoverageIgnoreEnd
        }

        return ($set + $givenFamilyStructure);
    }

    /**
     * Method to fill Exclusion list with DrugTypeFamily::$family array; list of ID, with keyed drug type
     *
     * @param  array  $set
     *
     * @return array
     */
    private function _restructureExclusionList(array $set): array
    {
        static $exclusionFamilyStructure;

        if (is_null($exclusionFamilyStructure)) {
            // @codeCoverageIgnoreStart
            $exclusionFamilyStructure = array_combine(
                DrugTypeFamily::$family,
                array_fill(0, count(DrugTypeFamily::$family), array())
            );
            // @codeCoverageIgnoreEnd
        }

        return ($set + $exclusionFamilyStructure);
    }
}
