<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment as Twig;

/**
 * Service: MailService.
 *
 * @since  1.0.0
 */
class MailService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * From Email Address
     *
     * @var string
     */
    private $mailFrom;

    /**
     * Email Subject Prefix
     *
     * @var string
     */
    private $subjectPrefix = '[DrugsDb]';

    /**
     * Email Template
     *
     * @var string
     */
    private $template = 'email/lined.html.twig';

    /**
     * Recording all details on last sentEmail(); \Swift_Message
     *
     * @var \Swift_Message
     */
    private $email;

    /**
     * @param ContainerInterface  $container
     * @param Twig                $twig
     * @param LoggerInterface     $logger
     * @param string              $mailFrom
     */
    public function __construct(ContainerInterface $container, Twig $twig, LoggerInterface $logger, string $mailFrom)
    {
        $this->container = $container;
        $this->twig      = $twig;
        $this->logger    = $logger;
        $this->mailFrom  = $mailFrom;
    }

    /**
     * Subject Prefix Setter
     *
     * @param string $prefix
     */
    public function setSubjectPrefix(string $prefix): self
    {
        $this->subjectPrefix = $prefix;

        return $this;
    }

    /**
     * Template Setter
     *
     * @param string $template
     */
    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Template Getter
     *
     * @return  string
     */
    public function getTemplate(): string
    {

        return $this->template;
    }

    /**
     * Method to sendout Email content in template.
     *
     * @param mixed   $to
     * @param string  $subject
     * @param mixed   $content
     * @param array   $attachments
     * @param mixed   $cc
     * @param mixed   $bcc
     *
     * @return int  The number of successful recipients. Can be 0 which indicates failure
     */
    public function sentEmail($to, $subject, $content, array $attachments = array(), $cc = null, $bcc = null): int
    {
        // prep message
        $this->email = new \Swift_Message();

        // sender
        $this->email->setFrom($this->mailFrom);

        // recipient
        $this->email->setTo($to);

        // cc
        if (!empty($cc)) {
            $this->email->setCc($cc);
        }

        // bcc
        if (!empty($bcc)) {
            $this->email->setBcc($bcc);
        }

        // content - render
        if (is_string($content)) {
            $content = array(
                'content' => $content,
            );
        }
        $content = (array) $content;

        $defaults = array(
            'heading'    => 'DrugsDb',
            'content'    => '',
        );
        $content = array_merge($defaults, $content);

        // templating
        $template = $this->twig->render($this->template, $content);
        $this->email->setBody($template, 'text/html', 'UTF-8');

        // subject
        if (!empty($this->subjectPrefix)) {
            $subject = $this->subjectPrefix . " {$subject}";
        }
        $this->email->setSubject($subject);

        // attachments
        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                $this->email->attach(\Swift_Attachment::fromPath($attachment));
            }
        }

        /**
         * @var \Swift_Mailer
         */
        $mailer = $this->container->get('mailer');

        // send
        $result = $mailer->send($this->email);

        $this->logger->info("MAIL: {$result} Submitted", array(
            'to'         => preg_replace('/(.{1,3})(.*)@(.*)/i', '$1*@*', $to),
            'subject'    => $subject,
            'attachment' => (!empty($attachments) ? count($attachments) : 0),
            'cc'         => (int) (!empty($cc)),
            'bcc'        => (int) (!empty($bcc)),
        ));

        return $result;
    }
}
