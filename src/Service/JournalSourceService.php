<?php

namespace App\Service;

use App\Entity\Journal;
use App\Exception\RemoteSyncException;
use App\Utility\MsSqlRemoteConnection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Service: Journal Source Service.
 * Process Query & Sync process for Journal remote source.
 *
 * Source:
 * - We will give an ID, so ALTER permission is needed for IDENTITY_INSERT
 * - Journal might only have on EITHER of the Table - uneven
 * - Journal Primary table using INT ID (provided from DrugDb end)
 * - but Journal Secondary are based on Primary.UNID field - generated on their end
 *
 * @since  1.2.0
 */
class JournalSourceService
{
    use \App\Service\Traits\SourceDataConverterServiceTrait;
    use \App\Service\Traits\SqlQueryBuildHelperServiceTrait;

    /**
     * @see services "app_remote_mssql.journal"
     *
     * @var MsSqlRemoteConnection
     */
    public $remote;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var boolean
     */
    private $upSync;

    /**
     * @var string  Remote SQL Server DB Schema
     */
    private $dbSchema = 'dbo';

    /**
     * @var string
     */
    private $tblPrimary;

    /**
     * @var string
     */
    private $tblSecondary;

    /**
     * @var string
     */
    private $tblSpecialties;

    /**
     * Last Query Error Info
     *
     * @var array
     */
    private $lastQueryErrorInfo;

    /**
     * @param MsSqlRemoteConnection  $remote
     * @param EntityManagerInterface $em
     * @param LoggerInterface        $logger
     * @param boolean                $upSync
     * @param string                 $dbSchema
     * @param string                 $tblPrimary
     * @param string                 $tblSecondary
     * @param string                 $tblSpecialties
     */
    public function __construct(MsSqlRemoteConnection $remote, EntityManagerInterface $em, LoggerInterface $logger, bool $upSync, string $dbSchema, string $tblPrimary, string $tblSecondary, string $tblSpecialties)
    {
        $this->remote         = $remote;
        $this->em             = $em;
        $this->logger         = $logger;
        $this->upSync         = $upSync;
        $this->dbSchema       = $dbSchema;
        $this->tblPrimary     = $tblPrimary;
        $this->tblSecondary   = $tblSecondary;
        $this->tblSpecialties = $tblSpecialties;
    }

    /**
     * @return boolean
     */
    public function isUpSync(): bool
    {

        return $this->upSync;
    }

    /**
     * Method to return Main Journal table name; Journal
     *
     * @param  boolean $withSchema  Return with Schema prefix
     * @param  boolean $withQoute   Wrap table name & schema (if any) with double quote
     *
     * @return string
     */
    public function getPrimaryTableName(bool $withSchema = true, bool $withQoute = true): string
    {

        return $this->__tableNameFormatter($this->tblPrimary, ($withSchema ? $this->dbSchema : null), $withQoute);
    }

    /**
     * Method to return Journal-Details table name; Journals_Detail
     *
     * @param  boolean $withSchema  Return with Schema prefix
     * @param  boolean $withQoute   Wrap table name & schema (if any) with double quote
     *
     * @return string
     */
    public function getSecondaryTableName(bool $withSchema = true, bool $withQoute = true): string
    {

        return $this->__tableNameFormatter($this->tblSecondary, ($withSchema ? $this->dbSchema : null), $withQoute);
    }

    /**
     * Method to return Journal-Specialties table name; JournalSpecialties
     *
     * @param  boolean $withSchema  Return with Schema prefix
     * @param  boolean $withQoute   Wrap table name & schema (if any) with double quote
     *
     * @return string
     */
    public function getSpecialtiesTableName(bool $withSchema = true, bool $withQoute = true): string
    {

        return $this->__tableNameFormatter($this->tblSpecialties, ($withSchema ? $this->dbSchema : null), $withQoute);
    }

    /**
     * Method to up-sync Journal.
     *
     * @param Journal $journal  Note by reference
     *
     * @throws \Exception           If Missing ID - but this is quite impossible
     * @throws RemoteSyncException  If up-sync process failed;
     *                              - \PDOException - Query / Permission
     *                              - App\Exception\MsSqlRemoteConnectionException - Connection / Permission
     *
     * @return  boolean if upsync is made
     */
    public function upsync(Journal &$journal): bool
    {
        $processed = false;

        if (false == $this->isUpSync()) {
            return $processed;
        }

        if (empty($journal->getId())) {
            throw new \Exception('Missing Journal ID');
        }

        try {
            $this->remote->makeConnection(true);

            // uneven
            if ($this->_checkIdExitsOnTable($this->getPrimaryTableName(), $journal->getId())) {
                $process = 'update';
                if (empty($journal->getUnid())) {
                    $this->_createJournalUnid($journal->getId());
                    $this->_copyRemoteIds($journal);
                }

                if (empty($journal->getUnid())) {
                    throw new \Exception('Journal has no UNID');
                }

                $this->_createNewJournalOnTable($this->getSecondaryTableName(), $journal->getUnid(), false);
            }
            else {
                $process = 'insert';
                $this->_createNewJournalOnTable($this->getPrimaryTableName(), $journal->getId(), true);
                $this->_copyRemoteIds($journal);

                if (empty($journal->getUnid())) {
                    throw new \Exception('Unable to get Journal UNID');
                }

                // using generated id here
                $this->_createNewJournalOnTable($this->getSecondaryTableName(), $journal->getUnid(), false);
            }

            $this->_updateJournal($journal);

            $processed = $this->_updateJournalSpecialties($journal);

            $this->logger->info('Remote Journal UpSync Complete', array(
                // expected string 'insert' / 'update'
                'P' => $process,
                // expected true
                'D' => $processed,
                // expects INT
                'I' => $journal->getId(),
                // expect ARRAY ["00000", null, null]
                'E' => $this->lastQueryErrorInfo,
            ));
        }
        catch (\Exception $e) {
            throw new RemoteSyncException($e, $this->logger);
        }

        return $processed;
    }

    /**
     * Method to remove journal.
     *
     * @param Journal $journal
     *
     * @throws RemoteSyncException  If up-sync process failed;
     *                              - \PDOException - Query / Permission
     *                              - App\Exception\MsSqlRemoteConnectionException - Connection / Permission
     *
     * @return  boolean if upsync is made
     */
    public function remove(Journal $journal): bool
    {
        if (false == $this->isUpSync()) {
            return false;
        }

        if (
            (empty($journal->getId())) ||
            (empty($journal->getUnid()))
        ) {
            throw new RemoteSyncException(new \Exception('Journal has no record of Previous Site ID.'), $this->logger);
        }

        try {
            $this->remote->makeConnection(true);
            $conn = $this->remote->getConnection();

            $tableName = $this->getSpecialtiesTableName();
            $statement = $conn->prepare("DELETE FROM {$tableName} WHERE JournalID = :JournalID;");
            $statement->bindValue(':JournalID', $journal->getId(), \PDO::PARAM_INT);
            $processedSpecialties = $statement->execute();
            $specialtiesCount = $statement->rowCount();
            $this->lastQueryErrorInfo = $conn->errorInfo();

            $tableName = $this->getSecondaryTableName();
            $statement = $conn->prepare("DELETE FROM {$tableName} WHERE JournalID = :JournalID;");
            $statement->bindValue(':JournalID', $journal->getUnid(), \PDO::PARAM_STR);
            $processedSecondary = $statement->execute();
            $this->lastQueryErrorInfo = $conn->errorInfo();

            $tableName = $this->getPrimaryTableName();
            $statement = $conn->prepare("DELETE FROM {$tableName} WHERE JournalID = :JournalID;");
            $statement->bindValue(':JournalID', $journal->getId(), \PDO::PARAM_INT);
            $processedPrimary = $statement->execute();
            $this->lastQueryErrorInfo = $conn->errorInfo();

            $this->logger->info('Remote Journal Removal Complete', array(
                // expected string 'insert' / 'update'
                'P' => 'delete',
                // expected true
                'primary' => $processedPrimary,
                // expected true
                'secondary' => $processedSecondary,
                // expected true
                'specialties' => $processedSpecialties,
                // expects INT
                'specialties-count' => $specialtiesCount,
                // expects INT - id
                'I' => $journal->getId(),
                // expects STRING - CongressID
                'V' => $journal->getUnid(),
                // expect ARRAY ["00000", null, null]
                'E' => $this->lastQueryErrorInfo,
            ));
        }
        catch (\Exception $e) {
            throw new RemoteSyncException($e, $this->logger);
        }

        return ($processedPrimary && $processedSecondary && $processedSpecialties);
    }

    /**
     * Method to check if ID already had on Primary table, to identify if Journal is to be Update.
     *
     * @param  string      $tableName
     * @param  int|string  $journalId
     *
     * @return boolean
     */
    private function _checkIdExitsOnTable(string $tableName, $journalId): bool
    {
        $conn = $this->remote->getConnection();

        $statement = $conn->prepare("SELECT COUNT(*) AS count FROM {$tableName} WHERE JournalID = :JournalID;");
        $statement->bindValue(':JournalID', $journalId, (is_int($journalId) ? \PDO::PARAM_INT : \PDO::PARAM_STR));
        $statement->execute();
        $this->lastQueryErrorInfo = $conn->errorInfo();

        $count = $statement->fetchColumn();

        return (!empty($count));
    }

    /**
     * Create new record with just an ID on give table
     *
     * @param string      $tableName
     * @param int|string  $journalId
     * @param boolean     $skipCheck   Skip checking if ID already had
     *
     * @return boolean
     */
    private function _createNewJournalOnTable(string $tableName, $journalId, bool $skipCheck = false): bool
    {
        $processed = false;

        if (
            (false == $skipCheck) &&
            ($this->_checkIdExitsOnTable($tableName, $journalId))
        ) {
            return $processed;
        }

        $conn = $this->remote->getConnection();

        if ($this->getPrimaryTableName() !== $tableName) {
            $statement = $conn->prepare("INSERT INTO {$tableName} (JournalID) VALUES (:JournalID);");
            $statement->bindValue(':JournalID', $journalId, \PDO::PARAM_STR);
            $processed = $statement->execute();
            $this->lastQueryErrorInfo = $conn->errorInfo();
        }
        else {
            $statement = $conn->prepare("SET IDENTITY_INSERT {$tableName} ON;");
            $statement->execute();

            $statement = $conn->prepare("INSERT INTO {$tableName} (JournalID, Name, UNID, rowguid) VALUES (:JournalID, '', CONVERT(VARCHAR(32), REPLACE(NEWID(),'-','')), NEWID());");
            $statement->bindValue(':JournalID', $journalId, \PDO::PARAM_INT);
            $processed = $statement->execute();
            $this->lastQueryErrorInfo = $conn->errorInfo();

            $statement = $conn->prepare("SET IDENTITY_INSERT {$tableName} OFF;");
            $statement->execute();
        }

        return $processed;
    }

    /**
     * Method to force update Remote Journal UNID, may overwrite it.
     *
     * @param int $journalId
     *
     * @return boolean
     */
    private function _createJournalUnid(int $journalId): bool
    {
        $processed = false;

        $conn = $this->remote->getConnection();

        $tableName = $this->getPrimaryTableName();
        $statement = $conn->prepare("UPDATE {$tableName} SET (UNID = CONVERT(VARCHAR(32), REPLACE(NEWID(),'-',''))) WHERE JournalID = :JournalID;");
        $statement->bindValue(':JournalID', $journalId, \PDO::PARAM_INT);
        $processed = $statement->execute();
        $this->lastQueryErrorInfo = $conn->errorInfo();

        return $processed;
    }

    /**
     * Method to query Remote Journal by ID, and copy the generated IDs
     *
     * @param Journal $journal
     */
    private function _copyRemoteIds(Journal $journal): void
    {
        $conn = $this->remote->getConnection();

        $tableName = $this->getPrimaryTableName();
        $statement = $conn->prepare("SELECT UNID, CAST(rowguid AS VARCHAR(36)) AS rowguid FROM {$tableName} WHERE JournalID = :JournalID;");
        $statement->bindValue(':JournalID', $journal->getId(), \PDO::PARAM_INT);
        $statement->execute();
        $this->lastQueryErrorInfo = $conn->errorInfo();

        $remoteJournal = $statement->fetch(\PDO::FETCH_ASSOC);

        if (!empty($remoteJournal['UNID'])) {
            $journal->setUnid($remoteJournal['UNID']);
        }

        if (!empty($remoteJournal['rowguid'])) {
            $journal->setDetailsPreviousRowGuid($remoteJournal['rowguid']);
        }
    }

    /**
     * Method to Update Journal records.
     *
     * @param Journal $journal
     *
     * @throws \Exception
     *
     * @return boolean  PDO Executed with no Error Code
     */
    private function _updateJournal(Journal $journal): bool
    {
        $processed = false;

        $conn = $this->remote->getConnection();

        // primary table
        $mapping = $this->_getMappingForPrimaryTable($journal);
        list($setters, $binders) = $this->__convertMappingIntoSettersAndBinders($mapping);

        $tableName = $this->getPrimaryTableName();
        $statement = $conn->prepare("UPDATE {$tableName} SET {$setters} WHERE JournalID = :JournalID;");

        $this->__bindingValues($statement, $binders);

        $statement->bindValue(':JournalID', $journal->getId(), \PDO::PARAM_INT);

        $processed = $statement->execute();
        $this->lastQueryErrorInfo = $conn->errorInfo();

        if (false == $processed) {
            throw new \Exception('Unable to update Primary Table. ' . json_encode([
                'error-info' => $this->lastQueryErrorInfo,
                'id'         => $journal->getId(),
            ]));
        }

        // secondary table
        $mapping = $this->_getMappingForSecondaryTable($journal);
        list($setters, $binders) = $this->__convertMappingIntoSettersAndBinders($mapping);

        $tableName = $this->getSecondaryTableName();
        $statement = $conn->prepare("UPDATE {$tableName} SET {$setters} WHERE JournalID = :JournalUNID;");

        $this->__bindingValues($statement, $binders);

        $statement->bindValue(':JournalUNID', $journal->getUnid(), \PDO::PARAM_STR);

        $processed = $statement->execute();
        $this->lastQueryErrorInfo = $conn->errorInfo();

        if (false == $processed) {
            throw new \Exception('Unable to update Secondary Table. ' . json_encode([
                'error-info' => $this->lastQueryErrorInfo,
                'id'         => $journal->getId(),
            ]));
        }

        return $processed;
    }

    /**
     * Provide mapping and populate value for Main (Journal) Table.
     * Format @see SqlQueryBuildHelperTrait::__convertMappingIntoSetterAndBinder()
     *
     * @param Journal $journal
     *
     * @return array
     */
    private function _getMappingForPrimaryTable(Journal $journal): array
    {
        return [
            [
                'field' => 'Name',
                'token' => ':Name',
                'bind'  => (!empty($journal->getName()) ? $journal->getName() : null),
            ],
            [
                'field' => 'Abbreviation',
                'token' => ':Abbreviation',
                'bind'  => (!empty($journal->getAbbreviation()) ? $journal->getAbbreviation() : null),
            ],
            [
                'field' => 'URL',
                'token' => ':URL',
                'bind'  => (!empty($journal->getDetailsUrlUrl()) ? $journal->getDetailsUrlUrl() : null),
            ],
            [
                'field' => 'Status',
                'token' => ':Status',
                'bind'  => (!empty($journal->getStatus()) ? ($journal->getStatus() == Journal::STATUS_ACTIVE ? 'Active' : 'Archive') : null),
            ],
            [
                'field' => 'BeginDate',
                'token' => ':BeginDate',
                'bind'  => (null !== $journal->getDetailsUrlUpdatedOn() ? $journal->getDetailsUrlUpdatedOn()->format('Y-m-d H:i:s') : null),
            ],
            [
                'field' => 'LastUpdate',
                'token' => ':LastUpdate',
                'bind'  => (null !== $journal->getDetailsUrlToUpdate() ? $journal->getDetailsUrlToUpdate()->format('Y-m-d H:i:s') : null),
            ],
            [
                'field' => 'Deleted',
                'token' => ':Deleted',
                'bind'  => ($journal->isDeleted() ? 1 : 0),
            ],
            [
                'field' => 'JR1',
                'token' => ':JR1',
                'bind'  => $journal->getJournalReport1(),
            ],
            [
                'field' => 'JR2',
                'token' => ':JR2',
                'bind'  => $journal->getJournalReport2(),
            ],
            [
                'field' => 'ISSN',
                'token' => ':ISSN',
                'bind'  => $journal->getMedlineIssn(),
            ],
            [
                'field' => 'DGAbstract',
                'token' => ':DGAbstract',
                'bind'  => ($journal->isDgAbstract() ? 1 : 0),
            ],
            [
                'field' => 'SecondLine',
                'token' => ':SecondLine',
                'bind'  => ($journal->isSecondLine() ? 1 : 0),
            ],
        ];
    }

    /**
     * Provide mapping and populate value for Detail (Journals_Detail) Table.
     * Format @see SqlQueryBuildHelperTrait::__convertMappingIntoSetterAndBinder()
     *
     * @param Journal $journal
     *
     * @return array
     */
    private function _getMappingForSecondaryTable(Journal $journal): array
    {
        $lastView = [];
        if (null !== $journal->getDetailsUrlViewedOn()) {
            $lastView[] = $journal->getDetailsUrlViewedOn()->format('Y-m-d H:i:s');
        }
        if (!empty($journal->getDetailsUrlViewedOnComment())) {
            $lastView[] = $journal->getDetailsUrlViewedOnComment();
        }

        $countryName = null;
        if (!empty($journal->getDetailsCountry())) {
            /**
             * @var \App\Repository\CountryRepository<\App\Entity\Country>
             */
            $countryRepo = $this->em->getRepository(\App\Entity\Country::class);

            /**
             * @var \App\Entity\Country|null
             */
            $country = $countryRepo->findOneBy(['isoCode' => $journal->getDetailsCountry()]);
            if (!empty($country)) {
                $countryName = $country->getName();

            }
        }
        // off Alias
        $countryName = self::___convertOutCountryName($countryName);

        $regionName = null;
        if (!empty($journal->getDetailsRegion())) {
            /**
             * @var \App\Repository\CountryTaxonomyRepository<\App\Entity\CountryTaxonomy>
             */
            $countryTaxRepo = $this->em->getRepository(\App\Entity\CountryTaxonomy::class);

            /**
             * @var \App\Entity\CountryTaxonomy|null
             */
            $countryTax = $countryTaxRepo->findOneBy(['id' => $journal->getDetailsRegion()]);
            if (!empty($countryTax)) {
                $regionName = $countryTax->getName();
            }
        }
        // off Alias
        $regionName = self::___convertOutRegionName($regionName);

        return [
            [
                'field' => 'JournalVolume',
                'token' => ':JournalVolume',
                'bind'  => $journal->getVolume(),
            ],
            [
                'field' => 'JournalNumber',
                'token' => ':JournalNumber',
                'bind'  => $journal->getNumber(),
            ],
            [
                'field' => 'LastViewed',
                'token' => ':LastViewed',
                'bind'  => (!empty($lastView) ? implode('; ', $lastView) : null),
            ],
            [
                'field' => 'Notes',
                'token' => ':Notes',
                'bind'  => (!empty($journal->getDetailsUrlNote()) ? $journal->getDetailsUrlNote() : null),
            ],
            [
                'field' => 'Frequency',
                'token' => ':Frequency',
                'bind'  => (!empty($journal->getDetailsUrlFrequency()) ? $journal->getDetailsUrlFrequency() : null),
            ],
            [
                'field' => 'UserName',
                'token' => ':UserName',
                'bind'  => (!empty($journal->getDetailsUrlUsername()) ? $journal->getDetailsUrlUsername() : null),
            ],
            [
                'field' => 'Password',
                'token' => ':Password',
                'bind'  => (!empty($journal->getDetailsUrlPassword()) ? $journal->getDetailsUrlPassword() : null),
            ],
            [
                'field' => 'Publisher',
                'token' => ':Publisher',
                'bind'  => (!empty($journal->getDetailsPublisherName()) ? $journal->getDetailsPublisherName() : null),
            ],
            [
                'field' => 'regurl',
                'token' => ':regurl',
                'bind'  => (!empty($journal->getDetailsPublisherRegUrl()) ? $journal->getDetailsPublisherRegUrl() : null),
            ],
            [
                'field' => 'Language',
                'token' => ':Language',
                'bind'  => (!empty($journal->getDetailsLanguage()) ? $journal->getDetailsLanguage() : null),
            ],
            [
                'field' => 'Country',
                'token' => ':Country',
                'bind'  => $countryName,
            ],
            [
                'field' => 'Region',
                'token' => ':Region',
                'bind'  => $regionName,
            ],
            [
                'field' => 'Editions',
                'token' => ':Editions',
                'bind'  => (!empty($journal->getDetailsEdition()) ? $journal->getDetailsEdition() : null),
            ],
            [
                'field' => 'Copyright',
                'token' => ':Copyright',
                'bind'  => (!empty($journal->getDetailsCopyright()) ? $journal->getDetailsCopyright() : null),
            ],
            [
                'field' => 'NotBeingUpdated',
                'token' => ($journal->isNotBeingUpdated() ? "CONVERT(BINARY, 'Y')" : "CONVERT(BINARY, 'N')"),
                'bind'  => false,
            ],
            [
                'field' => 'DrugsMonitored',
                'token' => ($journal->isDrugsMonitored() ? "CONVERT(BINARY, 'Y')" : "CONVERT(BINARY, 'N')"),
                'bind'  => false,
            ],
            [
                'field' => 'GlobalEditionJournal',
                'token' => ($journal->isGlobalEditionJournal() ? "CONVERT(BINARY, 'Y')" : "CONVERT(BINARY, 'N')"),
                'bind'  => false,
            ],
            [
                'field' => 'abstract',
                'token' => ($journal->getDetailsAbstractAbstract() ? "CONVERT(BINARY, 'Y')" : "CONVERT(BINARY, 'N')"),
                'bind'  => false,
            ],
            [
                'field' => 'regabstract',
                'token' => ($journal->getDetailsAbstractRegForAbstract() ? "CONVERT(BINARY, 'Y')" : "CONVERT(BINARY, 'N')"),
                'bind'  => false,
            ],
            [
                'field' => 'fulltxt',
                'token' => ($journal->getDetailsAbstractFullTextAvailablity() ? "CONVERT(BINARY, 'Y')" : "CONVERT(BINARY, 'N')"),
                'bind'  => false,
            ],
            [
                'field' => 'regfulltxt',
                'token' => ($journal->getDetailsAbstractRegForFullText() ? "CONVERT(BINARY, 'Y')" : "CONVERT(BINARY, 'N')"),
                'bind'  => false,
            ],
            [
                'field' => 'feefulltxt',
                'token' => ($journal->getDetailsAbstractFeeForFullText() ? "CONVERT(BINARY, 'Y')" : "CONVERT(BINARY, 'N')"),
                'bind'  => false,
            ],
            [
                'field' => 'comments',
                'token' => ':comments',
                'bind'  => (!empty($journal->getDetailsAbstractComments()) ? $journal->getDetailsAbstractComments() : null),
            ],
            [
                'field' => 'DocAuthor',
                'token' => ':DocAuthor',
                'bind'  => (!empty($journal->getDetailsValidationAuthor()) ? $journal->getDetailsValidationAuthor() : null),
            ],
            [
                'field' => 'cleared_by',
                'token' => ':cleared_by',
                'bind'  => (!empty($journal->getDetailsValidationClearedBy()) ? $journal->getDetailsValidationClearedBy() : null),
            ],
            [
                'field' => 'cleared_on',
                'token' => ':cleared_on',
                'bind'  => (null !== $journal->getDetailsValidationClearedOn() ? $journal->getDetailsValidationClearedOn()->format('d M Y H:i:s') : null),
            ],
            [
                'field' => 'Copied',
                'token' => ':Copied',
                'bind'  => ($journal->isExported() ? 'Yes' : 'No'),
            ],
            [
                'field' => 'Modified',
                'token' => ':Modified',
                'bind'  => (!empty($journal->getDetailsValidationModified()) ? $journal->getDetailsValidationModified() : null),
            ],
            [
                'field' => 'JournalHTMLCode',
                'token' => ':JournalHTMLCode',
                'bind'  => (!empty($journal->getDetailsOrderingHtmlCode()) ? $journal->getDetailsOrderingHtmlCode() : null),
            ],
            [
                'field' => 'CGIValue',
                'token' => ':CGIValue',
                'bind'  => (!empty($journal->getDetailsOrderingCgiValue()) ? $journal->getDetailsOrderingCgiValue() : null),
            ],
            [
                'field' => 'PrinterName',
                'token' => ':PrinterName',
                'bind'  => (!empty($journal->getDetailsOrderingPrinterName()) ? $journal->getDetailsOrderingPrinterName() : null),
            ],
        ];
    }

    /**
     * Remove all OneToMany relationships, and create new records of Specialty
     *
     * @param Journal $journal
     *
     * @return bool
     */
    private function _updateJournalSpecialties(Journal $journal): bool
    {
        $conn = $this->remote->getConnection();

        $tableName = $this->getSpecialtiesTableName();

        $statement = $conn->prepare("DELETE FROM {$tableName} WHERE JournalID = :JournalID;");
        $statement->bindValue(':JournalID', $journal->getId(), \PDO::PARAM_INT);
        $processed = $statement->execute();
        $this->lastQueryErrorInfo = $conn->errorInfo();

        if (0 == $journal->getSpecialties()->count()) {
            return true;
        }

        $fields  = '';
        $tokens  = [];
        $binders = [];
        foreach ($journal->getSpecialties() as $specialty) {
            list($fields, $tokens[], $binders[]) = $this->__convertMappingIntoFieldsTokensAndBinders([
                [
                    'field' => 'JournalID',
                    'token' => ':JournalID' . $specialty->getSpecialty()->getId(),
                    'bind'  => $journal->getId(),
                ],
                [
                    'field' => 'Specialty',
                    'token' => ':Specialty' . $specialty->getSpecialty()->getId(),
                    'bind'  => $specialty->getSpecialty()->getSpecialty(),
                ],
            ]);
        }

        $tokens = 'SELECT ' . implode(' UNION ALL SELECT ', $tokens);

        $statement = $conn->prepare("INSERT INTO {$tableName} ({$fields}) {$tokens};");

        foreach ($binders as $binder) {
            $this->__bindingValues($statement, $binder);
        }

        $processed = $statement->execute();
        $this->lastQueryErrorInfo = $conn->errorInfo();

        return $processed;
    }
}
