<?php

namespace App\Service;

use App\Entity\Brand;
use App\Entity\Experimental;
use App\Entity\Family\DrugTypeFamily;
use App\Entity\Generic;
use App\Exception\LotusSyncException;
use App\Utility\Helpers as H;
use App\Utility\LotusConnection;
use Psr\Log\LoggerInterface;

/**
 * Service: LotusService.
 *
 * @since  1.0.0
 */
class LotusService extends LotusConnection
{
    /**
     * Last Query Responses.
     *
     * @var array
     */
    private $lastQueryErrorInfo = array();

    /**
     * Method to return last query error, queries within these class only.
     *
     * @codeCoverageIgnore
     *
     * @return array  SEE PDO::errorInfo
     */
    public function getLastQueryError(): array
    {
        return $this->lastQueryErrorInfo;
    }

    /**
     * Method to UPSERT (UPDATE / INSERT) Drug to Remote.
     *
     * Note:
     * - We use local ID to sync back to Lotus.
     *     - So in the event of INSERT, Lotus PK check were disabled first.
     * - Connection based on Exception during error, to try/catch block will in action
     *
     * @codeCoverageIgnore
     *
     * @param string          $entity
     * @param DrugTypeFamily  $drug
     *
     * @throws LotusSyncException  As process to sync were rejected/failed.
     *
     * @return bool  Process Success Flag
     */
    public function upsertDrug(string $entity, DrugTypeFamily $drug): bool
    {
        $processed = false;

        do {
            if (!$this->isUpSync()) {
                break;
            }

            if (!in_array($entity, DrugTypeFamily::$mainDrugs)) {
                $this->logger->error('LOTUS: Unknown Entity', array('entity' => $entity));
                break;
            }

            try {
                // throwing if fail
                $this->makeConnection(true);

                $found = $this->_ifIdExistsOnLotus($drug);
                if ($found) {
                    // update
                    $processed = $this->_updateDrugOnLotus($drug);
                }
                else {
                    // insert
                    $processed = $this->_insertDrugOnLotus($drug);
                }

                $last_id = $this->lotus->lastInsertId();

                $this->logger->info('LOTUS: Result ' . __METHOD__, array(
                    // expected true
                    'P' => $processed,
                    // count found
                    'C' => $found,
                    // expects INT for insert, FALSE for update
                    'I' => $last_id,
                    // expect ARRAY ["00000", null, null]
                    'E' => $this->lastQueryErrorInfo,
                ));
            }
            catch (\Exception $e) {
                throw new LotusSyncException($e, $this->logger);
            }
        } while (false);

        return !empty($processed);
    }

    /**
     * Method to UPDATE / DELETE Drug Keywords.
     *
     * @codeCoverageIgnore
     *
     * @param string  $type    Keyword, Expects 'typo', 'synonym'
     * @param string  $entity  Drug, Expects 'experimental', 'generic', 'brand'
     * @param int     $drugId  Drug ID
     * @param array   $new     New Drug List, uses $new['name']
     * @param array   $still   Continue Exits Drug List, uses $still['name']
     *
     * @throws LotusSyncException  As process to sync were rejected/failed.
     *
     * @return bool  Process Success Flag
     */
    public function refreshDrugKeywords(string $type, string $entity, int $drugId, $new = array(), $still = array()): bool
    {
        $processed = false;

        do {
            if (!$this->isUpSync()) {
                break;
            }

            if (!in_array($type, DrugTypeFamily::$mainKeywords)) {
                $this->logger->error('LOTUS: Unknown Keyword', array('keyword' => $type));
                break;
            }

            if (!in_array($entity, DrugTypeFamily::$mainDrugs)) {
                $this->logger->error('LOTUS: Unknown Entity', array('entity' => $entity));
                break;
            }

            $sql = $this->_getKeywordUpdateSql($type, $entity, $drugId, $new, $still);
            if (empty($sql)) {
                break;
            }

            try {
                // throwing if fail
                $this->makeConnection(true);

                $processed = $this->lotus->exec($sql);

                $this->lastQueryErrorInfo = $this->lotus->errorInfo();

                $this->logger->info('LOTUS: Result ' . __METHOD__, array(
                    'P' => $processed,
                    'E' => $this->lastQueryErrorInfo,
                ));
            }
            catch (\Exception $e) {
                throw new LotusSyncException($e, $this->logger);
            }
        } while (false);

        return !empty($processed);
    }

    /**
     * Method to INSERT / DELETE to Drug relation.
     *
     * @codeCoverageIgnore
     *
     * @param bool    $coupling     true to create, false to remove
     * @param string  $lower        Drug, Expects 'experimental', 'generic', 'brand'
     * @param int     $lowerDrugId  Lower Drug ID (CHILDREN)
     * @param string  $upper        Drug, Expects 'experimental', 'generic', 'brand'
     * @param int     $upperDrugId  Upper Drug ID (PARENT)
     *
     * @throws LotusSyncException  As process to sync were rejected/failed.
     *
     * @return bool  Process Success Flag
     */
    public function updateDrugRelation(bool $coupling, string $lower, int $lowerDrugId, string $upper, int $upperDrugId = 0): bool
    {
        $processed = false;

        do {
            if (!$this->isUpSync()) {
                break;
            }

            if (!in_array($lower, DrugTypeFamily::$mainDrugs)) {
                $this->logger->error('LOTUS: Unknown Lower Entity', array('entity' => $lower));
                break;
            }

            if (!in_array($upper, DrugTypeFamily::$mainDrugs)) {
                $this->logger->error('LOTUS: Unknown Upper Entity', array('entity' => $upper));
                break;
            }

            // Currently only support 1 relationships Generic -> Brand
            if (
                ('brand' != $lower) ||
                ('generic' != $upper)
            ) {
                break;
            }

            $sql = $this->_getDrugRelationSql($coupling, $lower, $lowerDrugId, $upper, $upperDrugId);
            if (empty($sql)) {
                break;
            }

            try {
                // throwing if fail
                $this->makeConnection(true);

                $stmt = $this->lotus->prepare($sql);
                $processed = $stmt->execute();
                $stmt->closeCursor();

                $this->lastQueryErrorInfo = $this->lotus->errorInfo();

                if ($coupling) {
                    if ('00000' == $this->lastQueryErrorInfo[0]) {
                        $processed = 1;
                    }
                }

                $this->logger->info('LOTUS: Result ' . __METHOD__, array(
                    'P' => $processed,
                    'E' => $this->lastQueryErrorInfo,
                ));
            }
            catch (\Exception $e) {
                throw new LotusSyncException($e, $this->logger);
            }
        } while (false);

        return !empty($processed);
    }

    /**
     * Method to DELETE Drug.
     *
     * @codeCoverageIgnore
     *
     * @param string  $entity  Drug, Expects 'experimental', 'generic', 'brand'
     * @param int     $drugId  Drug ID
     *
     * @throws LotusSyncException  As process to sync were rejected/failed.
     *
     * @return boolean
     */
    public function deleteDrug(string $entity, int $drugId): bool
    {
        $processed = false;

        do {
            if (!$this->isUpSync()) {
                break;
            }

            if (!in_array($entity, DrugTypeFamily::$mainDrugs)) {
                $this->logger->error('LOTUS: Unknown Entity', array('entity' => $entity));
                break;
            }

            $sql = $this->_getDeleteDrugSql($entity, $drugId);
            if (empty($sql)) {
                break;
            }

            try {
                // throwing if fail
                $this->makeConnection(true);

                $processed = $this->lotus->exec($sql);

                $this->lastQueryErrorInfo = $this->lotus->errorInfo();

                if (empty($processed)) {
                    if ('00000' == $this->lastQueryErrorInfo[0]) {
                        $processed = 1;
                    }
                }

                $this->logger->info('LOTUS: Result ' . __METHOD__, array(
                    'P' => $processed,
                    'E' => $this->lastQueryErrorInfo,
                ));
            }
            catch (\Exception $e) {
                throw new LotusSyncException($e, $this->logger);
            }
        } while (false);

        return (!empty($processed));
    }

    /**
     * Method to check if Drug ID exists on Lotus Db
     *
     * @param DrugTypeFamily|Brand|Experimental|Generic $drug
     *
     * @return boolean
     */
    private function _ifIdExistsOnLotus(DrugTypeFamily $drug): bool
    {
        $id = $drug->getId();
        if (empty($id)) {
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }

        $found = $sql = false;

        switch (true) {
            case ($drug instanceof Experimental):
                $sql = 'SELECT COUNT(*) AS found FROM Experimental WHERE ExperimentalID = :syncid;';
                break;

            case ($drug instanceof Generic):
                $sql = 'SELECT COUNT(*) AS found FROM Generic WHERE GenericID = :syncid;';
                break;

            case ($drug instanceof Brand):
                $sql = 'SELECT COUNT(*) AS found FROM Brand WHERE BrandID = :syncid;';
                break;
        }

        if (!empty($sql)) {
            $sql = H::removeExcessWhitespace($sql);

            $statement = $this->lotus->prepare($sql);
            $statement->bindParam(':syncid', $id, \PDO::PARAM_INT);
            $statement->execute();
            $found = $statement->fetch(\PDO::FETCH_ASSOC)['found'];
            $statement->closeCursor();
            $this->logger->debug('LOTUS: Check ID Exists', array('drug' => get_class($drug), 'found' => $found, 'sql' => $sql));
        }

        return (!empty($found));
    }

    /**
     * Method to INSERT drug to Lotus DB
     *
     * @param DrugTypeFamily $drug
     *
     * @return boolean  $statement->execute() Result
     */
    private function _insertDrugOnLotus(DrugTypeFamily $drug): bool
    {
        $sql = $tableName = $statement = false;
        $extra = array();

        switch (true) {
            case ($drug instanceof Experimental):
                $tableName = 'Experimental';
                $sql = '
                    INSERT INTO
                        Experimental (
                            ExperimentalID,
                            ExperimentalName,
                            ExperimentalUNID
                        )
                    VALUES (
                        :syncid,
                        :name,
                        :unid
                    );
                ';
                break;

            case ($drug instanceof Generic):
                $tableName = 'Generic';
                $sql = '
                    INSERT INTO
                        Generic (
                            GenericID,
                            GenericName,
                            GenericUNID,
                            Level1,
                            Level2,
                            Level3,
                            Level4,
                            Level5
                        )
                    VALUES (
                        :syncid,
                        :name,
                        :unid,
                        :syncLvl1,
                        :syncLvl2,
                        :syncLvl3,
                        :syncLvl4,
                        :syncLvl5
                    );
                ';
                // getter extra
                for ($i = 1; $i <= 5; ++$i) {
                    $fuc = "getLevel{$i}";
                    $extra[":syncLvl{$i}"] = array($drug->$fuc(), \PDO::PARAM_STR);
                }
                break;

            case ($drug instanceof Brand):
                $tableName = 'Brand';
                $sql = '
                    INSERT INTO
                        Brand (
                            BrandID,
                            BrandName,
                            BrandUNID
                        )
                    VALUES (
                        :syncid,
                        :name,
                        :unid
                    );
                ';
                break;
        }

        if (!empty($sql)) {
            $sql = H::removeExcessWhitespace($sql);
            $this->logger->debug('LOTUS: Insert Drug', array('drug' => get_class($drug), 'sql' => $sql));
            $statement = $this->lotus->prepare($sql);

            $this->_setUpsertParamBinding($statement, $drug->getId(), $drug->getName(), ($drug->getUid() ?? ''), $extra);
        }

        $processed = false;
        if ((!empty($statement)) && (!empty($tableName))) {
            $stmt = $this->lotus->prepare("SET IDENTITY_INSERT {$tableName} ON;");
            $stmt->execute();
            $stmt->closeCursor();

            $processed = $statement->execute();
            $this->lastQueryErrorInfo = $this->lotus->errorInfo();
            $statement->closeCursor();

            $stmt = $this->lotus->prepare("SET IDENTITY_INSERT {$tableName} OFF;");
            $stmt->execute();
            $stmt->closeCursor();
        }

        return $processed;
    }

    /**
     * Method to UPDATE drug to Lotus DB
     *
     * @param DrugTypeFamily $drug
     *
     * @return boolean  $statement->execute() Result
     */
    private function _updateDrugOnLotus(DrugTypeFamily $drug): bool
    {
        $sql = $statement = false;
        $extra = array();

        switch (true) {
            case ($drug instanceof Experimental):
                $sql = '
                    UPDATE
                        Experimental SET
                            ExperimentalName = :name,
                            ExperimentalUNID = :unid
                    WHERE
                        ExperimentalID = :syncid
                    ;
                ';
                break;

            case ($drug instanceof Generic):
                $sql = '
                    UPDATE
                        Generic SET
                            GenericName = :name,
                            GenericUNID = :unid,
                            Level1 = :syncLvl1,
                            Level2 = :syncLvl2,
                            Level3 = :syncLvl3,
                            Level4 = :syncLvl4,
                            Level5 = :syncLvl5
                    WHERE
                        GenericID = :syncid
                    ;
                ';
                // getter extra
                for ($i = 1; $i <= 5; ++$i) {
                    $fuc = "getLevel{$i}";
                    $extra[":syncLvl{$i}"] = array($drug->$fuc(), \PDO::PARAM_STR);
                }
                break;

            case ($drug instanceof Brand):
                $sql = '
                    UPDATE
                        Brand SET
                            BrandName = :name,
                            BrandUNID = :unid
                    WHERE
                        BrandID = :syncid
                ';
                break;
        }

        if (!empty($sql)) {
            $sql = H::removeExcessWhitespace($sql);
            $this->logger->debug('LOTUS: Update Drug', array('drug' => get_class($drug), 'sql' => $sql));
            $statement = $this->lotus->prepare($sql);

            $this->_setUpsertParamBinding($statement, $drug->getId(), $drug->getName(), ($drug->getUid() ?? ''), $extra);
        }

        $processed = false;
        if (!empty($statement)) {
            $processed = $statement->execute();
            $statement->closeCursor();
            $this->lastQueryErrorInfo = $this->lotus->errorInfo();
        }

        return $processed;
    }

    /**
     * Helper method to Bind Param, with null check for UPSERT SQL.
     * Caution:
     * - Read Up on bindParam(); second argument return with reference.
     * - Any changes, it will consider a new object/referral
     *
     * @param \PDOStatement  $statement
     * @param integer|null   $id
     * @param string|null    $name
     * @param string|null    $uid
     * @param array          $extra  [[0: value, 1: PDO-PARAM-TYPE-IDENTIFIER]]
     */
    private function _setUpsertParamBinding(\PDOStatement &$statement, ?int $id = null, ?string $name = null, ?string $uid = null, array $extra = array()): void
    {
        $set = array(
            ':syncid' => array('id',   \PDO::PARAM_INT),
            ':name'   => array('name', \PDO::PARAM_STR),
            ':unid'   => array('uid',  \PDO::PARAM_STR),
        );

        $null = null;

        foreach ($set as $param => $sv) {
            $var = $sv[0];
            if (
                (empty($$var)) &&
                (':unid' != $param)
            ) {
                // @codeCoverageIgnoreStart
                $statement->bindParam($param, $null, \PDO::PARAM_NULL);
                // @codeCoverageIgnoreEnd
            }
            else {
                $statement->bindParam($param, $$var, $sv[1]);
            }
        }

        foreach ($extra as $param => $ext) {
            if (empty($ext[0])) {
                $statement->bindParam($param, $null, \PDO::PARAM_NULL);
            }
            else {
                $statement->bindParam($param, $ext[0], $ext[1]);
            }
        }
    }

    /**
     * Method to Build SQL Command for DELETE, INSERT Drug Keywords.
     *
     * @param string  $type    Keyword, Expects 'typo', 'synonym'
     * @param string  $entity  Drug, Expects 'experimental', 'generic', 'brand'
     * @param int     $drugId  Drug ID
     * @param array   $new     New Drug List, uses $new['name']
     * @param array   $still   Continue Exits Drug List, uses $still['name']
     *
     * @return string  SQL Command String
     */
    private function _getKeywordUpdateSql(string $type, string $entity, int $drugId, array $new, array $still): string
    {
        $drug  = ucwords($entity);
        $field = ucwords($type);
        $table = "{$drug}{$field}s";

        $namelist = array_merge(
            array_column($new, 'name'),
            array_column($still, 'name')
        );
        $namelist = array_filter($namelist);
        $namelist = array_unique($namelist);
        $namelist = array_map(array($this->lotus, 'quote'), $namelist);

        $names = array();
        foreach ($namelist as $name) {
            $names[] = "SELECT {$drugId}, {$name}";
        }

        $sql = "
            DELETE FROM
                {$table}
            WHERE
                {$drug}ID = {$drugId};
        ";

        if (!empty($names)) {
            // support V9 / 2005, > 1k
            $names = implode(' UNION ALL ', $names);
            $sql .= "
                INSERT INTO
                    {$table} (
                        {$drug}ID,
                        {$field}
                    )
                SELECT {$drug}ID, {$field} FROM (
                    {$names}
                ) AS temp ({$drug}ID, {$field});
            ";
        }

        $sql = H::removeExcessWhitespace($sql);
        $this->logger->debug("LOTUS: {$entity} {$type} Keyword Changes Command", array('sql' => $sql));

        return $sql;
    }

    /**
     * Method to Build SQL Command to INSERT / DELETE Drug Relation Row.
     *
     * @param bool    $coupling     true to create, false to remove
     * @param string  $lower        Drug, Expects 'experimental', 'generic', 'brand'
     * @param int     $lowerDrugId  Lower Drug ID (CHILDREN)
     * @param string  $upper        Drug, Expects 'experimental', 'generic', 'brand'
     * @param int     $upperDrugId  Upper Drug ID (PARENT)
     *
     * @return string  SQL Command String
     */
    private function _getDrugRelationSql(bool $coupling, string $lower, int $lowerDrugId, string $upper, int $upperDrugId = 0): string
    {
        $lower_field = ucwords($lower);
        $upper_field = ucwords($upper);
        $table       = "{$lower_field}{$upper_field}";

        $sql = '';

        $conds = array();
        if (!empty($lowerDrugId)) {
            $conds[] = "{$lower_field}ID = {$lowerDrugId}";
        }
        if (!empty($upperDrugId)) {
            $conds[] = "{$upper_field}ID = {$upperDrugId}";
        }
        $conds = implode(' AND ', $conds);

        if ($coupling) {
            $sql = "
                IF NOT EXISTS (SELECT * FROM {$table} WHERE {$conds})
                    INSERT INTO {$table} (
                        {$lower_field}ID,
                        {$upper_field}ID
                    )
                    VALUES (
                        {$lowerDrugId},
                        {$upperDrugId}
                    )
            ";
        }
        elseif (!empty($conds)) {
            $sql = "
                DELETE FROM
                    {$table}
                WHERE
                    {$conds}
            ";
        }

        $sql = H::removeExcessWhitespace($sql);
        $this->logger->debug("LOTUS: {$lower}-{$upper} Relation Changes Command", array('sql' => $sql));

        return $sql;
    }

    /**
     * Method to Build SQL Command to DELETE Drug & Its other's relation.
     *
     * @param string  $entity  Drug, Expects 'experimental', 'generic', 'brand'
     * @param int     $drugId  Drug ID
     *
     * @return string  SQL Command String
     */
    private function _getDeleteDrugSql(string $entity, int $drugId): string
    {
        $sql = array();

        switch ($entity) {
            case 'experimental':
                $sql[] = "DELETE FROM Experimental WHERE ExperimentalID = {$drugId}";
                break;

            case 'generic':
                $sql[] = "DELETE FROM GenericSynonyms WHERE GenericID = {$drugId}";
                $sql[] = "DELETE FROM GenericTypos WHERE GenericID = {$drugId}";
                $sql[] = "DELETE FROM BrandGeneric WHERE GenericID = {$drugId}";
                $sql[] = "DELETE FROM Generic WHERE GenericID = {$drugId}";
                break;

            case 'brand':
                $sql[] = "DELETE FROM BrandSynonyms WHERE BrandID = {$drugId}";
                $sql[] = "DELETE FROM BrandTypos WHERE BrandID = {$drugId}";
                $sql[] = "DELETE FROM BrandGeneric WHERE BrandID = {$drugId}";
                $sql[] = "DELETE FROM Brand WHERE BrandID = {$drugId}";
                break;
        }

        $sql = implode('; ', $sql);
        $sql = H::removeExcessWhitespace($sql);
        $this->logger->debug("LOTUS: {$entity} Delete Command", array('sql' => $sql));

        return $sql;
    }
}
