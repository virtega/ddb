<?php

namespace App\Service;

use App\Exception\TrirdPartyException;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Provide Service to run query over NCBI E-Utilities.
 * Currently supports:
 * - ESearch   - Since 1.2.1
 * - ESummary  - Since 1.2.1
 *
 * @link  https://dataguide.nlm.nih.gov/eutilities/utilities.html   The 9 E-utilities and Associated Parameters.
 * @link  https://dataguide.nlm.nih.gov/eutilities/utilities.html#esearch   Database Reference for Query
 * @link  https://www.ncbi.nlm.nih.gov/books/NBK3827/#pubmedhelp.Search_Field_Descriptions_and    Details on available query Fields.
 *
 * @since  1.2.1
 */
class NcbiQueryService
{
    /**
     * Search Field for Journal Title & Abbreviation
     *
     * @var  string
     */
    public const SEARCH_FIELD_TITLE = 'JOURNAL';

    /**
     * Search Field for Journal ISSN
     *
     * @var  string
     */
    public const SEARCH_FIELD_ISSN = 'ISSN';

    /**
     * @var  string NCBI EUTILS Complete path with trailing slash
     */
    private const NCBI_UTILS_PATH = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/';

    /**
     * @var  string NCBI FCGI Endpoint for E-Search
     */
    private const NCBI_UTILS_ESEARCH = 'esearch.fcgi';

    /**
     * @var  string NCBI FCGI Endpoint for E-Summary
     */
    private const NCBI_UTILS_ESUMMARY = 'esummary.fcgi';

    /**
     * @var string ESearch Db
     */
    private const SEARCH_DB = 'nlmcatalog';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * Current request pagination.
     *
     * @var int
     */
    private $queryPaginationPage = 1;

    /**
     * Maximum result per pagination.
     *
     * @var int
     */
    private $queryPaginationMaxPerPage = 20;

    /**
     * @param LoggerInterface      $logger
     * @param HttpClientInterface  $client
     */
    function __construct(LoggerInterface $logger, HttpClientInterface $client)
    {
        $this->logger = $logger;
        $this->client = $client;
    }

    /**
     * Method to set Current pagination Page, starts with 1.
     *
     * @param int|null  $page  Sent NULL to get defaulting value
     *
     * @throws \Exception  If given value is below 0.
     *
     * @return int  New/Default value
     */
    public function setCurrentPaginationPage(?int $page = null): int
    {
        if (null === $page) {
            $page = $this->queryPaginationPage;
        }

        if ($page < 1) {
            throw new \Exception('Invalid current-page Value');
        }

        $this->queryPaginationPage = $page;

        return $this->queryPaginationPage;
    }

    /**
     * Method to set Maximum result per Page pagination, defaulting 20 as NCBI does.
     *
     * @param int|null  $maxPerPage  Sent NULL to get defaulting value
     *
     * @throws \Exception  If given value is below 0.
     *
     * @return int  New/Default value
     */
    public function setMaximumPerPaginationPage(?int $maxPerPage = null): int
    {
        if (null === $maxPerPage) {
            $maxPerPage = $this->queryPaginationMaxPerPage;
        }

        if ($maxPerPage < 1) {
            throw new \Exception('Invalid maximum-per-page Value');
        }

        $this->queryPaginationMaxPerPage = $maxPerPage;

        return $this->queryPaginationMaxPerPage;
    }

    /**
     * Method to clean up and re-structure Journal array from E-Summary.
     *
     * @todo clean up / sanitization; may apply journal data here soon
     *
     * @param array $journal
     *
     * @return array
     */
    public static function restructureJournalDetails(array $journal): array
    {

        return $journal;
    }

    /**
     * Method to query Journal using e-Search tool.
     * Submit search "term" and "field", return found UIDs.
     *
     * @param string $field
     * @param string $term
     *
     * @return array
     */
    public function eSearch(string $field, string $term): array
    {
        $payload = [
            'db'       => self::SEARCH_DB,
            'field'    => $field,
            'term'     => $term,
            'rettype'  => 'uilist',
            'retmode'  => 'json',
            'retmax'   => $this->queryPaginationMaxPerPage,
            'retstart' => (($this->queryPaginationPage - 1) * $this->queryPaginationMaxPerPage),
        ];

        return $this->_query(self::NCBI_UTILS_ESEARCH, $payload);
    }

    /**
     * Method to query Journals using e-Summary tool.
     * Submit "UIDs", and return Journals data package.
     *
     * @param array $ids
     *
     * @return array
     */
    public function eSummary(array $ids): array
    {
        $payload = [
            'db'       => self::SEARCH_DB,
            'id'       => implode(',', $ids),
            'retmode'  => 'json',
            'retmax'   => $this->queryPaginationMaxPerPage,
            'retstart' => (($this->queryPaginationPage - 1) * $this->queryPaginationMaxPerPage),
        ];

        return $this->_query(self::NCBI_UTILS_ESUMMARY, $payload);
    }

    /**
     * Method to sent query, validate and return JSON content
     *
     * @param string  $util     Expects string from constants NCBI_UTILS_*
     * @param array   $payload
     *
     * @throws TrirdPartyException  If unable to connection issue, unpack response & etc.
     *
     * @return array
     */
    private function _query(string $util, array $payload): array
    {
        /**
         * @var \Symfony\Contracts\HttpClient\ResponseInterface
         */
        $response = $this->client->request('GET', self::NCBI_UTILS_PATH . $util, [
            'query'   => $payload,
            'headers' => [
                'Content-Type' => 'application/json; charset=UTF-8',
            ],
        ]);

        /**
         * @var array  While disable Error thrown
         */
        $content = $response->toArray(false);

        $this->logger->debug('NCBI Query', [
            'method'  => $response->getInfo('http_method'),
            'path'    => $response->getInfo('url'),
            'code'    => $response->getStatusCode(),
            'content' => $content,
        ]);

        if (200 !== $response->getStatusCode()) {
            throw new TrirdPartyException("NCBI Error: {$util} - " . ($content['error'] ?? 'Unknown Error'));
        }

        $result = $content;
        switch ($util) {
            case self::NCBI_UTILS_ESEARCH:
                if (empty($content['esearchresult'])) {
                    throw new TrirdPartyException('NCBI Error: E-Search - Invalid Return structure.');
                }

                if (isset($content['esearchresult']['ERROR'])) {
                    throw new TrirdPartyException('NCBI Error: E-Search - ' . $content['esearchresult']['ERROR']);
                }

                $result = $content['esearchresult'];
                break;

            case self::NCBI_UTILS_ESUMMARY:
                if (!isset($content['result'])) {
                    $error = (array) ($content['esummaryresult'] ?? 'Invalid Return structure.');
                    throw new TrirdPartyException('NCBI Error: E-Summary - ' . end($error));
                }

                $result = $content['result'];
                break;
        }

        return $result;
    }
}
