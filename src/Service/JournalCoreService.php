<?php

namespace App\Service;

use App\Entity\Journal;
use App\Exception\RemoteSyncException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Service: Journal Core Service.
 * Process Basic CRUD for Journal entity.
 *
 * @since  1.2.0
 */
class JournalCoreService
{
    use \App\Service\Traits\EntityFormUpackServiceTrait;

    /**
     * @var JournalSourceService
     */
    protected $journalRemote;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Security
     */
    private $symfonySecurity;

    /**
     * @param JournalSourceService     $journalRemote
     * @param EntityManagerInterface   $em
     * @param LoggerInterface          $logger
     * @param Security                 $symfonySecurity
     */
    public function __construct(JournalSourceService $journalRemote, EntityManagerInterface $em, LoggerInterface $logger, Security $symfonySecurity)
    {
        $this->journalRemote   = $journalRemote;
        $this->em              = $em;
        $this->logger          = $logger;
        $this->symfonySecurity = $symfonySecurity;
    }

    /**
     * Method to get Form defaults in set or single per field name.
     *
     * @param string|null  $fieldName
     *
     * @throws \Exception  If key is unknown.
     *
     * @return mixed
     */
    public static function getFormDefaults(string $fieldName = null)
    {
        $fields = [
            'name'                 => ''  ,
            'abbreviation'         => ''  ,
            'specialty'            => []  ,
            'volume'               => ''  ,
            'issue'                => ''  ,
            'jr1'                  => 0.00,
            'jr2'                  => 0.00,
            'issn'                 => ''  ,
            'ismedline'            => 'no',
            'issecondline'         => 'no',
            'isdgabstract'         => 'no',
            'notbeingupdated'      => 'no',
            'drugsmonitored'       => 'no',
            'globaleditionjournal' => 'no',
            'manualcreation'       => 'no',
            'deleted'              => 'no',
            'urlurl'               => ''  ,
            'urlupdatedon'         => ''  ,
            'urltoupdate'          => ''  ,
            'urlviewedon'          => ''  ,
            'urlviewedoncomment'   => ''  ,
            'urlfrequency'         => ''  ,
            'urlnote'              => ''  ,
            'urlusername'          => ''  ,
            'urlpassword'          => ''  ,
            'publishername'        => ''  ,
            'publisherregurl'      => ''  ,
            'audiencelanguages'    => []  ,
            'audiencecountry'      => ''  ,
            'audienceregion'       => 0   ,
            'edition'              => []  ,
            'copyright'            => ''  ,
            'absabstract'          => 'no',
            'absregforabstract'    => 'no',
            'absftavailable'       => 'no',
            'absregforfulltext'    => 'no',
            'absfeeforfulltext'    => 'no',
            'abscomments'          => ''  ,
            'valstatus'            => ''  ,
            'valauthor'            => ''  ,
            'valdatecomposed'      => ''  ,
            'valclearedby'         => ''  ,
            'valclearedon'         => ''  ,
            'valmodified'          => ''  ,
            'valexported'          => 'no',
            'artodrhtmlcode'       => ''  ,
            'artodrcgival'         => ''  ,
            'artodrprinter'        => ''  ,
        ];

        if (null === $fieldName) {
            return $fields;
        }

        if (!isset($fields[$fieldName])) {
            throw new \Exception("Unknown field name: '{$fieldName}'.");
        }

        return $fields[$fieldName];
    }

    /**
     * Method to create new Journal from Form.
     * UpSync after Flush, so changes will be reverted (removed).
     *
     * @param array $fields
     *
     * @throws \Exception  If ID is still null after EM flush.
     *
     * @return Journal
     */
    public function create(array $fields): Journal
    {
        $journal = new Journal;
        $this->entityFormSetter($journal, $fields);

        $journal->setDetailsMetaCreatedOn(new \DateTime('now'));
        $journal->setDetailsMetaCreatedBy( $this->__getCurrentUserUsername() );

        $this->em->persist($journal);
        $this->em->flush();

        try {
            if (empty($journal->getId())) {
                throw new \Exception('Journal entity failed to return new ID, please check with Administrator.');
            }

            if ($this->journalRemote->upsync($journal)) {
                // save additional changes
                $this->em->persist($journal);
                $this->em->flush();
            }
        }
        catch (\Exception $e) {
            // revert local created objects
            foreach ($journal->getSpecialties() as $journalSpecialties) {
                $this->em->remove($journalSpecialties);
            }

            $this->em->remove($journal);
            $this->em->flush();

            throw $e;
        }

        $this->logger->info('Journal Created', [
            'id'   => $journal->getId(),
            'user' => $this->__getCurrentUserUsername(),
        ]);

        return $journal;
    }

    /**
     * Method to update Journal from Form.
     * UpSync before Flush, so changes is not permanent.
     *
     * @param Journal  $journal
     * @param array    $fields
     *
     * @return Journal
     */
    public function update(Journal $journal, array $fields): Journal
    {
        $this->entityFormSetter($journal, $fields);

        $journal->setDetailsMetaModifiedOn(new \DateTime('now'));
        $journal->setDetailsMetaModifiedBy( $this->__getCurrentUserUsername() );

        $this->journalRemote->upsync($journal);

        $this->em->persist($journal);
        $this->em->flush();

        $this->logger->info('Journal Updated', [
            'id'   => $journal->getId(),
            'user' => $this->__getCurrentUserUsername(),
        ]);

        return $journal;
    }

    /**
     * Method to remove Journal both local and remote.
     *
     * @param Journal  $journal
     */
    public function delete(Journal $journal): void
    {
        $this->journalRemote->remove($journal);

        foreach ($journal->getSpecialties() as $journalSpecialties) {
            $this->em->remove($journalSpecialties);
        }

        $id = $journal->getId();

        $this->em->remove($journal);
        $this->em->flush();

        $this->logger->info('Journal Deleted', [
            'id'   => $id,
            'user' => $this->__getCurrentUserUsername(),
        ]);
    }

    /**
     * Method to set Forms value to a Journal
     *
     * @param Journal  $journal  Note by reference
     * @param array    $fields
     */
    private function entityFormSetter(Journal &$journal, array $fields): void
    {
        $fields = $fields + self::getFormDefaults(null);

        $journal->setName($fields['name']);

        $value = (!empty($fields['abbreviation']) ? $fields['abbreviation'] : null);
        $journal->setAbbreviation($value);

        $value = (!empty($fields['volume']) ? $fields['volume'] : null);
        $journal->setVolume($value);

        $value = (!empty($fields['issue']) ? $fields['issue'] : null);
        $journal->setNumber($value);

        $journal->setStatus((int) $fields['valstatus']);

        $value = $this->__unpackYesNoToIntRadioField($fields['valexported']);
        $journal->setExported($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['ismedline']);
        $journal->setIsMedline($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['issecondline']);
        $journal->setIsSecondLine($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['isdgabstract']);
        $journal->setIsDgAbstract($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['notbeingupdated']);
        $journal->setNotBeingUpdated($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['drugsmonitored']);
        $journal->setDrugsMonitored($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['globaleditionjournal']);
        $journal->setGlobalEditionJournal($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['deleted']);
        $journal->setDeleted($value);

        $value = $this->__unpackYesNoToIntRadioField($fields['manualcreation']);
        $journal->setManualCreation($value);

        $value = (!empty($fields['issn']) ? $fields['issn'] : null);
        $journal->setMedlineIssn($value);

        $value = (float) $fields['jr1'];
        $journal->setJournalReport1($value);

        $value = (float) $fields['jr2'];
        $journal->setJournalReport2($value);

        $value = $this->__unpackStringBaseListField($fields['audiencelanguages']);
        $journal->setDetailsLanguage($value);

        $value = (!empty($fields['audiencecountry']) ? $fields['audiencecountry'] : null);
        $journal->setDetailsCountry($value);

        $value = (!empty($fields['audienceregion']) ? $fields['audienceregion'] : null);
        $journal->setDetailsRegion($value);

        $value = $this->__unpackStringBaseListField($fields['edition']);
        $journal->setDetailsEdition($value);

        $value = (!empty($fields['copyright']) ? $fields['copyright'] : '');
        $journal->setDetailsCopyright($value);

        $value = $this->__unpackYesNoToBoolRadioField($fields['absabstract']);
        $journal->setDetailsAbstractAbstract($value);

        $value = $this->__unpackYesNoToBoolRadioField($fields['absregforabstract']);
        $journal->setDetailsAbstractRegForAbstract($value);

        $value = $this->__unpackYesNoToBoolRadioField($fields['absftavailable']);
        $journal->setDetailsAbstractFullTextAvailablity($value);

        $value = $this->__unpackYesNoToBoolRadioField($fields['absregforfulltext']);
        $journal->setDetailsAbstractRegForFullText($value);

        $value = $this->__unpackYesNoToBoolRadioField($fields['absfeeforfulltext']);
        $journal->setDetailsAbstractFeeForFullText($value);

        $value = (!empty($fields['abscomments']) ? $fields['abscomments'] : '');
        $journal->setDetailsAbstractComments($value);

        $value = (!empty($fields['artodrhtmlcode']) ? $fields['artodrhtmlcode'] : '');
        $journal->setDetailsOrderingHtmlCode($value)
        ;
        $value = (!empty($fields['artodrcgival']) ? $fields['artodrcgival'] : '');
        $journal->setDetailsOrderingCgiValue($value);

        $value = (!empty($fields['artodrprinter']) ? $fields['artodrprinter'] : '');
        $journal->setDetailsOrderingPrinterName($value);

        $value = (!empty($fields['publishername']) ? $fields['publishername'] : '');
        $journal->setDetailsPublisherName($value);

        $value = (!empty($fields['publisherregurl']) ? $fields['publisherregurl'] : '');
        $journal->setDetailsPublisherRegUrl($value);

        $value = (!empty($fields['urlurl']) ? $fields['urlurl'] : '');
        $journal->setDetailsUrlUrl($value);

        $value = $this->__unpackDateTimePickerField($fields['urlupdatedon'], 'Y-m-d');
        $journal->setDetailsUrlUpdatedOn($value);

        $value = $this->__unpackDateTimePickerField($fields['urlviewedon'], 'Y-m-d');
        $journal->setDetailsUrlViewedOn($value);

        $value = (!empty($fields['urlviewedoncomment']) ? $fields['urlviewedoncomment'] : '');
        $journal->setDetailsUrlViewedOnComment($value);

        $value = $this->__unpackDateTimePickerField($fields['urltoupdate'], 'Y-m-d');
        $journal->setDetailsUrlToUpdate($value);

        $value = (!empty($fields['urlnote']) ? $fields['urlnote'] : '');
        $journal->setDetailsUrlNote($value);

        $value = (!empty($fields['urlfrequency']) ? $fields['urlfrequency'] : '');
        $journal->setDetailsUrlFrequency($value);

        $value = (!empty($fields['urlusername']) ? $fields['urlusername'] : '');
        $journal->setDetailsUrlUsername($value);

        $value = (!empty($fields['urlpassword']) ? $fields['urlpassword'] : '');
        $journal->setDetailsUrlPassword($value);

        $value = (!empty($fields['valauthor']) ? $fields['valauthor'] : '');
        $journal->setDetailsValidationAuthor($value);

        $value = $this->__unpackDateTimePickerField($fields['valdatecomposed'], 'Y-m-d H:i:s');
        $journal->setDetailsValidationDateComposed($value);

        $value = (!empty($fields['valclearedby']) ? $fields['valclearedby'] : '');
        $journal->setDetailsValidationClearedBy($value);

        $value = $this->__unpackDateTimePickerField($fields['valclearedon'], 'Y-m-d H:i:s');
        $journal->setDetailsValidationClearedOn($value);

        $value = (!empty($fields['valmodified']) ? $fields['valmodified'] : '');
        $journal->setDetailsValidationModified($value);

        $this->__unpackAndAssignJournalSpecilties($journal, $fields['specialty']);
    }
}
