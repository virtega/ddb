<?php

namespace App\Service;

use App\Security\User\LdapUser;
use App\Utility\Crypto;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Managing encrypted Cookies.
 * - Encrypt user name and email for lock-screen use.
 *
 * @since  1.2.0
 */
class LoginCookieService
{
    /**
     * Cookie Key reference
     * @var string
     */
    public static $cookieName = 'drugdb-settings';

    /**
     * Cookie Playload
     * @var array
     */
    private $cookieData = [
        'login' => [
            'name'  => null,
            'email' => null,
        ],
    ];

    /**
     * @var Crypto
     */
    private $crypto;

    /**
     * @param Crypto $crypto
     */
    function __construct(Crypto $crypto)
    {
        $this->crypto = $crypto;
    }

    /**
     * Preset cookie details from given Request.
     *
     * @param Request        $request
     * @param Response       $response
     * @param TokenInterface $token
     */
    public function setCookie(Request $request, Response $response, TokenInterface $token): void
    {
        /**
         * @var \Symfony\Component\HttpFoundation\Session\Session
         */
        $session = $request->getSession();
        if (!empty($session)) {
            // update set for user details
            $user = $token->getUser();
            if ($user instanceof LdapUser) {
                $this->setUserDetails($user);
            }

            $response->headers->setCookie(Cookie::create(self::$cookieName, $this->encrypt()));
        }
    }

    /**
     * Force loading local cookie reference from Request.
     *
     * @codeCoverageIgnore  Did not implemented for this version
     *
     * @param Request $request
     */
    public function loadNewCookie(Request $request): void
    {
        $cookie = $request->cookies->get(self::$cookieName);
        if (!empty($cookie)) {
            $this->decrypt($cookie);
        }
    }

    /**
     * Check is cookie have login details.
     *
     * @param Request $request
     *
     * @return boolean
     */
    public function doCookieHasLogin(?Request $request): bool
    {
        do {
            if (!is_null($request)) {
                $cookie = $request->cookies->get(self::$cookieName);
                if (empty($cookie)) {
                    break;
                }

                if (!$this->decrypt($cookie))  {
                    break;
                }
            }

            if (
                (empty($this->cookieData['login']['name'])) ||
                (empty($this->cookieData['login']['email']))
            ) {
                break;
            }

            return true;
        } while (false);

        return false;
    }

    /**
     * Method to get login Username on loaded cookie.
     *
     * @return string|null
     */
    public function getCookieUsername(): ?string
    {

        return (empty($this->cookieData['login']['name']) ? null : $this->cookieData['login']['name']);
    }

    /**
     * Method to get login Email on loaded cookie.
     *
     * @return string|null
     */
    public function getCookieEmail(): ?string
    {
        return (empty($this->cookieData['login']['email']) ? null : $this->cookieData['login']['email']);
    }

    /**
     * Method to start encrypt loaded cookie.
     *
     * @return string  Encrypted string
     */
    private function encrypt(): string
    {
        $encrypted = (string) json_encode($this->cookieData);
        $encrypted = $this->crypto::encrypt($encrypted);

        return $encrypted;
    }

    /**
     * Method to decrypt string (data), and loaded to current object.
     * If process failed, this will return null; IE
     * - encryption may have expiration
     * - encryption salt changed
     *
     * @param string $encrypted
     *
     * @return boolean
     */
    private function decrypt(string $encrypted): bool
    {
        $encrypted = $this->crypto::decrypt($encrypted);
        $encrypted = json_decode($encrypted, true);

        if ($encrypted) {
            $this->cookieData = $encrypted;
            return true;
        }

        return false;
    }

    /**
     * Method to set local cookie detail based based on LDAP User object.
     *
     * @param LdapUser $user
     */
    private function setUserDetails(LdapUser $user): void
    {
        $this->cookieData['login']['name']  = $user->getDisplayName();
        $this->cookieData['login']['email'] = $user->getEmail();
    }
}
