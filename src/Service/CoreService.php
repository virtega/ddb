<?php

namespace App\Service;

use App\Entity\Brand;
use App\Entity\Experimental;
use App\Entity\Family\DrugTypeFamily;
use App\Entity\Generic;
use App\Exception\DrugCrudException;
use App\Exception\LotusSyncException;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Service: core.
 *
 * @since  1.0.0
 */
class CoreService
{
    /**
     * Version.
     *
     * @var float
     */
    const VERSION = 1.2;

    /**
     * General Branding.
     * - Download prefix
     *
     * @var string
     */
    const BRANDING = 'DrugDb';

    /**
     * @var \Doctrine\ORM\EntityManager
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var LotusService
     */
    private $lotus;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param EntityManagerInterface  $em
     * @param ContainerInterface      $container
     * @param LotusService            $lotus
     * @param LoggerInterface         $logger
     */
    public function __construct(EntityManagerInterface $em, ContainerInterface $container, LotusService $lotus, LoggerInterface $logger)
    {
        $this->em        = $em;
        $this->container = $container;
        $this->lotus     = $lotus;
        $this->logger    = $logger;
    }

    /**
     * Method to get Any Drug by ID.
     *
     * @param string  $entity  Expects 'experimental', 'generic', 'brand'
     * @param int     $id      Drug ID
     *
     * @throws DrugCrudException  If required item not meet expected criteria, or not found
     *
     * @return DrugTypeFamily
     */
    public function getDrug(string $entity, int $id): DrugTypeFamily
    {
        if (!in_array($entity, DrugTypeFamily::$mainDrugs)) {
            throw new DrugCrudException("Unknown Drug Type: '{$entity}'.");
        }

        /**
         * @var \Doctrine\ORM\EntityRepository<\App\Repository\Drug\BrandDrugRepository>
         * @var \Doctrine\ORM\EntityRepository<\App\Repository\Drug\ExperimentalDrugRepository>
         * @var \Doctrine\ORM\EntityRepository<\App\Repository\Drug\GenericDrugRepository>
         */
        $repo = $this->_getDrugRepo($entity);
        $found = $repo->getDrug($id);
        if (null == $found) {
            throw new DrugCrudException("Unknown {$entity} Drug #{$id}.");
        }

        return $found;
    }

    /**
     * Method to get Drug Family by Main ID.
     *
     * @param string  $entity  Expects 'experimental', 'generic', 'brand'
     * @param int     $id      Drug ID
     *
     * @throws DrugCrudException  If required item not meet expected criteria, or not found
     *
     * @return array
     */
    public function getDrugFamily(string $entity, int $id): array
    {
        if (!in_array($entity, DrugTypeFamily::$mainDrugs)) {
            throw new DrugCrudException("Unknown Drug Type: '{$entity}'.");
        }

        return $this->_getDrugRepo($entity)->getDrugFamily($id);
    }

    /**
     * Method to get Drug Complete NET Family by one of the member.
     *
     * @param string  $entity  Expects 'experimental', 'generic', 'brand'
     * @param int     $id      Drug ID
     *
     * @throws DrugCrudException  If required item not meet expected criteria, or not found
     *
     * @return array
     */
    public function getDrugEntireFamily(string $entity, int $id): array
    {
        if (!in_array($entity, DrugTypeFamily::$mainDrugs)) {
            throw new DrugCrudException("Unknown Drug Type: '{$entity}'.");
        }

        /**
         * @var \App\Repository\DrugRepository
         */
        $repo = $this->_getDrugRepo($entity);
        $found = $repo->queryDrugFamily($entity, $id, false, true);

        $count = array_map('count', $found);
        $count = array_filter($count);
        $count = count($count);
        if (0 == $count) {
            throw new DrugCrudException("Unknown {$entity} Drug #{$id}.");
        }

        return $found;
    }

    /**
     * Method to get Experimental Drug Family by ID.
     *
     * @param int  $id  Drug ID
     *
     * @return array
     */
    public function getExperimentalDrug(int $id): array
    {
        return $this->em->getRepository(Experimental::class)->getDrugFamily($id);
    }

    /**
     * Method to get Generic Drug Family by ID.
     *
     * @param int  $id  Drug ID
     *
     * @return array
     */
    public function getGenericDrug(int $id): array
    {
        return $this->em->getRepository(Generic::class)->getDrugFamily($id);
    }

    /**
     * Method to get Brand Drug Family by ID.
     *
     * @param int  $id  Drug ID
     *
     * @return array
     */
    public function getBrandDrug(int $id): array
    {
        return $this->em->getRepository(Brand::class)->getDrugFamily($id);
    }

    /**
     * Method to get Generic Reference List Member by Level.
     *
     * @param int  $level  Level Number, expects 1 / 2 / 3 / 4 only
     *
     * @throws DrugCrudException  If required item not meet expected criteria
     *
     * @return array
     */
    public function getGenericRefLevel(int $level = 1): array
    {
        if (!in_array($level, range(1, 4))) {
            throw new DrugCrudException("Unknown Generic Reference Level: '{$level}'.");
        }

        $function = "getAllGenericRefLevel{$level}";

        return $this->em->getRepository(Generic::class)->$function();
    }

    /**
     * Method to process New / Update Drug; Form Submission.
     *
     * @param string                                          $entity   Expects 'experimental', 'generic', 'brand'
     * @param \Symfony\Component\HttpFoundation\ParameterBag  $request  Request ParameterBag
     * @param bool|int                                        $id       Drug ID, or false for New Flag
     *
     * @throws DrugCrudException   If required item not meet expected criteria, or Drug Name is Empty
     * @throws LotusSyncException  As process to sync were rejected/failed.
     *
     * @return array    [0] status boolean, [1] message, [2] drug.`id`
     */
    public function updateDrug($entity, \Symfony\Component\HttpFoundation\ParameterBag $request, $id = false): array
    {
        if (!in_array($entity, DrugTypeFamily::$mainDrugs)) {
            throw new DrugCrudException("Unknown Drug Type: '{$entity}'.");
        }

        $repo = $this->_getDrugRepo($entity);

        $data = array();
        foreach ($request->keys() as $key) {
            if (in_array($key, array(
                // process keyword later
                "{$entity}_synonyms",
                "{$entity}_typos",
                // from API-add-drug
                'format',
                'mapped_experimental',
                'mapped_generic',
                'mapped_brand',
            ))) {
                continue;
            }
            $data[$key] = $request->get($key);
        }

        $must_have = array(
            // all
            'name'                => '',
            'type'                => $entity,
            'comments'            => null,
            'valid'               => 0,
            'unid'                => null,
            'creation_date'       => null,
            // extra - Generic & Brand Only
            'auto_encode_explude' => 0,
            // extra - Generic Only
            'no_double_bounce'    => 0,
            'level_1'             => null,
            'level_2'             => null,
            'level_3'             => null,
            'level_4'             => null,
            'level_5'             => null,
        );

        foreach ($must_have as $key => $default) {
            if (!isset($data[$key])) {
                $data[$key] = $default;
            }
        }

        if (empty($data['creation_date'])) {
            $data['creation_date'] = null;
        }

        $data['name']     = trim($data['name']);
        $data['comments'] = trim($data['comments']);

        if (empty($data['name'])) {
            throw new DrugCrudException('Drug Name is empty.');
        }

        $originalClone = null;
        if (!empty($id)) {
            $originalClone = clone $repo->getDrug($id);
        }

        $drug = $repo->updateDrug($id, $data);
        $repo->flushAllNow();

        try {
            $lotus = $this->lotus->upsertDrug($entity, $drug);
        }
        // @codeCoverageIgnoreStart
        catch(LotusSyncException $e) {
            // revert it
            $this->_revertDrugEntity($drug, $originalClone, (empty($id)));

            // re-throw
            throw $e;
        }
        // @codeCoverageIgnoreEnd

        $this->logger->info("CORE: Drug {$entity} updated: " . (!empty($id) ? 'update' : 'add'), array(
            'id'    => $id,
            'lotus' => ($lotus ?? false),
        ));

        // processing sub-drugs; synonyms & typos
        if (in_array($entity, array('generic', 'brand'))) {
            $predata = $repo->getDrugFamily($drug->getId());

            try {
                $this->_processSynonymsSubmission(
                    $repo,
                    $entity,
                    $predata,
                    $request->get("{$entity}_synonyms")
                );

                $this->_processTyposSubmission(
                    $repo,
                    $entity,
                    $predata,
                    $request->get("{$entity}_typos")
                );
            }
            // @codeCoverageIgnoreStart
            catch(LotusSyncException $e) {
                // revert parent only, those attachments just persist, but not flushed yet
                $this->_revertDrugEntity($drug, $originalClone, (empty($id)));

                // re-throw
                throw $e;
            }
            // @codeCoverageIgnoreEnd

            $repo->flushAllNow();
        }

        // update indexes
        $this->triggerCommandProcess((string) \App\Command\WidgetUpdatesCommand::getDefaultName(), 0);

        $msg = 'Drug details has been updated';
        if (empty($id)) {
            $msg = 'There an issue to register the drug';
            if (!empty($drug)) {
                $msg = 'Drug has been registered #' . $drug->getId();
            }
        }
        // @codeCoverageIgnoreStart
        elseif (empty($drug)) {
            $msg = 'There an issue to update the drug';
        }
        // @codeCoverageIgnoreEnd

        return array(
            (!empty($drug)),
            $msg,
            $drug->getId(),
            $drug,
        );
    }

    /**
     * Method to process Drug removal; JS-POST Submission.
     *
     * @param string  $entity  Expects 'experimental', 'generic', 'brand'
     * @param int     $id
     *
     * @throws DrugCrudException  If required item not meet expected criteria
     *
     * @return array    [0] status boolean, [1] message
     */
    public function deleteDrug($entity, $id): array
    {
        if (!in_array($entity, DrugTypeFamily::$mainDrugs)) {
            throw new DrugCrudException("Unknown Drug Type: '{$entity}'.");
        }

        $untied     = $removed     = 0;
        $unties     = $removes     = array();
        $untiesRepo = $removesRepo = false;

        switch ($entity) {
            case 'experimental':
                $removesRepo = $this->_getDrugRepo('experimental');
                $untiesRepo  = $this->_getDrugRepo('generic');
                $removes['experimental'] = $removesRepo->getDrugFamily($id);
                $unties['generic']       = $untiesRepo->getDrugFamily($id, 'experimental');
                break;

            case 'generic':
                $removesRepo = $this->_getDrugRepo('generic');
                $untiesRepo  = $this->_getDrugRepo('brand');
                $removes['generic'] = $removesRepo->getDrugFamily($id);
                $unties['brand']    = $untiesRepo->getDrugFamily($id, 'generic');
                break;

            case 'brand':
                $removesRepo = $this->_getDrugRepo('brand');
                $removes['brand'] = $removesRepo->getDrugFamily($id);
                break;
        }

        // remove from other entity
        foreach ($unties as $type => $set) {
            $set = array_reverse($set, true);
            foreach ($set as $set_type => $set_entity) {
                if (('main' != $set_type) || (empty($set_entity))) {
                    continue;
                }

                $lower = false;
                switch ($type) {
                    case 'generic': $lower = 'experimental'; break;
                    case 'brand':   $lower = 'generic';      break;
                }

                if (!empty($lower)) {
                    $lower_ent = 'get' . ucwords($lower);
                    $lower_ent = $set_entity->$lower_ent();

                    $lotus = $this->lotus->updateDrugRelation(false, $type, $set['main']->getId(), $lower, $lower_ent->getId());

                    $this->logger->info("CORE: Delete Drug {$type}-{$lower} Relation", array(
                        $lower_ent->getId() => array(
                            $type => array(
                                'before' => $set_entity->getId(),
                                'after'  => 0,
                                'lotus'  => $lotus,
                            ),
                        ),
                    ));

                    $func = 'set' . ucwords($lower);
                    $set_entity->$func(null);
                    if (!empty($untiesRepo)) {
                        $untiesRepo->persist($set_entity);
                    }

                    ++$untied;
                }

            }
        }
        if (!empty($untiesRepo)) {
            $untiesRepo->flushAllNow();
        }

        // remove it
        foreach ($removes as $type => $set) {
            $set = array_reverse($set, true);
            foreach ($set as $set_type => $set_entity) {
                // mark to which it's own relation
                if ('main' == $set_type) {
                    $rev = false;
                    switch ($type) {
                        case 'generic': $rev = 'getExperimental'; break;
                        case 'brand'  : $rev = 'getGeneric';      break;
                    }
                    if (!empty($rev)) {
                        if (!empty($set_entity->$rev())) {
                            // just mark, no removal, as this will be removed anyhow
                            ++$untied;
                        }
                    }
                }

                if (
                    (false !== strpos($set_type, '_keys')) ||
                    (empty($set_entity))
                ) {
                    continue;
                }

                $ftype = ucwords($type);

                // by type
                if ('main' == $set_type) {
                    // sent to lotus by main only, it will process the rest
                    $lotus = $this->lotus->deleteDrug($type, $set_entity->getId());

                    $this->logger->info("CORE: Delete {$ftype} Drug", array(
                        'id'    => $set_entity->getId(),
                        'name'  => $set_entity->getName(),
                        'lotus' => $lotus,
                    ));

                    // removal
                    // false for keywords, which should be remove from family list above
                    if (!empty($removesRepo)) {
                        $removed += $removesRepo->removeDrug($set_entity, false);
                    }
                }
                else {
                    // keywords, wraps in array
                    $func = false;
                    switch ($set_type) {
                        case 'typos':    $func = 'removeTypo';    break;
                        case 'synonyms': $func = 'removeSynonym'; break;
                    }

                    $items = array();
                    if (!empty($func)) {
                        foreach ($set_entity as $ent_id => $an_entity) {
                            // for log
                            $items[] = array(
                                'id'   => $an_entity->getId(),
                                'name' => $an_entity->getName(),
                            );
                            // delete
                            $removed += $removesRepo->$func($an_entity);
                        }
                    }

                    $this->logger->info("CORE: Delete {$ftype} {$set_type}", array(
                        $set['main']->getId() => $items,
                    ));
                }
            }
        }
        if (!empty($removesRepo)) {
            $removesRepo->flushAllNow();
        }

        // update indexes
        $this->triggerCommandProcess((string) \App\Command\WidgetUpdatesCommand::getDefaultName(), 0);

        return array(
            true,
            "{$removed} Record" . ($removed > 1 ? 's' : '') . " Removed, meanwhile {$untied} detached.",
            $removed,
            $untied,
        );
    }

    /**
     * Method to update Drug Entity relations.
     *
     * @param string       $upperEntity
     * @param int          $upperId
     * @param bool|string  $lowerEntity
     * @param bool|int     $lowerId
     *
     * @throws DrugCrudException  If Lower Entity given but missing ID
     *
     * @return boolean
     */
    public function updateDrugRelation($upperEntity, $upperId, $lowerEntity = false, $lowerId = false): bool
    {
        $complete = false;

        // get upper
        $upperRepo = $this->_getDrugRepo($upperEntity);
        $upper     = $upperRepo->getDrug($upperId);

        if (empty($upper)) {
            // @codeCoverageIgnoreStart
            throw new DrugCrudException(sprintf('Unknown %s ID #%d.', $upperEntity, $upperId));
            // @codeCoverageIgnoreEnd
        }

        if (
            (empty($lowerEntity)) &&
            (empty($lowerId))
        ) {
            // removal
            switch ($upperEntity) {
                case 'generic': $lowerEntity = 'experimental'; break;
                case 'brand'  : $lowerEntity = 'generic';      break;
            }

            // method
            if (!is_bool($lowerEntity)) {
                $uc_lower = ucwords($lowerEntity);
                $func     = "get{$uc_lower}";

                // note that lotus only support generic-brand, other will be ignore.
                // thus we can expect return false.
                // but in the event of LotusSyncException, all of these will stops.
                $lotus = $this->lotus->updateDrugRelation(
                    false,
                    $upperEntity,
                    $upper->getId(),
                    $lowerEntity,
                    ($upper->$func() ? $upper->$func()->getId() : 0)
                );

                $this->logger->info("CORE: Delete {$upperEntity} relation", array(
                    $upper->getId() => array(
                        $lowerEntity => array(
                            'before' => ($upper->$func() ? $upper->$func()->getId() : 0),
                            'after'  => 0,
                            'lotus'  => $lotus,
                        ),
                    ),
                ));

                // update to removal
                $func = "set{$uc_lower}";
                $upper->$func(null);

                // done
                $complete = true;
            }
        }
        // @codeCoverageIgnoreStart
        // PHPUNIT Bug not able to cover the conditional line only, test for logic is provided
        elseif (empty($lowerId)) {
            throw new DrugCrudException('Missing lower-entity ID.');
        }
        // @codeCoverageIgnoreEnd
        elseif (!is_bool($lowerEntity)) {
            // get lower
            $lowerRepo = $this->_getDrugRepo($lowerEntity);
            $uc_lower  = ucwords($lowerEntity);
            $lower     = $lowerRepo->getDrug($lowerId);

            // add / change
            $action = 'Add';
            $func   = "get{$uc_lower}";
            if ($upper->$func()) {
                $action = 'Update';
            }

            $lotus = $this->lotus->updateDrugRelation(
                true,
                $upperEntity,
                $upper->getId(),
                $lowerEntity,
                $lower->getId()
            );

            $this->logger->info("CORE: {$action} {$upperEntity} relation", array(
                $upper->getId() => array(
                    $lowerEntity => array(
                        'before' => ($upper->$func() ? $upper->$func()->getId() : 0),
                        'after'  => $lower->getId(),
                        'lotus'  => $lotus,
                    ),
                ),
            ));

            $func = "set{$uc_lower}";
            $upper->$func($lower);

            $complete = true;
        }

        $upperRepo->persist($upper);
        $upperRepo->flushAllNow();

        $this->triggerCommandProcess((string) \App\Command\WidgetUpdatesCommand::getDefaultName(), 0);

        return $complete;
    }

    /**
     * Method to trigger Command in Background Process, and return its Process State.
     *
     * @uses \usleep() Waiting the process to execute
     * - If PS five Loading flag, it will re-trigger sleep FIVE (5) times before final check
     *
     * @param string     $commandWithAttrs  bin/console command + attributes only
     * @param float|int  $checkAfterSec     false to just return PS, this value use for usleep() above.
     *                                       Best for Command to set this as 1-2 Seconds.
     *                                       Suggested 0.2 best fits for stand-alone PHP.
     *
     * @return array
     */
    public function triggerCommandProcess($commandWithAttrs, $checkAfterSec = 0.2): array
    {
        if (!$this->_isThisLinuxFamily()) {
            // @codeCoverageIgnoreStart
            throw new DrugCrudException('This functionality only support Linux Host.');
            // @codeCoverageIgnoreEnd
        }

        $kernal = $this->container->get('kernel');
        if (null === $kernal) {
            // @codeCoverageIgnoreStart
            throw new DrugCrudException('Fail to load Application Kernel.');
            // @codeCoverageIgnoreEnd
        }

        $project_root = $kernal->getProjectDir();

        $command = implode(' ', array(
            PHP_BINDIR . '/php',
            $project_root . '/bin/console',
            $commandWithAttrs,
        ));
        $command = "nohup {$command} >> /dev/null 2>&1 & echo $!";

        $process = Process::fromShellCommandline($command);
        $process->run();

        $pid = $process->getOutput();
        $pid = (int) trim($pid);

        if (empty($checkAfterSec)) {
            return $this->_getProcessStateById($pid);
        }

        // @codeCoverageIgnoreStart
        // wait for 0.2s by default
        $wait = (int) ($checkAfterSec * 1000000);
        usleep($wait);

        $status = $this->_getProcessStateById($pid);
        // This this not tesable, based on enviroment, traffic & etc
        if (!empty($status)) {
            /*
            @source: https://idea.popcount.org/2012-12-11-linux-process-states/

            R: running / runnable:: newly called
                 - state is not ready yet
                 - at this state we not sure if it run yet

            D: uninterrupted sleep (usually IO):: loading still
                 - still determine state
                 - at this state we not sure if it run yet

            S: interruptible sleep (waiting for an event to complete)
                 - this meant it's processing
            */
            $stat_char = substr($status['STAT'], 0, 1);
            // if the process still loading it; R / D,then RE-RUN
            if (false !== stripos('RD', $stat_char)) {
                // retry several times based on given time
                for ($i = 0; $i < 5; ++$i) {
                    usleep($wait);
                    $status = $this->_getProcessStateById($pid);

                    if (empty($status)) {
                        // masking with Z exit state
                        $stat_char = 'Z';
                        break; // for
                    }

                    $stat_char = substr($status['STAT'], 0, 1);

                    // finally have different state
                    if (false === stripos('RD', $stat_char)) {
                        break; // for
                    }
                }

                // D is still acceptable, so it go
                if (false !== stripos('DS', $stat_char)) {
                    return $status;
                }
            }
            elseif (false !== stripos('RDS', $stat_char)) {
                return $status;
            }
        }

        return array();
        // @codeCoverageIgnoreEnd
    }

    /**
     * Method to realign all Entity entity manually.
     * Since clone's ID is just on memory, and private.
     * However this only covers main (top-layer) entity.
     *
     * @codeCoverageIgnore Not testable at this point
     *
     * @param DrugTypeFamily       $drug
     * @param DrugTypeFamily|null  $clone
     * @param bool                 $new
     */
    private function _revertDrugEntity(DrugTypeFamily $drug, ?DrugTypeFamily $clone = null, bool $new): void
    {
        $done = $new;

        if ($new) {
            $this->em->remove($drug);
        }
        elseif (!empty($clone)) {
            $drug->setName( $clone->getName() );
            $drug->setValid( $clone->getValid() );
            $drug->setUid( $clone->getUid() );
            $drug->setComments( $clone->getComments() );
            $drug->setCreationDate( $clone->getCreationDate() );

            if ($done = ($drug instanceof Experimental)) {
                $drug->setType( $clone->getType() );
            }
            elseif ($done = ($drug instanceof Generic)) {
                $drug->setLevel1( $clone->getLevel1() );
                $drug->setLevel2( $clone->getLevel2() );
                $drug->setLevel3( $clone->getLevel3() );
                $drug->setLevel4( $clone->getLevel4() );
                $drug->setLevel5( $clone->getLevel5() );
                $drug->setNodoublebounce( $clone->getNodoublebounce() );
                $drug->setAutoencodeexclude( $clone->getAutoencodeexclude() );
                $drug->setExperimental( $clone->getExperimental() );
            }
            elseif ($done = ($drug instanceof Brand)) {
                $drug->setAutoencodeexclude( $clone->getAutoencodeexclude() );
                $drug->setGeneric( $clone->getGeneric() );
            }

            if ($done) {
                $this->em->persist($drug);
            }
        }

        if ($done) {
            $this->em->flush($drug);
        }
    }

    /**
     * Method to get corresponding Repository to Drug Entity.
     *
     * @param string  $entity  Expects 'experimental', 'generic', 'brand'
     *
     * @return ObjectRepository
     */
    private function _getDrugRepo($entity): ObjectRepository
    {
        $entity = strtolower($entity);

        switch ($entity) {
            case 'generic':
                $repo = $this->em->getRepository(Generic::class);
                break;

            case 'brand':
                $repo = $this->em->getRepository(Brand::class);
                break;

            case 'experimental':
            default:
                $repo = $this->em->getRepository(Experimental::class);
                break;
        }

        return $repo;
    }

    /**
     * Method to process Generic/Brand Synonym New/Edit Form Submission.
     *
     * @param object      $repo     Active Drug Repository
     * @param string      $entity   Expect only 'generic' & 'brand'
     * @param array       $predata
     * @param array|null  $synonyms
     */
    private function _processSynonymsSubmission(&$repo, $entity, &$predata, ?array $synonyms = null): void
    {
        $new    = array();
        $still  = array();
        $remove = array();

        if (!empty($synonyms)) {
            foreach ($synonyms as $synonym) {
                $keyword = 0;
                $name    = '';
                $country = '';

                foreach ($synonym as $syn_key => $syn_val) {
                    if ('country' == $syn_key) {
                        $country = $syn_val;
                    }
                    else {
                        if ('new' != $syn_key) {
                            $keyword = (int) $syn_key;
                        }
                        $name = $syn_val;
                    }
                }

                if (empty($name)) {
                    continue;
                }

                // each-repeater-set
                if (empty($keyword)) {
                    // no id - it's new
                    $new[] = array(
                        'name'    => $name,
                        'country' => $country,
                    );
                }
                elseif (
                    // listed, and given id
                    (!empty($predata['synonyms_keys'])) &&
                    (in_array($keyword, $predata['synonyms_keys']))
                ) {
                    $still[$keyword] = array(
                        'id'      => $keyword,
                        'name'    => $name,
                        'country' => $country,
                    );
                }
            }
        }

        // get one that not listed
        if (!empty($predata['synonyms_keys'])) {
            $still_keys = array_keys($still);
            foreach ($predata['synonyms_keys'] as $synonym_key) {
                if (!in_array($synonym_key, $still_keys)) {
                    $remove[] = $synonym_key;
                }
            }
        }

        // process synonym
        // - adding
        if (!empty($new)) {
            $items = array();

            foreach ($new as $nw) {
                $keyword_obj = $repo->updateSynonym(0, array(
                    'name'    => $nw['name'],
                    'country' => $nw['country'],
                    $entity   => $predata['main'],
                ));
                $predata['synonyms'][$keyword_obj->getId()] = $keyword_obj;

                $items[] = array(
                    'name'    => $keyword_obj->getName(),
                    'country' => $keyword_obj->getCountry(),
                );
            }

            $this->logger->info("CORE: Add {$entity} synonym", array(
                $predata['main']->getId() => $items,
            ));
        }

        // - update
        if (!empty($still)) {
            $items = array();
            foreach ($still as $sl) {
                $predata['synonyms'][$sl['id']] = $repo->updateSynonym($sl['id'], array(
                    'name'    => $sl['name'],
                    'country' => $sl['country'],
                    $entity   => $predata['main'],
                ));

                $items[] = array(
                    'id'      => $predata['synonyms'][$sl['id']]->getId(),
                    'name'    => $predata['synonyms'][$sl['id']]->getName(),
                    'country' => $predata['synonyms'][$sl['id']]->getCountry(),
                );
            }

            $this->logger->info("CORE: Update {$entity} synonym", array(
                $predata['main']->getId() => $items,
            ));
        }

        // - remove
        if (!empty($remove)) {
            $items = array();

            foreach ($remove as $rv) {
                $items[] = array(
                    'id'      => $predata['synonyms'][$rv]->getId(),
                    'name'    => $predata['synonyms'][$rv]->getName(),
                    'country' => $predata['synonyms'][$rv]->getCountry(),
                );

                if (
                    (!empty($predata['synonyms_keys'])) &&
                    (in_array($rv, $predata['synonyms_keys']))
                ) {
                    $keyword_id = $rv;
                    $rv         = $predata['synonyms'][$keyword_id];
                    unset($predata['synonyms'][$keyword_id]);
                }

                $repo->removeSynonym($rv);
            }

            $this->logger->info("CORE: Delete {$entity} synonym", array(
                $predata['main']->getId() => $items,
            ));
        }

        $lotus = $this->lotus->refreshDrugKeywords('synonym', $entity, $predata['main']->getId(), $new, $still);

        $this->logger->info("CORE: {$entity} synonym changes", array(
            'drug-id'   => $predata['main']->getId(),
            'pre-new'   => count($new),
            'pre-still' => count($still),
            'changed'   => $lotus,
        ));
    }

    /**
     * Method to process Generic/Brand Typo New/Edit Form Submission.
     *
     * @param object      $repo     Active Drug Repository
     * @param string      $entity   Expect only 'generic' & 'brand'
     * @param array       $predata
     * @param array|null  $typos
     *
     * @return  void
     */
    private function _processTyposSubmission(&$repo, $entity, &$predata, ?array $typos = null): void
    {
        $new    = array();
        $still  = array();
        $remove = array();

        if (!empty($typos)) {
            foreach ($typos as $typo) {
                $typo = array_map('trim', $typo);

                // basic
                $keyword = 0;
                $name    = '';

                // loop-repeater
                foreach ($typo as $syn_key => $syn_val) {
                    if ('new' != $syn_key) {
                        $keyword = (int) $syn_key;
                    }
                    $name = $syn_val;
                }

                if (empty($name)) {
                    continue;
                }

                // each-repeater-set
                if (empty($keyword)) {
                    // no id - it's new
                    $new[] = array(
                        'name' => $name,
                    );
                }
                elseif (
                    // have listed, and given id
                    (!empty($predata['typos_keys'])) &&
                    (in_array($keyword, $predata['typos_keys']))
                ) {
                    $still[$keyword] = array(
                        'id'   => $keyword,
                        'name' => $name,
                    );
                }
            }
        }

        // get one that not listed
        if (!empty($predata['typos_keys'])) {
            $still_keys = array_keys($still);
            foreach ($predata['typos_keys'] as $typo_key) {
                if (!in_array($typo_key, $still_keys)) {
                    $remove[] = $typo_key;
                }
            }
        }

        // process typo
        // - adding
        if (!empty($new)) {
            $items = array();

            foreach ($new as $nw) {
                $keyword_obj = $repo->updateTypo(0, array(
                    'name'  => $nw['name'],
                    $entity => $predata['main'],
                ));
                $predata['typos'][$keyword_obj->getId()] = $keyword_obj;

                $items[] = array(
                    'name' => $keyword_obj->getName(),
                );
            }

            $this->logger->info("CORE: Add {$entity} typo", array(
                $predata['main']->getId() => $items,
            ));
        }

        // - update
        if (!empty($still)) {
            $items = array();

            foreach ($still as $sl) {
                $predata['typos'][$sl['id']] = $repo->updateTypo($sl['id'], array(
                    'name'  => $sl['name'],
                    $entity => $predata['main'],
                ));

                $items[] = array(
                    'id'   => $predata['typos'][$sl['id']]->getId(),
                    'name' => $predata['typos'][$sl['id']]->getName(),
                );
            }

            $this->logger->info("CORE: Update {$entity} typo", array(
                $predata['main']->getId() => $items,
            ));
        }

        // - remove
        if (!empty($remove)) {
            $items = array();

            foreach ($remove as $rv) {
                $items[] = array(
                    'id'   => $predata['typos'][$rv]->getId(),
                    'name' => $predata['typos'][$rv]->getName(),
                );

                if (
                    (!empty($predata['typos_keys'])) &&
                    (in_array($rv, $predata['typos_keys']))
                ) {
                    $keyword_id = $rv;
                    $rv         = $predata['typos'][$keyword_id];
                    unset($predata['typos'][$keyword_id]);
                }

                $repo->removeTypo($rv);
            }

            $this->logger->info("CORE: Delete {$entity} typo", array(
                $predata['main']->getId() => $items,
            ));
        }

        $lotus = $this->lotus->refreshDrugKeywords('typo', $entity, $predata['main']->getId(), $new, $still);

        $this->logger->info("CORE: {$entity} typo changes", array(
            'drug-id'   => $predata['main']->getId(),
            'pre-new'   => count($new),
            'pre-still' => count($still),
            'changed'   => $lotus,
        ));
    }

    /**
     * Helper Method to Get PS from PID.
     *
     * @param int  $pid
     *
     * @return array
     */
    private function _getProcessStateById($pid): array
    {
        // run on process
        $process = Process::fromShellCommandline("ps u --pid {$pid}");
        $process->run();

        // get output
        $status = $process->getOutput();

        // unpack string
        $status = $this->_unpackTerminalCommandPsResult($status);

        // @codeCoverageIgnoreStart
        if (empty($status)) {
            return array();
        }

        return end($status);
        // @codeCoverageIgnoreEnd
    }

    /**
     * Helper to extract result of PS command.
     *
     * @param string|array  $results
     * @param bool          $noHeaders  Flag if result from --no-headers command
     *
     * @return array
     */
    private function _unpackTerminalCommandPsResult($results = '', $noHeaders = false): array
    {
        if (is_string($results)) {
            // break lines
            $results = explode(PHP_EOL, $results);
            $results = array_map('trim', $results);
            $results = array_filter($results);
        }

        $headers  = array();
        $unpacked = array();
        $comIndex = false;

        foreach ($results as $ri => $result) {
            // break to each cell
            $result = explode(' ', $result);

            $result = array_filter($result, function ($v, $k) {
                return 0 != strlen($v);
            }, ARRAY_FILTER_USE_BOTH);

            if (empty($result)) {
                // @codeCoverageIgnoreStart
                continue;
                // @codeCoverageIgnoreEnd
            }

            $result = array_values($result);

            if ($noHeaders) {
                // @codeCoverageIgnoreStart
                $unpacked[] = $result;
                // @codeCoverageIgnoreEnd
            }
            else {
                if (0 == $ri) {
                    // this is headers
                    $headers  = $result;
                    $comIndex = array_search('COMMAND', $headers);
                    if (false === $comIndex) {
                        // force overwrite this
                        $noHeaders = true;
                    }
                }
                else {
                    // merging back commands
                    $res_command  = array();
                    $count_before = count($result);
                    for ($i = $comIndex; $i < $count_before; ++$i) {
                        $res_command[] = $result[$i];
                        unset($result[$i]);
                    }
                    $result[$comIndex] = implode(' ', $res_command);

                    $unpacked[] = array_combine($headers, $result);
                }
            }
        }

        return $unpacked;
    }

    /**
     * Helper method to Check OS Version.
     *
     * @codeCoverageIgnore
     *
     * @return boolean
     */
    private function _isThisLinuxFamily(): bool
    {
        static $linux = null;

        if (null === $linux) {
            if (
                // php > 7.2
                (defined('PHP_OS_FAMILY')) &&
                (PHP_OS_FAMILY == 'Windows')
            ) {
                $linux = false;
            }
            elseif ('WIN' === strtoupper(substr(PHP_OS, 0, 3))) {
                $linux = false;
            }
            else {
                $linux = true;
            }
        }

        return $linux;
    }
}
