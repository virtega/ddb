<?php

namespace App\Service;

use App\Entity\Experimental;
use App\Exception\DrugCrudException;
use App\Service\Factory\SearchServiceFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Service: Search.
 *
 * @since  1.1.0
 */
class SearchService extends SearchServiceFactory
{
    /**
     * @var \Doctrine\ORM\EntityManager
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param EntityManagerInterface  $em
     * @param LoggerInterface         $logger
     * @param int                     $searchPgLimit
     * @param int                     $searchPgSubLimit
     * @param int                     $searchAcLimit
     * @param int                     $searchAcSubLimit
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, int $searchPgLimit, int $searchPgSubLimit, int $searchAcLimit, int $searchAcSubLimit)
    {
        $this->em     = $em;
        $this->logger = $logger;

        parent::__construct($searchPgLimit, $searchPgSubLimit, $searchAcLimit, $searchAcSubLimit);
    }

    /**
     * Method to kick off Search process.
     *
     * @see Returning value structure App\Repository\DrugRepository::queryDrugSearch
     *
     * @throws DrugCrudException
     *
     * @return array
     */
    public function execute(): array
    {
        $result = array();
        $error  = false;

        try {
            // validate input
            if (false == $this->hasGivenList()) {
                if (empty($this->getTerm())) {
                    throw new DrugCrudException('Missing search query.');
                }

                if (!in_array($this->getType(), \App\Entity\Family\DrugTypeFamily::$mainDrugsAny)) {
                    throw new DrugCrudException("Unsupported Drug-Type: '{$this->getType()}'.");
                }
            }

            /**
             * @var \App\Repository\DrugRepository<Experimental>
             */
            $repo = $this->em->getRepository(Experimental::class);

            // query now
            $result = $repo->queryDrugSearch($this);
        } catch (\Exception $e) {
            $error = $e;
        }

        $this->logger->info(__METHOD__, array(
            'search' => $this->getDetails(),
            'error'  => $error,
            'result' => $result,
        ));

        if ($error instanceof \Exception) {
            throw $error;
        }

        return $result;
    }
}
