<?php
namespace App\Service;

use App\Utility\ESQueryBuilder;
use Elasticsearch\Client;
use Psr\Log\LoggerInterface;

/**
 * Service: ESDG
 */
class ESDGService
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var Client
     */
    private $client;
    /**
     * @var ESQueryBuilder
     */
    private $ESQB;
    /**
     * @var int
     */
    private $ESMaxResults;

    /**
     * ESDGService constructor.
     *
     * @param LoggerInterface $logger
     * @param Client $client
     * @param string $ESIndex
     * @param int $ESMaxResults
     * @param int $FBXDaysSearch
     */
    public function __construct(LoggerInterface $logger, Client $client, string $ESIndex, int $ESMaxResults, int $FBXDaysSearch)
    {
        $this->logger = $logger;
        $this->client = $client;
        $this->ESQB = new ESQueryBuilder($ESIndex, $FBXDaysSearch);
        $this->ESMaxResults = $ESMaxResults;
    }

    /**
     * @return array
     *
     * @throws \Exception
     */
    public function getPapersOnly(): array
    {
        $ESquery = $this->ESQB->buildPapersOnlyQuery();
        $ESresponse = $this->client->search($ESquery);
        $data = $this->parsePapersOnly($ESresponse);

        return $data;
    }

    /**
     * @param string $date
     *
     * @return array
     *
     * @throws \Exception
     */
    public function searchPapersOnlyByDate(string $date): array
    {
        $return['data'] = '';
        $return['message'] = '';
        $ESquery = $this->ESQB->buildPapersOnlyQuery($date);
        $ESresponse = $this->client->search($ESquery);
        $count = $this->getESresultCount($ESresponse);
        if ($count > $this->ESMaxResults) {
            $count = $this->ESMaxResults;
            $return['message'] = 'Results size is too large, displaying only ' . $count . ' records';
        }
        $ESquery = $this->ESQB->buildPapersOnlyQuery($date, $count);
        $ESresponse = $this->client->search($ESquery);
        $return['data'] = $this->parsePapersOnly($ESresponse);

        return $return;
    }

    /**
     * @param array $ESResponse
     *
     * @return array
     */
    public function parsePapersOnly(array $ESResponse): array
    {
        $list = [];
        if (!isset($ESResponse['hits']['hits'])) {
            return $list;
        }

        foreach ($ESResponse['hits']['hits'] as $hit) {
            $article['title'] = $hit['_source']['title'];
            $article['status'] = $hit['_source']['status'];
            $article['updated'] = date('Y-m-d H:i:s', $hit['_source']['updated']);
            $list[] = $article;
        }

        return $list;
    }

    /**
     * @return array
     *
     * @throws \Exception
     */
    public function getTimeStamp(): array {
        $ESquery = $this->ESQB->buildTimeStampQuery();
        $ESresponse = $this->client->search($ESquery);
        $data = $this->parseTimeStamp($ESresponse);

        return $data;
    }

    /**
     * @param string $date
     *
     * @return array
     *
     * @throws \Exception
     */
    public function searchTimeStampByDate(string $date): array
    {
        $return['data'] = '';
        $return['message'] = '';
        $ESquery = $this->ESQB->buildTimeStampQuery($date);
        $ESresponse = $this->client->search($ESquery);
        $count = $this->getESresultCount($ESresponse);
        if ($count > $this->ESMaxResults) {
            $count = $this->ESMaxResults;
            $return['message'] = 'Results size is too large, displaying only ' . $count . ' records';
        }
        $ESquery = $this->ESQB->buildTimeStampQuery($date, $count);
        $ESresponse = $this->client->search($ESquery);
        $return['data'] = $this->parseTimeStamp($ESresponse);

        return $return;
    }

    /**
     * @param array $ESResponse
     *
     * @return array
     */
    public function parseTimeStamp(array $ESResponse): array
    {
        $list = [];
        if (!isset($ESResponse['hits']['hits'])) {
            return $list;
        }

        foreach ($ESResponse['hits']['hits'] as $hit) {
            $article['title'] = $hit['_source']['title'];
            $article['type'] = $hit['_source']['type'];
            $article['updated'] = date('Y-m-d H:i:s', $hit['_source']['updated']);
            $list[] = $article;
        }

        return $list;
    }

    /**
     * @param string $date
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getAbstractsReadOnly(string $date): array
    {
        $return['data'] = '';
        $return['message'] = '';
        $ESquery = $this->ESQB->buildAbstractCountsESQuery($date);
        $ESresponse = $this->client->search($ESquery);
        $count = $this->getESresultCount($ESresponse);
        if ($count > $this->ESMaxResults) {
            $count = $this->ESMaxResults;
            $return['message'] = 'Results size is too large, displaying only ' . $count . ' records';
        }
        $ESquery = $this->ESQB->buildAbstractCountsESQuery($date, true, $count);
        $ESresponse = $this->client->search($ESquery);
        $return['data'] = $this->parseAbstractsReadOnly($date, $ESresponse);

        return $return;
    }

    /**
     * @param string $date
     *
     * @return array
     *
     * @throws \Exception
     */
    public function searchAbstractsReadOnly(string $date): array
    {
        $return['data'] = '';
        $return['message'] = '';
        $ESquery = $this->ESQB->buildAbstractCountsESQuery($date);
        $ESresponse = $this->client->search($ESquery);
        $count = $this->getESresultCount($ESresponse);
        if ($count > $this->ESMaxResults) {
            $count = $this->ESMaxResults;
            $return['message'] = 'Results size is too large, displaying only ' . $count . ' records';
        }
        $ESquery = $this->ESQB->buildAbstractCountsESQuery($date, true, $count);
        $ESresponse = $this->client->search($ESquery);
        $return['data'] = $this->parseAbstractsReadOnly($date, $ESresponse);

        return $return;
    }

    /**
     * @param string $date
     * @param array $ESResponse
     *
     * @return array
     */
    public function parseAbstractsReadOnly(string $date, array $ESResponse): array
    {
        $ids = [];
        if (!isset($ESResponse['hits']['hits']) || empty($ESResponse['hits']['hits'])) {
            return $ids;
        }

        foreach ($ESResponse['hits']['hits'] as $hit) {
            $ids[] = ['id' => $hit['_source']['id'], 'title' => $hit['_source']['title'], 'updated' => $date];
        }

        return $ids;
    }

    /**
     * @param array $ESresponse
     *
     * @return int
     */
    public function getESresultCount(array $ESresponse): int
    {
        return (isset($ESresponse['hits']['total']) && !empty($ESresponse['hits']['total'])) ? $ESresponse['hits']['total'] : 0;
    }

    /**
     * @param array $result
     *
     * @return array
     */
    public function parseDGMonitorStats(array $result): array
    {
        $stats = [];
        foreach ($result as $data) {
            $stats[$data['date']->format('Y-m-d')][] = $data['title'];
        }

        return $stats;
    }

    /**
     * @param string $date
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getFBArticles(string $date = ''): array
    {
        $ESquery = $this->ESQB->buildFBArticlesQuery($date);
        $ESresponse = $this->client->search($ESquery);
        $count = $this->getESresultCount($ESresponse);
        if ($count > $this->ESMaxResults) {
            $count = $this->ESMaxResults;
        }
        $ESquery = $this->ESQB->buildFBArticlesQuery($date, $count);
        $ESresponse = $this->client->search($ESquery);
        $data = $this->parseFBArticles($ESresponse);

        return $data;
    }

    /**
     * @param array $ESResponse
     *
     * @return array
     */
    public function parseFBArticles(array $ESResponse): array
    {
        $list = [];
        if (!isset($ESResponse['hits']['hits'])) {
            return $list;
        }

        foreach ($ESResponse['hits']['hits'] as $hit) {
            $article['id'] = $hit['_source']['id'];
            $article['title'] = $hit['_source']['title'];
            $article['status'] = $hit['_source']['status'];
            $article['updated'] = date('Y-m-d H:i:s', $hit['_source']['updated']);
            $article['type'] = $hit['_source']['type'];
            $list[] = $article;
        }

        return $list;
    }

    /**
     * @param string $date
     *
     * @return array
     *
     * @throws \Exception
     */
    public function searchFBArticlesByDate(string $date): array
    {
        $return['data'] = '';
        $return['message'] = '';
        $ESquery = $this->ESQB->buildFBArticlesQuery($date);
        $ESresponse = $this->client->search($ESquery);
        $count = $this->getESresultCount($ESresponse);
        if ($count > $this->ESMaxResults) {
            $count = $this->ESMaxResults;
            $return['message'] = 'Results size is too large, displaying only ' . $count . ' records';
        }
        $ESquery = $this->ESQB->buildFBArticlesQuery($date, $count);
        $ESresponse = $this->client->search($ESquery);
        $return['data'] = $this->parseFBArticles($ESresponse);

        return $return;
    }

    /**
     * @param string $date
     *
     * @return array
     *
     * @throws \Exception
     */
    public function searchFBArticlesLastXDays(string $date): array
    {
        $return['data'] = '';
        $return['message'] = '';
        $ESquery = $this->ESQB->buildFBArticlesQuery($date, 0, true);
        $ESresponse = $this->client->search($ESquery);
        $count = $this->getESresultCount($ESresponse);
        if ($count > $this->ESMaxResults) {
            $count = $this->ESMaxResults;
            $return['message'] = 'Results size is too large, displaying only ' . $count . ' records';
        }
        $ESquery = $this->ESQB->buildFBArticlesQuery($date, $count, true);
        $ESresponse = $this->client->search($ESquery);
        $return['data'] = $this->parseFBArticles($ESresponse);

        return $return;
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function getFBArticleById(int $id): array {
        $ESquery = $this->ESQB->buildFBGetArticleByIdQuery($id);
        $ESresponse = $this->client->search($ESquery);

        return $this->parseFBArticle($ESresponse);
    }

    /**
     * @param array $ESresponse
     *
     * @return array
     */
    public function parseFBArticle(array $ESresponse): array {
        $article = [];
        $articleParsed = [];
        $fields = [
            'title',
            'body',
            'type',
            'status',
            'lima_parser',
            'human_url',
            'md5_human_url',
            'score'
        ];

        if (isset($ESresponse['hits']['hits']) && count($ESresponse['hits']['hits'])) {
            $article = $ESresponse['hits']['hits'][0]['_source'];
            $articleParsed['publication_date'] = date('F j, Y - H:i:s', $article['publication_date']);
            $articleParsed['created'] = date('F j, Y - H:i:s', $article['created']);
            $articleParsed['updated'] = date('F j, Y - H:i:s', $article['updated']);

            $articleParsed['extra_fields'] = [];
            if (isset($article['extra_fields']) && count($article['extra_fields'])) {
                foreach ($article['extra_fields'] as $map => $value) {
                    $key = $this->snakeToCamelCase($map);
                    $data = @unserialize($value);
                    if ($data !== false) {
                        $articleParsed['extra_fields'][$key] = '<div class="mt-2"><pre class="code-block">' . json_encode($data, JSON_PRETTY_PRINT) . '</pre></div>';
                    } else {
                        if (strpos($key, 'Is') !== false && $key !== 'Issn') {
                            $key .= '?';
                        }
                        if ($article['extra_fields'][$map] == '1' || $article['extra_fields'][$map] == '0') {
                            $articleParsed['extra_fields'][$key] = (int)$article['extra_fields'][$map];
                        } else {
                            $articleParsed['extra_fields'][$key] = isset($article['extra_fields'][$map]) && !empty($article['extra_fields'][$map]) ? $value : '';
                        }
                    }
                }
            }

            if (isset($article['tags']) && count($article['tags'])) {
                foreach ($article['tags'] as $category => $tags) {
                    $key = $this->snakeToCamelCase($category);
                    if ($category == 'external_ids') {
                        foreach ($article['tags']['external_ids'] as $type => $id) {
                            $articleParsed['external_ids'][$type][] = $id;
                        }
                    } else {
                        foreach ($tags as $tag) {
                            $articleParsed['tags'][$key][] = $tag;
                        }
                    }
                }
                if (isset($articleParsed['tags']['All'])) {
                    unset($articleParsed['tags']['All']);
                }
            }

            foreach ($fields as $map => $field) {
                $articleParsed[$field] = isset($article[$field]) && !empty($article[$field]) ? $article[$field] : '';
            }
        }

        return $articleParsed;
    }

    /**
     * @param $str
     *
     * @return string
     */
    private function snakeToCamelCase(string $str): string {
        $key = explode('_', $str);
        $key = array_map('ucwords', $key);
        $key = implode(' ', $key);
        $key = ltrim(rtrim($key, ' '), ' ');

        return $key;
    }
}
