<?php

namespace App\Service;

use App\Entity\Experimental;
use App\Entity\Family\DrugTypeFamily;
use App\Exception\DrugCrudException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service: MapService.
 *
 * @since  1.0.0
 */
class MapService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var CoreService
     */
    private $cs;

    /**
     * @var \App\Repository\DrugRepository<Experimental>
     */
    private $repo;

    /**
     * Container.
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ContainerInterface      $container
     * @param CoreService             $cs
     * @param EntityManagerInterface  $em
     * @param LoggerInterface         $logger
     */
    public function __construct(ContainerInterface $container, CoreService $cs, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->container = $container;
        $this->cs        = $cs;
        $this->repo      = $em->getRepository(Experimental::class);
        $this->logger    = $logger;
    }

    /**
     * Method to generate output for Map data.
     *
     * @param string  $entity
     * @param int     $entityId
     * @param array   $mapped    Initial data to jump-index number - OPERATOR positioning
     *
     * @throws DrugCrudException  Unknown Drug Type
     *
     * @return array
     */
    public function getDrugFamilyForMap(string $entity, int $entityId, array $mapped = array()): array
    {
        $this->logger->info('MAP: Get Drug Family', array(
            'entity' => $entity,
            'id'     => $entityId,
            'mapped' => $mapped,
        ));

        if (!in_array($entity, DrugTypeFamily::$mainDrugs)) {
            throw new DrugCrudException("Unknown Drug Type: '{$entity}'.");
        }

        // format mapped
        $mapped = array_map('intval', $mapped);

        // base result
        $mapdata = array(
            'operators' => array(),
            'links'     => array(),
        );

        // query family members
        $family = $this->repo->queryDrugFamily($entity, $entityId, false, true);
        // to be safe, this is impossible on this implementation
        // @codeCoverageIgnoreStart
        if (empty($family)) {
            return array();
        }
        // @codeCoverageIgnoreEnd

        foreach ($family['experimental'] as $ei => $experimental) {
            // up index
            if (!empty($mapped['experimental'])) {
                $ei += $mapped['experimental'];
            }

            $counts = array(
                'out' => (count($family['generic'])),
            );

            // operator
            $operator = $this->_prepMapOperatorData('experimental', $experimental['main'], $counts, $ei, array(), array());

            // assign operator
            $mapdata['operators'][$operator[0]] = $operator[1];
            // no-link
        }

        foreach ($family['generic'] as $ei => $generic) {
            // up index
            if (!empty($mapped['generic'])) {
                $ei += $mapped['generic'];
            }

            $counts = array(
                'in'  => (count($family['experimental'])),
                'out' => (count($family['brand'])),
            );

            // operator
            $operator = $this->_prepMapOperatorData('generic', $generic['main'], $counts, $ei, $generic['synonyms'], $generic['typos']);

            // assign operator
            $mapdata['operators'][$operator[0]] = $operator[1];

            // link
            $link = $this->_prepMapLinkData('generic', $generic['main']);
            // skip
            if (empty($link)) {
                // @codeCoverageIgnoreStart
                continue;
                // @codeCoverageIgnoreEnd
            }

            // assign link
            $mapdata['links'][$link[0]] = $link[1];
        }

        foreach ($family['brand'] as $ei => $brand) {
            // up index
            if (!empty($mapped['brand'])) {
                $ei += $mapped['brand'];
            }

            $counts = array(
                'in'  => (count($family['generic'])),
            );

            // operator
            $operator = $this->_prepMapOperatorData('brand', $brand['main'], $counts, $ei, $brand['synonyms'], $brand['typos']);

            // assign operator
            $mapdata['operators'][$operator[0]] = $operator[1];

            // link
            $link = $this->_prepMapLinkData('brand', $brand['main']);
            // skip
            if (empty($link)) {
                // @codeCoverageIgnoreStart
                continue;
                // @codeCoverageIgnoreEnd
            }
            // assign link
            $mapdata['links'][$link[0]] = $link[1];
        }

        return $mapdata;
    }

    /**
     * Method to Add / Remove Relations between Drugs.
     *
     * @param array  $operators  List of Drug in Maps Object format
     * @param array  $links      List of Relation in Maps Object format
     *
     * @throws DrugCrudException    Missing argument, and invalid format
     *
     * @return int  Number of Drug which effected by the changes
     */
    public function updateDrugRelations(array $operators, array $links = array()): int
    {
        $this->logger->info('MAP: Update Drug Relation', array(
            'operator' => count($operators),
            'links'    => count($links),
        ));

        // validate format
        $opt = array_keys($operators);
        $opt = $opt[0]; // first key

        if (
            (empty($operators[$opt]['properties'])) ||
            (empty($operators[$opt]['drug']))
        ) {
            throw new DrugCrudException('Invalid "operators" Format; missing either "property" or "drug" property.');
        }

        $changes = 0;

        foreach ($operators as $operator_key => $operators_data) {
            if (in_array($operators_data['drug']['type'], array('generic', 'brand'))) {
                $relation = false;

                // check on LINKS
                foreach ($links as $link_key => $link_data) {
                    if ($link_data['toOperator'] == $operator_key) {
                        $relation = true;

                        $this->cs->updateDrugRelation(
                            $operators_data['drug']['type'],
                            $operators_data['drug']['id'],
                            $operators[$link_data['fromOperator']]['drug']['type'],
                            $operators[$link_data['fromOperator']]['drug']['id']
                        );
                        ++$changes;

                        break; // foreach $links, there only one relations
                    }
                }

                // no relation found
                if (!$relation) {
                    $this->cs->updateDrugRelation(
                        $operators_data['drug']['type'],
                        $operators_data['drug']['id'],
                        false,
                        false
                    );
                    ++$changes;
                }
            }
        }

        return $changes;
    }

    /**
     * Map use: To generate OPERATOR (BOX) id & data.
     *
     * @param string          $type      Expects `experimental`, `generic`, `brand`
     * @param DrugTypeFamily  $entity    Entity expects Experimental, Generic, Brand active object
     * @param array           $counts    int value on "in" & "out"
     * @param int             $index     in data-loop, for positioning
     * @param array           $synonyms  Indexed array of entity's Synonym object
     * @param array           $typos     Indexed array of entity's Typo object
     *
     * @return array  [0] string - operator_id, [1] array - operator data
     */
    private function _prepMapOperatorData(string $type, DrugTypeFamily $entity, array $counts, int $index, array $synonyms = array(), array $typos = array()): array
    {
        /**
         * @var \Symfony\Component\Routing\Generator\UrlGenerator
         */
        $router = $this->container->get('router');

        // basic
        $stud = substr($type, 0, 1);
        $id   = $entity->getId();

        // format operator id
        $operator_id = "{$stud}_{$id}";

        // input / output
        // + calculate operator position top/left; in px
        $left = $top = 0;
        $inputs = $outputs = null;
        switch ($type) {
            case 'experimental':
                $inputs  = null;
                $outputs = array(
                    "o_{$stud}_{$id}" => array(
                        'label'    => 'G: ' . ($counts['out'] ?? 0),
                        'multiple' => false,
                    ),
                );

                $top  = (20 + ($index * 80));
                $left = 20;
                break;

            case 'generic':
                $inputs = array(
                    "i_{$stud}_{$id}" => array(
                        'label'    => 'E: ' . ($counts['in'] ?? 0),
                        'multiple' => false,
                    ),
                );
                $outputs = array(
                    "o_{$stud}_{$id}" => array(
                        'label'    => 'B: ' . ($counts['out'] ?? 0),
                        'multiple' => false,
                    ),
                );

                $top  = (20 + ($index * 80));
                $left = 330;
                break;

            case 'brand':
                $inputs = array(
                    "i_{$stud}_{$id}" => array(
                        'label'    => 'G: ' .  ($counts['in'] ?? 0),
                        'multiple' => false,
                    ),
                );
                $outputs = null;

                $top  = (20 + ($index * 80));
                $left = 660;
                break;
        }

        // format operator data
        $operator_data = array(
            'top'        => $top,
            'left'       => $left,
            'properties' => array(
                'title'   => strtoupper($stud) . ': ' . $entity->getName(),
                'class'   => $type,
                'inputs'  => $inputs,
                'outputs' => $outputs,
            ),
            // additional data appended fro drugdb js
            'drug' => array(
                'id'   => $entity->getId(),
                'name' => $entity->getName(),
                'type' => $type,
            ),
        );

        return array($operator_id, $operator_data);
    }

    /**
     * Map use: To generate LINK (LINE) id & data.
     *
     * @param string          $type    Expects `generic`, `brand`
     * @param DrugTypeFamily  $entity  Entity expects Generic, Brand active object
     *
     * @return array    [0] string - operator_id, [1] array - operator data
     */
    private function _prepMapLinkData(string $type, DrugTypeFamily $entity): array
    {
        // basic
        $stud = substr($type, 0, 1);
        $id   = $entity->getId();

        $ent_query = $ext_stud = false;
        switch ($type) {
            case 'generic':
                $ext_stud  = 'e';
                $ent_query = 'getExperimental';
                break;

            case 'brand':
                $ext_stud  = 'g';
                $ent_query = 'getGeneric';
                break;
        }

        // validate if relationship is "physical" .. erm..
        if (
            (!empty($ent_query)) &&
            ($entity->$ent_query()) &&
            ($extid = $entity->$ent_query()->getId())
        ) {
            // format link id
            // (from->to)_(from->to)
            $link_id = "{$ext_stud}_{$stud}_{$extid}_{$id}";

            $link_data = array(
                // from
                'fromOperator'  => "{$ext_stud}_{$extid}",
                'fromConnector' => "o_{$ext_stud}_{$extid}",
                // to
                'toOperator'  => "{$stud}_{$id}",
                'toConnector' => "i_{$stud}_{$id}",
            );

            return array($link_id, $link_data);
        }

        // @codeCoverageIgnoreStart
        return array();
        // @codeCoverageIgnoreEnd
    }
}
