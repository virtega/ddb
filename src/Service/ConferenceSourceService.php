<?php

namespace App\Service;

use App\Entity\Conference;
use App\Exception\RemoteSyncException;
use App\Utility\MsSqlRemoteConnection;
use Psr\Log\LoggerInterface;

/**
 * Service: Conference Source Service.
 * Process Query & Sync process for Conference remote source.
 *
 * Exception in this class rather a safe-net, as PDO from MsSqlRemoteConnection class in on PDO::ERRMODE_EXCEPTION
 *
 * Source:
 * - Table has PK but did not have declared as IDENTITY, so PDO::lastInsertId() return false.
 * - Conference might only have on EITHER of the Table - uneven
 *
 * @since  1.2.0
 */
class ConferenceSourceService
{
    use \App\Service\Traits\SourceDataConverterServiceTrait;
    use \App\Service\Traits\SqlQueryBuildHelperServiceTrait;

    /**
     * @see services "app_remote_mssql.conference"
     *
     * @var MsSqlRemoteConnection
     */
    public $remote;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var boolean
     */
    private $upSync;

    /**
     * @var string  Remote SQL Server DB Schema
     */
    private $dbSchema = 'dbo';

    /**
     * @var string
     */
    private $tblPrimary;

    /**
     * @var string
     */
    private $tblSecondary;

    /**
     * Last Query Error Info
     *
     * @var array
     */
    private $lastQueryErrorInfo;

    /**
     * @param MsSqlRemoteConnection  $remote
     * @param LoggerInterface        $logger
     * @param boolean                $upSync
     * @param string                 $dbSchema
     * @param string                 $tblPrimary
     * @param string                 $tblSecondary
     */
    public function __construct(MsSqlRemoteConnection $remote, LoggerInterface $logger, bool $upSync, string $dbSchema, string $tblPrimary, string $tblSecondary)
    {
        $this->remote       = $remote;
        $this->logger       = $logger;
        $this->upSync       = $upSync;
        $this->dbSchema     = $dbSchema;
        $this->tblPrimary   = $tblPrimary;
        $this->tblSecondary = $tblSecondary;
    }

    /**
     * @return boolean
     */
    public function isUpSync(): bool
    {

        return $this->upSync;
    }

    /**
     * Method to return Main Conference table name; CRC
     *
     * @param  boolean $withSchema  Return with Schema prefix
     * @param  boolean $withQoute   Wrap table name & schema (if any) with double quote
     *
     * @return string
     */
    public function getPrimaryTableName(bool $withSchema = true, bool $withQoute = true): string
    {

        return $this->__tableNameFormatter($this->tblPrimary, ($withSchema ? $this->dbSchema : null), $withQoute);
    }

    /**
     * Method to return Conference-Details table name; CRC_Details
     *
     * @param  boolean $withSchema  Return with Schema prefix
     * @param  boolean $withQoute   Wrap table name & schema (if any) with double quote
     *
     * @return string
     */
    public function getSecondaryTableName(bool $withSchema = true, bool $withQoute = true): string
    {

        return $this->__tableNameFormatter($this->tblSecondary, ($withSchema ? $this->dbSchema : null), $withQoute);
    }

    /**
     * Method to up-sync conference.
     * Local ID is not needed, instead this take the generated CongressID as previous_id.
     * So this conference may not have ID - new before persist / flush.
     *
     * @param Conference $conference  Note by reference
     *
     * @throws RemoteSyncException  If up-sync process failed;
     *                              - \PDOException - Query / Permission
     *                              - App\Exception\MsSqlRemoteConnectionException - Connection / Permission
     *
     * @return  boolean if upsync is made
     */
    public function upsync(Conference &$conference): bool
    {
        $processed = false;

        if (false == $this->isUpSync()) {
            return $processed;
        }

        try {
            $this->remote->makeConnection(true);

            // uneven
            if (empty($conference->getPreviousId())) {
                $process = 'insert';
                $this->_generateNewConference($conference);
            }
            else {
                $process = 'update';
                $this->_createNewConferenceOnTable($this->getPrimaryTableName(), $conference->getPreviousId(), false);
                $this->_createNewConferenceOnTable($this->getSecondaryTableName(), $conference->getPreviousId(), false);
            }

            $processed = $this->_updateConference($conference);

            $this->logger->info('Remote Conference UpSync Complete', array(
                // expected string 'insert' / 'update'
                'P' => $process,
                // expected true
                'D' => $processed,
                // expects STRING - CongressID
                'I' => $conference->getPreviousId(),
                // expect ARRAY ["00000", null, null]
                'E' => $this->lastQueryErrorInfo,
            ));
        }
        catch (\Exception $e) {
            throw new RemoteSyncException($e, $this->logger);
        }

        return $processed;
    }

    /**
     * Method to remove conference.
     *
     * @param Conference $conference
     *
     * @throws RemoteSyncException  If up-sync process failed;
     *                              - \PDOException - Query / Permission
     *                              - App\Exception\MsSqlRemoteConnectionException - Connection / Permission
     *
     * @return  boolean if upsync is made
     */
    public function remove(Conference $conference): bool
    {
        if (false == $this->isUpSync()) {
            return false;
        }

        if (empty($conference->getPreviousId())) {
            throw new RemoteSyncException(new \Exception('Conference has no record of Previous Site ID.'), $this->logger);
        }

        try {
            $this->remote->makeConnection(true);
            $conn = $this->remote->getConnection();

            $tableName = $this->getSecondaryTableName();
            $statement = $conn->prepare("DELETE FROM {$tableName} WHERE CongressID = :CongressID;");
            $statement->bindValue(':CongressID', $conference->getPreviousId(), \PDO::PARAM_STR);
            $processedSecondary = $statement->execute();
            $this->lastQueryErrorInfo = $conn->errorInfo();

            $tableName = $this->getPrimaryTableName();
            $statement = $conn->prepare("DELETE FROM {$tableName} WHERE CongressID = :CongressID;");
            $statement->bindValue(':CongressID', $conference->getPreviousId(), \PDO::PARAM_STR);
            $processedPrimary = $statement->execute();
            $this->lastQueryErrorInfo = $conn->errorInfo();

            $this->logger->info('Remote Conference Removal Complete', array(
                // expected string
                'P' => 'delete',
                // expected true
                'primary' => $processedPrimary,
                // expected true
                'secondary' => $processedSecondary,
                // expects INT - id
                'I' => $conference->getId(),
                // expects STRING - CongressID
                'V' => $conference->getPreviousId(),
                // expect ARRAY ["00000", null, null]
                'E' => $this->lastQueryErrorInfo,
            ));
        }
        catch (\Exception $e) {
            throw new RemoteSyncException($e, $this->logger);
        }

        return ($processedPrimary && $processedSecondary);
    }

    /**
     * Submit INSERT command with NEWID() function, add to Conference $previosId.
     *
     * @param Conference $conference
     *
     * @throws \Exception  If ID generation failed
     *
     * @return boolean
     */
    private function _generateNewConference(Conference &$conference): bool
    {
        $done = false;

        $conn = $this->remote->getConnection();

        $uniqueName = implode('-', [
            'DrugDbTemporaryIDENTIFIER',
            md5( microtime(true) . rand() ),
            rand(0, 1000),
            microtime(true),
        ]);

        do {
            // primary table
            $tableName = $this->getPrimaryTableName();
            $statement = $conn->prepare("INSERT INTO {$tableName} (CongressID, Name) VALUES (CONVERT(VARCHAR(32), REPLACE(NEWID(),'-','')), :Name);");
            $statement->bindValue(':Name', $uniqueName, \PDO::PARAM_STR);
            $processed = $statement->execute();
            $this->lastQueryErrorInfo = $conn->errorInfo();

            if (!$processed) {
                break;
            }

            $statement = $conn->prepare("SELECT CongressID FROM {$tableName} WHERE Name = :Name;");
            $statement->bindValue(':Name', $uniqueName, \PDO::PARAM_STR);
            $processed = $statement->execute();
            $new_id = $statement->fetchColumn();
            $this->lastQueryErrorInfo = $conn->errorInfo();

            if (
                (!$processed) ||
                (empty($new_id))
            ) {
                break;
            }

            // secondary table
            $processed = $this->_createNewConferenceOnTable($this->getSecondaryTableName(), $new_id, true);

            if (empty($processed)) {
                break;
            }

            $conference->setPreviousId($new_id);
            $done = true;
        } while(false);

        if (false == $done) {
            throw new \Exception("Fail to Generate new ID: {$uniqueName}");
        }

        return $done;
    }

    /**
     * Create new record with just an ID on give table
     *
     * @param string   $tableName
     * @param string   $congressId
     * @param boolean  $skipCheck   Skip checking if ID already had
     *
     * @return boolean
     */
    private function _createNewConferenceOnTable(string $tableName, string $congressId, bool $skipCheck = false): bool
    {
        $processed = false;

        $conn = $this->remote->getConnection();

        do {
            if (false == $skipCheck) {
                $statement = $conn->prepare("SELECT COUNT(*) AS count FROM {$tableName} WHERE CongressID = :CongressID;");
                $statement->bindValue(':CongressID', $congressId, \PDO::PARAM_STR);
                $statement->execute();
                $this->lastQueryErrorInfo = $conn->errorInfo();

                $count = $statement->fetchColumn();
                if (!empty($count)) {
                    break;
                }
            }

            $statement = $conn->prepare("INSERT INTO {$tableName} (CongressID) VALUES (:CongressID);");
            $statement->bindValue(':CongressID', $congressId, \PDO::PARAM_STR);
            $processed = $statement->execute();
            $this->lastQueryErrorInfo = $conn->errorInfo();
        } while(false);

        return $processed;
    }

    /**
     * Build Update Conference Query, and Execute
     *
     * @param Conference $conference
     *
     * @throws \Exception
     *
     * @return boolean  PDO Executed with no Error Code
     */
    private function _updateConference(Conference &$conference): bool
    {
        $processed = false;

        $conn = $this->remote->getConnection();

        // primary table
        $mapping = $this->_getMappingForPrimaryTable($conference);
        list($setters, $binders) = $this->__convertMappingIntoSettersAndBinders($mapping);

        $tableName = $this->getPrimaryTableName();
        $statement = $conn->prepare("UPDATE {$tableName} SET {$setters} WHERE CongressID = :PreviousID;");

        $this->__bindingValues($statement, $binders);

        $statement->bindValue(':PreviousID', $conference->getPreviousId(), \PDO::PARAM_STR);

        $processed = $statement->execute();
        $this->lastQueryErrorInfo = $conn->errorInfo();

        if (false == $processed) {
            throw new \Exception('Unable to update Primary Table. ' . json_encode([
                'error-info'  => $this->lastQueryErrorInfo,
                'previous_id' => $conference->getPreviousId(),
            ]));
        }

        // secondary table
        $mapping = $this->_getMappingForSecondaryTable($conference);
        list($setters, $binders) = $this->__convertMappingIntoSettersAndBinders($mapping);

        $tableName = $this->getSecondaryTableName();
        $statement = $conn->prepare("UPDATE {$tableName} SET {$setters} WHERE CongressID = :PreviousID;");

        $this->__bindingValues($statement, $binders);

        $statement->bindValue(':PreviousID', $conference->getPreviousId(), \PDO::PARAM_STR);

        $processed = $statement->execute();
        $this->lastQueryErrorInfo = $conn->errorInfo();

        if (false == $processed) {
            throw new \Exception('Unable to update Secondary Table. ' . json_encode([
                'error-info'  => $this->lastQueryErrorInfo,
                'previous_id' => $conference->getPreviousId(),
            ]));
        }

        return $processed;
    }

    /**
     * Provide mapping and populate value for Main (CRC) Table.
     * Format @see SqlQueryBuildHelperTrait::__convertMappingIntoSetterAndBinder()
     *
     * @param Conference $conference
     *
     * @return array
     */
    private function _getMappingForPrimaryTable(Conference $conference): array
    {
        $specialties = [];
        foreach ($conference->getSpecialties() as $specialty) {
            $specialties[] = $specialty->getSpecialty()->getSpecialty();
        }

        $countryCode = null;
        $countryName = null;
        if (null !== $conference->getCountry()) {
            $countryCode = $conference->getCountry()->getIsoCode();
            $countryCode = self::___convertOutCountryIso($countryCode);
            if (null !== $countryCode) {
                $countryCode = strtolower($countryCode);
            }

            $countryName = $conference->getCountry()->getName();
            $countryName = self::___convertOutCountryName($countryName);
        }

        $regionName = null;
        if (null !== $conference->getRegion()) {
            $regionName = $conference->getRegion()->getName();
            $regionName = self::___convertOutRegionName($regionName);
        }

        return [
            [
                'field' => 'Name',
                'token' => ':Name',
                'bind'  => (!empty($conference->getName()) ? $conference->getName() : null),
            ],
            [
                'field' => 'City',
                'token' => ':City',
                'bind'  => (!empty($conference->getCity()) ? $conference->getCity() : null),
            ],
            [
                'field' => 'CityGuide',
                'token' => ':CityGuide',
                'bind'  => (!empty($conference->getCityGuide()) ? $conference->getCityGuide() : null),
            ],
            [
                'field' => 'CountryCode',
                'token' => ':CountryCode',
                'bind'  => $countryCode,
            ],
            [
                'field' => 'CountryName',
                'token' => ':CountryName',
                'bind'  => $countryName,
            ],
            [
                'field' => 'State',
                'token' => ':State',
                'bind'  => (!empty($conference->getDetailsState()) ? $conference->getDetailsState() : null),
            ],
            [
                'field' => 'Region',
                'token' => ':Region',
                'bind'  => $regionName,
            ],
            [
                'field' => 'Discontinued',
                'token' => ':Discontinued',
                'bind'  => ($conference->isDiscontinued() ? 'yes' : null),
            ],
            [
                'field' => 'StartDate',
                'token' => ':StartDate',
                'bind'  => (null !== $conference->getStartDate() ? $conference->getStartDate()->format('Y-m-d H:i:s') : null),
            ],
            [
                'field' => 'EndDate',
                'token' => ':EndDate',
                'bind'  => (null !== $conference->getEndDate() ? $conference->getEndDate()->format('Y-m-d H:i:s') : null),
            ],
            [
                'field' => 'Contact',
                'token' => ':Contact',
                'bind'  => (!empty($conference->getDetailsContactDesc()) ? $conference->getDetailsContactDesc() : null),
            ],
            [
                'field' => 'Email',
                'token' => ':Email',
                'bind'  => (!empty($conference->getDetailsContactEmails()) ? implode('; ', $conference->getDetailsContactEmails()) : null),
            ],
            [
                'field' => 'Fax',
                'token' => ':Fax',
                'bind'  => (!empty($conference->getDetailsContactFaxes()) ? implode(' / ', $conference->getDetailsContactFaxes()) : null),
            ],
            [
                'field' => 'Phone',
                'token' => ':Phone',
                'bind'  => (!empty($conference->getDetailsContactPhones()) ? implode(' / ', $conference->getDetailsContactPhones()) : null),
            ],
            [
                'field' => 'Specialty',
                'token' => ':Specialty',
                'bind'  => (!empty($specialties) ? implode('; ', $specialties) : null),
            ],
            [
                'field' => 'Cruise',
                'token' => ':Cruise',
                'bind'  => ($conference->isCruise() ? 'yes' : null),
            ],
            [
                'field' => 'KeyCongress',
                'token' => ':KeyCongress',
                'bind'  => ($conference->isKeyEvent() ? 'yes' : null),
            ],
            [
                'field' => 'Online',
                'token' => ':Online',
                'bind'  => ($conference->isOnline() ? 'yes' : null),
            ],
        ];
    }

    /**
     * Provide mapping and populate value for Detail (CRC_Detail) Table.
     * Format @see SqlQueryBuildHelperTrait::__convertMappingIntoSetterAndBinder()
     *
     * @param Conference $conference
     *
     * @return array
     */
    private function _getMappingForSecondaryTable(Conference $conference): array
    {
        $mapping = [
            [
                'field' => 'IncludeCongressGuide',
                'token' => ($conference->isHasGuide() ? "CONVERT(BINARY, 'I')" : ':IncludeCongressGuide'),
                'bind'  => ($conference->isHasGuide() ? false : null),
            ],
        ];


        // static six links
        $links = $conference->getDetailsContactLinks();
        foreach (['caption' => 'CongressLink', 'url' => 'CongressURL'] as $key => $field) {
            for ($i = 1; $i <= 6; $i++) {
                $value = null;
                if (!empty($links[($i-1)])) {
                    $value = $links[($i-1)][$key];
                }
                $mapping[] = [
                    'field' => "{$field}{$i}",
                    'token' => ":{$field}{$i}",
                    'bind'  => $value,
                ];
            }
        }

        $mapping[] = [
            'field' => 'UniqueName',
            'token' => ':UniqueName',
            'bind'  => $conference->getUniqueName(),
        ];

       return $mapping;
    }
}
