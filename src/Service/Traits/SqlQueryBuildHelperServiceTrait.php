<?php

namespace App\Service\Traits;

/**
 * Service Traits: SQL Query Build Helper
 *
 * Expected Mapping Format:
 *    [
 *        [
 *            'field' => field name
 *            'token' => unique eligible PDO param naming
 *            'bind'  => field value | false to skip binding
 *        ]
 *    ],
 *
 * @since  1.2.0
 */
trait SqlQueryBuildHelperServiceTrait
{
    /**
     * Method to format Table name for query
     *
     * @param string      $tableName
     * @param string|null $schema
     * @param boolean     $quote
     *
     * @return string
     */
    private function __tableNameFormatter(string $tableName, ?string $schema, bool $quote = true): string
    {
        $result = [];

        $quote = ($quote ? '"' : false);

        if (!empty($schema)) {
            $result[] = $quote;
            $result[] = $schema;
            $result[] = $quote;
            $result[] = '.';
        }

        $result[] = $quote;
        $result[] = $tableName;
        $result[] = $quote;

        $result = array_filter($result);

        return implode('', $result);
    }

    /**
     * Based on Mapping format, we create 3 sets of array for SQL INSERT.
     *
     * @param array $mapping
     *
     * @return array[string, string, array]
     *                 [0] - string: list of field name
     *                 [1] - string: list of token name
     *                 [2] - array: assoc of keyed-token against bind-value
     */
    private function __convertMappingIntoFieldsTokensAndBinders(array $mapping): array
    {
        $fields = array_column($mapping, 'field');
        $fields = implode(',', $fields);

        $tokens = array_column($mapping, 'token');
        $tokens = implode(',', $tokens);

        $binders = array_column($mapping, 'bind', 'token');

        return [$fields, $tokens, $binders];
    }

    /**
     * Based on Mapping format, we create 2 sets of array for SQL UPDATE.
     *
     * @param array $mapping
     *
     * @return array[string, array]
     *                 [0] - string: setter list of keyed-field against string of field-token
     *                 [1] - array: assoc array of keyed-token against bind-value
     */
    private function __convertMappingIntoSettersAndBinders(array $mapping): array
    {
        $setters = array_column($mapping, 'token', 'field');
        array_walk($setters, function(&$value, $key) {
            $value = "{$key}={$value}";
        });
        $setters = implode(', ', $setters);

        $binders = array_column($mapping, 'bind', 'token');

        return [$setters, $binders];
    }

    /**
     * Execute bindingValue() to each in given array
     *
     * @param \PDOStatement $statement
     * @param array         $bindings
     */
    private function __bindingValues(\PDOStatement &$statement, array $bindings): void
    {
        foreach ($bindings as $token => $value) {
            if (false === $value) {
                continue;
            }

            if (null === $value) {
                $statement->bindValue($token, null, \PDO::PARAM_NULL);
            }
            elseif (is_int($value)) {
                $statement->bindValue($token, $value, \PDO::PARAM_INT);
            }
            else {
                $statement->bindValue($token, $value, \PDO::PARAM_STR);
            }
        }
    }
}
