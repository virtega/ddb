<?php

namespace App\Service\Traits;

use App\Entity\Conference;
use App\Entity\ConferenceSpecialties;
use App\Entity\Country;
use App\Entity\CountryTaxonomy;
use App\Entity\Journal;
use App\Entity\JournalSpecialties;
use App\Entity\SpecialtyTaxonomy;

/**
 * Service Traits: Conference & Journal Form basic methods.
 *
 * @since  1.2.0
 */
trait EntityFormUpackServiceTrait
{
    /**
     * Method to get session Current User's Username
     *
     * @return string
     */
    private function __getCurrentUserUsername(): string
    {
        $username = 'Unknown';

        /**
         * @var \App\Security\User\LdapUser|\Symfony\Component\Security\Core\User\UserInterface|null
         */
        $user = $this->symfonySecurity->getUser();
        if (!empty($user)) {
            $username = $user->getUsername();
        }

        return $username;
    }

    /**
     * Method to get Country by IsoCode
     *
     * @param string $isoCode
     *
     * @return Country|null
     */
    private function __getCountryByIsoCode(string $isoCode): ?Country
    {
        $result = null;
        if (empty($isoCode)) {
            return $result;
        }

        /**
         * @var \App\Repository\CountryRepository<Country>
         */
        $countryRepo = $this->em->getRepository(Country::class);

        /**
         * @var Country|null
         */
        $country = $countryRepo->findOneBy(['isoCode' => $isoCode]);
        if ($country) {
            $result = $country;
        }

        return $result;
    }

    /**
     * Method to get CountryTaxonomy (region) by  ID
     *
     * @param int $id
     *
     * @return CountryTaxonomy|null
     */
    private function __getCountryTaxonomyById(int $id): ?CountryTaxonomy
    {
        $result = null;

        if (empty($id)) {
            return $result;
        }

        /**
         * @var \App\Repository\CountryRepository<CountryTaxonomy>
         */
        $countryTaxRepo = $this->em->getRepository(CountryTaxonomy::class);

        /**
         * @var CountryTaxonomy|null
         */
        $countryTax = $countryTaxRepo->findOneBy(['id' => $id]);
        if ($countryTax) {
            $result = $countryTax;
        }

        return $result;
    }

    /**
     * Unpack and parsing selected Specialty, on a Conference
     * May remove ConferenceSpecialties, and create ConferenceSpecialties and SpecialtyTaxonomy.
     *
     * @param Conference $conference
     * @param array      $specialties  May contains ID, or new Specialty - input-tags
     */
    private function __unpackAndAssignConferenceSpecilties(Conference $conference, array $specialties): void
    {
        /**
         * @var ConferenceSpecialties[]
         */
        $preassigned = $conference->getSpecialties();
        foreach ($preassigned as $conferenceSpecialties) {
            $fIdx = array_search($conferenceSpecialties->getSpecialty()->getId(), $specialties);
            if (false !== $fIdx) {
                unset($specialties[$fIdx]);
            }
            else {
                $conference->removeSpecialty($conferenceSpecialties);
                $this->em->remove($conferenceSpecialties);
            }
        }

        foreach ($specialties as $specialty) {
            $specialtyTax = $this->___searchOrCreateNewSpecialtyTaxonomy($specialty);

            $conferenceSpecialties = new ConferenceSpecialties;
            $conferenceSpecialties->setConference($conference);
            $conferenceSpecialties->setSpecialty($specialtyTax);
            $this->em->persist($conferenceSpecialties);

            $conference->addSpecialty($conferenceSpecialties);
        }
    }

    /**
     * Unpack and parsing selected Specialty, on a Journal
     * May remove JournalSpecialties, and create JournalSpecialties and SpecialtyTaxonomy.
     *
     * @param Journal $journal
     * @param array   $specialties  May contains ID, or new Specialty - input-tags
     */
    private function __unpackAndAssignJournalSpecilties(Journal $journal, array $specialties): void
    {
        /**
         * @var JournalSpecialties[]
         */
        $preassigned = $journal->getSpecialties();
        foreach ($preassigned as $journalSpecialties) {
            $fIdx = array_search($journalSpecialties->getSpecialty()->getId(), $specialties);
            if (false !== $fIdx) {
                unset($specialties[$fIdx]);
            }
            else {
                $journal->removeSpecialty($journalSpecialties);
                $this->em->remove($journalSpecialties);
            }
        }

        foreach ($specialties as $specialty) {
            $specialtyTax = $this->___searchOrCreateNewSpecialtyTaxonomy($specialty);

            $journalSpecialties = new JournalSpecialties;
            $journalSpecialties->setJournal($journal);
            $journalSpecialties->setSpecialty($specialtyTax);
            $this->em->persist($journalSpecialties);

            $journal->addSpecialty($journalSpecialties);
        }
    }

    /**
     * Method to search Specialty base on field-value;
     * May contains ID, or new Specialty - input-tags
     *
     * @param string $identifier    string-int ID, or Specialty name
     *
     * @return SpecialtyTaxonomy
     */
    private function ___searchOrCreateNewSpecialtyTaxonomy(string $identifier): SpecialtyTaxonomy
    {
        /**
         * @var \Doctrine\ORM\EntityRepository<SpecialtyTaxonomy>
         */
        $specialtyTaxonomyRepo = $this->em->getRepository(SpecialtyTaxonomy::class);

        /**
         * Search by ID
         *
         * @var SpecialtyTaxonomy|null
         */
        $specialtyTax = $specialtyTaxonomyRepo->findOneBy(['id' => $identifier]);

        if (empty($specialtyTax)) {
            /**
             * Search by Name
             *
             * @var SpecialtyTaxonomy|null
             */
            $specialtyTax = $specialtyTaxonomyRepo->findOneBy(['specialty' => $identifier]);
        }

        if (empty($specialtyTax)) {
            $specialtyTax = new SpecialtyTaxonomy;
            $specialtyTax->setSpecialty($identifier);
            $this->em->persist($specialtyTax);
        }

        return $specialtyTax;
    }

    /**
     * Method to unpack and convert DateTime picker value
     *
     * @param string  $datetime
     * @param string  $format
     *
     * @return \DateTime|null
     */
    private function __unpackDateTimePickerField(string $datetime, string $format = 'Y-m-d H:i:s'): ?\DateTime
    {
        $result = null;

        if (!empty($datetime)) {
            // reset to 0hr - dont have any time
            if (empty(preg_grep('/[aABgGhHisuv]/', [$format]))) {
                $format .= ' H:i:s';
                $datetime .= ' 00:00:00';
            }

            $convert = \DateTime::createFromFormat($format, $datetime);
            if (!empty($convert)) {
                $result = $convert;
            }
        }

        return $result;
    }

    /**
     * Method to convert value of Yes/No Radio field int value
     *
     * @param string $value
     *
     * @return int
     */
    private function __unpackYesNoToIntRadioField(string $value): int
    {

        return ('yes' === $value ? 1 : 0);
    }

    /**
     * Method to convert value of Yes/No Radio field boolean value
     *
     * @param string $value
     *
     * @return bool
     */
    private function __unpackYesNoToBoolRadioField(string $value): bool
    {

        return ('yes' === $value);
    }

    /**
     * Method to unpack and process simple String-List field
     *
     * @param array $values
     *
     * @return string
     */
    private function __unpackStringBaseListField(array $values): string
    {
        $result = '';

        if (!empty($values)) {
            $values = array_map('trim', $values);
            $values = array_filter($values);

            $result = implode(';', $values);
        }

        return $result;
    }
}
