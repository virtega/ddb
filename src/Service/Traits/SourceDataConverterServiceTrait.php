<?php

namespace App\Service\Traits;

/**
 * Service Traits: Source Data Alias Converter
 * Source data maybe dirty or need re-alignment to DrugDb.
 *
 * Dev:  As in PHP7 the mapping can be overridden if needed to.
 *
 * @since  1.2.0
 */
trait SourceDataConverterServiceTrait
{
    /**
     * Mapping Country ISO code 2.
     *
     * @var array
     */
    private static $countryIsoCodeAlias = [
        'aa'  => 'AW',
        'av'  => 'AI',
        'eu'  => false,
        'ko'  => 'KR',
        'lo'  => 'LS',
        'uk'  => 'GB',
        'usa' => 'US',
        '75'  => 'US',
    ];

    /**
     * Mapping Country Names.
     *
     * @var array
     */
    private static $countryNameAlias = [
        'Czeck Republic'         => 'Czech Republic',
        'Czechia'                => 'Czech Republic',
        'Korea (SOUTH)'          => 'South Korea',
        'Slovak Republic'        => 'Slovakia',
        'Czechoslovakia(former)' => 'Slovakia',
    ];

    /**
     * Mapping Region Names.
     *
     * @var array
     */
    private static $regionNameAlias = [
        'Cruises'                         => false,
        'On line'                         => false,
        'Australia & Oceania'             => 'Australia / Oceania',
        'Latin America & the Caribbean'   => 'Latin America and the Caribbean',
        'Latin America and the Caribbean' => 'Latin America and the Caribbean',
    ];

    /**
     * Mapping Specialty Names.
     *
     * @var array
     */
    private static $specialtyNameAlias = [
        'AIDS/HIV'               => 'HIV/AIDS',
        'Gynaecology/Obstetrics' => 'Gynaecology and Obstetrics',
    ];

    /**
     * Lookup $string in $list key, expecting $list key is uppercased - for in case-insensitive comparison.
     *
     * @param string|null $string
     * @param array       $list
     *
     * @return boolean
     */
    private static function ___aliasUcStrInConversion(?string &$string, array $list): bool
    {
        if (null === $string) {
            return false;
        }

        $cased = strtoupper($string);

        if (false !== strpos($cased, 'ERROR')) {
            return false;
        }

        if (isset($list[$cased])) {
            if (false === $list[$cased]) {
                return false;
            }

            $string = $list[$cased];
        }

        return true;
    }

    /**
     * Look up $string in $list value, expecting the $string is formatted - case-sensitive
     *
     * @param string|null $string
     * @param array       $list
     *
     * @return string
     */
    private static function ___aliasAsIsOutConversion(?string $string, array $list): ?string
    {
        if (null === $string) {
            return null;
        }

        $found = array_search($string, $list);
        if (
            (false !== $found) &&
            (!is_int($found))
        ) {
            return $found;
        }

        return $string;
    }

    /**
     * Change Country ISO code to known conversion.
     * Previous system might refer to WHO code, while DrugDb using UN code.
     *
     * @param string|null  $string   Note argument by reference
     *
     * @return boolean  False result suggest to reject the entry
     */
    private static function ___convertInCountryIso(?string &$string): bool
    {
        static $caseCountryIsos;
        if (null === $caseCountryIsos) {
            array_walk(self::$countryIsoCodeAlias, function($val, $key) use (&$caseCountryIsos) {
                $caseCountryIsos[strtoupper($key)] = $val;
            });
        }

        return self::___aliasUcStrInConversion($string, $caseCountryIsos);
    }

    /**
     * Change Country ISO code to first known alias.
     *
     * @param string|null  $string
     *
     * @return string|null
     */
    private static function ___convertOutCountryIso(?string $string): ?string
    {

        return self::___aliasAsIsOutConversion($string, self::$countryIsoCodeAlias);
    }

    /**
     * Change Country Name to known conversion.
     *
     * @param string|null $string  Note argument by reference
     *
     * @return boolean
     */
    private static function __convertInCountryName(?string &$string): bool
    {
        static $casedCountryNames;
        if (null === $casedCountryNames) {
            array_walk(self::$countryNameAlias, function($val, $key) use (&$casedCountryNames) {
                $casedCountryNames[strtoupper($key)] = $val;
            });
        }

        return self::___aliasUcStrInConversion($string, $casedCountryNames);
    }

    /**
     * Change Country Name to first known alias.
     *
     * @param string|null  $string
     *
     * @return string|null
     */
    private static function ___convertOutCountryName(?string $string): ?string
    {

        return self::___aliasAsIsOutConversion($string, self::$countryNameAlias);
    }

    /**
     * Change Region Name to known conversion.
     *
     * @param string|null $string  Note argument by reference
     *
     * @return boolean
     */
    private static function __convertInRegionName(?string &$string): bool
    {
        static $casedRegions;
        if (null === $casedRegions) {
            array_walk(self::$regionNameAlias, function($val, $key) use (&$casedRegions) {
                $casedRegions[strtoupper($key)] = $val;
            });
        }

        return self::___aliasUcStrInConversion($string, $casedRegions);
    }

    /**
     * Change Region Name to first known alias.
     *
     * @param string|null  $string
     *
     * @return string|null
     */
    private static function ___convertOutRegionName(?string $string): ?string
    {

        return self::___aliasAsIsOutConversion($string, self::$regionNameAlias);
    }

    /**
     * Change Specialty Name to known conversion.
     *
     * @param string|null $string  Note argument by reference
     *
     * @return boolean
     */
    private static function __convertInSpecialtyName(?string &$string): bool
    {
        static $casedSpecialties;
        if (null === $casedSpecialties) {
            array_walk(self::$specialtyNameAlias, function($val, $key) use (&$casedSpecialties) {
                $casedSpecialties[strtoupper($key)] = $val;
                $casedSpecialties[strtoupper($val)] = $val;
            });
        }

        return self::___aliasUcStrInConversion($string, $casedSpecialties);
    }
}
