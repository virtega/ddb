<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GenericRefLevel2.
 *
 * @ORM\Table(name="generic_ref_level2")
 * @ORM\Entity
 *
 * @since 1.0.0
 */
class GenericRefLevel2
{
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10, nullable=false)
     * @ORM\Id
     * #ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name.
     *
     * // not applicable
     *
     * @codeCoverageIgnore
     *
     * @param string|null $name
     *
     * @return GenericRefLevel2
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
}
