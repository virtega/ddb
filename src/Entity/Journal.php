<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Journal
 *
 * @ORM\Table(name="journal", indexes={
 *     @ORM\Index(name="journal_names", columns={"name", "abbreviation"}),
 *     @ORM\Index(name="journal_features", columns={
 *         "status",
 *         "exported",
 *         "is_medline",
 *         "is_second_line",
 *         "is_dgabstract",
 *         "not_being_updated",
 *         "drugs_monitored",
 *         "global_edition_journal",
 *         "deleted",
 *         "manual_creation"
 *     })
 * })
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\JournalRepository")
 *
 * @since 1.2.0  Migrated via Version20190813080003
 */
class Journal extends JournalDetails
{
    /**
     * @var  int  status - NULL
     */
    const STATUS_NONE = 0;

    /**
     * @var  int  status - 'Active'
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var  int  status - 'Archive'
     */
    const STATUS_ARCHIVE = 5;

    public static $statuses = [
        self::STATUS_NONE    => 'None',
        self::STATUS_ACTIVE  => 'Active',
        self::STATUS_ARCHIVE => 'Archived',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="abbreviation", type="string", length=100, nullable=true)
     */
    private $abbreviation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="volume", type="string", length=200, nullable=true)
     */
    private $volume;

    /**
     * @var string|null
     *
     * @ORM\Column(name="number", type="string", length=200, nullable=true)
     */
    private $number;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $status = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="exported", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $exported = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="is_medline", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $isMedline = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="is_second_line", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $isSecondLine = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="is_dgabstract", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $isDgAbstract = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="not_being_updated", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $notBeingUpdated = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="drugs_monitored", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $drugsMonitored = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="global_edition_journal", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $globalEditionJournal = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $deleted = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="manual_creation", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $manualCreation = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="medline_issn", type="string", length=9, nullable=true)
     */
    private $medlineIssn;

    /**
     * @var float
     *
     * @ORM\Column(name="journal_report_1", type="decimal", precision=4, scale=2, nullable=false, options={"default"=0.00})
     */
    private $journalReport1 = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="journal_report_2", type="decimal", precision=4, scale=2, nullable=false, options={"default"=0.00})
     */
    private $journalReport2 = 0.00;

    /**
     * @var string|null
     *
     * @ORM\Column(name="unid", type="string", length=32, nullable=true)
     */
    private $unid;

    /**
     * @var ArrayCollection|JournalSpecialties[]
     *
     * @ORM\OneToMany(targetEntity="JournalSpecialties", mappedBy="journal", orphanRemoval=true)
     */
    private $specialties;

    /**
     * ORM Strategy on IDENTITY, know the risk defining ID manually.
     * It will be ignore, unless for JournalsSyncDownCommand which disable the generator.
     *
     * @param int|null $id
     */
    public function __construct(?int $id = null)
    {
        if (null !== $id) {
            // @codeCoverageIgnoreStart
            $this->id = $id;
            // @codeCoverageIgnoreEnd
        }

        parent::__construct();

        $this->specialties = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {

        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {

        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAbbreviation(): ?string
    {

        return $this->abbreviation;
    }

    /**
     * @param string|null $abbreviation
     *
     * @return self
     */
    public function setAbbreviation(?string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVolume(): ?string
    {

        return $this->volume;
    }

    /**
     * @param string|null $volume
     *
     * @return self
     */
    public function setVolume(?string $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {

        return $this->number;
    }

    /**
     * @param string|null $number
     *
     * @return self
     */
    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {

        return $this->status;
    }

    /**
     * Method to get Status in Readable String version
     *
     * @return string
     */
    public function getStatusReadable(): string
    {
        $status = 'None';

        switch ($this->getStatus()) {
            case (self::STATUS_ACTIVE):
                $status = 'Active';
                break;

            case (self::STATUS_ARCHIVE):
                $status = 'Archived';
                break;
        }

        return $status;
    }

    /**
     * @return boolean
     */
    public function isStatusNone(): bool
    {

        return self::STATUS_NONE == $this->getStatus();
    }

    /**
     * @return boolean
     */
    public function isStatusActive(): bool
    {

        return self::STATUS_ACTIVE == $this->getStatus();
    }

    /**
     * @return boolean
     */
    public function isStatusArchived(): bool
    {

        return self::STATUS_ARCHIVE == $this->getStatus();
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getExported(): int
    {

        return $this->exported;
    }

    /**
     * @return boolean
     */
    public function isExported(): bool
    {

        return !empty($this->getExported());
    }

    /**
     * @param int $exported
     *
     * @return self
     */
    public function setExported(int $exported): self
    {
        $this->exported = $exported;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsMedline(): int
    {

        return $this->isMedline;
    }

    /**
     * @return boolean
     */
    public function isMedLine(): bool
    {

        return !empty($this->getIsMedline());
    }

    /**
     * @param int $isMedline
     *
     * @return self
     */
    public function setIsMedline(int $isMedline): self
    {
        $this->isMedline = $isMedline;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsSecondLine(): int
    {

        return $this->isSecondLine;
    }

    /**
     * @return boolean
     */
    public function isSecondLine(): bool
    {

        return !empty($this->getIsSecondLine());
    }

    /**
     * @param int $isSecondLine
     *
     * @return self
     */
    public function setIsSecondLine(int $isSecondLine): self
    {
        $this->isSecondLine = $isSecondLine;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsDgAbstract(): int
    {

        return $this->isDgAbstract;
    }

    /**
     * @return boolean
     */
    public function isDgAbstract(): bool
    {

        return !empty($this->getIsDgAbstract());
    }

    /**
     * @param int $isDgAbstract
     *
     * @return self
     */
    public function setIsDgAbstract(int $isDgAbstract): self
    {
        $this->isDgAbstract = $isDgAbstract;

        return $this;
    }

    /**
     * @return int
     */
    public function getNotBeingUpdated(): int
    {

        return $this->notBeingUpdated;
    }

    /**
     * @return boolean
     */
    public function isNotBeingUpdated(): bool
    {

        return !empty($this->getNotBeingUpdated());
    }

    /**
     * @param int $notBeingUpdated
     *
     * @return self
     */
    public function setNotBeingUpdated(int $notBeingUpdated): self
    {
        $this->notBeingUpdated = $notBeingUpdated;

        return $this;
    }

    /**
     * @return int
     */
    public function getDrugsMonitored(): int
    {

        return $this->drugsMonitored;
    }

    /**
     * @return boolean
     */
    public function isDrugsMonitored(): bool
    {

        return !empty($this->getDrugsMonitored());
    }

    /**
     * @param int $drugsMonitored
     *
     * @return self
     */
    public function setDrugsMonitored(int $drugsMonitored): self
    {
        $this->drugsMonitored = $drugsMonitored;

        return $this;
    }

    /**
     * @return int
     */
    public function getGlobalEditionJournal(): int
    {

        return $this->globalEditionJournal;
    }

    /**
     * @return boolean
     */
    public function isGlobalEditionJournal(): bool
    {

        return !empty($this->getGlobalEditionJournal());
    }

    /**
     * @param int $globalEditionJournal
     *
     * @return self
     */
    public function setGlobalEditionJournal(int $globalEditionJournal): self
    {
        $this->globalEditionJournal = $globalEditionJournal;

        return $this;
    }

    /**
     * @return int
     */
    public function getDeleted(): int
    {

        return $this->deleted;
    }

    /**
     * @return boolean
     */
    public function isDeleted(): bool
    {

        return !empty($this->getDeleted());
    }

    /**
     * @param int $deleted
     *
     * @return self
     */
    public function setDeleted(int $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return int
     */
    public function getManualCreation(): int
    {

        return $this->manualCreation;
    }

    /**
     * @return boolean
     */
    public function isManualCreation(): bool
    {

        return !empty($this->getManualCreation());
    }

    /**
     * @param int $manualCreation
     *
     * @return self
     */
    public function setManualCreation(int $manualCreation): self
    {
        $this->manualCreation = $manualCreation;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMedlineIssn(): ?string
    {

        return $this->medlineIssn;
    }

    /**
     * @param string|null $medlineIssn
     *
     * @return self
     */
    public function setMedlineIssn(?string $medlineIssn): self
    {
        $this->medlineIssn = $medlineIssn;

        return $this;
    }

    /**
     * @return float
     */
    public function getJournalReport1(): float
    {

        return $this->journalReport1;
    }

    /**
     * @param float $journalReport1
     *
     * @return self
     */
    public function setJournalReport1(float $journalReport1): self
    {
        $this->journalReport1 = $this->_setRoundFloatValue($journalReport1);

        return $this;
    }

    /**
     * @return float
     */
    public function getJournalReport2(): float
    {

        return $this->journalReport2;
    }

    /**
     * @param float $journalReport2
     *
     * @return self
     */
    public function setJournalReport2(float $journalReport2): self
    {
        $this->journalReport2 = $this->_setRoundFloatValue($journalReport2);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUnid(): ?string
    {

        return $this->unid;
    }

    /**
     * @param string|null $unid
     *
     * @return self
     */
    public function setUnid(?string $unid): self
    {
        $this->unid = $unid;

        return $this;
    }

    /**
     * @return Collection|JournalSpecialties[]
     */
    public function getSpecialties(): Collection
    {

        return $this->specialties;
    }

    /**
     * @param JournalSpecialties $specialty
     *
     * @return self
     */
    public function addSpecialty(JournalSpecialties $specialty): self
    {
        if (!$this->specialties->contains($specialty)) {
            $specialty->setJournal($this);
            $this->specialties->add($specialty);
        }

        return $this;
    }

    /**
     * @param JournalSpecialties $specialty
     *
     * @return self
     */
    public function removeSpecialty(JournalSpecialties $specialty): self
    {
        if ($this->specialties->contains($specialty)) {
            $this->specialties->removeElement($specialty);
        }

        return $this;
    }
}
