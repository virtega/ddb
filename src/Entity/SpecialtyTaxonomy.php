<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpecialtyTaxonomy
 *
 * @ORM\Table(name="specialty_taxonomy")
 * @ORM\Entity
 */
class SpecialtyTaxonomy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="specialty", type="string", length=200, nullable=false)
     */
    private $specialty;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {

        return $this->id;
    }

    /**
     * @return string
     */
    public function getSpecialty(): string
    {

        return $this->specialty;
    }

    /**
     * @param string $specialty
     *
     * @return self
     */
    public function setSpecialty(string $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }
}
