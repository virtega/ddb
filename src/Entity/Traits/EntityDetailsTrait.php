<?php

namespace App\Entity\Traits;

use \DateTime;
use \DateTimeInterface;

/**
 * Shared methods and property for $details:
 * - ConferenceDetails | conference.details
 * - JournalDetails | journal.details
 *
 * @since 1.2.0
 */
trait EntityDetailsTrait
{
    /**
     * @var  string DateTime object format
     */
    public static $format_datetime = 'Y-m-d H:i:s';

    /**
     * @var array|null
     *
     * @ORM\Column(name="details", type="json", nullable=true)
     */
    protected $details;

    public function __construct()
    {

        $this->setDetails( $this->details );
    }

    /**
     * Method accessibility changed to Protected.
     * Use getDetails*() for control.
     *
     * @return array
     */
    public function getDetails(): ?array
    {

        return $this->details;
    }

    /**
     * Method accessibility changed to Protected.
     * Use setDetails*() for control.
     *
     * @param array $details
     *
     * @return self
     */
    public function setDetails(?array $details): self
    {
        $details = $details ?? [];
        $this->details = array_replace_recursive(self::$defaults, $details);

        return $this;
    }

    // META

    /**
     * @return DateTime|null
     */
    public function getDetailsMetaCreatedOn(): ?DateTime
    {
        $value = $this->details['_meta']['created_on'] ?? self::$defaults['_meta']['created_on'];

        return $this->_readDateTimeValue($value);
    }

    /**
     * @param DateTime $created_on
     */
    public function setDetailsMetaCreatedOn(DateTime $created_on): self
    {
        $this->details['_meta']['created_on'] = $this->_setDateTimeValue($created_on);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDetailsMetaCreatedBy(): ?string
    {

        return $this->details['_meta']['created_by'] ?? self::$defaults['_meta']['created_by'];
    }

    /**
     * @param string $created_by
     *
     * @return self
     */
    public function setDetailsMetaCreatedBy(string $created_by): self
    {
        $this->details['_meta']['created_by'] = trim($created_by);

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDetailsMetaModifiedOn(): ?DateTime
    {
        $value = $this->details['_meta']['modified_on'] ?? self::$defaults['_meta']['modified_on'];

        return $this->_readDateTimeValue($value);
    }

    /**
     * @param DateTime $modified_on
     */
    public function setDetailsMetaModifiedOn(DateTime $modified_on): self
    {
        $this->details['_meta']['modified_on'] = $this->_setDateTimeValue($modified_on);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDetailsMetaModifiedBy(): ?string
    {

        return $this->details['_meta']['modified_by'] ?? self::$defaults['_meta']['modified_by'];
    }

    /**
     * @param string $modified_by
     *
     * @return self
     */
    public function setDetailsMetaModifiedBy(string $modified_by): self
    {
        $this->details['_meta']['modified_by'] = trim($modified_by);

        return $this;
    }

    // HELPERS

    /**
     * Convert Multi Lines single-line values into string with carriage return
     *
     * @param string|null $given
     *
     * @return string
     */
    protected function _readMultiLineValue(?string $given): string
    {
        if (empty($given)) {
            return '';
        }

        $value = preg_replace('/\<br(\s*)?\/?\>/i', PHP_EOL, $given);

        return null === $value ? '' : $value;
    }

    /**
     * Convert Multi Lines values into single-line
     *
     * @param string $given
     *
     * @return string
     */
    protected function _setMultiLineValue(string $given): string
    {
        $given = trim($given);

        return empty($given) ? $given : str_replace(["\r\n", "\r", "\n"], '<br/>', $given);
    }

    /**
     * Convert DateTime values into object
     *
     * @param string $given
     *
     * @return DateTime|null
     */
    protected function _readDateTimeValue(?string $given): ?DateTime
    {
        if (empty($given)) {
            return null;
        }

        $value = DateTime::createFromFormat(self::$format_datetime, $given);

        return (false === $value) ? null : $value;
    }

    /**
     * Convert DateTime values into string for storage
     *
     * @param DateTimeInterface|DateTime|null $given
     * @param string|null                      $format
     *
     * @return string|null
     */
    protected function _setDateTimeValue(?DateTimeInterface $given, ?string $format = null): ?string
    {
        if (empty($given)) {
            return null;
        }

        if (empty($format)) {
            $format = self::$format_datetime;
        }

        $value = $given->format($format);

        return empty($value) ? null : $value;
    }

    /**
     * Round Up Float values into nearest decimal
     *
     * @param float $given
     * @param int   $precision
     *
     * @return float
     */
    protected function _setRoundFloatValue(float $given, int $precision = 2): float
    {

        return round($given, $precision);
    }
}
