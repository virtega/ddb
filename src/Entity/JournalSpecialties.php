<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JournalSpecialties
 *
 * @ORM\Table(name="journal_specialties", indexes={
 *     @ORM\Index(name="FK_2C63585E9A353316", columns={"specialty_id"}),
 *     @ORM\Index(name="FK_2C63585E478E8802", columns={"journal_id"})
 * })
 * @ORM\Entity
 *
 * @since 1.2.0  Migrated via Version20190813080003
 */
class JournalSpecialties
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Journal
     *
     * @ORM\ManyToOne(targetEntity="Journal", inversedBy="specialties")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(
     *       name="journal_id",
     *       referencedColumnName="id",
     *        nullable=false,
     *        onDelete="NO ACTION"
     *   )
     * })
     */
    private $journal;

    /**
     * @var SpecialtyTaxonomy
     *
     * @ORM\ManyToOne(targetEntity="SpecialtyTaxonomy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(
     *       name="specialty_id",
     *       referencedColumnName="id",
     *       nullable=false,
     *       onDelete="NO ACTION"
     *   )
     * })
     */
    private $specialty;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {

        return $this->id;
    }

    /**
     * @return Journal
     */
    public function getJournal(): Journal
    {

        return $this->journal;
    }

    /**
     * @param Journal $journal
     *
     * @return self
     */
    public function setJournal(Journal $journal): self
    {
        $this->journal = $journal;

        return $this;
    }

    /**
     * @return SpecialtyTaxonomy
     */
    public function getSpecialty(): SpecialtyTaxonomy
    {

        return $this->specialty;
    }

    /**
     * @param SpecialtyTaxonomy $specialty
     *
     * @return self
     */
    public function setSpecialty(SpecialtyTaxonomy $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }
}
