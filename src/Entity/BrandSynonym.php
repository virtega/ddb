<?php

namespace App\Entity;

use App\Entity\Family\KeywordTypeFamily;
use Doctrine\ORM\Mapping as ORM;

/**
 * BrandSynonym.
 *
 * @ORM\Table(name="brand_synonym", indexes={@ORM\Index(name="generic", columns={"brand"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\Drug\BrandDrugRepository")
 *
 * @since 1.0.0
 */
class BrandSynonym extends KeywordTypeFamily
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true, options={"comment"="JSON string for multiple Country ISO code"})
     */
    private $country;

    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand", referencedColumnName="id")
     * })
     */
    private $brand;

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return BrandSynonym
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country.
     *
     * @param array $country
     *
     * @return BrandSynonym
     */
    public function setCountry($country = null)
    {
        $json = (array) $country;
        $json = json_encode($json);
        if (false == $json) {
            // @codeCoverageIgnoreStart
            $this->country = '';
            // @codeCoverageIgnoreEnd
        }
        else {
            $this->country = $json;
        }

        return $this;
    }

    /**
     * Get country.
     *
     * @return array
     */
    public function getCountry()
    {
        $json = $this->country;

        if (!empty($json)) {
            $json = json_decode($json, true);
            $json = (array) $json;
        }
        else {
            // @codeCoverageIgnoreStart
            $json = array();
            // @codeCoverageIgnoreEnd
        }

        return $json;
    }

    /**
     * Set brand.
     *
     * @param Brand $brand
     *
     * @return BrandSynonym
     */
    public function setBrand(Brand $brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }
}
