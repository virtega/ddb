<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CountryRegions
 *
 * @ORM\Table(name="country_regions", indexes={
 *     @ORM\Index(name="FK_6B2B219EF62F176", columns={"region"}),
 *     @ORM\Index(name="FK_6B2B219E5373C966", columns={"country"})
 * })
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CountryRegionsRepository")
 */
class CountryRegions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(
     *       name="country",
     *       referencedColumnName="iso_code",
     *       nullable=false,
     *       onDelete="NO ACTION"
     *   )
     * })
     */
    private $country;

    /**
     * @var CountryTaxonomy
     *
     * @ORM\ManyToOne(targetEntity="CountryTaxonomy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(
     *       name="region",
     *       referencedColumnName="id",
     *       nullable=false,
     *       onDelete="NO ACTION"
     *   )
     * })
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $region;

    /**
     * Get country relation ID
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get Country
     *
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * Set Country
     *
     * @param Country $country
     *
     * @return self
     */
    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get Country Region
     *
     * @return CountryTaxonomy
     */
    public function getRegion(): CountryTaxonomy
    {
        return $this->region;
    }

    /**
     * Set Country Region
     *
     * @param CountryTaxonomy $region
     *
     * @return self
     */
    public function setRegion(CountryTaxonomy $region): self
    {
        $this->region = $region;

        return $this;
    }
}
