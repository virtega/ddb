<?php

namespace App\Entity;

use App\Entity\Family\KeywordTypeFamily;
use Doctrine\ORM\Mapping as ORM;

/**
 * GenericTypo.
 *
 * @ORM\Table(name="generic_typo", indexes={@ORM\Index(name="generic", columns={"generic"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\Drug\GenericDrugRepository")
 *
 * @since 1.0.0
 */
class GenericTypo extends KeywordTypeFamily
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     */
    private $name;

    /**
     * @var Generic
     *
     * @ORM\ManyToOne(targetEntity="Generic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="generic", referencedColumnName="id")
     * })
     */
    private $generic;

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return GenericTypo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set generic.
     *
     * @param Generic $generic
     *
     * @return GenericTypo
     */
    public function setGeneric(Generic $generic)
    {
        $this->generic = $generic;

        return $this;
    }

    /**
     * Get generic.
     *
     * @return Generic
     |
     */
    public function getGeneric()
    {
        return $this->generic;
    }
}
