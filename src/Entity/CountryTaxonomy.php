<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CountryTaxonomy
 *
 * @ORM\Table(name="country_taxonomy", indexes={
 *     @ORM\Index(name="FK_CED8E9803D8E604F", columns={"parent"})
 * })
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CountryTaxonomyRepository")
 */
class CountryTaxonomy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var CountryTaxonomy|null
     *
     * @ORM\ManyToOne(targetEntity="CountryTaxonomy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(
     *       name="parent",
     *       referencedColumnName="id",
     *       nullable=true,
     *       onDelete="NO ACTION"
     *   )
     * })
     */
    private $parent;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set Region Name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set Region Name
     *
     * // as reference only
     * @codeCoverageIgnore
     *
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get Region Parent
     *
     * @return CountryTaxonomy|null
     */
    public function getParent(): ?CountryTaxonomy
    {
        return $this->parent;
    }

    /**
     * Set Region Parent
     *
     * // as reference only
     * @codeCoverageIgnore
     *
     * @param CountryTaxonomy|null $parent
     *
     * @return self
     */
    public function setParent(?CountryTaxonomy $parent): self
    {
        $this->parent = $parent;

        return $this;
    }
}
