<?php

namespace App\Entity;

use \DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * JournalDetails, an Abstract Entity for Journal
 *
 * @since 1.2.0  Migrated via Version20190813080003
 */
class JournalDetails
{
    use \App\Entity\Traits\EntityDetailsTrait;

    /**
     * @var array
     */
    public static $defaults = [
        'language'               => '',
        'country'                => null, // Country iso_code - CHAR(2)|null
        'region'                 => null, // CountryTaxonomy id - INT|null
        'edition'                => '',
        'copyright'              => '',
        'auto_publishing_source' => '',
        'previous_rowguid'       => '',
        'abstract'               => [
            'abstract'             => false,
            'reg_for_abstract'     => false,
            'fulltext_availablity' => false,
            'reg_for_fulltext'     => false,
            'fee_for_fulltext'     => false,
            'comments'             => '', // MultiLine
        ],
        'ordering' => [
            'html_code'    => '',
            'cgi_value'    => '',
            'printer_name' => '',
        ],
        'publisher' => [
            'name'    => '',
            'reg_url' => '',
        ],
        'url' => [
            'url'               => '',
            'updated_on'        => null,
            'viewed_on'         => null,
            'viewed_on_comment' => '',
            'to_update'         => null,
            'note'              => '', // MultiLine
            'frequency'         => '',
            'username'          => '',
            'password'          => '',
        ],
        'validation' => [
            'author'        => '',
            'date_composed' => null,
            'cleared_by'    => '',
            'cleared_on'    => null,
            'modified'      => '',
        ],
        '_meta' => [
            'created_on'  => null,
            'created_by'  => null,
            'modified_on' => null,
            'modified_by' => null,
        ],
    ];

    // ROOT

    /**
     * @return string
     */
    public function getDetailsLanguage(): string
    {

        return $this->details['language'] ?? self::$defaults['language'];
    }

    /**
     * @param string $language
     *
     * @return self
     */
    public function setDetailsLanguage(string $language): self
    {
        $this->details['language'] = trim($language);

        return $this;
    }

    /**
     * Country iso_code - CHAR(2)|null
     *
     * @return ?string
     */
    public function getDetailsCountry(): ?string
    {

        return $this->details['country'] ?? self::$defaults['country'];
    }

    /**
     * Country iso_code - CHAR(2)|null
     *
     * @param ?string $country
     *
     * @return self
     */
    public function setDetailsCountry(?string $country): self
    {
        $this->details['country'] = $country;

        return $this;
    }

    /**
     * CountryTaxonomy id - INT|null
     *
     * @return ?int
     */
    public function getDetailsRegion(): ?int
    {

        return $this->details['region'] ?? self::$defaults['region'];
    }

    /**
     * CountryTaxonomy id - INT|null
     *
     * @param ?int $region
     *
     * @return self
     */
    public function setDetailsRegion(?int $region): self
    {
        $this->details['region'] = empty($region) ? null : $region;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsEdition(): string
    {

        return $this->details['edition'] ?? self::$defaults['edition'];
    }

    /**
     * @param string $edition
     *
     * @return self
     */
    public function setDetailsEdition(string $edition): self
    {
        $this->details['edition'] = trim($edition);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsCopyright(): string
    {

        return $this->details['copyright'] ?? self::$defaults['copyright'];
    }

    /**
     * @param string $copyright
     *
     * @return self
     */
    public function setDetailsCopyright(string $copyright): self
    {
        $this->details['copyright'] = trim($copyright);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsAutoPublishingSource(): string
    {

        return $this->details['auto_publishing_source'] ?? self::$defaults['auto_publishing_source'];
    }

    /**
     * @param string $auto_publishing_source
     *
     * @return self
     */
    public function setDetailsAutoPublishingSource(string $auto_publishing_source): self
    {
        $this->details['auto_publishing_source'] = trim($auto_publishing_source);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsPreviousRowGuid(): string
    {

        return $this->details['previous_rowguid'] ?? self::$defaults['previous_rowguid'];
    }

    /**
     * @param string $previous_rowguid
     *
     * @return self
     */
    public function setDetailsPreviousRowGuid(string $previous_rowguid): self
    {
        $this->details['previous_rowguid'] = trim($previous_rowguid);

        return $this;
    }

    // ABSTRACT

    /**
     * @return boolean
     */
    public function getDetailsAbstractAbstract(): bool
    {

        return $this->details['abstract']['abstract'] ?? self::$defaults['abstract']['abstract'];
    }

    /**
     * @param boolean $abstract
     *
     * @return self
     */
    public function setDetailsAbstractAbstract(bool $abstract): self
    {
        $this->details['abstract']['abstract'] = $abstract;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getDetailsAbstractRegForAbstract(): bool
    {

        return $this->details['abstract']['reg_for_abstract'] ?? self::$defaults['abstract']['reg_for_abstract'];
    }

    /**
     * @param boolean $reg_for_abstract
     *
     * @return self
     */
    public function setDetailsAbstractRegForAbstract(bool $reg_for_abstract): self
    {
        $this->details['abstract']['reg_for_abstract'] = $reg_for_abstract;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getDetailsAbstractFullTextAvailablity(): bool
    {

        return $this->details['abstract']['fulltext_availablity'] ?? self::$defaults['abstract']['fulltext_availablity'];
    }

    /**
     * @param boolean $fulltext_availablity
     *
     * @return self
     */
    public function setDetailsAbstractFullTextAvailablity(bool $fulltext_availablity): self
    {
        $this->details['abstract']['fulltext_availablity'] = $fulltext_availablity;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getDetailsAbstractRegForFullText(): bool
    {

        return $this->details['abstract']['reg_for_fulltext'] ?? self::$defaults['abstract']['reg_for_fulltext'];
    }

    /**
     * @param boolean $reg_for_fulltext
     *
     * @return self
     */
    public function setDetailsAbstractRegForFullText(bool $reg_for_fulltext): self
    {
        $this->details['abstract']['reg_for_fulltext'] = $reg_for_fulltext;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getDetailsAbstractFeeForFullText(): bool
    {

        return $this->details['abstract']['fee_for_fulltext'] ?? self::$defaults['abstract']['fee_for_fulltext'];
    }

    /**
     * @param boolean $fee_for_fulltext
     *
     * @return self
     */
    public function setDetailsAbstractFeeForFullText(bool $fee_for_fulltext): self
    {
        $this->details['abstract']['fee_for_fulltext'] = $fee_for_fulltext;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsAbstractComments(): string
    {
        $value = $this->details['abstract']['comments'] ?? self::$defaults['abstract']['comments'];

        return $this->_readMultiLineValue($value);
    }

    /**
     * @param string $comments
     *
     * @return self
     */
    public function setDetailsAbstractComments(string $comments): self
    {
        $this->details['abstract']['comments'] = $this->_setMultiLineValue($comments);

        return $this;
    }

    // ORDERING

    /**
     * @return string
     */
    public function getDetailsOrderingHtmlCode(): string
    {

        return $this->details['ordering']['html_code'] ?? self::$defaults['ordering']['html_code'];
    }

    /**
     * @param string $html_code
     *
     * @return self
     */
    public function setDetailsOrderingHtmlCode(string $html_code): self
    {
        $this->details['ordering']['html_code'] = trim($html_code);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsOrderingCgiValue(): string
    {

        return $this->details['ordering']['cgi_value'] ?? self::$defaults['ordering']['cgi_value'];
    }

    /**
     * @param string $cgi_value
     *
     * @return self
     */
    public function setDetailsOrderingCgiValue(string $cgi_value): self
    {
        $this->details['ordering']['cgi_value'] = trim($cgi_value);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsOrderingPrinterName(): string
    {

        return $this->details['ordering']['printer_name'] ?? self::$defaults['ordering']['printer_name'];
    }

    /**
     * @param string $printer_name
     *
     * @return self
     */
    public function setDetailsOrderingPrinterName(string $printer_name): self
    {
        $this->details['ordering']['printer_name'] = trim($printer_name);

        return $this;
    }

    // PUBLISHER

    /**
     * @return string
     */
    public function getDetailsPublisherName(): string
    {

        return $this->details['publisher']['name'] ?? self::$defaults['publisher']['name'];
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setDetailsPublisherName(string $name): self
    {
        $this->details['publisher']['name'] = trim($name);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsPublisherRegUrl(): string
    {

        return $this->details['publisher']['reg_url'] ?? self::$defaults['publisher']['reg_url'];
    }

    /**
     * @param string $reg_url
     *
     * @return self
     */
    public function setDetailsPublisherRegUrl(string $reg_url): self
    {
        $this->details['publisher']['reg_url'] = trim($reg_url);

        return $this;
    }

    // URL

    /**
     * @return string
     */
    public function getDetailsUrlUrl(): string
    {

        return $this->details['url']['url'] ?? self::$defaults['url']['url'];
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setDetailsUrlUrl(string $url): self
    {
        $this->details['url']['url'] = trim($url);

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDetailsUrlUpdatedOn(): ?DateTime
    {
        $value = $this->details['url']['updated_on'] ?? self::$defaults['url']['updated_on'];

        return $this->_readDateTimeValue($value);
    }

    /**
     * @param DateTime|null $updated_on
     *
     * @return self
     */
    public function setDetailsUrlUpdatedOn(?DateTime $updated_on): self
    {
        $this->details['url']['updated_on'] = $this->_setDateTimeValue($updated_on);

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDetailsUrlViewedOn(): ?DateTime
    {
        $value = $this->details['url']['viewed_on'] ?? self::$defaults['url']['viewed_on'];

        return $this->_readDateTimeValue($value);
    }

    /**
     * @param DateTime|null $viewed_on
     *
     * @return self
     */
    public function setDetailsUrlViewedOn(?DateTime $viewed_on): self
    {
        $this->details['url']['viewed_on'] = $this->_setDateTimeValue($viewed_on);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsUrlViewedOnComment(): string
    {
        $value = $this->details['url']['viewed_on_comment'] ?? self::$defaults['url']['viewed_on_comment'];

        return $value;
    }

    /**
     * @param string $viewed_on_comment
     *
     * @return self
     */
    public function setDetailsUrlViewedOnComment(string $viewed_on_comment): self
    {
        $this->details['url']['viewed_on_comment'] = trim($viewed_on_comment);

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDetailsUrlToUpdate(): ?DateTime
    {
        $value = $this->details['url']['to_update'] ?? self::$defaults['url']['to_update'];

        return $this->_readDateTimeValue($value);
    }

    /**
     * @param DateTime|null $to_update
     *
     * @return self
     */
    public function setDetailsUrlToUpdate(?DateTime $to_update): self
    {
        $this->details['url']['to_update'] = $this->_setDateTimeValue($to_update);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsUrlNote(): string
    {
        $value = $this->details['url']['note'] ?? self::$defaults['url']['note'];

        return $this->_readMultiLineValue($value);
    }

    /**
     * @param string $note
     *
     * @return self
     */
    public function setDetailsUrlNote(string $note): self
    {
        $this->details['url']['note'] = $this->_setMultiLineValue($note);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsUrlFrequency(): string
    {

        return $this->details['url']['frequency'] ?? self::$defaults['url']['frequency'];
    }

    /**
     * @param string $frequency
     *
     * @return self
     */
    public function setDetailsUrlFrequency(string $frequency): self
    {
        $this->details['url']['frequency'] = trim($frequency);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsUrlUsername(): string
    {

        return $this->details['url']['username'] ?? self::$defaults['url']['username'];
    }

    /**
     * @param string $username
     *
     * @return self
     */
    public function setDetailsUrlUsername(string $username): self
    {
        $this->details['url']['username'] = trim($username);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsUrlPassword(): string
    {

        return $this->details['url']['password'] ?? self::$defaults['url']['password'];
    }

    /**
     * @param string $password
     *
     * @return self
     */
    public function setDetailsUrlPassword(string $password): self
    {
        $this->details['url']['password'] = trim($password);

        return $this;
    }

    // VALIDATION

    /**
     * @return string
     */
    public function getDetailsValidationAuthor(): string
    {

        return $this->details['validation']['author'] ?? self::$defaults['validation']['author'];
    }

    /**
     * @param string $author
     *
     * @return self
     */
    public function setDetailsValidationAuthor(string $author): self
    {
        $this->details['validation']['author'] = trim($author);

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDetailsValidationDateComposed(): ?DateTime
    {
        $value = $this->details['validation']['date_composed'] ?? self::$defaults['validation']['date_composed'];

        return $this->_readDateTimeValue($value);
    }

    /**
     * @param DateTime|null $date_composed
     *
     * @return self
     */
    public function setDetailsValidationDateComposed(?DateTime $date_composed): self
    {
        $this->details['validation']['date_composed'] = $this->_setDateTimeValue($date_composed);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsValidationClearedBy(): string
    {

        return $this->details['validation']['cleared_by'] ?? self::$defaults['validation']['cleared_by'];
    }

    /**
     * @param string $cleared_by
     *
     * @return self
     */
    public function setDetailsValidationClearedBy(string $cleared_by): self
    {
        $this->details['validation']['cleared_by'] = trim($cleared_by);

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDetailsValidationClearedOn(): ?DateTime
    {
        $value = $this->details['validation']['cleared_on'] ?? self::$defaults['validation']['cleared_on'];

        return $this->_readDateTimeValue($value);
    }

    /**
     * @param DateTime|null $cleared_on
     *
     * @return self
     */
    public function setDetailsValidationClearedOn(?DateTime $cleared_on): self
    {
        $this->details['validation']['cleared_on'] = $this->_setDateTimeValue($cleared_on);

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsValidationModified(): string
    {

        return $this->details['validation']['modified'] ?? self::$defaults['validation']['modified'];
    }

    /**
     * @param string $modified
     *
     * @return self
     */
    public function setDetailsValidationModified(string $modified): self
    {
        $this->details['validation']['modified'] = trim($modified);

        return $this;
    }
}
