<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Security\User\LdapUserProvider;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRolesRepository")
 */
class UserRoles
{
    /**
     * READER / WRITER User
     * @var  string     Super Admin string
     */
    const ADMIN   = 'ROLE_ADMIN';

    /**
     * READER / WRITER User
     * @var  string     Editor User identifier string
     */
    const EDITOR  = 'ROLE_EDITOR';

    /**
     * READER User
     * @var  string     Normal User identifier string
     */
    const USER    = 'ROLE_USER';

    /**
     * Login user - ANY
     * @var  string     Normal User identifier string
     */
    const VISITOR = 'ROLE_VISITOR';

    /**
     * @var ?int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $roles;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(name="last_login", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $lastLogin;

    /**
     * @param ?string $username
     */
    function __construct(?string $username = null)
    {
        if (!empty($username)) {
            $this->setUsername($username);
        }
        $this->setRoles(LdapUserProvider::$defaultRoles);
        $this->setLastLogin(new \DateTime('now'));
    }

    /**
     * @return ?int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getLastLogin(): \DateTimeInterface
    {
        return $this->lastLogin;
    }

    /**
     * @param \DateTimeInterface $lastLogin
     */
    public function setLastLogin(\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }
}
