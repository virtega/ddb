<?php

namespace App\Entity;

use App\Entity\Family\KeywordTypeFamily;
use Doctrine\ORM\Mapping as ORM;

/**
 * BrandTypo.
 *
 * @ORM\Table(name="brand_typo", indexes={@ORM\Index(name="brand", columns={"brand"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\Drug\BrandDrugRepository")
 *
 * @since 1.0.0
 */
class BrandTypo extends KeywordTypeFamily
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     */
    private $name;

    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand", referencedColumnName="id")
     * })
     */
    private $brand;

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return BrandTypo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set brand.
     *
     * @param Brand $brand
     *
     * @return BrandTypo
     */
    public function setBrand(Brand $brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }
}
