<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Conference
 *
 * @ORM\Table(name="conference", indexes={
 *     @ORM\Index(name="FK_911533C85373C966", columns={"country"}),
 *     @ORM\Index(name="FK_911533C8F62F176", columns={"region"}),
 *     @ORM\Index(name="conference_name", columns={"name"}),
 *     @ORM\Index(name="conference_features", columns={"cruise", "online", "key_event", "has_guide", "discontinued"})
 * })
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ConferenceRepository")
 *
 * @since 1.2.0  Migrated via Version20190724074105
 */
class Conference extends ConferenceDetails
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=500, nullable=true)
     */
    private $name;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="string", length=150, nullable=true)
     */
    private $city;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city_guide", type="string", length=150, nullable=true)
     */
    private $cityGuide;

    /**
     * @var Country|null
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(
     *     name="country",
     *     referencedColumnName="iso_code",
     *     nullable=true,
     *     onDelete="NO ACTION"
     *   )
     * })
     */
    private $country;

    /**
     * @var CountryTaxonomy|null
     *
     * @ORM\ManyToOne(targetEntity="CountryTaxonomy")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(
     *     name="region",
     *     referencedColumnName="id",
     *     nullable=true,
     *     onDelete="NO ACTION"
     *   )
     * })
     */
    private $region;

    /**
     * @var string|null
     *
     * @ORM\Column(name="previous_id", type="string", length=32, nullable=true)
     */
    private $previousId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="unique_name", type="string", length=200, nullable=true)
     */
    private $uniqueName;

    /**
     * @var int
     *
     * @ORM\Column(name="cruise", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $cruise = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="online", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $online = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="key_event", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $keyEvent = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="has_guide", type="integer", nullable=false, options={"unsigned"=true, "default"=0})
     */
    private $hasGuide = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="discontinued", type="integer", nullable=false, options={"default"=0})
     */
    private $discontinued = 0;

    /**
     * @var ArrayCollection|ConferenceSpecialties[]
     *
     * @ORM\OneToMany(targetEntity="ConferenceSpecialties", mappedBy="conference", orphanRemoval=true)
     */
    private $specialties;

    public function __construct()
    {
        parent::__construct();

        $this->specialties = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {

        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {

        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getStartDate(): ?\DateTimeInterface
    {

        return $this->startDate;
    }

    /**
     * @param \DateTimeInterface|null $startDate
     *
     * @return self
     */
    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getEndDate(): ?\DateTimeInterface
    {

        return $this->endDate;
    }

    /**
     * @param \DateTimeInterface|null $endDate
     *
     * @return self
     */
    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {

        return $this->city;
    }

    /**
     * @param string|null $city
     *
     * @return self
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCityGuide(): ?string
    {

        return $this->cityGuide;
    }

    /**
     * @param string|null $cityGuide
     *
     * @return self
     */
    public function setCityGuide(?string $cityGuide): self
    {
        $this->cityGuide = $cityGuide;

        return $this;
    }

    /**
     * @return Country|null
     *
     * @return self
     */
    public function getCountry(): ?Country
    {

        return $this->country;
    }

    /**
     * @param Country|null $country
     *
     * @return self
     */
    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return CountryTaxonomy|null
     */
    public function getRegion(): ?CountryTaxonomy
    {

        return $this->region;
    }

    /**
     * @param CountryTaxonomy|null $region
     *
     * @return self
     */
    public function setRegion(?CountryTaxonomy $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPreviousId(): ?string
    {

        return $this->previousId;
    }

    /**
     * @param string|null $previousId
     *
     * @return self
     */
    public function setPreviousId(?string $previousId): self
    {
        $this->previousId = $previousId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUniqueName(): ?string
    {

        return $this->uniqueName;
    }

    /**
     * @param string|null $uniqueName
     *
     * @return self
     */
    public function setUniqueName(?string $uniqueName): self
    {
        $this->uniqueName = $uniqueName;

        return $this;
    }

    /**
     * @return int
     */
    public function getCruise(): int
    {

        return $this->cruise;
    }

    /**
     * @return boolean
     */
    public function isCruise(): bool
    {

        return !empty($this->getCruise());
    }

    /**
     * @param int $cruise
     *
     * @return self
     */
    public function setCruise(int $cruise): self
    {
        $this->cruise = $cruise;

        return $this;
    }

    /**
     * @return int
     */
    public function getOnline(): int
    {

        return $this->online;
    }

    /**
     * @return boolean
     */
    public function isOnline(): bool
    {

        return !empty($this->getOnline());
    }

    /**
     * @param int $online
     *
     * @return self
     */
    public function setOnline(int $online): self
    {
        $this->online = $online;

        return $this;
    }

    /**
     * @return int
     */
    public function getKeyEvent(): int
    {

        return $this->keyEvent;
    }

    /**
     * @return boolean
     */
    public function isKeyEvent(): bool
    {

        return !empty($this->getKeyEvent());
    }

    /**
     * @param int $keyEvent
     *
     * @return self
     */
    public function setKeyEvent(int $keyEvent): self
    {
        $this->keyEvent = $keyEvent;

        return $this;
    }

    /**
     * @return int
     */
    public function getHasGuide(): int
    {

        return $this->hasGuide;
    }

    /**
     * @return boolean
     */
    public function isHasGuide(): bool
    {

        return !empty($this->getHasGuide());
    }

    /**
     * @param int $hasGuide
     *
     * @return self
     */
    public function setHasGuide(int $hasGuide): self
    {
        $this->hasGuide = $hasGuide;

        return $this;
    }

    /**
     * @return int
     */
    public function getDiscontinued(): int
    {

        return $this->discontinued;
    }

    /**
     * @return boolean
     */
    public function isDiscontinued(): bool
    {

        return !empty($this->getDiscontinued());
    }

    /**
     * @param int $discontinued
     *
     * @return self
     */
    public function setDiscontinued(int $discontinued): self
    {
        $this->discontinued = $discontinued;

        return $this;
    }

    /**
     * @return Collection|ConferenceSpecialties[]
     */
    public function getSpecialties(): Collection
    {

        return $this->specialties;
    }

    /**
     * @param ConferenceSpecialties $specialty
     *
     * @return self
     */
    public function addSpecialty(ConferenceSpecialties $specialty): self
    {
        if (!$this->specialties->contains($specialty)) {
            $specialty->setConference($this);
            $this->specialties->add($specialty);
        }

        return $this;
    }

    /**
     * @param ConferenceSpecialties $specialty
     *
     * @return self
     */
    public function removeSpecialty(ConferenceSpecialties $specialty): self
    {
        if ($this->specialties->contains($specialty)) {
            $this->specialties->removeElement($specialty);
        }

        return $this;
    }
}
