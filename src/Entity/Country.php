<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country.
 *
 * @ORM\Table(name="country")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 *
 * @since 1.0.0
 */
class Country
{
    /**
     * @var string
     *
     * @ORM\Column(name="iso_code", type="string", length=2, nullable=false)
     * @ORM\Id
     * #ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $isoCode;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="capital_city", type="string", length=50, nullable=true)
     */
    private $capitalCity;

    /**
     * Get isoCode.
     *
     * @return string
     */
    public function getIsoCode()
    {
        return $this->isoCode;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get Country Capital City Name
     *
     * @return string|null
     */
    public function getCapitalCity(): ?string
    {
        return $this->capitalCity;
    }

    /**
     * Set Country Capital City Name
     *
     * @param string|null $capitalCity
     *
     * @return self
     */
    public function setCapitalCity(?string $capitalCity): self
    {
        $this->capitalCity = $capitalCity;

        return $this;
    }
}
