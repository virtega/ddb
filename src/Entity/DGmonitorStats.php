<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DGmonitorStats
 *
 * @ORM\Table(name="dgmonitor_stats", indexes={@ORM\Index(name="IDX_DATE", columns={"date"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\DGMonitorRepository")
 */
class DGmonitorStats
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var int|null
     *
     * @ORM\Column(name="fb_id", type="integer", nullable=true)
     */
    private $fbId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", nullable=true, length=600)
     */
    private $title;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime|null $date
     */
    public function setDate(?\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int|null
     */
    public function getFbId(): ?int
    {
        return $this->fbId;
    }

    /**
     * @param int|null $fbId
     */
    public function setFbId(?int $fbId): void
    {
        $this->fbId = $fbId;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }
}
