<?php

namespace App\Entity;

use App\Entity\Family\DrugTypeFamily;
use Doctrine\ORM\Mapping as ORM;

/**
 * Brand.
 *
 * @ORM\Table(name="brand", indexes={@ORM\Index(name="valid", columns={"valid"}), @ORM\Index(name="generic", columns={"generic"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\Drug\BrandDrugRepository")
 *
 * @since 1.0.0
 */
class Brand extends DrugTypeFamily
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="valid", type="integer", nullable=false, options={"unsigned"=true,"comment"="Options: 1=True, 0=False"})
     */
    private $valid = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="AutoEncodeExclude", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $autoencodeexclude = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uid", type="string", length=32, nullable=true, options={"comment"="ID use by previous System"})
     */
    private $uid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comments", type="string", length=2000, nullable=true)
     */
    private $comments;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var Generic|null
     *
     * @ORM\ManyToOne(targetEntity="Generic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="generic", referencedColumnName="id")
     * })
     */
    private $generic;

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Brand
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set valid.
     *
     * @param int $valid
     *
     * @return Brand
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid.
     *
     * @return integer
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Set autoencodeexclude.
     *
     * @param int $autoencodeexclude
     *
     * @return Brand
     */
    public function setAutoencodeexclude($autoencodeexclude)
    {
        $this->autoencodeexclude = $autoencodeexclude;

        return $this;
    }

    /**
     * Get autoencodeexclude.
     *
     * @return integer
     */
    public function getAutoencodeexclude()
    {
        return $this->autoencodeexclude;
    }

    /**
     * Set uid.
     *
     * @param string $uid
     *
     * @return Brand
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid.
     *
     * @return string|null
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set generic.
     *
     * @param Generic|null $generic
     *
     * @return Brand
     */
    public function setGeneric(?Generic $generic = null)
    {
        $this->generic = $generic;

        return $this;
    }

    /**
     * Get generic.
     *
     * @return Generic|null
     */
    public function getGeneric()
    {
        return $this->generic;
    }

    /**
     * Set comments.
     *
     * @param string|null $comments
     *
     * @return Brand
     */
    public function setComments($comments = null)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments.
     *
     * @return string|null
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set creationDate.
     *
     * @param \DateTime|null $creationDate
     *
     * @return Brand
     */
    public function setCreationDate($creationDate = null)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate.
     *
     * @return \DateTime|null
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
}
