<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConferenceDetails, an Abstract Entity for Conference
 *
 * @since 1.2.0  Migrated via Version20190724074105
 */
class ConferenceDetails
{
    use \App\Entity\Traits\EntityDetailsTrait;

    /**
     * @var array
     */
    public static $defaults = [
        'state'   => '',
        'contact' => [
            'desc'  => '', // Multi Line
            'phone' => [],
            'fax'   => [],
            'email' => [],
        ],
        'links' => [],
        '_meta' => [
            'created_on'  => null,
            'created_by'  => null,
            'modified_on' => null,
            'modified_by' => null,
        ],
    ];

    /**
     * @return string
     */
    public function getDetailsState(): string
    {

        return $this->details['state'] ?? self::$defaults['state'];
    }

    /**
     * @param string $state
     *
     * @return self
     */
    public function setDetailsState(string $state): self
    {
        $this->details['state'] = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetailsContactDesc(): string
    {
        $value = $this->details['contact']['desc'] ?? self::$defaults['contact']['desc'];

        return $this->_readMultiLineValue($value);
    }

    /**
     * @param string $desc
     *
     * @return self
     */
    public function setDetailsContactDesc(string $desc): self
    {
        $this->details['contact']['desc'] = $this->_setMultiLineValue($desc);

        return $this;
    }

    /**
     * @return string[]
     */
    public function getDetailsContactPhones(): array
    {

        return $this->details['contact']['phone'] ?? self::$defaults['contact']['phone'];
    }

    /**
     * @param string[] $phones
     *
     * @return self
     */
    public function setDetailsContactPhones(array $phones): self
    {
        $this->details['contact']['phone'] = $phones;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getDetailsContactFaxes(): array
    {

        return $this->details['contact']['fax'] ?? self::$defaults['contact']['fax'];
    }

    /**
     * @param string[] $faxes
     *
     * @return self
     */
    public function setDetailsContactFaxes(array $faxes): self
    {
        $this->details['contact']['fax'] = $faxes;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getDetailsContactEmails(): array
    {

        return $this->details['contact']['email'] ?? self::$defaults['contact']['email'];
    }

    /**
     * @param string[] $emails
     *
     * @return self
     */
    public function setDetailsContactEmails(array $emails): self
    {
        $this->details['contact']['email'] = $emails;

        return $this;
    }

    /**
     * @return array[]
     */
    public function getDetailsContactLinks(): array
    {

        return $this->details['links'] ?? self::$defaults['links'];
    }

    /**
     * @param array[] $links
     *
     * @return self
     */
    public function setDetailsContactLinks(array $links): self
    {
        $this->details['links'] = $links;

        return $this;
    }

    /**
     * @param string $caption
     * @param string $url
     *
     * @return self
     */
    public function addDetailsContactLink(string $caption, string $url): self
    {
        $this->details['links'][] = [
            'caption' => $caption,
            'url'     => $url,
        ];

        return $this;
    }
}
