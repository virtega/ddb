<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Variable.
 *
 * @ORM\Table(name="variable")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\VariableRepository")
 *
 * @since 1.0.0
 */
class Variable
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @ORM\Id
     * #ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false, options={"comment"="Expected Data Type for processing"})
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value", type="text", length=65535, nullable=true, options={"comment"="Value Data"})
     */
    private $value;

    /**
     * Set name during construction.
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Variable
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value.
     *
     * @param mixed    $value
     * @param boolean  $skip_encoding
     *
     * @return Variable
     */
    public function setValue($value, $skip_encoding = false)
    {
        // defaulting
        $this->value = $value;

        if ($skip_encoding) {
            return $this;
        }

        // force checking Type first
        if (empty($this->getType())) {
            $type = gettype($value);

            // force change
            if ('object' == $type) {
                if ($value instanceof \DateTime) {
                    $type = 'datetime';
                }
            }

            $this->setType($type);
        }

        // encoding
        switch ($this->getType()) {
            case 'array':
                // force re-format
                $this->setType('json');
                // no break
            case 'object':
            case 'json':
                $value = json_encode($value);
                $this->value = (empty($value) ? null : $value);
                break;

            case 'datetime':
                if (
                    (is_object($this->value)) &&
                    (is_a($this->value, 'DateTime'))
                ) {
                    $this->value = $this->value->format('Y-m-d H:i:s');
                }
                elseif (is_numeric($this->value)) {
                    $value = (int) $value;
                    $this->value = date('Y-m-d H:i:s', $value);
                }
                elseif (is_string($this->value)) {
                    $value = strtotime($value);
                    $this->value = date('Y-m-d H:i:s', $value);
                }
                break;

            case 'bool':
                // force re-format
                $this->setType('boolean');
                // no break
                case 'boolean':
                    $this->value = (!empty($this->value) ? '1' : '0');
                    break;

            case 'int':
                // force re-format
                $this->setType('integer');
                // no break
            case 'integer':
            case 'float':
                $this->value = (string) $value;
                break;

            case 'text':
                $this->value = (string) $value;
                break;
        }

        return $this;
    }

    /**
     * Get value.
     *
     * $param  boolean  $skip_decoding
     *
     * @return mixed
     */
    public function getValue($skip_decoding = false)
    {
        // raw, or no value
        if ($skip_decoding) {
            return $this->value;
        }

        // default
        $value = $this->value;

        // decode
        switch ($this->getType()) {
            case 'object':
                if (!is_null($this->value)) {
                    $value = json_decode($this->value);
                }
                break;

            case 'array':
            case 'json':
                if (!is_null($this->value)) {
                    $value = json_decode($this->value, true);
                }
                break;

            case 'datetime':
                if (!is_null($this->value)) {
                    $value = \DateTime::createFromFormat('Y-m-d H:i:s', $this->value);
                }
                break;

            case 'bool':
            case 'boolean':
                $value = (int) $this->value;
                $value = (!empty($value));
                break;

            case 'int':
            case 'integer':
                $value = (int) $this->value;
                break;

            case 'float':
                $value = (float) $this->value;
                break;

            case 'text':
                $value = (string) $this->value;
                break;
        }

        return $value;
    }
}
