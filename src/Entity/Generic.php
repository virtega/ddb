<?php

namespace App\Entity;

use App\Entity\Family\DrugTypeFamily;
use Doctrine\ORM\Mapping as ORM;

/**
 * Generic.
 *
 * @ORM\Table(name="generic", indexes={@ORM\Index(name="valid", columns={"valid"}), @ORM\Index(name="experimental", columns={"experimental"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\Drug\GenericDrugRepository")
 *
 * @since 1.0.0
 */
class Generic extends DrugTypeFamily
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="valid", type="integer", nullable=false, options={"unsigned"=true,"comment"="Options: 1=True, 0=False"})
     */
    private $valid = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Level1", type="string", length=5, nullable=true)
     */
    private $level1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Level2", type="string", length=5, nullable=true)
     */
    private $level2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Level3", type="string", length=5, nullable=true)
     */
    private $level3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Level4", type="string", length=5, nullable=true)
     */
    private $level4;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Level5", type="string", length=5, nullable=true)
     */
    private $level5;

    /**
     * @var int
     *
     * @ORM\Column(name="NoDoubleBounce", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $nodoublebounce = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="AutoEncodeExclude", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $autoencodeexclude = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uid", type="string", length=32, nullable=true, options={"comment"="ID use by previous System"})
     */
    private $uid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comments", type="string", length=2000, nullable=true)
     */
    private $comments;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var Experimental|null
     *
     * @ORM\ManyToOne(targetEntity="Experimental")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="experimental", referencedColumnName="id")
     * })
     */
    private $experimental;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Generic
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set valid.
     *
     * @param int $valid
     *
     * @return Generic
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid.
     *
     * @return int
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Set level1.
     *
     * @param string|null $level1
     *
     * @return Generic
     */
    public function setLevel1($level1 = null)
    {
        $this->level1 = $level1;

        return $this;
    }

    /**
     * Get level1.
     *
     * @return string|null
     */
    public function getLevel1()
    {
        return $this->level1;
    }

    /**
     * Set level2.
     *
     * @param string|null $level2
     *
     * @return Generic
     */
    public function setLevel2($level2 = null)
    {
        $this->level2 = $level2;

        return $this;
    }

    /**
     * Get level2.
     *
     * @return string|null
     */
    public function getLevel2()
    {
        return $this->level2;
    }

    /**
     * Set level3.
     *
     * @param string|null $level3
     *
     * @return Generic
     */
    public function setLevel3($level3 = null)
    {
        $this->level3 = $level3;

        return $this;
    }

    /**
     * Get level3.
     *
     * @return string|null
     */
    public function getLevel3()
    {
        return $this->level3;
    }

    /**
     * Set level4.
     *
     * @param string|null $level4
     *
     * @return Generic
     */
    public function setLevel4($level4 = null)
    {
        $this->level4 = $level4;

        return $this;
    }

    /**
     * Get level4.
     *
     * @return string|null
     */
    public function getLevel4()
    {
        return $this->level4;
    }

    /**
     * Set level5.
     *
     * @param string|null $level5
     *
     * @return Generic
     */
    public function setLevel5($level5 = null)
    {
        $this->level5 = $level5;

        return $this;
    }

    /**
     * Get level5.
     *
     * @return string|null
     */
    public function getLevel5()
    {
        return $this->level5;
    }

    /**
     * Set nodoublebounce.
     *
     * @param int $nodoublebounce
     *
     * @return Generic
     */
    public function setNodoublebounce($nodoublebounce)
    {
        $this->nodoublebounce = $nodoublebounce;

        return $this;
    }

    /**
     * Get nodoublebounce.
     *
     * @return int
     */
    public function getNodoublebounce()
    {
        return $this->nodoublebounce;
    }

    /**
     * Set autoencodeexclude.
     *
     * @param int $autoencodeexclude
     *
     * @return Generic
     */
    public function setAutoencodeexclude($autoencodeexclude)
    {
        $this->autoencodeexclude = $autoencodeexclude;

        return $this;
    }

    /**
     * Get autoencodeexclude.
     *
     * @return int
     */
    public function getAutoencodeexclude()
    {
        return $this->autoencodeexclude;
    }

    /**
     * Set uid.
     *
     * @param string|null $uid
     *
     * @return Generic
     */
    public function setUid($uid = null)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid.
     *
     * @return string|null
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set experimental.
     *
     * @param Experimental|null $experimental
     *
     * @return Generic
     */
    public function setExperimental(?Experimental $experimental = null)
    {
        $this->experimental = $experimental;

        return $this;
    }

    /**
     * Get experimental.
     *
     * @return Experimental|null
     */
    public function getExperimental()
    {
        return $this->experimental;
    }

    /**
     * Set comments.
     *
     * @param string|null $comments
     *
     * @return Generic
     */
    public function setComments($comments = null)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments.
     *
     * @return string|null
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set creationDate.
     *
     * @param \DateTime|null $creationDate
     *
     * @return Generic
     */
    public function setCreationDate($creationDate = null)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate.
     *
     * @return \DateTime|null
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
}
