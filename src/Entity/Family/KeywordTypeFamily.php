<?php

namespace App\Entity\Family;

/**
 * Drugs Keywords Family.
 *
 * This class introduce parent class to All Drugs Typo & Synonyms related Entities.
 * Mostly for Validation purposes.
 *
 * @since 1.0.0
 */
class KeywordTypeFamily
{
}
