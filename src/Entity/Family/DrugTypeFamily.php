<?php

namespace App\Entity\Family;

/**
 * Drugs Family.
 *
 * This class introduce parent class to All Drugs related Entities.
 * Mostly for Validation purposes.
 *
 * @since 1.0.0
 */
class DrugTypeFamily
{
    /**
     * @var array
     */
    public static $mainDrugs = array('experimental', 'generic', 'brand');

    /**
     * @var array
     */
    public static $mainDrugsAny = array('experimental', 'generic', 'brand', 'any');

    /**
     * @var array
     */
    public static $mainKeywords = array('typo', 'synonym');

    /**
     * @var array
     */
    public static $family = array(
        'experimental',
        'generic',
        'generic_typo',
        'generic_synonym',
        'brand',
        'brand_typo',
        'brand_synonym',
    );
}
