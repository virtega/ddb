<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConferenceSpecialties
 *
 * @ORM\Table(name="conference_specialties", indexes={
 *     @ORM\Index(name="FK_E4F3796CE066A6EC", columns={"specialty_id"}),
 *     @ORM\Index(name="FK_E4F3796C604B8382", columns={"conference_id"})
 * })
 * @ORM\Entity
 *
 * @since 1.2.0  Migrated via Version20190724074100
 */
class ConferenceSpecialties
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Conference
     *
     * @ORM\ManyToOne(targetEntity="Conference", inversedBy="specialties")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(
     *         name="conference_id",
     *         referencedColumnName="id",
     *         nullable=false,
     *         onDelete="NO ACTION"
     *     )
     * })
     */
    private $conference;

    /**
     * @var SpecialtyTaxonomy
     *
     * @ORM\ManyToOne(targetEntity="SpecialtyTaxonomy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(
     *       name="specialty_id",
     *       referencedColumnName="id",
     *       nullable=false,
     *       onDelete="NO ACTION"
     *   )
     * })
     */
    private $specialty;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {

        return $this->id;
    }

    /**
     * @return Conference
     */
    public function getConference(): Conference
    {

        return $this->conference;
    }

    /**
     * @param Conference $conference
     *
     * @return self
     */
    public function setConference(Conference $conference): self
    {
        $this->conference = $conference;

        return $this;
    }

    /**
     * @return SpecialtyTaxonomy
     */
    public function getSpecialty(): SpecialtyTaxonomy
    {
        return $this->specialty;
    }

    /**
     * @param SpecialtyTaxonomy $specialty
     *
     * @return self
     */
    public function setSpecialty(SpecialtyTaxonomy $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }
}
