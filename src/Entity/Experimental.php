<?php

namespace App\Entity;

use App\Entity\Family\DrugTypeFamily;
use Doctrine\ORM\Mapping as ORM;

/**
 * Experimental.
 *
 * @ORM\Table(name="experimental", indexes={@ORM\Index(name="type", columns={"type"}), @ORM\Index(name="valid", columns={"valid"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\Drug\ExperimentalDrugRepository")
 *
 * @since 1.0.0
 */
class Experimental extends DrugTypeFamily
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=15, nullable=false, options={"default"="experimental","comment"="Options: experimental, generic, brand"})
     */
    private $type = 'experimental';

    /**
     * @var int
     *
     * @ORM\Column(name="valid", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $valid = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uid", type="string", length=32, nullable=true, options={"comment"="ID use by previous System"})
     */
    private $uid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comments", type="string", length=2000, nullable=true)
     */
    private $comments;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Experimental
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Experimental
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set valid.
     *
     * @param int $valid
     *
     * @return Experimental
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid.
     *
     * @return integer
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Set uid.
     *
     * @param string $uid
     *
     * @return Experimental
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid.
     *
     * @return string|null
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set comments.
     *
     * @param string|null $comments
     *
     * @return Experimental
     */
    public function setComments($comments = null)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments.
     *
     * @return string|null
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set creationDate.
     *
     * @param \DateTime|null $creationDate
     *
     * @return Experimental
     */
    public function setCreationDate($creationDate = null)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate.
     *
     * @return \DateTime|null
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
}
