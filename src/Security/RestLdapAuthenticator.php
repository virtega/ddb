<?php

namespace App\Security;

use App\Security\User\LdapUser;
use App\Security\User\LdapUserProvider;
use App\Utility\Traits\RouteCheckTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * REST + LDAP User Authenticator.
 * This is use to Authenticate API Request.
 * UI API request use session-id instead.
 *
 * @since  1.0.0
 */
class RestLdapAuthenticator extends AbstractGuardAuthenticator
{
    use RouteCheckTrait;

    /**
     * Bind from env(SECURITY_API_TOKEN)
     *
     * @var string
     */
    private $securityApiToken;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface  $router
     * @param string           $securityApiToken
     */
    public function __construct(RouterInterface $router, string $securityApiToken)
    {
        $this->router           = $router;
        $this->securityApiToken = $securityApiToken;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request): bool
    {
        $tokens = $this->extractCredential($request);
        return (
            (RouteCheckTrait::validateRequestOnApi($request)) &&
            (count($tokens) > 0)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request): array
    {
        return $this->extractCredential($request);
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $userProvider): UserInterface
    {
        if (empty($credentials)) {
            // @codeCoverageIgnoreStart
            throw new AuthenticationException('Missing Security Tokens.');
            // @codeCoverageIgnoreEnd
        }

        if (!$userProvider instanceof LdapUserProvider) {
            // @codeCoverageIgnoreStart
            throw new AuthenticationException(sprintf('Unsupported User Provider, Expected "LdapUserProvider", Received "%s".', get_class($userProvider)));
            // @codeCoverageIgnoreEnd
        }

        if (empty($credentials['token'])) {
            // @codeCoverageIgnoreStart
            throw new AuthenticationException('Your Security Token is Missing.');
            // @codeCoverageIgnoreEnd
        }

        if (empty($credentials['username'])) {
            // @codeCoverageIgnoreStart
            throw new AuthenticationException('Your Security Token Missing Username.');
            // @codeCoverageIgnoreEnd
        }

        if (empty($credentials['password'])) {
            // @codeCoverageIgnoreStart
            throw new AuthenticationException('Your Security Token Missing Password.');
            // @codeCoverageIgnoreEnd
        }

        if ($credentials['token'] != $this->securityApiToken) {
            throw new AuthenticationException('Your Security Token is Invalid.');
        }

        try {
            $userProvider->bindingLdapForQuery($credentials['username'], $credentials['password']);
            return $userProvider->loadUserByUsername($credentials['username']);
        }
        // @codeCoverageIgnoreStart
        catch (\Exception $e) {
            throw new AuthenticationException($e->getMessage());
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * {@inheritdoc}
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        if (!$user instanceof LdapUser) {
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // yes do nothing; let it process over router
        // - We also embed on EventListener to handle the output.
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // do nothing just re-throw previous error
        throw $exception;
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        $tokens = $this->extractCredential($request);
        if ((!RouteCheckTrait::validateRequestOnApi($request)) || (count($tokens) === 0)) {
            $url = $this->router->generate('login');
            return new RedirectResponse($url);
        }

        $msg = 'Invalid Access Token.';
        if (
            (!empty($authException)) &&
            ($authException instanceof \Exception)
        ) {
            $msg = $authException->getMessage();
        }

        $payload = array(
            'msg'     => $msg,
            'success' => false,
        );

        $response = new Response();
        $response->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        $response->setContent(json_encode($payload));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function supportsRememberMe(): bool
    {
        return false;
    }

    /**
     * Method to extract Credential from header
     *
     * @param Request $request
     *
     * @return array
     */
    private function extractCredential(Request $request): array
    {
        $found = array();

        // either
        foreach (array('X-AUTH-TOKEN', 'X-API-TOKEN') as $head) {
            $token = $request->headers->get($head);
            if (!empty($token)) {
                $found['token'] = $token;
            }
        }

        // get from realm
        $token = $request->headers->get('Authorization');
        if (!empty($token)) {
            foreach (array('php-auth-user' => 'username', 'php-auth-pw' => 'password') as $head => $key) {
                $token = $request->headers->get($head);
                if (!empty($token)) {
                    $found[$key] = $token;
                }
            }
        }

        return $found;
    }
}
