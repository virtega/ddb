<?php

namespace App\Security\Http;

use App\Service\LoginCookieService;
use App\Utility\Traits\FlashBagTrait;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;

class SessionLogoutHandler implements LogoutHandlerInterface
{
    /**
     * @var LoginCookieService
     */
    private $cookie;

    public function __construct(LoginCookieService $cookie)
    {
        $this->cookie = $cookie;
    }


    /**
     * {@inheritdoc}
     */
    public function logout(Request $request, Response $response, TokenInterface $token)
    {
        /**
         * @var \Symfony\Component\HttpFoundation\Session\Session
         */
        $session = $request->getSession();
        if (!empty($session)) {

            $this->cookie->setCookie($request, $response, $token);

            $session->invalidate();
            FlashBagTrait::setOffFlashMsg($session, 'info', 'You have logged out.');
        }
    }
}
