<?php

namespace App\Security\Http;

use App\Security\User\LdapUser;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Refresh Session
 * Listen to all request, and update session date-stamps.
 * Session only were set as use logs in, this renew it.
 * Also in play, HTML meta-refresh tag.
 *
 * @since  1.0.1 UIC
 */
class UserSessionLongevityEventListener
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param SessionInterface       $session
     * @param TokenStorageInterface  $tokenStorage
     */
    public function __construct(SessionInterface $session, TokenStorageInterface $tokenStorage)
    {
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Tagged to response to every kernel request.
     * This refresh the cookie each request is made.
     * If it has expire, lets Authenticator catch it.
     *
     * @param  GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }

        if ($token = $this->tokenStorage->getToken()) {
            if (
                ($user = $token->getUser()) &&
                (is_object($user)) &&
                (!($user instanceof AnonymousToken)) &&
                ($user instanceof LdapUser)
            ) {
                if ((time() - $this->session->getMetadataBag()->getLastUsed()) < $this->session->getMetadataBag()->getLifetime()) {
                    // @codeCoverageIgnoreStart
                    $this->session->migrate();
                    // @codeCoverageIgnoreEnd
                }
            }
        }
    }
}
