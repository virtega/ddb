<?php

namespace App\Security\User;

use App\Entity\UserRoles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Ldap\Adapter\QueryInterface;
use Symfony\Component\Ldap\Entry;
use Symfony\Component\Ldap\Exception\ConnectionException;
use Symfony\Component\Ldap\Exception\LdapException;
use Symfony\Component\Ldap\Exception\NotBoundException;
use Symfony\Component\Ldap\LdapInterface;
use Symfony\Component\Security\Core\Exception\InvalidArgumentException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * LDAP User Provider.
 * Here credential will be bind to LDAP. Then search user profile.
 * Once user profile is return, this will query local database (UserRoles) to attached predefined user roles.
 *
 * However during any of above process failed, it will FORCE to generate bad-credential (UsernameNotFoundException) error.
 * - Why? we don't use RO search user. Given will be bind and query.
 *
 * @since  1.0.0
 */
class LdapUserProvider implements UserProviderInterface
{
    /**
     * @var string  AD User Prefix by default
     */
    const USERNAME_PREFIX = 'PSL\\';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \App\Repository\UserRolesRepository<UserRoles>
     */
    private $userRolesRepo;

    /**
     * @var LdapInterface
     */
    private $ldap;

    /**
     * Additional filter.
     * ENV able to inject Special Group query permission here.
     *
     * @var  string
     */
    private $additionalQuery = '(objectClass=user)';

    /**
     * Base Search DN
     *
     * @var string
     */
    private $baseDn = 'DC=pslgroup,DC=com';

    /**
     * AD Search Query
     *
     * @var string
     */
    private $searcQuery = '(|(mail={email})(userPrincipalName={email})(proxyAddresses=smtp:*{email}*)(sAMAccountName={username}))';

    /**
     * @var array
     */
    public static $defaultRoles = array( UserRoles::VISITOR );

    /**
     * AD Username Attr Key
     *
     * @var string
     */
    private $userNameKey = 'sAMAccountName';

    /**
     * Flag if LDAP has been bind
     *
     * @var boolean
     */
    private $ldapBinded = false;

    /**
     * OPT: Attribute Filters
     *
     * @var  array
     */
    private $optFilters = array(
        'co',
        'displayName',
        'distinguishedName',
        'givenName',
        'mail',
        'memberOf',
        'name',
        'objectClass',
        'pager',
        'sAMAccountName',
        'title',
    );

    /**
     * OPT: Maximum profile search result
     * Minimal just so stop request bot.
     *
     * @var  int
     */
    private $optMaxItems = 3;

    /**
     * @param EntityManagerInterface  $em
     * @param LdapInterface           $ldap
     * @param string                  $additionalQuery  Replacement LDAP query to filter down user, this will be appended in the final query
     * @param string|null             $baseDn           Replacement LDAP Base DN string
     * @param string|null             $searcQuery       Replacement LDAP Base search Query filter, $additionalQuery will be append nonetheless
     * @param string|null             $userNameKey      Replacement of LDAP attribute name hold Username - this will be use to identify on LdapUser & UserRolesEntity
     */
    public function __construct(EntityManagerInterface $em, LdapInterface $ldap, string $additionalQuery, ?string $baseDn = null, ?string $searcQuery = null, ?string $userNameKey = null)
    {
        $this->em               = $em;
        $this->ldap             = $ldap;
        $this->additionalQuery  = $additionalQuery;
        $this->baseDn           = $baseDn       ?? $this->baseDn;
        $this->searcQuery       = $searcQuery   ?? $this->searcQuery;
        $this->userNameKey      = $userNameKey  ?? $this->userNameKey;

        $this->userRolesRepo    = $em->getRepository(UserRoles::class);
    }

    /**
     * Method to binding LDAP with Username & Password
     *
     * @param string $username  Either full email address or "DOMAIN\Username" only
     * @param string $password
     */
    public function bindingLdapForQuery(string $username, string $password): void
    {

        if (
            (strpos($username, '@') === false) &&
            (strpos($username, self::USERNAME_PREFIX) === false)
        ) {
            $username = self::USERNAME_PREFIX . $username;
        }

        $this->ldap->bind($username, $password);
        $this->ldapBinded = true;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($email)
    {
        if (!$this->ldapBinded) {
            throw new UsernameNotFoundException('Binding were skipped.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        try {
            // inject username
            $ecapedUser = $this->ldap->escape($email, '', LdapInterface::ESCAPE_FILTER);

            // clean up username
            $username = $ecapedUser;
            if (strpos($email, '@') === false) {
                $username = str_replace(self::USERNAME_PREFIX, '', $email);
                $username = $this->ldap->escape($username, '', LdapInterface::ESCAPE_FILTER);
            }

            $query = $this->searcQuery;
            $query = str_replace('{email}',    $ecapedUser, $query);
            $query = str_replace('{username}', $username, $query);

            // inject additional query
            $query      = '(&' . $query . $this->additionalQuery . ')';

            // apply attribute filters
            $options = array(
                'filter'   => $this->optFilters,
                'maxItems' => $this->optMaxItems
            );

            // main search
            $search     = $this->ldap->query($this->baseDn, $query, $options);
            // @codeCoverageIgnoreStart
        } catch (ConnectionException $e) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $email), Response::HTTP_FORBIDDEN, $e);
        }
        // @codeCoverageIgnoreEnd

        try {
            /**
             * execute search: query will stop if bind-user credential is wrong
             * @var Entry[]
             */
            $entries = $search->execute();
            // @codeCoverageIgnoreStart
        } catch (LdapException $e) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $email), Response::HTTP_FORBIDDEN, $e);
        }
        // @codeCoverageIgnoreEnd
        $count = \count($entries);

        if (!$count) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $email), Response::HTTP_FORBIDDEN);
        }

        if ($count > 1) {
            // @codeCoverageIgnoreStart
            throw new UsernameNotFoundException('More than one user found.', Response::HTTP_FORBIDDEN);
            // @codeCoverageIgnoreEnd
        }

        $entry = $entries[0];

        try {
            $username = $this->getAttributeValue($entry, $this->userNameKey);
            // @codeCoverageIgnoreStart
        } catch (InvalidArgumentException $e) {
            throw new UsernameNotFoundException(sprintf('User object different from expected format, missing "%s".', $this->userNameKey), Response::HTTP_FORBIDDEN);
        }
        // @codeCoverageIgnoreEnd

        return $this->loadUser($username, $entry);
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof LdapUser) {
            // @codeCoverageIgnoreStart
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)), Response::HTTP_INTERNAL_SERVER_ERROR);
            // @codeCoverageIgnoreEnd
        }

        // copy previous login DT
        $dt = $user->getLastLogin();

        $user = new LdapUser($user->getUsername(), null, $user->getRoles(), $user->getLdapEntry());

        if (!empty($dt)) {
            $user->setLastLogin(clone $dt);
        }

        $this->userRolesRepo->furnishLdapUserRoles($user, false);

        return $user;
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function supportsClass($class)
    {
        return ('App\Security\LdapUser' === $class);
    }

    /**
     * {@inheritdoc}
     *
     * @param string  $username
     * @param Entry   $entry
     */
    protected function loadUser($username, Entry $entry): LdapUser
    {
        $user = new LdapUser($username, null, self::$defaultRoles, $entry);

        $this->userRolesRepo->furnishLdapUserRoles($user, true);

        return $user;
    }

    /**
     * Fetches a required unique attribute value from an LDAP entry.
     *
     * @param Entry   $entry
     * @param string  $attribute
     *
     * @return mixed
     *
     * @throws InvalidArgumentException    If there are no attribute on Entry, or have multiple value (result)
     */
    private function getAttributeValue(Entry $entry, $attribute)
    {
        if (!$entry->hasAttribute($attribute)) {
            // @codeCoverageIgnoreStart
            throw new InvalidArgumentException(sprintf('Missing attribute "%s" for user "%s".', $attribute, $entry->getDn()));
            // @codeCoverageIgnoreEnd
        }

        $values = $entry->getAttribute($attribute);

        if (
            (empty($values)) ||
            (1 !== \count($values))
        ) {
            // @codeCoverageIgnoreStart
            throw new InvalidArgumentException(sprintf('Attribute "%s" has multiple values.', $attribute));
            // @codeCoverageIgnoreEnd
        }

        return $values[0];
    }
}
