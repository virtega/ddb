<?php

namespace App\Security\User;

use Symfony\Component\Ldap\Entry;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * LDAP User.
 * Several additional attached to UserInterface.
 * This allow we to access the LdapEntry anywhere else needed.
 *
 * @since  1.0.0
 */
class LdapUser implements UserInterface, EquatableInterface
{
    /**
     * LDAP Attribute: "sAMAccountName"
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var array
     */
    protected $roles;

    /**
     * @var Entry
     */
    protected $ldapEntry;

    /**
     * LDAP Attribute: "name"
     * @var string
     */
    protected $displayName;

    /**
     * LDAP Attribute: "givenName"
     * @var string
     */
    protected $nickName;

    /**
     * LDAP Attribute: strtolower("mail")
     * @var string
     */
    protected $email;

    /**
     * LDAP Attribute: "title"
     * @var string
     */
    protected $position;

    /**
     * @var ?\DateTimeInterface
     */
    protected $lastLogin = null;

    /**
     * {@inheritdoc}
     */
    public function __construct(?string $username, ?string $password = null, array $roles, Entry $ldapEntry)
    {
        if ('' === $username || null === $username) {
            throw new \InvalidArgumentException('The username cannot be empty.');
        }

        $this->username    = $this->extractSingleValueByKeyFromEntry($ldapEntry, 'sAMAccountName', $username);
        $this->password    = ($password ?? '');
        $this->roles       = $roles;
        $this->ldapEntry   = $ldapEntry;

        $this->displayName = $this->extractSingleValueByKeyFromEntry($ldapEntry, 'name', $username);
        $this->nickName    = $this->extractSingleValueByKeyFromEntry($ldapEntry, 'givenName', '');
        $this->email       = strtolower($this->extractSingleValueByKeyFromEntry($ldapEntry, 'mail', $username));
        $this->position    = $this->extractSingleValueByKeyFromEntry($ldapEntry, 'title', '');
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @return Entry
     */
    public function getLdapEntry(): Entry
    {
        return $this->ldapEntry;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * Method to set/override User Roles.
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function getSalt()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Method to get User Email Address
     *
     * @return  string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Method to get User First name
     *
     * @return  string
     */
    public function getNickName(): string
    {
        return $this->nickName;
    }

    /**
     * Method to get User Title Position
     *
     * @return  string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function eraseCredentials()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof LdapUser) {
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }

        if ($this->getUsername() !== $user->getUsername()) {
            return false;
        }

        if ($this->getEmail() !== $user->getEmail()) {
            return false;
        }

        if ($this->getRoles() !== $user->getRoles()) {
            return false;
        }

        return true;
    }

    /**
     * @return ?\DateTimeInterface
     */
    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    /**
     * @param \DateTimeInterface $lastLogin
     */
    public function setLastLogin(\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Extracts single value from entry's array value by key.
     *
     * @param Entry          $entry             Ldap entry
     * @param string         $key               Key
     * @param null|mixed     $defaultValue      Default value
     *
     * @return mixed|null
     */
    protected function extractSingleValueByKeyFromEntry(Entry $entry, string $key, $defaultValue = null)
    {
        $value = $this->extractFromLdapEntry($entry, $key, $defaultValue);

        return ((is_array($value) && isset($value[0])) ? $value[0] : $defaultValue);
    }

    /**
     * Extracts value from entry by key.
     *
     * @param Entry          $entry             Ldap entry
     * @param string         $key               Key
     * @param mixed|null     $defaultValue      Default value
     *
     * @return mixed|null
     */
    protected function extractFromLdapEntry(Entry $entry, string $key, $defaultValue = null)
    {
        if (!$entry->hasAttribute($key)) {
            // @codeCoverageIgnoreStart
            return $defaultValue;
            // @codeCoverageIgnoreEnd
        }

        return $entry->getAttribute($key);
    }
}
