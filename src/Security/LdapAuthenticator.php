<?php

namespace App\Security;

use App\Security\User\LdapUser;
use App\Security\User\LdapUserProvider;
use App\Service\LoginCookieService;
use App\Utility\Traits\FlashBagTrait;
use App\Utility\Traits\RouteCheckTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * LDAP User Authenticator.
 * This is use for UI Authentication.
 * Once authenticate SAME LdapUserProvider use for API request by Session.
 *
 * @since  1.0.0
 */
class LdapAuthenticator extends AbstractGuardAuthenticator
{
    use FlashBagTrait;
    use RouteCheckTrait;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var LoginCookieService
     */
    private $cookie;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router, Security $security, LoginCookieService $cookie, string $appEnv)
    {
        $this->router   = $router;
        $this->security = $security;
        $this->cookie   = $cookie;

        FlashBagTrait::setEnv($appEnv);
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request): bool
    {
        // if there is already an authenticated user (likely due to the session)
        // then return false and skip authentication: there is no need.
        if ($this->security->getUser()) {
            return false;
        }

        return (
            ('/login' == $request->getPathInfo()) &&
            ($request->isMethod('POST')) &&
            (!empty($request->request->get('_email'))) &&
            (!empty($request->request->get('_password')))
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request): array
    {
        return array(
            'email'    => $request->request->get('_email'),
            'password' => $request->request->get('_password'),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $userProvider): UserInterface
    {
        if (!$userProvider instanceof LdapUserProvider) {
            // @codeCoverageIgnoreStart
            throw new AuthenticationException(sprintf('Unsupported User Provider, Expected "LdapUserProvider", Received "%s".', get_class($userProvider)));
            // @codeCoverageIgnoreEnd
        }

        try {
            $userProvider->bindingLdapForQuery($credentials['email'], $credentials['password']);
            return $userProvider->loadUserByUsername($credentials['email']);
        }
        catch (\Exception $e) {
            throw new AuthenticationException($e->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        if (!$user instanceof LdapUser) {
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }

        if (empty($credentials['email'])) {
            // @codeCoverageIgnoreStart
            throw new AuthenticationException('Email should not be empty.');
            // @codeCoverageIgnoreEnd
        }

        if (empty($credentials['password'])) {
            // @codeCoverageIgnoreStart
            throw new AuthenticationException('Password should not be empty.');
            // @codeCoverageIgnoreEnd
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $url = $request->request->get('_target_path');
        if (empty($url)) {
            $url = $this->router->generate('dashboard');
        }
        $response = new RedirectResponse($url);

        $this->cookie->setCookie($request, $response, $token);

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {

        FlashBagTrait::setOffFlashMsgByRequest($request, 'danger', $exception->getMessage());
    }

    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        // if API request
        if (RouteCheckTrait::validateRequestOnApi($request)) {
            // For fallback if supports() failed. @todo review
            // @codeCoverageIgnoreStart
            $msg = 'Invalid Access Token.';
            if (
                (!empty($authException)) &&
                ($authException instanceof \Exception)
            ) {
                $msg = $authException->getMessage();
            }

            $payload = array(
                'msg'     => $msg,
                'success' => false,
            );

            $response = new Response();
            $response->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
            $response->setContent(json_encode($payload));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
            // @codeCoverageIgnoreEnd
        }

        $params = array();
        if (in_array($request->attributes->get('_route'), array('root', 'login')) === false) {
            $params['redirect'] = $this->router->generate(
                $request->attributes->get('_route'),
                $request->attributes->get('_route_params')
            );

            FlashBagTrait::setOffFlashMsgByRequest($request, 'danger', 'Your session has expired, please login.');
        }

        $url = $this->router->generate('login', $params);
        return new RedirectResponse($url);
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function supportsRememberMe(): bool
    {
        return true;
    }
}
