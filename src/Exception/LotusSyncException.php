<?php

namespace App\Exception;

use App\Utility\Traits\RouteCheckTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @codeCoverageIgnore This is not testable at this stage
 *
 * Masking Exception for both PDO & General Exception for Lotus MsSQl Sync Process.
 *
 * Response should always return as Complete / 200, however
 * - Notify client/user the process failed
 * - Process has to be revert;
 *     - New Entity need to be removed
 *     - Update Entity attribute need to be revert
 * - This class responsible
 *     - Prepare RESPONSE - by identify type of request; UI / API
 *     - Log the Error
 * - Then, on CoreService
 *     - it will re-catch this exception
 *     - revert the local action - Creation / Update
 *     - then return the "RESPONSE", @see ExceptionListener
 */
class LotusSyncException extends \Exception
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var LotusConnectionException
     * @var \PDOException
     * @var \Exception
     */
    private $previous;

    /**
     * @param \PDOException|\Exception  $previous
     * @param LoggerInterface           $logger
     * @param string|null               $message
     * @param int                       $code
     */
    public function __construct(\Exception $previous, LoggerInterface $logger, ?string $message = null, int $code = Response::HTTP_UNPROCESSABLE_ENTITY)
    {
        $this->previous = $previous;
        $this->logger   = $logger;

        if (empty($message)) {
            $message = $this->getErrorMessage();
        }

        $this->message = $message;

        $this->writeLog($message);

        parent::__construct($message, $code);
    }

    /**
     * Method to get User level message.
     *
     * @return string
     */
    private function getErrorMessage(): string
    {
        $message = 'Unable to sync the request to Lotus. Unknown Error. Request will be revert.';

        do {
            if (empty($this->previous)) {
                break;
            }

            if ($this->previous instanceof LotusConnectionException) {
                $message = rtrim($this->previous->getMessage(), '.') . '. Your request has been canceled.';
                break;
            }

            if ($this->previous instanceof \PDOException) {
                $message = 'Lotus Database Up-Sync failed at SQL level. Your request has been canceled.';
                break;
            }

            if ($this->previous instanceof \Exception) {
                $message = 'Lotus Database Up-Sync failed at PHP level. Your request has been canceled.';
                break;
            }
        } while(false);

        return $message;
    }

    /**
     * Method to capture Exception and log them.
     *
     * @param string|null  $message
     */
    private function writeLog(?string $message = null): void
    {
        if (empty($this->logger)) {
            return;
        }

        $context = array();
        if (empty($this->previous)) {
            $context = array('LotusSyncException' => 'Unknown Error');
        }
        else {
            $context['class']   = get_class($this->previous);
            $context['message'] = $this->previous->getMessage();
            $context['code']    = $this->previous->getCode();
            if (method_exists($this->previous, 'getFile')) {
                $context['file'] = $this->previous->getFile();
            }
            if (method_exists($this->previous, 'getLine')) {
                $context['line'] = $this->previous->getLine();
            }
            if (property_exists($this->previous, 'errorInfo')) {
                $context['error-info'] = $this->previous->errorInfo;
            }
        }

        $message =  $message ?? 'Lotus-Sync failed';
        do {
            if (empty($this->previous)) {
                break;
            }

            if ($this->previous instanceof \PDOException) {
                $message = 'Lotus-Sync failed PDO Error';
                break;
            }

            if ($this->previous instanceof \Exception) {
                $message = 'Lotus-Sync failed PHP Error';
                break;
            }
        } while(false);

        $this->logger->error($message, $context);
    }
}
