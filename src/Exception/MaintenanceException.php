<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Masking Exception for Maintenance Mode.
 * Allow Handler to append header for Retry-After
 *
 * Response sent back ERROR 400
 * - 503 The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay.
 */
class MaintenanceException extends \Exception
{
    /**
     * @var  string  Second - Retry-After heading value
     */
    const RETRY_AFTER = '3600';

    /**
     * @param string $message  Message of exception
     * @param int    $code     Code of exception
     */
    public function __construct(string $message = 'Offline, Maintenance Mode.', int $code = Response::HTTP_SERVICE_UNAVAILABLE)
    {

        parent::__construct($message, $code);
    }
}
