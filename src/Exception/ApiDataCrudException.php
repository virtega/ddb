<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Masking error that normally cause by client
 * - Missing subject / pre-required alpha/delta
 * - Unknown format / syntax
 *
 * Response sent back ERROR 400
 * - 400 Bad Request response status code indicates that the server could not understand the request due to invalid syntax.
 *
 * @since  1.2.1
 */
class ApiDataCrudException extends \Exception
{
    /**
     * @param string  $message  Message of exception
     * @param int     $code     Code of exception
     */
    public function __construct(string $message = '', int $code = Response::HTTP_BAD_REQUEST)
    {

        parent::__construct($message, $code);
    }
}
