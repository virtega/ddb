<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Masking Error if API request required Attribute is missing/malformed.
 *
 * Response sent back ERROR 400
 * - 400 Bad Request response status code indicates that the server could not understand the request due to invalid syntax.
 */
class ApiIncompleteException extends \Exception
{
    const MESSAGE = 'Invalid Request, Missing Argument';

    /**
     * @var string
     */
    private $arg;

    /**
     * @param string  $arg      Argument Name / Reference String
     * @param string  $message  Message of exception
     * @param int     $code     Code of exception
     */
    public function __construct(string $arg, string $message = '', $code = Response::HTTP_BAD_REQUEST)
    {
        $this->arg = $arg;

        if (empty($message)) {
            $message = self::MESSAGE;
        }

        parent::__construct($message, $code);
    }

    /**
     * Getter for $arg
     *
     * @return string
     */
    public function getApiArgument(): string
    {
        return $this->arg;
    }
}
