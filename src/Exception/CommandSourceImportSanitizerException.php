<?php

namespace App\Exception;

/**
 * Masking error for Command Sync known sanitization errors.
 *
 * @since  1.2.0
 */
class CommandSourceImportSanitizerException extends \Exception
{
    /**
     * @param string $message
     * @param int    $code
     */
    public function __construct(string $message = 'Minor Sync Error occur', int $code = 1)
    {

        parent::__construct($message, $code);
    }
}
