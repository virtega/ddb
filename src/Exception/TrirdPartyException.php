<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Masking error that normally cause by 3rd party services
 * - Bad Connection / Request etc
 *
 * Response sent back ERROR 200
 * - 400 Bad Request response status code indicates that the server failed to use/contact 3rd party services
 */
class TrirdPartyException extends \Exception
{
    /**
     * @param string  $message  Message of exception
     * @param int     $code     Code of exception
     */
    public function __construct(string $message = '', int $code = Response::HTTP_BAD_REQUEST)
    {

        parent::__construct($message, $code);
    }
}
