<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Masking error as Lotus Db connection failed
 *
 * Response sent back ERROR 502
 * - 502 Invalid response from the upstream server
 */
class LotusConnectionException extends \Exception
{
    const MSG_PREFIX = 'Lotus Database Connection failed: ';

    /**
     * @param string  $message
     * @param int     $code
     */
    public function __construct(string $message = '', int $code = Response::HTTP_BAD_GATEWAY)
    {

        parent::__construct(self::MSG_PREFIX . $message, $code);
    }
}
