<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Masking error as Remove MySQL Db connection failed
 *
 * Response sent back ERROR 502
 * - 502 Invalid response from the upstream server
 *
 * @since  1.2.0
 */
class MsSqlRemoteConnectionException extends \Exception
{
    /**
     * @var string
     */
    const MSG_PREFIX = 'MySQL Remote Database Connection failed: ';

    /**
     * @param string  $host
     * @param string  $message
     * @param int     $code
     */
    public function __construct(string $host, string $message = '', int $code = Response::HTTP_BAD_GATEWAY)
    {

        parent::__construct("[{$host}] " . self::MSG_PREFIX . $message, $code);
    }
}
