<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Extend Country Data & Make Reference to Continents.
 *
 * @link https://www.geonames.org/countries/
 *
 * @since  1.2.0
 */
final class Version20190724000001 extends AbstractMigration
{
    /**
     * Get SQL for update and insert Country, and inject Capital Cities
     *
     * @return string
     */
    public static function getUpdateCountryAndCityDataSQL(): string
    {
        return 'INSERT INTO `country` (`iso_code`, `name`, `capital_city`) VALUES
            ("AD", "Andorra", "Andorra la Vella"),
            ("AE", "United Arab Emirates", "Abu Dhabi"),
            ("AF", "Afghanistan", "Kabul"),
            ("AG", "Antigua and Barbuda", "St. John\'s"),
            ("AI", "Anguilla", "The Valley"),
            ("AL", "Albania", "Tirana"),
            ("AM", "Armenia", "Yerevan"),
            ("AN", "Netherlands Antilles", "Willemstad"),
            ("AO", "Angola", "Luanda"),
            ("AQ", "Antarctica", NULL),
            ("AR", "Argentina", "Buenos Aires"),
            ("AS", "American Samoa", "Pago Pago"),
            ("AT", "Austria", "Vienna"),
            ("AU", "Australia", "Canberra"),
            ("AW", "Aruba", "Oranjestad"),
            ("AX", "Åland", "Mariehamn"),
            ("AZ", "Azerbaijan", "Baku"),
            ("BA", "Bosnia and Herzegovina", "Sarajevo"),
            ("BB", "Barbados", "Bridgetown"),
            ("BD", "Bangladesh", "Dhaka"),
            ("BE", "Belgium", "Brussels"),
            ("BF", "Burkina Faso", "Ouagadougou"),
            ("BG", "Bulgaria", "Sofia"),
            ("BH", "Bahrain", "Manama"),
            ("BI", "Burundi", "Gitega"),
            ("BJ", "Benin", "Porto-Novo"),
            ("BL", "Saint Barthélemy", NULL),
            ("BM", "Bermuda", "Hamilton"),
            ("BN", "Brunei", "Bandar Seri Begawan"),
            ("BO", "Bolivia", "Sucre"),
            ("BQ", "Bonaire", "Kralendijk"),
            ("BR", "Brazil", "Brasilia"),
            ("BS", "Bahamas", "Nassau"),
            ("BT", "Bhutan", "Thimphu"),
            ("BV", "Bouvet Island", NULL),
            ("BW", "Botswana", "Gaborone"),
            ("BY", "Belarus", "Minsk"),
            ("BZ", "Belize", "Belmopan"),
            ("CA", "Canada", "Ottawa"),
            ("CC", "Cocos [Keeling] Islands", "West Island"),
            ("CD", "Democratic Republic of the Congo", "Kinshasa"),
            ("CF", "Central African Republic", "Bangui"),
            ("CG", "Republic of the Congo", "Brazzaville"),
            ("CH", "Switzerland", "Bern"),
            ("CI", "Ivory Coast", "Yamoussoukro"),
            ("CK", "Cook Islands", "Avarua"),
            ("CL", "Chile", "Santiago"),
            ("CM", "Cameroon", "Yaounde"),
            ("CN", "China", "Beijing"),
            ("CO", "Colombia", "Bogota"),
            ("CR", "Costa Rica", "San Jose"),
            ("CS", "Serbia and Montenegro", "Belgrade"),
            ("CU", "Cuba", "Havana"),
            ("CV", "Cabo Verde", "Praia"),
            ("CW", "Curacao", "Willemstad"),
            ("CX", "Christmas Island", "Flying Fish Cove"),
            ("CY", "Cyprus", "Nicosia"),
            ("CZ", "Czechia", "Prague"),
            ("DE", "Germany", "Berlin"),
            ("DJ", "Djibouti", "Djibouti"),
            ("DK", "Denmark", "Copenhagen"),
            ("DM", "Dominica", "Roseau"),
            ("DO", "Dominican Republic", "Santo Domingo"),
            ("DZ", "Algeria", "Algiers"),
            ("EC", "Ecuador", "Quito"),
            ("EE", "Estonia", "Tallinn"),
            ("EG", "Egypt", "Cairo"),
            ("EH", "Western Sahara", "El-Aaiun"),
            ("ER", "Eritrea", "Asmara"),
            ("ES", "Spain", "Madrid"),
            ("ET", "Ethiopia", "Addis Ababa"),
            ("FI", "Finland", "Helsinki"),
            ("FJ", "Fiji", "Suva"),
            ("FK", "Falkland Islands", "Stanley"),
            ("FM", "Micronesia", "Palikir"),
            ("FO", "Faroe Islands", "Torshavn"),
            ("FR", "France", "Paris"),
            ("GA", "Gabon", "Libreville"),
            ("GB", "United Kingdom", "London"),
            ("GD", "Grenada", "St. George\'s"),
            ("GE", "Georgia", "Tbilisi"),
            ("GF", "French Guiana", "Cayenne"),
            ("GG", "Guernsey", "St Peter Port"),
            ("GH", "Ghana", "Accra"),
            ("GI", "Gibraltar", "Gibraltar"),
            ("GL", "Greenland", "Nuuk"),
            ("GM", "Gambia", "Banjul"),
            ("GN", "Guinea", "Conakry"),
            ("GP", "Guadeloupe", "Basse-Terre"),
            ("GQ", "Equatorial Guinea", "Malabo"),
            ("GR", "Greece", "Athens"),
            ("GS", "South Georgia and the South Sandwich Islands", "Grytviken"),
            ("GT", "Guatemala", "Guatemala City"),
            ("GU", "Guam", "Hagatna"),
            ("GW", "Guinea-Bissau", "Bissau"),
            ("GY", "Guyana", "Georgetown"),
            ("HK", "Hong Kong", "Hong Kong"),
            ("HM", "Heard Island and McDonald Islands", NULL),
            ("HN", "Honduras", "Tegucigalpa"),
            ("HR", "Croatia", "Zagreb"),
            ("HT", "Haiti", "Port-au-Prince"),
            ("HU", "Hungary", "Budapest"),
            ("ID", "Indonesia", "Jakarta"),
            ("IE", "Ireland", "Dublin"),
            ("IL", "Israel", "Jerusalem"),
            ("IM", "Isle of Man", "Douglas"),
            ("IN", "India", "New Delhi"),
            ("IO", "British Indian Ocean Territory", "Diego Garcia"),
            ("IQ", "Iraq", "Baghdad"),
            ("IR", "Iran", "Tehran"),
            ("IS", "Iceland", "Reykjavik"),
            ("IT", "Italy", "Rome"),
            ("JE", "Jersey", "Saint Helier"),
            ("JM", "Jamaica", "Kingston"),
            ("JO", "Jordan", "Amman"),
            ("JP", "Japan", "Tokyo"),
            ("KE", "Kenya", "Nairobi"),
            ("KG", "Kyrgyzstan", "Bishkek"),
            ("KH", "Cambodia", "Phnom Penh"),
            ("KI", "Kiribati", "Tarawa"),
            ("KM", "Comoros", "Moroni"),
            ("KN", "Saint Kitts and Nevis", "Basseterre"),
            ("KP", "North Korea", "Pyongyang"),
            ("KR", "South Korea", "Seoul"),
            ("KW", "Kuwait", "Kuwait City"),
            ("KY", "Cayman Islands", "George Town"),
            ("KZ", "Kazakhstan", "Nur-Sultan"),
            ("LA", "Laos", "Vientiane"),
            ("LB", "Lebanon", "Beirut"),
            ("LC", "Saint Lucia", "Castries"),
            ("LI", "Liechtenstein", "Vaduz"),
            ("LK", "Sri Lanka", "Colombo"),
            ("LR", "Liberia", "Monrovia"),
            ("LS", "Lesotho", "Maseru"),
            ("LT", "Lithuania", "Vilnius"),
            ("LU", "Luxembourg", "Luxembourg"),
            ("LV", "Latvia", "Riga"),
            ("LY", "Libya", "Tripoli"),
            ("MA", "Morocco", "Rabat"),
            ("MC", "Monaco", "Monaco"),
            ("MD", "Moldova", "Chisinau"),
            ("ME", "Montenegro", "Podgorica"),
            ("MF", "Saint Martin", "Marigot"),
            ("MG", "Madagascar", "Antananarivo"),
            ("MH", "Marshall Islands", "Majuro"),
            ("MK", "North Macedonia", "Skopje"),
            ("ML", "Mali", "Bamako"),
            ("MM", "Myanmar [Burma]", "Nay Pyi Taw"),
            ("MN", "Mongolia", "Ulaanbaatar"),
            ("MO", "Macao", "Macao"),
            ("MP", "Northern Mariana Islands", "Saipan"),
            ("MQ", "Martinique", "Fort-de-France"),
            ("MR", "Mauritania", "Nouakchott"),
            ("MS", "Montserrat", "Plymouth"),
            ("MT", "Malta", "Valletta"),
            ("MU", "Mauritius", "Port Louis"),
            ("MV", "Maldives", "Male"),
            ("MW", "Malawi", "Lilongwe"),
            ("MX", "Mexico", "Mexico City"),
            ("MY", "Malaysia", "Kuala Lumpur"),
            ("MZ", "Mozambique", "Maputo"),
            ("NA", "Namibia", "Windhoek"),
            ("NC", "New Caledonia", "Noumea"),
            ("NE", "Niger", "Niamey"),
            ("NF", "Norfolk Island", "Kingston"),
            ("NG", "Nigeria", "Abuja"),
            ("NI", "Nicaragua", "Managua"),
            ("NL", "Netherlands", "Amsterdam"),
            ("NO", "Norway", "Oslo"),
            ("NP", "Nepal", "Kathmandu"),
            ("NR", "Nauru", "Yaren"),
            ("NU", "Niue", "Alofi"),
            ("NZ", "New Zealand", "Wellington"),
            ("OM", "Oman", "Muscat"),
            ("PA", "Panama", "Panama City"),
            ("PE", "Peru", "Lima"),
            ("PF", "French Polynesia", "Papeete"),
            ("PG", "Papua New Guinea", "Port Moresby"),
            ("PH", "Philippines", "Manila"),
            ("PK", "Pakistan", "Islamabad"),
            ("PL", "Poland", "Warsaw"),
            ("PM", "Saint Pierre and Miquelon", "Saint-Pierre"),
            ("PN", "Pitcairn Islands", "Adamstown"),
            ("PR", "Puerto Rico", "San Juan"),
            ("PS", "Palestine", "East Jerusalem"),
            ("PT", "Portugal", "Lisbon"),
            ("PW", "Palau", "Melekeok"),
            ("PY", "Paraguay", "Asuncion"),
            ("QA", "Qatar", "Doha"),
            ("RE", "Réunion", "Saint-Denis"),
            ("RO", "Romania", "Bucharest"),
            ("RS", "Serbia", "Belgrade"),
            ("RU", "Russia", "Moscow"),
            ("RW", "Rwanda", "Kigali"),
            ("SA", "Saudi Arabia", "Riyadh"),
            ("SB", "Solomon Islands", "Honiara"),
            ("SC", "Seychelles", "Victoria"),
            ("SD", "Sudan", "Khartoum"),
            ("SE", "Sweden", "Stockholm"),
            ("SG", "Singapore", "Singapore"),
            ("SH", "Saint Helena", "Jamestown"),
            ("SI", "Slovenia", "Ljubljana"),
            ("SJ", "Svalbard and Jan Mayen", "Longyearbyen"),
            ("SK", "Slovakia", "Bratislava"),
            ("SL", "Sierra Leone", "Freetown"),
            ("SM", "San Marino", "San Marino"),
            ("SN", "Senegal", "Dakar"),
            ("SO", "Somalia", "Mogadishu"),
            ("SR", "Suriname", "Paramaribo"),
            ("SS", "South Sudan", "Juba"),
            ("ST", "São Tomé and Príncipe", "Sao Tome"),
            ("SV", "El Salvador", "San Salvador"),
            ("SX", "Sint Maarten", "Philipsburg"),
            ("SY", "Syria", "Damascus"),
            ("SZ", "Eswatini", "Mbabane"),
            ("TC", "Turks and Caicos Islands", "Cockburn Town"),
            ("TD", "Chad", "N\'Djamena"),
            ("TF", "French Southern Territories", "Port-aux-Francais"),
            ("TG", "Togo", "Lome"),
            ("TH", "Thailand", "Bangkok"),
            ("TJ", "Tajikistan", "Dushanbe"),
            ("TK", "Tokelau", NULL),
            ("TL", "Timor-Leste", "Dili"),
            ("TM", "Turkmenistan", "Ashgabat"),
            ("TN", "Tunisia", "Tunis"),
            ("TO", "Tonga", "Nuku\'alofa"),
            ("TR", "Turkey", "Ankara"),
            ("TT", "Trinidad and Tobago", "Port of Spain"),
            ("TV", "Tuvalu", "Funafuti"),
            ("TW", "Taiwan", "Taipei"),
            ("TZ", "Tanzania", "Dodoma"),
            ("UA", "Ukraine", "Kyiv"),
            ("UG", "Uganda", "Kampala"),
            ("UM", "U.S. Minor Outlying Islands", NULL),
            ("US", "United States", "Washington"),
            ("UY", "Uruguay", "Montevideo"),
            ("UZ", "Uzbekistan", "Tashkent"),
            ("VA", "Vatican City", "Vatican City"),
            ("VC", "Saint Vincent and the Grenadines", "Kingstown"),
            ("VE", "Venezuela", "Caracas"),
            ("VG", "British Virgin Islands", "Road Town"),
            ("VI", "U.S. Virgin Islands", "Charlotte Amalie"),
            ("VN", "Vietnam", "Hanoi"),
            ("VU", "Vanuatu", "Port Vila"),
            ("WF", "Wallis and Futuna", "Mata Utu"),
            ("WS", "Samoa", "Apia"),
            ("XK", "Kosovo", "Pristina"),
            ("YE", "Yemen", "Sanaa"),
            ("YT", "Mayotte", "Mamoudzou"),
            ("YU", "Yugoslavia", "Belgrade"),
            ("ZA", "South Africa", "Pretoria"),
            ("ZM", "Zambia", "Lusaka"),
            ("ZW", "Zimbabwe", "Harare")
            ON DUPLICATE KEY UPDATE
                `name`         = VALUES(`name`),
                `capital_city` = VALUES(`capital_city`)
        ;';
    }

    /**
     * Get SQL for set basic Country relation to hierarchical continents
     *
     * @return string
     */
    public static function getCountryContinentDataSQL(): string
    {
        return 'INSERT INTO `country_regions` (`country`, `region`) VALUES
            ("AD", 253),("AD", 250),("AD", 200),("AD", 2  ),("AE", 207),("AE", 205),("AE", 200),("AE", 2  ),("AF", 208),("AF", 204),("AF", 200),
            ("AF", 2  ),("AG", 362),("AG", 361),("AG", 353),("AG", 351),("AG", 300),("AG", 3  ),("AI", 362),("AI", 361),("AI", 353),("AI", 351),
            ("AI", 300),("AI", 3  ),("AL", 253),("AL", 250),("AL", 200),("AL", 2  ),("AM", 206),("AM", 200),("AM", 2  ),("AN", 353),("AN", 351),
            ("AN", 350),("AN", 3  ),("AO", 104),("AO", 1  ),("AQ", 4  ),("AR", 357),("AR", 354),("AR", 350),("AR", 3  ),("AS", 553),("AS", 550),
            ("AS", 5  ),("AT", 254),("AT", 250),("AT", 2  ),("AU", 500),("AU", 5  ),("AW", 362),("AW", 361),("AW", 353),("AW", 351),("AW", 300),
            ("AW", 3  ),("AX", 252),("AX", 250),("AX", 2  ),("AZ", 206),("AZ", 200),("AZ", 2  ),("BA", 253),("BA", 250),("BA", 200),("BA", 2  ),
            ("BB", 361),("BB", 353),("BB", 351),("BB", 300),("BB", 3  ),("BD", 208),("BD", 204),("BD", 200),("BD", 2  ),("BE", 254),("BE", 250),
            ("BE", 2  ),("BF", 103),("BF", 1  ),("BG", 252),("BG", 250),("BG", 2  ),("BH", 207),("BH", 205),("BH", 200),("BH", 2  ),("BI", 101),
            ("BI", 1  ),("BJ", 103),("BJ", 1  ),("BL", 353),("BL", 351),("BL", 350),("BL", 3  ),("BM", 300),("BM", 3  ),("BN", 203),("BN", 200),
            ("BN", 2  ),("BO", 371),("BO", 354),("BO", 350),("BO", 3  ),("BQ", 354),("BQ", 350),("BQ", 3  ),("BR", 372),("BR", 354),("BR", 350),
            ("BR", 3  ),("BS", 353),("BS", 351),("BS", 350),("BS", 3  ),("BT", 208),("BT", 204),("BT", 200),("BT", 2  ),("BV", 4  ),("BW", 102),
            ("BW", 1  ),("BY", 252),("BY", 250),("BY", 2  ),("BZ", 352),("BZ", 351),("BZ", 350),("BZ", 3  ),("CA", 300),("CA", 3  ),("CC", 501),
            ("CC", 500),("CC", 5  ),("CC", 203),("CC", 200),("CC", 2  ),("CD", 104),("CD", 1  ),("CF", 104),("CF", 1  ),("CG", 104),("CG", 1  ),
            ("CH", 254),("CH", 250),("CH", 2  ),("CI", 103),("CI", 1  ),("CK", 553),("CK", 550),("CK", 5  ),("CL", 357),("CL", 354),("CL", 350),
            ("CL", 3  ),("CM", 104),("CM", 1  ),("CN", 202),("CN", 200),("CN", 2  ),("CO", 370),("CO", 354),("CO", 350),("CO", 3  ),("CR", 352),
            ("CR", 351),("CR", 350),("CR", 3  ),("CS", 260),("CS", 253),("CS", 250),("CS", 2  ),("CU", 360),("CU", 353),("CU", 351),("CU", 350),
            ("CU", 3  ),("CV", 103),("CV", 1  ),("CW", 354),("CW", 350),("CW", 3  ),("CX", 203),("CX", 200),("CX", 2  ),("CY", 206),("CY", 200),
            ("CY", 2  ),("CZ", 252),("CZ", 250),("CZ", 2  ),("DE", 254),("DE", 250),("DE", 2  ),("DJ", 101),("DJ", 1  ),("DK", 251),("DK", 250),
            ("DK", 2  ),("DM", 361),("DM", 353),("DM", 351),("DM", 300),("DM", 3  ),("DO", 360),("DO", 353),("DO", 351),("DO", 350),("DO", 3  ),
            ("DZ", 100),("DZ", 1  ),("EC", 370),("EC", 354),("EC", 350),("EC", 3  ),("EE", 251),("EE", 250),("EE", 2  ),("EG", 207),("EG", 205),
            ("EG", 200),("EG", 2  ),("EG", 1  ),("EH", 100),("EH", 1  ),("ER", 101),("ER", 1  ),("ES", 253),("ES", 250),("ES", 200),("ES", 2  ),
            ("ET", 101),("ET", 1  ),("FI", 251),("FI", 250),("FI", 2  ),("FJ", 551),("FJ", 550),("FJ", 5  ),("FK", 357),("FK", 354),("FK", 350),
            ("FK", 3  ),("FM", 552),("FM", 550),("FM", 5  ),("FO", 251),("FO", 250),("FO", 2  ),("FR", 254),("FR", 250),("FR", 2  ),("GA", 104),
            ("GA", 1  ),("GB", 251),("GB", 250),("GB", 2  ),("GD", 361),("GD", 353),("GD", 351),("GD", 300),("GD", 3  ),("GE", 206),("GE", 200),
            ("GE", 2  ),("GF", 355),("GF", 354),("GF", 350),("GF", 3  ),("GG", 254),("GG", 250),("GG", 2  ),("GH", 103),("GH", 1  ),("GI", 253),
            ("GI", 250),("GI", 200),("GI", 2  ),("GL", 300),("GL", 3  ),("GM", 103),("GM", 1  ),("GN", 103),("GN", 1  ),("GP", 361),("GP", 353),
            ("GP", 351),("GP", 300),("GP", 3  ),("GQ", 104),("GQ", 1  ),("GR", 253),("GR", 250),("GR", 200),("GR", 2  ),("GS", 4  ),("GT", 352),
            ("GT", 351),("GT", 350),("GT", 3  ),("GU", 552),("GU", 550),("GU", 5  ),("GW", 103),("GW", 1  ),("GY", 356),("GY", 354),("GY", 350),
            ("GY", 3  ),("HK", 202),("HK", 200),("HK", 2  ),("HM", 352),("HM", 351),("HM", 350),("HM", 3  ),("HN", 253),("HN", 250),("HN", 200),
            ("HN", 2  ),("HR", 360),("HR", 353),("HR", 351),("HR", 350),("HR", 3  ),("HT", 302),("HT", 301),("HT", 300),("HT", 3  ),("HU", 252),
            ("HU", 250),("HU", 2  ),("ID", 203),("ID", 200),("ID", 2  ),("IE", 251),("IE", 250),("IE", 2  ),("IL", 207),("IL", 206),("IL", 205),
            ("IL", 200),("IL", 200),("IL", 2  ),("IL", 2  ),("IM", 251),("IM", 250),("IM", 2  ),("IN", 208),("IN", 204),("IN", 200),("IN", 2  ),
            ("IO", 6  ),("IQ", 207),("IQ", 206),("IQ", 205),("IQ", 200),("IQ", 200),("IQ", 2  ),("IQ", 2  ),("IR", 208),("IR", 204),("IR", 200),
            ("IR", 2  ),("IS", 251),("IS", 250),("IS", 2  ),("IT", 253),("IT", 250),("IT", 200),("IT", 2  ),("JE", 251),("JE", 250),("JE", 2  ),
            ("JM", 360),("JM", 353),("JM", 351),("JM", 350),("JM", 3  ),("JO", 207),("JO", 206),("JO", 205),("JO", 200),("JO", 200),("JO", 2  ),
            ("JO", 2  ),("JP", 202),("JP", 200),("JP", 2  ),("KE", 101),("KE", 1  ),("KG", 207),("KG", 200),("KG", 2  ),("KH", 203),("KH", 200),
            ("KH", 2  ),("KI", 552),("KI", 550),("KI", 5  ),("KM", 101),("KM", 1  ),("KN", 361),("KN", 353),("KN", 351),("KN", 300),("KN", 3  ),
            ("KP", 202),("KP", 200),("KP", 2  ),("KR", 202),("KR", 200),("KR", 2  ),("KW", 207),("KW", 206),("KW", 205),("KW", 200),("KW", 200),
            ("KW", 2  ),("KW", 2  ),("KY", 360),("KY", 353),("KY", 351),("KY", 350),("KY", 3  ),("KZ", 207),("KZ", 200),("KZ", 2  ),("LA", 203),
            ("LA", 200),("LA", 2  ),("LB", 207),("LB", 206),("LB", 205),("LB", 200),("LB", 200),("LB", 2  ),("LB", 2  ),("LC", 361),("LC", 353),
            ("LC", 351),("LC", 300),("LC", 3  ),("LI", 254),("LI", 250),("LI", 2  ),("LK", 208),("LK", 204),("LK", 200),("LK", 2  ),("LR", 103),
            ("LR", 1  ),("LS", 102),("LS", 1  ),("LT", 251),("LT", 250),("LT", 2  ),("LU", 254),("LU", 250),("LU", 2  ),("LV", 251),("LV", 250),
            ("LV", 2  ),("LY", 100),("LY", 1  ),("MA", 100),("MA", 1  ),("MC", 253),("MC", 250),("MC", 200),("MC", 2  ),("MD", 252),("MD", 250),
            ("MD", 2  ),("ME", 253),("ME", 250),("ME", 200),("ME", 2  ),("MF", 302),("MF", 301),("MF", 300),("MF", 3  ),("MG", 101),("MG", 1  ),
            ("MH", 552),("MH", 550),("MH", 5  ),("MK", 253),("MK", 250),("MK", 200),("MK", 2  ),("ML", 103),("ML", 1  ),("MM", 203),("MM", 200),
            ("MM", 2  ),("MN", 202),("MN", 200),("MN", 2  ),("MO", 202),("MO", 200),("MO", 2  ),("MP", 552),("MP", 550),("MP", 5  ),("MQ", 361),
            ("MQ", 353),("MQ", 351),("MQ", 300),("MQ", 3  ),("MR", 103),("MR", 1  ),("MS", 361),("MS", 353),("MS", 351),("MS", 300),("MS", 3  ),
            ("MT", 253),("MT", 250),("MT", 200),("MT", 2  ),("MU", 101),("MU", 1  ),("MV", 208),("MV", 204),("MV", 200),("MV", 2  ),("MW", 101),
            ("MW", 1  ),("MX", 300),("MX", 3  ),("MY", 203),("MY", 200),("MY", 2  ),("MZ", 101),("MZ", 1  ),("NA", 102),("NA", 1  ),("NC", 551),
            ("NC", 550),("NC", 5  ),("NE", 103),("NE", 1  ),("NF", 500),("NF", 5  ),("NG", 103),("NG", 1  ),("NI", 352),("NI", 351),("NI", 350),
            ("NI", 3  ),("NL", 254),("NL", 250),("NL", 2  ),("NO", 251),("NO", 250),("NO", 2  ),("NP", 208),("NP", 204),("NP", 200),("NP", 2  ),
            ("NR", 552),("NR", 550),("NR", 5  ),("NU", 553),("NU", 550),("NU", 5  ),("NZ", 500),("NZ", 5  ),("OM", 207),("OM", 205),("OM", 200),
            ("OM", 2  ),("PA", 352),("PA", 351),("PA", 350),("PA", 3  ),("PE", 357),("PE", 354),("PE", 350),("PE", 3  ),("PF", 553),("PF", 550),
            ("PF", 5  ),("PG", 551),("PG", 550),("PG", 5  ),("PH", 203),("PH", 200),("PH", 2  ),("PK", 208),("PK", 204),("PK", 200),("PK", 2  ),
            ("PL", 252),("PL", 250),("PL", 2  ),("PM", 302),("PM", 301),("PM", 300),("PM", 3  ),("PN", 553),("PN", 550),("PN", 5  ),("PR", 360),
            ("PR", 353),("PR", 351),("PR", 350),("PR", 3  ),("PS", 207),("PS", 206),("PS", 205),("PS", 200),("PS", 200),("PS", 2  ),("PS", 2  ),
            ("PT", 253),("PT", 250),("PT", 200),("PT", 2  ),("PW", 552),("PW", 550),("PW", 5  ),("PY", 371),("PY", 354),("PY", 350),("PY", 3  ),
            ("QA", 207),("QA", 205),("QA", 200),("QA", 2  ),("RE", 101),("RE", 1  ),("RO", 252),("RO", 250),("RO", 2  ),("RS", 253),("RS", 250),
            ("RS", 200),("RS", 2  ),("RU", 290),("RU", 252),("RU", 201),("RU", 2  ),("RW", 101),("RW", 1  ),("SA", 207),("SA", 205),("SA", 200),
            ("SA", 2  ),("SB", 551),("SB", 550),("SB", 5  ),("SC", 101),("SC", 1  ),("SD", 100),("SD", 1  ),("SE", 251),("SE", 250),("SE", 2  ),
            ("SG", 203),("SG", 200),("SG", 2  ),("SH", 103),("SH", 1  ),("SI", 253),("SI", 250),("SI", 200),("SI", 2  ),("SJ", 251),("SJ", 250),
            ("SJ", 2  ),("SK", 252),("SK", 250),("SK", 2  ),("SL", 103),("SL", 1  ),("SM", 253),("SM", 250),("SM", 200),("SM", 2  ),("SN", 103),
            ("SN", 1  ),("SO", 101),("SO", 1  ),("SR", 356),("SR", 354),("SR", 350),("SR", 3  ),("SS", 110),("SS", 101),("SS", 1  ),("ST", 104),
            ("ST", 1  ),("SV", 352),("SV", 351),("SV", 350),("SV", 3  ),("SX", 353),("SX", 351),("SX", 350),("SX", 3  ),("SY", 207),("SY", 206),
            ("SY", 205),("SY", 200),("SY", 200),("SY", 2  ),("SY", 2  ),("SZ", 102),("SZ", 1  ),("TC", 353),("TC", 351),("TC", 350),("TC", 3  ),
            ("TD", 104),("TD", 1  ),("TF", 4  ),("TF", 357),("TF", 354),("TF", 350),("TF", 3  ),("TG", 103),("TG", 1  ),("TH", 203),("TH", 200),
            ("TH", 2  ),("TJ", 207),("TJ", 200),("TJ", 2  ),("TK", 5  ),("TL", 203),("TL", 200),("TL", 2  ),("TM", 207),("TM", 200),("TM", 2  ),
            ("TN", 100),("TN", 1  ),("TO", 553),("TO", 550),("TO", 5  ),("TR", 260),("TR", 253),("TR", 250),("TR", 206),("TR", 200),("TR", 2  ),
            ("TR", 2  ),("TT", 355),("TT", 354),("TT", 353),("TT", 351),("TT", 350),("TT", 350),("TT", 3  ),("TT", 3  ),("TV", 553),("TV", 550),
            ("TV", 5  ),("TW", 202),("TW", 200),("TW", 2  ),("TZ", 101),("TZ", 1  ),("UA", 252),("UA", 250),("UA", 2  ),("UG", 101),("UG", 1  ),
            ("UM", 300),("UM", 3  ),("US", 300),("US", 3  ),("UY", 359),("UY", 354),("UY", 350),("UY", 3  ),("UZ", 207),("UZ", 200),("UZ", 2  ),
            ("VA", 253),("VA", 250),("VA", 200),("VA", 2  ),("VC", 361),("VC", 353),("VC", 351),("VC", 300),("VC", 3  ),("VE", 355),("VE", 354),
            ("VE", 350),("VE", 3  ),("VG", 361),("VG", 353),("VG", 351),("VG", 300),("VG", 3  ),("VI", 361),("VI", 353),("VI", 351),("VI", 300),
            ("VI", 3  ),("VN", 203),("VN", 200),("VN", 2  ),("VU", 551),("VU", 550),("VU", 5  ),("WF", 553),("WF", 550),("WF", 5  ),("WS", 553),
            ("WS", 550),("WS", 5  ),("XK", 260),("XK", 253),("XK", 250),("XK", 2  ),("YE", 207),("YE", 205),("YE", 200),("YE", 2  ),("YT", 101),
            ("YT", 1  ),("YU", 260),("YU", 253),("YU", 250),("YU", 2  ),("ZA", 102),("ZA", 1  ),("ZM", 101),("ZM", 1  ),("ZW", 101),("ZW", 1  )
        ;';
    }

    /**
     * Get SQL for set for EU5 Group
     *
     * @return string
     */
    public static function getCountryContinentSpecialGroupEU5(): string
    {
        return 'INSERT INTO `country_regions` (`country`, `region`) VALUES
            ("DE", 5000),("ES", 5000),("FR", 5000),("GB", 5000),("IT", 5000)
        ;';
    }

    public function getDescription() : string
    {
        return 'Extend Country Data & Continents.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            ALTER TABLE `country`
                ADD `capital_city`  VARCHAR(50)  CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  DEFAULT NULL AFTER `name`
        ;');

        $this->addSql('
            CREATE TABLE `country_regions` (
                `id`        INT(11)    UNSIGNED NOT NULL AUTO_INCREMENT,
                `country`   VARCHAR(2) COLLATE utf8mb4_unicode_ci NOT NULL,
                `region`    INT(11)    UNSIGNED NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB
        ;');

        $this->addSql('
            ALTER TABLE `country_regions`
                ADD CONSTRAINT `FK_6B2B219E5373C966`
                    FOREIGN KEY (`country`)
                        REFERENCES `country`(`iso_code`)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                ADD CONSTRAINT `FK_6B2B219EF62F176`
                    FOREIGN KEY (`region`)
                        REFERENCES `country_taxonomy`(`id`)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
        ;');

        $this->addSql( self::getUpdateCountryAndCityDataSQL() );

        $this->addSql( self::getCountryContinentDataSQL() );

        $this->addSql( self::getCountryContinentSpecialGroupEU5() );
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            ALTER TABLE `country`
                DROP `capital_city`
        ;');

        $this->addSql('ALTER TABLE `country_regions` DROP FOREIGN KEY `FK_6B2B219E5373C966`, DROP FOREIGN KEY `FK_6B2B219EF62F176`;');

        $this->addSql('DROP TABLE `country_regions`;');
    }
}
