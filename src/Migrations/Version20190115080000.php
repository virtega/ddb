<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20190115080000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' != $this->connection->getDatabasePlatform()->getName(), "Migration can only be executed safely on 'MySQL'.");

        $this->addSql("ALTER TABLE `variable` DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' != $this->connection->getDatabasePlatform()->getName(), "Migration can only be executed safely on 'MySQL'.");

        $this->addSql("ALTER TABLE `variable` DEFAULT CHARSET=utf8 COLLATE utf8mb4_unicode_ci;");
    }
}
