<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200124151320 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $sql = 'CREATE TABLE `dgmonitor_stats` ('
            . ' `id` int(11) unsigned NOT NULL AUTO_INCREMENT,'
            . ' `date` date DEFAULT NULL,'
            . ' `fb_id` int(5) DEFAULT NULL,'
            . ' PRIMARY KEY (`id`),'
            . ' KEY `IDX_DATE` (`date`)'
            . ' ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci';
        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP TABLE IF EXISTS dgmonitor_stats');
    }
}
