<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Change Country "Czechia" to "Czech Republic"
 */
final class Version20200319104221 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Change Country "Czechia" to "Czech Republic"';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $sql = 'UPDATE `country` SET `name` = "Czech Republic" WHERE `iso_code` = "CZ";';

        $this->addSql($sql);

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $sql = 'UPDATE `country` SET `name` = "Czechia" WHERE `iso_code` = "CZ";';

        $this->addSql($sql);
    }
}
