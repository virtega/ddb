<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Adding table for Specialty Taxonomy feature.
 *
 * @since  1.2.0
 */
final class Version20190724074100 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Tables for Specialty Taxonomy integration.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE `specialty_taxonomy` (
                `id`            INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
                `specialty`     VARCHAR(200)        NOT NULL COLLATE utf8mb4_unicode_ci,
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `specialty_taxonomy`;');
    }
}
