<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Adding table for Conference / Congress feature.
 *
 * @since  1.2.0
 */
final class Version20190724074105 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Tables for Conference integration.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE `conference` (
                `id`            INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
                `name`          VARCHAR(500)        DEFAULT NULL,
                `start_date`    DATETIME            DEFAULT NULL,
                `end_date`      DATETIME            DEFAULT NULL,
                `city`          VARCHAR(150)        DEFAULT NULL,
                `city_guide`    VARCHAR(150)        DEFAULT NULL,
                `country`       VARCHAR(2)          DEFAULT NULL COLLATE utf8mb4_unicode_ci,
                `region`        INT(11) UNSIGNED    DEFAULT NULL,
                `previous_id`   VARCHAR(32)         DEFAULT NULL,
                `unique_name`   VARCHAR(200)        DEFAULT NULL,
                `cruise`        INT(1) UNSIGNED     NOT NULL DEFAULT 0,
                `online`        INT(1) UNSIGNED     NOT NULL DEFAULT 0,
                `key_event`     INT(1) UNSIGNED     NOT NULL DEFAULT 0,
                `has_guide`     INT(1) UNSIGNED     NOT NULL DEFAULT 0,
                `discontinued`  INT(1)              NOT NULL DEFAULT 0,
                `details`       JSON                DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `conference_features` (`cruise`,`online`,`key_event`,`has_guide`,`discontinued`),
                FULLTEXT KEY `conference_name` (`name`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
        ;');

        $this->addSql('
            ALTER TABLE
                `conference`
                    ADD CONSTRAINT `FK_911533C85373C966`
                        FOREIGN KEY (`country`)
                        REFERENCES `country`(`iso_code`)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                    ADD CONSTRAINT `FK_911533C8F62F176`
                        FOREIGN KEY (`region`)
                        REFERENCES `country_taxonomy`(`id`)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
        ;');

        $this->addSql('
            CREATE TABLE `conference_specialties` (
                `id`            INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
                `conference_id` INT(10) UNSIGNED    NOT NULL,
                `specialty_id`  INT(10) UNSIGNED    NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
        ;');

        $this->addSql('
            ALTER TABLE
                `conference_specialties`
                    ADD CONSTRAINT `FK_E4F3796C604B8382`
                        FOREIGN KEY (`conference_id`)
                            REFERENCES `conference`(`id`)
                            ON DELETE NO ACTION
                            ON UPDATE NO ACTION,
                    ADD CONSTRAINT `FK_E4F3796CE066A6EC`
                        FOREIGN KEY (`specialty_id`)
                            REFERENCES `specialty_taxonomy`(`id`)
                            ON DELETE NO ACTION
                            ON UPDATE NO ACTION
        ;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `conference` DROP FOREIGN KEY `FK_911533C85373C966`;');

        $this->addSql('ALTER TABLE `conference` DROP FOREIGN KEY `FK_911533C8F62F176`;');

        $this->addSql('ALTER TABLE `conference_specialties` DROP FOREIGN KEY `FK_E4F3796C604B8382`;');

        $this->addSql('ALTER TABLE `conference_specialties` DROP FOREIGN KEY `FK_E4F3796CE066A6EC`;');

        $this->addSql('DROP TABLE `conference`;');

        $this->addSql('DROP TABLE `conference_specialties`;');
    }
}
