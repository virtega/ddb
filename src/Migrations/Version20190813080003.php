<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Adding table for Journal feature.
 *
 * @since  1.2.0
 */
final class Version20190813080003 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Tables for Journal integration.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE `journal` (
                `id`                      INT(10)       UNSIGNED AUTO_INCREMENT NOT NULL,
                `name`                    VARCHAR(100)  DEFAULT NULL COLLATE utf8mb4_unicode_ci,
                `abbreviation`            VARCHAR(100)  DEFAULT NULL COLLATE utf8mb4_unicode_ci,
                `volume`                  VARCHAR(200)  DEFAULT NULL COLLATE utf8mb4_unicode_ci,
                `number`                  VARCHAR(200)  DEFAULT NULL COLLATE utf8mb4_unicode_ci,
                `status`                  INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `exported`                INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `is_medline`              INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `is_second_line`          INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `is_dgabstract`           INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `not_being_updated`       INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `drugs_monitored`         INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `global_edition_journal`  INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `deleted`                 INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `manual_creation`         INT(1)        UNSIGNED DEFAULT 0 NOT NULL,
                `medline_issn`            VARCHAR(9)    DEFAULT NULL COLLATE utf8mb4_unicode_ci,
                `journal_report_1`        DECIMAL(4, 2) DEFAULT 0.00 NOT NULL,
                `journal_report_2`        DECIMAL(4, 2) DEFAULT 0.00 NOT NULL,
                `unid`                    VARCHAR(32)   DEFAULT NULL COLLATE utf8mb4_unicode_ci,
                `details`                 JSON          DEFAULT NULL,
                PRIMARY KEY(id),
                FULLTEXT INDEX `journal_names` (`name`, `abbreviation`),
                INDEX `journal_features` (
                    `status`,
                    `exported`,
                    `is_medline`,
                    `is_second_line`,
                    `is_dgabstract`,
                    `not_being_updated`,
                    `drugs_monitored`,
                    `global_edition_journal`,
                    `deleted`,
                    `manual_creation`
                )
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
        ;');

        $this->addSql('
            CREATE TABLE `journal_specialties` (
                `id`            INT UNSIGNED AUTO_INCREMENT NOT NULL,
                `journal_id`    INT UNSIGNED NOT NULL,
                `specialty_id`  INT UNSIGNED NOT NULL,
                PRIMARY KEY(id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
        ;');

        $this->addSql('
            ALTER TABLE
                `journal_specialties`
                    ADD CONSTRAINT `FK_2C63585E478E8802`
                        FOREIGN KEY (`journal_id`)
                            REFERENCES `journal`(`id`)
                            ON DELETE NO ACTION
                            ON UPDATE NO ACTION,
                    ADD CONSTRAINT `FK_2C63585E9A353316`
                        FOREIGN KEY (`specialty_id`)
                            REFERENCES `specialty_taxonomy`(`id`)
                            ON DELETE NO ACTION
                            ON UPDATE NO ACTION
        ;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `journal_specialties` DROP FOREIGN KEY `FK_2C63585E9A353316`;');

        $this->addSql('ALTER TABLE `journal_specialties` DROP FOREIGN KEY `FK_2C63585E478E8802`;');

        $this->addSql('DROP TABLE `journal`;');

        $this->addSql('DROP TABLE `journal_specialties`;');
    }
}
