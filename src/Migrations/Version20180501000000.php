<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Initial Database Structure
 * - Alter defaulting
 * - Alter ID for reference tables; ORM did not support STRING as INDEX
 * - Add Countries.
 *
 * @since 1.0.0
 */
class Version20180501000000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' != $this->connection->getDatabasePlatform()->getName(), "Migration can only be executed safely on 'MySQL'.");

        //
        // MAIN DRUGS
        //
        $this->addSql("CREATE TABLE `brand` (
            `id` int(10) UNSIGNED NOT NULL,
            `generic` int(10) UNSIGNED DEFAULT NULL,
            `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `valid` int(10) UNSIGNED NOT NULL COMMENT 'Options: 1=True, 0=False',
            `AutoEncodeExclude` int(10) UNSIGNED NOT NULL,
            `uid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ID use by previous System',
            `comments` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `creation_date` datetime DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

        $this->addSql("CREATE TABLE `brand_synonym` (
            `id` int(10) UNSIGNED NOT NULL,
            `brand` int(10) UNSIGNED DEFAULT NULL,
            `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
            `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON string for multiple Country ISO code'
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

        $this->addSql('CREATE TABLE `brand_typo` (
            `id` int(10) UNSIGNED NOT NULL,
            `brand` int(10) UNSIGNED DEFAULT NULL,
            `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql("CREATE TABLE `experimental` (
            `id` int(10) UNSIGNED NOT NULL,
            `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'experimental' COMMENT 'Options: experimental, generic, brand',
            `valid` int(10) UNSIGNED NOT NULL,
            `uid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ID use by previous System',
            `comments` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `creation_date` datetime DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

        $this->addSql("CREATE TABLE `generic` (
            `id` int(10) UNSIGNED NOT NULL,
            `experimental` int(10) UNSIGNED DEFAULT NULL,
            `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `valid` int(10) UNSIGNED NOT NULL COMMENT 'Options: 1=True, 0=False',
            `Level1` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `Level2` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `Level3` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `Level4` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `Level5` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `NoDoubleBounce` int(10) UNSIGNED NOT NULL,
            `AutoEncodeExclude` int(10) UNSIGNED NOT NULL,
            `uid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ID use by previous System',
            `comments` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `creation_date` datetime DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

        $this->addSql('CREATE TABLE `generic_ref_level1` (
            `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
            `name` text COLLATE utf8mb4_unicode_ci
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql('CREATE TABLE `generic_ref_level2` (
            `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
            `name` text COLLATE utf8mb4_unicode_ci
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql('CREATE TABLE `generic_ref_level3` (
            `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
            `name` text COLLATE utf8mb4_unicode_ci
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql('CREATE TABLE `generic_ref_level4` (
            `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
            `name` text COLLATE utf8mb4_unicode_ci
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql("CREATE TABLE `generic_synonym` (
            `id` int(10) UNSIGNED NOT NULL,
            `generic` int(10) UNSIGNED DEFAULT NULL,
            `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
            `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON string for multiple Country ISO code'
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

        $this->addSql('CREATE TABLE `generic_typo` (
            `id` int(10) UNSIGNED NOT NULL,
            `generic` int(10) UNSIGNED DEFAULT NULL,
            `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        //
        // MAIN DRUG INDEXES
        //
        $this->addSql('ALTER TABLE `brand`
            ADD PRIMARY KEY (`id`),
            ADD KEY `valid` (`valid`),
            ADD KEY `generic` (`generic`);');

        $this->addSql('ALTER TABLE `brand_synonym`
            ADD PRIMARY KEY (`id`),
            ADD KEY `generic` (`brand`);');

        $this->addSql('ALTER TABLE `brand_typo`
            ADD PRIMARY KEY (`id`),
            ADD KEY `brand` (`brand`);');

        $this->addSql('ALTER TABLE `experimental`
            ADD PRIMARY KEY (`id`),
            ADD KEY `type` (`type`),
            ADD KEY `valid` (`valid`);');

        $this->addSql('ALTER TABLE `generic`
            ADD PRIMARY KEY (`id`),
            ADD KEY `valid` (`valid`),
            ADD KEY `experimental` (`experimental`);');

        $this->addSql('ALTER TABLE `generic_ref_level1` ADD PRIMARY KEY (`code`);');

        $this->addSql('ALTER TABLE `generic_ref_level2` ADD PRIMARY KEY (`code`);');

        $this->addSql('ALTER TABLE `generic_ref_level3` ADD PRIMARY KEY (`code`);');

        $this->addSql('ALTER TABLE `generic_ref_level4` ADD PRIMARY KEY (`code`);');

        $this->addSql('ALTER TABLE `generic_synonym`
            ADD PRIMARY KEY (`id`),
            ADD KEY `generic` (`generic`);');

        $this->addSql('ALTER TABLE `generic_typo`
            ADD PRIMARY KEY (`id`),
            ADD KEY `generic` (`generic`);');

        //
        // MAIN DRUG AI
        //
        $this->addSql('ALTER TABLE `brand` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE `brand_synonym` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE `brand_typo` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE `experimental` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE `generic` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE `generic_synonym` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE `generic_typo` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;');

        //
        // MAIN DRUG CONSTRAINTS
        //
        $this->addSql('ALTER TABLE `brand` ADD CONSTRAINT `FK_1C52F958964FA1D3` FOREIGN KEY (`generic`) REFERENCES `generic` (`id`);');
        $this->addSql('ALTER TABLE `brand_synonym` ADD CONSTRAINT `FK_410AFBB31C52F958` FOREIGN KEY (`brand`) REFERENCES `brand` (`id`);');
        $this->addSql('ALTER TABLE `brand_typo` ADD CONSTRAINT `FK_4B6CB4881C52F958` FOREIGN KEY (`brand`) REFERENCES `brand` (`id`);');
        $this->addSql('ALTER TABLE `generic` ADD CONSTRAINT `FK_964FA1D3CA7863F9` FOREIGN KEY (`experimental`) REFERENCES `experimental` (`id`);');
        $this->addSql('ALTER TABLE `generic_synonym` ADD CONSTRAINT `FK_1B7F7C99964FA1D3` FOREIGN KEY (`generic`) REFERENCES `generic` (`id`);');
        $this->addSql('ALTER TABLE `generic_typo` ADD CONSTRAINT `FK_567152A4964FA1D3` FOREIGN KEY (`generic`) REFERENCES `generic` (`id`);');

        //
        // SUPPORTING TABLES
        //
        $this->addSql('CREATE TABLE `country` (
            `iso_code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
            `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql('ALTER TABLE `country` ADD PRIMARY KEY (`iso_code`);');

        $this->addSql("CREATE TABLE `variable` (
            `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
            `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Expected Data Type for processing',
            `value` text COLLATE utf8mb4_unicode_ci COMMENT 'Value Data'
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

        $this->addSql('ALTER TABLE `variable` ADD PRIMARY KEY (`name`);');
    }

    /**
     * @param Schema $schema
     *
     * Consider using "doctrine:database:drop"
     */
    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' != $this->connection->getDatabasePlatform()->getName(), "Migration can only be executed safely on 'MySQL'.");

        $this->addSql('ALTER TABLE `brand` DROP FOREIGN KEY `FK_1C52F958964FA1D3`;');
        $this->addSql('ALTER TABLE `brand_synonym` DROP FOREIGN KEY `FK_410AFBB31C52F958`;');
        $this->addSql('ALTER TABLE `brand_typo` DROP FOREIGN KEY `FK_4B6CB4881C52F958`;');
        $this->addSql('ALTER TABLE `generic` DROP FOREIGN KEY `FK_964FA1D3CA7863F9`;');
        $this->addSql('ALTER TABLE `generic_synonym` DROP FOREIGN KEY `FK_1B7F7C99964FA1D3`;');
        $this->addSql('ALTER TABLE `generic_typo` DROP FOREIGN KEY `FK_567152A4964FA1D3`;');

        $this->addSql('DROP TABLE `brand`;');
        $this->addSql('DROP TABLE `brand_synonym`;');
        $this->addSql('DROP TABLE `brand_typo`;');
        $this->addSql('DROP TABLE `experimental`;');
        $this->addSql('DROP TABLE `generic_ref_level1`;');
        $this->addSql('DROP TABLE `generic_ref_level2`;');
        $this->addSql('DROP TABLE `generic_ref_level3`;');
        $this->addSql('DROP TABLE `generic_ref_level4`;');
        $this->addSql('DROP TABLE `generic_synonym`;');
        $this->addSql('DROP TABLE `generic_typo`;');
        $this->addSql('DROP TABLE `country`;');
        $this->addSql('DROP TABLE `variable`;');
    }
}
