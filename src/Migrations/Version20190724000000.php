<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Adding new table for Country Continent Region Taxonomy.
 *
 * @link https://www.crwflags.com/fotw/flags/cou_reg.html#oth
 * Always refer to UN Continents known breakdown.
 *
 * @since  1.2.0
 */
final class Version20190724000000 extends AbstractMigration
{
    /**
     * @return string
     */
    public static function getCountryTaxonomyDataSQL(): string
    {
        return '
            INSERT IGNORE INTO `country_taxonomy` VALUES
                (1, "Africa", NULL),
                    (100, "North Africa", 1),
                    (101, "East Africa", 1),
                        (110, "East Central Africa", 101),
                    (102, "South Africa", 1),
                    (103, "West Africa", 1),
                    (104, "Central Africa", 1),
                (2, "Euroasia", NULL),
                    (200, "Asia", 2),
                        (201, "North Asia", 200),
                        (202, "East Asia", 200),
                        (203, "Southeast Asia", 200),
                        (204, "South Asia", 200),
                            (208, "South-Central Asia", 204),
                        (205, "Southwest Asia", 200),
                            (207, "Middle East", 205),
                        (206, "Western Asia", 200),
                        (207, "Central Asia", 200),
                    (250, "Europe", 2),
                        (251, "Northern Europe", 250),
                        (252, "Eastern Europe", 250),
                        (253, "Southern Europe", 250),
                            (260, "Southeastern Europe", 253),
                        (254, "Western Europe", 250),
                    (290, "Eastern Europe - Northern Asia", 2),
                (3, "Americas", NULL),
                    (300, "North America", 3),
                        (301, "America", 300),
                            (302, "Northeast America", 301),
                            (303, "Midwest America", 301),
                            (304, "South America", 301),
                            (305, "West America", 301),
                    (350, "Latin America and the Caribbean", 3),
                        (351, "North American", 350),
                            (352, "Central America", 351),
                            (353, "Caribbean", 351),
                                (360, "Greater Antilles", 353),
                                (361, "Lesser Antilles", 353),
                                    (362, "Leeward Islands", 361),
                                    (363, "Windward Islands", 361),
                                    (364, "Leeward Antilles", 361),
                        (354, "South America", 350),
                            (355, "Northern South America", 354),
                            (356, "Northeastern South America", 354),
                                (390, "The Guianas", 356),
                            (357, "Southern South America", 354),
                                (391, "Southern Cone", 357),
                            (358, "Western South America", 354),
                                (392, "Andean states", 358),
                            (359, "Eastern South America", 354),
                            (370, "North West South America", 354),
                            (371, "Central South America", 354),
                            (372, "Central Eastern South America", 354),
                (4, "Antarctica", NULL),
                    (401, "East Antarctica", 4),
                    (402, "West Antarctica", 4),
                (5, "Australia / Oceania", NULL),
                    (500, "Australia & New Zealand", 5),
                        (501, "Australia", 500),
                        (502, "New Zealand", 500),
                    (550, "Oceania", 5),
                        (551, "Melanesia", 550),
                        (552, "Micronesia", 550),
                        (553, "Polynesia", 550),
                (6, "Indian Ocean", NULL),

                (5000, "EU5", 250)
        ;';
    }

    public function getDescription() : string
    {
        return 'Adding new table for Country Continent Region Taxonomy.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE `country_taxonomy` (
                `id`        INT(11)     UNSIGNED NOT NULL AUTO_INCREMENT,
                `name`      VARCHAR(50) NOT NULL,
                `parent`    INT(11)     UNSIGNED DEFAULT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB
        ;');

        $this->addSql('
            ALTER TABLE `country_taxonomy`
                ADD CONSTRAINT `FK_CED8E9803D8E604F`
                    FOREIGN KEY (`parent`)
                        REFERENCES `country_taxonomy`(`id`)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
        ;');

        $this->addSql(self::getCountryTaxonomyDataSQL());
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `country_taxonomy` DROP FOREIGN KEY `FK_CED8E9803D8E604F`;');

        $this->addSql('DROP TABLE `country_taxonomy`;');
    }
}
