<?php

namespace App\Utility;

/**
 * This is super-helper class available to any to use.
 *
 * @since 1.0.0
 */
class Helpers
{
    /**
     * Method to format Int/Float value into string.
     *
     * @param int|float  $value
     * @param string     $singular
     * @param string     $plural
     *
     * @return string
     */
    public static function stringNumFormat($value, string $singular = '', string $plural = 's'): string
    {
        $postfix = self::stringPluralFormat($value, $singular, $plural);

        $value = number_format($value, 0);

        return $value . (empty($postfix) ? '' : ' ' . $postfix);
    }

    /**
     * Method to determine and return for plural for word if related.
     *
     * @param int|float  $value
     * @param string     $singular
     * @param string     $plural
     *
     * @return string
     */
    public static function stringPluralFormat($value, string $singular = '', string $plural = 's'): string
    {
        if (!empty($singular)) {
            if (strlen($plural) == 1) {
                return $singular . ($value > 1 ? $plural : '');
            }
            elseif ($value > 1) {
                return $plural;
            }
        }

        return $singular;
    }

    /**
     * Method to convert byte-size into human-readable value.
     *
     * @param float  $bytes
     * @param int    $decimals
     *
     * @return string
     */
    public static function convertHumanReadableSize($bytes, int $decimals = 2): string
    {
        $factor = (string) $bytes;
        $factor = floor((strlen($factor) - 1) / 3);

        $sz = array();
        if ($factor > 0) {
            $sz = array('K', 'M', 'G', 'T');
        }

        $seg = '';
        if (isset($sz[$factor - 1])) {
            $seg = $sz[$factor - 1];
        }

        return @sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . $seg . 'b';
    }

    /**
     * Method to convert seconds into human-readable value.
     *
     * @param int  $inputSeconds
     * @param int  $initial
     * @param int  $specific       0 for all detail, other will cut off after n
     *
     * @return string
     */
    public static function convertHumanReadbleTime(int $inputSeconds, int $initial = 0, int $specific = 0): string
    {
        if (!empty($initial)) {
            $inputSeconds = ($inputSeconds - $initial);
        }

        $secondsInAMinute = 60;
        $secondsInAnHour  = 60 * $secondsInAMinute;
        $secondsInADay    = 24 * $secondsInAnHour;

        // Extract days
        $days = floor($inputSeconds / $secondsInADay);

        // Extract hours
        $hourSeconds = $inputSeconds % $secondsInADay;
        $hours       = floor($hourSeconds / $secondsInAnHour);

        // Extract minutes
        $minuteSeconds = $hourSeconds % $secondsInAnHour;
        $minutes       = floor($minuteSeconds / $secondsInAMinute);

        // Extract the remaining seconds
        $remainingSeconds = $minuteSeconds % $secondsInAMinute;
        $seconds          = ceil($remainingSeconds);

        // Format and return
        $timeParts = array();
        $sections  = array(
            'day'    => (int) $days,
            'hour'   => (int) $hours,
            'minute' => (int) $minutes,
            'second' => (int) $seconds,
        );

        if (array_sum($sections) === 0) {
            return 'Just now';
        }

        if (!empty($specific)) {
            $sections = array_filter($sections);
            $sections = array_splice($sections, 0, $specific);
        }

        foreach ($sections as $name => $value) {
            if ($value > 0) {
                $timeParts[] = $value . ' ' . $name . (1 == $value ? '' : 's');
            }
        }

        return implode(', ', $timeParts);
    }

    /**
     * Method to remove all redundant white-spaces into one.
     *
     * @param string  $string
     *
     * @return string
     */
    public static function removeExcessWhitespace(string $string): string
    {
        $string = (string) preg_replace(
            array('/^\s*/', '/\s*$/', '/\s+/'),
            array('', '', ' '),
            $string
        );

        return $string;
    }

    /**
     * Method to check if 2 array do have Equal array Values.
     * Only check first level only.
     *
     * @param array    $arrayA
     * @param array    $arrayB
     * @param boolean  $skipType
     *
     * @return boolean
     */
    public static function validateEqualArrayValueSet(array $arrayA, array $arrayB, bool $skipType = false): bool
    {
        $arrayA = array_values($arrayA);
        $arrayB = array_values($arrayB);

        if (count($arrayA) != count($arrayB)) {
            return false;
        }

        if (!$skipType) {
            $typesA = array_map('gettype', $arrayA);
            $typesB = array_map('gettype', $arrayB);

            if (!self::validateEqualArrayValueSet($typesA, $typesB, true)) {
                return false;
            }
        }

        // suppressing notice array-string conversion
        return (@array_diff($arrayA, $arrayB) === @array_diff($arrayB, $arrayA));
    }

    /**
     * Method to search in $tree array-value same to $subset, return key/index.
     *
     * @param array  $tree
     * @param array  $subset
     *
     * @return string|integer|null
     */
    public static function getKeyOfSameArraySet(array $tree, array $subset, ?string $in = null)
    {
        foreach ($tree as $key => $set) {
            if (empty($in)) {
                if (self::validateEqualArrayValueSet($set, $subset)) {
                    return $key;
                }
            }
            elseif (
                (isset($set[$in])) &&
                (self::validateEqualArrayValueSet($set[$in], $subset))
            ) {
                return $key;
            }
        }

        return null;
    }

    /**
     * Method to get event-span message between start and end dates.
     * Convert two dates to readable manner.
     * However this did not validate if $dateTwo before  $dateOne.
     *
     * @param  \DateTimeInterface|null  $dateOne        Start Date
     * @param  \DateTimeInterface|null  $dateTwo        End Date
     * @param  string|null              $dateWraperTag  Tag-wrap the date and time
     *
     * @return string|null
     */
    public static function getEventReadSpanBetweenDates(?\DateTimeInterface $dateOne, ?\DateTimeInterface $dateTwo, ?string $dateWraperTag = null): ?string
    {
        $span = null;

        $wrap = ['', ''];
        if (null !== $dateWraperTag) {
            $wrap = ["<{$dateWraperTag}>", "</{$dateWraperTag}>"];
        }

        if (
            (!empty($dateOne)) &&
            (!empty($dateTwo))
        ) {
            if ($dateOne->format('Y-m-d') == $dateTwo->format('Y-m-d')) {
                // - one day
                $span = 'On ' . $wrap[0] . $dateOne->format('l, jS M Y') . $wrap[1];
            }
            elseif ($dateOne->format('Y') == $dateTwo->format('Y')) {
                // - same year
                if ($dateOne->format('m') == $dateTwo->format('m')) {
                    // - same month
                    $span = 'Between ' . $wrap[0] . $dateOne->format('l, jS') . $wrap[1] . ' and ' . $wrap[0] . $dateTwo->format('l, jS M Y') . $wrap[1];
                }
                else {
                    $span = 'Between ' . $wrap[0] . $dateOne->format('l, jS M') . $wrap[1] . ' and ' . $wrap[0] . $dateTwo->format('l, jS M Y') . $wrap[1];
                }
            }
            else {
                // - different year
                $span = 'Between ' . $wrap[0] . $dateOne->format('l, jS M Y') . $wrap[1] . ' and ' . $wrap[0] . $dateTwo->format('l, jS M Y') . $wrap[1];
            }
        }
        elseif (
            (empty($dateOne)) &&
            (!empty($dateTwo))
        ) {
            $span = 'Until ' . $wrap[0] . $dateTwo->format('l, jS M Y') . $wrap[1];
        }
        elseif (
            (empty($dateTwo)) &&
            (!empty($dateOne))
        ) {
            $span = 'Begins ' . $wrap[0] . $dateOne->format('l, jS M Y') . $wrap[1];
        }

        return $span;
    }
}
