<?php

namespace App\Utility;

use App\Exception\LotusConnectionException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

/**
 * Utility: LotusConnection.
 *
 * @since  1.0.0
 */
class LotusConnection
{
    /**
     * @var  float  Timeout value in second to check database connection
     */
    const TIMEOUT = 2.0;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * DB Connection.
     *
     * @var \PDO
     */
    protected $lotus;

    /**
     * Remote Connection Flag.
     *
     * @var bool
     */
    private $connect = false;

    /**
     * DB DSN String.
     * env(LOTUS_DB_DSN)
     *
     * @var string
     */
    private $dbDsn;

    /**
     * DB DSN String Parsed as Array.
     *
     * @var array
     */
    private $dbDsnParsed;

    /**
     * DB Username.
     * env(LOTUS_DB_USER)
     *
     * @var string
     */
    private $dbUser;

    /**
     * DB Password.
     * env(LOTUS_DB_PASSWORD)
     *
     * @var string
     */
    private $dbPass;

    /**
     * Remote UpSync Feature Flag.
     * env(LOTUS_DB_UPSYNC)
     *
     * @var bool
     */
    private $upsync;

    /**
     * Error Flag.
     *
     * @var bool
     */
    private $error = true;

    /**
     * Error message.
     *
     * @var string
     */
    private $errorMsg = 'Not Initiated';

    /**
     * Gathering data from configuration file.
     *
     * @param LoggerInterface  $logger
     * @param string           $lotusDbDsn
     * @param string           $lotusDbUser
     * @param string           $lotusDbPwd
     * @param bool             $lotusDbSync
     */
    public function __construct(LoggerInterface $logger, string $lotusDbDsn, string $lotusDbUser, string $lotusDbPwd, bool $lotusDbSync)
    {
        $this->logger = $logger;

        $this->dbDsn  = $lotusDbDsn;
        $this->dbUser = $lotusDbUser;
        $this->dbPass = $lotusDbPwd;
        $this->upsync = $lotusDbSync;

        $this->dbDsnParsed = self::_extractDsnString($this->dbDsn);
    }

    /**
     * Method to get Connection status Flag.
     *
     * @return bool
     */
    public function isConnected(): bool
    {
        return $this->connect;
    }

    /**
     * Method to get UPSYNC feature status Flag.
     *
     * @return bool
     */
    public function isUpSync(): bool
    {
        return $this->upsync;
    }

    /**
     * Method to return current remote db info.
     *
     * @return array
     */
    public function getInfo(): array
    {
        return array(
            'dsn'       => ($this->dbDsn ? $this->dbDsn : false),
            'host'      => ($this->dbDsnParsed['host'] ? $this->dbDsnParsed['host'] : false),
            'port'      => ($this->dbDsnParsed['port'] ? $this->dbDsnParsed['port'] : false),
            'dbname'    => ($this->dbDsnParsed['dbname'] ? $this->dbDsnParsed['dbname'] : false),
            'options'   => ($this->dbDsnParsed['options'] ? $this->dbDsnParsed['options'] : false),
            'username'  => ($this->dbUser ? $this->dbUser : false),
            'password'  => ($this->dbPass ? str_repeat('*', strlen($this->dbPass)) : false),
            'connected' => empty($this->error),
            'upsync'    => $this->upsync,
            'error'     => $this->error,
            'error-msg' => ($this->error ? $this->errorMsg : false),
        );
    }

    /**
     * Method to establish connection to remote db.
     *
     * @param  bool $throwConnErr Throw LotusConnectionException, otherwise log and @see $this->error
     *
     * @throws LotusConnectionException If connection failed, IF $throwConnErr == true
     * @throws \PDOException            If error occur during PDO setup
     *
     * @return bool Connection status
     */
    public function makeConnection(bool $throwConnErr = false): bool
    {
        if ($this->isConnected()) {
            return $this->isConnected();
        }

        $this->error    = true;
        $this->errorMsg = 'Unknown Issue';

        do {
            // check readable details
            if (
                (empty($this->dbDsn)) ||
                (empty($this->dbDsnParsed['host'])) ||
                (empty($this->dbDsnParsed['port'])) ||
                (empty($this->dbDsnParsed['dbname'])) ||
                (empty($this->dbUser)) ||
                (empty($this->dbPass))
            ) {
                $this->errorMsg = 'Incomplete Access Details';
                break;
            }

            // check DNS/IP
            $process = Process::fromShellCommandline('ping -c 1 -W ' . self::TIMEOUT . ' ' . $this->dbDsnParsed['host']);
            $process->run();
            if (!$process->isSuccessful()) {
                $this->errorMsg = 'Host did not Reachable; ping failed.';
                break;
            }

            // check host
            $fp = @fsockopen($this->dbDsnParsed['host'], $this->dbDsnParsed['port'], $errno, $errstr, self::TIMEOUT);
            if (!$fp) {
                $this->errorMsg = 'Host did not Responded; fsockopen() failed.';
                break;
            }

            // make connection
            try {
                $this->lotus = new \PDO('dblib:' . $this->dbDsn, $this->dbUser, $this->dbPass);
                $this->lotus->setAttribute(\PDO::ATTR_PERSISTENT, false);
                $this->lotus->setAttribute(\PDO::MYSQL_ATTR_FOUND_ROWS, true);
                $this->lotus->setAttribute(\PDO::ATTR_TIMEOUT, self::TIMEOUT);
                $this->lotus->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

                // connection settings; @TODO: Review
                // $this->lotus->exec("SET ANSI_WARNINGS ON");
                // $this->lotus->exec("SET ANSI_PADDING ON");
                // $this->lotus->exec("SET ANSI_NULLS ON");
                // $this->lotus->exec("SET QUOTED_IDENTIFIER ON");
                // $this->lotus->exec("SET CONCAT_NULL_YIELDS_NULL ON");
            }
            catch (\PDOException $e) {
                $this->errorMsg = 'Connection Failed: ' . $e->getMessage();
                break;
            }

            $this->error    = (empty($this->lotus));
            $this->errorMsg = 'Connection Established';
        } while (false);

        if ($this->error) {
            $this->logger->error('LOTUS: Db Connection failed.', array(
                'error-msg' => $this->errorMsg,
            ));

            if ($throwConnErr) {
                throw new LotusConnectionException($this->errorMsg);
            }
        }
        else {
            $this->connect = true;
            $this->logger->info('LOTUS: Db Connection Success.');
        }

        return empty($this->error);
    }

    /**
     * Method to return the Connection Object, if connected.
     * May throw Exception if connection were failed.
     *
     * @throws \Exception On Error
     *
     * @return \PDO Connection
     */
    public function getConnection(): \PDO
    {
        if ($this->error) {
            throw new \Exception($this->errorMsg, \Symfony\Component\HttpFoundation\Response::HTTP_BAD_GATEWAY);
        }

        return $this->lotus;
    }

    /**
     * Method to extracting DSN String.
     *
     * @param string    $string
     *
     * @return array<string, mixed>
     */
    public static function _extractDsnString(string $string): array
    {
        $defaults = array(
            'host'    => '',
            'port'    => '',
            'dbname'  => '',
            'options' => array(),
        );

        $parts = explode(';', $string);
        $parts = array_map('trim', $parts);

        foreach ($parts as $part) {
            $part = explode('=', $part);
            $part = array_map('trim', $part);

            if (2 != count($part)) {
                continue;
            }

            list($key, $value) = $part;

            if (isset($defaults[$key])) {
                $defaults[$key] = $value;
            }
            else {
                $defaults['options'][$key] = $value;
            }
        }

        if (!empty($defaults['host'])) {
            $host = explode(':', $defaults['host']);
            if (2 == count($host)) {
                list($defaults['host'], $defaults['port']) = $host;
            }
        }

        return $defaults;
    }
}
