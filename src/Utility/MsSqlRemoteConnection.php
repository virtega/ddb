<?php

namespace App\Utility;

use App\Exception\MsSqlRemoteConnectionException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

/**
 * Utility: MsSqlRemoteConnection.
 * Handle PDO connection for any Remote MsSQl.
 *
 * @since  1.2.0
 */
class MsSqlRemoteConnection
{
    /**
     * @var  int  Default timeout value in second to check database connection; configurable $optTimeout
     */
    const TIMEOUT = 2;

    /**
     * @var  string  Default PDO driver - MsSQL & Sybase support; configurable $optDriver
     */
    const DRIVER = 'dblib';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * DB Connection.
     *
     * @var \PDO
     */
    private $connection;

    /**
     * Remote Connection Flag.
     *
     * @var bool
     */
    private $connected = false;

    /**
     * DB DSN String.
     *
     * @var string
     */
    private $dbDsn;

    /**
     * DB DSN String Parsed as Array.
     *
     * @var array
     */
    private $dbDsnParsed;

    /**
     * DB Username.
     *
     * @var string
     */
    private $dbUser;

    /**
     * DB Password.
     *
     * @var string
     */
    private $dbPass;

    /**
     * DB Connection Timeout.
     *
     * @var int
     */
    private $optTimeout;

    /**
     * DB PDO Driver.
     *
     * @var string
     */
    private $optDriver;

    /**
     * Error Flag.
     *
     * @var bool
     */
    private $error = true;

    /**
     * Error message.
     *
     * @var string
     */
    private $errorMsg = 'Not Initiated';

    /**
     * Gathering data from configuration file.
     *
     * @param LoggerInterface  $logger
     * @param string           $dbDsn
     * @param string           $dbUser
     * @param string           $dbPwd
     * @param string|null      $optDriver
     * @param int|null         $optTimeout
     */
    public function __construct(LoggerInterface $logger, string $dbDsn, string $dbUser, string $dbPwd, ?string $optDriver = null, ?int $optTimeout = null)
    {
        $this->logger = $logger;

        $this->dbDsn  = $dbDsn;
        $this->dbUser = $dbUser;
        $this->dbPass = $dbPwd;

        $this->optDriver  = $optDriver ?? self::DRIVER;
        $this->optTimeout = $optTimeout ?? self::TIMEOUT;

        $this->dbDsnParsed = self::_extractDsnString($this->dbDsn);
    }

    /**
     * Method to get Connection status Flag.
     *
     * @return bool
     */
    public function isConnected(): bool
    {
        return $this->connected;
    }

    /**
     * Method to return current remote db info.
     *
     * @return array
     */
    public function getInfo(): array
    {
        return [
            'dsn'       => ($this->dbDsn ? $this->dbDsn : false),
            'host'      => ($this->dbDsnParsed['host'] ? $this->dbDsnParsed['host'] : false),
            'port'      => ($this->dbDsnParsed['port'] ? $this->dbDsnParsed['port'] : false),
            'dbname'    => ($this->dbDsnParsed['dbname'] ? $this->dbDsnParsed['dbname'] : false),
            'options'   => ($this->dbDsnParsed['options'] ? $this->dbDsnParsed['options'] : false),
            'username'  => ($this->dbUser ? $this->dbUser : false),
            'password'  => ($this->dbPass ? str_repeat('*', strlen($this->dbPass)) : false),
            'connected' => empty($this->error),
            'error'     => $this->error,
            'error-msg' => ($this->error ? $this->errorMsg : false),
        ];
    }

    /**
     * Method to establish connection to remote db.
     *
     * @param  bool $throwConnErr Throw MsSqlRemoteConnectionException, otherwise log and @see $this->error
     *
     * @throws MsSqlRemoteConnectionException If connection failed, IF $throwConnErr == true
     * @throws \PDOException                  If error occur during PDO setup
     *
     * @return bool Connection status
     */
    public function makeConnection(bool $throwConnErr = false): bool
    {
        if ($connected = $this->isConnected()) {
            return $connected;
        }

        $this->error    = true;
        $this->errorMsg = 'Unknown Issue';

        $connElapsed = microtime(true);
        do {
            // check readable details
            if (
                (empty($this->dbDsn)) ||
                (empty($this->dbDsnParsed['host'])) ||
                (empty($this->dbDsnParsed['port'])) ||
                (empty($this->dbDsnParsed['dbname'])) ||
                (empty($this->dbUser)) ||
                (empty($this->dbPass))
            ) {
                $this->errorMsg = 'Incomplete Access Details';
                break;
            }

            // check DNS/IP
            $process = Process::fromShellCommandline('ping -c 1 ' . $this->dbDsnParsed['host']);
            $process->run();
            if (!$process->isSuccessful()) {
                $this->errorMsg = 'Host did not Reachable; ping failed.';
                break;
            }

            // check host
            $fp = @fsockopen($this->dbDsnParsed['host'], $this->dbDsnParsed['port'], $errno, $errstr, $this->optTimeout);
            if (!$fp) {
                $this->errorMsg = 'Host did not Responded; fsockopen() failed.';
                break;
            }

            // make connection
            try {
                $this->connection = new \PDO($this->optDriver . ':' . $this->dbDsn, $this->dbUser, $this->dbPass);
                $this->connection->setAttribute(\PDO::ATTR_PERSISTENT, false);
                $this->connection->setAttribute(\PDO::MYSQL_ATTR_FOUND_ROWS, true);
                $this->connection->setAttribute(\PDO::ATTR_TIMEOUT, $this->optTimeout);
                $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }
            catch (\PDOException $e) {
                $this->errorMsg = $e->getMessage();
                break;
            }

            $this->error    = (empty($this->connection));
            $this->errorMsg = 'Connection Established';
        } while (false);

        $connElapsed = microtime(true) - $connElapsed;
        $connElapsed = number_format($connElapsed, 4);

        if ($this->error) {
            $this->logger->error('MsSQL Remote: Db Connection failed.', array(
                'error-msg' => $this->errorMsg,
                'dsn'       => (!empty($this->dbDsnParsed['host']) ? $this->dbDsnParsed : $this->dbDsn),
                'elapsed'   => $connElapsed,
            ));

            if ($throwConnErr) {
                throw new MsSqlRemoteConnectionException(
                    (!empty($this->dbDsnParsed['host']) ? $this->dbDsnParsed['host'] : $this->dbDsn),
                    $this->errorMsg
                );
            }
        }
        else {
            $this->connected = true;
            $this->logger->info("MsSQL Remote: Db Connection Success in {$connElapsed} seconds.");
        }

        return empty($this->error);
    }

    /**
     * Method to return the Connection Object, if connected.
     * May throw Exception if connection were failed.
     *
     * @throws \Exception On Error
     *
     * @return \PDO Connection
     */
    public function getConnection(): \PDO
    {
        if (
            ($this->error) ||
            (!$this->isConnected())
        ) {
            throw new \Exception(
                $this->errorMsg,
                \Symfony\Component\HttpFoundation\Response::HTTP_BAD_GATEWAY
            );
        }

        return $this->connection;
    }

    /**
     * Method to extracting DSN String.
     *
     * @param string  $string
     *
     * @return array<string, mixed>
     */
    public static function _extractDsnString(string $string): array
    {
        $defaults = array(
            'host'    => '',
            'port'    => '',
            'dbname'  => '',
            'options' => array(),
        );

        $parts = explode(';', $string);
        $parts = array_map('trim', $parts);

        foreach ($parts as $part) {
            $part = explode('=', $part);
            $part = array_map('trim', $part);

            if (2 != count($part)) {
                continue;
            }

            list($key, $value) = $part;

            if (isset($defaults[$key])) {
                $defaults[$key] = $value;
            }
            else {
                $defaults['options'][$key] = $value;
            }
        }

        if (!empty($defaults['host'])) {
            $host = explode(':', $defaults['host']);
            if (2 == count($host)) {
                list($defaults['host'], $defaults['port']) = $host;
            }
        }

        return $defaults;
    }
}
