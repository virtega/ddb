<?php

namespace App\Utility;

/**
 * Using OpenSSL basics.
 *
 * @since 1.0.0
 */
class Crypto
{
    /**
     * cipher method, but this MAY change id not supported - see __construct().
     *
     * @var string
     */
    protected static $cipher = 'AES-128-CBC';

    /**
     * string from env(APP_SECRET)
     *
     * @var string
     */
    private static $appSecret = '';

    /**
     * Checking and replace Cipher method if not supported.
     *
     * @param string $appSecret
     */
    public function __construct(string $appSecret)
    {
        self::$appSecret = $appSecret;

        $ciphers = openssl_get_cipher_methods();
        if (!in_array(self::$cipher, $ciphers)) {
            // change to nearest one ~ "AES-256-XXX"
            $ori = preg_replace('/^([a-z0-9-]+)-[a-z0-9]+$/i', '$1', self::$cipher);
            if (empty($ori)) {
                // @codeCoverageIgnoreStart
                $ori = self::$cipher;
                // @codeCoverageIgnoreEnd
            }

            foreach ($ciphers as $cipher) {
                if (false !== stripos($cipher, $ori)) {
                    self::$cipher = $cipher;
                    break;
                }
            }

            // recheck - still not available
            if (!in_array(self::$cipher, $ciphers)) {
                // any, first
                self::$cipher = $ciphers[0];
            }
        }
    }

    /**
     * Method to encrypt a string into hashed strong.
     *
     * @param string  $plaintext
     *
     * @return string  Encrypted String
     */
    public static function encrypt(string $plaintext): string
    {
        $converted = (string) openssl_encrypt(
            $plaintext,
            self::$cipher,
            self::getSecret(),
            OPENSSL_RAW_DATA,
            self::getIv()
        );

        $converted = base64_encode($converted);
        $replaced  = preg_replace('/(=+)$/i', '', $converted);
        if (!empty($replaced)) {
            $converted = $replaced;
        }
        $converted = strrev($converted);

        return $converted;
    }

    /**
     * Method to decrypt a hashed strong intp a string.
     *
     * @param string  $encrypted  Encrypted String
     *
     * @return string  Plain-text
     */
    public static function decrypt(string $encrypted): string
    {
        $encrypted = strrev($encrypted);
        $encrypted = (string) base64_decode($encrypted);

        return (string) openssl_decrypt(
            $encrypted,
            self::$cipher,
            self::getSecret(),
            OPENSSL_RAW_DATA,
            self::getIv()
        );
    }

    /**
     * Method to get Secret String.
     * This taken from .evn APP_SECRET.
     *
     * @return string
     */
    private static function getSecret(): string
    {

        return self::$appSecret;
    }

    /**
     * Method to get IV string or length.
     *
     * @return int
     */
    private static function getIvLength(): int
    {
        return (int) openssl_cipher_iv_length(self::$cipher);
    }

    /**
     * Method to get IV string or length.
     *
     * @return string
     */
    private static function getIv(): string
    {
        $ivlen = self::getIvLength();

        $hex = implode('', array(
            'v2CGBUHPuvResb0F',
            'L0Pwm3qPNwIP1fmR',
            'L57p4JtNurQKIUNW',
            'DD1WRVDAZYsggtnB',
            'V3Jhi9RHxgOvalpc',
            'FEE4QNA0tqP4xO2D',
            'keKVNhHIp5f4L31t',
            '1aqMYVEy53spFuSg',
            '1pfJaXK1KCnsFofD',
            'zXh3Cg0Aok1Y0Y1e',
            '58m3orel2uH7lxRQ',
            't6niTN2jLVEVo84A',
            'wgpQurRQlwYgNRZi',
            'X5Klc5uGzduMHfHf',
        ));

        return substr($hex, 0, $ivlen);
    }
}
