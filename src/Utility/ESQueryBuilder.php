<?php


namespace App\Utility;


class ESQueryBuilder
{
    private const ES_RESULT_SIZE = 100;
    /**
     * @var string
     */
    private $ESIndex;
    /**
     * @var string
     */
    private $fbXDaysSearch;

    /**
     * ESQueryBuilder constructor.
     *
     * @param string $ESindex
     * @param int $fbXDaysSearch
     */
    public function __construct(string $ESindex, int $fbXDaysSearch)
    {
        $this->ESIndex = $ESindex;
        $this->fbXDaysSearch = ' -' . $fbXDaysSearch . ' days';
    }

    /**
     * @param string $date
     * @param bool $isXdays
     *
     * @return array
     */
    public function getDateRange(string $date, bool $isXdays = false): array
    {
        $range = '';
        if ($isXdays) {
            $range = $this->fbXDaysSearch;
        }
        $date   .= ' 00:00:00';
        $from   = strtotime($date . $range);
        $to     = strtotime($date . ' +1 days');

        return [$from, $to];
    }

    /**
     * @param string $date
     * @param int $size
     *
     * @return array
     */
    public function buildPapersOnlyQuery(string $date = '', int $size = 0): array
    {
        $conditions = [
            'bool' => [
                'should' => [
                    'match' => ['type' => 'DGAbstract'],
                ],
            ],
        ];

        if (!empty($date)) {
            list($from, $to) = $this->getDateRange($date);
            $conditions = [
                'bool' => [
                    'must' => [
                        ['term' => ['type' => 'DGAbstract']]
                    ],
                    'filter' => [
                        'range' => [
                            'updated' => [
                                'gt' => $from,
                                'lt' => $to
                            ]
                        ]
                    ],
                ],
            ];
        }

        $esQuery = $this->initQuery();
        $esQuery['body'] = [
                '_source' => [
                    'title',
                    'updated',
                    'status',
                ],
                'query' => $conditions,
                'sort' => [
                    'updated' => ['order' => 'desc']
                ],
        ];
        if ($size > 0) {
            $esQuery['size'] = $size;
        }

        return $this->forceGETClient($esQuery);
    }

    /**
     * @param string $date
     * @param int $size
     *
     * @return array
     */
    public function buildTimeStampQuery(string $date = '', int $size = 0): array
    {
        $conditions = [
            'bool' => [
                'must' => [
                    ['term' => ['status' => 1]],
                    ['terms' => ['type' => ['DGNews', 'DGDispatch']]],
                ],
            ],
        ];

        if (!empty($date)) {
            list($from, $to) = $this->getDateRange($date);
            $conditions['bool']['filter'] = [
                'range' => [
                    'updated' => [
                        'gt' => $from,
                        'lt' => $to
                    ]
                ]
            ];
        }

        $esQuery = $this->initQuery();
        $esQuery['body'] = [
                '_source' => [
                    'title',
                    'updated',
                    'type',
                ],
                'query' => $conditions,
                'sort' => [
                    'updated' => ['order' => 'desc']
                ],
        ];
        if ($size > 0) {
            $esQuery['size'] = $size;
        }

        return $this->forceGETClient($esQuery);
    }

    /**
     * @param string $date
     * @param bool $getIds
     * @param int $size
     *
     * @return array
     */
    public function buildAbstractCountsESQuery(string $date, bool $getIds = false, int $size = 0): array
    {
        list($from, $to) = $this->getDateRange($date);
        $conditions = [
            'bool' => [
                'must' => [
                    ['term' => ['status' => 1]],
                    ['term' => ['type' => 'DGAbstract']]
                ],
                'filter' => [
                    'range' => [
                        'updated' => [
                            'gt' => $from,
                            'lt' => $to
                        ]
                    ]
                ],
            ],
        ];
        $esQuery = [
            'size' => 0,
            'index' => $this->ESIndex,
            'body' => [
                'query' => $conditions,
                'sort' => [
                    'updated' => ['order' => 'desc']
                ],
            ]
        ];
        if ($getIds) {
            $esQuery['size'] = $size;
            $esQuery['body']['_source'] = ['id', 'title'];
        }

        return $this->forceGETClient($esQuery);
    }

    /**
     * @param string $date
     * @param int $size
     * @param bool $isXdays
     *
     * @return array
     */
    public function buildFBArticlesQuery(string $date = '', int $size = 0, bool $isXdays = false): array
    {
        $conditions = [];

        if (!empty($date)) {
            list($from, $to) = $this->getDateRange($date, $isXdays);
            $conditions = [
                'bool' => [
                    'filter' => [
                        'range' => [
                            'updated' => [
                                'gt' => $from,
                                'lt' => $to
                            ]
                        ]
                    ],
                ],
            ];
        }

        $esQuery = $this->initQuery();
        $esQuery['body'] = [
                '_source' => [
                    'id',
                    'title',
                    'updated',
                    'type',
                    'status',
                ],
                'sort' => [
                    'updated' => ['order' => 'desc']
                ],
        ];
        if ($size > 0) {
            $esQuery['size'] = $size;
        }
        if (!empty($conditions)) {
            $esQuery['body']['query'] = $conditions;
        }

        return $this->forceGETClient($esQuery);
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function buildFBGetArticleByIdQuery(int $id) {
        $esQuery = $this->initQuery();
        $esQuery['body']['query'] = [
            'terms' => [
                '_id' => [$id]
            ]
        ];
        $esQuery['size'] = 1;

        return $this->forceGETClient($esQuery);
    }

    /**
     * @return array
     */
    private function initQuery() {
        return [
            'scroll' => '1m',
            'size' => self::ES_RESULT_SIZE,
            'index' => $this->ESIndex
        ];
    }

    /**
     * @param array $query
     *
     * @return array
     */
    private function forceGETClient($query) {
        $query['client'] = [
            'curl' => [
                CURLOPT_CUSTOMREQUEST => 'GET'
            ]
        ];

        return $query;
    }
}