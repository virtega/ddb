<?php

namespace App\Utility;

/**
 * This class write CSV file content, for Data Export features Use.
 *
 * @since 1.0.0
 */
class ExportCsv
{
    /**
     * Temporary file path.
     *
     * @var string
     */
    private $tmpFile;

    /**
     * Active File Object, with Append options.
     *
     * @var ?resource
     */
    private $fileHp;

    /**
     * Constructing method, prepare the file name.
     *
     * @param string $process  Process name which be use for export filename postfix.
     *
     * @throws \Exception  Failed to create or rename temporary file; possible permission issue. @see tempnam()
     */
    public function __construct(string $process = '')
    {
        // create file on temp dir
        $date = date('Y-m-d-His');
        $tmpDir = sys_get_temp_dir();
        if (empty($tmpDir)) {
            // @codeCoverageIgnoreStart
            $tmpDir = '/tmp';
            // @codeCoverageIgnoreEnd
        }
        $this->tmpFile = (string) @tempnam($tmpDir, \App\Service\CoreService::BRANDING . "-{$process}-{$date}-");

        // double check, tempnam() is know to just Notice if file creation failed
        if (!file_exists($this->tmpFile)) {
            throw new \Exception(
                'Unable to create CSV file on Temporary Folder.',
                \Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        // change the filename with extension
        rename($this->tmpFile, $this->tmpFile . '.csv');
        if (file_exists($this->tmpFile . '.csv')) {
            $this->tmpFile .= '.csv';
        }
        else {
            // this is almost impossible to happen, as the file created;
            // - permission should not be an issue
            // - clashing should be avoided by tempnam()
            // @codeCoverageIgnoreStart
            throw new \Exception(
                'Unable to renamed CSV file.',
                \Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR
            );
            // @codeCoverageIgnoreEnd
        }

        // change permission tempnam() use 600 by default
        chmod($this->tmpFile, 0777);

        $this->openFile();
    }

    /**
     * Method destruct, close the file.
     */
    public function __destruct()
    {
        $this->closeFile();
    }

    /**
     * Method to return File Path,.
     *
     * @uses \ExportCsv::$tmpFile
     *
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->tmpFile;
    }

    /**
     * Method to open/create file for edit, with append option.
     *
     * @uses \ExportCsv::$fileHp
     *
     * @throws \Exception  Failed to create or rename temporary file; possible permission issue. @see tempnam()
     */
    public function openFile(): void
    {
        // open to write the file
        $fileResource = fopen($this->tmpFile, 'a');
        if (empty($fileResource)) {
            // @codeCoverageIgnoreStart
            throw new \Exception(
                'Unable to create handler for the file.',
                \Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR
            );
            // @codeCoverageIgnoreEnd
        }

        $this->fileHp = $fileResource;

        // UTF-8 Support
        fputs($this->fileHp, (chr(0xEF) . chr(0xBB) . chr(0xBF)));
    }

    /**
     * Method to close opened file.
     *
     * @uses \ExportCsv::$fileHp
     */
    public function closeFile(): void
    {
        if (!empty($this->fileHp)) {
            // write file
            fclose($this->fileHp);
            $this->fileHp = null;
        }
    }

    /**
     * Method to write Array of data as row set.
     *
     * @param array $cells  indexed Array to each columns
     */
    public function writeRowCells(array $cells): void
    {
        if ($this->fileHp) {
            fputcsv($this->fileHp, $cells);
        }
    }

    /**
     * Method to write multiple rows of data.
     *
     * @param array $rows  rows indexed Array to each rows, with list of Array columns
     */
    public function writeRows(array $rows): void
    {
        // each rows
        foreach ($rows as $row) {
            // the row
            $this->writeRowCells($row);
        }
    }
}
