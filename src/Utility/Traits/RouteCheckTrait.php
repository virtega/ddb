<?php

namespace App\Utility\Traits;

use Symfony\Component\HttpFoundation\Request;

/**
 * Helper to check request URI.
 * This is allow to share same logic to any controllers/services.
 *
 * @since  1.0.0
 */
trait RouteCheckTrait
{
    /**
     * Method to check if request made on API Routes
     *
     * @param  Request  $request
     *
     * @return boolean
     */
    public static function validateRequestOnApi(Request $request): bool
    {
        return (
            (strpos($request->attributes->get('_route'), 'api_') !== false) ||
            (preg_match('/^\/v\d+(?:\.?\d?)*(?!:\/?)/', $request->getPathInfo(), $versionFound) >= 1)
        );
    }
}
