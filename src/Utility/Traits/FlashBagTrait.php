<?php

namespace App\Utility\Traits;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Helper to post Flash Messages for UI use.
 * This avoid controller to repeatedly validate session.
 * And better for phpstan that not able to see abstract calls.
 *
 * @since  1.0.0
 */
trait FlashBagTrait
{
    /**
     * @var string
     */
    private static $appEnv = 'prod';

    public static function setEnv(string $appEnv): void
    {
        self::$appEnv = $appEnv;
    }

    /**
     * Method to set Flash Message by Using Session from Request
     *
     * @param Request  $request
     * @param string   $status
     * @param string   $message
     */
    public static function setOffFlashMsgByRequest(Request $request, string $status, string $message): void
    {
        // avoid header error
        // @codeCoverageIgnoreStart
        if (
            ('dev' == self::$appEnv) &&
            (!empty($request->get('debug'))) &&
            ($request->get('debug') == 'tokens')
        ) {
            return;
        }
        // @codeCoverageIgnoreEnd

        if ($request->hasSession()) {
            /**
             * @var SessionInterface
             */
            $session = $request->getSession();
            self::setOffFlashMsg($session, $status, $message);
        }
    }

    /**
     * Method to set Flash Message by using given Session
     *
     * @param SessionInterface|null  $session
     * @param string                 $status
     * @param string                 $message
     */
    public static function setOffFlashMsg(?SessionInterface $session = null, string $status, string $message): void
    {
        if (
            (!empty($session)) &&
            ($session instanceof Session)
        ) {
            $session->getFlashBag()->add($status, $message);
        }
    }
}
