<?php

namespace App\Utility\Repository;

use App\Entity\Family\DrugTypeFamily;
use App\Entity\Family\KeywordTypeFamily;
use App\Service\Factory\SearchServiceFactory;

/**
 * Helper Class which be use by DrugRepository.
 *
 * @since 1.0.0
 */
class DrugRepositoryHelpers
{
    /**
     * Date format.
     *
     * @var string
     */
    const DATEFORMAT = 'Y-m-d H:i:s';

    /**
     * For DEV to get generic-full-code on Search SQL
     * Require to remove sql_mode "ONLY_FULL_GROUP_BY"
     *
     * @var  boolean
     */
    const DEV_SEARCH_GET_FULLCODE = false;

    /**
     * Helper extract object IDs into _keys arrays, clean up structure.
     *
     * @param array   $data
     * @param string  $key
     * @param string  $getId
     */
    public static function reSortEntityKeywords(array &$data, string $key, string $getId = 'getId'): void
    {
        if (!empty($data[$key])) {
            $temp = array();
            foreach ($data[$key] as $keyword) {
                $keyword_id = (int) $keyword->$getId();

                $data[$key . '_keys'][] = $keyword_id;
                $temp[$keyword_id]      = $keyword;
            }
            $data[$key] = $temp;
        }
    }

    /**
     * Converting query string into MySQL Entity `name` WHERE statement.
     *
     * @param string  $query
     *
     * @return string
     */
    public static function translateQueryToSql(string &$query): string
    {
        // sanitize
        $query_type = 'LIKE';
        $query      = trim($query);

        // break-down - ignore symbols
        $query = (array) preg_split('/[ -]/', $query);
        $query = implode('%', $query);

        $both_capped = [false, false];

        // intercept prefix
        $q_postfix = $q_prefix = '%';
        if ('^' == substr($query, 0, 1)) {
            $q_prefix = '';
            $both_capped[0] = true;
            $query    = str_replace('^', '', $query);

            // custom for regex; target for INDEX search
            // this is available by button only, other will be ignored
            if (0 === strpos($query, ':numeric')) {
                $query_type = 'REGEXP';
                $q_postfix  = '';
                $query      = '^([0-9]|[[:space:]]?[0-9])';
            }
            elseif (0 === strpos($query, ':symbol')) {
                $query_type = 'REGEXP';
                $q_postfix  = '';
                $query      = '^[^a-zA-Z0-9 ]';
            }
            elseif (1 == strlen($query)) {
                // this enable us to ignore white space issue
                $query_type = 'REGEXP';
                $q_postfix  = '';
                $query      = "^[[:space:]]?[{$query}]";
            }
            else {
                // normal query NOT REGEX; respect space
                $query = str_replace('+', ' ', $query);
            }
        }

        // intercept postfix
        if ('$' == substr($query, -1, 1)) {
            $q_postfix = '';
            $both_capped[1] = true;
            $query     = str_replace('$', '', $query);
        }

        if (
            (empty($q_prefix)) &&
            (empty($q_postfix)) &&
            (
                (
                    (true === $both_capped[0]) &&
                    (true === $both_capped[1])
                ) &&
                (false === strpos($query, '*'))
            ) &&
            ('LIKE' == $query_type)
        ) {
            $query_type = '=';
        }

        // BASE STRUCTURE
        $query = "{$q_prefix}{$query}{$q_postfix}";

        // intercept + sign; must have space
        $query = (string) preg_replace('/[+]/', ' ', $query);

        // intercept wildcard symbols& clean up
        $query = (string) preg_replace("/%\*|\*%|\*+|%+/", '%', $query);

        // safe query
        $query = addslashes($query);

        // format
        $query = "{$query_type} '{$query}'";

        return $query;
    }

    /**
     * Method just provide SQL query-string based on type.
     *
     * @param SearchServiceFactory  $src
     * @param boolean               $overallCount   Flag if need just Count
     *
     * @return string
     * EXAMPLE: WITH DISTICT; $output_type: 'search' / 'download-full' / 'download-less'
     *  array:6 [
     *    "name"         => "Ryan||Ryan New Brand||RyanGen||RyanGen2||Ryan||Ryanodex"
     *    "experimental" => "3523||3523||3523"
     *    "generic"      => "17174||17174||17174||17175||17175||17175"
     *    "brand"        => "15211||3084"
     *    "source"       => "1||2b||2g||2g||1||2b"
     *    "locked"       => "1"
     *  ]
     *
     * EXAMPLE: WITH NO DISTICT; $output_type: 'autocomplete'
     *  array:6 [
     *    "name"         => "rRyan Dolask||Ryan||rRyan Dolask5||Ryan||RyanGen31||Ryan||RyanGen31||RyanNol 4|...
     *                        ...|Ryan||Ryan||Ryan NewBrand||RyanGen||Ryan||RyanGen2||Ryan||RyanGen2||Ryanodex||Ryan||RyanGen3"
     *    "experimental" => "3523"
     *    "generic"      => "17174||17175||17176||17177||17178||17182||17190||17191"
     *    "brand"        => "15211||15213||15216||15221||15222||15223||15224||1bae6a6b-e163-11e7-9536-080027b9ea01|...
     *                        ...|1bae6ab0-e163-11e7-9536-080027b9ea01||3084"
     *    "source"       => "1||1||2b||2g||1||2g"
     *    "locked"       => "1"
     *   ]
     *
     * NOTE:
     *   - in Example SEPARATOR is "||"
     *   - ID Tracing based on `name` index
     *      - `name` is use to track the original source ID `index`.
     *      - While, DISTICT will remove `index`; so ID & Name are related but not correspond.
     *   - GROUP are known that will change the order, so result are not always going to same , hence `locked` were used
     *   - UUID given above is result from a null
     *   - `locked` can be ignore
     *   - v1.1 "source" changed to 4-5 string format "[x|yy|zz?]
     *       - x: TYPE;     (1) main drug, (2) keyword
     *       - y: SCORE;    (00-99) on Natural ASC Sort order, so smaller is higher
     *       - z: SPECIFIC; (e|g|gt|gs|b|bt|bs) Drug Type specific
     */
    public static function getDrugSearchSql(SearchServiceFactory &$src, bool $overallCount = false): string
    {
        // TYPE: safe fallback reset to defaulting 'any'
        if (!in_array($src->getType(), DrugTypeFamily::$family)) {
            $src->setType(SearchServiceFactory::TYPE_ANY);
        }

        // TERM
        $query = (string) $src->getTerm();
        self::translateQueryToSql($query);
        $src->setTermQuery($query);
        $querySafe = (string) preg_replace('/[^a-z0-9 -]/i', ' ', (string) $src->getTerm());
        $querySafe = trim($querySafe);

        // sql
        $sql = array();

        // qbse - group 3rd last
        if ($overallCount) {
            $sql[] = 'SELECT SQL_CALC_FOUND_ROWS ';
        }
        else {
            $sql[] = 'SELECT ';
        }

        $sql[] = "  GROUP_CONCAT(DISTINCT(`name`) SEPARATOR '||') AS `name`,";
        $sql[] = '  `experimental`,';
        $sql[] = "  GROUP_CONCAT(DISTINCT(`generic`) SEPARATOR '||') AS `generic`,";
        if (self::DEV_SEARCH_GET_FULLCODE) {
            // @codeCoverageIgnoreStart
            $sql[] = '  `generic_fullcode`,';
            // @codeCoverageIgnoreEnd
        }
        $sql[] = "  GROUP_CONCAT(DISTINCT(`brand`) SEPARATOR '||') AS `brand`,";
        $sql[] = "  GROUP_CONCAT(DISTINCT(`source`) SEPARATOR '||') AS `source`,";
        $sql[] = '  MIN(`locked`) AS `locked`';
        $sql[] = 'FROM (';

        // qbs - group 2nd
        $sql[] = 'SELECT';
        $sql[] = "  GROUP_CONCAT(DISTINCT(`name`) SEPARATOR '||') AS `name`,";
        $sql[] = "  GROUP_CONCAT(DISTINCT(`experimental`) SEPARATOR '||') AS `experimental`,";
        $sql[] = '  `generic`,';
        if (self::DEV_SEARCH_GET_FULLCODE) {
            // @codeCoverageIgnoreStart
            $sql[] = '  `generic_fullcode`,';
            // @codeCoverageIgnoreEnd
        }
        $sql[] = "  GROUP_CONCAT(DISTINCT(`brand`) SEPARATOR '||') AS `brand`,";
        $sql[] = "  GROUP_CONCAT(DISTINCT(`source`) SEPARATOR '||') AS `source`,";
        $sql[] = '  MIN(`locked`) AS `locked`';
        $sql[] = 'FROM (';

        // qb - group 1st
        $sql[] = 'SELECT';
        $sql[] = "  GROUP_CONCAT(DISTINCT(`name`) SEPARATOR '||') AS `name`,";
        $sql[] = "  GROUP_CONCAT(DISTINCT(`experimental`) SEPARATOR '||') AS `experimental`,";
        $sql[] = "  GROUP_CONCAT(DISTINCT(`generic`) SEPARATOR '||') AS `generic`,";
        if (self::DEV_SEARCH_GET_FULLCODE) {
            // @codeCoverageIgnoreStart
            $sql[] = '  `generic_fullcode`,';
            // @codeCoverageIgnoreEnd
        }
        $sql[] = '  `brand`,';
        $sql[] = "  GROUP_CONCAT(DISTINCT(`source`) SEPARATOR '||') AS `source`,";
        $sql[] = '  MIN(`locked`) AS `locked`';
        $sql[] = 'FROM (';

        // if NOT AUTOCOMPLETE, do not DISTICT, so everything still attached in sequence.
        if (false == $src->isAutoComplete()) {
            foreach ($sql as $si => $q) {
                $sql[$si] = preg_replace(
                    "/GROUP_CONCAT\(DISTINCT\(`([a-z]+)`\) SEPARATOR /",
                    'GROUP_CONCAT(`$1` SEPARATOR',
                    (string) $q
                );
            }
        }

        // q - pool result
        $sql[] = 'SELECT';
        $sql[] = ' (@cnt := @cnt + 1) AS `locked`,';
        $sql[] = ' `name`,';
        $sql[] = ' IFnull(`experimental`, UUID()) AS `experimental`,';
        $sql[] = ' IFnull(`generic`, UUID()) AS `generic`,';
        if (self::DEV_SEARCH_GET_FULLCODE) {
            // @codeCoverageIgnoreStart
            $sql[] = '  `generic_fullcode`,';
            // @codeCoverageIgnoreEnd
        }
        $sql[] = ' IFnull(`brand`, UUID()) AS `brand`,';
        $sql[] = ' `source`';
        $sql[] = 'FROM (';

        $excludes = $src->getExclusionList();

        // inner table break into smaller portions
        $ptn = array();

        // source "1xxe"  - experimental
        $ptn['E'] = array();
        if (
            (SearchServiceFactory::TYPE_ANY == $src->getType()) ||
            (!in_array($src->getType(), array('generic', 'brand')))
        ) {
            $ptn['E'][] = 'SELECT';
            $ptn['E'][] = '   IF(`e`.`name` = \'\', NULL, `e`.`name`) AS `name`,';
            $ptn['E'][] = '   `e`.`id` AS `experimental`,';
            $ptn['E'][] = '   `generic`.`id` AS `generic`,';
            if (self::DEV_SEARCH_GET_FULLCODE) {
                // @codeCoverageIgnoreStart
                $ptn['E'][] = '  NULL AS `generic_fullcode`,';
                // @codeCoverageIgnoreEnd
            }
            $ptn['E'][] = '   `brand`.`id` AS `brand`,';
            $ptn['E'][] = '   CONCAT(';
            $ptn['E'][] = '     \'1\',';
            $ptn['E'][] = '     LPAD((';
            $ptn['E'][] = '       99 - ';
            if (false == $src->isUnidOnly()) {
                $ptn['E'][] = "       (IF((`e`.`name` REGEXP '[[:<:]]{$querySafe}[[:>:]]') = 1, 4, 0)) - ";
                $ptn['E'][] = "       (IF((`e`.`name` REGEXP '[[:<:]]{$querySafe}') = 1, 2, 0)) - ";
            }
            $ptn['E'][] = '       (IF((`e`.`name` REGEXP \'([,+&] +| and |/)\') = 1, 0, 1))';
            $ptn['E'][] = '     ), 2, \'0\'),';
            $ptn['E'][] = '     \'e\'';
            $ptn['E'][] = '   ) AS `source`';
            $ptn['E'][] = 'FROM `experimental` `e`';
            $ptn['E'][] = 'LEFT JOIN `generic` ON `generic`.`experimental` = `e`.`id`';
            $ptn['E'][] = 'LEFT JOIN `brand` ON `brand`.`generic` = `generic`.`id`';
            $ptn['E'][] = 'WHERE ';
            if ($src->isUnidOnly()) {
                $ptn['E'][] = "`e`.`uid` <> '' AND `e`.`uid` {$query}";
            }
            else {
                $ptn['E'][] = "`e`.`name` <> '' AND `e`.`name` {$query}";
            }

            if (!empty($excludes['experimental'])) {
                $excludes['experimental'] = implode(',', $excludes['experimental']);
                $ptn['E'][] = "AND `e`.`id` NOT IN ({$excludes['experimental']})";
            }
        }

        // source "1xxg"  - generic
        $ptn['G'] = array();
        if (
            (SearchServiceFactory::TYPE_ANY == $src->getType()) ||
            (!in_array($src->getType(), array('brand', 'experimental')))
        ) {
            $ptn['G'][] = 'SELECT';
            $ptn['G'][] = '   IF(`g`.`name` = \'\', NULL, `g`.`name`) AS `name`,';
            $ptn['G'][] = '   `g`.`experimental` as `experimental`,';
            $ptn['G'][] = '   `g`.`id` AS `generic`,';
            if (self::DEV_SEARCH_GET_FULLCODE) {
                // @codeCoverageIgnoreStart
                $ptn['G'][] = '  CONCAT(`g`.`Level1`,`g`.`Level2`,`g`.`Level3`,`g`.`Level4`,`g`.`Level5`) AS `generic_fullcode`,';
                // @codeCoverageIgnoreEnd
            }
            $ptn['G'][] = '   `brand`.`id` AS `brand`,';
            $ptn['G'][] = '   CONCAT(';
            $ptn['G'][] = '     \'1\',';
            $ptn['G'][] = '     LPAD((';
            $ptn['G'][] = '       99 - ';
            if (false == $src->isUnidOnly()) {
                $ptn['G'][] = "       (IF((`g`.`name` REGEXP '[[:<:]]{$querySafe}[[:>:]]') = 1, 4, 0)) - ";
                $ptn['G'][] = "       (IF((`g`.`name` REGEXP '[[:<:]]{$querySafe}') = 1, 2, 0)) - ";
            }
            $ptn['G'][] = '       (IF((`g`.`name` REGEXP \'([,+&] +| and |/)\') = 1, 0, 1))';
            $ptn['G'][] = '       -';
            $ptn['G'][] = '       (IF(';
            $ptn['G'][] = '         CONCAT(`g`.`Level1`,`g`.`Level2`,`g`.`Level3`,`g`.`Level4`,`g`.`Level5`) = \'\',';
            $ptn['G'][] = '         0,';
            if ('generic' == $src->getType()) {
                $ptn['G'][] = '         (CHAR_LENGTH(TRIM(CONCAT(`g`.`Level1`,`g`.`Level2`,`g`.`Level3`,`g`.`Level4`,`g`.`Level5`))))';
            }
            else {
                $ptn['G'][] = '         2';
            }
            $ptn['G'][] = '       ))';
            $ptn['G'][] = '     ), 2, \'0\'),';
            $ptn['G'][] = '     \'g\'';
            $ptn['G'][] = '   ) AS `source`';
            $ptn['G'][] = 'FROM `generic` `g`';
            $ptn['G'][] = 'LEFT JOIN `brand` ON `brand`.`generic` = `g`.`id`';
            $ptn['G'][] = 'WHERE ';
            if ($src->isUnidOnly()) {
                $ptn['G'][] = "`g`.`uid` <> '' AND `g`.`uid` {$query}";
            }
            else {
                $ptn['G'][] = "`g`.`name` <> '' AND `g`.`name` {$query}";
            }

            if (!empty($excludes['generic'])) {
                $excludes['generic'] = implode(',', $excludes['generic']);
                $ptn['G'][] = "AND `g`.`id` NOT IN ({$excludes['generic']})";
            }
            if ($src->isHasCodeOnly()) {
                $ptn['G'][] = ' AND CHAR_LENGTH(TRIM(CONCAT(`g`.`Level1`,`g`.`Level2`,`g`.`Level3`,`g`.`Level4`,`g`.`Level5`))) > 0 ';
            }
        }

        // source "1xxg"  - brand
        $ptn['B'] = array();
        if (
            (SearchServiceFactory::TYPE_ANY == $src->getType()) ||
            (!in_array($src->getType(), array('generic', 'experimental')))
        ) {
            $ptn['B'][] = 'SELECT';
            $ptn['B'][] = '   IF(`b`.`name` = \'\', NULL, `b`.`name`) AS `name`,';
            $ptn['B'][] = '   `generic`.`experimental` AS `experimental`,';
            $ptn['B'][] = '   `generic`.`id` AS `generic`,';
            if (self::DEV_SEARCH_GET_FULLCODE) {
                // @codeCoverageIgnoreStart
                $ptn['B'][] = '  NULL AS `generic_fullcode`,';
                // @codeCoverageIgnoreEnd
            }
            $ptn['B'][] = '   `b`.`id` AS `brand`,';
            $ptn['B'][] = '   CONCAT(';
            $ptn['B'][] = '     \'1\',';
            $ptn['B'][] = '     LPAD((';
            $ptn['B'][] = '       99 - ';
            if (false == $src->isUnidOnly()) {
                $ptn['B'][] = "       (IF((`b`.`name` REGEXP '[[:<:]]{$querySafe}[[:>:]]') = 1, 4, 0)) - ";
                $ptn['B'][] = "       (IF((`b`.`name` REGEXP '[[:<:]]{$querySafe}') = 1, 2, 0)) - ";
            }
            $ptn['B'][] = '       (IF((`b`.`name` REGEXP \'([,+&] +| and |/)\') = 1, 0, 1))';
            $ptn['B'][] = '     ), 2, \'0\'),';
            $ptn['B'][] = '     \'b\'';
            $ptn['B'][] = '   ) AS `source`';
            $ptn['B'][] = 'FROM `brand` `b`';
            $ptn['B'][] = ' LEFT JOIN `generic` ON `generic`.`id` = `b`.`generic`';
            $ptn['B'][] = 'WHERE ';
            if ($src->isUnidOnly()) {
                $ptn['B'][] = "`b`.`uid` <> '' AND `b`.`uid` {$query}";
            }
            else {
                $ptn['B'][] = "`b`.`name` <> '' AND `b`.`name` {$query}";
            }

            if (!empty($excludes['brand'])) {
                $excludes['brand'] = implode(',', $excludes['brand']);
                $ptn['B'][] = "AND `b`.`id` NOT IN ({$excludes['brand']})";
            }
        }

        // source "2xxgs"  - generic_synonym
        // source "2xxgt"  - generic_typo
        $ptn['GK'] = array();
        if (
            (false == $src->isUnidOnly()) &&
            (
                (SearchServiceFactory::TYPE_ANY == $src->getType()) ||
                (!in_array($src->getType(), array('brand', 'experimental')))
            )
        ) {
            $ptn['GK'][] = 'SELECT';
            $ptn['GK'][] = '   IF(`gs`.`name` = \'\', NULL, `gs`.`name`) as `name`,';
            $ptn['GK'][] = '   `generic`.`experimental` AS `experimental`,';
            $ptn['GK'][] = '   `generic`.`id` AS `generic`,';
            if (self::DEV_SEARCH_GET_FULLCODE) {
                // @codeCoverageIgnoreStart
                $ptn['GK'][] = '  NULL AS `generic_fullcode`,';
                // @codeCoverageIgnoreEnd
            }
            $ptn['GK'][] = '   `brand`.`id` AS `brand`,';
            $ptn['GK'][] = '   CONCAT(';
            $ptn['GK'][] = '     \'2\',';
            $ptn['GK'][] = '     LPAD((';
            $ptn['GK'][] = '       99 - ';
            $ptn['GK'][] = "       (IF((`gs`.`name` REGEXP '[[:<:]]{$querySafe}[[:>:]]') = 1, 3, 0))";
            $ptn['GK'][] = '       -';
            $ptn['GK'][] = "       (IF((`gs`.`name` REGEXP '[[:<:]]{$querySafe}') = 1, 1, 0))";
            $ptn['GK'][] = '       -';
            $ptn['GK'][] = '       (IF((`gs`.`name` REGEXP \'([,+&] +| and |/)\') = 1, 0, 1))';
            $ptn['GK'][] = '     ), 2, \'0\'),';
            $ptn['GK'][] = '     \'gs\'';
            $ptn['GK'][] = '   ) AS `source`';
            $ptn['GK'][] = 'FROM `generic_synonym` `gs`';
            $ptn['GK'][] = ' LEFT JOIN `generic` ON `generic`.`id` = `gs`.`generic`';
            $ptn['GK'][] = ' LEFT JOIN `brand` ON `brand`.`generic` = `generic`.`id`';
            $ptn['GK'][] = 'WHERE ';
            $ptn['GK'][] = "`gs`.`name` <> '' AND `gs`.`name` {$query}";

            if (!empty($excludes['generic_synonym'])) {
                $excludes['generic_synonym'] = implode(',', $excludes['generic_synonym']);
                $ptn['GK'][] = "AND `gs`.`id` NOT IN ({$excludes['generic_synonym']})";
            }

            $ptn['GK'][] = ' UNION ALL';

            $ptn['GK'][] = 'SELECT';
            $ptn['GK'][] = '   IF(`gt`.`name` = \'\', NULL, `gt`.`name`) as `name`,';
            $ptn['GK'][] = '   `generic`.`experimental` AS `experimental`,';
            $ptn['GK'][] = '   `generic`.`id` AS `generic`,';
            if (self::DEV_SEARCH_GET_FULLCODE) {
                // @codeCoverageIgnoreStart
                $ptn['GK'][] = '  NULL AS `generic_fullcode`,';
                // @codeCoverageIgnoreEnd
            }
            $ptn['GK'][] = '   `brand`.`id` AS `brand`,';
            $ptn['GK'][] = '   CONCAT(';
            $ptn['GK'][] = '     \'2\',';
            $ptn['GK'][] = '     LPAD((';
            $ptn['GK'][] = '       99 - ';
            $ptn['GK'][] = "       (IF((`gt`.`name` REGEXP '[[:<:]]{$querySafe}[[:>:]]') = 1, 3, 0))";
            $ptn['GK'][] = '       -';
            $ptn['GK'][] = "       (IF((`gt`.`name` REGEXP '[[:<:]]{$querySafe}') = 1, 1, 0))";
            $ptn['GK'][] = '       -';
            $ptn['GK'][] = '       (IF((`gt`.`name` REGEXP \'([,+&] +| and |/)\') = 1, 0, 1))';
            $ptn['GK'][] = '     ), 2, \'0\'),';
            $ptn['GK'][] = '     \'gt\'';
            $ptn['GK'][] = '   ) AS `source`';
            $ptn['GK'][] = 'FROM `generic_typo` `gt`';
            $ptn['GK'][] = ' LEFT JOIN `generic` ON `generic`.`id` = `gt`.`generic`';
            $ptn['GK'][] = ' LEFT JOIN `brand` ON `brand`.`generic` = `generic`.`id`';
            $ptn['GK'][] = 'WHERE ';
            $ptn['GK'][] = "`gt`.`name` <> '' AND `gt`.`name` {$query}";

            if (!empty($excludes['generic_typo'])) {
                $excludes['generic_typo'] = implode(',', $excludes['generic_typo']);
                $ptn['GK'][] = "AND `gt`.`id` NOT IN ({$excludes['generic_typo']})";
            }
        }

        // source "2xxbs"  - brand_synonym
        // source "2xxbt"  - brand_typo
        $ptn['BK'] = array();
        if (
            (false == $src->isUnidOnly()) &&
            (
                (SearchServiceFactory::TYPE_ANY == $src->getType()) ||
                (!in_array($src->getType(), array('generic', 'experimental')))
            )
        ) {
            $ptn['BK'][] = 'SELECT';
            $ptn['BK'][] = '   IF(`bs`.`name` = \'\', NULL, `bs`.`name`) as `name`,';
            $ptn['BK'][] = '   `generic`.`experimental` AS `experimental`,';
            $ptn['BK'][] = '   `generic`.`id` AS `generic`,';
            if (self::DEV_SEARCH_GET_FULLCODE) {
                // @codeCoverageIgnoreStart
                $ptn['BK'][] = '  NULL AS `generic_fullcode`,';
                // @codeCoverageIgnoreEnd
            }
            $ptn['BK'][] = '   `brand`.`id` AS `brand`,';
            $ptn['BK'][] = '   CONCAT(';
            $ptn['BK'][] = '     \'2\',';
            $ptn['BK'][] = '     LPAD((';
            $ptn['BK'][] = '       99 - ';
            $ptn['BK'][] = "       (IF((`bs`.`name` REGEXP '[[:<:]]{$querySafe}[[:>:]]') = 1, 3, 0))";
            $ptn['BK'][] = '       -';
            $ptn['BK'][] = "       (IF((`bs`.`name` REGEXP '[[:<:]]{$querySafe}') = 1, 1, 0))";
            $ptn['BK'][] = '       -';
            $ptn['BK'][] = '       (IF((`bs`.`name` REGEXP \'([,+&] +| and |/)\') = 1, 0, 1))';
            $ptn['BK'][] = '     ), 2, \'0\'),';
            $ptn['BK'][] = '     \'bs\'';
            $ptn['BK'][] = '   ) AS `source`';
            $ptn['BK'][] = 'FROM `brand_synonym` `bs`';
            $ptn['BK'][] = ' LEFT JOIN `brand` ON `brand`.`id` = `bs`.`brand`';
            $ptn['BK'][] = ' LEFT JOIN `generic` ON `generic`.`id` = `brand`.`generic`';
            $ptn['BK'][] = 'WHERE ';
            $ptn['BK'][] = "`bs`.`name` <> '' AND `bs`.`name` {$query}";

            if (!empty($excludes['brand_synonym'])) {
                $excludes['brand_synonym'] = implode(',', $excludes['brand_synonym']);
                $ptn['BK'][] = "AND `bs`.`id` NOT IN ({$excludes['brand_synonym']})";
            }

            $ptn['BK'][] = ' UNION ALL';

            $ptn['BK'][] = 'SELECT';
            $ptn['BK'][] = '   IF(`bt`.`name` = \'\', NULL, `bt`.`name`) as `name`,';
            $ptn['BK'][] = '   `generic`.`experimental` AS `experimental`,';
            $ptn['BK'][] = '   `generic`.`id` AS `generic`,';
            if (self::DEV_SEARCH_GET_FULLCODE) {
                // @codeCoverageIgnoreStart
                $ptn['BK'][] = '  NULL AS `generic_fullcode`,';
                // @codeCoverageIgnoreEnd
            }
            $ptn['BK'][] = '   `brand`.`id` AS `brand`,';
            $ptn['BK'][] = '   CONCAT(';
            $ptn['BK'][] = '     \'2\',';
            $ptn['BK'][] = '     LPAD((';
            $ptn['BK'][] = '       99 - ';
            $ptn['BK'][] = "       (IF((`bt`.`name` REGEXP '[[:<:]]{$querySafe}[[:>:]]') = 0, 3, 0))";
            $ptn['BK'][] = '       -';
            $ptn['BK'][] = "       (IF((`bt`.`name` REGEXP '[[:<:]]{$querySafe}') = 0, 1, 0))";
            $ptn['BK'][] = '       -';
            $ptn['BK'][] = '       (IF((`bt`.`name` REGEXP \'([,+&] +| and |/)\') = 0, 0, 1))';
            $ptn['BK'][] = '     ), 2, \'0\'),';
            $ptn['BK'][] = '     \'bt\'';
            $ptn['BK'][] = '   ) AS `source`';
            $ptn['BK'][] = 'FROM `brand_typo` `bt`';
            $ptn['BK'][] = ' LEFT JOIN `brand` ON `brand`.`id` = `bt`.`brand`';
            $ptn['BK'][] = ' LEFT JOIN `generic` ON `generic`.`id` = `brand`.`generic`';
            $ptn['BK'][] = 'WHERE ';
            $ptn['BK'][] = "`bt`.`name` <> '' AND `bt`.`name` {$query}";

            if (!empty($excludes['brand_typo'])) {
                $excludes['brand_typo'] = implode(',', $excludes['brand_typo']);
                $ptn['BK'][] = "AND `bt`.`id` NOT IN ({$excludes['brand_typo']})";
            }
        }

        // merge portions using UNION ALL
        foreach ($ptn as $pi => $pt) {
            if (empty($pt))  {
                continue;
            }

            $ptn[$pi] = implode(' ', (array) $pt);
        }
        $ptn = array_filter($ptn);
        $sql[] = implode(' UNION ALL ', $ptn);
        unset($ptn);

        // q - pool result
        $sql[] = ') `q`';
        $sql[] = 'CROSS JOIN (SELECT @cnt := 0) AS `locking`';
        $sql[] = 'ORDER BY `source` ASC';

        // qb - group 1st
        $sql[] = ') `qb`';
        $sql[] = 'GROUP BY `brand`';
        if ($src->isUnidOnly()) {
            $sql[] = 'ORDER BY `name` ASC';
        }
        else {
            $sql[] = 'ORDER BY `source` ASC';
        }

        // qbs - group 2nd
        $sql[] = ') `qbs`';
        $sql[] = 'GROUP BY `generic`';
        if ($src->isUnidOnly()) {
            $sql[] = 'ORDER BY `name` ASC';
        }
        else {
            $sql[] = 'ORDER BY `source` ASC';
        }

        // qbse - group 3rd last
        $sql[] = ') `qbse`';
        $sql[] = 'GROUP BY `experimental`';
        if ($src->isUnidOnly()) {
            $sql[] = 'ORDER BY `name` ASC';
        }
        else {
            $sql[] = 'ORDER BY `source` ASC';
        }

        // limit
        if (!empty($src->getLimitMain())) {
            $sql[] = "LIMIT {$src->getLimitMain()}";
        }

        $sql = array_map('trim', $sql);
        $sql = implode(' ', $sql);

        return $sql;
    }

    /**
     * Method to process each result from Search Query; translateQueryToSql()
     * Data will be formatted into AutoComplete-TypeAhead format.
     *
     * Note that SQL with DISTINCT.
     *
     * @param array  $row       A Row Data
     * @param int    $subLimit  Sub / Keywords Limit
     *
     * @return array
     * EXAMPLE:
     * array:4 [
     *   "id"     => 3526
     *   "family" => "experimental"
     *   "name"   => "Ryan Gen 3 / RyanExpr50"
     *   "desc"   => "an Experimental Drug"
     * ]
     */
    public static function processSearchResultForAutoComplete(array $row, int $subLimit = 100): array
    {
        // names
        $n = explode('||', $row['name']);
        $n = array_unique($n);
        $names   = '';
        $postfix = '';
        if ((!empty($n)) && (count($n) > $subLimit)) {
            $n = array_splice($n, 0, $subLimit);
            $postfix = '...';
        }
        $names = implode(' / ', $n) . $postfix;

        // extract ids
        $e = explode('||', $row['experimental']);
        $g = explode('||', $row['generic']);
        $b = explode('||', $row['brand']);

        // remove uid()
        foreach (array('e', 'g', 'b') as $v) {
            foreach ($$v as $key => $value) {
                if (false !== strpos($value, '-')) {
                    unset($$v[$key]);
                }
            }
            if (empty($$v)) {
                $$v = array();
            }
            else {
                // reset keys
                $$v = array_values($$v);
                // reformat
                $$v = array_map('intval', $$v);
            }
        }

        // first valid source ONLY; $id will break the loop
        $sources  = explode('||', $row['source']);
        foreach ($sources as $si => $source) {
            // expect only runs once
            $sourceType = self::_breakdownSearchSourceCode($source);
            if (empty($sourceType['parent'])) {
                continue;
            }

            $family = $sourceType['parent'];

            $id     = 0;
            $desc   = '';
            switch ($family) {
                case 'experimental':
                    $id   = $e[$si];
                    $desc = 'an Experimental Drug';
                    break;

                case 'generic':
                    $id   = $g[$si];
                    $desc = 'a Generic Drug';
                    if ($sourceType['keyword']) {
                        // gs & gt
                        $desc = 'a reference to Generic Drug';
                    }
                    break;

                case 'brand':
                    $id   = $b[$si];
                    $desc = 'a Brand Drug';
                    if ($sourceType['keyword']) {
                        // bs & bt
                        $desc = 'a reference to Brand Drug';
                    }
                    break;
            }

            // ok
            if (!empty($id)) {
                $result = array(
                    'id'     => $id,
                    'family' => $family,
                    'name'   => $names,
                    'desc'   => $desc,
                );

                return $result;
            }
        }

        return array();
    }

    /**
     * Method to process each result from Search Query; translateQueryToSql()
     * Data will be formatted into DataTable for search result.
     *
     * Note that SQL with NO DISTINCT.
     *
     * @param array  $row       A Row Data
     * @param int    $subLimit  Sub / Keywords Limit
     *
     * @return array
     * EXAMPLE:
     * array:7 [
     *   "experimental"    => array:1 [
     *       0 => array:2 [
     *         "id"   => 3523
     *         "name" => "Ryan"
     *       ]
     *    ]
     *    "generic"         => array:2 [
     *        0 => array:2 [
     *          "id"   => 17178
     *          "name" => "rRyan Dolask"
     *       ]
     *       1 => array:2 [
     *           "id"   => 17176
     *           "name" => "RyanGen3"
     *       ]
     *    ]
     *   "generic_synonym" => []
     *   "generic_typo"    => []
     *   "brand"           => []
     *   "brand_synonym"   => []
     *   "brand_typo"      => []
     * ]
     */
    public static function processSearchResultForSearchPage(array $row, int $subLimit = 100): array
    {
        $result = (array) array_combine(
            DrugTypeFamily::$family,
            array_fill(0, count(DrugTypeFamily::$family), array())
        );

        // unpack
        foreach (array('name', 'experimental', 'generic', 'brand', 'source') as $v) {
            $row[$v] = explode('||', $row[$v]);
        }

        // result based on `source` INDEX for `name` reference
        // while for each drug by sequence
        $casting = array();
        foreach ($row['source'] as $index => $source) {
            // get type
            $sourceType = self::_breakdownSearchSourceCode($source);

            // check source, then caps
            if (
                (empty($sourceType['parent'])) ||
                (empty($sourceType['type'])) ||
                (count($result[$sourceType['type']]) > $subLimit)
            ) {
                continue;
            }

            // based on ID; to determine if keywords
            $id = false;
            if (isset($row[$sourceType['parent']][0])) {
                // get id
                if (false === strpos($row[$sourceType['parent']][0], '-')) {
                    // ID is not suppose to be UID()
                    // get it, remove from ref
                    $id = intval($row[$sourceType['parent']][0]);
                    unset($row[$sourceType['parent']][0]);
                    $row[$sourceType['parent']] = array_values($row[$sourceType['parent']]);
                }
            }

            // get name
            if (empty($row['name'][$index])) {
                // this is wrong the NAME is not suppose to be empty
                continue;
            }

            if (!empty($id)) {
                // manual distinct
                if (!in_array($id, array_column($result[$sourceType['type']], 'id'))) {
                    $result[$sourceType['type']][$index] = array(
                        'id'   => $id,
                        'name' => array($row['name'][$index]),
                    );
                }
            }
            elseif (isset($result[$sourceType['type']][($index - 1)])) {
                // handling synonyms / typos reference
                $result[$sourceType['type']][($index - 1)]['name'][] = $row['name'][$index];
            }
        }

        // final clean up
        $count = 0;
        foreach ($result as $type => $drugs) {
            // count
            if (!empty($drugs)) {
                $count += count($drugs);
            }
            // concatenate names
            foreach ($drugs as $di => $set) {
                $drugs[$di]['name'] = implode(' / ', (array) $set['name']);
            }
            // remove index ref
            $result[$type] = array_values($drugs);
        }

        if (empty($count)) {
            return array();
        }

        return $result;
    }

    /**
     * Method to generate Download array.
     *  - Step 1, extractions.
     *  - No Entity were query, just juggling Ids.
     *
     * @param array  $output  Collected data from different extracting
     * @param array  $row     Object from each loop
     */
    public static function processSearchResultForDownload(array &$output, array $row): void
    {
        // ID baseline
        if (empty($output)) {
            // initialize array
            $output = (array) array_combine(
                DrugTypeFamily::$mainDrugs,
                array_fill(0, count(DrugTypeFamily::$mainDrugs), array())
            );
        }

        // extract ids only
        foreach ($output as $type => $id) {
            // read field, extract id
            $type_ids = explode('||', $row[$type]);
            // each id
            foreach ($type_ids as $tid => $type_id) {
                // remove UUID() ones
                if (false !== strpos($type_id, '-')) {
                    unset($type_ids[$tid]);
                }
            }
            // remove keys
            $type_ids = array_values($type_ids);
            // ensure int
            $type_ids = array_map('intval', $type_ids);

            // merge values
            if (!empty($type_ids)) {
                // create as key for status-like structure, for later use
                $type_ids = array_fill_keys($type_ids, 0);
                // merge
                $output[$type] = ($output[$type] + $type_ids);
            }
        }
    }

    /**
     * Write download file headings.
     *
     * @param bool   $complete         Type of Downloads for Full or Less
     * @param array  $maxKeywordCount  See variable initialization from previous method
     *
     * @return array  A row cells data
     */
    public static function prepareExportHeadings(bool $complete, array $maxKeywordCount): array
    {
        $headings = array();

        if ($complete) {
            // EXPERIMENTAL: basics
            $headings[] = 'Experimental-Id';
            $headings[] = 'Experimental-Name';
            $headings[] = 'Experimental-Type';
            $headings[] = 'Experimental-Valid';
            $headings[] = 'Experimental-UUID';
            $headings[] = 'Experimental-Comments';
            $headings[] = 'Experimental-Creation-Date';

            // GENERIC: basics
            $headings[] = 'Generic-Id';
            $headings[] = 'Generic-Name';
            $headings[] = 'Generic-Valid';
            $headings[] = 'Generic-UUID';
            $headings[] = 'Generic-Comments';
            $headings[] = 'Generic-Creation-Date';
            // GENERIC: extras
            $headings[] = 'Generic-Level-1';
            $headings[] = 'Generic-Level-2';
            $headings[] = 'Generic-Level-3';
            $headings[] = 'Generic-Level-4';
            $headings[] = 'Generic-Level-5';
            $headings[] = 'Generic-No-Double-Bounce';
            $headings[] = 'Generic-Auto-Encode-Exclude';
            // GENERIC: keywords
            for ($i = 1; $i <= $maxKeywordCount['generic']['synonym']; ++$i) {
                $headings[] = 'Generic-Synonym-' . $i;
                $headings[] = 'Generic-Synonym-Country-' . $i;
            }
            for ($i = 1; $i <= $maxKeywordCount['generic']['typo']; ++$i) {
                $headings[] = 'Generic-Typo-' . $i;
            }

            // BRAND: basics
            $headings[] = 'Brand-Id';
            $headings[] = 'Brand-Name';
            $headings[] = 'Brand-Valid';
            $headings[] = 'Brand-UUID';
            $headings[] = 'Brand-Comments';
            $headings[] = 'Brand-Creation-Date';
            // BRAND: extras
            $headings[] = 'Brand-Auto-Encode-Exclude';
            // BRAND: keywords
            for ($i = 1; $i <= $maxKeywordCount['brand']['synonym']; ++$i) {
                $headings[] = 'Brand-Synonym-' . $i;
                $headings[] = 'Brand-Synonym-Country-' . $i;
            }
            for ($i = 1; $i <= $maxKeywordCount['brand']['typo']; ++$i) {
                $headings[] = 'Brand-Typo-' . $i;
            }
        }
        else {
            // e
            $headings[] = 'Experimental-Id';
            $headings[] = 'Experimental-Name';
            // g
            $headings[] = 'Generic-Id';
            $headings[] = 'Generic-Name';
            // b
            $headings[] = 'Brand-Id';
            $headings[] = 'Brand-Name';
        }

        return $headings;
    }

    /**
     * Helper method to unpack the IDs in DATA-LIST key-string.
     * Example:
     *     "1|2|3"  = {"experimental" => 1, "generic" => 2, "brand" => 3}
     *     "1||3"   = {"experimental" => 1, "generic" => 0, "brand" => 3}
     *     "1||3|4" = {"experimental" => 0, "generic" => 0, "brand" => 3}
     *
     * @param string  $string
     *
     * @return array
     */
    public static function readDataSetKeys(string $string): array
    {
        $string = explode('|', $string);
        $string = (array) array_replace(
                array_fill(0, count(DrugTypeFamily::$mainDrugs), 0),
                array_slice($string, 0, count(DrugTypeFamily::$mainDrugs))
        );
        $string = (array) array_combine(DrugTypeFamily::$mainDrugs, $string);
        $string = array_map('intval', $string);

        return $string;
    }

    /**
     * Method to write temp file with data; Complex version.
     *
     * @param array  $drugs
     * @param array  $maxKeywordCount
     *
     * @return array  A row cells data
     */
    public static function downloadWriteDrugCollumnsForComplete(array $drugs, array $maxKeywordCount): array
    {
        $row = array();

        foreach ($drugs as $type => $drug) {
            if ($drug['main'] instanceof DrugTypeFamily) {
                // have drug
                $row[] = $drug['main']->getId();
                $row[] = $drug['main']->getName();
                if ('experimental' == $type) {
                    $temp  = $drug['main']->getType();
                    $row[] = ucwords($temp);
                }
                $temp  = $drug['main']->getValid();
                $row[] = (!empty($temp) ? 'Yes' : 'No');
                $row[] = $drug['main']->getUid();
                $row[] = $drug['main']->getComments();

                $creation_date = $drug['main']->getCreationDate();
                if (!empty($creation_date)) {
                    $row[] = $creation_date->format(self::DATEFORMAT);
                }
                else {
                    $row[] = '';
                }

                if ('generic' == $type) {
                    $row[] = $drug['main']->getLevel1();
                    $row[] = $drug['main']->getLevel2();
                    $row[] = $drug['main']->getLevel3();
                    $row[] = $drug['main']->getLevel4();
                    $row[] = $drug['main']->getLevel5();

                    $temp  = (int) $drug['main']->getNodoublebounce();
                    $row[] = (!empty($temp) ? 'Yes' : 'No');
                }

                if (in_array($type, array('generic', 'brand'))) {
                    $temp  = (int) $drug['main']->getAutoencodeexclude();
                    $row[] = (!empty($temp) ? 'Yes' : 'No');
                }
            }
            else {
                // no drug
                $col_size = 6;
                switch ($type) {
                    case 'experimental':
                        ++$col_size;
                        break;

                    case 'generic':
                        $col_size += 7;
                        break;

                    case 'brand':
                        $col_size += 2;
                        break;
                }

                for ($i = 0; $i < $col_size; ++$i) {
                    $row[] = '';
                }
            }

            // next for keywords only
            if ('experimental' == $type) {
                continue;
            }

            $synonyms = array();
            if (!empty($drug['synonyms'])) {
                /**
                 * @var \App\Entity\BrandSynonym[]
                 * @var \App\Entity\GenericSynonym[]
                 */
                $synonyms = array_values($drug['synonyms']);
            }
            for ($i = 0; $i < $maxKeywordCount[$type]['synonym']; ++$i) {
                if (
                    (!empty($synonyms[$i])) &&
                    (is_object($synonyms[$i]))
                ) {
                    $row[]     = $synonyms[$i]->getName();
                    $countries = $synonyms[$i]->getCountry();
                    $row[]     = implode(', ', $countries);
                }
                else {
                    $row[] = '';
                    $row[] = '';
                }
            }

            $typos = array();
            if (!empty($drug['typos'])) {
                /**
                 * @var \App\Entity\BrandTypo[]
                 * @var \App\Entity\GenericTypo[]
                 */
                $typos = array_values($drug['typos']);
            }
            for ($i = 0; $i < $maxKeywordCount[$type]['typo']; ++$i) {
                if (
                    (!empty($typos[$i])) &&
                    (is_object($typos[$i]))
                ) {
                    $row[] = $typos[$i]->getName();
                }
                else {
                    $row[] = '';
                }
            }
        }

        return $row;
    }

    /**
     * Method to write temp file with data; Simple version.
     *
     * @param array  $drugs
     *
     * @return array  A row cells data
     */
    public static function downloadWriteDrugCollumnsForLess(array $drugs): array
    {
        $row = array();

        foreach ($drugs as $type => $drug) {
            if (
                (!empty($drug)) &&
                (is_object($drug))
            ) {
                $row[] = $drug->getId();
                $row[] = $drug->getName();
            }
            else {
                $row[] = '';
                $row[] = '';
            }
        }

        return $row;
    }

    /**
     * Method to breakdown source String, @see getDrugSearchSql() comment.
     *
     * @param string $source  Single source string of 4-5 characters only [x|yy|zz?].
     *
     * @return array
     */
    private static function _breakdownSearchSourceCode(string $source): array
    {
        $result = array(
            'type'    => false,
            'parent'  => false,
            'keyword' => false,
        );

        $find = preg_match('/(\d{1})(\d{2})((?:e|g(?:t|s)?|b(?:t|s)?){1})/', $source, $found);

        if (empty($find)) {
            return $result;
        }

        switch($found[3]) {
            case 'e':
                $result['type']   = 'experimental';
                $result['parent'] = 'experimental';
                break;

            case 'g':
                $result['type']   = 'generic';
                $result['parent'] = 'generic';
                break;

            case 'gs':
                $result['type']    = 'generic_synonym';
                $result['parent']  = 'generic';
                $result['keyword'] = true;
                break;

            case 'gt':
                $result['type']    = 'generic_typo';
                $result['parent']  = 'generic';
                $result['keyword'] = true;
                break;

            case 'b':
                $result['type']   = 'brand';
                $result['parent'] = 'brand';
                break;

            case 'bs':
                $result['type']    = 'brand_synonym';
                $result['parent']  = 'brand';
                $result['keyword'] = true;
                break;

            case 'bt':
                $result['type']    = 'brand_typo';
                $result['parent']  = 'brand';
                $result['keyword'] = true;
                break;
        }

        return $result;
    }
}
