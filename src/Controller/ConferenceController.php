<?php

namespace App\Controller;

use App\Entity\Conference;
use App\Service\ConferenceCoreService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UI /conference
 *
 * Extending BaseController which extends Controller.
 *
 * Class handing Pages for Conference CRUD.
 *
 * @since 1.2.0
 */
class ConferenceController extends BaseController
{
    use \App\Controller\Traits\BasicFormControllerTrait;
    use \App\Controller\Traits\CountryControllerTrait;
    use \App\Controller\Traits\SpecialtyControllerTrait;

    /**
     * @Route("/conference/search", name="conference_search")
     * @IsGranted("ROLE_USER", message="Conference Search only available for User.")
     *
     * Render Conference Search Page.
     *
     * @param  Request   $request
     *
     * @return Response
     */
    public function searchAction(Request $request): Response
    {
        $fields = [
            'name'          => (string) $request->get('name', ''),
            'city'          => (array)  $request->get('city', []),
            'country'       => (array)  $request->get('country', []),
            'region'        => (array)  $request->get('region', []),
            'specialty'     => (array)  $request->get('specialty', []),
            'startdate'     => (string) $request->get('startdate', ''),
            'enddate'       => (string) $request->get('enddate', ''),
            'ids'           => (string) $request->get('ids', ''),
            'keycongress'   => (string) $request->get('keycongress', ''),
            'oncruise'      => (string) $request->get('oncruise', ''),
            'online'        => (string) $request->get('online', ''),
            'hasguide'      => (string) $request->get('hasguide', ''),
            'discontinued'  => (string) $request->get('discontinued', ''),
            'showcorrupted' => (string) $request->get('showcorrupted', 'no'),
        ];

        $arrAYNSelection = [
            ''    => 'Any',
            'yes' => 'Yes',
            'no'  => 'No',
        ];

        $template = [
            'city'          => $this->__getSelectionCities( $fields['city'] ),
            'country'       => $this->__getSelectionCountries( $fields['country'] ),
            'region'        => $this->__getSelectionRegions( $fields['region'] ),
            'specialty'     => $this->__getSelectionSpecialties( $fields['specialty'] ),
            'keycongress'   => $this->__getRadioboxSetSettings( $fields['keycongress'],   $arrAYNSelection ),
            'oncruise'      => $this->__getRadioboxSetSettings( $fields['oncruise'],      $arrAYNSelection ),
            'online'        => $this->__getRadioboxSetSettings( $fields['online'],        $arrAYNSelection ),
            'hasguide'      => $this->__getRadioboxSetSettings( $fields['hasguide'],      $arrAYNSelection ),
            'discontinued'  => $this->__getRadioboxSetSettings( $fields['discontinued'],  $arrAYNSelection ),
            'showcorrupted' => $this->__getRadioboxSetSettings( $fields['showcorrupted'], ['' => 'Show All', 'no' => 'Hide Them']),
        ];

        $meta = [
            'searched' => (!empty($request->query->keys())),
            'count'    => 0,
            'overall'  => 0,
            'perpage'  => 500,
            'page'     => (int) $request->get('page', 1),
            'maxpage'  => 0,
        ];

        $results = [];
        $restbl_dates = [];
        if ($meta['searched']) {
            // copy and sanitize for search
            $terms = $fields;
            $terms['city']      = $this->__filterSelectionSelected($template['city'],      'selected', 'name');
            $terms['specialty'] = $this->__filterSelectionSelected($template['specialty'], 'selected', 'id');

            /**
             * @var \App\Repository\ConferenceRepository<Conference>
             */
            $confRepo = $this->em->getRepository(Conference::class);

            /**
             * @var int
             */
            $meta['overall'] = $confRepo->searchResultCount($terms);

            if ($meta['overall']) {
                $meta['maxpage'] = (int) ceil($meta['overall'] / $meta['perpage']);
                $meta['offset'] = (($meta['page'] - 1) * $meta['perpage']);

                /**
                 * @var Conference[]
                 */
                $results = $confRepo->search($terms, $meta['perpage'], $meta['offset']);

                $meta['count'] = count($results);

                // extra for result tables
                if ($meta['count']) {
                    foreach ($results as $result) {
                        $date = \App\Utility\Helpers::getEventReadSpanBetweenDates($result->getStartDate(), $result->getEndDate(), null);
                        if (empty($date)) {
                            $date = [
                                $result->getStartDate(),
                                $result->getEndDate(),
                            ];
                            $date = array_filter($date);
                            $date = implode(' to ', $date);
                        }
                        $restbl_dates[ $result->getId() ] = $date;
                    }
                }
            }
        }

        $tokens = [
            'head'      => [
                'title'    => 'Conference Advance Search',
                'entity'   => 'Conferences',
                'action'   => 'Search',
                'subtitle' => 'Advance Search',
            ],
            'template' => $template,
            'form'     => $fields,
            'meta'     => $meta,
            'results'  => $results,
            'restable' => [
                'dates' => $restbl_dates,
            ],
        ];

        return $this->renderResponse('pages/conferences/search.html.twig', $tokens);
    }

    /**
     * @Route("/conference/registry", name="conference_registry")
     * @IsGranted("ROLE_EDITOR", message="Conference Registry only available for Editor.")
     *
     * Render Conference Registry Page.
     *
     * @param  Request                $request
     * @param  ConferenceCoreService  $conferenceService
     *
     * @return Response
     */
    public function registryAction(Request $request, ConferenceCoreService $conferenceService): Response
    {
        list($form, $template) = $this->prepareFieldAndTempleteTokens($request, null);

        if (!empty($request->request->keys())) {
            try {
                $conference = $conferenceService->create($form);

                $this->addFlash('info', 'Conference has been successfully created!');
                return $this->redirectToRoute('conference_view', ['id' => $conference->getId()]);
            }
            catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());
            }
        }

        $tokens = [
            'head' => [
                'title'    => 'Conference New Registration',
                'entity'   => 'Conferences',
                'action'   => 'Create New',
                'subtitle' => 'Registration',
            ],
            'template' => $template,
            'form'     => $form,
        ];

        return $this->renderResponse('pages/conferences/registry.html.twig', $tokens);
    }

    /**
     * @Route("/conference/modify/{id}", name="conference_modify", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_EDITOR", message="Conference Modify only available for Editor.")
     *
     * Render Conference Modify Page.
     *
     * @param  Request                $request
     * @param  ConferenceCoreService  $conferenceService
     * @param  int                    $id                 Conference ID
     *
     * @return Response
     */
    public function modifyAction(Request $request, ConferenceCoreService $conferenceService, $id): Response
    {
        if (!($conference = $this->loadRequestedConference($id)) instanceof Conference) {
            return $conference;
        }

        if (!empty($request->request->keys())) {
            list($form, $template) = $this->prepareFieldAndTempleteTokens($request, null);

            try {
                $updatedConference = $conferenceService->update($conference, $form);

                $this->addFlash('info', 'Conference has been successfully updated!');
                return $this->redirectToRoute('conference_view', ['id' => $updatedConference->getId()]);
            }
            catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());
            }
        }

        list($form, $template) = $this->prepareFieldAndTempleteTokens($request, $conference);

        $tokens = [
            'head' => [
                'title'    => 'Edit Conference #' . $id,
                'entity'   => 'Conferences',
                'action'   => 'Edit',
                'subtitle' => 'Modify Conference #' . $id,
            ],
            'template'   => $template,
            'form'       => $form,
            'conference' => $conference,
        ];

        return $this->renderResponse('pages/conferences/modify.html.twig', $tokens);
    }

    /**
     * @Route("/conference/view/{id}", name="conference_view", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_USER", message="Conference Modify only available for User.")
     *
     * Render Conference View Page.
     *
     * @param  Request   $request
     * @param  int       $id        Conference ID
     *
     * @return Response
     */
    public function viewAction(Request $request, $id): Response
    {
        if (!($conference = $this->loadRequestedConference($id)) instanceof Conference) {
            return $conference;
        }

        $parse_links = [];
        foreach ($conference->getDetailsContactLinks() as $li => $link) {
            $parse_links[$li] = $this->__parsingAndSanitizeUrl($link['url']);
        }

        $tokens = [
            'head' => [
                'title'    => 'View Conference #' . $id,
                'entity'   => 'Conferences',
                'action'   => 'View',
                'subtitle' => 'Viewing Conference #' . $id,
            ],
            'template' => [
                'id' => $id,
            ],
            'conference'  => $conference,
            'event_span'  => \App\Utility\Helpers::getEventReadSpanBetweenDates($conference->getStartDate(), $conference->getEndDate()),
            'parse_links' => $parse_links,
        ];

        return $this->renderResponse('pages/conferences/view.html.twig', $tokens);
    }

    /**
     * @Route("/conference/remove/{id}", name="conference_remove", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_EDITOR", message="Conference Removal only available for Editor.")
     *
     * Delete Conference.
     *
     * @param  Request                $request
     * @param  ConferenceCoreService  $conferenceService
     * @param  int                    $id                 Conference ID
     *
     * @return Response
     */
    public function deleteAction(Request $request, ConferenceCoreService $conferenceService, $id): Response
    {
        if (!($conference = $this->loadRequestedConference($id)) instanceof Conference) {
            return $conference;
        }

        try {
            $conferenceService->delete($conference);

            $this->addFlash('info', 'Conference has been successfully removed!');
            return $this->redirectToRoute('conference_search');
        }
        catch (\Exception $e) {
            $this->addFlash('danger', $e->getMessage());
        }

        return $this->redirectToRoute('conference_view', ['id' => $id]);
    }

    /**
     * Method to load Conference by ID, else redirect page to Dashboard with flash message.
     *
     * @param int $id
     *
     * @return Conference|Response
     */
    private function loadRequestedConference(int $id)
    {
        /**
         * @var \App\Repository\ConferenceRepository<Conference>
         */
        $confRepo = $this->em->getRepository(Conference::class);

        /**
         * @var Conference|null
         */
        $conference = $confRepo->findOneBy(['id' => $id]);

        if (empty($conference)) {
            $this->addFlash('danger', "Requested Conference #{$id} is not available.");
            return $this->redirectToRoute('dashboard');
        }

        return $conference;
    }

    /**
     * Method to prepare Template token by merging the setting, Conference and posted query.
     *
     * @param Request         $request
     * @param Conference|null $conference
     *
     * @return array[array 'fields', array 'template']
     */
    private function prepareFieldAndTempleteTokens(Request $request, ?Conference $conference): array
    {
        $form = [
            'name'         => (string) $request->get('name',         ConferenceCoreService::getFormDefaults('name')),
            'startdate'    => (string) $request->get('startdate',    ConferenceCoreService::getFormDefaults('startdate')),
            'enddate'      => (string) $request->get('enddate',      ConferenceCoreService::getFormDefaults('enddate')),
            'city'         => (string) $request->get('city',         ConferenceCoreService::getFormDefaults('city')),
            'cityguide'    => (string) $request->get('cityguide',    ConferenceCoreService::getFormDefaults('cityguide')),
            'state'        => (string) $request->get('state',        ConferenceCoreService::getFormDefaults('state')),
            'country'      => (string) $request->get('country',      ConferenceCoreService::getFormDefaults('country')),
            'region'       => (int)    $request->get('region',       ConferenceCoreService::getFormDefaults('region')),
            'oncruise'     => (string) $request->get('oncruise',     ConferenceCoreService::getFormDefaults('oncruise')),
            'online'       => (string) $request->get('online',       ConferenceCoreService::getFormDefaults('online')),
            'keycongress'  => (string) $request->get('keycongress',  ConferenceCoreService::getFormDefaults('keycongress')),
            'discontinued' => (string) $request->get('discontinued', ConferenceCoreService::getFormDefaults('discontinued')),
            'specialty'    => (array)  $request->get('specialty',    ConferenceCoreService::getFormDefaults('specialty')),
            'contactnote'  => (string) $request->get('contactnote',  ConferenceCoreService::getFormDefaults('contactnote')),
            'contactphone' => (array)  $request->get('contactphone', ConferenceCoreService::getFormDefaults('contactphone')),
            'contactemail' => (array)  $request->get('contactemail', ConferenceCoreService::getFormDefaults('contactemail')),
            'contactfax'   => (array)  $request->get('contactfax',   ConferenceCoreService::getFormDefaults('contactfax')),
            'hasguide'     => (string) $request->get('hasguide',     ConferenceCoreService::getFormDefaults('hasguide')),
            'links'        => (array)  $request->get('links',        ConferenceCoreService::getFormDefaults('links')),
            'uniquename'   => (string) $request->get('uniquename',   ConferenceCoreService::getFormDefaults('uniquename')),
        ];

        $form['contactphone'] = $this->__getSelectionContactsRepeaterFields($form['contactphone']);
        $form['contactemail'] = $this->__getSelectionContactsRepeaterFields($form['contactemail']);
        $form['contactfax']   = $this->__getSelectionContactsRepeaterFields($form['contactfax']);
        $form['links']        = $this->__getSelectionLinksRepeaterFields($form['links']);

        if (null !== $conference) {
            $specialties = [];
            foreach ($conference->getSpecialties() as $specialty) {
                $specialties[] = $specialty->getSpecialty()->getId();
            }
            $form = [
                'name'         => ($conference->getName()       ??  ConferenceCoreService::getFormDefaults('name')),
                'startdate'    => (null !== $conference->getStartDate() ? $conference->getStartDate()->format('Y-m-d') : ConferenceCoreService::getFormDefaults('startdate')),
                'enddate'      => (null !== $conference->getEndDate()   ? $conference->getEndDate()->format('Y-m-d')   : ConferenceCoreService::getFormDefaults('enddate')),
                'city'         => ($conference->getCity()       ??  ConferenceCoreService::getFormDefaults('city')),
                'cityguide'    => ($conference->getCityGuide()  ??  ConferenceCoreService::getFormDefaults('cityguide')),
                'state'        => $conference->getDetailsState(),
                'country'      => (null !== $conference->getCountry() ? $conference->getCountry()->getIsoCode()  : ConferenceCoreService::getFormDefaults('country')),
                'region'       => (null !== $conference->getRegion()  ? ($conference->getRegion()->getId() ?? 0) : 0 ),
                'oncruise'     => ($conference->isCruise() ?       'yes' : 'no'),
                'online'       => ($conference->isOnline() ?       'yes' : 'no'),
                'keycongress'  => ($conference->isKeyEvent() ?     'yes' : 'no'),
                'discontinued' => ($conference->isDiscontinued() ? 'yes' : 'no'),
                'specialty'    => $specialties,
                'contactnote'  => $conference->getDetailsContactDesc(),
                'contactphone' => $conference->getDetailsContactPhones(),
                'contactemail' => $conference->getDetailsContactEmails(),
                'contactfax'   => $conference->getDetailsContactFaxes(),
                'hasguide'     => ($conference->isHasGuide() ?     'yes' : 'no'),
                'links'        => $conference->getDetailsContactLinks(),
                'uniquename'   => $conference->getUniqueName() ?? ConferenceCoreService::getFormDefaults('uniquename'),
            ];
        }

        $template = [
            'id'            => (null !== $conference ? $conference->getId() : false),
            'city'          => $this->__getSelectionCities( [ $form['city'] ] ),
            'country'       => $this->__getSelectionCountries( [ $form['country'] ] ),
            'region'        => $this->__getSelectionRegions( [ $form['region'] ] ),
            'oncruise'      => $this->__getRadioboxSetSettings( $form['oncruise'] ),
            'online'        => $this->__getRadioboxSetSettings( $form['online'] ),
            'keycongress'   => $this->__getRadioboxSetSettings( $form['keycongress'] ),
            'discontinued'  => $this->__getRadioboxSetSettings( $form['discontinued'] ),
            'hasguide'      => $this->__getRadioboxSetSettings( $form['hasguide'] ),
            'specialty'     => $this->__getSelectionSpecialties( $form['specialty'] ),
        ];

        return [$form, $template];
    }
}
