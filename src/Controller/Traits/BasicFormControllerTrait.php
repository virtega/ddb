<?php

namespace App\Controller\Traits;

use App\Entity\Journal;

/**
 * Controller Basic Form Helper
 *
 * @since 1.2.0
 */
trait BasicFormControllerTrait
{
    /**
     * Method to get list of Radiobox settings
     *
     * @param string $selected
     *
     * @return array[string 'value', string 'caption', boolean 'checked']
     */
    private function __getRadioboxSetSettings(string $selected, ?array $options = null): array
    {
        $output = [];

        if (null === $options) {
            $options = [
                'yes' => 'Yes',
                'no'  => 'No',
            ];
        }

        foreach ($options as $oi => $option) {
            $output[] = [
                'value'   => $oi,
                'caption' => $option,
                'checked' => ($oi == $selected),
            ];
        }

        return $output;
    }

    /**
     * Method to prepare Language drop down settings.
     *
     * @param array $selected
     *
     * @return array[string 'value', boolean 'selected']
     */
    private function __getSelectionLanguages(array $selected): array
    {
        $output = [];

        $selected = array_map('trim', $selected);
        $selected = array_filter($selected);
        $selectedLowered = array_map('strtolower', $selected);

        $languages = [
            'Afrikaans',
            'Arabic',
            'Bulgarian',
            'Catalan',
            'Chinese',
            'Czech',
            'Danish',
            'Dutch',
            'English',
            'French',
            'German',
            'Greek',
            'Hausa',
            'Hebrew',
            'Hindustani',
            'Hungarian',
            'Italian',
            'Japanese',
            'Korean',
            'Malay',
            'Norwegian',
            'Persian',
            'Polish',
            'Portuguese',
            'Quechua',
            'Russian',
            'Serbo-Croatian',
            'Slovak',
            'Sotho',
            'Spanish',
            'Swahili',
            'Swedish',
            'Tamil',
            'Thai',
            'Turkish',
            'Ukrainian',
            'Yoruba',
        ];

        foreach ($languages as $language) {
            $preselected = array_search( strtolower($language), $selectedLowered );
            if (false !== $preselected) {
                unset($selected[$preselected]);
                $preselected = true;
            }
            $output[] = [
                'value'    => $language,
                'selected' => $preselected,
            ];
        }

        foreach ($selected as $select) {
            if (!in_array($select, array_column($output, 'name'), true)) {
                $output[] = [
                    'value'    => $select,
                    'selected' => true,
                ];
            }
        }

        return $output;
    }

    /**
     * Method to get the list of Journal statuses
     *
     * @param string $selected
     * @param bool   $any      include Any option
     *
     * @return array[string 'value', string 'caption', boolean 'selected']
     */
    private function __getSelectionJournalStatuses(string $selected, bool $any = false): array
    {
        $selected = (string) $selected;

        $output = [];

        if ($any) {
            $output[] = [
                'value'    => '',
                'caption'  => 'Any',
                'selected' => ('' === $selected),
            ];
        }

        $output[] = [
            'value'    => (string) Journal::STATUS_NONE,
            'caption'  => 'None',
            'selected' => ((string) Journal::STATUS_NONE === $selected),
        ];

        $output[] = [
            'value'    => (string) Journal::STATUS_ACTIVE,
            'caption'  => 'Active',
            'selected' => ((string) Journal::STATUS_ACTIVE === $selected),
        ];

        $output[] = [
            'value'    => (string) Journal::STATUS_ARCHIVE,
            'caption'  => 'Archived',
            'selected' => ((string) Journal::STATUS_ARCHIVE === $selected),
        ];

        return $output;
    }

    /**
     * Method to get Contacts single-input repeater values.
     *
     * @param array $values
     *
     * @return array
     */
    private function __getSelectionContactsRepeaterFields(array $values): array
    {
        $results = [];

        if (!empty($values)) {
            $convert = array_column($values, 'input');
            $convert = array_map('trim', $convert);
            $convert = array_filter($convert);
            $convert = array_values($convert);
            $results = $convert;
        }

        return $results;
    }

    /**
     * Method to get Links two-inputs repeater values.
     * Also add caption using URL hostname if left empty.
     *
     * @param array $values
     *
     * @return array
     */
    private function __getSelectionLinksRepeaterFields(array $values): array
    {
        $results = [];

        if (!empty($values)) {
            foreach ($values as $set) {
                $set = array_map('trim', $set);
                $set = array_filter($set);

                if (!empty($set['url'])) {
                    $caption = $set['caption'] ?? '';

                    if (empty($caption)) {
                        $url = $this->__parsingAndSanitizeUrl($set['url']);
                        if (!empty($url['host'])) {
                            $caption = $url['host'];
                        }
                    }

                    if (empty($caption)) {
                        $caption = 'Unknown';
                    }

                    $results[] = [
                        'caption' => $caption,
                        'url'     => $set['url'],
                    ];
                }
            }
        }

        return $results;
    }

    /**
     * Method to filter "value" of __selection method result, to which selected.
     * Without proper/complete pair of $selected & $name; will result this to return an empty array.
     * - __selection() method should always return array-child set with same structure.
     *
     * @param array $set        __selection method result
     * @param string $selected  key that holds the "boolean" value to be filter - using array_filter()
     * @param string $name      key that holds the "value" - must be unique
     *
     * @return array
     */
    private function __filterSelectionSelected(array $set, string $selected = 'selected', string $name = 'name'): array
    {
        do {
            if (empty($set)) {
                break;
            }

            // Check 1: $name is not holding unique value / not exits
            try {
                $set = array_column($set, $selected, $name);

                $filter = (array) array_filter($set);

                $filter = array_combine(
                    range(0, (count($filter) - 1)),
                    array_keys($filter)
                );

                // manual catch if error muted
                if (false === $filter) {
                    $filter = [];
                }
            }
            catch (\Exception $e) {
                $set = [];
                break;
            }
            // @codeCoverageIgnoreStart
            catch (\Error $e) {
                $set = [];
                break;
            }
            // @codeCoverageIgnoreEnd

            // Check 2: certain of $name missing from list / unbalance
            if (count(array_filter($set)) != count(array_filter($filter))) {
                $set = [];
                break;
            }

            $set = $filter;
        } while(false);

        // manual catch if error muted
        if (false === $set) {
            $set = [];
        }

        return $set;
    }

    /**
     * Method to unpack URL for UI use.
     * Malformed URL without host, this method will return null.
     *
     * @param string|null $url
     *
     * @return array[string 'host', ?string 'path']|null
     */
    private function __parsingAndSanitizeUrl(?string $url): ?array
    {
        if (null === $url) {
            return null;
        }

        $result = [];

        $parsing = strtolower($url);
        $parsing = parse_url($parsing);

        if (
            // no schema issue
            (false !== $parsing) &&
            (!empty($parsing['path'])) &&
            (1 == count($parsing)) &&
            (false !== strpos($parsing['path'], '.'))
        ) {
            $parsing = strtolower($url);

            // enforce basic schema notation
            if (
                (false === strpos($parsing, 'http')) &&
                (false == strpos($parsing, '//'))
            ) {
                $parsing = '//' . $parsing;
            }

            $parsing = parse_url($parsing);
        }

        if (false !== $parsing) {
            if (!empty($parsing['host'])) {
                $parsing['host'] = ltrim($parsing['host'], 'www.');
                $parsing['host'] = ltrim($parsing['host'], 'w3.');

                if (!empty($parsing['host'])) {
                    $result = [
                        'host' => $parsing['host'],
                        'path' => null,
                    ];

                    if (!empty($parsing['path'])) {
                        if (basename($parsing['path'])) {
                            $parsing['path'] = basename($parsing['path']);
                        }
                        $parsing['path'] = ltrim($parsing['path'], '/');
                        if (!empty($parsing['path'])) {
                            $result['path'] = $parsing['path'];
                        }
                    }
                }

            }
        }

        return empty($result) ? null : $result;
    }
}
