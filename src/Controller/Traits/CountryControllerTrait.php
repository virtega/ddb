<?php

namespace App\Controller\Traits;

use App\Entity\Country;
use App\Entity\CountryTaxonomy;

/**
 * Controller Country Entity Helper
 *
 * @since 1.2.0
 */
trait CountryControllerTrait
{
    /**
     * Method to get list of Cities of Country.
     * The form allow new / partial City search.
     *
     * @param string[] $selected  Selected Country Names
     *
     * @return array[string 'name', boolean 'selected']
     */
    private function __getSelectionCities(array $selected): array
    {
        $output = [];

        $selected = array_map('trim', $selected);
        $selected = array_filter($selected);
        $selectedLowered = array_map('strtolower', $selected);

        /**
         * @var \App\Repository\CountryRepository<Country>
         */
        $countryRepo = $this->em->getRepository(Country::class);

        /**
         * @var string[]
         */
        $countries = $countryRepo->getDistictCapitalCities();

        foreach ($countries as $country) {
            $preselected = array_search(strtolower($country), $selectedLowered);
            if (false !== $preselected)  {
                unset($selected[$preselected]);
                $preselected = true;
            }

            $output[] = [
                'name'     => $country,
                'selected' => $preselected,
            ];
        }

        foreach ($selected as $select) {
            $output[] = [
                'name'     => $select,
                'selected' => true,
            ];
        }

        return $output;
    }

    /**
     * Method to get list of Country
     *
     * @param string[] $selected  Selected Country ISO code
     *
     * @return array[string 'isocode', string 'name', boolean 'selected']
     */
    private function __getSelectionCountries(array $selected): array
    {
        $output = [];

        /**
         * @var \App\Repository\CountryRepository<Country>
         */
        $countryRepo = $this->em->getRepository(Country::class);

        /**
         * @var Country[]
         */
        $countries = $countryRepo->getCountryList(['name' => 'ASC']);

        foreach ($countries as $country) {
            $output[] = [
                'isocode'  => $country->getIsoCode(),
                'name'     => $country->getName(),
                'selected' => (in_array($country->getIsoCode(), $selected)),
            ];
        }

        return $output;
    }

    /**
     * Method to get list of Regions
     *
     * @param int[] $selected  Selected Region ID
     *
     * @return array[string 'id', string 'name', string 'caption', boolean 'top', boolean 'selected']
     */
    private function __getSelectionRegions(array $selected): array
    {
        $output = [];

        /**
         * @var \App\Repository\CountryTaxonomyRepository<CountryTaxonomy>
         */
        $countryRepo = $this->em->getRepository(CountryTaxonomy::class);

        /**
         * @var array<\App\Entity\CountryTaxonomy[]>
         */
        $allRegions = $countryRepo->getAllLevelRegions();

        foreach ($allRegions as $ri => $regions) {
            foreach ($regions as $lvl => $region) {
                $lvl = (int) $lvl;

                $caption = '';
                if ($lvl) {
                    $caption = array_fill(0, $lvl, '>');
                    $caption = implode('', $caption) . ' ';
                }
                $caption .= $region->getName();

                $output[ $region->getId() ] = [
                    'id'       => $region->getId(),
                    'name'     => $region->getName(),
                    'caption'  => $caption,
                    'top'      => (0 == $lvl),
                    'selected' => (in_array($region->getId(), $selected)),
                ];
            }
        }

        return $output;
    }
}
