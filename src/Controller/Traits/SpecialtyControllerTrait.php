<?php

namespace App\Controller\Traits;

use App\Entity\SpecialtyTaxonomy;

/**
 * Controller Specialty Taxonomy Entity Helper
 *
 * @since 1.2.0
 */
trait SpecialtyControllerTrait
{
    /**
     * Method to get list of Specialty
     * The form allow new / partial Specialty search.
     *
     * @param array $selected  Selected Specialty ID / "tagged value"
     *
     * @return array[int|string 'id', string 'name', boolean 'selected']
     */
    private function __getSelectionSpecialties(array $selected): array
    {
        $output = [];

        /**
         * @var \Doctrine\ORM\EntityRepository<SpecialtyTaxonomy>
         */
        $specialtyTaxRepo = $this->em->getRepository(SpecialtyTaxonomy::class);

        /**
         * @var SpecialtyTaxonomy[]
         */
        $specialties = $specialtyTaxRepo->findBy([], ['specialty' => 'ASC']);

        foreach ($specialties as $specialty) {
            $preselected = array_search($specialty->getId(), $selected);
            if (false !== $preselected) {
                unset($selected[$preselected]);
                $preselected = true;
            }
            $output[] = [
                'id'       => $specialty->getId(),
                'name'     => $specialty->getSpecialty(),
                'selected' => $preselected,
            ];
        }

        foreach ($selected as $select) {
            if (!in_array($select, array_column($output, 'name'), true)) {
                $output[] = [
                    'id'       => $select,
                    'name'     => $select,
                    'selected' => true,
                ];
            }
        }

        return $output;
    }
}
