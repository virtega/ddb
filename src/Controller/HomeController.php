<?php

namespace App\Controller;

use App\Exception\MaintenanceException;
use App\Service\LoginCookieService;
use App\Utility\Traits\FlashBagTrait;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * UI /
 * UI /login
 * UI /logout.
 *
 * Extending BaseController which extends Controller.
 *
 * Class handing routes for login/login.
 *
 * @since 1.0.0
 */
class HomeController extends BaseController
{
    /**
     * @Route("/", name="root")
     * @Route("/login", name="login")
     *
     * Render Login Page.
     *
     * @param  Request             $request
     * @param  LoginCookieService  $cookie
     * @param  RouterInterface     $router
     *
     * @return Response
     */
    public function loginAction(Request $request, LoginCookieService $cookie, RouterInterface $router): Response
    {
        if (!empty($this->getUser())) {
            $url = $router->generate('dashboard');
            return new RedirectResponse($url);
        }

        $redirect = $request->get('redirect');
        if (empty($redirect)) {
            $redirect = $router->generate('dashboard');
        }

        $tokens = array(
            'head'        => array('title' => 'Login'),
            'target_path' => $redirect,
        );

        $alerts = [
            'msg'     => 'info',
            'error'   => 'danger',
            'danger'  => 'danger',
            'warning' => 'warning',
            'success' => 'success',
            'info'    => 'info'
        ];
        foreach($alerts as $type => $alert) {
            $msg = $request->get($type);
            if (!empty($msg)) {
                FlashBagTrait::setOffFlashMsg($request->getSession(), $alert, $msg);
            }
        }

        $cookieName  = null;
        $cookieEmail = null;
        if ($cookie->doCookieHasLogin($request)) {
            $cookieName  = $cookie->getCookieUsername();
            $cookieEmail = $cookie->getCookieEmail();
        }

        if (
            ($cookieEmail && $cookieName) &&
            (null == $request->get('new'))
        ) {
            $tokens['head'] = ['title' => "Welcome back {$cookieName}!"];
            $tokens['cookie'] = [
                'email' => $cookieEmail,
                'name'  => $cookieName,
            ];
            return $this->renderResponse('pages/lock.html.twig', $tokens);
        }

        return $this->renderResponse('pages/login.html.twig', $tokens);
    }

    /**
     * @Route("/maintenance", name="maintenance")
     *
     * Render Maintenance / Offline Page.
     *
     * @access In Maintenance Mode Only; see ./maintenance.lock.dist.
     * @access On this mode, only Super Admin is allowed to login.
     *
     * @param  Request      $request
     *
     * @return Response
     */
    public function maintenance(Request $request): Response
    {
        $response = new Response();
        $response->setStatusCode(Response::HTTP_SERVICE_UNAVAILABLE);
        $response->headers->set('Retry-After', MaintenanceException::RETRY_AFTER);

        $tokens = array(
            'head' => array('title' => 'Closed for Maintenance'),
        );

        return $this->renderResponse('pages/maintenance.html.twig', $tokens, $response);
    }
}
