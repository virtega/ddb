<?php

namespace App\Controller;

use App\Service\SearchService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UI /drug/search
 *
 * Extending BaseController which extends Controller.
 *
 * Class handing Extra Pages for Drug Search
 *
 * @since 1.0.0
 */
class DrugSearchController extends BaseController
{
    /**
     * @Route("/drug/search", name="drug_search")
     * @IsGranted("ROLE_USER", message="Drug Search only available for User.")
     *
     * Render Search Page.
     *
     * @param Request        $request
     * @param SearchService  $ss
     *
     * @return Response
     */
    public function search(Request $request, SearchService $ss): Response
    {
        /**
         * @var \Symfony\Component\Routing\Generator\UrlGenerator
         */
        $router = $this->get('router');

        // POST
        $rq = $request->request;
        if (empty($rq->keys())) {
            // GET
            $rq = $request->query;
        }

        // prep
        $query = $rq->get('q');
        $query = trim($query);

        // indexing buttons
        $indexing = array();
        $set = range('A', 'Z');
        $set = (array) array_combine($set, $set);
        $set = array_merge($set, array(
            ':numeric' => '0-9',
            ':symbol'  => 'Others',
        ));
        foreach ($set as $char => $charcap) {
            $keyword    = "^{$char}";
            $indexing[] = array(
                'caption'  => $charcap,
                'link'     => $router->generate('drug_search', ['q' => $keyword, 'has-code' => 'true']),
                'selected' => ($query == $keyword),
            );
        }

        $results = array(
            'search-results' => array(),
            'overall-count'  => 0,
        );

        $type      = $rq->get('type', 'any');
        $hasCode   = (null === $rq->get('has-code') ? false : filter_var($rq->get('has-code'), FILTER_VALIDATE_BOOLEAN)); // defaulting to true
        $uidSearch = filter_var($rq->get('uid-search'), FILTER_VALIDATE_BOOLEAN);

        if (!empty($rq->keys())) {
            // search result
            $results = $ss
                ->setTerm($query)
                ->setType($type)
                ->setHasCodeOnly($hasCode)
                ->setUnidOnly($uidSearch)
                ->execute();
        }

        // basic tokens
        $tokens = array(
            'head' => [
                'title'    => 'Drugs Advance Search',
                'entity'   => 'Drugs',
                'action'   => 'Search',
                'subtitle' => 'Advance Search',
            ],
            // search use
            'indexing'   => $indexing,
            'q'          => $query,
            'type'       => $type,
            'has_code'   => $hasCode,
            'uid_search' => $uidSearch,
            // table use
            'searched' => (!empty($rq->keys())),
            'found'    => $results['search-results'],
            'count'    => count($results['search-results']),
            'overall'  => $results['overall-count'],
            // data on template use: see drugTemplateTokenFinalize()
            'template' => array(
                'countries' => false,
            ),
        );

        return $this->renderResponse('pages/drugs/search.html.twig', $tokens);
    }
}
