<?php


namespace App\Controller;

use App\Service\ESDGService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FeedbuilderViewerController extends BaseController
{
    /**
     * @Route("/feedbuilder", name="fb_article_list")
     * @IsGranted("ROLE_USER", message="Feedbuilder articles viewer.")
     *
     * Render FB Article List page.
     *
     * @param   Request     $request
     * @param   ESDGService $dgService
     *
     * @return Response
     */
    public function viewFBArticleList(Request $request, ESDGService $dgService): Response
    {
        $data = $dgService->getFBArticles(date('Y-m-d'));

        $tokens = array(
            'head' => array(
                'title'    => 'Feedbuilder',
                'entity'   => 'Article List',
                'action'   => 'Article List',
                'subtitle' => '',
            ),
            'body' => array(
                'ESdata' => $data,
            ),
        );

        return $this->renderResponse('pages/feedbuilder/article-list.html.twig', $tokens);
    }

    /**
     * @Route("/feedbuilder/article/{id}", name="fb_article", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_USER", message="Feedbuilder articles viewer.")
     *
     * Render FB Article page.
     *
     * @param   Request     $request
     * @param   ESDGService $dgService
     * @param   int         $id
     *
     * @return Response
     */
    public function viewFBArticle(Request $request, ESDGService $dgService, int $id): Response
    {
        $article = $dgService->getFBArticleById($id);
        if (empty($article)) {
            $this->addFlash('danger', "Requested Article #{$id} is not available.");
            return $this->redirectToRoute('fb_article_list');
        }

        $tokens = array(
            'head' => array(
                'title'    => 'View Article #' . $id,
                'entity'   => 'FB Articles',
                'action'   => 'View',
                'subtitle' => 'Viewing Article #' . $id,
            ),
            'article' => $article,
        );

        return $this->renderResponse('pages/feedbuilder/view.html.twig', $tokens);
    }
}