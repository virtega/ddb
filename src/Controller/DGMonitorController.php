<?php

namespace App\Controller;

use App\Repository\DGMonitorRepository;
use App\Service\ESDGService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * UI /dgmonitor.
 *
 * Extending BaseController which extends Controller.
 * DG Monitor.
 *
 */
class DGMonitorController extends BaseController
{
    /**
     * @Route("/dgmonitor/papers-only", name="dgmonitor_papers_only")
     * @IsGranted("ROLE_USER", message="Monitoring DG Abstract papers only.")
     *
     * Render DG Monitor Papers Only page.
     *
     * @param   Request     $request
     * @param   ESDGService $dgService
     *
     * @return Response
     */
    public function viewDGMonitorPapersOnly(Request $request, ESDGService $dgService): Response
    {
        $data = $dgService->getPapersOnly();

        $tokens = array(
            'head' => array(
                'title'    => 'DG Content',
                'entity'   => 'DG Content Monitor',
                'action'   => 'DG Papers Only',
                'subtitle' => 'DG Papers Only',
            ),
            'body' => array(
                'ESdata' => $data,
            ),
        );

        return $this->renderResponse('pages/dgmonitor/papers.html.twig', $tokens);
    }

    /**
     * @Route("/dgmonitor/time-stamp", name="dgmonitor_time_stamp")
     * @IsGranted("ROLE_USER", message="Monitoring DG Monitor Time Stamp articles")
     *
     * Render DG Monitor Time Stamp page.
     *
     * @param   Request     $request
     * @param   ESDGService $dgService
     *
     * @return Response
     */
    public function viewTimeStamp(Request $request, ESDGService $dgService): Response
    {
        $data = $dgService->getTimeStamp();

        $tokens = array(
            'head' => array(
                'title'    => 'DG Content',
                'entity'   => 'DG Content Monitor',
                'action'   => 'Time Stamp',
                'subtitle' => 'Time Stamp',
            ),
            'body' => array(
                'ESdata' => $data,
            ),
        );

        return $this->renderResponse('pages/dgmonitor/timestamp.html.twig', $tokens);
    }

    /**
     * @Route("/dgmonitor/abstracts-read-only", name="dgmonitor_abstracts_read_only")
     * @IsGranted("ROLE_USER", message="Monitoring DG Monitor Abstracts Read Only articles")
     *
     * Render DG Monitor Abstracts Read Only page.
     *
     * @param Request $request
     * @param DGMonitorRepository $dgRepository
     * @param ESDGService $dgService
     *
     * @return Response
     */
    public function viewAbstractsReadOnly(Request $request, DGMonitorRepository $dgRepository, ESDGService $dgService): Response
    {
        $stats = $dgRepository->getDGMonitorStats();
        $stats = $dgService->parseDGMonitorStats($stats);

        $tokens = array(
            'head' => array(
                'title'    => 'DG Content',
                'entity'   => 'DG Content Monitor',
                'action'   => 'Abstracts Read Only',
                'subtitle' => 'Abstracts Read Only',
            ),
            'body' => array(
                'stats' => $stats,
            ),
        );

        return $this->renderResponse('pages/dgmonitor/abstracts.html.twig', $tokens);
    }
}
