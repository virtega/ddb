<?php

namespace App\Controller;

use App\Entity\Country;
use App\Service\CoreService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Base class for UI Controller.
 * Prepare basic needs to other extending controllers.
 *
 * @since 1.0.0
 */
abstract class BaseController extends AbstractController
{
    /**
     * @var CoreService
     */
    protected $cs;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Method construction, prepare several tools for Extending Controllers for UI Page.
     *
     * @param CoreService             $cs
     * @param EntityManagerInterface  $em
     * @param LoggerInterface         $logger
     */
    public function __construct(CoreService $cs, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->cs     = $cs;
        $this->em     = $em;
        $this->logger = $logger;
    }

    /**
     * Renders a view.
     *
     * @access DEV append request "?debug=tokens" - print dump for all TWIG tokens
     *
     * @param  string        $view
     * @param  array         $parameters
     * @param  Response|null $response
     *
     * @return Response
     */
    protected function renderResponse(string $view, array $parameters = array(), Response $response = null): Response
    {
        $this->variables($parameters);

        if (in_array($this->getParameter('APP_ENV'), ['dev', 'test'])) {
            // @codeCoverageIgnoreStart
            $request = $this->getCurrentRequest();
            if (
                (!empty($request)) &&
                ('tokens' == $request->get('debug'))
            ) {
                $func = (function_exists('dump') ? 'dump' : 'var_export');
                $func($parameters);
                if (method_exists($this, 'getUser')) {
                    $func($this->getUser());
                }
                $func($_SERVER);
                $func($_ENV);
                die();
            }
            // @codeCoverageIgnoreEnd
        }

        return $this->render($view, $parameters, $response);
    }

    /**
     * Method to merge view parameters with defaulting structure & values.
     *
     * @param array  $parameters
     */
    private function variables(array &$parameters = array()): void
    {
        /**
         * @var \Symfony\Component\Routing\Generator\UrlGenerator
         */
        $router = $this->get('router');

        $defaults = array(
            'head' => array(
                'title'   => 'Welcome!',
                'version' => CoreService::VERSION,
                'year'    => date('Y'),
                'sym'     => Kernel::VERSION,
            ),
            'template' => array(
                'body_classes' => array(),
                // JS ref: countries list
                'countries' => array(),
                // JS ref: drug - generics reference selection
                'generic_fullcode' => array(),
            ),
            'user' => $this->getUser(),
        );

        // append body class from cookie
        $request = $this->getCurrentRequest();
        if (
            (!empty($request)) &&
            (!empty($request->cookies)) &&
            (!empty($theme = $request->cookies->get('drugdb-theme')))
        ) {
           $defaults['template']['body_classes'] = array_merge($defaults['template']['body_classes'], [$theme]);
        }

        // add template data
        if (
            (isset($parameters['template']['countries'])) &&
            ($parameters['template']['countries'] === true)
        ) {
            $parameters['template']['countries'] = $this->em->getRepository(Country::class)->getCountryGroupList();
        }

        if (
            (isset($parameters['template']['generic_fullcode'])) &&
            ($parameters['template']['generic_fullcode'] === true)
        ) {
            $parameters['template']['generic_fullcode'] = array(
                'level_1' => $this->cs->getGenericRefLevel(1),
                'level_2' => $this->cs->getGenericRefLevel(2),
                'level_3' => $this->cs->getGenericRefLevel(3),
                'level_4' => $this->cs->getGenericRefLevel(4),
            );
        }

        $parameters = array_merge_recursive($defaults, $parameters);
        // sanitize recursive; as $default should be just single; string & boolean only
        foreach ($defaults as $def_key => $def_set) {
            if (
                (!is_array($def_set)) ||
                (empty($def_set))
            ) {
                continue;
            }
            foreach ($def_set as $def_set_key => $def_set_val) {
                if (
                    (!is_array($def_set_val)) &&
                    (is_array($parameters[$def_key][$def_set_key])) &&
                    (count($parameters[$def_key][$def_set_key]) > 1)
                ) {
                    $parameters[$def_key][$def_set_key] = end($parameters[$def_key][$def_set_key]);
                }
            }
        }
    }

    private function getCurrentRequest(): ?Request
    {
        /**
         * @var \Symfony\Component\HttpFoundation\RequestStack
         */
        $request = $this->get('request_stack');
        /**
         * @var Request|null
         */
        $request = $request->getCurrentRequest();

        return $request;
    }
}
