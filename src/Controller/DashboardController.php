<?php

namespace App\Controller;

use App\Entity\Country;
use App\Entity\Variable;
use App\Service\ConferenceSourceService;
use App\Service\CoreService;
use App\Service\JournalSourceService;
use App\Utility\Helpers as H;
use App\Utility\LotusConnection;
use App\Utility\Traits\FlashBagTrait;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UI /dashboard.
 *
 * Extending BaseController which extends Controller.
 *
 * Dashboard show several widgets, latest counts and feature information.
 * - This query were calculation and pool under Variable.
 * - Then it will be wrapped, and push to Variable as a Cache.
 * - This cache were set with 1 hour expiration.
 * - To force reset cache use /dashboard?fresh.
 *
 * @since 1.0.0
 */
class DashboardController extends BaseController
{
    /**
     * Configuration.
     *
     * @var array
     */
    private $cfg = [
        'fresh'        => false,
        'cache'        => false,
        'cache-name'   => 'dashboard-widget-tokens-cache',
        'cache-expiry' => 3600, // seconds
    ];

    /**
     * @var LotusConnection
     */
    private $lotusConn;

    /**
     * @var JournalSourceService
     */
    private $journalSourceSrv;

    /**
     * @var ConferenceSourceService
     */
    private $conferenceSourceSrv;

    /**
     * @param CoreService              $cs
     * @param EntityManagerInterface   $em
     * @param LoggerInterface          $log
     * @param LotusConnection          $lcn
     * @param JournalSourceService     $jss
     * @param ConferenceSourceService  $css
     */
    public function __construct(CoreService $cs, EntityManagerInterface $em, LoggerInterface $log, LotusConnection $lcn, JournalSourceService $jss, ConferenceSourceService $css)
    {
        $this->lotusConn           = $lcn;
        $this->journalSourceSrv    = $jss;
        $this->conferenceSourceSrv = $css;

        parent::__construct($cs, $em, $log);
    }

    /**
     * @Route("/dashboard", name="dashboard")
     * @IsGranted("ROLE_VISITOR")
     *
     * Render Dashboard Page.
     *
     * @param  Request  $request
     *
     * @throws \App\Exception\LotusConnectionException
     *
     * @return Response
     */
    public function dashboardAction(Request $request): Response
    {
        $tokens = [];

        if ($this->isGranted('ROLE_USER')) {
            // check connection
            $this->checkRemoteConnections($request);

            // dashboard - action
            $hasCode = true;
            if (null !== $request->query->get('has-code')) {
                $hasCode = filter_var($request->query->get('has-code'), FILTER_VALIDATE_BOOLEAN);
            }

            // replacing parent tokens
            $type = ($hasCode ? 'has_code' : 'no_code');

            $this->cfg['fresh'] = $request->query->get('fresh');
            if (is_null($this->cfg['fresh'])) {
                $this->cfg['cache'] = $this->em->getRepository(Variable::class)->getVariable($this->cfg['cache-name']);
            }

            // top_level_stats > cache update
            $topLevelStatsCache = false;

            // cache / fresh
            do {
                // check cache
                if (!empty($this->cfg['cache'])) {
                    $tokens = $this->cfg['cache']->getValue();
                    if (!empty($tokens[$type]['build'])) {
                        $timed = abs(time() - $tokens[$type]['build']);
                        if ($timed < $this->cfg['cache-expiry']) {
                            // usable still
                            $topLevelStatsCache = ['info', 'Cached data; taken ' . H::convertHumanReadbleTime(time(), $tokens[$type]['build'], 1) . ' ago.'];
                            break; //do
                        }
                    }
                }

                // build fresh new
                $tokens = [
                    'has_code' => $this->buildWidgetsTokensData(true),
                    'no_code'  => $this->buildWidgetsTokensData(false),
                ];

                // store
                $this->em->getRepository(Variable::class)->updateWidgetValue($this->cfg['cache-name'], $tokens, 'array', true);
            } while (false);

            // override for display
            $tokens = $tokens[$type];
            if (false == $topLevelStatsCache) {
                $tokens['widget_value']['top_level_stats']['cache']['status'] = 'complete';
                $tokens['widget_value']['top_level_stats']['cache']['captions'][] = ['info', 'Fresh generated data.'];
            }
            else {
                $tokens['widget_value']['top_level_stats']['cache']['captions'][] = $topLevelStatsCache;
            }
        }

        // head
        $tokens['head'] = ['title' => 'Dashboard'];

        try {
            return $this->renderResponse('pages/dashboard.html.twig', $tokens);
        }
        catch (\Twig\Error\RuntimeError $e) {
            // the token on dashboard view may have changed, lets refresh them first
            if ($this->cfg['fresh']) {
                throw $e;
            }
            else {
                return $this->redirectToRoute('dashboard', ['fresh' => true]);
            }
        }
    }

    /**
     * Method to query fresh data for Dashboard parameter.
     *
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return array
     */
    private function buildWidgetsTokensData(bool $hasCode = true): array
    {
        $tokens = [
            'head'            => ['title' => 'Drugs Database'],
            'opt_has_code'    => $hasCode,
            'widget_value'    => [],
            'template'        => [],
            'build'           => time(),
        ];

        // data defaults
        $wdgts = [
            // alternate header use
            'widget-experimental-count'                      => 0,
            'widget-generic-count'                           => 0,
            'widget-brand-count'                             => 0,
            // header - side
            'widget-generic-synonym-count'                   => 0,
            'widget-generic-typo-count'                      => 0,
            'widget-brand-synonym-count'                     => 0,
            'widget-brand-typo-count'                        => 0,
            // specific
            'widget-specific-complete-count'                 => 0,
            'widget-specific-experimental-generic-count'     => 0,
            'widget-specific-generic-brand-count'            => 0,
            'widget-specific-brand-generic-count'            => 0,
            'widget-specific-experimental-count'             => 0,
            'widget-specific-generic-count'                  => 0,
            'widget-specific-brand-count'                    => 0,
            // breakdown-table - experimental
            'widget-experimental-experimental-valid-count'   => 0,
            'widget-experimental-experimental-invalid-count' => 0,
            'widget-experimental-generic-valid-count'        => 0,
            'widget-experimental-generic-invalid-count'      => 0,
            'widget-experimental-brand-valid-count'          => 0,
            'widget-experimental-brand-invalid-count'        => 0,
            // breakdown-table - generic
            'widget-generic-valid-orphan-count'              => 0,
            'widget-generic-invalid-orphan-count'            => 0,
            'widget-generic-valid-non_orphan-count'          => 0,
            'widget-generic-invalid-non_orphan-count'        => 0,
            // breakdown-table - brand
            'widget-brand-valid-orphan-count'                => 0,
            'widget-brand-invalid-orphan-count'              => 0,
            'widget-brand-valid-non_orphan-count'            => 0,
            'widget-brand-invalid-non_orphan-count'          => 0,
            // additional attributes
            'widget-generic-synonym-orphan-count'            => 0,
            'widget-generic-typo-orphan-count'               => 0,
            'widget-brand-synonym-orphan-count'              => 0,
            'widget-brand-typo-orphan-count'                 => 0,
            // features                                      : creation-date-sync
            'ext-script-import-creation-date-count'          => 0,
            'ext-script-import-creation-date-last'           => false,
            // features                                      : creation-date
            'widget-experimental-creation-date-count'        => 0,
            'widget-generic-creation-date-count'             => 0,
            'widget-brand-creation-date-count'               => 0,
            // features                                      : comments
            'widget-experimental-comments-count'             => 0,
            'widget-generic-comments-count'                  => 0,
            'widget-brand-comments-count'                    => 0,
            // journal - data
            'widget-journals-overall-count'                  => 0,
            'widget-journals-corrupted-count'                => 0,
            'widget-journals-key-active-count'               => 0,
            'widget-journals-key-archive-count'              => 0,
            'widget-journals-key-is-medline-count'           => 0,
            'widget-journals-key-is-secondline-count'        => 0,
            'widget-journals-key-is-dgabstract-count'        => 0,
            'widget-journals-key-is-notbeingupdated-count'   => 0,
            'widget-journals-key-is-deleted-count'           => 0,
            'widget-journals-key-is-globaleditions-count'    => 0,
            // journal - sync
            'journal-sync-last-date'                         => null,
            'journal-sync-last-id'                           => 0,
            'journal-sync-last-processed-count'              => 0,
            'journal-sync-last-ignored-count'                => 0,
            // conference - data
            'widget-conferences-overall-count'               => 0,
            'widget-conferences-corrupted-count'             => 0,
            'widget-conferences-key-discontinued-count'      => 0,
            'widget-conferences-key-not-discontinued-count'  => 0,
            'widget-conferences-key-online-count'            => 0,
            'widget-conferences-key-not-online-count'        => 0,
            'widget-conferences-key-key-event-count'         => 0,
            'widget-conferences-key-not-key-event-count'     => 0,
            // conference - sync
            'conference-sync-last-date'                      => null,
            'conference-sync-last-processed-count'           => 0,
            'conference-sync-last-ignored-count'             => 0,
        ];

        // no-code aliases
        $wdgtsWithNoCode = [
            'widget-generic-count'                       => 'widget-generic-no-fullcode-count',
            'widget-generic-invalid-non_orphan-count'    => 'widget-generic-invalid-non_orphan-no-fullcode-count',
            'widget-generic-invalid-orphan-count'        => 'widget-generic-invalid-orphan-no-fullcode-count',
            'widget-generic-synonym-count'               => 'widget-generic-synonym-no-fullcode-count',
            'widget-generic-synonym-orphan-count'        => 'widget-generic-synonym-orphan-no-fullcode-count',
            'widget-generic-typo-count'                  => 'widget-generic-typo-no-fullcode-count',
            'widget-generic-typo-orphan-count'           => 'widget-generic-typo-orphan-no-fullcode-count',
            'widget-generic-valid-non_orphan-count'      => 'widget-generic-valid-non_orphan-no-fullcode-count',
            'widget-generic-valid-orphan-count'          => 'widget-generic-valid-orphan-no-fullcode-count',
            'widget-specific-brand-count'                => 'widget-specific-brand-no-fullcode-count',
            'widget-specific-complete-count'             => 'widget-specific-complete-no-fullcode-count',
            'widget-specific-experimental-count'         => 'widget-specific-experimental-no-fullcode-count',
            'widget-specific-experimental-generic-count' => 'widget-specific-experimental-generic-no-fullcode-count',
            'widget-specific-generic-brand-count'        => 'widget-specific-generic-brand-no-fullcode-count',
            'widget-specific-brand-generic-count'        => 'widget-specific-brand-generic-no-fullcode-count',
            'widget-specific-generic-count'              => 'widget-specific-generic-no-fullcode-count',
        ];

        // data population
        foreach ($wdgts as $wid_name => $wid_val) {
            $varName = $wid_name;

            // if no-fullcode only, then override it
            if (!$hasCode) {
                if (isset($wdgtsWithNoCode[$wid_name])) {
                    $varName = $wdgtsWithNoCode[$wid_name];
                }
            }

            $var = $this->em->getRepository(Variable::class)->getVariable($varName);
            if (!is_null($var)) {
                $wdgts[$wid_name] = $var->getValue();
            }
        }

        // additional - headings
        $tokens['widget_value']['drugs'] = $this->fillDrugsCountsData($wdgts);

        // attribute list - experimental
        $tokens['widget_value']['specific_drugs'] = $this->fillSpecificsDrugsData($wdgts);

        // breakdown table
        $tokens['widget_value']['drug_breaks_down'] = $this->fillDrugsBreakdownsData($wdgts);

        // top-level status
        $tokens['widget_value']['top_level_stats'] = [
            'cache'              => [
                'status'   => 'warning',
                'captions' => [],
            ],
            'drug_initial'       => $this->fillDrugsInitialSyncData($wdgts),
            'drug_sync'          => $this->prepareDetailsForRemoteSync(
                $this->lotusConn->getInfo(),
                $this->lotusConn->getInfo()['upsync']
            ),

            'journal_initial'    => $this->fillJournalsInitialSyncData($wdgts),
            'journal_data'       => $this->fillJournalRecordsData($wdgts),
            'journal_sync'       => $this->prepareDetailsForRemoteSync(
                $this->journalSourceSrv->remote->getInfo(),
                $this->journalSourceSrv->isUpSync(),
                [
                    'Primary Table'   => $this->journalSourceSrv->getPrimaryTableName(false, false),
                    'Secondary Table' => $this->journalSourceSrv->getSecondaryTableName(false, false),
                    'Specialty Table' => $this->journalSourceSrv->getSpecialtiesTableName(false, false),
                ]
            ),

            'conference_initial' => $this->fillConferencesInitialSyncData($wdgts),
            'conference_data'    => $this->fillConferenceRecordsData($wdgts),
            'conference_sync'    => $this->prepareDetailsForRemoteSync(
                $this->conferenceSourceSrv->remote->getInfo(),
                $this->conferenceSourceSrv->isUpSync(),
                [
                    'Primary Table'   => $this->journalSourceSrv->getPrimaryTableName(false, false),
                    'Secondary Table' => $this->journalSourceSrv->getSecondaryTableName(false, false),
                ]
            ),
        ];

        // Cache
        if ($hasCode) {
            $tokens['widget_value']['top_level_stats']['cache']['captions'][] = ['info', 'Showing Drugs count filtered by Generic with Full-Code only.'];
        }
        else {
            $tokens['widget_value']['top_level_stats']['cache']['captions'][] = ['info', 'Showing Drugs count by absolute records number.'];
        }

        return $tokens;
    }

    /**
     * Check & Make Remote connection for checking.
     * But do not throw error, handle as soft warning.
     *
     * @todo  Simplify this once LotusConnection has been replace.
     *
     * @param  Request $request
     */
    private function checkRemoteConnections(Request $request): void
    {
        if ($this->lotusConn->isUpSync()) {
            //
            if (!$this->lotusConn->makeConnection(false)) {
                FlashBagTrait::setOffFlashMsg($request->getSession(), 'danger',
                    '<strong>Drugs Remote Connection Error:</strong><br/>' .
                    ($this->lotusConn->getInfo()['error-msg'] ?? 'Unknown Failure') .
                    '<br/>If the connection is down or not configured, please turn the Up-Sync feature OFF; ' .
                    'See <code class="fs-13 p-1 m-l-5">LOTUS_DB_UPSYNC</code>.'
                );
            }
        }

        if ($this->journalSourceSrv->isUpSync()) {
            if (!$this->journalSourceSrv->remote->makeConnection(false)) {
                FlashBagTrait::setOffFlashMsg($request->getSession(), 'danger',
                    '<strong>Journals Connection Error:</strong><br/>' .
                    ($this->journalSourceSrv->remote->getInfo()['error-msg'] ?? 'Unknown Failure') .
                    '<br/>If the connection is down or not configured, please turn the Up-Sync feature OFF; ' .
                    'See <code class="fs-13 p-1 m-l-5">JOURNAL_DB_UPSYNC</code>.'
                );
            }
        }

        if ($this->conferenceSourceSrv->isUpSync()) {
            if (!$this->conferenceSourceSrv->remote->makeConnection(false)) {
                FlashBagTrait::setOffFlashMsg($request->getSession(), 'danger',
                    '<strong>Conferences Connection Error:</strong><br/>' .
                    ($this->conferenceSourceSrv->remote->getInfo()['error-msg'] ?? 'Unknown Failure') .
                    '<br/>If the connection is down or not configured, please turn the Up-Sync feature OFF; ' .
                    'See <code class="fs-13 p-1 m-l-5">CONFERENCE_DB_UPSYNC</code>.'
                );
            }
        }
    }

    /**
     * Fill token with Drugs Counts data
     *
     * @param array $wdgts
     */
    private function fillDrugsCountsData(array $wdgts): array
    {
        $results = [];

        $results['experimental_count'] = (int) $wdgts['widget-experimental-count'];
        $results['generic_count']      = (int) $wdgts['widget-generic-count'];
        $results['brand_count']        = (int) $wdgts['widget-brand-count'];
        $results['generic_keywords']   = (int) ($wdgts['widget-generic-synonym-count'] + $wdgts['widget-generic-typo-count']);
        $results['brand_keywords']     = (int) ($wdgts['widget-brand-synonym-count'] + $wdgts['widget-brand-typo-count']);

        return $results;
    }

    /**
     * Fill token with Specific Drugs data
     *
     * @param array $wdgts
     */
    private function fillSpecificsDrugsData(array $wdgts): array
    {
        return [
            'sum' => [
                'Drug Net Count',
                H::stringNumFormat((
                    $wdgts['widget-specific-complete-count']
                    + $wdgts['widget-specific-experimental-generic-count']
                    + $wdgts['widget-specific-generic-brand-count']
                    + $wdgts['widget-specific-brand-generic-count']
                    + $wdgts['widget-specific-experimental-count']
                    + $wdgts['widget-specific-generic-count']
                    + $wdgts['widget-specific-brand-count']
                )),
            ],
            'all' => [
                'Experimental related to Brand Drug',
                'Number of relations between All THREE (3) Drug types',
                H::stringNumFormat($wdgts['widget-specific-complete-count']),
            ],
            'e_g' => [
                'Experimental related to Generic Drug',
                'Number of relations between TWO (2) Drug types',
                H::stringNumFormat($wdgts['widget-specific-experimental-generic-count']),
            ],
            'g_b' => [
                'Generic related to Brand Drug',
                'Number of relations between TWO (2) Drug types',
                H::stringNumFormat($wdgts['widget-specific-generic-brand-count']),
            ],
            'b_g' => [
                'Brand related to Generic Drug',
                'Number of relations between TWO (2) Drug types',
                H::stringNumFormat($wdgts['widget-specific-brand-generic-count']),
            ],
            'e' => [
                'Experimental ' . H::stringPluralFormat($wdgts['widget-specific-experimental-count'], 'Drug'),
                'Number of Drug which have NO relations between other Drug type',
                H::stringNumFormat($wdgts['widget-specific-experimental-count']),
            ],
            'g' => [
                'Generic ' . H::stringPluralFormat($wdgts['widget-specific-generic-count'], 'Drug'),
                'Number of Drug which have NO relations between other Drug type',
                H::stringNumFormat($wdgts['widget-specific-generic-count']),
            ],
            'b' => [
                'Brand ' . H::stringPluralFormat($wdgts['widget-specific-brand-count'], 'Drug'),
                'Number of Drug which have NO relations between other Drug type',
                H::stringNumFormat($wdgts['widget-specific-brand-count']),
            ],
        ];
    }

    /**
     * Fill token with Drugs Breakdown data
     *
     * @param array $wdgts
     */
    private function fillDrugsBreakdownsData(array $wdgts): array
    {
        return [
            [
                'head'         => 'Overall Count',
                'experimental' => H::stringNumFormat($wdgts['widget-experimental-count'], 'Drug'),
                'generic'      => H::stringNumFormat($wdgts['widget-generic-count'], 'Drug'),
                'brand'        => H::stringNumFormat($wdgts['widget-brand-count'], 'Drug'),
            ],
            [
                'head'         => 'Experimental Type',
                'experimental' => [
                    H::stringNumFormat($wdgts['widget-experimental-experimental-valid-count'], 'Valid'),
                    H::stringNumFormat($wdgts['widget-experimental-experimental-invalid-count'], 'Invalid')
                ],
                'generic'      => [
                    H::stringNumFormat($wdgts['widget-experimental-generic-valid-count'], 'Valid'),
                    H::stringNumFormat($wdgts['widget-experimental-generic-invalid-count'], 'Invalid')
                ],
                'brand'        => [
                    H::stringNumFormat($wdgts['widget-experimental-brand-valid-count'], 'Valid'),
                    H::stringNumFormat($wdgts['widget-experimental-brand-invalid-count'], 'Invalid')
                ],
            ],
            [
                'head'         => 'Have Relation Count',
                'experimental' => 'n/a',
                'generic'      => [
                    H::stringNumFormat($wdgts['widget-generic-valid-non_orphan-count'], 'Valid'),
                    H::stringNumFormat($wdgts['widget-generic-invalid-non_orphan-count'], 'Invalid')
                ],
                'brand'        => [
                    H::stringNumFormat($wdgts['widget-brand-valid-non_orphan-count'], 'Valid'),
                    H::stringNumFormat($wdgts['widget-brand-invalid-non_orphan-count'], 'Invalid')
                ],
            ],
            [
                'head'         => 'Orphan Drug Count',
                'experimental' => 'n/a',
                'generic'      => [
                    H::stringNumFormat($wdgts['widget-generic-valid-orphan-count'], 'Valid'),
                    H::stringNumFormat($wdgts['widget-generic-invalid-orphan-count'], 'Invalid')
                ],
                'brand'        => [
                    H::stringNumFormat($wdgts['widget-brand-valid-orphan-count'], 'Valid'),
                    H::stringNumFormat($wdgts['widget-brand-invalid-orphan-count'], 'Invalid')
                ],
            ],
            [
                'head'         => 'Synonym Count',
                'experimental' => 'n/a',
                'generic'      => H::stringNumFormat($wdgts['widget-generic-synonym-count'], 'Record'),
                'brand'        => H::stringNumFormat($wdgts['widget-brand-synonym-count'], 'Record'),
            ],
            [
                'head'         => 'Typo Count',
                'experimental' => 'n/a',
                'generic'      => H::stringNumFormat($wdgts['widget-generic-typo-count'], 'Record'),
                'brand'        => H::stringNumFormat($wdgts['widget-brand-typo-count'], 'Record'),
            ],
            [
                'head'         => 'Orphan Keyword Count',
                'experimental' => 'n/a',
                'generic'      => [
                    H::stringNumFormat($wdgts['widget-generic-synonym-orphan-count'], 'Synonym'),
                    H::stringNumFormat($wdgts['widget-generic-typo-orphan-count'], 'Typo')
                ],
                'brand'        => [
                    H::stringNumFormat($wdgts['widget-brand-valid-orphan-count'], 'Synonym'),
                    H::stringNumFormat($wdgts['widget-brand-invalid-orphan-count'], 'Typo')
                ],
            ],
            [
                'head'         => 'Has Creation Date',
                'experimental' => H::stringNumFormat($wdgts['widget-experimental-creation-date-count'], 'Drug'),
                'generic'      => H::stringNumFormat($wdgts['widget-generic-creation-date-count'], 'Drug'),
                'brand'        => H::stringNumFormat($wdgts['widget-brand-creation-date-count'], 'Drug'),
            ],
            [
                'head'         => 'Has Comments',
                'experimental' => H::stringNumFormat($wdgts['widget-experimental-comments-count'], 'Drug'),
                'generic'      => H::stringNumFormat($wdgts['widget-generic-comments-count'], 'Drug'),
                'brand'        => H::stringNumFormat($wdgts['widget-brand-comments-count'], 'Drug'),
            ],
        ];
    }

    /**
     * Prepare details for Remote Db sync and connection
     *
     * @param array $dbInfo
     * @param bool  $upSync
     *
     * @return array
     */
    private function prepareDetailsForRemoteSync(array $dbInfo, bool $upSync, array $tables = []): array
    {
        $result = [
            'db'       => 'none',
            'status'   => 'muted',
            'captions' => [
                ['muted', 'Up-Sync disabled']
            ],
        ];

        $result['db'] = implode(' ', [
            (!empty($dbInfo['dbname']) ? $dbInfo['dbname'] : '?'),
            '@',
            (!empty($dbInfo['host']) ? $dbInfo['host'] : '?'),
        ]);

        if (true == $dbInfo['error']) {
            if ($upSync) {
                $result['status'] = 'danger';
                $result['captions'] = [
                    ['danger', 'Unable to make connection.'],
                    ['danger', 'Up-Sync will fail.'],
                ];
            }
        }
        else {
            if ($upSync) {
                $result['status']  = 'complete';
                $result['captions'] = [
                    ['success', 'Connection OK.'],
                    ['success', 'Up-Sync enabled.'],
                ];
            }
        }

        if (!empty($tables)) {
            foreach ($tables as $head => $body) {
                $result['captions'][] = ['info', "{$head}: \"{$body}\""];
            }
        }

        return $result;
    }

    /**
     * Fill token with Drugs Initial Sync data
     *
     * @param array $wdgts
     */
    private function fillDrugsInitialSyncData(array $wdgts): array
    {
        $results = [
            'status'   => 0,
            'captions' => [],
        ];

        // Initial Sync: Migration - Countries
        $countries = $this->em->getRepository(Country::class)->getCountryGroupList();
        if (empty($countries)) {
            $results['captions'][] = ['danger', '0 Country synced.'];
        }
        else {
            $countries = array_map('count', $countries);
            $countries = array_sum($countries);
            $results['status'] += 1;
            $results['captions'][] = ['info', H::stringNumFormat($countries, 'Country', 'Countries') . ' synced.'];
        }

        // Initial Sync: External Script - Sync Creation Date
        if ($wdgts['ext-script-import-creation-date-count']) {
            $results['status'] += 1;
            if ($wdgts['ext-script-import-creation-date-last']) {
                $results['captions'][] = [
                    'info',
                    H::stringNumFormat($wdgts['ext-script-import-creation-date-count'], 'Creation date')
                        . ' synced ('
                        . H::convertHumanReadbleTime(time(), $wdgts['ext-script-import-creation-date-last']->format('U'), 1) . ' ago'
                        . ')'
                ];
            }
            else {
                $results['captions'][] = [
                    'warning',
                    H::stringNumFormat($wdgts['ext-script-import-creation-date-count'], 'Creation date')
                        . ' synced (Unknown Sync Date)'
                ];
            }
        }

        // Initial Sync: Synced - Total Comments
        $comments = [
            'Experimental' => $wdgts['widget-experimental-comments-count'],
            'Generic'      => $wdgts['widget-generic-comments-count'],
            'Brand'        => $wdgts['widget-brand-comments-count'],
        ];
        $comments_sum = array_sum($comments);
        if ($comments_sum) {
            $results['status'] += 1;
            $results['captions'][] = ['info', H::stringNumFormat($comments_sum, 'Comment') . ' synced.'];
        }
        else {
            $results['captions'][] = ['danger', '0 Comment synced.'];
        }

        // Initial Sync: Total
        if (empty($results['status'])) {
            $results['status'] = 'danger';
        }
        else {
            $results['status'] = ($results['status'] == 3 ? 'complete' : 'warning');
        }

        return $results;
    }

    /**
     * Fill token with Journals Initial Sync data
     *
     * @param array $wdgts
     */
    private function fillJournalsInitialSyncData(array $wdgts): array
    {
        $results = [
            'status'   => 'danger',
            'captions' => [],
        ];

        if (!empty($wdgts['journal-sync-last-date'])) {
            $results['status'] = 'complete';
            $results['captions'][] = [
                'info',
                'Last synced on '
                    . $wdgts['journal-sync-last-date']->format('Y-m-d H:i:s')
                    . ' ('
                    . H::convertHumanReadbleTime(time(), $wdgts['journal-sync-last-date']->format('U'), 1) . ' ago'
                    . ')'
            ];
        }
        else {
            $results['captions'][] = [
                'danger',
                'Journal data were never been synced before.'
            ];
        }

        if (!empty($wdgts['journal-sync-last-processed-count'])) {
            $results['captions'][] = [
                'info',
                H::stringNumFormat($wdgts['journal-sync-last-processed-count'], 'Processed Count')
            ];
        }

        if (!empty($wdgts['journal-sync-last-ignored-count'])) {
            $results['captions'][] = [
                'info',
                H::stringNumFormat($wdgts['journal-sync-last-ignored-count'], 'Ignored Count')
            ];
        }

        if (!empty($wdgts['journal-sync-last-id'])) {
            $results['captions'][] = [
                'info',
                'Last ID #'
                    . H::stringNumFormat($wdgts['journal-sync-last-id'])
            ];
        }

        return $results;
    }

    /**
     * Fill token with Conferences Initial Sync data
     *
     * @param array $wdgts
     */
    private function fillConferencesInitialSyncData(array $wdgts): array
    {
        $results = [
            'status'   => 'danger',
            'captions' => [],
        ];

        if (!empty($wdgts['conference-sync-last-date'])) {
            $results['status'] = 'complete';
            $results['captions'][] = [
                'info',
                'Last synced on '
                    . $wdgts['conference-sync-last-date']->format('Y-m-d H:i:s')
                    . ' ('
                    . H::convertHumanReadbleTime(time(), $wdgts['conference-sync-last-date']->format('U'), 1) . ' ago'
                    . ')'
            ];
        }
        else {
            $results['captions'][] = [
                'danger',
                'Conference data were never been synced before.'
            ];
        }

        if (!empty($wdgts['conference-sync-last-processed-count'])) {
            $results['captions'][] = [
                'info',
                H::stringNumFormat($wdgts['conference-sync-last-processed-count'], 'Processed Count')
            ];
        }

        if (!empty($wdgts['conference-sync-last-ignored-count'])) {
            $results['captions'][] = [
                'info',
                H::stringNumFormat($wdgts['conference-sync-last-ignored-count'], 'Ignored Count')
            ];
        }

        return $results;
    }

    /**
     * Method to fill Journal Records details
     *
     * @param array $wdgts
     *
     * @return array
     */
    private function fillJournalRecordsData(array $wdgts): array
    {
        $results = [
            'status'   => 'danger',
            'captions' => [],
        ];

        $captions = [
            'widget-journals-overall-count'                  => 'Overall',
            'widget-journals-corrupted-count'                => 'Corrupted',
            'widget-journals-key-active-count'               => 'Active',
            'widget-journals-key-archive-count'              => 'Archived',
            'widget-journals-key-is-medline-count'           => 'MedLine',
            'widget-journals-key-is-secondline-count'        => 'SecondLine',
            'widget-journals-key-is-dgabstract-count'        => 'DgAbstract',
            'widget-journals-key-is-notbeingupdated-count'   => 'Not Being Updated',
            'widget-journals-key-is-globaleditions-count'    => 'Global Editions',
            'widget-journals-key-is-deleted-count'           => 'Deleted',
        ];

        foreach ($captions as $key => $caption) {
            if (!empty($wdgts[$key])) {
                $results['captions'][] = [
                    'info',
                    H::stringNumFormat($wdgts[$key], "{$caption} Journal")
                ];
            }
        }

        if (empty($results['captions'])) {
            $results['captions'][] = ['danger', 'No active record found.'];
        }
        else {
            $results['status'] = 'complete';
        }

        return $results;
    }

    /**
     * Method to fill Conference Records details
     *
     * @param array $wdgts
     *
     * @return array
     */
    private function fillConferenceRecordsData(array $wdgts): array
    {
        $results = [
            'status'   => 'danger',
            'captions' => [],
        ];

        $captions = [
            'widget-conferences-overall-count'               => 'Overall',
            'widget-conferences-corrupted-count'             => 'Corrupted',
            'widget-conferences-key-not-discontinued-count'  => 'On Going',
            'widget-conferences-key-discontinued-count'      => 'Discontinued',
            'widget-conferences-key-online-count'            => 'Online',
            'widget-conferences-key-not-online-count'        => 'Not Online',
            'widget-conferences-key-key-event-count'         => 'Key',
            'widget-conferences-key-not-key-event-count'     => 'Not Key',
        ];

        foreach ($captions as $key => $caption) {
            if (!empty($wdgts[$key])) {
                $results['captions'][] = [
                    'info',
                    H::stringNumFormat($wdgts[$key], "{$caption} Conference")
                ];
            }
        }

        if (empty($results['captions'])) {
            $results['captions'][] = ['danger', 'No active record found.'];
        }
        else {
            $results['status'] = 'complete';
        }

        return $results;
    }
}
