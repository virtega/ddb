<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UI /admin/manage-roles.
 *
 * Extending BaseController which extends Controller.
 * Managing user roles.
 *
 * @since 1.0.0
 */
class AdminManageUserRolesController extends BaseController
{
    /**
     * @Route("/admin/manage-roles", name="admin_manage_roles")
     * @IsGranted("ROLE_ADMIN", message="Managing User Roles only available for Admin.")
     *
     * Render Manage Role Page.
     *
     * @param  Request      $request
     *
     * @return Response
     */
    public function viewManageUserRolesPage(Request $request): Response
    {
        /**
         * @var \App\Repository\UserRolesRepository<\App\Entity\UserRoles>
         */
        $repository = $this->em->getRepository(\App\Entity\UserRoles::class);

        $tokens = array(
            'head' => array(
                'title'    => 'Administer User Roles',
                'entity'   => 'Administration',
                'action'   => 'Managing Roles',
                'subtitle' => 'Manage User & Roles',
            ),
            'body' => array(
                'roles'    => $repository->getRoleHierarchy(),
            ),
        );

        return $this->renderResponse('pages/admin/manage-roles.html.twig', $tokens);
    }
}
