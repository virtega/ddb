<?php

namespace App\Controller;

use App\Entity\Journal;
use App\Service\JournalCoreService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UI /journal
 *
 * Extending BaseController which extends Controller.
 *
 * Class handing Pages for Journal CRUD.
 *
 * @since 1.2.0
 */
class JournalController extends BaseController
{
    use \App\Controller\Traits\BasicFormControllerTrait;
    use \App\Controller\Traits\CountryControllerTrait;
    use \App\Controller\Traits\SpecialtyControllerTrait;

    /**
     * @Route("/journal/search", name="journal_search")
     * @IsGranted("ROLE_USER", message="Journal Search only available for User.")
     *
     * Render Journal Search Page.
     *
     * @param  Request   $request
     *
     * @return Response
     */
    public function searchAction(Request $request): Response
    {
        $fields = [
            'name'               => (string) $request->get('name', ''),
            'volume'             => (string) $request->get('volume', ''),
            'status'             => (string) $request->get('status', ''),
            'issn'               => (string) $request->get('issn', ''),
            'specialty'          => (array)  $request->get('specialty', []),
            'ids'                => (string) $request->get('ids', ''),
            'ismedline'          => (string) $request->get('ismedline', ''),
            'isdgabstract'       => (string) $request->get('isdgabstract', ''),
            'issecondline'       => (string) $request->get('issecondline', ''),
            'notbeingupdated'    => (string) $request->get('notbeingupdated', ''),
            'drugbeingmonitored' => (string) $request->get('drugbeingmonitored', ''),
            'deleted'            => (string) $request->get('deleted', ''),
            'showcorrupted'      => (string) $request->get('showcorrupted', 'no'),
        ];

        $arrAYNSelection = [
            ''    => 'Any',
            'yes' => 'Yes',
            'no'  => 'No',
        ];

        $template = [
            'specialty'          => $this->__getSelectionSpecialties( $fields['specialty'] ),
            'status'             => $this->__getSelectionJournalStatuses( $fields['status'], true ),
            'ismedline'          => $this->__getRadioboxSetSettings( $fields['ismedline'],          ['' => 'Any', 'yes' => 'Linked', 'no'  => 'Not Linked']),
            'isdgabstract'       => $this->__getRadioboxSetSettings( $fields['isdgabstract'],       $arrAYNSelection),
            'issecondline'       => $this->__getRadioboxSetSettings( $fields['issecondline'],       $arrAYNSelection),
            'notbeingupdated'    => $this->__getRadioboxSetSettings( $fields['notbeingupdated'],    $arrAYNSelection),
            'drugbeingmonitored' => $this->__getRadioboxSetSettings( $fields['drugbeingmonitored'], $arrAYNSelection),
            'deleted'            => $this->__getRadioboxSetSettings( $fields['deleted'],            $arrAYNSelection),
            'showcorrupted'      => $this->__getRadioboxSetSettings( $fields['showcorrupted'],      ['' => 'Show All', 'no' => 'Hide Them']),
        ];

        $meta = [
            'searched' => (!empty($request->query->keys())),
            'count'    => 0,
            'overall'  => 0,
            'perpage'  => 500,
            'page'     => (int) $request->get('page', 1),
            'maxpage'  => 0,
        ];

        $results = [];
        if ($meta['searched']) {
            // copy and sanitize for search
            $terms = $fields;
            $terms['specialty'] = $this->__filterSelectionSelected($template['specialty'], 'selected', 'id');

            /**
             * @var \App\Repository\JournalRepository<Journal>
             */
            $journalRepo = $this->em->getRepository(Journal::class);

            /**
             * @var int
             */
            $meta['overall'] = $journalRepo->searchResultCount($terms);

            if ($meta['overall']) {
                $meta['maxpage'] = (int) ceil($meta['overall'] / $meta['perpage']);
                $meta['offset'] = (($meta['page'] - 1) * $meta['perpage']);

                /**
                 * @var Journal[]
                 */
                $results = $journalRepo->search($terms, $meta['perpage'], $meta['offset']);

                $meta['count'] = count($results);
            }
        }

        $tokens = [
            'head' => [
                'title'    => 'Journal Advance Search',
                'entity'   => 'Journals',
                'action'   => 'Search',
                'subtitle' => 'Advance Search',
            ],
            'template' => $template,
            'form'     => $fields,
            'meta'     => $meta,
            'results'  => $results,
        ];

        return $this->renderResponse('pages/journals/search.html.twig', $tokens);
    }

    /**
     * @Route("/journal/registry", name="journal_registry")
     * @IsGranted("ROLE_EDITOR", message="Journal Registry only available for Editor.")
     *
     * Render Journal Registry Page.
     *
     * @param  Request             $request
     * @param  JournalCoreService  $journalService
     *
     * @return Response
     */
    public function registryAction(Request $request, JournalCoreService $journalService): Response
    {
        list($form, $template) = $this->prepareFieldAndTempleteTokens($request, null);

        if (!empty($request->request->keys())) {
            try {
                $journal = $journalService->create($form);

                $this->addFlash('info', 'Journal has been successfully created!');
                return $this->redirectToRoute('journal_view', ['id' => $journal->getId()]);
            }
            catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());
            }
        }

        $tokens = [
            'head' => [
                'title'    => 'Journal New Registration',
                'entity'   => 'Journals',
                'action'   => 'Create',
                'subtitle' => 'Registration',
            ],
            'template' => $template,
            'form'     => $form,
        ];

        return $this->renderResponse('pages/journals/registry.html.twig', $tokens);
    }

    /**
     * @Route("/journal/modify/{id}", name="journal_modify", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_EDITOR", message="Journal Modify only available for Editor.")
     *
     * Render Journal Modify Page.
     *
     * @param  Request             $request
     * @param  JournalCoreService  $journalService
     * @param  int                 $id              Journal ID
     *
     * @return Response
     */
    public function modifyAction(Request $request, JournalCoreService $journalService, $id): Response
    {
        if (!($journal = $this->loadRequestedJournal($id)) instanceof Journal) {
            return $journal;
        }

        if (!empty($request->request->keys())) {
            list($form, $template) = $this->prepareFieldAndTempleteTokens($request, null);

            try {
                $updatedJournal = $journalService->update($journal, $form);

                $this->addFlash('info', 'Journal has been successfully updated!');
                return $this->redirectToRoute('journal_view', ['id' => $updatedJournal->getId()]);
            }
            catch (\Exception $e) {
                $this->addFlash('danger', $e->getMessage());
            }
        }

        list($form, $template) = $this->prepareFieldAndTempleteTokens($request, $journal);

        $tokens = [
            'head' => [
                'title'    => 'Edit Journal #' . $id,
                'entity'   => 'Journals',
                'action'   => 'Edit',
                'subtitle' => 'Modify Journal #' . $id,
            ],
            'template' => $template,
            'form'     => $form,
            'journal'  => $journal,
        ];

        return $this->renderResponse('pages/journals/modify.html.twig', $tokens);
    }

    /**
     * @Route("/journal/view/{id}", name="journal_view", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_USER", message="Journal Modify only available for User.")
     *
     * Render Journal View Page.
     *
     * @param  Request   $request
     * @param  int       $id        Journal ID
     *
     * @return Response
     */
    public function viewAction(Request $request, $id): Response
    {
        if (!($journal = $this->loadRequestedJournal($id)) instanceof Journal) {
            return $journal;
        }

        /**
         * @var \App\Repository\JournalRepository<Journal>
         */
        $journalRepo = $this->em->getRepository(Journal::class);

        $tokens = [
            'head' => [
                'title'    => 'View Journal #' . $id,
                'entity'   => 'Journals',
                'action'   => 'View',
                'subtitle' => 'Viewing Journal #' . $id,
            ],
            'template' => [
                'id' => $id,
            ],
            'journal' => $journal,
            'parsed_links' => [
                'details_url'    => $this->__parsingAndSanitizeUrl($journal->getDetailsUrlUrl()),
                'details_pubreg' => $this->__parsingAndSanitizeUrl($journal->getDetailsPublisherRegUrl()),
            ],
            'audience' => [
                'country' => $journalRepo->getJournalDetailsCountry($journal),
                'region'  => $journalRepo->getJournalDetailsCountryTaxonomy($journal),
            ],
        ];

        return $this->renderResponse('pages/journals/view.html.twig', $tokens);
    }

    /**
     * @Route("/journal/remove/{id}", name="journal_remove", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_EDITOR", message="Journal Removal only available for Editor.")
     *
     * Delete Journal.
     *
     * @param  Request             $request
     * @param  JournalCoreService  $journalService
     * @param  int                 $id              Journal ID
     *
     * @return Response
     */
    public function deleteAction(Request $request, JournalCoreService $journalService, $id): Response
    {
        if (!($journal = $this->loadRequestedJournal($id)) instanceof Journal) {
            return $journal;
        }

        try {
            $journalService->delete($journal);

            $this->addFlash('info', 'Journal has been successfully removed!');
            return $this->redirectToRoute('journal_search');
        }
        catch (\Exception $e) {
            $this->addFlash('danger', $e->getMessage());
        }

        return $this->redirectToRoute('journal_view', ['id' => $id]);
    }

    /**
     * Method to load Journal by ID, else redirect page to Dashboard with flash message.
     *
     * @param int $id
     *
     * @return Journal|Response
     */
    private function loadRequestedJournal(int $id)
    {
        /**
         * @var \App\Repository\JournalRepository<Journal>
         */
        $journalRepo = $this->em->getRepository(Journal::class);

        /**
         * @var Journal|null
         */
        $journal = $journalRepo->findOneBy(['id' => $id]);

        if (empty($journal)) {
            $this->addFlash('danger', "Requested Journal #{$id} is not available.");
            return $this->redirectToRoute('dashboard');
        }

        return $journal;
    }

    /**
     * Method to prepare Template token by merging the setting, Journal and posted query.
     *
     * @param Request       $request
     * @param Journal|null  $journal
     *
     * @return array[array 'fields', array 'template']
     */
    private function prepareFieldAndTempleteTokens(Request $request, ?Journal $journal): array
    {
        $form = [
            'name'                 => (string) $request->get('name',                 JournalCoreService::getFormDefaults('name')),
            'abbreviation'         => (string) $request->get('abbreviation',         JournalCoreService::getFormDefaults('abbreviation')),
            'specialty'            => (array)  $request->get('specialty',            JournalCoreService::getFormDefaults('specialty')),
            'volume'               => (string) $request->get('volume',               JournalCoreService::getFormDefaults('volume')),
            'issue'                => (string) $request->get('issue',                JournalCoreService::getFormDefaults('issue')),
            'jr1'                  => (float)  $request->get('jr1',                  JournalCoreService::getFormDefaults('jr1')),
            'jr2'                  => (float)  $request->get('jr2',                  JournalCoreService::getFormDefaults('jr2')),
            'issn'                 => (string) $request->get('issn',                 JournalCoreService::getFormDefaults('issn')),
            'ismedline'            => (string) $request->get('ismedline',            JournalCoreService::getFormDefaults('ismedline')),
            'issecondline'         => (string) $request->get('issecondline',         JournalCoreService::getFormDefaults('issecondline')),
            'isdgabstract'         => (string) $request->get('isdgabstract',         JournalCoreService::getFormDefaults('isdgabstract')),
            'notbeingupdated'      => (string) $request->get('notbeingupdated',      JournalCoreService::getFormDefaults('notbeingupdated')),
            'drugsmonitored'       => (string) $request->get('drugsmonitored',       JournalCoreService::getFormDefaults('drugsmonitored')),
            'globaleditionjournal' => (string) $request->get('globaleditionjournal', JournalCoreService::getFormDefaults('globaleditionjournal')),
            'manualcreation'       => (string) $request->get('manualcreation',       JournalCoreService::getFormDefaults('manualcreation')),
            'deleted'              => (string) $request->get('deleted',              JournalCoreService::getFormDefaults('deleted')),
            'urlurl'               => (string) $request->get('urlurl',               JournalCoreService::getFormDefaults('urlurl')),
            'urlupdatedon'         => (string) $request->get('urlupdatedon',         JournalCoreService::getFormDefaults('urlupdatedon')),
            'urltoupdate'          => (string) $request->get('urltoupdate',          JournalCoreService::getFormDefaults('urltoupdate')),
            'urlviewedon'          => (string) $request->get('urlviewedon',          JournalCoreService::getFormDefaults('urlviewedon')),
            'urlviewedoncomment'   => (string) $request->get('urlviewedoncomment',   JournalCoreService::getFormDefaults('urlviewedoncomment')),
            'urlfrequency'         => (string) $request->get('urlfrequency',         JournalCoreService::getFormDefaults('urlfrequency')),
            'urlnote'              => (string) $request->get('urlnote',              JournalCoreService::getFormDefaults('urlnote')),
            'urlusername'          => (string) $request->get('urlusername',          JournalCoreService::getFormDefaults('urlusername')),
            'urlpassword'          => (string) $request->get('urlpassword',          JournalCoreService::getFormDefaults('urlpassword')),
            'publishername'        => (string) $request->get('publishername',        JournalCoreService::getFormDefaults('publishername')),
            'publisherregurl'      => (string) $request->get('publisherregurl',      JournalCoreService::getFormDefaults('publisherregurl')),
            'audiencelanguages'    => (array)  $request->get('audiencelanguages',    JournalCoreService::getFormDefaults('audiencelanguages')),
            'audiencecountry'      => (string) $request->get('audiencecountry',      JournalCoreService::getFormDefaults('audiencecountry')),
            'audienceregion'       => (int)    $request->get('audienceregion',       JournalCoreService::getFormDefaults('audienceregion')),
            'edition'              => (array)  $request->get('edition',              JournalCoreService::getFormDefaults('edition')),
            'copyright'            => (string) $request->get('copyright',            JournalCoreService::getFormDefaults('copyright')),
            'absabstract'          => (string) $request->get('absabstract',          JournalCoreService::getFormDefaults('absabstract')),
            'absregforabstract'    => (string) $request->get('absregforabstract',    JournalCoreService::getFormDefaults('absregforabstract')),
            'absftavailable'       => (string) $request->get('absftavailable',       JournalCoreService::getFormDefaults('absftavailable')),
            'absregforfulltext'    => (string) $request->get('absregforfulltext',    JournalCoreService::getFormDefaults('absregforfulltext')),
            'absfeeforfulltext'    => (string) $request->get('absfeeforfulltext',    JournalCoreService::getFormDefaults('absfeeforfulltext')),
            'abscomments'          => (string) $request->get('abscomments',          JournalCoreService::getFormDefaults('abscomments')),
            'valstatus'            => (string) $request->get('valstatus',            JournalCoreService::getFormDefaults('valstatus')),
            'valauthor'            => (string) $request->get('valauthor',            JournalCoreService::getFormDefaults('valauthor')),
            'valdatecomposed'      => (string) $request->get('valdatecomposed',      JournalCoreService::getFormDefaults('valdatecomposed')),
            'valclearedby'         => (string) $request->get('valclearedby',         JournalCoreService::getFormDefaults('valclearedby')),
            'valclearedon'         => (string) $request->get('valclearedon',         JournalCoreService::getFormDefaults('valclearedon')),
            'valmodified'          => (string) $request->get('valmodified',          JournalCoreService::getFormDefaults('valmodified')),
            'valexported'          => (string) $request->get('valexported',          JournalCoreService::getFormDefaults('valexported')),
            'artodrhtmlcode'       => (string) $request->get('artodrhtmlcode',       JournalCoreService::getFormDefaults('artodrhtmlcode')),
            'artodrcgival'         => (string) $request->get('artodrcgival',         JournalCoreService::getFormDefaults('artodrcgival')),
            'artodrprinter'        => (string) $request->get('artodrprinter',        JournalCoreService::getFormDefaults('artodrprinter')),
        ];

        if (null !== $journal) {
            $specialties = [];
            foreach ($journal->getSpecialties() as $specialty) {
                $specialties[] = $specialty->getSpecialty()->getId();
            }

            $form = [
                'name'                 => ($journal->getName() ??           JournalCoreService::getFormDefaults('name')),
                'abbreviation'         => ($journal->getAbbreviation() ??   JournalCoreService::getFormDefaults('abbreviation')),
                'specialty'            => $specialties,
                'volume'               => ($journal->getVolume() ??         JournalCoreService::getFormDefaults('volume')),
                'issue'                => ($journal->getNumber() ??         JournalCoreService::getFormDefaults('issue')),
                'jr1'                  => ($journal->getJournalReport1() ?? JournalCoreService::getFormDefaults('jr1')),
                'jr2'                  => ($journal->getJournalReport2() ?? JournalCoreService::getFormDefaults('jr2')),
                'issn'                 => ($journal->getMedlineIssn() ??    JournalCoreService::getFormDefaults('issn')),
                'ismedline'            => ($journal->isMedLine()              ? 'yes' : 'no'),
                'issecondline'         => ($journal->isSecondLine()           ? 'yes' : 'no'),
                'isdgabstract'         => ($journal->isDgAbstract()           ? 'yes' : 'no'),
                'notbeingupdated'      => ($journal->isNotBeingUpdated()      ? 'yes' : 'no'),
                'drugsmonitored'       => ($journal->isDrugsMonitored()       ? 'yes' : 'no'),
                'globaleditionjournal' => ($journal->isGlobalEditionJournal() ? 'yes' : 'no'),
                'manualcreation'       => ($journal->isManualCreation()       ? 'yes' : 'no'),
                'deleted'              => ($journal->isDeleted()              ? 'yes' : 'no'),
                'urlurl'               => $journal->getDetailsUrlUrl(),
                'urlupdatedon'         => (null !== $journal->getDetailsUrlUpdatedOn() ? $journal->getDetailsUrlUpdatedOn()->format('Y-m-d') : JournalCoreService::getFormDefaults('urlupdatedon')),
                'urltoupdate'          => (null !== $journal->getDetailsUrlToUpdate()  ? $journal->getDetailsUrlToUpdate()->format('Y-m-d')  : JournalCoreService::getFormDefaults('urltoupdate')),
                'urlviewedon'          => (null !== $journal->getDetailsUrlViewedOn()  ? $journal->getDetailsUrlViewedOn()->format('Y-m-d')  : JournalCoreService::getFormDefaults('urlviewedon')),
                'urlviewedoncomment'   => $journal->getDetailsUrlViewedOnComment(),
                'urlfrequency'         => $journal->getDetailsUrlFrequency(),
                'urlnote'              => $journal->getDetailsUrlNote(),
                'urlusername'          => $journal->getDetailsUrlUsername(),
                'urlpassword'          => $journal->getDetailsUrlPassword(),
                'publishername'        => $journal->getDetailsPublisherName(),
                'publisherregurl'      => $journal->getDetailsPublisherRegUrl(),
                'audiencelanguages'    => (array) explode(';', $journal->getDetailsLanguage()),
                'audiencecountry'      => ($journal->getDetailsCountry() ?? JournalCoreService::getFormDefaults('audiencecountry')),
                'audienceregion'       => ($journal->getDetailsRegion()  ?? JournalCoreService::getFormDefaults('audienceregion')),
                'edition'              => (array) explode(';', $journal->getDetailsEdition()),
                'copyright'            => $journal->getDetailsCopyright(),
                'absabstract'          => ($journal->getDetailsAbstractAbstract()            ? 'yes' : 'no'),
                'absregforabstract'    => ($journal->getDetailsAbstractRegForAbstract()      ? 'yes' : 'no'),
                'absftavailable'       => ($journal->getDetailsAbstractFullTextAvailablity() ? 'yes' : 'no'),
                'absregforfulltext'    => ($journal->getDetailsAbstractRegForFullText()      ? 'yes' : 'no'),
                'absfeeforfulltext'    => ($journal->getDetailsAbstractFeeForFullText()      ? 'yes' : 'no'),
                'abscomments'          => $journal->getDetailsAbstractComments(),
                'valstatus'            => (string) $journal->getStatus(),
                'valauthor'            => $journal->getDetailsValidationAuthor(),
                'valdatecomposed'      => (null !== $journal->getDetailsValidationDateComposed() ? $journal->getDetailsValidationDateComposed()->format('Y-m-d H:i:s') : JournalCoreService::getFormDefaults('valdatecomposed')),
                'valclearedby'         => $journal->getDetailsValidationClearedBy(),
                'valclearedon'         => (null !== $journal->getDetailsValidationClearedOn()    ? $journal->getDetailsValidationClearedOn()->format('Y-m-d H:i:s')    : JournalCoreService::getFormDefaults('valclearedon')),
                'valexported'          => ($journal->isExported()                            ? 'yes' : 'no'),
                'valmodified'          => $journal->getDetailsValidationModified(),
                'artodrhtmlcode'       => $journal->getDetailsOrderingHtmlCode(),
                'artodrcgival'         => $journal->getDetailsOrderingCgiValue(),
                'artodrprinter'        => $journal->getDetailsOrderingPrinterName(),
            ];
        }

        $template = [
            'id'                   => (null !== $journal ? $journal->getId() : false),
            'audiencelanguages'    => $this->__getSelectionLanguages( $form['audiencelanguages'] ),
            'edition'              => $this->__getSelectionLanguages( $form['edition'] ),
            'audiencecountry'      => $this->__getSelectionCountries( [ $form['audiencecountry'] ] ),
            'audienceregion'       => $this->__getSelectionRegions( [ $form['audienceregion'] ] ),
            'valstatus'            => $this->__getSelectionJournalStatuses( $form['valstatus'], false ),
            'specialty'            => $this->__getSelectionSpecialties( $form['specialty'] ),
            'ismedline'            => $this->__getRadioboxSetSettings( $form['ismedline'] ),
            'issecondline'         => $this->__getRadioboxSetSettings( $form['issecondline'] ),
            'isdgabstract'         => $this->__getRadioboxSetSettings( $form['isdgabstract'] ),
            'notbeingupdated'      => $this->__getRadioboxSetSettings( $form['notbeingupdated'] ),
            'drugsmonitored'       => $this->__getRadioboxSetSettings( $form['drugsmonitored'] ),
            'globaleditionjournal' => $this->__getRadioboxSetSettings( $form['globaleditionjournal'] ),
            'manualcreation'       => $this->__getRadioboxSetSettings( $form['manualcreation'] ),
            'deleted'              => $this->__getRadioboxSetSettings( $form['deleted'] ),
            'absabstract'          => $this->__getRadioboxSetSettings( $form['absabstract'] ),
            'absregforabstract'    => $this->__getRadioboxSetSettings( $form['absregforabstract'] ),
            'absftavailable'       => $this->__getRadioboxSetSettings( $form['absftavailable'] ),
            'absregforfulltext'    => $this->__getRadioboxSetSettings( $form['absregforfulltext'] ),
            'absfeeforfulltext'    => $this->__getRadioboxSetSettings( $form['absfeeforfulltext'] ),
            'valexported'          => $this->__getRadioboxSetSettings( $form['valexported'] ),
        ];

        return [$form, $template];
    }
}
