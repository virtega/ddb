<?php

namespace App\Controller;

use App\Service\CoreService;
use App\Service\MapService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UI /drug/map
 *
 * Extending BaseController which extends Controller.
 *
 * Class handing Extra Pages for Drug Map
 *
 * @since 1.0.0
 */
class DrugMapController extends BaseController
{
    /**
     * @var MapService
     */
    private $mapService;

    /**
     * Adding MapService in Construction.
     *
     * @param CoreService               $cs
     * @param EntityManagerInterface    $em
     * @param LoggerInterface           $logger
     * @param MapService                $ms
     */
    public function __construct(CoreService $cs, EntityManagerInterface $em, LoggerInterface $logger, MapService $ms)
    {
        $this->mapService = $ms;

        parent::__construct($cs, $em, $logger);
    }

    /**
     * @Route("/drug/map/open", name="drug_open_map")
     * @IsGranted("ROLE_USER", message="Drug Map only available for User.")
     *
     * Render Drug Empty Map.
     *
     * @param  Request      $request
     *
     * @return Response
     */
    public function openMapAction(Request $request): Response
    {
        $tokens = array(
            'head'    => [
                'title'    => 'Open Drug Map',
                'entity'   => 'Drugs',
                'action'   => 'Open Map',
                'subtitle' => 'Open Drug Map',
            ],
            'map'     => array(
                'operators' => array(),
                'links'     => array(),
            ),
            'template' => array(
                'countries' => false,
            ),
        );

        return $this->renderResponse('pages/drugs/map.html.twig', $tokens);
    }

    /**
     * @Route("/drug/map/{type}/{id}", name="drug_map", requirements={"type" = "experimental|generic|brand", "id" = "\d+"})
     * @IsGranted("ROLE_USER", message="Drug Map only available for User.")
     *
     * Render Drug Map for specific Drug Family by Source.
     *
     * @param  Request   $request
     * @param  string    $type     Expects `experimental`, `generic`, `brand`
     * @param  int       $id       Drug ID
     *
     * @return Response
     */
    public function mapAction(Request $request, $type, $id): Response
    {
        // query family
        $drugmap = $this->mapService->getDrugFamilyForMap($type, $id);

        if (empty($drugmap)) {
            $this->addFlash('danger', 'There is no record of Drug #ID ' . $id . '.');

            return $this->redirectToRoute('drug_open_map');
        }

        $uc_type = ucwords($type);
        $tokens  = array(
            'head'     => [
                'title'    => "{$uc_type} Drug #{$id} Map",
                'entity'   => 'Drugs',
                'action'   => "Drug Map",
                'subtitle' => "Map for {$uc_type} Drug #{$id}",
            ],
            'type'     => $type,
            'uc_type'  => $uc_type,
            'id'       => $id,
            'map'      => $drugmap,
            'template' => array(
                'countries' => false,
            ),
        );

        return $this->renderResponse('pages/drugs/map.html.twig', $tokens);
    }
}
