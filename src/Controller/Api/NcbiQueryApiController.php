<?php

namespace App\Controller\Api;

use App\Exception\ApiIncompleteException;
use App\Service\NcbiQueryService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Provide date endpoint for NCBI eUtilities queries.
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.2.1
 */
class NcbiQueryApiController extends BaseApiController
{
    /**
     * @var NcbiQueryService
     */
    private $ncbiService;

    /**
     * Current request pagination.
     * @see  $_POST[page]
     *
     * @var int
     */
    private $queryPaginationPage = 1;

    /**
     * Maximum result per pagination.
     * @see  $_POST[maxperpage]
     *
     * @var int
     */
    private $queryPaginationMaxPerPage = 10;

    /**
     * @Route("/v1/ncbi-search-journal-by-name", name="api_ncbi_search_journal_by_name", methods={"POST"})
     * @IsGranted("ROLE_USER", message="NCBI Journal Query only available for User.")
     *
     * Endpoint to search Journal on NCBI by Title or Abbreviation.
     *
     * @param  Request      $request
     * @uses   $_POST[term]        String  Required  Search Term; possibly Title / Abbreviation
     * @uses   $_POST[page]        Int     Optional  Current Pagination Page
     * @uses   $_POST[maxperpage]  Int     Optional  Maximum Result Per Pagination Page
     *
     * @return JsonResponse   data: array
     */
    public function searchJournalByName(NcbiQueryService $ncbiService, Request $request): JsonResponse
    {
        $this->ncbiService = $ncbiService;

        $this->logger->info('API: Query NCBI by Title.', [__METHOD__]);

        return $this->searchJournal(NcbiQueryService::SEARCH_FIELD_TITLE, $request);
    }

    /**
     * @Route("/v1/ncbi-search-journal-by-issn", name="api_ncbi_search_journal_by_issn", methods={"POST"})
     * @IsGranted("ROLE_USER", message="NCBI Journal Query only available for User.")
     *
     * Endpoint to search Journal on NCBI by ISSN.
     *
     * @param  Request      $request
     * @uses   $_POST[term]        String  Required  Search Term; ISSN
     * @uses   $_POST[page]        Int     Optional  Current Pagination Page
     * @uses   $_POST[maxperpage]  Int     Optional  Maximum Result Per Pagination Page
     *
     * @return JsonResponse   data: array
     */
    public function searchJournalByIssn(NcbiQueryService $ncbiService, Request $request): JsonResponse
    {
        $this->ncbiService = $ncbiService;

        $this->logger->info('API: Query NCBI by ISSN.', [__METHOD__]);

        return $this->searchJournal(NcbiQueryService::SEARCH_FIELD_ISSN, $request);
    }

    /**
     * Method to query Journal
     *
     * @param string  $field
     * @param Request $request
     *
     * @throws ApiIncompleteException  Missing required parameter
     *
     * @return JsonResponse
     */
    private function searchJournal(string $field, Request $request): JsonResponse
    {
        $page = $request->request->get('page', $this->queryPaginationPage);
        try {
            $this->queryPaginationPage = $this->ncbiService->setCurrentPaginationPage($page);
        }
        catch (\Exception $e) {
            throw new ApiIncompleteException('page', $e->getMessage());
        }

        $maxPerPage = $request->request->get('maxperpage', $this->queryPaginationMaxPerPage);
        try {
            $this->queryPaginationMaxPerPage = $this->ncbiService->setMaximumPerPaginationPage($maxPerPage);
        }
        catch (\Exception $e) {
            throw new ApiIncompleteException('maxperpage', $e->getMessage());
        }

        $term = $request->request->get('term');
        $term = trim($term);
        if (empty($term)) {
            throw new ApiIncompleteException('term');
        }

        $results = [];

        $idsLookUpResult = $this->ncbiService->eSearch($field, $term);
        if (!empty($idsLookUpResult['idlist'])) {
            /**
             * @var array
             */
            $foundSummaries = $this->ncbiService->eSummary($idsLookUpResult['idlist']);
            foreach($foundSummaries['uids'] as $uid) {
                if (isset($foundSummaries[$uid])) {
                    $results[$uid] = NcbiQueryService::restructureJournalDetails($foundSummaries[$uid]);
                }
            }
        }

        $this->processSuccess = (!empty($results));

        $results = [
            'msg'        => '',
            'data'       => $results,
            'pagination' => [
                'page'    => (int) $this->queryPaginationPage,
                'overall' => (int) $idsLookUpResult['count'] ?? null,
                'perpage' => (int) $this->queryPaginationMaxPerPage,
                'maxpage' => (int) $idsLookUpResult['count'] ? (ceil($idsLookUpResult['count'] / $this->queryPaginationMaxPerPage)) : 0,
            ],
            'ncbi_query' => [
                'term'  => $term,
                'field' => $field,
            ],
        ];

        return $this->jsonResponse($results);
    }
}
