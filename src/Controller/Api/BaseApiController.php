<?php

namespace App\Controller\Api;

use App\Service\CoreService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Base class for API Controller.
 * Prepare basic needs to other extending controllers.
 *
 * @since 1.0.0
 */
abstract class BaseApiController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var CoreService
     */
    protected $cs;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Response Status.
     *
     * @var int Defined by Response constants
     */
    protected $responseStatus = Response::HTTP_OK;

    /**
     * Flag if response need to be formated to default structure.
     *
     * @var boolean
     */
    protected $payloadWrap = true;

    /**
     * Flag if process complete, no issue encountered.
     *
     * @var boolean
     */
    protected $processSuccess = false;

    /**
     * Method construction, prepare several tools for Extending Controllers for API Requests.
     *
     * @param CoreService            $cs
     * @param EntityManagerInterface $em
     * @param LoggerInterface        $logger
     */
    public function __construct(CoreService $cs, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->cs     = $cs;
        $this->em     = $em;
        $this->logger = $logger;
    }

    /**
     * Method to render API output.
     *
     * @param array|string   $payload
     *
     * @return JsonResponse
     */
    protected function jsonResponse($payload): JsonResponse
    {
        $payload = (array) $payload;

        if ($this->payloadWrap) {
            $this->variables($payload);
        }

        return parent::json($payload, $this->responseStatus);
    }

    /**
     * Method to trigger & Response on Error.
     *
     * @codeCoverageIgnore
     *
     * @param string|array   $errorMessage
     * @param int            $response
     *
     * @return JsonResponse
     */
    protected function errorResponse($errorMessage, int $response = Response::HTTP_BAD_REQUEST): JsonResponse
    {
        // reset
        $this->payloadWrap = true;

        $this->responseStatus = $response;

        return $this->jsonResponse($errorMessage);
    }

    /**
     * Method to render output for FILE handler.
     *
     * Can't depend on parent::file(), lacking of
     * - filename fallback
     * - heading: content-type
     * - heading: X-Sendfile-Type
     *
     * @param string    $filePath     Export file Absolute Path
     * @param string    $fileName     Download file Name Mask
     * @param string    $contentType  Download file MIME
     *
     * @return BinaryFileResponse|JsonResponse      JsonResponse if error
     */
    protected function downloadResponse($filePath, $fileName, $contentType = null): Response
    {
        $fs = new FileSystem();
        if (!$fs->exists($filePath)) {
            // @codeCoverageIgnoreStart
            return $this->errorResponse('Error reading exported file.', Response::HTTP_INTERNAL_SERVER_ERROR);
            // @codeCoverageIgnoreEnd
        }

        if (empty($contentType)) {
            // @codeCoverageIgnoreStart
            $contentType = mime_content_type($filePath);
            // @codeCoverageIgnoreEnd
        }
        $contentType = (array) $contentType;
        $contentType = array_filter($contentType);

        $response = new BinaryFileResponse($filePath);

        $response->headers->set('Content-Type', $contentType);
        $response->trustXSendfileTypeHeader();

        $fileNameFallback = iconv('UTF-8', 'ASCII//TRANSLIT', $fileName);
        if (false == $fileNameFallback) {
            // @codeCoverageIgnoreStart
            $fileNameFallback = '';
            // @codeCoverageIgnoreEnd
        }

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileName,
            $fileNameFallback
        );

        return $response;
    }

    /**
     * Method to execute Command, then JSON response from executed command
     *
     * @param string $command  Command with arguments needed
     * @param float  $timed
     *
     * @return JsonResponse
     */
    protected function runCommandAndReturnResponse(string $command, float $timed = 1.5): JsonResponse
    {
        $ps = $this->cs->triggerCommandProcess($command, $timed);

        $payload = array(
            'msg'  => 'There seems to be an Error, the Command did not execute.',
            'data' => array(),
        );

        if ('dev' == $this->getParameter('APP_ENV')) {
            $payload['data'] = array(
                'com' => $command,
                'ps'  => $ps,
            );
        }

        // This may happen either way
        if (!empty($ps)) {
            $this->processSuccess = true;
            $payload['msg']       = 'Processing...';
        }
        else {
            // this is secondary pass-fix, time given could be too early or late to check PS
            $this->processSuccess = true;
            $payload['msg']       = 'Probably in processing...';
        }

        return $this->jsonResponse($payload);
    }

    /**
     * Method to merge payload with defaulting structure & values.
     *
     * @param array     $payload
     */
    private function variables(array &$payload = array()): void
    {
        // default payload wrapper
        $default = array(
            'msg'        => 'Unknown request',
            'data'       => array(),
            'count'      => 0,
            'success'    => $this->processSuccess,
            'pagination' => [],
            'drugdb'     => array(
                'version' => CoreService::VERSION,
                'date'    => new \DateTime(),
            ),
        );

        if (!isset($payload['pagination'])) {
            unset($default['pagination']);
        }

        // payload adjustment
        if (!is_array($payload)) {
            // no data, just text
            // @codeCoverageIgnoreStart
            $payload = array(
                'msg'     => $payload,
                'success' => $this->processSuccess,
            );
            // @codeCoverageIgnoreEnd
        }
        elseif (!isset($payload['data'])) {
            // re-set on correct structure
            $payload = array(
                'data' => $payload,
            );
        }

        // merge
        $payload = array_merge($default, $payload);

        // auto variables - COUNT
        if (
            (empty($payload['count'])) &&
            (!empty($payload['data']))
        ) {
            $payload['count'] = count($payload['data']);
        }

        // auto variables - MSG
        if (
            ($payload['msg'] == $default['msg'])
        ) {
            $payload['msg'] = ($payload['count'] ? 'complete' : 'no-data');
        }
    }
}
