<?php

namespace App\Controller\Api;

use App\Entity\Country;
use App\Entity\CountryRegions;
use App\Entity\CountryTaxonomy;
use App\Exception\ApiIncompleteException;
use App\Exception\DrugCrudException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class CountryApiController extends BaseApiController
{
    /**
     * @Route("/v1/get-country-list", name="api_get_country_list", methods={"GET"})
     * @IsGranted("ROLE_USER", message="Country Query only available for User.")
     *
     * Endpoint to get Known Countries List.
     *
     * @param  Request      $request
     *
     * @return JsonResponse   data: array
     */
    public function getCountryList(Request $request): JsonResponse
    {
        $this->logger->info('API: Get Country List.', array(__METHOD__));

        /**
         * @var \App\Repository\CountryRepository<Country>
         */
        $countryRepo = $this->em->getRepository(Country::class);
        $payload = $countryRepo->getCountryList();

        $this->processSuccess = true;

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/get-country-group-list", name="api_get_country_group_list", methods={"GET"})
     * @IsGranted("ROLE_USER", message="Country Query only available for User.")
     *
     * Endpoint to get Known Countries List in Group.
     *
     * @param  Request      $request
     *
     * @return JsonResponse   data: array
     */
    public function getCountryGroupList(Request $request): JsonResponse
    {
        $this->logger->info('API: Get Country Group List.', array(__METHOD__));

        /**
         * @var \App\Repository\CountryRepository<Country>
         */
        $countryRepo = $this->em->getRepository(Country::class);
        $payload = $countryRepo->getCountryGroupList();

        $this->processSuccess = true;

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/get-countries-regions", name="api_get_countries_regions", methods={"GET"})
     * @IsGranted("ROLE_USER", message="Country Query only available for User.")
     *
     * Endpoint to get all Countries Regions
     *
     * @param  Request      $request
     *
     * @return JsonResponse   data: array
     */
    public function getCountriesRegions(Request $request): JsonResponse
    {
        $this->logger->info('API: Get Countries Regions.', array(__METHOD__));

        $countryTaxRepo = $this->em->getRepository(CountryTaxonomy::class);
        $payload = $countryTaxRepo->getAllLevelRegions(null, false);

        $this->processSuccess = (!empty($payload));

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/get-a-country-details", name="api_get_a_country_details", methods={"POST"})
     * @IsGranted("ROLE_USER", message="Country Query only available for User.")
     *
     * Endpoint to get A Country Details
     *
     * @param  Request      $request
     * @uses   $_POST[country_iso]     String Required  Country CHAR(2) iso_code
     *
     * @throws ApiIncompleteException  Missing required argument
     *
     * @return JsonResponse   data: array
     */
    public function getACountryDetails(Request $request): JsonResponse
    {
        $this->logger->info('API: Get A Country Details.', array(__METHOD__));

        $iso = $request->request->get('country_iso');
        if (empty($iso)) {
            throw new ApiIncompleteException('country_iso');
        }

        $payload = $this->prepACountryDetails($iso);

        $this->processSuccess = (!empty($payload['country']));

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/get-a-city-countries", name="api_get_a_city_countries", methods={"POST"})
     * @IsGranted("ROLE_USER", message="City-Country Query only available for User.")
     *
     * Endpoint to get Countries from City name Search
     *
     * @param  Request      $request
     * @uses   $_POST[city]           String Required  City Name
     *
     * @throws ApiIncompleteException  Missing required argument
     *
     * @return JsonResponse   data: array
     */
    public function getACityCountries(Request $request): JsonResponse
    {
        $this->logger->info('API: Get A City Countries.', array(__METHOD__));

        $city = $request->request->get('city');
        if (empty($city)) {
            throw new ApiIncompleteException('city');
        }

        /**
         * @var \App\Repository\CountryRepository<Country>
         */
        $countryRepo = $this->em->getRepository(Country::class);

        /**
         * @var \App\Entity\Country[]
         */
        $countries = $countryRepo->findBy(['capitalCity' => $city], ['name' => 'ASC']);

        $payload = [];
        foreach ($countries as $country) {
            $payload[] = [
                'iso_code'     => $country->getIsoCode(),
                'name'         => $country->getName(),
                'capital_city' => $country->getCapitalCity(),
            ];
        }

        $this->processSuccess = (!empty($payload));

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/get-a-country-regions", name="api_get_a_country_regions", methods={"POST"})
     * @IsGranted("ROLE_USER", message="Country Query only available for User.")
     *
     * Endpoint to get A Country Regions
     *
     * @param  Request      $request
     * @uses   $_POST[country_iso]     String Required  Country CHAR(2) iso_code
     *
     * @throws ApiIncompleteException  Missing required argument
     *
     * @return JsonResponse   data: array
     */
    public function getACountryRegions(Request $request): JsonResponse
    {
        $this->logger->info('API: Get A Country Regions.', array(__METHOD__));

        $iso = $request->request->get('country_iso');
        if (empty($iso)) {
            throw new ApiIncompleteException('country_iso');
        }

        $payload = $this->prepACountryDetails($iso);

        $this->processSuccess = (!empty($payload['regions']));

        return $this->jsonResponse($payload['regions']);
    }

    /**
     * @Route("/v1/get-a-region-countries", name="api_get_a_region_countries", methods={"POST"})
     * @IsGranted("ROLE_USER", message="Country Query only available for User.")
     *
     * Endpoint to get A Region Countries
     *
     * @param  Request      $request
     * @uses   $_POST[region_id]     Int Required  CountryTaxonomy ID
     *
     * @throws ApiIncompleteException  Missing required argument
     * @throws DrugCrudException       Unknown Region ID
     *
     * @return JsonResponse   data: array
     */
    public function getARegionCountries(Request $request): JsonResponse
    {
        $this->logger->info('API: Get A Region Countries.', array(__METHOD__));

        $region_id = $request->request->get('region_id');
        if (empty($region_id)) {
            throw new ApiIncompleteException('region_id');
        }

        $countryTaxRepo = $this->em->getRepository(CountryTaxonomy::class);
        /**
         * @var  \App\Entity\CountryTaxonomy|null
         */
        $region = $countryTaxRepo->findOneBy(['id' => $region_id]);
        if (empty($region)) {
            throw new DrugCrudException("Unknown Region with Code: '{$region_id}'.");
        }

        $countryRegionRepo = $this->em->getRepository(CountryRegions::class);
        /**
         * @var array<\App\Entity\CountryRegions>|null
         */
        $found = $countryRegionRepo->getRegionCountries($region);

        $payload = [];
        if (!is_null($found)) {
            foreach ($found as $fnd) {
                $payload[] = [
                    'iso_code'     => $fnd->getCountry()->getIsoCode(),
                    'name'         => $fnd->getCountry()->getName(),
                    'capital_city' => $fnd->getCountry()->getCapitalCity(),
                ];
            }
        }

        $this->processSuccess = (!empty($payload));

        return $this->jsonResponse($payload);
    }

    /**
     * Method to query detail on A country
     *
     * @param string $country_iso_code CHAR(2) ref iso_code
     *
     * @throws DrugCrudException  Unknown Country ISO Code
     *
     * @return array
     */
    private function prepACountryDetails(string $country_iso_code): array
    {
        $result = [
            'country' => '',
            'regions' => [],
        ];

        $countryRepo = $this->em->getRepository(Country::class);
        /**
         * @var \App\Entity\Country|null
         */
        $country = $countryRepo->findOneBy(['isoCode' => $country_iso_code]);
        if (empty($country)) {
            throw new DrugCrudException("Unknown Country with Code: '{$country_iso_code}'.");
        }
        $result['country'] = [
            'iso_code'     => $country->getIsoCode(),
            'name'         => $country->getName(),
            'capital_city' => $country->getCapitalCity(),
        ];

        $countryRegionRepo = $this->em->getRepository(CountryRegions::class);
        /**
         * @var array<\App\Entity\CountryRegions>|null
         */
        $found = $countryRegionRepo->getCountryRegions($country);

        /**
         * @var int[]
         */
        $ids = [];
        if (!empty($found)) {
            foreach ($found as $fnd) {
                $reg = $fnd->getRegion();
                $ids[] = $reg->getId();
                $prt = $reg->getParent();
                if (!is_null($prt)) {
                    $ids[] = $prt->getId();
                }
            }
            $ids = array_unique($ids);
        }

        $countryTaxRepo = $this->em->getRepository(CountryTaxonomy::class);
        $result['regions'] = $countryTaxRepo->getAllLevelRegions($ids, false);

        return $result;
    }
}
