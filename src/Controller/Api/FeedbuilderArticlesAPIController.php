<?php

namespace App\Controller\Api;

use App\Service\ESDGService;
use App\Exception\ApiIncompleteException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class FeedbuilderArticlesAPIController extends BaseApiController
{
    /**
     * @Route("/v1/feedbuilder-articles", name="api_feedbuilder_articles", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Search feedbuilder articles by date, only available for Editor & Admin roles")
     *
     * Search ES FB articles by date
     *
     * @param Request $request
     * @param ESDGService $service
     *
     * @return JsonResponse
     *
     * @throws ApiIncompleteException
     */
    public function searchFBArticles(Request $request, ESDGService $service): JsonResponse
    {
        $this->logger->info('API: Get FB Articles request received.', array(__METHOD__));

        $date = $request->request->get('date');
        if (empty($date)) {
            throw new ApiIncompleteException('username');
        }

        $data = $service->searchFBArticlesByDate($date);

        return $this->jsonResponse($data);
    }

    /**
     * @Route("/v1/feedbuilder-articles-x-days", name="api_feedbuilder_articles_x_days", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Search feedbuilder articles for last X days, only available for Editor & Admin roles")
     *
     * Search ES FB articles for last X days
     *
     * @param Request $request
     * @param ESDGService $service
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function searchFBArticlesLastXDays(Request $request, ESDGService $service): JsonResponse
    {
        $this->logger->info('API: Get FB Articles request received.', array(__METHOD__));

        $date = $request->request->get('date');
        if (empty($date)) {
            throw new ApiIncompleteException('username');
        }
        $data = $service->searchFBArticlesLastXDays($date);

        return $this->jsonResponse($data);
    }

}
