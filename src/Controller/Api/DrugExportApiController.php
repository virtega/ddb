<?php

namespace App\Controller\Api;

use App\Exception\ApiIncompleteException;
use App\Service\SearchService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class DrugExportApiController extends BaseApiController
{
    /**
     * @Route("/v1/export-name-search", name="api_export_name_search", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Export only available for Editor.")
     *
     * Endpoint to get Export Drug by Search Name.
     *
     * @param Request        $request
     * @param SearchService  $ss
     *
     * @uses  $_POST[q]         Required  String, search term
     * @uses  $_POST[type]      Optional  String, Expects 'experimental', 'generic', 'brand', 'any'
     * @uses  $_POST[less]      Optional  Boolean flag if need a simple-export format
     * @uses  $_POST[fullcode]  Optional  Boolean flag if require Generic with Full Code; default true
     * @uses  $_POST[uuid]      Optional  Boolean flag if lookup for UUID instead; default false
     * @uses  $_POST[email]     Optional  String, if wish to email the export
     *
     * @throws ApiIncompleteException     This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse|BinaryFileResponse    JsonResponse if error
     */
    public function nameSearch(Request $request, SearchService $ss): Response
    {
        $this->logger->info('API: Export by Name Search Request Received.', array(__METHOD__));

        $query = $request->request->get('q');
        if (empty($query)) {
            throw new ApiIncompleteException('q');
        }

        $type     = $request->request->get('type', 'any');
        $complete = (empty((int) $request->request->get('less', 0)));
        $email    = $request->request->get('email');
        $fullcode = filter_var($request->request->get('fullcode', true), FILTER_VALIDATE_BOOLEAN);
        $uuid     = filter_var($request->request->get('uuid', false), FILTER_VALIDATE_BOOLEAN);

        //  if email defined
        if (!empty($email)) {
            return $this->_searchForDrugsExportAndEmail($email, $query, $type, $complete, $fullcode, $uuid);
        }

        // query
        $result = $ss
            ->setTerm($query)
            ->setType($type)
            ->setHasCodeOnly($fullcode)
            ->setUnidOnly($uuid)
            ->setExport(true)
            ->setExportComplete($complete)
            ->execute();

        $filename = \App\Service\CoreService::BRANDING . '-search-export-'
            . ($complete ? 'complete' : 'simple')
            . '-'
            . date('Y-m-d-His')
            . '.csv';

        // download file
        return $this->downloadResponse($result['file-path'], $filename, 'text/csv');
    }

    /**
     * @Route("/v1/export-id-search", name="api_export_id_search", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Export only available for Editor.")
     *
     * Endpoint to get Export Drug by Given IDs.
     * This were use for MAP, quickly export the illustrated drugs.
     * - We did not expect much drug will be exported from this end.
     *
     * @param Request        $request
     * @param SearchService  $ss
     *
     * @uses  $_POST[experimental]  Optional* Array / String (comma separated) List if Experimental IDs
     * @uses  $_POST[generic]       Optional* Array / String (comma separated) List if Generic IDs
     * @uses  $_POST[brand]         Optional* Array / String (comma separated) List if Brand IDs
     * @uses  $_POST[type]          Optional  String, Expects 'experimental', 'generic', 'brand', 'any'
     * @uses  $_POST[less]          Optional  Boolean flag if need a simple-export format
     *
     * @throws \App\Exception\DrugCrudException   If Drug type given is empty
     *
     * @return BinaryFileResponse|JsonResponse    JsonResponse if error
     */
    public function idSearch(Request $request, SearchService $ss): Response
    {
        $this->logger->info('API: Export by IDs Request Received.', array(__METHOD__));

        // change into post-query structure
        $types = $experimental = $generic = $brand = array();
        foreach (\App\Entity\Family\DrugTypeFamily::$mainDrugs as $type) {
            $types[$type] = array();
            $$type        = $request->request->get($type);
            if (!empty($$type)) {
                if (is_string($$type)) {
                    $$type = explode(',', $$type);
                }
                $$type        = (array) $$type;
                $$type        = array_map('intval', $$type);
                $$type        = array_filter($$type);
                $$type        = array_unique($$type);
                $types[$type] = $$type;
            }
        }
        $complete = (empty((int) $request->request->get('less', 0)));

        // wrapping only
        $result = $ss
            ->setGivenList($types)
            ->setExport(true)
            ->setExportComplete($complete)
            ->setExportPrefix('map-export')
            ->execute();

        $filename = \App\Service\CoreService::BRANDING . '-map-export-'
            . ($complete ? 'complete' : 'simple')
            . '-'
            . date('Y-m-d-His')
            . '.csv';

        return $this->downloadResponse($result['file-path'], $filename, 'text/csv');
    }

    /**
     * Method to sent request to Drugs Export Command instead for Search, Export and Email.
     *
     * @param string   $email
     * @param string   $query
     * @param string   $type
     * @param boolean  $complete
     * @param boolean  $fullcode
     * @param boolean  $uuid
     *
     * @return JsonResponse
     */
    private function _searchForDrugsExportAndEmail(string $email, string $query, string $type, bool $complete, bool $fullcode, bool $uuid): JsonResponse
    {
        $email    = addslashes(trim($email));
        $complete = (int) $complete;
        $fullcode = (int) $fullcode;
        $uuid     = (int) $uuid;

        $timed = ($complete ? 1.5 : 0.8);

        $command = \App\Command\DrugsDataExportCommand::getDefaultName();
        $command = "{$command} '{$email}' '{$query}' '{$type}' '{$complete}' '{$fullcode}' '{$uuid}'";

        return $this->runCommandAndReturnResponse($command, $timed);
    }
}
