<?php

namespace App\Controller\Api;

use App\Exception\ApiIncompleteException;
use App\Service\CoreService;
use App\Service\MapService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class MapApiController extends BaseApiController
{
    /**
     * @var MapService
     */
    protected $ms;

    /**
     * Adding MapService in Construction.
     *
     * @param MapService              $ms
     * @param CoreService             $cs
     * @param EntityManagerInterface  $em
     * @param LoggerInterface         $logger
     */
    public function __construct(MapService $ms, CoreService $cs, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->ms = $ms;

        parent::__construct($cs, $em, $logger);
    }

    /**
     * @Route("/v1/get-map-drug", name="api_get_map_drug", methods={"POST"})
     * @IsGranted("ROLE_USER", message="Map view only available for User.")
     *
     * Endpoint to get Drug Family Record.
     *
     * @param Request                     $request
     * @uses  $_POST[type]                Required  Expects 'experimental', 'generic', 'brand'
     * @uses  $_POST[id]                  Required  Expects integer as Drug ID
     * @uses  $_POST[mapped_experimental] Optional  Related Drug ID count
     * @uses  $_POST[mapped_generic]      Optional  Related Drug ID count
     * @uses  $_POST[mapped_brand]        Optional  Related Drug ID count
     *
     * @throws ApiIncompleteException     This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse     data: array
     */
    public function getMapDrug(Request $request): JsonResponse
    {
        $this->logger->info('API: Get Drug Family via Map.', array('method' => __METHOD__));

        $type = $request->request->get('type');
        if (empty($type)) {
            throw new ApiIncompleteException('type');
        }

        $id = (int) $request->request->get('id');
        if (empty($id)) {
            throw new ApiIncompleteException('id');
        }

        $mapped = array();
        foreach (\App\Entity\Family\DrugTypeFamily::$mainDrugs as $tpe) {
            $mapped[$tpe] = $data[$tpe] = (int) $request->request->get("mapped_{$tpe}", 0);
        }

        $payload = array(
            'msg'  => '',
            'data' => $this->ms->getDrugFamilyForMap($type, $id, $mapped),
        );

        $this->processSuccess = (!empty($payload['data']));

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/add-new-map-drug", name="api_add_new_map_drug", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Map changes only available for Editor.")
     *
     * Endpoint to Add Drug via Map.
     *
     * @param Request                     $request
     * @uses  $_POST[type]                Required  Expects 'experimental', 'generic', 'brand'
     * @uses  $_POST[name]                Required  Expects string as Drug Name
     * @uses  $_POST[valid]               Required  Expects integer as Drug Valid Flag
     * @uses  $_POST[mapped_experimental] Optional  Related Drug ID
     * @uses  $_POST[mapped_generic]      Optional  Related Drug ID
     * @uses  $_POST[mapped_brand]        Optional  Related Drug ID
     *
     * @throws ApiIncompleteException     This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse     data: Boolean
     */
    public function addNewMapDrug(Request $request): JsonResponse
    {
        $this->logger->info('API: Add New Drug via Map.', array(__METHOD__));

        $type = $request->request->get('type');
        if (empty($type)) {
            throw new ApiIncompleteException('type');
        }

        if (empty($request->request->get('name'))) {
            throw new ApiIncompleteException('name');
        }

        if (null === $request->request->get('valid')) {
            throw new ApiIncompleteException('valid');
        }

        list($success, $msg, $id, $drug) = $this->cs->updateDrug($type, $request->request, 0);

        $mapped = array();
        foreach (\App\Entity\Family\DrugTypeFamily::$mainDrugs as $tpe) {
            $mapped[$tpe] = $data[$tpe] = (int) $request->request->get("mapped_{$tpe}", 0);
        }

        $payload = array(
            'msg'  => '',
            'data' => $this->ms->getDrugFamilyForMap(
                $request->request->get('type'),
                $id,
                $mapped
            ),
        );

        $this->processSuccess = (!empty($payload['data']));

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/map-update-drug-relations", name="api_update_drug_relations", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Map changes only available for Editor.")
     *
     * Endpoint to Update Drug Relations, from MAP JSON structure only.
     *
     * @param Request                    $request
     * @uses  $_POST[operators]          Required  JSON String, map's list of Operators
     * @uses  $_POST[links]              Required  JSON String, map's list of Links
     *
     * @throws ApiIncompleteException    This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse     data: Boolean
     */
    public function updateDrugRelations(Request $request): JsonResponse
    {
        $this->logger->info('API: Update Drug Relations via Map.', array(__METHOD__));

        $operators = $request->request->get('operators');
        $operators = (array) json_decode($operators, true);
        if (empty($operators)) {
            throw new ApiIncompleteException('operators');
        }

        $links = $request->request->get('links');
        $links = (array) json_decode($links, true);

        $changes = $this->ms->updateDrugRelations($operators, $links);

        $this->processSuccess = (!empty($changes));

        $payload = array(
            'msg'  => (!empty($changes) ? 'complete' : 'incomplete'),
            'data' => array(
                'changes-count' => $changes,
            ),
            'count' => $changes,
        );

        return $this->jsonResponse($payload);
    }
}
