<?php

namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class WidgetApiController extends BaseApiController
{
    /**
     * @Route("/v1/widgets-update", name="api_update_widgets", methods={"GET"})
     * @IsGranted("ROLE_EDITOR", message="Widget updates only available for Editor.")
     *
     * Endpoint to trigger widget recalculation via Command app:update-widgets.
     *
     * @param Request   $request
     *
     * @return JsonResponse     data: array
     */
    public function widgetsUpdate(Request $request): JsonResponse
    {
        $this->logger->info('API: Update Widget Request Received.', array(__METHOD__));

        // this is command ends very fast, so cancel checking
        $command = (string) \App\Command\WidgetUpdatesCommand::getDefaultName();
        $ps      = $this->cs->triggerCommandProcess($command, 0);

        $payload = array(
            'msg'  => 'There seems to be an Error, the Command did not execute.',
            'data' => array(),
        );

        if (!empty($ps)) {
            $this->processSuccess = true;
            $payload['msg'] = 'Processing...';
        }

        if ('dev' == $this->getParameter('APP_ENV')) {
            // @codeCoverageIgnoreStart
            $payload['data'] = array(
                'com' => $command,
                'ps'  => $ps,
            );
            // @codeCoverageIgnoreEnd
        }

        return $this->jsonResponse($payload);
    }
}
