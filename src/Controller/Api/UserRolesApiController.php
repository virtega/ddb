<?php

namespace App\Controller\Api;

use App\Utility\Helpers as H;
use App\Entity\UserRoles;
use App\Exception\ApiIncompleteException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class UserRolesApiController extends BaseApiController
{
    /**
     * @Route("/v1/user-roles-list", name="api_user_roles_list", methods={"GET"})
     * @IsGranted("ROLE_ADMIN", message="List of User Roles only available for Admin.")
     *
     * Get a list of Stored UserROles
     *
     * @param Request   $request
     *
     * @return JsonResponse     data: array
     */
    public function getUserList(Request $request): JsonResponse
    {
        $this->logger->info('API: Get User Roles List Request Received.', array(__METHOD__));

        $payload = array(
            'data' => array(),
        );

        /**
         * @var \App\Repository\UserRolesRepository<UserRoles>
         */
        $repository = $this->em->getRepository(UserRoles::class);
        $hierarchy  = $repository->getRoleHierarchy();

        /**
         * @var UserRoles[]
         */
        $found = $repository->findAll();

        foreach ($found as $user) {
            $found = $user->getRoles();
            if (!empty($found)) {
                $found = H::getKeyOfSameArraySet($hierarchy, $found, 'roles');
            }
            if (empty($found)) {
                // @codeCoverageIgnoreStart
                $found = 'unknown';
                // @codeCoverageIgnoreEnd
            }

            $payload['data'][] = array(
                'id'       => $user->getId(),
                'username' => $user->getUsername(),
                'role'     => $found,
            );
        }

        $this->processSuccess = (!empty($found));

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/set-user-role", name="api_set_user_role", methods={"POST"})
     * @IsGranted("ROLE_ADMIN", message="Setting User Role only available for Admin.")
     *
     * Update/Create User Role.
     *
     * @param Request   $request
     * @uses  $_POST[username]   Required  Expects username string
     * @uses  $_POST[role]       Required  Expects "admin" / "editor" / "user" / "visitor"
     *
     * @throws ApiIncompleteException   On missing required attributes; `username`, `role`, or invalid `role`
     *
     * @return JsonResponse     data: array
     */
    public function setUserRole(Request $request): JsonResponse
    {
        $this->logger->info('API: Set User Role Request Received.', array(__METHOD__));

        $username = $request->request->get('username');
        if (empty($username)) {
            throw new ApiIncompleteException('username');
        }

        $role = $request->request->get('role');
        if (empty($role)) {
            throw new ApiIncompleteException('role');
        }

        /**
         * @var \App\Repository\UserRolesRepository<UserRoles>
         */
        $repository = $this->em->getRepository(UserRoles::class);
        $hierarchy  = $repository->getRoleHierarchy();

        if (!isset($hierarchy[$role])) {
            throw new ApiIncompleteException('role', "Invalid Request, unknown role type '{$role}'");
        }

        $userRoles = $repository->getUserRoleEnt($username);

        $this->processSuccess = $repository->createUpdateUserRoles($userRoles, $username, $hierarchy[$role]['roles'], false);

        $payload = array(
            'msg'   => 'Process ' . ($this->processSuccess ? 'complete' : 'incomplete'),
            'data'  => array(),
        );

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/remove-user-role", name="api_remove_user_role", methods={"POST"})
     * @IsGranted("ROLE_ADMIN", message="Removing User Role only available for Admin.")
     *
     * Remove User Role Record.
     *
     * @param Request   $request
     * @uses  $_POST[username]   Required  Expects username string
     *
     * @throws ApiIncompleteException   On missing required attributes; `username`
     *
     * @return JsonResponse     data: array
     */
    public function removeUserRole(Request $request): JsonResponse
    {
        $this->logger->info('API: Remove User Role Request Received.', array(__METHOD__));

        $username = $request->request->get('username');
        if (empty($username)) {
            throw new ApiIncompleteException('username');
        }

        /**
         * @var \App\Repository\UserRolesRepository<UserRoles>
         */
        $repository = $this->em->getRepository(UserRoles::class);
        $userRoles = $repository->getUserRoleEnt($username);

        if ($this->processSuccess = (!empty($userRoles->getId()))) {
            $this->em->remove($userRoles);
            $this->em->flush();
        }

        $payload = array(
            'msg'   => 'Process ' . ($this->processSuccess ? 'complete' : 'incomplete'),
            'data'  => array(),
        );

        return $this->jsonResponse($payload);
    }
}
