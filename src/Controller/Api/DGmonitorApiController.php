<?php

namespace App\Controller\Api;

use App\Service\ESDGService;
use App\Exception\ApiIncompleteException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class DGmonitorApiController extends BaseApiController
{
    /**
     * @Route("/v1/dgmonitor-papers-only", name="api_dgmonitor_papers_only", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Search article papers only by date, only available for Editor & Admin roles")
     *
     * Search ES Papers Only articles by date
     *
     * @param Request $request
     * @param ESDGService $service
     *
     * @return JsonResponse
     *
     * @throws ApiIncompleteException
     */
    public function searchPapersOnly(Request $request, ESDGService $service): JsonResponse
    {
        $this->logger->info('API: Get Papers Only by date request received.', array(__METHOD__));

        $date = $request->request->get('date');
        if (empty($date)) {
            throw new ApiIncompleteException('username');
        }

        $data = $service->searchPapersOnlyByDate($date);

        return $this->jsonResponse($data);
    }

    /**
     * @Route("/v1/dgmonitor-time-stamp", name="api_dgmonitor_time_stamp", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Search article time stamp by date, only available for Editor & Admin roles")
     *
     * Search ES Time Stamp articles by date
     *
     * @param Request $request
     * @param ESDGService $service
     *
     * @return JsonResponse
     *
     * @throws ApiIncompleteException
     */
    public function searchTimeStamp(Request $request, ESDGService $service): JsonResponse
    {
        $this->logger->info('API: Get Time Stamp by date request received.', array(__METHOD__));

        $date = $request->request->get('date');
        if (empty($date)) {
            throw new ApiIncompleteException('username');
        }

        $data = $service->searchTimeStampByDate($date);

        return $this->jsonResponse($data);
    }

    /**
     * @Route("/v1/dgmonitor-abstracts-read-only", name="api_dgmonitor_abstracts_read_only", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Search article abstracts read only by date, only available for Editor & Admin roles")
     *
     * Search ES Abstracts Read Only articles by date
     *
     * @param Request $request
     * @param ESDGService $service
     *
     * @return JsonResponse
     *
     * @throws ApiIncompleteException
     */
    public function searchReadOnly(Request $request, ESDGService $service): JsonResponse
    {
        $this->logger->info('API: Get Read Only by date request received.', array(__METHOD__));

        $date = $request->request->get('date');
        if (empty($date)) {
            throw new ApiIncompleteException('username');
        }

        $data = $service->searchAbstractsReadOnly($date);

        return $this->jsonResponse($data);
    }
}
