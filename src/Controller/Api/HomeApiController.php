<?php

namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class HomeApiController extends BaseApiController
{
    /**
     * @Route("/v1/index", name="api_index", methods={"POST", "GET"})
     * @Route("/v1/hello", name="api_hello", methods={"POST", "GET"})
     * @IsGranted("ROLE_VISITOR")
     *
     * Endpoint to check if REST is Available.
     *
     * @param Request   $request
     *
     * @return JsonResponse     data: string
     */
    public function index(Request $request): JsonResponse
    {
        $this->logger->info('API: Say Hello.', array(__METHOD__));

        $payload = array(
            'greet' => 'Hi, DrugDb is Online!',
            'user'  => $this->getUser(),
        );

        $this->processSuccess = true;

        return $this->jsonResponse($payload);
    }
}
