<?php

namespace App\Controller\Api;

use App\Command\JournalsDataExportCommand;
use App\Exception\ApiIncompleteException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Journal API Endpoints
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.2.3
 */
class JournalApiController extends BaseApiController
{
    /**
     * @Route("/v1/journal-search-export-csv", name="api_journal_export_csv", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Journal Export only available for Editor.")
     *
     * Endpoint to get Export Journal.
     *
     * @param Request        $request
     *
     * @uses  $_POST[query]     Required  Array, key-value search terms
     * @uses  $_POST[email]     Required  String, Email the sent export
     *
     * @throws ApiIncompleteException     This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse|BinaryFileResponse    JsonResponse if error
     */
    public function export(Request $request): Response
    {
        $this->logger->info('API: Export Journal Request Received.', array(__METHOD__));

        $email = $request->request->get('email');
        if (empty($email)) {
            $this->logger->error('Missing "email" in search query.');
            throw new ApiIncompleteException('email');
        }

        $query = $request->request->get('query');
        if (
            (!is_array($query)) ||
            (empty($query))
        ) {
            $this->logger->error('Missing "query" in search query.');
            throw new ApiIncompleteException('query');
        }

        $command = [
            JournalsDataExportCommand::getDefaultName(),
            "\"{$email}\"",
        ];

        foreach (JournalsDataExportCommand::$cfg as $argument => $default) {
            $value = $query[$argument] ?? $default;

            if (is_array($value)) {
                $value = implode(',', $value);
            }

            $command[] = "\"{$value}\"";
        }
        $command = implode(' ', $command);

        return $this->runCommandAndReturnResponse($command);
    }
}
