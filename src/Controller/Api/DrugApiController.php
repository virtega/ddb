<?php

namespace App\Controller\Api;

use App\Exception\ApiIncompleteException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class DrugApiController extends BaseApiController
{
    /**
     * @Route("/v1/get-drug", name="api_get_drug", methods={"POST"})
     * @IsGranted("ROLE_USER", message="Drug Query only available for User.")
     *
     * Endpoint to get Drug Record.
     *
     * @param request          $request
     * @uses  $_POST[type]     Required  Expects 'experimental', 'generic', 'brand'
     * @uses  $_POST[id]       Required  Expects integer as Drug ID
     *
     * @throws ApiIncompleteException    This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse     data: array
     */
    public function getDrug(Request $request): JsonResponse
    {
        $this->logger->info('API: Get Drug.', array('method' => __METHOD__));

        $type = $request->request->get('type');
        if (empty($type)) {
            throw new ApiIncompleteException('type');
        }

        $id = $request->request->get('id');
        if (empty($id)) {
            throw new ApiIncompleteException('id');
        }

        $payload = array(
            'msg'   => '',
            'data'  => $this->cs->getDrug($type, $id),
            'count' => 0,
        );

        $payload['count'] = (!empty($payload['data']) ? 1 : 0);

        $this->processSuccess = (!empty($payload['count']));

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/get-drug-family", name="api_get_drug_family", methods={"POST"})
     * @IsGranted("ROLE_USER", message="Drug Query only available for User.")
     *
     * Endpoint to get Drug Family Record.
     *
     * @param request          $request
     * @uses  $_POST[type]     Required  Expects 'experimental', 'generic', 'brand'
     * @uses  $_POST[id]       Required  Expects integer as Drug ID
     *
     * @throws ApiIncompleteException    This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse     data: array
     */
    public function getDrugFamily(Request $request): JsonResponse
    {
        $this->logger->info('API: Get Drug.', array('method' => __METHOD__));

        $type = $request->request->get('type');
        if (empty($type)) {
            throw new ApiIncompleteException('type');
        }

        $id = $request->request->get('id');
        if (empty($id)) {
            throw new ApiIncompleteException('id');
        }

        $payload = array(
            'msg'   => '',
            'data'  => $this->cs->getDrugFamily($type, $id),
            'count' => 0,
        );

        $payload['count'] = (!empty($payload['data']) ? 1 : 0);

        $this->processSuccess = (!empty($payload['count']));

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/get-drug-entire-family", name="api_get_drug_entire_family", methods={"POST"})
     * @IsGranted("ROLE_USER", message="Drug Query only available for User.")
     *
     * Endpoint to get Drug Entire/Complete Family Record, by single family member.
     *
     * @param request          $request
     * @uses  $_POST[type]     Required  Expects 'experimental', 'generic', 'brand'
     * @uses  $_POST[id]       Required  Expects integer as Drug ID
     *
     * @throws ApiIncompleteException    This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse     data: array
     */
    public function getDrugEntireFamily(Request $request): JsonResponse
    {
        $this->logger->info('API: Get Drug.', array('method' => __METHOD__));

        $type = $request->request->get('type');
        if (empty($type)) {
            throw new ApiIncompleteException('type');
        }

        $id = $request->request->get('id');
        if (empty($id)) {
            throw new ApiIncompleteException('id');
        }

        $family = $this->cs->getDrugEntireFamily($type, $id);

        $count = array_map('count', $family);
        $count = array_filter($count);
        $count = count($count);

        $payload = array(
            'msg'   => '',
            'data'  => $family,
            'count' => $count,
        );

        $this->processSuccess = ($count > 0);

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/add-new-drug", name="api_add_new_drug", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Drug Creation only available for Editor.")
     *
     * Endpoint to Add Drug.
     *
     * @param Request               $request
     * @uses  $_POST[type]          Required  Expects 'experimental', 'generic', 'brand'
     * @uses  $_POST[name]          Required  Expects string as Drug Name
     * @uses  $_POST[valid]         Required  Expects integer as Drug Valid Flag
     *
     * @throws ApiIncompleteException    This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse     data: Boolean
     */
    public function addNewDrug(Request $request): JsonResponse
    {
        $this->logger->info('API: Add New Drug.', array('method' => __METHOD__));

        $type = $request->request->get('type');
        if (empty($type)) {
            throw new ApiIncompleteException('type');
        }

        if (empty($request->request->get('name'))) {
            throw new ApiIncompleteException('name');
        }

        if (null === $request->request->get('valid')) {
            throw new ApiIncompleteException('valid');
        }

        list($success, $msg, $id, $drug) = $this->cs->updateDrug($type, $request->request, 0);

        $payload = array(
            'msg'   => '',
            'data'  => $drug,
            'count' => (int) (!empty($drug)),
        );

        $this->processSuccess = $success;

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/remove-drug", name="api_delete_drug", methods={"POST"})
     * @IsGranted("ROLE_EDITOR", message="Drug Removal only available for Editor.")
     *
     * Endpoint to Delete specific Drug with it's Relations, via Main drug ID.
     *
     * @param Request          $request
     * @uses  $_POST[type]     Required  Expects 'experimental', 'generic', 'brand'
     * @uses  $_POST[id]       Required  Expects integer as Drug ID
     *
     * @throws ApiIncompleteException    This should be handle by EventListerner/ExceptionListener
     *
     * @return JsonResponse     data: Boolean
     */
    public function deleteDrug(Request $request): JsonResponse
    {
        $this->logger->info('API: Delete Drug.', array('method' => __METHOD__));

        $type = $request->request->get('type');
        if (empty($type)) {
            throw new ApiIncompleteException('type');
        }

        $id = $request->request->get('id');
        if (empty($id)) {
            throw new ApiIncompleteException('id');
        }

        list($success, $msg, $removed, $untied) = $this->cs->deleteDrug($type, $id);

        $payload = array(
            'msg'   => $msg,
            'data'  => array(),
            'count' => ($removed + $untied),
        );

        $this->processSuccess = $success;

        return $this->jsonResponse($payload);
    }
}
