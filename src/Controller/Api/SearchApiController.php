<?php

namespace App\Controller\Api;

use App\Entity\Conference;
use App\Entity\Journal;
use App\Exception\ApiIncompleteException;
use App\Service\SearchService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Search-Overlay search by name endpoints
 * Extending BaseApiController which extends Controller.
 *
 * @since 1.0.0
 */
class SearchApiController extends BaseApiController
{
    /**
     * @Route("/v1/search-by-name", name="api_search_by_name", methods={"POST", "GET"})
     * @IsGranted("ROLE_USER", message="Search Drug only available for User.")
     *
     * Endpoint to query Drug by Name.
     *
     * @param  Request          $request
     * @param  SearchService    $ss
     *
     * @uses  $_POST|$_GET[q]             Required              String, search term; support special-search symbols
     * @uses  $_POST|$_GET[type]          Optional  [any]       Expects 'experimental', 'generic', 'brand', 'any'
     * @uses  $_POST|$_GET[excludes]      Optional  [array()]   JSON String / Array; List of IDs under drug-type key
     * @uses  $_POST|$_GET[auct]          Optional  [false]     Boolean flag if output meant for AutoComplete
     * @uses  $_POST|$_GET[has-code]      Optional  [true]      Boolean flag if to limit search with FullCode only; applicable for Generic Drug only
     * @uses  $_POST|$_GET[uid-search]    Optional  [false]     Boolean flag if to use "q" to search drug UID only; special-search symbols still applicable
     *
     * @throws ApiIncompleteException     This should be handle by EventListerner/ExceptionListener
     *
     * @return jsonResponse     data: array
     */
    public function searchDrugByName(Request $request, SearchService $ss): JsonResponse
    {
        $this->logger->info('API: Search Drug by Name.', array(__METHOD__));

        // POST / GET
        $rq = $request->request;
        if (empty($rq->keys())) {
            $rq = $request->query;
        }

        // QUERY
        $query = $rq->get('q');
        $query = trim($query);
        if (empty($query)) {
            throw new ApiIncompleteException('q');
        }

        // TYPE
        $type = $rq->get('type', \App\Service\Factory\SearchServiceFactory::TYPE_ANY);
        if (empty($type)) {
            $type = \App\Service\Factory\SearchServiceFactory::TYPE_ANY;
        }

        // EXCLUDE ARRAY / JSON
        $excludes = $rq->get('excludes', array());
        if ((!empty($excludes)) && (is_string($excludes))) {
            $excludes = json_decode($excludes, true);
        }

        // FLAGS
        $autoc     = filter_var($rq->get('auct', false), FILTER_VALIDATE_BOOLEAN);
        $hasCode   = filter_var($rq->get('has-code', true), FILTER_VALIDATE_BOOLEAN);
        $uidSearch = filter_var($rq->get('uid-search', false), FILTER_VALIDATE_BOOLEAN);

        $results = $ss
            ->setTerm($query)
            ->setType($type)
            ->setExclusionList($excludes)
            ->setAutoComplete($autoc)
            ->setHasCodeOnly($hasCode)
            ->setUnidOnly($uidSearch)
            ->execute();

        $this->processSuccess = (!empty($results['overall-count']));
        $payload = array(
            'msg'          => '',
            'search_count' => $results['overall-count'],
            'data'         => $results['search-results'],
        );

        if ($autoc) {
            // disable default wrapping process, working with typeahead / handlebar plugin directly
            $this->payloadWrap = false;
            $payload = $payload['data'];
        }

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/search-journal-by-name", name="api_journal_search_by_name", methods={"POST", "GET"})
     * @IsGranted("ROLE_USER", message="Search Journal only available for User.")
     *
     * Endpoint to query Journal by Name.
     *
     * @param  Request  $request
     *
     * @uses  $_POST|$_GET[q]  Required    String, search term; support special-search symbols
     *
     * @throws ApiIncompleteException     This should be handle by EventListerner/ExceptionListener
     *
     * @return jsonResponse     data: array
     */
    public function searchJournalByName(Request $request): JsonResponse
    {
        $this->logger->info('API: Search Journal by Name.', array(__METHOD__));

        // POST / GET
        $rq = $request->request;
        if (empty($rq->keys())) {
            $rq = $request->query;
        }

        // QUERY
        $query = $rq->get('q');
        $query = trim($query);
        if (empty($query)) {
            throw new ApiIncompleteException('q');
        }

        $terms = [
            'name' => $query,
            'issn' => $query,
        ];

        $payload = [
            'overall_count' => 0,
            'data'          => [],
        ];

        /**
         * @var \App\Repository\JournalRepository<Journal>
         */
        $journalRepo = $this->em->getRepository(Journal::class);
        $journalRepo->changeSearchConditionsBinder('OR');

        /**
         * @var int
         */
        $payload['overall_count'] = $journalRepo->searchResultCount($terms);

        if ($payload['overall_count']) {
            /**
             * @var Journal[]
             */
            $found = $journalRepo->search($terms, 10, 0);

            foreach ($found as $journal) {
                $specialties = [];
                foreach ($journal->getSpecialties() as $specialty) {
                    $specialties[] = $specialty->getSpecialty()->getSpecialty();
                }

                $payload['data'][] = [
                    'id'                => $journal->getId(),
                    'name'              => $journal->getName() ?? '',
                    'abbreviation'      => $journal->getAbbreviation() ?? '',
                    'status'            => $journal->getStatusReadable(),
                    'is_medline'        => $journal->isMedLine(),
                    'is_secondline'     => $journal->isSecondLine(),
                    'is_dgabstract'     => $journal->isDgAbstract(),
                    'is_global_edition' => $journal->isGlobalEditionJournal(),
                    'medline_issn'      => $journal->getMedlineIssn() ?? '',
                    'jr1'               => $journal->getJournalReport1(),
                    'specialties'       => $specialties,
                ];
            }
        }

        return $this->jsonResponse($payload);
    }

    /**
     * @Route("/v1/search-conference-by-name", name="api_conference_search_by_name", methods={"POST", "GET"})
     * @IsGranted("ROLE_USER", message="Search Conference only available for User.")
     *
     * Endpoint to query Conference by Name.
     *
     * @param  Request  $request
     *
     * @uses  $_POST|$_GET[q]  Required    String, search term; support special-search symbols
     *
     * @throws ApiIncompleteException     This should be handle by EventListerner/ExceptionListener
     *
     * @return jsonResponse     data: array
     */
    public function searchConferenceByName(Request $request): JsonResponse
    {
        $this->logger->info('API: Search Conference by Name.', array(__METHOD__));

        // POST / GET
        $rq = $request->request;
        if (empty($rq->keys())) {
            $rq = $request->query;
        }

        // QUERY
        $query = $rq->get('q');
        $query = trim($query);
        if (empty($query)) {
            throw new ApiIncompleteException('q');
        }

        $terms = [
            'name' => $query,
        ];

        $payload = [
            'overall_count' => 0,
            'data'          => [],
        ];

        /**
         * @var \App\Repository\ConferenceRepository<Conference>
         */
        $confRepo = $this->em->getRepository(Conference::class);
        // $confRepo->changeSearchConditionsBinder('OR');

        /**
         * @var int
         */
        $payload['overall_count'] = $confRepo->searchResultCount($terms);

        if ($payload['overall_count']) {
            /**
             * @var Conference[]
             */
            $found = $confRepo->search($terms, 10, 0);

            foreach ($found as $conference) {
                $specialties = [];
                foreach ($conference->getSpecialties() as $specialty) {
                    $specialties[] = $specialty->getSpecialty()->getSpecialty();
                }

                $locality = [];
                if (!empty($conference->getCity())) {
                    $locality[] = $conference->getCity();
                }
                if (!empty($conference->getCountry())) {
                    $locality[] = $conference->getCountry()->getName();
                }
                if (!empty($conference->getRegion())) {
                    $locality[] = $conference->getRegion()->getName();
                }
                $locality = implode(', ', $locality);

                $payload['data'][] = [
                    'id'                => $conference->getId(),
                    'name'              => $conference->getName() ?? '',
                    'event_span'        => \App\Utility\Helpers::getEventReadSpanBetweenDates($conference->getStartDate(), $conference->getEndDate()),
                    'locality'          => $locality,
                    'is_cruise'         => $conference->isCruise(),
                    'is_online'         => $conference->isOnline(),
                    'is_key_conference' => $conference->isKeyEvent(),
                    'has_guide'         => $conference->isHasGuide(),
                    'is_discontinued'   => $conference->isDiscontinued(),
                    'specialties'       => $specialties,
                ];
            }
        }

        return $this->jsonResponse($payload);
    }
}
