<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UI /register
 * UI /modify
 *
 * Extending BaseController which extends Controller.
 *
 * Class handing Pages for Add / Edit Drug.
 *
 * @since 1.0.0
 */
class DrugController extends BaseController
{
    /**
     * @Route("/register/experimental", name="drug_register_experimental")
     * @IsGranted("ROLE_EDITOR", message="Drug Registry only available for Editor.")
     *
     * Render Add Experimental Page.
     *
     * @param  Request   $request
     *
     * @return Response
     */
    public function experimentalRegistryAction(Request $request): Response
    {
        if (!empty($request->request->keys())) {
            list($status, $msg, $drugid) = $this->cs->updateDrug('experimental', $request->request, false);
            $this->addFlash(($status ? 'success' : 'danger'), $msg);

            if ($status) {
                return $this->redirectToRoute('drug_edit_experimental', array('id' => $drugid));
            }
        }

        $tokens = array(
            'head' => array(
                'title'    => 'Create Experimental Drug',
                'entity'   => 'Drugs',
                'action'   => 'Create',
                'subtitle' => 'Register an Experimental Drug',
            ),
            'drug' => array(
                'id'            => 0,
                'name'          => '',
                'type'          => 'experimental',
                'valid_int'     => 0,
                'unid'          => '',
                'comments'      => '',
                'creation_date' => '',
            ),
            'template' => array(
                'type'       => 'experimental',
                'countries'  => false,
                'drug_types' => \App\Entity\Family\DrugTypeFamily::$mainDrugs,
            ),
            'new' => true,
        );

        return $this->renderResponse('pages/drugs/experimental.html.twig', $tokens);
    }

    /**
     * @Route("/register/generic", name="drug_register_generic")
     * @IsGranted("ROLE_EDITOR", message="Drug Registry only available for Editor.")
     *
     * Render Add Generic Page.
     *
     * @param  Request   $request
     *
     * @return Response
     */
    public function genericRegistryAction(Request $request): Response
    {
        if (!empty($request->request->keys())) {
            list($status, $msg, $drugid) = $this->cs->updateDrug('generic', $request->request, false);
            $this->addFlash(($status ? 'success' : 'danger'), $msg);

            if ($status) {
                return $this->redirectToRoute('drug_edit_generic', array('id' => $drugid));
            }
        }

        $tokens = array(
            'head' => array(
                'title'    => 'Create Generic Drug',
                'entity'   => 'Drugs',
                'action'   => 'Create',
                'subtitle' => 'Register a Generic Drug',
            ),
            'drug' => array(
                'id'                      => 0,
                'name'                    => '',
                'valid_int'               => 0,
                'unid'                    => '',
                'no_double_bounce_int'    => 0,
                'auto_encode_explude_int' => 0,
                'level_1'                 => '',
                'level_2'                 => '',
                'level_3'                 => '',
                'level_4'                 => '',
                'level_5'                 => '',
                'comments'                => '',
                'creation_date'           => '',
                'synonyms'                => array(),
                'typos'                   => array(),
            ),
            'template' => array(
                'type'             => 'generic',
                'countries'        => true,
                'generic_fullcode' => true,
            ),
            'new' => true,
        );

        return $this->renderResponse('pages/drugs/generic.html.twig', $tokens);
    }

    /**
     * @Route("/register/brand", name="drug_register_brand")
     * @IsGranted("ROLE_EDITOR", message="Drug Registry only available for Editor.")
     *
     * Render Add Brand Page.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function brandRegistryAction(Request $request): Response
    {
        if (!empty($request->request->keys())) {
            list($status, $msg, $drugid) = $this->cs->updateDrug('brand', $request->request, false);
            $this->addFlash(($status ? 'success' : 'danger'), $msg);

            if ($status) {
                return $this->redirectToRoute('drug_edit_brand', array('id' => $drugid));
            }
        }

        $tokens = array(
            'head' => array(
                'title'    => 'Create Brand Drug',
                'entity'   => 'Drugs',
                'action'   => 'Create',
                'subtitle' => 'Register a Brand Drug',
            ),
            'drug' => array(
                'id'                      => 0,
                'name'                    => '',
                'valid_int'               => 0,
                'unid'                    => '',
                'auto_encode_explude_int' => 0,
                'comments'                => '',
                'creation_date'           => '',
                'synonyms'                => array(),
                'typos'                   => array(),
            ),
            'template' => array(
                'type'      => 'brand',
                'countries' => true,
            ),
            'new' => true,
        );

        return $this->renderResponse('pages/drugs/brand.html.twig', $tokens);
    }

    /**
     * @Route("/modify/experimental/{id}", name="drug_edit_experimental", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_USER", message="Drug Registry only available for User.")
     *
     * Render Edit Experimental Page.
     *
     * @param  Request   $request
     * @param  int       $id        Experimental Drug ID
     *
     * @return Response
     */
    public function experimentalEditAction(Request $request, $id): Response
    {
        if (!empty($request->request->keys())) {
            $status = false;
            $msg    = 'Permission Denied: Edit Drug only available for Editor.';
            if ($this->isGranted('ROLE_EDITOR')) {
                list($status, $msg, $drugid) = $this->cs->updateDrug('experimental', $request->request, $id);
            }
            $this->addFlash(($status ? 'success' : 'danger'), $msg);
        }

        $drug = $this->cs->getExperimentalDrug($id);

        // redirect if missing
        if (empty($drug['main'])) {
            $this->addFlash('danger', "Requested Experimental Drug #{$id} is not available.");

            return $this->redirectToRoute('dashboard');
        }

        $tokens = array(
            'head' => array(
                'title'    => "Edit Experimental Drug #{$id}",
                'entity'   => 'Drugs',
                'action'   => 'Edit',
                'subtitle' => "Modify #{$id}",
            ),
            'drug' => array(
                'id'            => $drug['main']->getId(),
                'name'          => $drug['main']->getName(),
                'type'          => $drug['main']->getType(),
                'valid_int'     => (int) $drug['main']->getValid(),
                'unid'          => $drug['main']->getUid(),
                'comments'      => $drug['main']->getComments(),
                'creation_date' => $drug['main']->getCreationDate(),
            ),
            'template' => array(
                'type'       => 'experimental',
                'countries'  => false,
                'drug_types' => \App\Entity\Family\DrugTypeFamily::$mainDrugs,
            ),
            'new' => false,
        );

        // format
        if (!is_null($tokens['drug']['creation_date'])) {
            $tokens['drug']['creation_date'] = $tokens['drug']['creation_date']->format('Y-m-d H:i:s');
        }

        return $this->renderResponse('/pages/drugs/experimental.html.twig', $tokens);
    }

    /**
     * @Route("/modify/generic/{id}", name="drug_edit_generic", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_USER", message="Drug Registry only available for User.")
     *
     * Render Edit Generic Page.
     *
     * @param  Request   $request
     * @param  int       $id       Generic Drug ID
     *
     * @return Response
     */
    public function genericEditAction(Request $request, $id): Response
    {
        if (!empty($request->request->keys())) {
            $status = false;
            $msg    = 'Permission Denied: Edit Drug only available for Editor.';
            if ($this->isGranted('ROLE_EDITOR')) {
                list($status, $msg, $drugid) = $this->cs->updateDrug('generic', $request->request, $id);
            }
            $this->addFlash(($status ? 'success' : 'danger'), $msg);
        }

        $drug = $this->cs->getGenericDrug($id);

        // redirect if missing
        if (empty($drug['main'])) {
            $this->addFlash('danger', "Requested Generic Drug #{$id} is not available.");

            return $this->redirectToRoute('dashboard');
        }

        $synonyms = array();
        foreach ($drug['synonyms'] as $synonym) {
            $synonyms[] = array(
                'id'      => $synonym->getId(),
                'name'    => $synonym->getName(),
                'country' => $synonym->getCountry(),
            );
        }

        $typos = array();
        foreach ($drug['typos'] as $typo) {
            $typos[] = array(
                'id'   => $typo->getId(),
                'name' => $typo->getName(),
            );
        }

        $tokens = array(
            'head' => array(
                'title'    => "Edit Generic Drug #{$id}",
                'entity'   => 'Drugs',
                'action'   => 'Edit',
                'subtitle' => "Modify #{$id}",
            ),
            'drug' => array(
                'id'                      => $drug['main']->getId(),
                'name'                    => $drug['main']->getName(),
                'valid_int'               => (int) $drug['main']->getValid(),
                'unid'                    => $drug['main']->getUid(),
                'comments'                => $drug['main']->getComments(),
                'creation_date'           => $drug['main']->getCreationDate(),
                'synonyms'                => $synonyms,
                'typos'                   => $typos,
                'no_double_bounce_int'    => (int) $drug['main']->getNodoublebounce(),
                'auto_encode_explude_int' => (int) $drug['main']->getAutoencodeexclude(),
                'level_1'                 => $drug['main']->getLevel1(),
                'level_2'                 => $drug['main']->getLevel2(),
                'level_3'                 => $drug['main']->getLevel3(),
                'level_4'                 => $drug['main']->getLevel4(),
                'level_5'                 => $drug['main']->getLevel5(),
            ),
            'template' => array(
                'type'             => 'generic',
                'countries'        => true,
                'generic_fullcode' => true,
            ),
            'new' => false,
        );

        // format
        if (!is_null($tokens['drug']['creation_date'])) {
            $tokens['drug']['creation_date'] = $tokens['drug']['creation_date']->format('Y-m-d H:i:s');
        }

        return $this->renderResponse('/pages/drugs/generic.html.twig', $tokens);
    }

    /**
     * @Route("/modify/brand/{id}", name="drug_edit_brand", requirements={"id" = "\d+"})
     * @IsGranted("ROLE_USER", message="Drug Registry only available for User.")
     *
     * Render Edit Brand Page.
     *
     * @param  Request   $request
     * @param  int       $id       Brand Drug ID
     *
     * @return Response
     */
    public function brandEditAction(Request $request, $id): Response
    {
        if (!empty($request->request->keys())) {
            $status = false;
            $msg    = 'Permission Denied: Edit Drug only available for Editor.';
            if ($this->isGranted('ROLE_EDITOR')) {
                list($status, $msg, $drugid) = $this->cs->updateDrug('brand', $request->request, $id);
            }
            $this->addFlash(($status ? 'success' : 'danger'), $msg);
        }

        $drug = $this->cs->getBrandDrug($id);

        // redirect if missing
        if (empty($drug['main'])) {
            $this->addFlash('danger', "Requested Brand Drug #{$id} is not available.");

            return $this->redirectToRoute('dashboard');
        }

        $synonyms = array();
        foreach ($drug['synonyms'] as $synonym) {
            $synonyms[] = array(
                'id'      => $synonym->getId(),
                'name'    => $synonym->getName(),
                'country' => $synonym->getCountry(),
            );
        }

        $typos = array();
        foreach ($drug['typos'] as $typo) {
            $typos[] = array(
                'id'   => $typo->getId(),
                'name' => $typo->getName(),
            );
        }

        $tokens = array(
            'head' => array(
                'title'    => "Edit Brand Drug #{$id}",
                'entity'   => 'Drugs',
                'action'   => 'Edit',
                'subtitle' => "Modify #{$id}",
            ),
            'drug' => array(
                'id'                      => $drug['main']->getId(),
                'name'                    => $drug['main']->getName(),
                'valid_int'               => (int) $drug['main']->getValid(),
                'unid'                    => $drug['main']->getUid(),
                'comments'                => $drug['main']->getComments(),
                'creation_date'           => $drug['main']->getCreationDate(),
                'auto_encode_explude_int' => (int) $drug['main']->getAutoencodeexclude(),
                'synonyms'                => $synonyms,
                'typos'                   => $typos,
            ),
            'template' => array(
                'type'      => 'brand',
                'countries' => true,
            ),
            'new' => false,
        );

        // format
        if (!is_null($tokens['drug']['creation_date'])) {
            $tokens['drug']['creation_date'] = $tokens['drug']['creation_date']->format('Y-m-d H:i:s');
        }

        return $this->renderResponse('/pages/drugs/brand.html.twig', $tokens);
    }
}
