<?php

namespace App\Command;

use App\Service\MailService;
use App\Service\SearchService;
use App\Utility\Helpers as H;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * $ bin/console app:drugs-export-data.
 *
 * Class handing Command request to export data.
 * This will be then email on argument.
 *
 * @since 1.0.0
 */
class DrugsDataExportCommand extends Command
{
    use \App\Command\Traits\CommandsBasicTrait;

    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'app:drugs-export-data';

    /**
     * @var SearchService
     */
    private $searchSrc;

    /**
     * @var MailService
     */
    private $mailSrc;

    /**
     * Command Configurations.
     *
     * @var array
     */
    private $cfg = [
        'email'    => '',
        'search'   => '',
        'type'     => 'any',
        'complete' => true,
        'fullcode' => true,
        'uuid'     => false,
    ];

    /**
     * @param SearchService    $searchSrc
     * @param MailService      $mailSrc
     * @param LoggerInterface  $logger
     */
    public function __construct(SearchService $searchSrc, MailService $mailSrc, LoggerInterface $logger)
    {
        $this->searchSrc = $searchSrc;
        $this->mailSrc   = $mailSrc;
        $this->logger    = $logger;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Export Drugs into CSV with Search Attribute, then Email it.')
            ->addArgument(
                'email',
                InputArgument::REQUIRED,
                'Email Address.'
            )
            ->addArgument(
                'search',
                InputArgument::REQUIRED,
                'Search Terms. Please refer Search Guide for Symbols Usage.'
            )
            ->addArgument(
                'type',
                InputArgument::OPTIONAL,
                'Drug type either; "any" / "experimental" / "generic" / "brand".',
                (string) ($this->cfg['type'] ?? 'any')
            )
            ->addArgument(
                'complete',
                InputArgument::OPTIONAL,
                'Download Version Flag; "1"/"yes"= Complete, "0"/"no" = Just Names.',
                ($this->cfg['complete'] ? 'yes' : 'no')
            )
            ->addArgument(
                'fullcode',
                InputArgument::OPTIONAL,
                'Generic must have Full Code; "1"/"yes" = With Fullcode, "0"/"no" = Without Fullcode.',
                ($this->cfg['fullcode'] ? 'yes' : 'no')
            )
            ->addArgument(
                'uuid',
                InputArgument::OPTIONAL,
                'Lookup for Drug UUID instead; "1"/"yes" = UUID Look Up, "0"/"no" = Normal Search.',
                ($this->cfg['uuid'] ? 'yes' : 'no')
            )
            ->setHelp(implode(PHP_EOL, [
                'Example 1: To Export all Drugs with Prefix of "R", with Complete Relation:',
                ' - `$ php bin/console app:drugs-export-data "john.doe@email.com" "^R" "any" "1"`',
                'Example 2: To Export Drugs with UUID of "12345678901234567890123456789012", on Generic Drug:',
                ' - `$ php bin/console app:drugs-export-data "john.doe@email.com" "12345678901234567890123456789012" "generic" "1" "no" "yes"`',
            ]));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // setup
        $this->cInput  = $input;
        $this->cOutput = new BufferedOutput($output->getVerbosity(), $output->isDecorated(),  $output->getFormatter());
        $this->_markCommandStarts();

        // validate
        foreach ($this->cfg as $key => $val) {
            $value = $this->cInput->getArgument($key);

            if (is_array($value)) {
                // @codeCoverageIgnoreStart
                $this->logger->error("Invalid '{$key}' multiple, expecting single value only.");
                return 1;
                // @codeCoverageIgnoreEnd
            }

            switch ($key) {
                case 'type':
                    $this->cfg[$key] = (is_null($value) ? '' : strtolower(trim($value)));
                    break;

                case 'complete':
                case 'fullcode':
                case 'uuid':
                    $this->cfg[$key] = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                    break;

                default:
                    $this->cfg[$key] = $value;
            }
        }

        // set welcome
        $this->_setWelcome([
            'Exporting Drugs Data',
            ['Email to',        $this->cfg['email']],
            ['Search term',     $this->cfg['search']],
            ['Limited to Drug', $this->cfg['type']],
            ['Limit FullCode',  ($this->cfg['fullcode'] ? 'Yes'      : 'No')],
            ['UUID Look Up',    ($this->cfg['uuid']     ? 'Yes'      : 'No')],
            ['CSV Version',     ($this->cfg['complete'] ? 'Complete' : 'Names Only')],
        ]);

        $this->logger->info('COMMAND: Export Drugs Data Request Received.', [
            'method' => __METHOD__,
            'config' => $this->cfg,
        ]);

        $this->cOutput->writeln('Processing...');

        $result_file = $error = false;
        try {
            $result = $this->searchSrc
                ->setTerm($this->cfg['search'])
                ->setType($this->cfg['type'])
                ->setHasCodeOnly($this->cfg['fullcode'])
                ->setUnidOnly($this->cfg['uuid'])
                ->setExport(true)
                ->setExportComplete($this->cfg['complete'])
                ->execute();

            $result_file = ($result['file-path'] ?? false);

            if ($result_file) {
                $this->cOutput->writeln('Export Complete.');

                $filesize = filesize($result_file);
                $filesizeRead = 'Unknown';
                if (false !== $filesize) {
                    $filesize = H::convertHumanReadableSize((float) $filesize, 1);
                }
                $this->cOutput->writeln('Export Size: ' . $filesizeRead);

                // compress >2Mb
                if (
                    (class_exists('ZipArchive')) &&
                    (false !== $filesize) &&
                    (2097152 < $filesize)
                ) {
                    // @codeCoverageIgnoreStart
                    $this->cOutput->writeln('Compressing...');

                    $ext = '.zip';

                    $zip = new \ZipArchive();
                    $zip->open($result_file . $ext, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
                    $zip->addFile($result_file, basename($result_file));
                    $zip->close();

                    if (file_exists($result_file . $ext)) {
                        $result_file .= $ext;
                        chmod($result_file, 0777);
                    }
                    // @codeCoverageIgnoreEnd
                }
            }
        }
        // @codeCoverageIgnoreStart
        catch (\Exception $e) {
            $error = [
                'Error occur: ' . $e->getMessage(),
                'File       : ' . $e->getFile(),
                'Line       : ' . $e->getLine(),
            ];
            $this->cOutput->writeln($error);
            $error['trace'] = $e->getTrace();
            $this->logger->error($e->getMessage(), $error);
        }
        // @codeCoverageIgnoreEnd

        $buffered = $this->cOutput->fetch();
        $output->write($buffered);

        if (!empty($result_file)) {
            // file path is passed
            $this->cOutput->writeln('Done.');

            $email = [
                'heading'            => 'Drugs Export Data Attached',
                'greeting'           => 'Hi,',
                'content_top'        => 'Export complete, file attached.',
                'pre_content_before' => 'Recorded process:',
                'pre_content'        => $this->_ansi2html($buffered),
            ];
        }
        else {
            // @codeCoverageIgnoreStart
            if (empty($error)) {
                $this->cOutput->writeln('Error occur: Unknown issue.');
            }

            $email = [
                'heading'            => 'Drugs Export Data Failed',
                'greeting'           => 'Hi,',
                'content_top'        => 'Export Incomplete, an error has occur.',
                'pre_content_before' => 'Error below were recorded:',
                'pre_content'        => $this->_ansi2html($buffered),
            ];
            // @codeCoverageIgnoreEnd
        }

        $this->cOutput->writeln('Emailing...');

        // email the file
        $sent = $this->mailSrc
            ->setTemplate('email/boxed.html.twig')
            ->sentEmail(
                $this->cfg['email'],
                'Your Drugs Export on ' . date('Y-m-d H:i', $this->timedStart),
                $email,
                (empty($result_file) ? [] : [$result_file])
            );

        $this->_markCommandEnds((!empty($result_file) ? (!empty($sent)) : false));

        $buffered = $this->cOutput->fetch();
        $output->write($buffered);
    }
}
