<?php

namespace App\Command;

use App\Entity\Journal;
use App\Service\JournalSourceService;
use App\Utility\Helpers as H;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * $ bin/console app:journals-sync-down.
 *
 * Extending RemoteSyncDownBySpawnCommand
 *
 * This class for handling ONE-OFF Source-Sync Command.
 * - All database SQL command made natively.
 * - RE-USE REMOTE ID to sync-down -- Duplicated and Empty will be IGNORE.
 * - Test show this may takes 10s to sync-down 4k items.
 *
 * @since 1.2.0
 */
class JournalsSyncDownCommand extends RemoteSyncDownBySpawnCommand
{
    use \App\Command\Traits\SourceImportSanitizerCommandsTrait;

    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'app:journals-sync-down';

    /**
     * @var JournalSourceService
     */
    private $journalSrcSrv;

    /**
     * Command Configurations.
     *
     * @var array
     */
    protected $cfg = [
        'query-limit'       => parent::PERLOOP_QUERY_LIMIT,
        'cycle'             => 0,
        'limit-loop'        => false,
        'skip-local-filter' => false,
        'processed'         => [],
        'ignored'           => [],
        'last-id'           => 0,
    ];

    /**
     * @param KernelInterface          $kernel
     * @param EntityManagerInterface   $em
     * @param LoggerInterface          $logger
     * @param JournalSourceService     $journalSrcSrv
     * @param int|null                 $spawnTimeout
     */
    public function __construct(KernelInterface $kernel, EntityManagerInterface $em, LoggerInterface $logger, JournalSourceService $journalSrcSrv, ?int $spawnTimeout = null)
    {
        $this->journalSrcSrv = $journalSrcSrv;

        parent::__construct($kernel, $em, $logger, $spawnTimeout);
    }

    /**
     * {@inheritdoc}
     */
    protected function getWelcomeTablesInfo(): array
    {
        return [
            ['DB Core Table',        $this->journalSrcSrv->getPrimaryTableName(true, false)],
            ['DB Detail Table',      $this->journalSrcSrv->getSecondaryTableName(true, false)],
            ['DB Specialties Table', $this->journalSrcSrv->getSpecialtiesTableName(true, false)],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getReadbleEntityName(bool $plural = true): string
    {
        if ($plural) {
            return 'Journals';
        }

        return 'Journal';
    }

    /**
     * {@inheritdoc}
     */
    protected function getRemoteDbInfo(): array
    {

        return $this->journalSrcSrv->remote->getInfo();
    }

    /**
     * {@inheritdoc}
     */
    protected function checkRemoteDb(bool $killConnection = false): void
    {
        $this->cOutput->writeln(['', '>> Connecting Remote Database...']);

        $this->makeRemoteDbConnection();
        $this->cOutput->writeln('-- Database Connection OK.');

        if ($killConnection) {
            unset($this->journalSrcSrv);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function makeRemoteDbConnection(): void
    {

        $this->journalSrcSrv->remote->makeConnection(true);
    }

    /**
     * {@inheritdoc}
     */
    protected function getRemoteDbConn(): \PDO
    {

        return $this->journalSrcSrv->remote->getConnection();
    }

    /**
     * {@inheritdoc}
     */
    protected function getPerCycleRemoteQuery(): string
    {
        return 'SELECT * FROM
        (
            SELECT
                ROW_NUMBER() OVER (ORDER BY "m"."JournalID" ASC) AS "seq",
                "m"."JournalID"                     AS "m_JournalID",
                "m"."Name"                          AS "m_Name",
                "m"."Abbreviation"                  AS "m_Abbreviation",
                "m"."UNID"                          AS "m_UNID",
                "m"."URL"                           AS "m_URL",
                "m"."Status"                        AS "m_Status",
                "m"."BeginDate"                     AS "m_BeginDate",
                "m"."LastUpdate"                    AS "m_LastUpdate",
                "m"."Deleted"                       AS "m_Deleted",
                "m"."ManualCreation"                AS "m_ManualCreation",
                "m"."JR1"                           AS "m_JR1",
                "m"."JR2"                           AS "m_JR2",
                "m"."ISSN"                          AS "m_ISSN",
                "m"."DGAbstract"                    AS "m_DGAbstract",
                CAST("m"."rowguid" AS VARCHAR(36))  AS "m_rowguid",
                "m"."SecondLine"                    AS "m_SecondLine",
                "d"."JournalID"                     AS "d_JournalID",
                "d"."JournalVolume"                 AS "d_JournalVolume",
                "d"."JournalNumber"                 AS "d_JournalNumber",
                "d"."LastViewed"                    AS "d_LastViewed",
                "d"."Notes"                         AS "d_Notes",
                "d"."Frequency"                     AS "d_Frequency",
                "d"."UserName"                      AS "d_UserName",
                "d"."Password"                      AS "d_Password",
                "d"."Publisher"                     AS "d_Publisher",
                "d"."regurl"                        AS "d_regurl",
                "d"."Language"                      AS "d_Language",
                "d"."Country"                       AS "d_Country",
                "d"."Region"                        AS "d_Region",
                "d"."Editions"                      AS "d_Editions",
                "d"."Copyright"                     AS "d_Copyright",
                "d"."NotBeingUpdated"               AS "d_NotBeingUpdated",
                "d"."DrugsMonitored"                AS "d_DrugsMonitored",
                "d"."GlobalEditionJournal"          AS "d_GlobalEditionJournal",
                "d"."abstract"                      AS "d_abstract",
                "d"."regabstract"                   AS "d_regabstract",
                "d"."fulltxt"                       AS "d_fulltxt",
                "d"."regfulltxt"                    AS "d_regfulltxt",
                "d"."feefulltxt"                    AS "d_feefulltxt",
                "d"."comments"                      AS "d_comments",
                "d"."DocAuthor"                     AS "d_DocAuthor",
                "d"."cleared_by"                    AS "d_cleared_by",
                "d"."cleared_on"                    AS "d_cleared_on",
                "d"."Copied"                        AS "d_Copied",
                "d"."Modified"                      AS "d_Modified",
                "d"."JournalHTMLCode"               AS "d_JournalHTMLCode",
                "d"."CGIValue"                      AS "d_CGIValue",
                "d"."PrinterName"                   AS "d_PrinterName",
                STUFF(
                    (
                        SELECT
                            CAST (\';\' as VARCHAR(max)) + "s"."Specialty"
                        FROM
                            ' . $this->journalSrcSrv->getSpecialtiesTableName() . ' "s"
                        WHERE
                            "s"."JournalID" = "m"."JournalID"
                        ORDER BY
                            "s"."Specialty"
                        FOR xml path(\'\')
                    ), 1, 1, \'\'
                ) AS "s_Specialties"
            FROM
                ' . $this->journalSrcSrv->getPrimaryTableName() . ' "m"
                    LEFT JOIN ' . $this->journalSrcSrv->getSecondaryTableName() . ' "d"
                           ON "m"."UNID" = "d"."JournalID"
           ) "t"
           WHERE
               "seq" BETWEEN :row_start AND :row_end
        ;';
    }

    /**
     * {@inheritdoc}
     */
    protected function extractDataRowKey(array $data): ?string
    {

        return $data['m_JournalID'] ?? null;
    }

    /**
     * {@inheritdoc}
     */
    protected function queryToFilterResultsPerCycle(): void
    {
        $remote_ids = array_keys($this->cycleDataSet);
        if (empty($remote_ids)) {
            return;
        }

        $sql = H::removeExcessWhitespace('
            SELECT
                `id` AS `known_id`
            FROM
                `journal`
            WHERE
                `id` IN (?)
        ');
        $localDb = $this->em->getConnection();
        $stmt = $localDb->executeQuery(
            $sql,
            [
                $remote_ids,
            ],
            [
                \Doctrine\DBAL\Connection::PARAM_STR_ARRAY,
            ]
        );
        $results = $stmt->fetchAll();
        $stmt->closeCursor();
        if (empty($results)) {
            return;
        }

        $known_ids = array_column($results, 'known_id');
        $results = array_flip($known_ids);
        $before = count($this->cycleDataSet);

        $this->cycleDataSet = array_diff_key($this->cycleDataSet, $results);
        $after = count($this->cycleDataSet);

        $count = ($before - $after);
        if ($count > 0) {
            $this->logger->warning('Source Journal Ignored as ID Found stored on Local', [
                'cycle'         => $this->cfg['cycle'],
                'ignored-count' => $count,
                // @todo review the needs to record this
                // 'known-ids'     => $known_ids,
            ]);
            $this->cfg['ignored'][ $this->cfg['cycle'] ] += $count;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function storeResultsPerCycle(): void
    {
        if (empty($this->cycleDataSet)) {
            return;
        }

        foreach ($this->cycleDataSet as $id => $result) {
            try {
                $unid = $this->__trimString($result['m_UNID']);
                if ('0' === (string) $unid) {
                    $unid = null;
                }

                $country = $this->__lookUpCountryName($result['d_Country']);
                $country_code  = null;
                if (null !== $country) {
                    $country_code = $country->getIsoCode();
                }

                $region = $this->__lookUpRegion($country, $result['d_Region']);
                if (null !== $region) {
                    $region = $region->getId();
                }

                list($publisher, $publisherRegUrl) = $this->__sanitizeJournalPublisher($result['d_Publisher'], $result['d_regurl']);

                list($lastViewed, $lastViewedComment) = $this->__sanitizeJournalLastViewed($result['d_LastViewed']);

                $journal = new Journal( $result['m_JournalID'] );
                $journal
                    ->setName( $this->__safeString($result['m_Name']) )
                    ->setAbbreviation( $this->__safeString($result['m_Abbreviation']) )
                    ->setVolume( $this->__trimString($result['d_JournalVolume']) )
                    ->setNumber( $this->__trimString($result['d_JournalNumber']) )

                    ->setStatus( $this->__convertJournalStatus($result['m_Status']) )
                    ->setExported( $this->__convertIntFromBitYN($result['d_Copied']) )
                    ->setIsMedline( $this->__convertJournalIsMedLine($result['m_ISSN']) )
                    ->setIsSecondLine( $this->__convertIntFromBitInt($result['m_SecondLine']) )
                    ->setIsDgAbstract( $this->__convertIntFromBitInt($result['m_DGAbstract']) )

                    ->setNotBeingUpdated( $this->__convertIntFromBitYN($result['d_NotBeingUpdated']) )
                    ->setDrugsMonitored( $this->__convertIntFromBitYN($result['d_DrugsMonitored']) )
                    ->setGlobalEditionJournal( $this->__convertIntFromBitYN($result['d_GlobalEditionJournal']) )

                    ->setDeleted( $this->__convertIntFromBitInt($result['m_Deleted']) )
                    ->setManualCreation( $this->__convertIntFromBitInt($result['m_ManualCreation']) )

                    ->setMedlineIssn( $this->__trimString($result['m_ISSN']) )

                    ->setJournalReport1( $this->__sanitizeDecimalValue($result['m_JR1']) )
                    ->setJournalReport2( $this->__sanitizeDecimalValue($result['m_JR2']) )

                    ->setUnid( $unid )

                    ->setDetailsLanguage( $this->__trimString($result['d_Language']) ?? '' )
                    ->setDetailsCountry( $country_code )
                    ->setDetailsRegion( $region )
                    ->setDetailsEdition( $this->__trimString($result['d_Editions']) ?? '' )
                    ->setDetailsCopyright( $this->__trimString($result['d_Copyright']) ?? '' )
                    ->setDetailsPreviousRowGuid( $this->__trimString($result['m_rowguid']) ?? '' )

                    ->setDetailsAbstractAbstract( $this->__convertBoolFromBitYN($result['d_abstract']) )
                    ->setDetailsAbstractRegForAbstract( $this->__convertBoolFromBitYN($result['d_regabstract']) )
                    ->setDetailsAbstractFullTextAvailablity( $this->__convertBoolFromBitYN($result['d_fulltxt']) )
                    ->setDetailsAbstractRegForFullText( $this->__convertBoolFromBitYN($result['d_regfulltxt']) )
                    ->setDetailsAbstractFeeForFullText( $this->__convertBoolFromBitYN($result['d_feefulltxt']) )
                    ->setDetailsAbstractComments( $this->__trimString($result['d_comments']) ?? '' )

                    ->setDetailsOrderingHtmlCode( $this->__trimString($result['d_JournalHTMLCode']) ?? '' )
                    ->setDetailsOrderingCgiValue( $this->__trimString($result['d_CGIValue']) ?? '' )
                    ->setDetailsOrderingPrinterName( $this->__trimString($result['d_PrinterName']) ?? '' )

                    ->setDetailsPublisherName( $publisher )
                    ->setDetailsPublisherRegUrl( $publisherRegUrl )

                    ->setDetailsUrlUrl( $this->__trimString($result['m_URL']) ?? '' )
                    ->setDetailsUrlUpdatedOn( $this->__convertDate($result['m_BeginDate']) )
                    ->setDetailsUrlViewedOn( $lastViewed )
                    ->setDetailsUrlViewedOnComment( $lastViewedComment )
                    ->setDetailsUrlToUpdate( $this->__convertDate($result['m_LastUpdate']) )
                    ->setDetailsUrlNote( $this->__trimString($result['d_Notes']) ?? '' )
                    ->setDetailsUrlFrequency( $this->__trimString($result['d_Frequency']) ?? '' )
                    ->setDetailsUrlUsername( $this->__trimString($result['d_UserName']) ?? '' )
                    ->setDetailsUrlPassword( $this->__trimString($result['d_Password']) ?? '' )

                    ->setDetailsValidationAuthor( $this->__trimString($result['d_DocAuthor']) ?? '' )
                    ->setDetailsValidationClearedBy( $this->__trimString($result['d_cleared_by']) ?? '' )
                    ->setDetailsValidationClearedOn( $this->__convertDate($result['d_cleared_on'], 'd M Y H:i:s') )

                    ->setDetailsMetaCreatedOn( new \DateTime('now') )
                    ->setDetailsMetaCreatedBy('Imported')
                ;

                $this->__convertSpecialty($journal, $result['s_Specialties']);

                $this->cfg['last-id'] = $result['m_JournalID'];

                $this->em->persist($journal);
                unset($journal);

                $this->cfg['processed'][ $this->cfg['cycle'] ] += 1;
            }
            catch (\Exception $e) {
                $this->errorHandlerOnStoreResultsPerCycle($e, $id, $result);
            }
        }

        // turn off Journal ID AI
        $metadata = $this->em->getClassMetaData(Journal::class);
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    protected function updateEntityWidgetData(int $processed = 0, int $ignored  = 0): void
    {
        /**
         * @var \App\Repository\VariableRepository<Variable>
         */
        $variableRepo = $this->em->getRepository(\App\Entity\Variable::class);

        $variableRepo->updateWidgetValue('journal-sync-last-date',            new \DateTime('now'),  'datetime');
        $variableRepo->updateWidgetValue('journal-sync-last-id',              $this->cfg['last-id'], 'integer');
        $variableRepo->updateWidgetValue('journal-sync-last-processed-count', $processed,            'integer');
        $variableRepo->updateWidgetValue('journal-sync-last-ignored-count',   $ignored,              'integer');
    }
}
