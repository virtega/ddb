<?php

namespace App\Command;

use App\Entity\Conference;
use App\Service\ConferenceSourceService;
use App\Utility\Helpers as H;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * $ bin/console app:conferences-sync-down.
 *
 * Extending RemoteSyncDownBySpawnCommand
 *
 * This class for handling ONE-OFF Source-Sync Command.
 * - All database SQL command made natively.
 * - RE-USE REMOTE ID to sync-down -- Duplicated will be IGNORE.
 * - Conference has no sequence ID, so Date were use to Order sync items.
 * - Test show this may takes 1:45s to sync-down 60k items.
 *
 * @since 1.2.0
 */
class ConferencesSyncDownCommand extends RemoteSyncDownBySpawnCommand
{
    use \App\Command\Traits\SourceImportSanitizerCommandsTrait;

    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'app:conferences-sync-down';

    /**
     * @var ConferenceSourceService
     */
    private $confSrcSrv;

    /**
     * Command Configurations.
     *
     * @var array
     */
    protected $cfg = [
        'query-limit'       => parent::PERLOOP_QUERY_LIMIT,
        'cycle'             => 0,
        'limit-loop'        => false,
        'skip-local-filter' => false,
        'processed'         => [],
        'ignored'           => [],
    ];

    /**
     * @param KernelInterface          $kernel
     * @param EntityManagerInterface   $em
     * @param LoggerInterface          $logger
     * @param ConferenceSourceService  $confSrcSrv
     * @param int|null                 $spawnTimeout
     */
    public function __construct(KernelInterface $kernel, EntityManagerInterface $em, LoggerInterface $logger, ConferenceSourceService $confSrcSrv, ?int $spawnTimeout = null)
    {
        $this->confSrcSrv = $confSrcSrv;

        parent::__construct($kernel, $em, $logger, $spawnTimeout);
    }

    /**
     * {@inheritdoc}
     */
    protected function getWelcomeTablesInfo(): array
    {
        return [
            ['DB Table Core',   $this->confSrcSrv->getPrimaryTableName(true, false)],
            ['DB Table Detail', $this->confSrcSrv->getSecondaryTableName(true, false)],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getReadbleEntityName(bool $plural = true): string
    {
        if ($plural) {
            return 'Conferences';
        }

        return 'Conference';
    }

    /**
     * {@inheritdoc}
     */
    protected function getRemoteDbInfo(): array
    {

        return $this->confSrcSrv->remote->getInfo();
    }

    /**
     * {@inheritdoc}
     */
    protected function checkRemoteDb(bool $killConnection = false): void
    {
        $this->cOutput->writeln(['', '>> Connecting Remote Database...']);

        $this->makeRemoteDbConnection();
        $this->cOutput->writeln('-- Database Connection OK.');

        if ($killConnection) {
            unset($this->confSrcSrv);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function makeRemoteDbConnection(): void
    {

        $this->confSrcSrv->remote->makeConnection(true);
    }

    /**
     * {@inheritdoc}
     */
    protected function getRemoteDbConn(): \PDO
    {

        return $this->confSrcSrv->remote->getConnection();
    }

    /**
     * {@inheritdoc}
     */
    protected function getPerCycleRemoteQuery(): string
    {
        return 'SELECT * FROM
            (
                SELECT
                    ROW_NUMBER() OVER (ORDER BY "m"."StartDate" ASC) AS "seq",
                    "m"."CongressID"            AS "m_CongressID",
                    "m"."Name"                  AS "m_Name",
                    "m"."City"                  AS "m_City",
                    "m"."CityGuide"             AS "m_CityGuide",
                    "m"."CountryCode"           AS "m_CountryCode",
                    "m"."State"                 AS "m_State",
                    "m"."Region"                AS "m_Region",
                    "m"."Discontinued"          AS "m_Discontinued",
                    "m"."StartDate"             AS "m_StartDate",
                    "m"."EndDate"               AS "m_EndDate",
                    "m"."Contact"               AS "m_Contact",
                    "m"."Email"                 AS "m_Email",
                    "m"."Fax"                   AS "m_Fax",
                    "m"."Phone"                 AS "m_Phone",
                    "m"."Specialty"             AS "m_Specialty",
                    "m"."Cruise"                AS "m_Cruise",
                    "m"."KeyCongress"           AS "m_KeyCongress",
                    "m"."Online"                AS "m_Online",
                    "d"."CongressID"            AS "d_CongressID",
                    "d"."IncludeCongressGuide"  AS "d_IncludeCongressGuide",
                    "d"."CongressLink1"         AS "d_CongressLink1",
                    "d"."CongressLink2"         AS "d_CongressLink2",
                    "d"."CongressLink3"         AS "d_CongressLink3",
                    "d"."CongressLink4"         AS "d_CongressLink4",
                    "d"."CongressLink5"         AS "d_CongressLink5",
                    "d"."CongressLink6"         AS "d_CongressLink6",
                    "d"."CongressURL1"          AS "d_CongressURL1",
                    "d"."CongressURL2"          AS "d_CongressURL2",
                    "d"."CongressURL3"          AS "d_CongressURL3",
                    "d"."CongressURL4"          AS "d_CongressURL4",
                    "d"."CongressURL5"          AS "d_CongressURL5",
                    "d"."CongressURL6"          AS "d_CongressURL6",
                    "d"."UniqueName"            AS "d_UniqueName"
                FROM
                    ' . $this->confSrcSrv->getPrimaryTableName() . ' "m"
                        LEFT JOIN ' . $this->confSrcSrv->getSecondaryTableName() . ' "d"
                            ON "m"."CongressID" = "d"."CongressID" COLLATE DATABASE_DEFAULT
            ) "t"
            WHERE
                "seq" BETWEEN :row_start AND :row_end
        ;';
    }

    /**
     * {@inheritdoc}
     */
    protected function extractDataRowKey(array $data): ?string
    {
        $id = ($data['m_CongressID'] ?? null);

        if (empty($id)) {
            $id = $data['d_CongressID'];
        }
        if (empty($id)) {
            $id = $data['d_UniqueName'];
        }

        return $id;
    }

    /**
     * {@inheritdoc}
     */
    protected function queryToFilterResultsPerCycle(): void
    {
        $remote_ids = array_keys($this->cycleDataSet);
        if (empty($remote_ids)) {
            return;
        }

        $sql = H::removeExcessWhitespace('
            SELECT DISTINCT `known_id` FROM (
                SELECT
                    `previous_id` AS `known_id`
                FROM
                    `conference`
                WHERE
                    `previous_id` IS NOT NULL
                    AND
                    `previous_id` != ""
                    AND
                    `previous_id` IN (?)

                UNION ALL

                SELECT
                    `unique_name` AS `known_id`
                FROM
                    `conference`
                WHERE
                    `unique_name` IS NOT NULL
                    AND
                    `unique_name` != ""
                    AND
                    `unique_name` IN (?)
            ) `t`
        ');
        $localDb = $this->em->getConnection();
        $stmt = $localDb->executeQuery(
            $sql,
            [
                $remote_ids,
                $remote_ids
            ],
            [
                \Doctrine\DBAL\Connection::PARAM_STR_ARRAY,
                \Doctrine\DBAL\Connection::PARAM_STR_ARRAY,
            ]
        );
        $results = $stmt->fetchAll();
        $stmt->closeCursor();
        if (empty($results)) {
            return;
        }

        $known_ids = array_column($results, 'known_id');
        $results = array_flip($known_ids);
        $before = count($this->cycleDataSet);

        $this->cycleDataSet = array_diff_key($this->cycleDataSet, $results);
        $after = count($this->cycleDataSet);

        $count = ($before - $after);
        if ($count > 0) {
            $this->logger->warning('Source Conference Ignored as ID Found stored on Local', [
                'cycle'         => $this->cfg['cycle'],
                'ignored-count' => $count,
                // @todo review the needs to record this
                // 'known-ids'     => $known_ids,
            ]);
            $this->cfg['ignored'][ $this->cfg['cycle'] ] += $count;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function storeResultsPerCycle(): void
    {
        if (empty($this->cycleDataSet)) {
            return;
        }

        foreach ($this->cycleDataSet as $id => $result) {
            try {
                $country = $this->__lookUpCountryIsoCode($result['m_CountryCode']);

                $region = $this->__lookUpRegion($country, $result['m_Region']);

                list($previous_id, $unique_name) = $this->__lookFilterIds(2, [
                    $result['m_CongressID'],
                    $result['d_CongressID'],
                    $result['d_UniqueName'],
                ]);

                $conference = new Conference;
                $conference
                    ->setName( $this->__safeString($result['m_Name']) )
                    ->setStartDate( $this->__convertDate($result['m_StartDate']) )
                    ->setEndDate( $this->__convertDate($result['m_EndDate']) )
                    ->setCity( $this->__trimString($result['m_City']) )
                    ->setCityGuide( $this->__trimString($result['m_CityGuide']) )
                    ->setCountry( $country )
                    ->setRegion( $region )
                    ->setPreviousId( $previous_id )
                    ->setUniqueName( $unique_name )
                    ->setCruise( $this->__convertIntFromYesString($result['m_Cruise']) )
                    ->setOnline( $this->__convertIntFromYesString($result['m_Online']) )
                    ->setKeyEvent( $this->__convertIntFromYesString($result['m_KeyCongress']) )
                    ->setHasGuide( $this->__convertIntFromBitI($result['d_IncludeCongressGuide']) )
                    ->setDiscontinued( $this->__convertIntFromYesString($result['m_Discontinued']) )
                    ->setDetailsState( ($this->__trimString($result['m_State']) ?? '') )
                    ->setDetailsContactDesc( ($this->__trimString($result['m_Contact']) ?? '') )
                    ->setDetailsContactPhones( $this->__convertPhonesFaxes($result['m_Phone']) )
                    ->setDetailsContactFaxes( $this->__convertPhonesFaxes($result['m_Fax']) )
                    ->setDetailsContactEmails( $this->__convertEmails($result['m_Email']) )
                    ->setDetailsContactLinks( $this->__convertConferenceLinks($result) )
                    ->setDetailsMetaCreatedOn( new \DateTime('now') )
                    ->setDetailsMetaCreatedBy('Imported')
                ;

                $this->__convertSpecialty($conference, $result['m_Specialty']);

                $this->em->persist($conference);
                unset($conference);

                $this->cfg['processed'][ $this->cfg['cycle'] ] += 1;
            }
            catch (\Exception $e) {
                $this->errorHandlerOnStoreResultsPerCycle($e, $id, $result);
            }
        }

        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    protected function updateEntityWidgetData(int $processed = 0, int $ignored  = 0): void
    {
        /**
         * @var \App\Repository\VariableRepository<Variable>
         */
        $variableRepo = $this->em->getRepository(\App\Entity\Variable::class);

        $variableRepo->updateWidgetValue('conference-sync-last-date',            new \DateTime('now'), 'datetime');
        $variableRepo->updateWidgetValue('conference-sync-last-processed-count', $processed,           'integer');
        $variableRepo->updateWidgetValue('conference-sync-last-ignored-count',   $ignored,             'integer');
    }
}
