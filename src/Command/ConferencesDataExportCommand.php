<?php

namespace App\Command;

use App\Entity\Conference;
use App\Entity\CountryTaxonomy;
use App\Entity\SpecialtyTaxonomy;
use App\Service\MailService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * $ bin/console app:conferences-export-data.
 *
 * Class handing Command request to export data.
 * This will be then email on argument.
 *
 * @since 1.2.3
 */
final class ConferencesDataExportCommand extends AbstractConfJourDataExportCommand
{
    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'app:conferences-export-data';

    /**
     * @var string
     */
    public static $type = AbstractConfJourDataExportCommand::TYPE_CONFERENCES;

    /**
     * Command Configurations.
     *
     * @var array
     */
    public static $cfg = [
        'name'          => '',
        'city'          => [],
        'country'       => [],
        'region'        => [],
        'specialty'     => [],
        'startdate'     => '',
        'enddate'       => '',
        'ids'           => '',
        'keycongress'   => '',
        'oncruise'      => '',
        'online'        => '',
        'hasguide'      => '',
        'discontinued'  => '',
        'showcorrupted' => 'no',
    ];

    /**
     * @var \Doctrine\ORM\EntityRepository<CountryTaxonomy>
     */
    private $regionRepo;

    /**
     * @var \Doctrine\ORM\EntityRepository<SpecialtyTaxonomy>
     */
    private $specialtyRepo;

    /**
     * @param EntityManagerInterface  $em
     * @param MailService             $mailSrc
     * @param LoggerInterface         $logger
     */
    public function __construct(EntityManagerInterface $em, MailService $mailSrc, LoggerInterface $logger)
    {
        /**
         * @var \App\Repository\ConferenceRepository
         */
        $this->repo = $em->getRepository(Conference::class);

        /**
         * @var \Doctrine\ORM\EntityRepository<SpecialtyTaxonomy>
         */
        $this->regionRepo = $em->getRepository(CountryTaxonomy::class);

        /**
         * @var \Doctrine\ORM\EntityRepository<SpecialtyTaxonomy>
         */
        $this->specialtyRepo = $em->getRepository(SpecialtyTaxonomy::class);

        parent::__construct($mailSrc, $logger);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Export Conference into CSV with Search Attribute, then Email it.')
            ->addArgument(
                'email',
                InputArgument::REQUIRED,
                'Email Address'
            )
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'Name',
                ''
            )
            ->addArgument(
                'city',
                InputArgument::OPTIONAL,
                'City IDs, separated by comma',
                ''
            )
            ->addArgument(
                'country',
                InputArgument::OPTIONAL,
                'Country IDs, separated by comma',
                ''
            )
            ->addArgument(
                'region',
                InputArgument::OPTIONAL,
                'Region IDs, separated by comma',
                ''
            )
            ->addArgument(
                'specialty',
                InputArgument::OPTIONAL,
                'Specialty IDs, separated by comma',
                ''
            )
            ->addArgument(
                'startdate',
                InputArgument::OPTIONAL,
                'Conference Start date, format YYYY-MM-DD',
                ''
            )
            ->addArgument(
                'enddate',
                InputArgument::OPTIONAL,
                'Conference End date, format YYYY-MM-DD',
                ''
            )
            ->addArgument(
                'ids',
                InputArgument::OPTIONAL,
                'ID /  UNID',
                ''
            )
            ->addArgument(
                'keycongress',
                InputArgument::OPTIONAL,
                'Marked as Key Congress; "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'oncruise',
                InputArgument::OPTIONAL,
                'Marked as On Cruise;    "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'online',
                InputArgument::OPTIONAL,
                'Marked as Online;       "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'hasguide',
                InputArgument::OPTIONAL,
                'Marked has Guide;       "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'discontinued',
                InputArgument::OPTIONAL,
                'Marked Discontinued;    "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'showcorrupted',
                InputArgument::OPTIONAL,
                'Export Corrupted;       "1"/"yes" = Yes, "0"/"no" = No',
                'no'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function getInputsToConfig(): ?array
    {
        $arguments = [];

        foreach (self::$cfg as $key => $cfgVal) {
            $value = $this->cInput->getArgument($key);

            if (is_array($value)) {
                $this->logger->error("Invalid '{$key}' multiple, expecting single value only.");
                return null;
            }

            switch ($key) {
                case 'startdate':
                case 'enddate':
                    $arguments[$key] = (is_null($value) ? $cfgVal : trim($value));
                    if (
                        (!empty($arguments[$key])) &&
                        (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $arguments[$key]))
                    ) {
                        $this->logger->error("Invalid '{$key}', expecting date format YYYY-MM-DD.");
                        return null;
                    }
                    break;

                case 'keycongress':
                case 'oncruise':
                case 'online':
                case 'hasguide':
                case 'discontinued':
                case 'showcorrupted':
                    $arguments[$key] = (is_null($value) ? $cfgVal : trim($value));
                    break;

                case 'city':
                case 'country':
                case 'region':
                case 'specialty':
                    $arguments[$key] = (is_null($value) ? $cfgVal : trim($value));
                    $arguments[$key] = explode(',', $arguments[$key]);
                    $arguments[$key] = array_map('trim', $arguments[$key]);
                    $arguments[$key] = array_filter($arguments[$key]);
                    $arguments[$key] = array_unique($arguments[$key]);
                    $arguments[$key] = array_values($arguments[$key]);
                    break;

                default:
                    $arguments[$key] = $value;
            }
        }

        return $arguments;
    }

    /**
     * {@inheritdoc}
     */
    protected function getHeaders(): array
    {
        $headers = [
            'Id',
            'Name',

            'Start Date',
            'End Date',

            'City',
            'City Guide',
            'State',
            'Country',
            'Region',

            'Previous ID',
            'Unique Name',

            'Is Cruise',
            'Is Online',
            'Is Key Event',
            'Is Has Guide',
            'Is Discontinued',

            'Contact: Details Desc',
            'Contact: Phones',
            'Contact: Faxes',
            'Contact: Emails',
            'Contact: Links',

            'Meta: Created on',
            'Meta: Created by',
            'Meta: Modified on',
            'Meta: Modified by',

            'Specialty Count',
        ];

        $specialty = $this->repo->countMaxSpecialtiesConnection();
        for ($i = 0; $i < $specialty; $i++) {
            $headers[] = 'Specialty ' . ($i + 1);
        }

        return $headers;
    }

    /**
     * {@inheritdoc}
     */
    protected function translateInputToReadable(string $key, $value): string
    {
        switch ($key) {
            case 'region':
                if (!is_array($value)) {
                    $value = [$value];
                }

                foreach ($value as $vi => $val) {
                    if ($region = $this->regionRepo->findOneBy(['id' => $val])) {
                        $value[$vi] = $region->getName() . " (#{$val})";
                    }
                }
                break;

            case 'specialty':
                if (!is_array($value)) {
                    $value = [$value];
                }

                foreach ($value as $vi => $val) {
                    if (is_numeric($val)) {
                        if ($specialty = $this->specialtyRepo->findOneBy(['id' => $val])) {
                            $value[$vi] = $specialty->getSpecialty() . " (#{$val})";
                        }
                    }
                }
                break;
        }

        $value = (is_array($value) ? implode(', ', $value) : "{$value}");

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    protected function convertConferenceToRow(Conference $conference): array
    {
        $country = '';
        if (null !== $conference->getCountry()) {
            $country = $conference->getCountry();
            $country = $country->getName();
        }

        $region = '';
        if (null !== $conference->getRegion()) {
            $region = $conference->getRegion();
            $region = $region->getName();
        }

        $links = $conference->getDetailsContactLinks();
        if (!empty($links)) {
            array_walk($links, function (&$link) {
                $url     = $link['url'] ?? '';
                $caption = $link['caption'] ?? 'Unknown';
                $link    = "[{$caption}]({$url})";
            });
        }

        $row = [
            $conference->getId(),
            $conference->getName(),

            $this->_setDateTimeValue($conference->getStartDate(), 'Y-m-d'),
            $this->_setDateTimeValue($conference->getEndDate(),   'Y-m-d'),

            $conference->getCity(),
            $conference->getCityGuide(),
            $conference->getDetailsState(),
            $country,
            $region,

            $conference->getPreviousId(),
            $conference->getUniqueName(),

            $conference->isCruise()       ? 'Yes' : 'No',
            $conference->isOnline()       ? 'Yes' : 'No',
            $conference->isKeyEvent()     ? 'Yes' : 'No',
            $conference->isHasGuide()     ? 'Yes' : 'No',
            $conference->isDiscontinued() ? 'Yes' : 'No',

            $conference->getDetailsContactDesc(),
            implode('; ', $conference->getDetailsContactPhones()),
            implode('; ', $conference->getDetailsContactFaxes()),
            implode('; ', $conference->getDetailsContactEmails()),
            implode('; ', $links),

            $this->_setDateTimeValue($conference->getDetailsMetaCreatedOn()),
            $conference->getDetailsMetaCreatedBy()  ?? '',
            $this->_setDateTimeValue($conference->getDetailsMetaModifiedOn()),
            $conference->getDetailsMetaModifiedBy() ?? '',
        ];

        $specialties = $conference->getSpecialties();
        $row[] = count($specialties);

        foreach ($specialties as $specialty) {
            $row[] = $specialty->getSpecialty()->getSpecialty();
        }

        return $row;
    }
}
