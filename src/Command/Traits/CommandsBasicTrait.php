<?php

namespace App\Command\Traits;

use App\Utility\Helpers as H;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * App basic Commands general helpers.
 *
 * @since 1.2.0
 */
trait CommandsBasicTrait
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var InputInterface
     */
    protected $cInput;

    /**
     * @var OutputInterface
     */
    protected $cOutput;

    /**
     * Time start record
     *
     * @var int
     */
    private $timedStart = 0;

    /**
     * Start recording Command processing time
     */
    private function _markCommandStarts(): void
    {
        $this->timedStart = time();
    }

    /**
     * Mark the end of Command processing time
     *
     * @param  bool|boolean $completed
     */
    private function _markCommandEnds(bool $completed = false): void
    {
        $this->cOutput->writeln(array('', ''));

        if ($completed) {
            $this->cOutput->writeln('Total Execution Time: ' . H::convertHumanReadbleTime(time(), $this->timedStart));
        }
        else {
            $this->cOutput->writeln('-- But nothing were processed.');
        }

        $this->cOutput->writeln('Command end.');
    }

    /**
     * Method to prepare DrugDb Welcome/Info Screen
     *
     * @param array $sets
     */
    private function _setWelcome($sets): void
    {
        $table = new Table($this->cOutput);

        foreach ($sets as $si => $set) {
            if (0 == $si) {
                $table->setHeaders(array(
                    new TableCell(
                        ' ==> COMMAND: ' . $set,
                        array('colspan' => 2)
                    ),
                ));
            }
            elseif (is_string($set)) {
                $table->addRow(array($set, ''));
            }
            else {
                $table->addRow($set);
            }
        }

        $table->addRow(array('Received', date('Y-m-d H:i:s', $this->timedStart)));
        $table->addRow(array('DrugDb Ver', \App\Service\CoreService::VERSION));

        $this->cOutput->writeln('');
        $table->render();
        $this->cOutput->writeln('');
    }

    /**
     * Method to prepare simple stats table
     *
     * @param array $stats
     */
    private function _showStatsTable($stats): void
    {
        $table = new Table($this->cOutput);

        foreach ($stats as $si => $set) {
            if (
                (0 == $si) &&
                (!is_array($set))
            ) {
                $table->setHeaders([
                    new TableCell(" ==> {$set}", ['colspan' => 2]),
                ]);
            }
            elseif (is_string($set)) {
                $table->addRow([$set, '']);
            }
            else {
                if (
                    (2 == count($set)) &&
                    (is_int($set[1]))
                ) {
                    $set[1] = number_format($set[1]);
                }

                $table->addRow($set);
            }
        }

        $table->addRow([
            'CPU Load',
            implode(' | ', sys_getloadavg())
        ]);

        $table->addRow([
            'Memory Usage / Peak',
            round(memory_get_usage() / 1024 / 1024, 2) . ' MB'
                . ' / '
                . round(memory_get_peak_usage() / 1024 / 1024, 2) . ' MB'
        ]);

        $table->addRow([
            'Time Lapsed',
            H::convertHumanReadbleTime(time(), $this->timedStart)
        ]);

        $this->cOutput->writeln('');
        $table->render();
        $this->cOutput->writeln('');
    }

    /**
     * Method to execute WidgetUpdatesCommand
     */
    private function _executeWidgetUpdatesCommand(): void
    {
        $this->cOutput->writeln(['', 'Executing "update-widgets" command...']);

        $name = (string) \App\Command\WidgetUpdatesCommand::getDefaultName();

        $input = new \Symfony\Component\Console\Input\ArrayInput(['command' => $name]);

        if (null !== $this->getApplication()) {
            $command = $this->getApplication()->find($name);
            $command->run($input, $this->cOutput);
        }
        else {
            $this->cOutput->writeln('Unable to load Application.');
        }

        $this->cOutput->writeln(' ');
    }

    /**
     * Wrap ANSI output to HTML
     *
     * @param string  $output
     *
     * @return string
     */
    private function _ansi2html(string $output): string
    {
        $output = (string) preg_replace('#\\x1b[[][^A-Za-z]*[A-Za-z]#', '', $output);

        return $output;
    }
}
