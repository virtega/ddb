<?php

namespace App\Command\Traits;

use App\Entity\Conference;
use App\Entity\ConferenceSpecialties;
use App\Entity\Country;
use App\Entity\CountryRegions;
use App\Entity\CountryTaxonomy;
use App\Entity\Journal;
use App\Entity\JournalSpecialties;
use App\Entity\SpecialtyTaxonomy;
use App\Exception\CommandSourceImportSanitizerException;

/**
 * App Commands Import Data Sanitizer helpers.
 *
 * @since 1.2.0
 */
trait SourceImportSanitizerCommandsTrait
{
    use \App\Service\Traits\SourceDataConverterServiceTrait;

    /**
     * Sanitizer: Clean simple string
     *
     * @param string $string
     *
     * @return string|null
     */
    private function __trimString(?string $string): ?string
    {
        if (null === $string) {
            return null;
        }

        return trim($string);
    }

    /**
     * Sanitizer: Sanitize simple string
     *
     * @param string $string
     *
     * @return string|null
     */
    private function __safeString(?string $string): ?string
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return null;
        }

        $string = str_replace(['"', '`'], '', $string);

        return $string;
    }

    /**
     * Sanitizer: Convert string to DateTime if any/possible
     *
     * @param string $string
     * @param string $format
     *
     * @return \DateTime|null
     */
    private function __convertDate(?string $string, string $format = 'Y-m-d H:i:s'): ?\DateTime
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return null;
        }

        $value = \DateTime::createFromFormat($format, $string);

        return empty($value) ? null : $value;
    }

    /**
     * Sanitizer: Clean string, convert to known-alias, search Country by ISO CODE 2.
     * If country is not found , it will return null.
     *
     * @param string $string
     *
     * @throws CommandSourceImportSanitizerException
     *
     * @return Country|null
     */
    private function __lookUpCountryIsoCode(?string $string): ?Country
    {
        static $countries;
        if (null === $countries) {
            $countries = [];
        }

        $string = $this->__trimString($string);
        if (!self::___convertInCountryIso($string)) {
            return null;
        }

        if (!isset($countries[$string])) {
            $countryRepo = $this->em->getRepository(Country::class);
            $found = $countryRepo->findOneBy(['isoCode' => $string]);

            if (empty($found)) {
                throw new CommandSourceImportSanitizerException("Invalid country code: '{$string}'");
            }

            $countries[$string] = (empty($found) ? null : $found);
        }

        return $countries[$string];
    }

    /**
     * Sanitizer: Clean string, convert to known-alias, search Country by Name.
     *
     * @param string $string
     *
     * @throws CommandSourceImportSanitizerException
     *
     * @return Country|null
     */
    private function __lookUpCountryName(?string $string): ?Country
    {
        static $countries;
        if (null === $countries) {
            $countries = [];
        }

        $string = $this->__trimString($string);
        if (null === $string) {
            return null;
        }

        if (!self::__convertInCountryName($string)) {
            return null;
        }

        if (!isset($countries[$string])) {
            $countryRepo = $this->em->getRepository(Country::class);
            $found = $countryRepo->findOneBy(['name' => $string]);

            if (empty($found)) {
                throw new CommandSourceImportSanitizerException("Invalid country name: '{$string}'");
            }

            $countries[$string] = (empty($found) ? null : $found);
        }

        return $countries[$string];
    }

    /**
     * Sanitizer: Clean string, try to identify known region.
     * May search & create new CountryTaxonomy & CountryRegions if given arguments met requirement.
     *
     * @param Country|null $country
     * @param string|null  $string
     * @param boolean      $fresh  avoid static cached
     *
     * @throws CommandSourceImportSanitizerException
     *
     * @return CountryTaxonomy|null
     */
    private function __lookUpRegion(?Country $country, ?string $string, bool $fresh = false): ?CountryTaxonomy
    {
        static $knownRegions;
        if (null === $knownRegions) {
            $knownRegions = [];
        }

        $md5 = md5((string) json_encode([$country, $string, ($fresh ? microtime(true) : $fresh)]));
        if (isset($knownRegions[$md5])) {
            // @codeCoverageIgnoreStart
            return $knownRegions[$md5];
            // @codeCoverageIgnoreEnd
        }

        $string = $this->__trimString($string);
        if (empty($string)) {
            $string = null;
        }

        $result = null;
        if (self::__convertInRegionName($string)) {
            $countryTaxRepo = $this->em->getRepository(CountryTaxonomy::class);
            $countryTax = $countryTaxRepo->findOneBy(['name' => $string]);

            if (!empty($countryTax)) {
                $result = $countryTax;

                // create new relation as record
                if (null !== $country) {
                    $countryRegionsRepo = $this->em->getRepository(CountryRegions::class);
                    $found = $countryRegionsRepo->findOneBy([
                        'country' => $country,
                        'region'  => $countryTax
                    ]);

                    if (empty($found)) {
                        $countryRegion = new CountryRegions;
                        $countryRegion
                            ->setCountry($country)
                            ->setRegion($countryTax);

                        $this->em->persist($countryRegion);
                    }
                }
            }
            else {
                throw new CommandSourceImportSanitizerException("Unknown Region name: '{$string}'");
            }
        }

        $knownRegions[$md5] = $result;

        return $result;
    }

    /**
     * Sanitizer: Identify n number if unique IDs from given list.
     *
     * @param int   $count number of expected return array
     * @param array $ids
     *
     * @throws CommandSourceImportSanitizerException
     *
     * @return array[ string|null, ... ]
     */
    private function __lookFilterIds(int $count, array $ids): array
    {
        $result = array_fill(0, $count, null);

        $ids = array_filter($ids);
        $ids = array_map('trim', $ids);
        $ids = array_unique($ids);
        $ids = array_values($ids);

        // this is impossible not happen, see Query. But left here just in case.
        if (empty($ids)) {
            throw new CommandSourceImportSanitizerException('Has no relevant IDs');
        }
        elseif (count($ids) > $count) {
            throw new CommandSourceImportSanitizerException('Has too many IDs: ' . json_encode($ids));
        }
        elseif ($count == count($ids)) {
            $result = $ids;
        }
        else {
            $found = count($ids);
            $result = array_merge($ids, array_fill(($found - 1), ($count - $found), null));
        }

        return $result;
    }

    /**
     * Sanitizer: Converting string-boolean value from String into Int
     *
     * @param string|null $string
     *
     * @return int
     */
    private function __convertIntFromYesString(?string $string): int
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return 0;
        }

        if ('yes' == strtolower($string)) {
            return 1;
        }

        return 0;
    }

    /**
     * Sanitizer: Converting string-Bit "i" value from String into Int
     *
     * @param string|null $string
     *
     * @return int
     */
    private function __convertIntFromBitI(?string $string): int
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return 0;
        }

        if ('i' == strtolower($string)) {
            return 1;
        }

        return 0;
    }

    /**
     * Sanitizer: Converting string-Bit "Y" value from String into Int
     *
     * @param string|null $string
     *
     * @return int
     */
    private function __convertIntFromBitYN(?string $string): int
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return 0;
        }

        if (
            ('y' == strtolower($string)) ||
            ('yes' == strtolower($string))
        ) {
            return 1;
        }

        return 0;
    }

    /**
     * Sanitizer: Converting string-Bit "T" value from String into Boolean
     *
     * @param string|null $string
     *
     * @return boolean
     */
    private function __convertBoolFromBitYN(?string $string): bool
    {
        $string = $this->__convertIntFromBitYN($string);

        return !empty($string);
    }

    /**
     * Sanitizer: Converting Int from Bit value to Int
     *
     * @param int|null $value
     *
     * @return int
     */
    private function __convertIntFromBitInt(?int $value): int
    {
        if (null === $value) {
            return 0;
        }

        return empty($value) ? 0 : 1;
    }

    /**
     * Sanitizer: Convert single string into Conference / Journal Specialty.
     * Look up Specialty, create one if not exits, then add the relation.
     *
     * @param Conference|Journal  $entity
     * @param string|null         $string
     */
    private function __convertSpecialty(object $entity, ?string $string): void
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return;
        }

        $items = explode(';', $string);
        $items = array_map('trim', $items);
        $items = array_filter($items);

        array_walk($items, function(&$specialty) {
            $specialty = strtolower($specialty);
            $specialty = ucwords($specialty);
            $specialty = preg_replace(['/\sAnd\s/', '/\s{2,}/'], [' and ', ' '], $specialty);
        });


        $items = array_unique($items);
        $filtered = [];
        foreach ($items as $item) {
            if (!self::__convertInSpecialtyName($item)) {
                continue;
            }
            $filtered[$item] = true;
        }
        $items = array_keys($filtered);

        foreach ($items as $item) {
            $found = $this->__lookUpSpecialtyCreate((string) $item);

            if ($entity instanceof Conference) {
                $entSpecialty = new ConferenceSpecialties;
                $entSpecialty
                    ->setConference($entity)
                    ->setSpecialty($found)
                ;
                $this->em->persist($entSpecialty);
                $entity->addSpecialty($entSpecialty);
            }
            elseif ($entity instanceof Journal) {
                $entSpecialty = new JournalSpecialties;
                $entSpecialty
                    ->setJournal($entity)
                    ->setSpecialty($found)
                ;
                $this->em->persist($entSpecialty);
                $entity->addSpecialty($entSpecialty);
            }
        }
    }

    /**
     * Sanitizer Helper: Create Specialty if not exits.
     *
     * @param string $string
     *
     * @return SpecialtyTaxonomy
     */
    private function __lookUpSpecialtyCreate(string $string): SpecialtyTaxonomy
    {
        static $knownSpecialty;
        if (null === $knownSpecialty) {
            $knownSpecialty = [];
        }

        if (!isset($knownSpecialty[$string])) {
            $countryRepo = $this->em->getRepository(SpecialtyTaxonomy::class);
            $found = $countryRepo->findOneBy(['specialty' => $string]);

            if (empty($found)) {
                $specialtyTax = new SpecialtyTaxonomy;
                $specialtyTax
                    ->setSpecialty($string)
                ;
                $this->em->persist($specialtyTax);

                $found = $specialtyTax;
            }

            $knownSpecialty[$string] = $found;
        }

        return $knownSpecialty[$string];
    }

    /**
     * Sanitizer: Sanitize Phones and Faxes single input
     *
     * @param string|null $string
     *
     * @return array
     */
    private function __convertPhonesFaxes(?string $string): array
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return [];
        }

        $string = explode("/", $string);
        $string = array_map('trim', $string);
        $string = array_filter($string);
        $string = array_unique($string);
        $string = array_values($string);

        return $string;
    }

    /**
     * Sanitizer: Sanitize Email single input
     *
     * @param string|null $string
     *
     * @return array
     */
    private function __convertEmails(?string $string): array
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return [];
        }

        $string = explode(";", $string);
        $string = array_map('trim', $string);
        $string = array_filter($string);
        $string = array_unique($string);
        $string = array_values($string);

        return $string;
    }

    /**
     * Sanitizer: Sanitize 6 Conference Links on data and pair them
     *
     * @param array $data
     *
     * @return array
     */
    private function __convertConferenceLinks(array $data): array
    {
        $found = [];

        for ($i = 1; $i <= 6; $i++) {
            $caption = "d_CongressLink{$i}";
            $url     = "d_CongressURL{$i}";

            $caption = $this->__trimString( ($data[$caption] ?? 'Unknown') );
            $url     = $this->__trimString( ($data[$url] ?? false) );

            if (!empty($url)) {
                $found[] = [
                    'caption' => $caption,
                    'url'     => $url,
                ];
            }
        }

        return $found;
    }

    /**
     * Convert string status to Journal controlled int
     *
     * @param string $string
     *
     * @return int
     */
    private function __convertJournalStatus(?string $string): int
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return Journal::STATUS_NONE;
        }

        switch (strtolower($string)) {
            case 'active':
                return Journal::STATUS_ACTIVE;

            case 'archive':
                return Journal::STATUS_ARCHIVE;

            default:
                return Journal::STATUS_NONE;
        }
    }

    /**
     * Validate if Journal ISSN, as its indicates it's MedLine; previous logic.
     *
     * @param string $string
     *
     * @return int
     */
    private function __convertJournalIsMedLine(?string $string): int
    {
        $string = $this->__trimString($string);
        if (null === $string) {
            return 0;
        }

        return empty($string) ? 0 : 1;
    }

    /**
     * Clean up value for JR score
     *
     * @param float $float
     *
     * @return float
     */
    private function __sanitizeDecimalValue(?float $float): float
    {
        if (null === $float) {
            $float = 0.00;
        }

        return $float;
    }

    /**
     * Sanitize publisher and publisher's URL.
     * Previous system had URL the on publisher, meanwhile URL is empty.
     *
     * @param string $pub
     * @param string $url
     *
     * @return array[string, string]
     */
    private function __sanitizeJournalPublisher(?string $pub, ?string $url): array
    {
        $pub = $this->__trimString($pub) ?? '';
        $url = $this->__trimString($url) ?? '';

        if (!empty($pub)) {
            preg_match('/https?:\/\//', $pub, $found);
            if (
                (!empty($found)) &&
                (empty($url))
            ) {
                $url = $pub;
                $pub = parse_url($pub, PHP_URL_HOST);
                if (empty($pub)) {
                    // @codeCoverageIgnoreStart
                    $pub = $url;
                    // @codeCoverageIgnoreEnd
                }
            }
        }

        return [$pub, $url];
    }

    /**
     * Journal "d"."LastViewed" is dirty, try to capture date, and separate them
     *
     * @param string $string
     *
     * @return array[\DateTime|null, $string]
     */
    private function __sanitizeJournalLastViewed(?string $string): array
    {
        $result = [null, ''];

        $string = $this->__trimString($string);
        if (null !== $string) {
            $result = [null, $string];

            if (preg_match('/\d{1,2}\/\d{1,2}\/(?:\d{4}|\d{2})/', $string, $found)) {
                $found = end($found);
                $yearFormat = explode('/', $found);
                $yearFormat = (string) end($yearFormat);
                $format = 'm/d/' . (4 == strlen($yearFormat) ? 'Y' : 'y');

                $date = $this->__convertDate($found, $format);

                // found it
                if (null !== $date) {
                    // check for comment
                    $comment = str_replace($found, '', $string);
                    $comment = trim($comment);

                    // more than just date, revert
                    if (!empty($comment)) {
                        $comment = $string;
                    }

                    $result = [$date, $comment];
                }
            }
        }

        return $result;
    }
}
