<?php

namespace App\Command;

use App\Entity\Variable;
use App\Utility\Helpers as H;
use App\Utility\LotusConnection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * $ bin/console app:drugs-sync-down.
 *
 * This class for Source-Sync Command.
 * - All database SQL command made natively.
 * - RE-USE REMOTE ID to sync-down -- Duplicated will be IGNORE.
 * - Additional config is required.
 *
 * @since 1.0.0
 */
class DrugsSyncDownCommand extends Command
{
    use \App\Command\Traits\CommandsBasicTrait;

    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'app:drugs-sync-down';

    /**
     * Command Configurations.
     *
     * @var array
     */
    private $cfg = array(
        'sync-limit'   => 1000,
        'query-limit'  => 1000,
        'processed'    => 0,
        'cycle'        => 1,
        'all'          => false,
        'process-list' => array(
            'experimental' => array(),
            'generic'      => array(),
            'brand'        => array(),
        ),
        'last' => array(
            'experimental' => 0,
            'generic'      => 0,
            'brand'        => 0,
        ),
        'preprocess-count' => array(),
        'complete'         => array(
            'experimental'            => 0,
            'experimental_comments'   => 0,
            'experimental_ignored'    => 0,
            'generic'                 => 0,
            'generic_comments'        => 0,
            'generic_ignored'         => 0,
            'generic_typo'            => 0,
            'generic_typo_ignored'    => 0,
            'generic_synonym'         => 0,
            'generic_synonym_ignored' => 0,
            'generic_ref_level_1'     => 0,
            'generic_ref_level_2'     => 0,
            'generic_ref_level_3'     => 0,
            'generic_ref_level_4'     => 0,
            'brand'                   => 0,
            'brand_comments'          => 0,
            'brand_ignored'           => 0,
            'brand_typo'              => 0,
            'brand_typo_ignored'      => 0,
            'brand_synonym'           => 0,
            'brand_synonym_ignored'   => 0,
        ),
        'interval' => 0,
    );

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LotusConnection
     */
    private $lotusConn;

    /**
     * @var \App\Repository\VariableRepository<Variable>
     */
    private $variableRepo;

    /**
     * Local Database Connection.
     *
     * @var \Doctrine\DBAL\Connection
     */
    private $localDb;

    /**
     * Remote Database Connection.
     *
     * @var \PDO
     */
    private $remoteDb;

    /**
     * @param EntityManagerInterface  $em
     * @param LoggerInterface         $logger
     * @param LotusConnection         $lotusConn
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, LotusConnection $lotusConn)
    {
        $this->em        = $em;
        $this->logger    = $logger;
        $this->lotusConn = $lotusConn;

        $this->variableRepo = $this->em->getRepository(Variable::class);

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Sync Drugs from Source Database.')
            ->addOption(
                'cycle',
                null,
                InputOption::VALUE_OPTIONAL,
                'Process Cycle Count (n X ' . $this->cfg['sync-limit'] . ').',
                1
            )
            ->addOption(
                'all',
                'a',
                InputOption::VALUE_NONE,
                'Flag if like to Sync ALL available Drug, or else will be limited at '
                    . $this->cfg['sync-limit']
                    . ' Drugs record per execution.'
            )
            ->setHelp(
                'Example: Sync 1 Cycle of ' . $this->cfg['sync-limit'] . ' Drugs;' . PHP_EOL
                    . ' - `$ php bin/console ' . self::$defaultName . '`' . PHP_EOL
                    . 'Example: Limit Sync at 5000 (' . $this->cfg['sync-limit'] . ' X 5);' . PHP_EOL
                    . ' - `$ php bin/console ' . self::$defaultName . ' -n 5`' . PHP_EOL
                    . 'Example: Sync all available Drugs;' . PHP_EOL
                    . ' - `$ php bin/console ' . self::$defaultName . ' -a`' . PHP_EOL
            );
    }

    /**
     * {@inheritdoc}
     *
     * @throws \App\Exception\LotusConnectionException If remote connection failed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // setup
        $this->cInput  = $input;
        $this->cOutput = $output;

        $this->cfg['all']   = $this->cInput->getOption('all');
        $this->cfg['cycle'] = $this->cInput->getOption('cycle');

        $this->_markCommandStarts();

        $lastRecordedIds = array();
        foreach ($this->cfg['last'] as $type => $id) {
            $var = $this->variableRepo->getVariable("source-sync-{$type}-last-id");
            if (!is_null($var)) {
                $this->cfg['last'][$type] = $var->getValue();
            }
            $val  = self::_numericalPrint($this->cfg['last'][$type], 7);
            $type = substr($type, 0, 1);
            $type = strtoupper($type);
            $lastRecordedIds[] = "$type:{$val}";
        }
        $lastRecordedIds = implode('; ', $lastRecordedIds);

        $remoteDbInfo = $this->lotusConn->getInfo();
        $this->_setWelcome(array(
            'Sync Down Drugs Record',
            array('Process All Flag',  ($this->cfg['all'] ? 'Yes' : 'No')),
            array('Cycle',             ($this->cfg['all'] ? 'All' : number_format($this->cfg['cycle']))),
            array('Limit',             ($this->cfg['all'] ? 'All which available' : number_format(($this->cfg['cycle'] * $this->cfg['sync-limit']), 0))),
            array('DB Host',           ($remoteDbInfo['host'] ? $remoteDbInfo['host'] : 'null')),
            array('DB Port',           ($remoteDbInfo['port'] ? $remoteDbInfo['port'] : 'null')),
            array('DB Name',           ($remoteDbInfo['dbname'] ? $remoteDbInfo['dbname'] : 'null')),
            array('DB User',           ($remoteDbInfo['username'] ? $remoteDbInfo['username'] : 'null')),
            array('DB Password',       ($remoteDbInfo['password'] ? $remoteDbInfo['password'] : 'null')),
            array('Last Recorded IDs', $lastRecordedIds),
        ));

        $this->cOutput->writeln(array('', '>> Connecting Remote Database...'));

        // make connection, and will throw
        $this->lotusConn->makeConnection(true);

        // connection ok
        $this->remoteDb = $this->lotusConn->getConnection();
        $this->cOutput->writeln('-- Database Connection OK.');

        $this->localDb = $this->em->getConnection();

        $this->logger->info('COMMAND: Source Sync Down Request Received.', array(
            'method' => __METHOD__,
            'config' => $this->cfg,
        ));

        foreach ($this->cfg['complete'] as $type => $count) {
            $this->cfg['preprocess-count'][$type] = 0;
            $type = str_replace('_', '-', $type);
            $var  = $this->variableRepo->getVariable("widget-{$type}-count");
            if (!is_null($var)) {
                $this->cfg['preprocess-count'][$type] = $var->getValue();
            }
        }

        $this->processing();

        $processed = false;
        if (!empty(array_sum($this->cfg['complete']))) {
            // update widgets
            $this->_executeWidgetUpdatesCommand();

            // show process results
            $this->cOutput->writeln(array('', '-- Processed Result:'));
            $table   = new Table($this->cOutput);
            $headers = array(
                'name'    => 'Drug Type',
                'pre'     => 'Pre-Sync Count',
                'sync'    => 'Synced Count',
                'comment' => 'Comment Count',
                'ignored' => 'Ignored Count',
            );
            $table->setHeaders($headers);
            $headers = array_map('strlen', $headers);
            $rows    = array();
            foreach ($this->cfg['complete'] as $type => $count) {
                $type = (string) $type;

                if (false !== strpos($type, '_ignored')) {
                    $type_x = str_replace('_ignored', '', $type);
                    if ($rows[$type_x]) {
                        $rows[$type_x]['ignored'] = self::_numericalPrint($count, $headers['ignored']);
                    }
                }
                elseif (false !== strpos($type, '_comments')) {
                    $type_x = str_replace('_comments', '', $type);
                    $rows[$type_x]['comment'] = self::_numericalPrint($count, $headers['ignored']);
                }
                else {
                    $type_n = str_replace('_', '-', $type);
                    $type_s = str_replace('-', ' ', $type_n);
                    $type_s = ucwords($type_s);
                    $type_d = (!empty($this->cfg['preprocess-count'][$type_n]) ? $this->cfg['preprocess-count'][$type_n] : 0);
                    $rows[$type] = array(
                        'name'    => $type_s,
                        'pre'     => self::_numericalPrint($type_d, $headers['pre']),
                        'sync'    => self::_numericalPrint($count, $headers['sync']),
                        'comment' => '',
                        'ignored' => '',
                    );
                }
            }
            $table->setRows($rows);
            $table->render();

            $processed = true;
        }

        $this->_markCommandEnds($processed);
    }

    /**
     * Method which kick off processing Sync Down
     * - First Create List Map, to which brand to be Process
     * - List map only contains IDs
     * - Once ready with all 3 Types, Trigger Query & Storage Method.
     */
    private function processing(): void
    {
        $cycle_counter = 0;
        $pl_base       = $this->cfg['process-list'];

        $this->_processingGenericRefLevels();

        while (($this->cfg['all']) || ($cycle_counter < $this->cfg['cycle'])) {
            $interval = 0;
            if (!empty($this->cfg['interval'])) {
                $interval = (time() - $this->cfg['interval']);
                $interval = ($interval < 0 ? 0 : $interval);
            }

            $this->cOutput->writeln(array(
                '',
                '',
                '>> Cycle: '
                    . number_format(($cycle_counter + 1), 0)
                    . ', Elapsed: '
                    . H::convertHumanReadbleTime(time(), $this->timedStart)
                    . (empty($this->cfg['interval']) ? '' : ', <comment>Interval: ' . number_format($interval, 0) . 's</comment>'),
            ));
            $this->cfg['interval'] = time();

            $this->cfg['processed']    = 0;
            $this->cfg['process-list'] = $pl_base;
            $this->cfg['query-limit']  = $this->cfg['sync-limit'];

            $progress = new ProgressBar($this->cOutput, 24);

            $this->_getListForExperimental();
            $progress->advance();

            $this->_getListForGeneric();
            $progress->advance();

            $this->_getListForBrand();
            $progress->advance();

            // if there any to process
            $s_count = $m_count = (array) array_combine(
                array_keys($this->cfg['process-list']),
                array_fill(0, count($this->cfg['process-list']), 0)
            );

            if ($this->cfg['processed']) {
                foreach ($this->cfg['process-list'] as $type => $set) {
                    $count = array_keys($this->cfg['process-list'][$type]);
                    $m_count[$type] = count($count);
                    $count = array_keys($set);
                    $s_count[$type] = count($count);
                    $s_count[$type] -= $m_count[$type];
                    if (!empty($m_count[$type])) {
                        $func = '_processingMap' . ucwords($type);
                        $this->$func($this->cfg['process-list'][$type], $progress);
                    }
                    $progress->advance();
                }
            }
            elseif ($this->cfg['all']) {
                // no more return results; stop the cycle
                $this->cfg['all'] = false;
            }

            $this->logger->info('source-sync-processed', array(
                'method' => __METHOD__,
                'what'   => array(
                    'main' => $m_count,
                    'subs' => $s_count,
                    'last' => $this->cfg['last'],
                ),
            ));

            ++$cycle_counter;
            $progress->finish();

            $table = new Table($this->cOutput);

            $table->setHeaders(array_merge(
                array('Processed'),
                array_map('ucwords', array_keys($this->cfg['process-list']))
            ));

            $table->addRow(array_merge(
                array('Main Drugs'),
                array(
                    self::_numericalPrint($m_count['experimental']),
                    self::_numericalPrint($m_count['generic']),
                    self::_numericalPrint($m_count['brand']),
                )
            ));

            $table->addRow(array_merge(
                array('Sub Drugs'),
                array(
                    self::_numericalPrint($s_count['experimental']),
                    self::_numericalPrint($s_count['generic']),
                    self::_numericalPrint($s_count['brand']),
                )
            ));

            $table->addRow(array_merge(
                array('> Latest Last IDs'),
                array(
                    self::_numericalPrint($this->cfg['last']['experimental']),
                    self::_numericalPrint($this->cfg['last']['generic']),
                    self::_numericalPrint($this->cfg['last']['brand']),
                )
            ));

            $this->cOutput->writeln('');
            $table->render();
        }
    }

    /**
     * Query & Store Experimental Drugs
     * - Single Drug, no subset
     * - Just query all, and One Large Insert Directly.
     *
     * @param array        $set       key is ID, expect no Subset; empty array value
     * @param ProgressBar  $progress  Output Object for Progress Bar (expect 3 advances only)
     *
     * @throws \Exception  If Remote query rejected
     */
    private function _processingMapExperimental(array $set, ProgressBar $progress): void
    {
        // query build
        $set   = array_keys($set);
        $query = 'SELECT
            "E"."ExperimentalID",
            "E"."ExperimentalName",
            "E"."ExperimentalUNID",
            "C"."Comments"
        FROM
            "dbo"."Experimental" "E"
            LEFT JOIN (
                (
                    SELECT
                        DISTINCT("Comments"),
                        "BrandName" as "ExperimentalName"
                    FROM
                        "dbo"."NewDrug"
                    WHERE
                        "Experimental" = 1
                        AND
                        "Comments" IS NOT null
                        AND
                        "DrugType" = \'Brand\'
                )
                UNION ALL
                (
                    SELECT
                        DISTINCT("Comments"),
                        "GenericName" AS "ExperimentalName"
                    FROM
                        "dbo"."NewDrug"
                    WHERE
                        "Experimental" = 1
                        AND
                        "Comments" IS NOT null
                        AND
                        "DrugType" = \'Generic\'
                        AND
                        "BrandUNID" IS null
                )
            ) "C" ON "C"."ExperimentalName" = "E"."ExperimentalName"
        WHERE
            "E"."ExperimentalID" IN  (' . implode(',', $set) . ')
        ORDER BY
            "E"."ExperimentalID" ASC
        ;';
        $query = H::removeExcessWhitespace($query);

        // query check
        $query = $this->remoteDb->query($query);
        if (empty($query)) {
            // @codeCoverageIgnoreStart
            $error = $this->remoteDb->errorInfo();
            $error = json_encode($error);
            throw new \Exception("REMOTE DB QUERY ERROR, Process Halted. ({$error}).", 1);
            // @codeCoverageIgnoreEnd
        }

        // extract & build insert
        $results = $query->fetchAll(\PDO::FETCH_ASSOC);
        $progress->advance();

        $inserts = array();
        if (!empty($results)) {
            foreach ($results as $row) {
                // comment
                $comments = trim($row['Comments']);
                if (!empty($comments)) {
                    // @codeCoverageIgnoreStart
                    // For now, Staging did not have any Comment
                    $comments = $this->localDb->quote($comments, \PDO::PARAM_STR);
                    ++$this->cfg['complete']['experimental_comments'];
                    // @codeCoverageIgnoreEnd
                }
                else {
                    $comments = 'null';
                }

                // build insert
                $tmp = array(
                    'id'       => $row['ExperimentalID'],
                    'name'     => $this->localDb->quote(trim($row['ExperimentalName']), \PDO::PARAM_STR),
                    'type'     => $this->localDb->quote('experimental', \PDO::PARAM_STR),
                    'valid'    => 0,
                    'uid'      => $this->localDb->quote(trim($row['ExperimentalUNID']), \PDO::PARAM_STR),
                    'comments' => $comments,
                );
                $tmp       = implode(',', $tmp);
                $inserts[] = "({$tmp})";
            }
        }
        $progress->advance();

        // insert
        if (!empty($inserts)) {
            // query
            $inserts = 'INSERT IGNORE INTO `experimental` ('
                . implode(', ', array(
                    '`id`',
                    '`name`',
                    '`type`',
                    '`valid`',
                    '`uid`',
                    '`comments`',
                ))
                . ') VALUES '
                . implode(', ', $inserts);
            $statement = $this->localDb->prepare($inserts);

            // push
            if ($statement->execute()) {
                // done; get last IDs
                $this->cfg['last']['experimental'] = max(array(
                    $this->cfg['last']['experimental'],
                    max($set),
                ));
                $this->variableRepo->updateWidgetValue(
                    'source-sync-experimental-last-id',
                    $this->cfg['last']['experimental'],
                    'integer',
                    true
                );

                // record total
                $set_count = count($set);
                $this->cfg['complete']['experimental'] += $set_count;

                // get warning - ignored duplicates count
                $executed = $statement->rowCount();
                $this->cfg['complete']['experimental_ignored'] += ($set_count - $executed);
            }
        }
        $progress->advance();
    }

    /**
     * Query & Store Generic Drugs.
     *
     * @param array        $set       Key is ID, expect no Subset; empty array value
     * @param ProgressBar  $progress  Output Object for Progress Bar (expect 3 advances only)
     *
     * @throws \Exception  If Remote query rejected
     */
    private function _processingMapGeneric(array $set, ProgressBar $progress): void
    {
        // prep query
        $drug_ids = array_keys($set);
        $query    = '
            SELECT
                "G"."GenericID",
                "G"."GenericName",
                "G"."GenericUNID",
                "G"."NoDoubleBounce",
                "G"."AutoEncodeExclude",
                "G"."Level1",
                "G"."Level2",
                "G"."Level3",
                "G"."Level4",
                "G"."Level5",
                "C"."Comments"
            FROM
                "dbo"."Generic" "G"
                LEFT JOIN (
                    SELECT
                        DISTINCT("Comments"),
                        "GenericUNID"
                    FROM
                        "dbo"."NewDrug"
                    WHERE
                        "DrugType" = \'Generic\'
                        AND
                        "Experimental" IS null
                        AND
                        "Comments" IS NOT null
                ) "C" ON "C"."GenericUNID" = "G"."GenericUNID"
            WHERE
                "G"."GenericID" IN (' . implode(',', $drug_ids) . ')
            ORDER BY
                "G"."GenericID" ASC
        ;';
        $query = H::removeExcessWhitespace($query);

        $query = $this->remoteDb->query($query);
        if (empty($query)) {
            // @codeCoverageIgnoreStart
            $error = $this->remoteDb->errorInfo();
            $error = json_encode($error);
            throw new \Exception("REMOTE DB QUERY ERROR, Process Halted. ({$error}).", 1);
            // @codeCoverageIgnoreEnd
        }

        // extract & build insert
        $results = $query->fetchAll(\PDO::FETCH_ASSOC);
        $progress->advance();

        $brand_relations = array();
        $inserts         = array();

        if (!empty($results)) {
            foreach ($results as $row) {
                // comment
                $comments = trim($row['Comments']);
                if (!empty($comments)) {
                    // @codeCoverageIgnoreStart
                    // For now, Staging did not have any Comment
                    $comments = $this->localDb->quote($comments, \PDO::PARAM_STR);
                    ++$this->cfg['complete']['generic_comments'];
                    // @codeCoverageIgnoreEnd
                }
                else {
                    $comments = 'null';
                }

                // build insert
                $tmp = array(
                    'id'                => $row['GenericID'],
                    'experimental'      => 'null',
                    'name'              => $this->localDb->quote(trim($row['GenericName']), \PDO::PARAM_STR),
                    'valid'             => 0,
                    'Level1'            => $this->localDb->quote(trim($row['Level1']), \PDO::PARAM_STR),
                    'Level2'            => $this->localDb->quote(trim($row['Level2']), \PDO::PARAM_STR),
                    'Level3'            => $this->localDb->quote(trim($row['Level3']), \PDO::PARAM_STR),
                    'Level4'            => $this->localDb->quote(trim($row['Level4']), \PDO::PARAM_STR),
                    'Level5'            => $this->localDb->quote(trim($row['Level5']), \PDO::PARAM_STR),
                    'NoDoubleBounce'    => $this->localDb->quote(($row['NoDoubleBounce'] ? trim($row['NoDoubleBounce']) : 0), \PDO::PARAM_INT),
                    'AutoEncodeExclude' => $this->localDb->quote(($row['AutoEncodeExclude'] ? trim($row['AutoEncodeExclude']) : 0), \PDO::PARAM_INT),
                    'uid'               => $this->localDb->quote(trim($row['GenericUNID']), \PDO::PARAM_STR),
                    'comments'          => $comments,
                );
                $tmp       = implode(',', $tmp);
                $inserts[] = "({$tmp})";

                // register generic
                if (!empty($set[$row['GenericID']])) {
                    foreach ($set[$row['GenericID']] as $brand_id) {
                        // get Brands out
                        $brand_relations[$brand_id] = $row['GenericID'];
                        unset($this->cfg['process-list']['brand'][$brand_id]);
                    }
                }
            }
            // re-count map
            $this->_updateProcessListRecount();
        }
        $progress->advance();

        // insert: stop if nothing to inserts
        if (empty($inserts)) {
            // This is more as failed safe counter measure
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }

        // query
        $inserts = 'INSERT IGNORE INTO `generic` ('
            . implode(', ', array(
                '`id`',
                '`experimental`',
                '`name`',
                '`valid`',
                '`Level1`',
                '`Level2`',
                '`Level3`',
                '`Level4`',
                '`Level5`',
                '`NoDoubleBounce`',
                '`AutoEncodeExclude`',
                '`uid`',
                '`comments`',
            ))
            . ') VALUES '
            . implode(', ', $inserts);
        $statement = $this->localDb->prepare($inserts);

        // push
        if ($statement->execute()) {
            // done; get last IDs
            $this->cfg['last']['generic'] = max(array(
                $this->cfg['last']['generic'],
                max($drug_ids),
            ));
            $this->variableRepo->updateWidgetValue(
                'source-sync-generic-last-id',
                $this->cfg['last']['generic'],
                'integer',
                true
            );

            // record total
            $set_count = count($drug_ids);
            $this->cfg['complete']['generic'] += $set_count;

            // get warning - ignored duplicates count
            $executed = $statement->rowCount();
            $this->cfg['complete']['generic_ignored'] += ($set_count - $executed);
        }

        // push brands drug
        if (!empty($brand_relations)) {
            $this->_processingMapBrand($brand_relations, $progress, true);
        }

        // clean-up
        unset($inserts);
        unset($statement);
        unset($brand_relations);
        $progress->advance();

        // ::DRUG Typo
        $this->_processingDrugKeywords('typo', 'generic', $drug_ids, $progress);

        // ::DRUG Synonym
        $this->_processingDrugKeywords('synonym', 'generic', $drug_ids, $progress);
    }

    /**
     * Query & Store Brand Drugs.
     *
     * @param array        $set       key is Origin IDs, while value to hold INT of Generic ID
     * @param ProgressBar  $progress  Output Object for Progress Bar (expect 3 advances only)
     * @param bool         $skip_max  Flag if to avoid update Last ID
     *
     * @throws \Exception  If Remote query rejected
     */
    private function _processingMapBrand(array $set, ProgressBar $progress, $skip_max = false): void
    {
        // prep query
        $drug_ids = array_keys($set);
        $query    = '
            SELECT
                "B"."BrandID",
                "B"."BrandName",
                "B"."AutoEncodeExclude",
                "B"."BrandUNID",
                "C"."Comments"
            FROM
                "dbo"."Brand" "B"
                LEFT JOIN (
                    SELECT
                        DISTINCT("Comments"),
                        "BrandUNID"
                    FROM
                        "dbo"."NewDrug"
                    WHERE
                        "DrugType" = \'Brand\'
                        AND
                        "Experimental" IS null
                        AND
                        "Comments" IS NOT null
                ) "C" ON "C"."BrandUNID" = "B"."BrandUNID"
            WHERE
                "B"."BrandID" IN (' . implode(',', $drug_ids) . ')
            ORDER BY
                "B"."BrandID" ASC
        ;';
        $query = H::removeExcessWhitespace($query);

        // query check
        $query = $this->remoteDb->query($query);
        if (empty($query)) {
            // @codeCoverageIgnoreStart
            $error = $this->remoteDb->errorInfo();
            $error = json_encode($error);
            throw new \Exception("REMOTE DB QUERY ERROR, Process Halted. ({$error}).", 1);
            // @codeCoverageIgnoreEnd
        }

        // extract & build insert
        $results = $query->fetchAll(\PDO::FETCH_ASSOC);
        $progress->advance();

        // extract & build insert
        $inserts = array();
        if (!empty($results)) {
            foreach ($results as $row) {
                // get related generic id
                $generic_id = 0;
                if (!empty($set[$row['BrandID']])) {
                    $generic_id = $set[$row['BrandID']];
                }

                // comment
                $comments = trim($row['Comments']);
                if (!empty($comments)) {
                    // @codeCoverageIgnoreStart
                    // For now, Staging did not have any Comment
                    $comments = $this->localDb->quote($comments, \PDO::PARAM_STR);
                    ++$this->cfg['complete']['brand_comments'];
                    // @codeCoverageIgnoreEnd
                }
                else {
                    $comments = 'null';
                }

                // build insert
                $tmp = array(
                    'id'                => $row['BrandID'],
                    'generic'           => (empty($generic_id) ? 'null' : $this->localDb->quote($generic_id, \PDO::PARAM_INT)),
                    'name'              => $this->localDb->quote(trim($row['BrandName']), \PDO::PARAM_STR),
                    'valid'             => 0,
                    'AutoEncodeExclude' => $this->localDb->quote(($row['AutoEncodeExclude'] ? trim($row['AutoEncodeExclude']) : 0), \PDO::PARAM_INT),
                    'uid'               => $this->localDb->quote(trim($row['BrandUNID']), \PDO::PARAM_STR),
                    'comments'          => $comments,
                );
                $tmp       = implode(',', $tmp);
                $inserts[] = "({$tmp})";
            }
        }
        $progress->advance();

        // insert: stop if nothing to inserts
        if (empty($inserts)) {
            // This is more as failed safe counter measure
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }

        // query
        $inserts = 'INSERT IGNORE INTO `brand` ('
            . implode(', ', array(
                '`id`',
                '`generic`',
                '`name`',
                '`valid`',
                '`AutoEncodeExclude`',
                '`uid`',
                '`comments`',
            ))
            . ') VALUES '
            . implode(', ', $inserts);
        $statement = $this->localDb->prepare($inserts);

        // push
        if ($statement->execute()) {
            // note that, Since this is Sub-Drug to Generic, we only store ID of last Orphan ID.
            if (!$skip_max) {
                // done; get last IDs
                $this->cfg['last']['brand'] = max(array(
                    $this->cfg['last']['brand'],
                    max($drug_ids),
                ));
                $this->variableRepo->updateWidgetValue(
                    'source-sync-brand-last-id',
                    $this->cfg['last']['brand'],
                    'integer',
                    true
                );
            }

            // record total
            $set_count = count($drug_ids);
            $this->cfg['complete']['brand'] += $set_count;

            // get warning - ignored duplicates count
            $executed = $statement->rowCount();
            $this->cfg['complete']['brand_ignored'] += ($set_count - $executed);
        }

        // clean-up
        unset($inserts);
        unset($statement);
        unset($generics);
        $progress->advance();

        // ::DRUG Typo
        $this->_processingDrugKeywords('typo', 'brand', $drug_ids, $progress);

        // ::DRUG Synonym
        $this->_processingDrugKeywords('synonym', 'brand', $drug_ids, $progress);
    }

    /**
     * Method to Sync either Typo or Synonym of either Generic and Brand Drug.
     *
     * @param string       $type      expect "typo" or "synonym"
     * @param string       $drug      expect "generic" or "brand"
     * @param array        $drug_ids  Drug Ids
     * @param ProgressBar  $progress  Progress Object
     *
     * @throws \Exception  If Remote query rejected
     */
    private function _processingDrugKeywords(string $type, string $drug, array $drug_ids, ProgressBar $progress): void
    {
        $local_db  = $drug . '_' . $type;
        $remote_fd = ucwords($drug) . 'ID';
        $remote_ty = ucwords($type);
        $remote_db = ucwords($drug) . $remote_ty . 's';

        // prep query
        $query = 'SELECT '
            . ' * '
            . ' FROM '
            . ' "dbo"."' . $remote_db . '" "T" '
            . ' WHERE "T"."' . $remote_fd . '" IN (' . implode(',', $drug_ids) . ')'
            . ' ORDER BY '
            . ' "T"."' . $remote_fd . '" ASC '
            . ';';
        $query = H::removeExcessWhitespace($query);

        // query check
        $query = $this->remoteDb->query($query);
        if (empty($query)) {
            // @codeCoverageIgnoreStart
            $error = $this->remoteDb->errorInfo();
            $error = json_encode($error);
            throw new \Exception("REMOTE DB QUERY ERROR, Process Halted. ({$error}).", 1);
            // @codeCoverageIgnoreEnd
        }

        // extract & build insert
        $results = $query->fetchAll(\PDO::FETCH_ASSOC);
        $progress->advance();

        $inserts = array();
        if (!empty($results)) {
            // query local keywords - for ignore
            // remote did no have any ID
            $local_query = "SELECT `id`, `{$drug}`, `name` FROM `{$local_db}` WHERE `{$drug}` IN (" . implode(',', $drug_ids) . ');';
            $local_query = $this->localDb->prepare($local_query);
            $local_query->execute();
            $local_query   = $local_query->fetchAll();
            $local_records = array();
            foreach ($local_query as $local) {
                if (!isset($local_records[$local[$drug]])) {
                    $local_records[$local[$drug]] = array();
                }
                $local_records[$local[$drug]][$local['id']] = $local;
            }
            unset($local_query);

            // build insert
            foreach ($results as $result) {
                $local_id = 'null';
                if (!empty($local_records[$result[$remote_fd]])) {
                    // stored locally
                    foreach ($local_records[$result[$remote_fd]] as $local_set) {
                        // still have the same name
                        if ($local_set['name'] == trim($result[$remote_ty])) {
                            // same
                            $local_id = $local_set['id'];
                            // minimizing
                            unset($local_records[$result[$remote_fd]][$local_id]);
                            break;
                        }
                    }
                }

                // build insert
                $tmp = array(
                    'id'   => $local_id,
                    $drug  => $this->localDb->quote($result[$remote_fd], \PDO::PARAM_INT),
                    'name' => $this->localDb->quote(trim($result[$remote_ty]), \PDO::PARAM_STR),
                );
                $tmp       = implode(',', $tmp);
                $inserts[] = "({$tmp})";
            }
        }
        $progress->advance();

        if (!empty($inserts)) {
            $count = count($inserts);

            $inserts = 'INSERT IGNORE INTO `' . $local_db . '` ('
                . implode(', ', array(
                    '`id`',
                    '`' . $drug . '`',
                    '`name`',
                ))
                . ') VALUES '
                . implode(', ', $inserts);

            $statement = $this->localDb->prepare($inserts);

            if ($statement->execute()) {
                $this->cfg['complete'][$local_db] += $count;

                $executed = $statement->rowCount();
                $this->cfg['complete']["{$drug}_{$type}_ignored"] += ($count - $executed);
            }
        }
        $progress->advance();
    }

    /**
     * Query List of Experimental Drugs
     * - List of IDs based on last run, with capped limit check.
     * - No Subset/Relation
     * - Last ID Stored in Option; exact last ID.
     *
     * @throws \Exception  If Remote query rejected
     */
    private function _getListForExperimental(): void
    {
        // limit hit
        if ($this->cfg['query-limit'] < 1) {
            // This is more as failed safe counter measure
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }

        // prep query - NO RELATION
        $query = 'SELECT TOP '
            . $this->cfg['query-limit']
            . ' "E"."ExperimentalID" AS "ExperimentalID" '
            . ' FROM '
            . ' "dbo"."Experimental" "E" '
            . ' WHERE "E"."ExperimentalID" > ' . $this->cfg['last']['experimental']
            . ' ORDER BY '
            . ' "E"."ExperimentalID" ASC '
            . ';';
        $query = H::removeExcessWhitespace($query);

        $query = $this->remoteDb->query($query);
        if (empty($query)) {
            // @codeCoverageIgnoreStart
            $error = $this->remoteDb->errorInfo();
            $error = json_encode($error);
            throw new \Exception("REMOTE DB QUERY ERROR, Process Halted. ({$error}).", 1);
            // @codeCoverageIgnoreEnd
        }

        $results = $query->fetchAll(\PDO::FETCH_ASSOC);

        if (!empty($results)) {
            foreach ($results as $row) {
                // no relationship, so downline
                $this->cfg['process-list']['experimental'][$row['ExperimentalID']] = array();
            }

            // re-count map
            $this->_updateProcessListRecount();
        }
    }

    /**
     * Query List of Generic Drugs
     * - Query from Relation First, Then
     * - Query those are Orphans
     * - Last ID Stored in Option; exact last ID.
     *
     * @throws \Exception  If Remote query rejected
     */
    private function _getListForGeneric(): void
    {
        // limit hit
        if ($this->cfg['query-limit'] < 1) {
            // This is more as failed safe counter measure
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }

        // prep query - WITH/OUT RELATION
        $query = 'SELECT TOP '
            . $this->cfg['query-limit']
            . ' "G"."GenericID" AS "GenericID", "B"."BrandID" AS "BrandID" '
            . ' FROM '
            . ' "dbo"."Generic" "G" '
            . ' LEFT JOIN "dbo"."BrandGeneric" "B" ON "B"."GenericID" = "G"."GenericID" '
            . ' WHERE "G"."GenericID" > ' . $this->cfg['last']['generic']
            . ' ORDER BY '
            . ' "G"."GenericID" ASC, '
            . ' "B"."BrandID" ASC '
            . ';';
        $query = H::removeExcessWhitespace($query);

        $query = $this->remoteDb->query($query);
        if (empty($query)) {
            // @codeCoverageIgnoreStart
            $error = $this->remoteDb->errorInfo();
            $error = json_encode($error);
            throw new \Exception("REMOTE DB QUERY ERROR, Process Halted. ({$error}).", 1);
            // @codeCoverageIgnoreEnd
        }

        $results = $query->fetchAll(\PDO::FETCH_ASSOC);

        if (!empty($results)) {
            $last_processed_generic = 0;
            foreach ($results as $row) {
                $last_processed_generic = $row['GenericID'];
                if (!isset($this->cfg['process-list']['generic'][$row['GenericID']])) {
                    $this->cfg['process-list']['generic'][$row['GenericID']] = array();
                }
                if (!empty($row['BrandID'])) {
                    // apply to relation
                    $this->cfg['process-list']['generic'][$row['GenericID']][] = $row['BrandID'];
                    // create non-relation
                    $this->cfg['process-list']['brand'][$row['BrandID']] = array();
                }
            }

            // if last result, ignore; the SQL LIMIT might have CUT OFF Brand full-list
            if (
                (count($results) > 1) &&
                (!empty($last_processed_generic)) &&
                (!empty($this->cfg['process-list']['generic'][$last_processed_generic]))
            ) {
                foreach ($this->cfg['process-list']['generic'][$last_processed_generic] as $brand) {
                    unset($this->cfg['process-list']['brand'][$brand]);
                }
                unset($this->cfg['process-list']['generic'][$last_processed_generic]);
            }

            // re-count map
            $this->_updateProcessListRecount();
        }
    }

    /**
     * Query List of Brand Drugs
     * - Query Orphan Brands only
     * - Last ID Stored in Option; Orphan Last ID only.
     *
     * @throws \Exception  If Remote query rejected
     */
    private function _getListForBrand(): void
    {
        // limit hit
        if ($this->cfg['query-limit'] < 1) {
            // This is more as failed safe counter measure
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }

        // prep query - ORPHANS ONLY
        $query = 'SELECT TOP '
            . $this->cfg['query-limit']
            . ' "B"."BrandID" AS "BrandID" '
            . ' FROM '
            . ' "dbo"."Brand" "B" '
            . ' WHERE '
            . ' "B"."BrandID" > ' . $this->cfg['last']['brand']
            . ' AND '
            . ' "B"."BrandID" NOT IN ( '
            . ' SELECT DISTINCT "BrandID" FROM "dbo"."BrandGeneric" '
            . ' ) '
            . ' ORDER BY '
            . ' "B"."BrandID" ASC '
            . ';';
        $query = H::removeExcessWhitespace($query);

        $query = $this->remoteDb->query($query);
        if (empty($query)) {
            // @codeCoverageIgnoreStart
            $error = $this->remoteDb->errorInfo();
            $error = json_encode($error);
            throw new \Exception("REMOTE DB QUERY ERROR, Process Halted. ({$error}).", 1);
            // @codeCoverageIgnoreEnd
        }

        $results = $query->fetchAll(\PDO::FETCH_ASSOC);

        if (!empty($results)) {
            foreach ($results as $row) {
                // no relationship, so downline
                $this->cfg['process-list']['brand'][$row['BrandID']] = array();
            }

            // re-count map
            $this->_updateProcessListRecount();
        }
    }

    /**
     * Method to recalculate in Processing Map List.
     */
    private function _updateProcessListRecount(): void
    {
        $this->cfg['processed'] = 0;

        foreach ($this->cfg['process-list'] as $type => $ids) {
            $ids = array_keys($ids);
            $this->cfg['processed'] += count($ids);
        }

        $this->cfg['query-limit'] = ($this->cfg['sync-limit'] - $this->cfg['processed']);
    }

    /**
     * Helper Method to Sync Generic Drug Level Attributes.
     *
     * @throws \Exception  If Remote query rejected
     */
    private function _processingGenericRefLevels(): void
    {
        $this->cOutput->writeln(array('', 'Generic References Sync...'));

        foreach (array(1, 2, 3, 4) as $curr_level) {
            // prep query - NO RELATION
            $query = 'SELECT '
                . ' "Code", "Name" '
                . ' FROM '
                . ' "dbo"."refLevel' . $curr_level . '" '
                . ' ORDER BY '
                . ' "Code" ASC '
                . ';';
            $query = H::removeExcessWhitespace($query);

            $query = $this->remoteDb->query($query);
            if (empty($query)) {
                // @codeCoverageIgnoreStart
                $error = $this->remoteDb->errorInfo();
                $error = json_encode($error);
                throw new \Exception("REMOTE DB QUERY ERROR, Process Halted. ({$error}).", 1);
                // @codeCoverageIgnoreEnd
            }

            $results = $query->fetchAll(\PDO::FETCH_ASSOC);

            $this->cOutput->writeln(">> Update Ref Lvl {$curr_level}...");

            $inserts = array();
            if (!empty($results)) {
                foreach ($results as $row) {
                    $tmp = array(
                        'code' => $this->localDb->quote($row['Code'], \PDO::PARAM_STR),
                        'name' => $this->localDb->quote($row['Name'], \PDO::PARAM_STR),
                    );
                    $tmp       = implode(',', $tmp);
                    $inserts[] = "({$tmp})";
                }
            }

            // insert into local
            if (!empty($inserts)) {
                // query
                $inserts = "INSERT INTO `generic_ref_level{$curr_level}` (`code`, `name`)"
                    . ' VALUES '
                    . implode(', ', $inserts)
                    . ' ON DUPLICATE KEY UPDATE `code`=VALUES(`code`), `name` = VALUES(`name`)';
                $statement = $this->localDb->prepare($inserts);

                if ($statement->execute()) {
                    // update count
                    $this->cfg['complete']["generic_ref_level_{$curr_level}"] = $statement->rowCount();
                }
            }
        }
    }

    /**
     * Method to return clean up version of numerical value.
     *
     * @param int      $value   the value
     * @param int      $length  whitespace length
     *
     * @return string
     */
    private static function _numericalPrint(int $value, int $length = 12): string
    {
        $value = $value;
        $value = number_format($value, 0);

        return str_pad($value, $length, ' ', STR_PAD_LEFT);
    }
}
