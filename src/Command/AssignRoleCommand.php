<?php

namespace App\Command;

use App\Entity\UserRoles;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * $ bin/console app:assign-role
 *
 * Assign User Role with LDAP username.
 *
 * @since 1.0.0
 */
class AssignRoleCommand extends Command
{
    use \App\Command\Traits\CommandsBasicTrait;

    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'app:assign-role';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \App\Repository\UserRolesRepository<UserRoles>
     */
    private $userRolesRepo;

    /**
     * @var array
     */
    private $roles;

    /**
     * @param EntityManagerInterface  $em
     * @param LoggerInterface         $logger
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em     = $em;
        $this->logger = $logger;

        $this->userRolesRepo = $this->em->getRepository(UserRoles::class);
        $this->roles         = $this->userRolesRepo->getRoleHierarchy();

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription(implode(PHP_EOL, [
                'Assign User Role to LDAP Username.',
                'Will create new UserRole profile if user is not yet available.',
                'User will be create without checking/lookup object on LDAP.',
            ]))
            ->addArgument(
                'username',
                InputArgument::REQUIRED,
                'Username to assigned to, case-insensitive, no domain prefix. IE: "John.Doe".'
            )
            ->addArgument(
                'role',
                InputArgument::REQUIRED,
                'available role options "' . implode('", "', array_keys($this->roles)) . '".'
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // setup
        $this->cInput  = $input;
        $this->cOutput = $output;
        $this->_markCommandStarts();

        $ioSytle = new SymfonyStyle($this->cInput, $this->cOutput);

        // validate
        $username = $this->validateUsername( $this->cInput->getArgument('username') );
        $role     = $this->validateRole( $this->cInput->getArgument('role') );

        /**
         * @var UserRoles
         */
        $userRoles = $this->userRolesRepo->getUserRoleEnt($username);
        $new = empty($userRoles->getId());

        // info table
        $this->_setWelcome([
            sprintf('Assign %s Role (%s) for: "%s"', strtoupper($role), $this->roles[$role]['name'], $username),
            ['Username',      $userRoles->getUsername()],
            ['Action',        ($new ? 'New Assignment' : 'Changing')],
            ['Profile',       ($new ? 'New Profile'    : 'Found ID #' . $userRoles->getId())],
            ['Current Roles', ($new ? 'none'           : implode(', ', $userRoles->getRoles()))],
            ['New Roles',     implode(', ', $this->roles[$role]['roles'])],
        ]);

        // start processing
        $new = empty($userRoles->getId());
        $res = $this->userRolesRepo->createUpdateUserRoles($userRoles, $userRoles->getUsername(), $this->roles[$role]['roles'], false);

        if ($res) {
            $ioSytle->note('User will have to re-login to see the changes.');
            $ioSytle->success(sprintf('Role %s is Completed!.', ($new ? 'Assignment' : 'Change')));
        }
        // @codeCoverageIgnoreStart
        else {
            $ioSytle->error(sprintf(
                'Role %s is NOT Complete. Entity %d were not applied.',
                ($new ? 'New Assignment' : 'Changing'),
                ($new ? 'Creation'       : 'Updates')
            ));
        }
        // @codeCoverageIgnoreEnd

        $this->logger->info('COMMAND: Super Admin De/Assignment.', [
            'method' => $this->getName(),
            'what'   => [
                'username' => $userRoles->getUsername(),
                'new'      => $new,
                'id'       => $userRoles->getId(),
                'success'  => $res,
            ],
        ]);

        $this->_markCommandEnds($res);
    }

    /**
     * Method to validate username input
     *
     * @param string|string[]|null $username
     *
     * @throws InvalidArgumentException
     *
     * @return string
     */
    private function validateUsername($username): string
    {
        if (!is_string($username)) {
            throw new InvalidArgumentException('Argument "username" is Invalid.', 1);
        }

        if (strpos($username, '@') !== false) {
            throw new InvalidArgumentException('Argument "username" is Invalid; remove email domain.', 1);
        }

        if (preg_match('/[^a-zA-Z0-9._]/', $username) === 1) {
            throw new InvalidArgumentException('Argument "username" format is Invalid.', 1);
        }

        return $username;
    }

    /**
     * Method to validate role input
     *
     * @param string|string[]|null $role
     *
     * @throws InvalidArgumentException
     *
     * @return string
     */
    private function validateRole($role): string
    {
        if (!is_string($role)) {
            throw new InvalidArgumentException('Argument "role" is Invalid.', 1);
        }

        $role = strtolower($role);
        if (!isset($this->roles[$role])) {
            throw new InvalidArgumentException('Role is Unknown.', 1);
        }

        return $role;
    }
}
