<?php

namespace App\Command;

use App\Utility\Helpers as H;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

/**
 * Abstract class for Conference and Journal Sync Down Command.
 * This class handling process sequence of for the query, storage and cosmetic only.
 *
 * Spawning Process sequence:
 *
 *   Command-User                 |      Command-Spawns
 * -------------------------------------------------------------------
 *   1. execute()
 *   2. spawningCycles()    -> loops ->  2.1. execute()
 *   3. execute() #closing               2.2. executeCycleSpawn()
 *   4. end                              2.3. processingCycle()
 *                                       2.4. executeCycleSpawn() #marking
 *                                       2.5. end
 *
 * @since 1.2.0
 */
abstract class RemoteSyncDownBySpawnCommand extends Command
{
    use LockableTrait;
    use \App\Command\Traits\CommandsBasicTrait;

    /**
     * @var int  Per-Loop remote query limit
     */
    protected const PERLOOP_QUERY_LIMIT = 1000;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * Enforce timeout for Process.
     * NULL will disable the timeout validation.
     *
     * @var int|null  In seconds
     */
    private $spawnTimeout;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Command Configurations.
     *
     * @var array
     */
    protected $cfg = [];

    /**
     * List of Data for process in a Cycle / Spawn
     *
     * @var array
     */
    protected $cycleDataSet = [];

    /**
     * @param KernelInterface         $kernel        Defined by CommandsSpawnTrait
     * @param EntityManagerInterface  $em
     * @param LoggerInterface         $logger        Defined by CommandsBasicTrait
     * @param int|null                $spawnTimeout
     */
    public function __construct(KernelInterface $kernel, EntityManagerInterface $em, LoggerInterface $logger, ?int $spawnTimeout = null)
    {
        $this->kernel       = $kernel;
        $this->em           = $em;
        $this->logger       = $logger;
        $this->spawnTimeout = $spawnTimeout;

        parent::__construct();

        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Sync all ' . $this->getReadbleEntityName() . ' from Source Database.')
            ->addOption(
                'skip-local-filter',
                null,
                InputOption::VALUE_NONE,
                'Skip to double-check with local Db, so it will not get overridden; this is meant for FRESH setup only!.'
            )
            ->addOption(
                'limit-loop',
                null,
                InputOption::VALUE_OPTIONAL,
                'Limit process to loop-cycle Count (n X ' . $this->cfg['query-limit'] . ').'
            )
            ->addOption(
                'lock-ignore',
                null,
                InputOption::VALUE_NONE,
                'Force to release a lock left by the previous process.'
            )
            ->addOption(
                'spawn',
                null,
                InputOption::VALUE_OPTIONAL,
                'This option for internal use only.',
                false
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_markCommandStarts();

        $this->cInput  = $input;
        $this->cOutput = $output;

        // if spawn, kick off here, and then stop
        if (true === $this->executeCycleSpawn()) {
            return 0;
        }

        if (
            (!$this->lock()) &&
            (empty($input->getOption('lock-ignore')))
        ) {
           $this->logger->error('The command is already running in another process.');
           return 1;
        }

        $this->setupOptionsAndWelcome();

        $this->checkRemoteDb();

        // start spawning process
        $this->spawningCycles();

        // #closing - post process
        $processed = (int) array_sum($this->cfg['processed']);
        $ignored   = (int) array_sum($this->cfg['ignored']);

        // update widget data
        if (!empty($processed)) {
            $this->updateEntityWidgetData($processed, $ignored);

            $this->_executeWidgetUpdatesCommand();

            $this->em->flush();
        }

        // status
        $this->_showStatsTable([
            $this->getReadbleEntityName() . ' Sync Down Completed.',
            ['Total Spawned Cycle', $this->cfg['cycle']],
            ['Total Processed',     $processed],
            ['Total Ignored',       $ignored],
        ]);

        $this->_markCommandEnds((!empty($processed)));
        return 0;
    }

    /**
     * For Welcome stats Table; append with Entity tables info
     *
     * @return array
     */
    abstract protected function getWelcomeTablesInfo(): array;

    /**
     * Get Sub Command Entity Name
     *
     * @param bool $plural
     *
     * @return string
     */
    abstract protected function getReadbleEntityName(bool $plural = true): string;

    /**
     * Method to get Remote Db Info
     *
     * @see \App\Utility\MsSqlRemoteConnection::getInfo()
     *
     * @return array
     */
    abstract protected function getRemoteDbInfo(): array;

    /**
     * Method to check remote db connection.
     *
     * @param boolean $killConnection  Kill Db connection after
     */
    abstract protected function checkRemoteDb(bool $killConnection = false): void;

    /**
     * Method to force Remote to make Connection.
     * This should trigger Exception if failed.
     */
    abstract protected function makeRemoteDbConnection(): void;

    /**
     * Method to get Remote Db Connection
     *
     * @see \App\Utility\MsSqlRemoteConnection::getConnection()
     *
     * @return \PDO
     */
    abstract protected function getRemoteDbConn(): \PDO;

    /**
     * Method to get per cycle Remote DB Query
     *
     * @return string
     */
    abstract protected function getPerCycleRemoteQuery(): string;

    /**
     * Method to determine Row-ID from a row result of Remote Query.
     * This will be use as $this->cycleDataSet key to the data-row.
     *
     * @param array   $data  Assoc array from PDO Fetch
     *
     * @return string|null
     */
    abstract protected function extractDataRowKey(array $data): ?string;

    /**
     * Process to filter FOUND list, by check Item-IDs against Local Db.
     * This process will be skipped if option --skip-local-filter is presented.
     */
    abstract protected function queryToFilterResultsPerCycle(): void;

    /**
     * Process to store FOUND list to Local Db.
     */
    abstract protected function storeResultsPerCycle(): void;

    /**
     * Method to process Widget data after Sync process Completed
     *
     * @param  int $processed  Total number of processed Item
     * @param  int $ignored    Total number of ignored Item
     */
    abstract protected function updateEntityWidgetData(int $processed = 0, int $ignored  = 0): void;

    /**
     * Handling exception during onStoreResultsPerCycle() loop;
     * - each queried data conversion & storage process.
     *
     * @param \Exception      $e
     * @param int|string|null $id
     * @param array|null      $result
     */
    protected function errorHandlerOnStoreResultsPerCycle(\Exception $e, $id = null, $result = null): void
    {
        $msg = 'Data Ignored due to Unknown Error';
        if ($e instanceof \App\Exception\CommandSourceImportSanitizerException) {
            $msg = 'Data Ignored due to Minor Error';
        }

        $this->logger->warning($msg, [
            'error'  => $e->getMessage(),
            'file'   => $e->getFile(),
            'line'   => $e->getLine(),
            'cycle'  => $this->cfg['cycle'],
            'set-id' => $id,
            'data'   => $result,
        ]);

        $this->cfg['ignored'][ $this->cfg['cycle'] ] += 1;
    }

    /**
     * Simply gather & validate Input, and show welcome info table.
     */
    private function setupOptionsAndWelcome(): void
    {
        $this->cfg['skip-local-filter'] = (bool) $this->cInput->getOption('skip-local-filter');

        $this->cfg['limit-loop'] = $this->cInput->getOption('limit-loop') ?? false;
        if (is_string($this->cfg['limit-loop'])) {
            $this->cfg['limit-loop'] = (int) $this->cfg['limit-loop'];
        }

        $rmtDbI = $this->getRemoteDbInfo();
        $this->logger->info('COMMAND: Source Sync Down Request Received.', [
            'entity' => $this->getReadbleEntityName(false),
            'config' => $this->cfg,
            'remote' => $rmtDbI,
        ]);

        $welcome = [
            'Sync Down ' . $this->getReadbleEntityName(false) . ' Records',
            ['Limit Cycle', (empty($this->cfg['limit-loop']) ? 'No' : 'Yes at ' . number_format(($this->cfg['limit-loop'] * $this->cfg['query-limit'])))],
        ];

        array_push($welcome,
            ['DB Host', ($rmtDbI['host'] ?? 'null')],
            ['DB Port', ($rmtDbI['port'] ?? 'null')],
            ['DB Name', ($rmtDbI['dbname'] ?? 'null')]
        );

        $tables = $this->getWelcomeTablesInfo();
        array_walk($tables, function($val) use (&$welcome) {
            array_push($welcome, $val);
        });

        array_push($welcome,
            ['DB User',     ($rmtDbI['username'] ?? 'null')],
            ['DB Password', ($rmtDbI['password'] ?? 'null')]
        );

        array_push($welcome,
            ['Skip Local Double-Check', $this->cfg['skip-local-filter'] ? 'Yes - WARNING!' : 'No']
        );

        $this->_setWelcome($welcome);
    }

    /**
     * Method to cycle loops on post-test loop.
     * Loop-control either by
     * - opt "limit-loop"
     * - last process #marking (@see executeCycleSpawn()) return count 0
     *
     * @throws \Exception
     */
    private function spawningCycles()
    {
        while (
            (empty($this->cfg['limit-loop'])) ||
            ($this->cfg['cycle'] < $this->cfg['limit-loop'])
        ) {
            $this->cfg['cycle']++;
            $count = 0;

            // prepare command
            $command = [
                PHP_BINDIR . '/php',
                $this->kernel->getProjectDir() . '/bin/console',
                (string) $this->getDefaultName(),
                '--spawn="' . $this->encryptConfig() . '"',
                '-v',
            ];
            $process = new Process($command);
            $process->setTimeout($this->spawnTimeout);

            try {
                $process->mustRun();
                $output = $process->getOutput();

                // unpack #marking from output
                $preg  = preg_match_all('/>> \[(?:count|config)\] > ([a-z0-9=]*)/i', $output, $found);
                $lines = $found[0];
                $found = $found[1];
                if (
                    (2 == $preg) &&
                    (2 == count($found))
                ) {
                    $count  = (int) $found[0];

                    $config = (string) $found[1];
                    $config = $this->decryptConfig($config);
                    if (null == $config) {
                        throw new \Exception('Output Config is Unreadable.');
                    }
                    // apply child-process config to current to pick up
                    $this->cfg = $config;
                }
                else {
                    throw new \Exception('No expected output.');
                }

                // forward child-process output
                foreach ($lines as $line) {
                    $output = str_replace($line, '', $output);
                }
                $this->cOutput->writeln(['', trim($output)]);
            }
            catch (\Exception $e) {
                $this->logger->error('Spawned Process Failed.', array(
                    'error'   => $e->getMessage(),
                    'command' => $command,
                ));
            }

            if (empty($count)) {
                break;
            }
        }
    }

    /**
     * Method that kickoff the cycle via Spawn execution.
     * Validate if has spawn command, apply config and execute it.
     *
     * Calling @see parent::processingCycle()
     *
     * @throws \Exception
     *
     * @return boolean  If its a Spawned process
     */
    private function executeCycleSpawn(): bool
    {
        $done = false;

        $spawn = $this->cInput->getOption('spawn');
        if (
            (!empty($spawn)) &&
            (is_string($spawn))
        ) {
            $spawn = $this->decryptConfig($spawn);
            if (null == $spawn) {
                throw new \Exception('Unknown spawn command.', 1);
            }
            else {
                // applying parent-process config to fresh child-process
                $this->cfg = $spawn;

                $this->makeRemoteDbConnection();

                // begin processing
                $count = $this->processingCycle();

                // leave #marking, for parent-process to pickup
                $this->cOutput->writeln('>> [count] > ' . $count);
                $this->cOutput->writeln('>> [config] > ' . $this->encryptConfig());

                $done = true;
            }
        }

        return $done;
    }

    /**
     * Process a Cycle of Sync Down, call by CommandsSpawnTrait::executeCycleSpawn()
     *
     * @return int  Number of processed item
     */
    private function processingCycle(): int
    {
        $row_start = (int) ((1 == $this->cfg['cycle']) ? 0 : ((($this->cfg['cycle'] - 1) * $this->cfg['query-limit']) + 1));
        $row_end   = (int) ($this->cfg['cycle'] * $this->cfg['query-limit']);

        // query remote
        $count = $this->queryPerCycle($row_start, $row_end);
        if ($count > 0) {
            // filter remote results
            if (false == $this->cfg['skip-local-filter']) {
                $this->queryToFilterResultsPerCycle();
            }

            // store results
            $this->storeResultsPerCycle();
        }

        $this->_showStatsTable([
            "Loop #{$this->cfg['cycle']} Done.",
            ['Query Rows', "{$row_start} to {$row_end} (Found {$count})"],
            ['Processed',   $this->cfg['processed'][ $this->cfg['cycle'] ]],
            ['Ignored',     $this->cfg['ignored'][ $this->cfg['cycle'] ]],
        ]);

        // clear
        $this->cycleDataSet = [];

        return $count;
    }

    /**
     * On each Loop-Cycle
     *
     * @param int $row_start Row begin SEQ Query
     * @param int $row_end   Row end SEQ Query
     *
     * @return int  Number of FOUND items
     */
    private function queryPerCycle(int $row_start, int $row_end): int
    {
        $this->cfg['processed'][ $this->cfg['cycle'] ] = 0;
        $this->cfg['ignored'][ $this->cfg['cycle'] ]   = 0;

        $query = H::removeExcessWhitespace( $this->getPerCycleRemoteQuery() );

        $conn = $this->getRemoteDbConn();
        $stmt = $conn->prepare($query);

        $stmt->bindParam(':row_start', $row_start, \PDO::PARAM_INT);
        $stmt->bindParam(':row_end', $row_end, \PDO::PARAM_INT);

        $queryResult = $stmt->execute();
        if (false == $queryResult) {
            $error = $stmt->errorInfo();
            $this->logger->error('Loop query failed.', $error);
        }
        else {
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                // get key ID
                $id = $this->extractDataRowKey($row);

                if (
                    (!empty($id)) &&
                    (!isset($this->cycleDataSet[$id]))
                ) {
                    $this->cycleDataSet[$id] = $row;
                }
                else {
                    $this->logger->warning('A source data has no relevant ID', [
                        'entity'    => $this->getReadbleEntityName(),
                        'cycle'     => $this->cfg['cycle'],
                        'seq-id'    => ($row['seq'] ?? false),
                        'row-start' => $row_start,
                        'row-end'   => $row_end,
                    ]);
                    $this->cfg['ignored'][ $this->cfg['cycle'] ]++;
                }
            }
            $stmt->closeCursor();
        }

        return count($this->cycleDataSet);
    }

    /**
     * Method to convert "current" config into safe-string.
     * Current can be either parent/child spawn process.
     * Base64 were use to ensure JSON did not break string-command line.
     *
     * @uses $this->cfg
     *
     * @return string
     */
    private function encryptConfig(): string
    {
        $config = (string) json_encode($this->cfg);
        $config = (string) base64_encode($config);

        return $config;
    }

    /**
     * Method to decrypt encrypted command.
     *
     * @param string $config
     *
     * @return array|null  Empty if failed to convert
     */
    private function decryptConfig(string $config): ?array
    {
        $config = (string) base64_decode($config);
        $config = json_decode($config, true);

        return empty($config) ? null : $config;
    }
}
