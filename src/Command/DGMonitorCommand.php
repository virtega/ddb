<?php

namespace App\Command;

use App\Repository\DGMonitorRepository;
use App\Service\ESDGService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * $ bin/console app:dgmonitor-fetch-stats
 *
 * Fetch published articles counts per day from ES.
 *
 */
class DGMonitorCommand extends Command
{
    use \App\Command\Traits\CommandsBasicTrait;

    private const DATE_FORMAT = 'Y-m-d';

    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'app:dgmonitor-fetch-stats';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ESDGService
     */
    private $dgService;

    /**
     * @var \App\Repository\DGMonitorRepository
     */
    private $dgMonitorRepository;

    /**
     * DGMonitorCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @param ESDGService $dgService
     * @param DGMonitorRepository $dgMonitorRepository
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, ESDGService $dgService, DGMonitorRepository $dgMonitorRepository)
    {
        $this->em                   = $em;
        $this->logger               = $logger;
        $this->dgService            = $dgService;
        $this->dgMonitorRepository  = $dgMonitorRepository;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setDescription('Get statistical data from abstracts published per day from ES.')
            ->addOption(
                'date',
                null,
                InputOption::VALUE_OPTIONAL,
                'Date from which to fetch data. Format: ' . self::DATE_FORMAT,
                null
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // setup
        $this->cInput  = $input;
        $this->cOutput = $output;
        $this->_markCommandStarts();

        // validate
        /**
         * @var string
         */
        $date = $this->cInput->getOption('date');
        $date = $this->validateDate($date);

        // delete previous data
        try{
            $deleted = $this->dgMonitorRepository->delete($date);

            if ($deleted > 0) {
                $this->cOutput->writeln('Deleted ' . $deleted . " ids for '" . $date . "'");
            } else {
                $this->cOutput->writeln("No data deleted for '" . $date . "'");
            }
        } catch (\Exception $ex) {
            $this->cOutput->writeln($ex->getMessage());
            $this->_markCommandEnds(false);

            return 1;
        }


        // start processing
        $ids = $this->dgService->getAbstractsReadOnly($date);
        if (!empty($ids['data'])) {
            if (!empty($ids['message'])) {
                $this->cOutput->writeln(str_replace('displaying', 'inserting', $ids['message']));
            }
            $res = $this->dgMonitorRepository->bulkInsertIds($date, $ids['data']);
            if ($res) {
                $this->cOutput->writeln('Inserted ' . count($ids['data']) . " ids for '" . $date . "'");
            } else {
                $this->cOutput->writeln('ERROR: Failed to insert ids, please check logs');
            }
        } else {
            $this->cOutput->writeln("No data found for date '" . $date. "'");
            $res = false;
        }

        $this->_markCommandEnds($res);

        return 0;
    }

    /**
     * Method to validate date input
     *
     * @param string|null $date
     *
     * @throws InvalidArgumentException
     *
     * @return string
     */
    private function validateDate($date): string
    {
        $format = self::DATE_FORMAT;
        if (empty($date)) {
            $date = date($format);
        }

        $d = DateTime::createFromFormat($format, $date);
        $valid = $d && $d->format($format) === $date;
        if (!$valid) {
            throw new InvalidArgumentException('Specified date not valid. Format should be: ' . $format, 1);
        }

        return $date;
    }
}
