<?php

namespace App\Command;

use App\Entity\Journal;
use App\Entity\SpecialtyTaxonomy;
use App\Service\MailService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * $ bin/console app:journals-export-data.
 *
 * Class handing Command request to export data.
 * This will be then email on argument.
 *
 * @since 1.2.3
 */
final class JournalsDataExportCommand extends AbstractConfJourDataExportCommand
{
    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'app:journals-export-data';

    /**
     * @var string
     */
    public static $type = AbstractConfJourDataExportCommand::TYPE_JOURNALS;

    /**
     * Command Configurations.
     *
     * @var array
     */
    public static $cfg = [
        'name'               => '',
        'volume'             => '',
        'status'             => '',
        'specialty'          => [],
        'ids'                => '',
        'ismedline'          => '',
        'isdgabstract'       => '',
        'issecondline'       => '',
        'notbeingupdated'    => '',
        'drugbeingmonitored' => '',
        'deleted'            => '',
        'showcorrupted'      => 'no',
    ];

    /**
     * @var \Doctrine\ORM\EntityRepository<SpecialtyTaxonomy>
     */
    private $specialtyRepo;

    /**
     * @param EntityManagerInterface  $em
     * @param MailService             $mailSrc
     * @param LoggerInterface         $logger
     */
    public function __construct(EntityManagerInterface $em, MailService $mailSrc, LoggerInterface $logger)
    {
        /**
         * @var \App\Repository\JournalRepository
         */
        $this->repo = $em->getRepository(Journal::class);

        /**
         * @var \Doctrine\ORM\EntityRepository<SpecialtyTaxonomy>
         */
        $this->specialtyRepo = $em->getRepository(SpecialtyTaxonomy::class);

        parent::__construct($mailSrc, $logger);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Export Journals into CSV with Search Attribute, then Email it.')
            ->addArgument(
                'email',
                InputArgument::REQUIRED,
                'Email Address'
            )
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'Name / Abbreviation',
                ''
            )
            ->addArgument(
                'volume',
                InputArgument::OPTIONAL,
                'Volume / Issue / Number',
                ''
            )
            ->addArgument(
                'status',
                InputArgument::OPTIONAL,
                'Status Flag;              ' . implode(', ', [
                    '"" = Any',
                    '"' . Journal::STATUS_NONE . '" = None',
                    '"' . Journal::STATUS_ACTIVE . '" = Active',
                    '"' . Journal::STATUS_ARCHIVE . '" = Archived',
                ]),
                ''
            )
            ->addArgument(
                'specialty',
                InputArgument::OPTIONAL,
                'Specialty IDs, separated by comma',
                ''
            )
            ->addArgument(
                'ids',
                InputArgument::OPTIONAL,
                'ID /  UNID',
                ''
            )
            ->addArgument(
                'ismedline',
                InputArgument::OPTIONAL,
                'Medline Journal;          "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'isdgabstract',
                InputArgument::OPTIONAL,
                'DG Abstract Journal;      "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'issecondline',
                InputArgument::OPTIONAL,
                'SecondLine Journal;       "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'notbeingupdated',
                InputArgument::OPTIONAL,
                'Marked not being Updated; "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'drugbeingmonitored',
                InputArgument::OPTIONAL,
                'Marked being Monitored;   "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'deleted',
                InputArgument::OPTIONAL,
                'Marked Deleted;           "" = Any, "1"/"yes" = Yes, "0"/"no" = No',
                ''
            )
            ->addArgument(
                'showcorrupted',
                InputArgument::OPTIONAL,
                'Export Corrupted;         "1"/"yes" = Yes, "0"/"no" = No',
                'no'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function getInputsToConfig(): ?array
    {
        $arguments = [];

        foreach (self::$cfg as $key => $cfgVal) {
            $value = $this->cInput->getArgument($key);

            if (is_array($value)) {
                $this->logger->error("Invalid '{$key}' multiple, expecting single value only.");
                return null;
            }

            switch ($key) {
                case 'ismedline':
                case 'isdgabstract':
                case 'issecondline':
                case 'notbeingupdated':
                case 'drugbeingmonitored':
                case 'globaledition':
                case 'deleted':
                case 'showcorrupted':
                    $arguments[$key] = (is_null($value) ? $cfgVal : trim($value));
                    break;

                case 'status':
                    $arguments[$key] = (is_null($value) ? $cfgVal : trim($value));
                    if (strlen($arguments[$key])) {
                        $arguments[$key] = (int) $arguments[$key];
                    }
                    break;

                case 'specialty':
                    $arguments[$key] = (is_null($value) ? $cfgVal : trim($value));
                    $arguments[$key] = explode(',', $arguments[$key]);
                    $arguments[$key] = array_map('trim', $arguments[$key]);
                    $arguments[$key] = array_filter($arguments[$key]);
                    $arguments[$key] = array_unique($arguments[$key]);
                    $arguments[$key] = array_values($arguments[$key]);
                    break;

                default:
                    $arguments[$key] = $value;
            }
        }

        return $arguments;
    }

    /**
     * {@inheritdoc}
     */
    protected function getHeaders(): array
    {
        $headers = [
            'Id',
            'Name',
            'Abbreviation',
            'Volume/Issue',
            'Number',
            'Status',
            'Exported',
            'Medline',
            'SecondLine',
            'DG Abstract',
            'Not being Updated',
            'Drugs being Monitored',
            'Global',
            'Deleted',
            'Manual Creation',
            'Medline ISSN',
            'Journal Report 1',
            'Journal Report 2',
            'UNID',

            'Language',
            'Country',
            'Region',
            'Edition',

            'Copyright',
            'Auto Publishing Source',
            'Previous Row GUID',

            'Abstract: With Abstract',
            'Abstract: Register for Abstract',
            'Abstract: FullText Availability',
            'Abstract: Register for FullText',
            'Abstract: Fee for FullText',
            'Abstract: Comments',

            'Ordering: HTML Code',
            'Ordering: CGI Value',
            'Ordering: Printer Name',

            'Publisher: Name',
            'Publisher: Registration URL',

            'URL: URL',
            'URL: Updated On',
            'URL: Viewed On',
            'URL: Viewed On Comment',
            'URL: To Update',
            'URL: Note',
            'URL: Frequency',
            'URL: Username',
            'URL: Password',

            'Validation: Author',
            'Validation: Date Composed',
            'Validation: Cleared by',
            'Validation: Cleared on',
            'Validation: Modified',

            'Meta: Created on',
            'Meta: Created by',
            'Meta: Modified on',
            'Meta: Modified by',

            'Specialty Count',
        ];

        $specialty = $this->repo->countMaxSpecialtiesConnection();
        for ($i = 0; $i < $specialty; $i++) {
            $headers[] = 'Specialty ' . ($i + 1);
        }

        return $headers;
    }

    /**
     * {@inheritdoc}
     */
    protected function translateInputToReadable(string $key, $value): string
    {
        switch ($key) {
            case 'status':
                if (isset(Journal::$statuses[$value])) {
                    $value = Journal::$statuses[$value] . " ({$value})";
                }
                break;

            case 'specialty':
                if (!is_array($value)) {
                    $value = [$value];
                }

                foreach ($value as $vi => $val) {
                    if (is_numeric($val)) {
                        if ($specialty = $this->specialtyRepo->findOneBy(['id' => $val])) {
                            $value[$vi] = $specialty->getSpecialty() . " (#{$val})";
                        }
                    }
                }
                break;
        }

        $value = (is_array($value) ? implode(', ', $value) : "{$value}");

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    protected function convertJournalToRow(Journal $journal): array
    {
        $country = '';
        if (null !== $journal->getDetailsCountry()) {
            if ($country = $this->repo->getJournalDetailsCountry($journal)) {
                $country = $country->getName();
            }
        }

        $region = '';
        if (null !== $journal->getDetailsRegion()) {
            if ($region = $this->repo->getJournalDetailsCountryTaxonomy($journal)) {
                $region = $region->getName();
            }
        }

        $row = [
            $journal->getId(),
            $journal->getName(),
            $journal->getAbbreviation(),
            $journal->getVolume(),
            $journal->getNumber(),
            $journal->getStatusReadable(),
            $journal->isExported()             ? 'Yes' : 'No',
            $journal->isMedLine()              ? 'Yes' : 'No',
            $journal->isSecondLine()           ? 'Yes' : 'No',
            $journal->isDgAbstract()           ? 'Yes' : 'No',
            $journal->isNotBeingUpdated()      ? 'Yes' : 'No',
            $journal->isDrugsMonitored()       ? 'Yes' : 'No',
            $journal->isGlobalEditionJournal() ? 'Yes' : 'No',
            $journal->isDeleted()              ? 'Yes' : 'No',
            $journal->isManualCreation()       ? 'Yes' : 'No',
            $journal->getMedlineIssn(),
            $journal->getJournalReport1(),
            $journal->getJournalReport2(),
            $journal->getUnid(),

            $journal->getDetailsLanguage(),
            $country,
            $region,
            $journal->getDetailsEdition(),

            $journal->getDetailsCopyright(),
            $journal->getDetailsAutoPublishingSource(),
            $journal->getDetailsPreviousRowGuid(),

            $journal->getDetailsAbstractAbstract()            ? 'Yes' : 'No',
            $journal->getDetailsAbstractRegForAbstract()      ? 'Yes' : 'No',
            $journal->getDetailsAbstractFullTextAvailablity() ? 'Yes' : 'No',
            $journal->getDetailsAbstractRegForFullText()      ? 'Yes' : 'No',
            $journal->getDetailsAbstractFeeForFullText()      ? 'Yes' : 'No',
            $journal->getDetailsAbstractComments(),

            $journal->getDetailsOrderingHtmlCode(),
            $journal->getDetailsOrderingCgiValue(),
            $journal->getDetailsOrderingPrinterName(),

            $journal->getDetailsPublisherName(),
            $journal->getDetailsPublisherRegUrl(),

            $journal->getDetailsUrlUrl(),
            $this->_setDateTimeValue($journal->getDetailsUrlUpdatedOn()),
            $this->_setDateTimeValue($journal->getDetailsUrlViewedOn()),
            $journal->getDetailsUrlViewedOnComment(),
            $this->_setDateTimeValue($journal->getDetailsUrlToUpdate()),
            $journal->getDetailsUrlNote(),
            $journal->getDetailsUrlFrequency(),
            $journal->getDetailsUrlUsername(),
            $journal->getDetailsUrlPassword(),

            $journal->getDetailsValidationAuthor(),
            $this->_setDateTimeValue($journal->getDetailsValidationDateComposed()),
            $journal->getDetailsValidationClearedBy(),
            $this->_setDateTimeValue($journal->getDetailsValidationClearedOn()),
            $journal->getDetailsValidationModified(),

            $this->_setDateTimeValue($journal->getDetailsMetaCreatedOn()),
            $journal->getDetailsMetaCreatedBy()  ?? '',
            $this->_setDateTimeValue($journal->getDetailsMetaModifiedOn()),
            $journal->getDetailsMetaModifiedBy() ?? '',
        ];

        $specialties = $journal->getSpecialties();
        $row[] = count($specialties);

        foreach ($specialties as $specialty) {
            $row[] = $specialty->getSpecialty()->getSpecialty();
        }

        return $row;
    }
}
