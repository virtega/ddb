<?php

namespace App\Command;

use App\Entity\Variable;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * $ bin/console app:update-widgets.
 *
 * This Class handing Command request to do Widget recalculation on demand.
 *
 * @since 1.0.0
 */
class WidgetUpdatesCommand extends Command
{
    use \App\Command\Traits\CommandsBasicTrait;

    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'app:update-widgets';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Tasks list.
     *
     * @var array
     */
    private $tasks = array(
        // EXPERIMENTAL
        // widget-experimental-count
        array('updateExperimentalCount',       array(null, null)),

        // widget-experimental-valid-count
        // array('updateExperimentalCount',    array(null, 1)),

        // widget-experimental-invalid-count
        // array('updateExperimentalCount',    array(null, 0)),

        // widget-experimental-experimental-count
        // array('updateExperimentalCount',    array('experimental', null)),

        // widget-experimental-experimental-valid-count
        array('updateExperimentalCount',       array('experimental', 1)),

        // widget-experimental-experimental-invalid-count
        array('updateExperimentalCount',       array('experimental', 0)),

        // widget-experimental-generic-count
        // array('updateExperimentalCount',    array('generic', null)),

        // widget-experimental-generic-valid-count
        array('updateExperimentalCount',       array('generic', 1)),

        // widget-experimental-generic-invalid-count
        array('updateExperimentalCount',       array('generic', 0)),

        // widget-experimental-brand-count
        // array('updateExperimentalCount',    array('brand', null)),

        // widget-experimental-brand-valid-count
        array('updateExperimentalCount',       array('brand', 1)),

        // widget-experimental-brand-invalid-count
        array('updateExperimentalCount',       array('brand', 0)),

        // GENERIC
        // widget-generic-count
        array('updateGenericCount',            array(null, null, true)),

        // widget-generic-non_orphan-count
        // array('updateGenericCount',         array(null, false, true)),

        // widget-generic-valid-count
        // array('updateGenericCount',         array(1, null, true)),

        // widget-generic-invalid-count
        // array('updateGenericCount',         array(0, null, true)),

        // widget-generic-valid-orphan-count
        array('updateGenericCount',            array(1, true, true)),

        // widget-generic-invalid-orphan-count
        array('updateGenericCount',            array(0, true, true)),

        // widget-generic-valid-non_orphan-count
        array('updateGenericCount',            array(1, false, true)),

        // widget-generic-invalid-non_orphan-count
        array('updateGenericCount',            array(0, false, true)),

        // widget-generic-no-fullcode-count
        array('updateGenericCount',            array(null, null, false)),

        // widget-generic-non_orphan-no-fullcode-count
        // array('updateGenericCount',         array(null, false, false)),

        // widget-generic-valid-no-fullcode-count
        // array('updateGenericCount',         array(1, null, false)),

        // widget-generic-invalid-no-fullcode-count
        // array('updateGenericCount',         array(0, null, false)),

        // widget-generic-valid-orphan-no-fullcode-count
        array('updateGenericCount',            array(1, true, false)),

        // widget-generic-invalid-orphan-no-fullcode-count
        array('updateGenericCount',            array(0, true, false)),

        // widget-generic-valid-non_orphan-no-fullcode-count
        array('updateGenericCount',            array(1, false, false)),

        // widget-generic-invalid-non_orphan-no-fullcode-count
        array('updateGenericCount',            array(0, false, false)),

        // widget-generic-ref-level-1-count
        // widget-generic-ref-level-2-count
        // widget-generic-ref-level-3-count
        // widget-generic-ref-level-4-count
        array('updateGenericLevels',           array()),

        // BRAND
        // widget-brand-count
        array('updateBrandCount',              array(null, null)),

        // widget-brand-non_orphan-count
        // array('updateBrandCount',           array(null, false)),

        // widget-brand-valid-count
        // array('updateBrandCount',           array(1, null)),

        // widget-brand-invalid-count
        // array('updateBrandCount',           array(0, null)),

        // widget-brand-valid-orphan-count
        array('updateBrandCount',              array(1, true)),

        // widget-brand-invalid-orphan-count
        array('updateBrandCount',              array(0, true)),

        // widget-brand-valid-non_orphan-count
        array('updateBrandCount',              array(1, false)),

        // widget-brand-invalid-non_orphan-count
        array('updateBrandCount',              array(0, false)),

        // GENERIC KEYWORDS
        // widget-generic-synonym-count
        // widget-generic-typo-count
        array('updateGenericKeywordsCount',    array(null, null)),

        // widget-generic-synonym-orphan-count
        // widget-generic-typo-orphan-count
        array('updateGenericKeywordsCount',    array(null, true)),

        // widget-generic-synonym-non_orphan-count
        // widget-generic-typo-non_orphan-count
        // array('updateGenericKeywordsCount', array(null, false)),

        // widget-generic-synonym-no-fullcode-count
        // widget-generic-typo-no-fullcode-count
        array('updateGenericKeywordsCount',    array(null, null, false)),

        // widget-generic-synonym-orphan-no-fullcode-count
        // widget-generic-typo-orphan-no-fullcode-count
        array('updateGenericKeywordsCount',    array(null, true, false)),

        // widget-generic-synonym-non_orphan-no-fullcode-count
        // widget-generic-typo-non_orphan-no-fullcode-count
        // array('updateGenericKeywordsCount', array(null, false, false)),

        // BRAND KEYWORDS
        // widget-brand-synonym-count
        // widget-brand-typo-count
        array('updateBrandKeywordsCount',      array(null, null)),

        // widget-generic-synonym-orphan-count
        // widget-generic-typo-orphan-count
        array('updateBrandKeywordsCount',      array(null, true)),

        // widget-generic-synonym-non_orphan-count
        // widget-generic-typo-non_orphan-count
        // array('updateBrandKeywordsCount',   array(null, false)),

        // SPECIFIC
        // widget-specific-complete-count
        // widget-specific-experimental-generic-count
        // widget-specific-generic-brand-count
        // widget-specific-brand-generic-count
        // widget-specific-experimental-count
        // widget-specific-generic-count
        // widget-specific-brand-count
        array('updateSpecificsCounts',         array(true)),

        // widget-specific-complete-no-fullcode-count
        // widget-specific-experimental-generic-no-fullcode-count
        // widget-specific-generic-brand-no-fullcode-count
        // widget-specific-brand-generic-no-fullcode-count
        // widget-specific-experimental-no-fullcode-count
        // widget-specific-generic-no-fullcode-count
        // widget-specific-brand-no-fullcode-count
        array('updateSpecificsCounts',         array(false)),

        // CREATION-DATE
        // widget-experimental-creation-date-count
        array('updateDrugCreationDate',        array('experimental')),
        // widget-generic-creation-date-count
        array('updateDrugCreationDate',        array('generic')),
        // widget-brand-creation-date-count
        array('updateDrugCreationDate',        array('brand')),

        // COMMENTS
        // widget-experimental-comments-count
        array('updateDrugComments',            array('experimental')),
        // widget-generic-comments-count
        array('updateDrugComments',            array('generic')),
        // widget-brand-comments-count
        array('updateDrugComments',            array('brand')),

        // CONFERENCES
        // widget-conferences-overall-count
        // widget-conferences-corrupted-count
        // widget-conferences-key-discontinued-count
        // widget-conferences-key-not-discontinued-count
        // widget-conferences-key-online-count
        // widget-conferences-key-not-online-count
        // widget-conferences-key-key-event-count
        // widget-conferences-key-not-key-event-count
        ['updateConferencesCount', []],

        // JOURNALS
        // widget-journals-overall-count
        // widget-journals-corrupted-count
        // widget-journals-key-active-count
        // widget-journals-key-archive-count
        // widget-journals-key-is-medline-count
        // widget-journals-key-is-secondline-count
        // widget-journals-key-is-dgabstract-count
        // widget-journals-key-is-notbeingupdated-count
        // widget-journals-key-is-deleted-count
        // widget-journals-key-is-globaleditions-count
        ['updateJournalsCount', []]
    );

    /**
     * @var \App\Repository\VariableRepository<Variable>
     */
    private $variableRepo;

    /**
     * @param EntityManagerInterface  $em
     * @param LoggerInterface         $logger
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em     = $em;
        $this->logger = $logger;

        $this->variableRepo = $this->em->getRepository(Variable::class);

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Update all Widget Calculations.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // setup
        $this->cInput  = $input;
        $this->cOutput = $output;
        $this->_markCommandStarts();

        $this->_setWelcome(array(
            'Update Widgets',
        ));

        $this->logger->info('COMMAND: Widget Update Request Received.', array(
            'method' => __METHOD__,
        ));

        $task_count = count($this->tasks);
        $progress   = new ProgressBar($this->cOutput, $task_count);
        $progress->setMessage('Start');

        foreach ($this->tasks as $task) {
            list($func, $args) = $task;

            $callable = array($this, $func);
            if (
                (method_exists($this, $func)) &&
                (is_callable($callable))
            ) {
                $set = call_user_func_array($callable, $args);
                $set = json_encode($set);
                $progress->setMessage('Updated: ' . $set);
            }
            else {
                // @codeCoverageIgnoreStart
                $this->cOutput->writeln("<error>Missing Widget Method: {$func}</error>");
                // @codeCoverageIgnoreEnd
            }

            $progress->advance();
        }

        $dashboard = $this->variableRepo->findOneBy(array('name' => 'dashboard-widget-tokens-cache'));
        if (!empty($dashboard)) {
            $this->em->remove($dashboard);
        }

        $this->em->flush();

        $this->_markCommandEnds(!empty($task_count));
    }

    /**
     * Method to calculate all Experimental Drug Available Indexes.
     *
     * @param string|null  $type   Experimental Drug Status
     * @param int|null     $valid  Drug valid Flag
     *
     * @return array  key is the index name given, follow by the calculated Value
     */
    private function updateExperimentalCount(?string $type = null, ?int $valid = null): array
    {
        // get count
        $count = $this->variableRepo->getExperimentalDrugCount($type, $valid);

        // generate index name
        $index = array('widget-experimental');
        if (null !== $type) {
            $index[] = $type;
        }

        if (null !== $valid) {
            if ($valid) {
                $index[] = 'valid';
            }
            else {
                $index[] = 'invalid';
            }
        }
        $index[] = 'count';
        $index   = implode('-', $index);

        // update widget
        $this->variableRepo->updateWidgetValue($index, $count);

        return array($index => $count);
    }

    /**
     * Method to calculate all Generic Drug Available Indexes.
     *
     * @param int|null      $valid    Drug valid Flag
     * @param boolean|null  $orphan   Flag Drug with no Experimental
     * @param boolean       $hasCode  If Generic has Full Code
     *
     * @return array    key is the index name given, follow by the calculated Value
     */
    private function updateGenericCount(?int $valid = null, ?bool $orphan = null, bool $hasCode = true): array
    {
        // get count
        $count = $this->variableRepo->getGenericDrugCount($valid, $orphan, $hasCode);

        // generate index name
        $index = array('widget-generic');
        if (null !== $valid) {
            if ($valid) {
                $index[] = 'valid';
            }
            else {
                $index[] = 'invalid';
            }
        }
        if (null !== $orphan) {
            if ($orphan) {
                $index[] = 'orphan';
            }
            else {
                $index[] = 'non_orphan';
            }
        }
        if (!$hasCode) {
            $index[] = 'no-fullcode';
        }
        $index[] = 'count';
        $index   = implode('-', $index);

        // update widget
        $this->variableRepo->updateWidgetValue($index, $count);

        return array($index => $count);
    }

    /**
     * Method to calculate all Brand Drug Available Indexes.
     *
     * @param int|null      $valid     Drug valid Flag
     * @param boolean|null  $orphan    Flag Drug with no Generic
     *
     * @return array    key is the index name given, follow by the calculated Value
     */
    private function updateBrandCount(?int $valid = null, ?bool $orphan = null): array
    {
        // get count
        $count = $this->variableRepo->getBrandDrugCount($valid, $orphan);

        // generate index name
        $index = array('widget-brand');
        if (null !== $valid) {
            if ($valid) {
                $index[] = 'valid';
            }
            else {
                $index[] = 'invalid';
            }
        }
        if (null !== $orphan) {
            if ($orphan) {
                $index[] = 'orphan';
            }
            else {
                $index[] = 'non_orphan';
            }
        }
        $index[] = 'count';
        $index   = implode('-', $index);

        // update widget
        $this->variableRepo->updateWidgetValue($index, $count);

        return array($index => $count);
    }

    /**
     * Method to calculate Generic Keywords Count.
     *
     * @param string|array  $type        Expect 'synonym' / 'typo', or in array
     * @param boolean       $orphan
     * @param boolean       $hasCode     If Generic has Full Code
     *
     * @return array
     */
    private function updateGenericKeywordsCount($type = null, $orphan = null, bool $hasCode = true): array
    {
        $type = (array) $type;

        if (empty($type)) {
            $type = array('synonym', 'typo');
        }

        $results = array();

        foreach ($type as $ty) {
            // get count
            $func  = 'getGeneric' . ucwords($ty) . 'Count';
            $count = $this->variableRepo->$func($orphan, $hasCode);

            // generate index name
            $index = array('widget-generic', $ty);
            if (null !== $orphan) {
                if ($orphan) {
                    $index[] = 'orphan';
                }
                else {
                    // @codeCoverageIgnoreStart
                    // Against the structure req allowed
                    $index[] = 'non_orphan';
                    // @codeCoverageIgnoreEnd
                }
            }

            if (!$hasCode) {
                $index[] = 'no-fullcode';
            }

            $index[] = 'count';
            $index   = implode('-', $index);

            // update widget
            $this->variableRepo->updateWidgetValue($index, $count);

            $results[$index] = $count;
        }

        return $results;
    }

    /**
     * Method to calculate Brand Keywords Count.
     *
     * @param string|array  $type       Expect 'synonym' / 'typo', or in array
     * @param boolean       $orphan
     *
     * @return array
     */
    private function updateBrandKeywordsCount($type = null, $orphan = null): array
    {
        $type = (array) $type;

        if (empty($type)) {
            $type = array('synonym', 'typo');
        }

        $results = array();

        foreach ($type as $ty) {
            // get count
            $func  = 'getBrand' . ucwords($ty) . 'Count';
            $count = $this->variableRepo->$func($orphan);

            // generate index name
            $index = array('widget-brand', $ty);
            if (null !== $orphan) {
                if ($orphan) {
                    $index[] = 'orphan';
                }
                else {
                    // @codeCoverageIgnoreStart
                    // Against the structure req allowed
                    $index[] = 'non_orphan';
                    // @codeCoverageIgnoreEnd
                }
            }

            $index[] = 'count';
            $index   = implode('-', $index);

            // update widget
            $this->variableRepo->updateWidgetValue($index, $count);

            $results[$index] = $count;
        }

        return $results;
    }

    /**
     * Method to Calculate Specific Counts.
     *
     * @param boolean   $hasCode   If Generic has Full Code
     *
     * @return array
     */
    private function updateSpecificsCounts(bool $hasCode = true): array
    {
        $set = array(
            'EB'           => 'complete',
            'EG'           => 'experimental-generic',
            'GB'           => 'generic-brand',
            'BG'           => 'brand-generic',
            'Experimental' => 'experimental',
            'Generic'      => 'generic',
            'Brand'        => 'brand',
        );

        $results = array();

        foreach ($set as $sk => $sv) {
            // get count
            $func  = "getSpecific{$sk}Count";
            $count = $this->variableRepo->$func($hasCode);

            // update widget
            $ind = "widget-specific-{$sv}-count";
            if (!$hasCode) {
                $ind = "widget-specific-{$sv}-no-fullcode-count";
            }
            $this->variableRepo->updateWidgetValue($ind, $count);

            $results[$ind] = $count;
        }

        return $results;
    }

    /**
     * Method to Calculate Generic Reference Table for Level 1 to 4 Availability.
     *
     * @return array
     */
    private function updateGenericLevels(): array
    {
        $levels = range(1, 4);

        $return = array();
        foreach ($levels as $lev) {
            $index = "widget-generic-ref-level-{$lev}-count";

            // get count
            $return[$index] = $this->variableRepo->getDrugSpecificFieldCount("generic_ref_level{$lev}", 'code');

            // update widget
            $this->variableRepo->updateWidgetValue($index, $return[$index]);
        }

        return $return;
    }

    /**
     * Method to Calculate Drug Creation Date Availability.
     *
     * @param string    $type   Drug Type - Expects: 'experimental' / 'generic' / 'brand'
     *
     * @return array
     */
    private function updateDrugCreationDate($type): array
    {
        // get count
        $count = $this->variableRepo->getDrugSpecificFieldCount($type, 'creation_date');

        // update widget
        $this->variableRepo->updateWidgetValue("widget-{$type}-creation-date-count", $count);

        return array("widget-{$type}-creation-date-count" => $count);
    }

    /**
     * Method to Calculate Drug Comments Availability.
     *
     * @param string    $type   Drug Type - Expects: 'experimental' / 'generic' / 'brand'
     *
     * @return array
     */
    private function updateDrugComments($type): array
    {
        // get count
        $count = $this->variableRepo->getDrugSpecificFieldCount($type, 'comments');

        // update widget
        $this->variableRepo->updateWidgetValue("widget-{$type}-comments-count", $count);

        return array("widget-{$type}-comments-count" => $count);
    }

    /**
     * Method to Calculate all for Conference data
     *
     * @return array
     */
    private function updateConferencesCount(): array
    {
        /**
         * @var \App\Repository\ConferenceRepository<Conference>
         */
        $conferenceRepo = $this->em->getRepository(\App\Entity\Conference::class);

        $count = $conferenceRepo->countOverallCount();
        $this->variableRepo->updateWidgetValue('widget-conferences-overall-count', $count);

        $count = $conferenceRepo->countCorruptedCount();
        $this->variableRepo->updateWidgetValue('widget-conferences-corrupted-count', $count);

        $count = $conferenceRepo->countKeyedCount('discontinued', 1);
        $this->variableRepo->updateWidgetValue('widget-conferences-key-discontinued-count', $count);

        $count = $conferenceRepo->countKeyedCount('discontinued', 0);
        $this->variableRepo->updateWidgetValue('widget-conferences-key-not-discontinued-count', $count);

        $count = $conferenceRepo->countKeyedCount('online', 1);
        $this->variableRepo->updateWidgetValue('widget-conferences-key-online-count', $count);

        $count = $conferenceRepo->countKeyedCount('online', 0);
        $this->variableRepo->updateWidgetValue('widget-conferences-key-not-online-count', $count);

        $count = $conferenceRepo->countKeyedCount('key_event', 1);
        $this->variableRepo->updateWidgetValue('widget-conferences-key-key-event-count', $count);

        $count = $conferenceRepo->countKeyedCount('key_event', 0);
        $this->variableRepo->updateWidgetValue('widget-conferences-key-not-key-event-count', $count);

        return ['conference-counts', 7];
    }

    /**
     * Method to Calculate all for Journal data
     *
     * @return array
     */
    private function updateJournalsCount(): array
    {
        /**
         * @var \App\Repository\JournalRepository<Journal>
         */
        $journalRepo = $this->em->getRepository(\App\Entity\Journal::class);

        $count = $journalRepo->countOverallCount();
        $this->variableRepo->updateWidgetValue('widget-journals-overall-count', $count);

        $count = $journalRepo->countCorruptedCount();
        $this->variableRepo->updateWidgetValue('widget-journals-corrupted-count', $count);

        $count = $journalRepo->countKeyedCount('status', 1);
        $this->variableRepo->updateWidgetValue('widget-journals-key-active-count', $count);

        $count = $journalRepo->countKeyedCount('status', 5);
        $this->variableRepo->updateWidgetValue('widget-journals-key-archive-count', $count);

        $count = $journalRepo->countKeyedCount('is_medline', 1);
        $this->variableRepo->updateWidgetValue('widget-journals-key-is-medline-count', $count);

        $count = $journalRepo->countKeyedCount('is_second_line', 1);
        $this->variableRepo->updateWidgetValue('widget-journals-key-is-secondline-count', $count);

        $count = $journalRepo->countKeyedCount('is_dgabstract', 1);
        $this->variableRepo->updateWidgetValue('widget-journals-key-is-dgabstract-count', $count);

        $count = $journalRepo->countKeyedCount('not_being_updated', 1);
        $this->variableRepo->updateWidgetValue('widget-journals-key-is-notbeingupdated-count', $count);

        $count = $journalRepo->countKeyedCount('deleted', 1);
        $this->variableRepo->updateWidgetValue('widget-journals-key-is-deleted-count', $count);

        $count = $journalRepo->countKeyedCount('global_edition_journal', 1);
        $this->variableRepo->updateWidgetValue('widget-journals-key-is-globaleditions-count', $count);

        return ['journal-counts', 10];
    }
}
