<?php

namespace App\Command;

use App\Entity\Conference;
use App\Entity\Journal;
use App\Service\MailService;
use App\Utility\ExportCsv;
use App\Utility\Helpers as H;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Abstract class handing Journal & Conference Export command request to export data.
 * This will be then email on argument.
 *
 * @since 1.2.3
 */
abstract class AbstractConfJourDataExportCommand extends Command
{
    use \App\Command\Traits\CommandsBasicTrait;
    use \App\Entity\Traits\EntityDetailsTrait;

    /**
     * @var string
     */
    protected const TYPE_JOURNALS = 'Journals';

    /**
     * @var string
     */
    protected const TYPE_CONFERENCES = 'Conferences';

    /**
     * @var int Query size per loop
     */
    private const MAXPERLOOP = 1000;

    /**
     * @var string
     */
    public static $type = '';

    /**
     * Command Configurations.
     *
     * @var array
     */
    public static $cfg = [];

    /**
     * @var \App\Repository\JournalRepository
     * @var \App\Repository\ConferenceRepository
     */
    protected $repo;

    /**
     * @var MailService
     */
    protected $mailSrc;

    /**
     * For EntityDetailsTrait
     *
     * @var array
     */
    private static $defaults = [];

    /**
     * @param MailService             $mailSrc
     * @param LoggerInterface         $logger
     */
    public function __construct(MailService $mailSrc, LoggerInterface $logger)
    {
        $this->mailSrc = $mailSrc;
        $this->logger  = $logger;

        parent::__construct();

        // post init check
        $this->getType();
    }

    /**
     * Method to get registered type
     *
     * @throws \Exception
     *
     * @return string
     */
    public function getType(): string
    {
        if (empty($this::$type)) {
            throw new \Exception('AbstractConfJourDataExportCommand $type is empty.', 1);
        }

        return $this::$type;
    }

    /**
     * Method to get registered config
     *
     * @return array
     */
    public function getArguments(): array
    {

        return $this::$cfg;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // setup
        $this->cInput  = $input;
        $this->cOutput = new BufferedOutput($output->getVerbosity(), $output->isDecorated(),  $output->getFormatter());
        $this->_markCommandStarts();

        $emailAddr = $this->cInput->getArgument('email');
        if (empty($emailAddr)) {
            throw new \Exception("Missing Email address on export-command argument.", 1);
        }

        // validate
        $arguments = $this->getInputsToConfig();
        if (null === $arguments) {
            return 1;
        }

        // set welcome
        $welcome = [
            "Exporting {$this->getType()} Data",
            ['Email to', $emailAddr],
            'Terms',
        ];
        foreach ($arguments as $key => $value) {
            $welcome[] = [
                "- {$key}",
                $this->translateInputToReadable($key, $value),
            ];
        }
        $this->_setWelcome($welcome);

        $this->logger->info("COMMAND: Export {$this->getType()} Data Request Received.", [
            'command' => $this::$defaultName,
            'config'  => $arguments,
        ]);

        $this->cOutput->writeln('Processing...');

        $result_file = $error = false;
        try {
            // loop controller
            $loopMax = $this->repo->searchResultCount($arguments);
            $loopMax = ceil($loopMax / self::MAXPERLOOP);

            // file & csv
            $csvExportFile = new ExportCsv("{$this->getType()}-export-data");
            $csvExportFile->writeRowCells($this->getHeaders());

            // query & write
            for ($i = 0; $i < $loopMax; $i++) {
                $results = $this->repo->search($arguments, self::MAXPERLOOP, ($i * self::MAXPERLOOP));
                foreach ($results as $result) {
                    if ($this->getType() == self::TYPE_JOURNALS) {
                        $csvExportFile->writeRowCells($this->convertJournalToRow($result));
                    }
                    elseif ($this->getType() == self::TYPE_CONFERENCES) {
                        $csvExportFile->writeRowCells($this->convertConferenceToRow($result));
                    }
                }
            }

            // closing
            $csvExportFile->closeFile();
            $result_file = $csvExportFile->getFilePath();

            if ($result_file) {
                $this->cOutput->writeln('Export Complete.');

                $filesize = filesize($result_file);
                $filesizeRead = 'Unknown';
                if (false !== $filesize) {
                    $filesizeRead = H::convertHumanReadableSize((float) $filesize, 1);
                }
                $this->cOutput->writeln('Export Size: ' . $filesizeRead);

                // compress >2Mb
                if (
                    (class_exists('ZipArchive')) &&
                    (false !== $filesize) &&
                    (2097152 < $filesize)
                ) {
                    // @codeCoverageIgnoreStart
                    $this->cOutput->writeln('Compressing...');

                    $ext = '.zip';

                    $zip = new \ZipArchive();
                    $zip->open($result_file . $ext, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
                    $zip->addFile($result_file, basename($result_file));
                    $zip->close();

                    if (file_exists($result_file . $ext)) {
                        $result_file .= $ext;
                        chmod($result_file, 0777);
                    }
                    // @codeCoverageIgnoreEnd
                }
            }
        }
        catch (\Exception $e) {
            $error = [
                'Error occur: ' . $e->getMessage(),
                'File       : ' . $e->getFile(),
                'Line       : ' . $e->getLine(),
            ];
            $this->cOutput->writeln($error);
            $error['trace'] = $e->getTrace();
            $this->logger->error($e->getMessage(), $error);
        }

        $buffered = $this->cOutput->fetch();
        $output->write($buffered);

        if (!empty($result_file)) {
            // file path is passed
            $this->cOutput->writeln('Done.');

            $email = [
                'heading'            => "{$this->getType()} Export Data Attached",
                'greeting'           => 'Hi,',
                'content_top'        => 'Export complete, file attached.',
                'pre_content_before' => 'Recorded process:',
                'pre_content'        => $this->_ansi2html($buffered),
            ];
        }
        else {
            if (empty($error)) {
                // @codeCoverageIgnoreStart
                $this->cOutput->writeln('Error occur: Unknown issue.');
                // @codeCoverageIgnoreEnd
            }

            $email = [
                'heading'            => "{$this->getType()} Export Data Failed",
                'greeting'           => 'Hi,',
                'content_top'        => 'Export Incomplete, an error has occur.',
                'pre_content_before' => 'Error below were recorded:',
                'pre_content'        => $this->_ansi2html($buffered),
            ];
        }

        $this->cOutput->writeln('Emailing...');

        // email the file
        $sent = $this->mailSrc
            ->setTemplate('email/boxed.html.twig')
            ->sentEmail(
                $emailAddr,
                "Your {$this->getType()} Export on " . date('Y-m-d H:i', $this->timedStart),
                $email,
                (empty($result_file) ? [] : [$result_file])
            );

        $this->_markCommandEnds((!empty($result_file) ? (!empty($sent)) : false));

        $buffered = $this->cOutput->fetch();
        $output->write($buffered);
    }

    /**
     * Method to convert Input argument to $cfg
     *
     * @return array|null
     */
    protected function getInputsToConfig(): ?array
    {

        return null;
    }

    /**
     * Provide CSV heading
     *
     * @return array
     */
    protected function getHeaders(): array
    {

        return [];
    }

    /**
     * Converting received input into Readable Values, for CLI-table output purposes only.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return string
     */
    protected function translateInputToReadable(string $key, $value): string
    {
        $value = (is_array($value) ? implode(', ', $value) : "{$value}");
        return $value;
    }

    /**
     * Provide CSV heading
     *
     * @param Journal $journal
     *
     * @return array
     */
    protected function convertJournalToRow(Journal $journal): array
    {

        return [];
    }

    /**
     * Provide CSV heading
     *
     * @param Conference $conference
     *
     * @return array
     */
    protected function convertConferenceToRow(Conference $conference): array
    {

        return [];
    }
}
