<?php

namespace App\Repository;

use App\Utility\Helpers as H;
use App\Entity\CountryRegions;
use App\Entity\CountryTaxonomy;
use Doctrine\ORM\EntityRepository;

/**
 * Country Taxonomy Extended Entity Repository.
 *
 * @since  1.2.0
 */
class CountryTaxonomyRepository extends EntityRepository
{
    /**
     * @var  int  Number of Regions max level/depth
     */
    const MAX_LEVEL = 6;

    /**
     * Get list of country-regions hierarchy.
     * Query hierarchical taxonomy
     * - in raw IDs, then converted into its objects.
     *
     * @todo  Cache the structure query
     *
     * @return array<\App\Entity\CountryTaxonomy[]>
     */
    public function getAllLevelRegions(?array $ids = null, bool $obj = true): array
    {
        $select   = [];
        $leftjoin = [];
        $orderby  = [];

        for ($i = 1; $i <= self::MAX_LEVEL; $i++) {

            $select[]  = "`t{$i}`.`id` AS `lev{$i}`";

            $orderby[] = "`lev{$i}` ASC";

            if ($i < self::MAX_LEVEL) {
                $t = ($i + 1);
                if (is_null($ids)) {
                    $leftjoin[] = "LEFT JOIN `country_taxonomy` AS `t{$t}` ON `t{$t}`.`parent` = `t{$i}`.`id`";
                }
                else {
                    $leftjoin[] = "LEFT JOIN `country_taxonomy` AS `t{$t}` ON `t{$t}`.`parent` = `t{$i}`.`id` AND `t{$t}`.`id` IN (" . implode(', ', $ids) . ")";
                }
            }
        }

        $t1where = '';
        if (!is_null($ids)) {
            $t1where = 'AND `t1`.`id` IN (' . implode(', ', $ids) . ')';
        }

        $sql = H::removeExcessWhitespace('
            SELECT
                *
            FROM (
                    SELECT
                        ' . implode(', ', $select) . '
                    FROM
                        `country_taxonomy` AS `t1`
                        ' . implode(' ', $leftjoin) . '
                    WHERE
                        `t1`.`parent` IS NULL
                        ' . $t1where . '
                ) `x`
            ORDER BY
                ' . implode(', ', $orderby) . '
        ');

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        $results = $stmt->fetchAll();

        $levels = [];
        $queried = [];
        foreach($results as $resi => $result) {
            $result = array_values($result);
            $result = array_filter($result);
            $result = array_map('intval', $result);

            foreach($result as $ri => $res) {
                if (isset($queried[$res])) {
                    $aReg = $queried[$res];
                }
                else {
                    $aReg = $queried[$res] = $this->findOneBy(['id' => $res]);
                }

                if (!empty($aReg)) {
                    if ($obj) {
                        // @codeCoverageIgnoreStart
                        $levels[$resi][] = $aReg;
                        // @codeCoverageIgnoreEnd
                    }
                    else {
                        $levels[$resi][] = [
                            'id'   => $aReg->getId(),
                            'name' => $aReg->getName(),
                        ];
                    }
                }
            }
        }

        return $levels;
    }
}
