<?php

namespace App\Repository;

use App\Entity\Conference;
use App\Utility\Helpers as H;
use App\Utility\Repository\DrugRepositoryHelpers;
use Doctrine\ORM\EntityRepository;

/**
 * Conference Repository.
 *
 * @since  1.2.0
 */
class ConferenceRepository extends EntityRepository
{
    /**
     * Defaulting search WHERE condition.
     *
     * @var string
     */
    private $searchConditionBinder = 'AND';

    /**
     * Method to change search binder
     *
     * @param string $binder Expecting 'AND' / 'OR' only.
     *
     * @throws \Exception  If other value given
     */
    public function changeSearchConditionsBinder(?string $binder = 'AND'): void
    {
        $binder = $binder ?? 'AND';

        if (!in_array(strtolower($binder), ['and', 'or'])) {
            throw new \Exception('Invalid search binder.');
        }

        $this->searchConditionBinder = $binder;
    }

    /**
     * Method to handle search query.
     *
     * @param  array  $terms
     * @param  int    $limit
     * @param  int    $offset
     *
     * @return Conference[]
     */
    public function search(array $terms, int $limit = 1000, int $offset = 0): array
    {
        $sql = $this->buildSearchQuery($terms, false, $limit, $offset);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        $ids = $stmt->fetchAll();
        $ids = array_column($ids, 'id');

        $orderBy = [
            'startDate' => 'DESC',
            'endDate'   => 'ASC',
            'name'      => 'ASC',
        ];

        return $this->findBy(['id' => $ids], $orderBy);
    }

    /**
     * Method to check search query result count
     *
     * @param  array  $terms
     *
     * @return int
     */
    public function searchResultCount(array $terms): int
    {
        $sql = $this->buildSearchQuery($terms, true);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to count Conference records count.
     *
     * @return int
     */
    public function countOverallCount(): int
    {
        $sql = 'SELECT COUNT(*) AS `count` FROM `conference`;';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to count Corrupted Conference records count.
     *
     * @return int
     */
    public function countCorruptedCount(): int
    {
        $sql = 'SELECT COUNT(*) AS `count` FROM `conference` WHERE '
            . '`name` IS NULL OR `name` = "" '
            . ' OR '
            . '`start_date` IS NULL OR `start_date` = "" '
            . ' OR '
            . '`end_date` IS NULL OR `end_date` = "" ';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to calculate int-indexed Conference records count.
     *
     * @param string $field
     * @param int    $value
     *
     * @return int
     */
    public function countKeyedCount(string $field, int $value): int
    {
        $sql = 'SELECT COUNT(*) AS `count` FROM `conference` WHERE '
            . "`{$field}` = {$value} ";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get Maximum Relationship count of Conference's Specialty
     *
     * @return int
     */
    public function countMaxSpecialtiesConnection(): int
    {
        $sql = 'SELECT COUNT(*) AS `count` FROM `conference_specialties` GROUP BY `conference_id` ORDER BY `count` DESC LIMIT 1;';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to build Conference search SQL Query.
     * Query should return relevant Conference IDs.
     *
     * @uses  $this->searchConditionBinder  to concatenate search conditions
     *
     * @param  array    $terms      note by-reference
     * @param  boolean  $countOnly  select count(), instead of ID
     * @param  int      $limit
     * @param  int      $offset
     *
     * @return string
     */
    private function buildSearchQuery(array &$terms, bool $countOnly = false, int $limit = 1, int $offset = 0): string
    {
        $terms = [
            'name'          => $terms['name']          ?? '',
            'city'          => $terms['city']          ?? [],
            'country'       => $terms['country']       ?? [],
            'region'        => $terms['region']        ?? [],
            'specialty'     => $terms['specialty']     ?? [],
            'startdate'     => $terms['startdate']     ?? '',
            'enddate'       => $terms['enddate']       ?? '',
            'ids'           => $terms['ids']           ?? '',
            'keycongress'   => $terms['keycongress']   ?? '',
            'oncruise'      => $terms['oncruise']      ?? '',
            'online'        => $terms['online']        ?? '',
            'hasguide'      => $terms['hasguide']      ?? '',
            'discontinued'  => $terms['discontinued']  ?? '',
            'showcorrupted' => $terms['showcorrupted'] ?? '',
        ];

        if ($countOnly) {
            $sql = "SELECT COUNT(*) AS `count` FROM `conference` `c` ";
        }
        else {
            $sql = "SELECT `c`.`id` FROM `conference` `c` ";
        }

        $andWhere = [];
        if (!empty($term = $terms['name'])) {
            DrugRepositoryHelpers::translateQueryToSql($term);
            $andWhere[] = "(`c`.`name` {$term})";
        }

        if (!empty($terms['city'])) {
            $sub = [];
            foreach ($terms['city'] as $city) {
                $city = addslashes($city);
                $sub[] = "(`c`.`city` LIKE \"{$city}\")";
            }
            $andWhere[] = '(' . implode(' OR ', $sub) . ')';
        }

        if (!empty($terms['country'])) {
            $andWhere[] = '(`c`.`country` IN ("' . implode('","', $terms['country']) . '"))';
        }

        if (!empty($terms['region'])) {
            $sql .= "
                RIGHT JOIN `country_taxonomy` `ct` ON (`ct`.`id` = `c`.`region` OR `ct`.`parent` = `c`.`region`)
            ";
            $sub = [
                '(`ct`.`id`     IN (' . implode(',', $terms['region']) . '))',
                '(`ct`.`parent` IN (' . implode(',', $terms['region']) . '))',
            ];
            $andWhere[] = '(' . implode(' OR ', $sub) . ')';
        }

        if (!empty($terms['startdate'])) {
            $andWhere[] = "(`c`.`start_date` >= \"{$terms['startdate']}\")";
        }

        if (!empty($terms['enddate'])) {
            $andWhere[] = "(`c`.`end_date` <= \"{$terms['enddate']}\")";
        }

        if (!empty($term = $terms['ids'])) {
            DrugRepositoryHelpers::translateQueryToSql($term);
            $sub = [
                "(`c`.`id` {$term})",
                "(`c`.`previous_id` {$term})",
                "(`c`.`unique_name` {$term})",
            ];
            $andWhere[] = '(' . implode(' OR ', $sub) . ')';
        }

        if ($terms['keycongress'] != '') {
            $term = filter_var($terms['keycongress'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`c`.`key_event` = {$term})";
        }

        if ($terms['oncruise'] != '') {
            $term = filter_var($terms['oncruise'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`c`.`cruise` = {$term})";
        }

        if ($terms['online'] != '') {
            $term = filter_var($terms['online'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`c`.`online` = {$term})";
        }

        if ($terms['hasguide'] != '') {
            $term = filter_var($terms['hasguide'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`c`.`has_guide` = {$term})";
        }

        if ($terms['discontinued'] != '') {
            $term = filter_var($terms['discontinued'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`c`.`discontinued` = {$term})";
        }

        if ($terms['showcorrupted'] != '') {
            $term = filter_var($terms['showcorrupted'], FILTER_VALIDATE_BOOLEAN);
            if (false == $term) {
                $andWhere[] = "(`c`.`name` IS NOT NULL)";
                $andWhere[] = "(`c`.`name` !=  \"\")";
            }
        }

        if (!empty($terms['specialty'])) {
            $ids = $names = [];
            foreach ($terms['specialty'] as $term) {
                if (is_numeric($term)) {
                    $ids[] = $term;
                }
                else {
                    $names[] = $term;
                }
            }

            if (!empty($ids)) {
                foreach ($ids as $idx => $id) {
                    $sql .= "
                        INNER JOIN
                            `conference_specialties` `csid{$idx}`
                        ON (
                            `csid{$idx}`.`conference_id` = `c`.`id`
                            AND
                            `csid{$idx}`.`specialty_id` = {$id}
                        )
                    ";
                }
            }

            if (!empty($names)) {
                $st = [];
                foreach ($names as $name) {
                    $st[] = "(`st`.`specialty` LIKE \"%{$name}%\")";
                }
                $st = '((`st`.`id` = `cs`.`specialty_id`) AND (' . implode(' OR ', $st) . '))';

                $sql .= "
                    INNER JOIN `conference_specialties` `cs` ON `cs`.`conference_id` = `c`.`id`
                    INNER JOIN `specialty_taxonomy` `st` ON {$st}
                ";
            }
        }

        $orderBy = '';
        if (false == $countOnly) {
            $orderBy = [
                '`c`.`start_date` DESC',
                '`c`.`end_date` ASC',
                '`c`.`name` ASC',
            ];
            $orderBy = ' ORDER BY ' . implode(', ', $orderBy) . ' ';
        }

        $limiter = '';
        if (false == $countOnly) {
            $limiter = " LIMIT {$limit} OFFSET {$offset} ";
        }

        if (!empty($andWhere)) {
            $andWhere = ' WHERE ' . implode(' ' . $this->searchConditionBinder . ' ', $andWhere);
        }
        else {
            $andWhere = '';
        }

        $sql = H::removeExcessWhitespace($sql . $andWhere . $orderBy . $limiter);

        return $sql;
    }
}
