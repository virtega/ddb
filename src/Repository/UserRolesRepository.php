<?php

namespace App\Repository;

use App\Entity\UserRoles;
use App\Security\User\LdapUser;
use App\Security\User\LdapUserProvider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

/**
 * User Roles Entity Repository.
 *
 * @since  1.0.0
 */
class UserRolesRepository extends ServiceEntityRepository
{
    /**
     * @var RoleHierarchy
     * @var RoleHierarchyInterface
     */
    private $roleHierarchy;

    /**
     * @param RegistryInterface       $registry
     * @param RoleHierarchyInterface  $roleHierarchy
     */
    public function __construct(RegistryInterface $registry, RoleHierarchyInterface $roleHierarchy)
    {
        $this->roleHierarchy = $roleHierarchy;

        parent::__construct($registry, UserRoles::class);
    }

    /**
     * Method to query single UserRoles item based on username; case-insensitive.
     * Will generate new one if not existed.
     *
     * @param string  $username
     *
     * @return UserRoles
     */
    public function getUserRoleEnt(string $username): UserRoles
    {
        /**
         * Using LIKE - case-insensitive
         * @var  UserRoles
         */
        $user = $this->findOneBy(array('username' => $username));

        if (empty($user)) {
            $user = new UserRoles($username);
        }
        return $user;
    }

    /**
     * Method to query and assign roles to LdapUser object.
     * Method also create new profile if it's a new user, post query.
     *
     * @param LdapUser      $user
     * @param bool|boolean  $updateProfile  Flag to update/create on Entity
     *
     * @return array    List of Roles
     */
    public function furnishLdapUserRoles(LdapUser &$user, bool $updateProfile = false): array
    {
        $stated = $user->getRoles();

        $role = $this->getUserRoleEnt($user->getUsername());
        $stored = $role->getRoles();
        $stated = array_merge($stated, $stored);
        $stated = $this->sanitizeRoles($stated);

        if ($updateProfile) {
            // capture previous login DT
            if ($role->getId()) {
                $user->setLastLogin( clone $role->getLastLogin() );
            }

            $this->createUpdateUserRoles($role, $user->getUsername(), $stated, true);
        }

        $user->setRoles($stated);

        return $stated;
    }

    /**
     * Method to Create / Update User Roles entity
     *
     * @param UserRoles|null  $role             NULL will create new back on $username
     * @param string          $username         Overwrite username value; If LdapUserProvider, it brings LDAP format (original)
     * @param array           $roles
     * @param boolean         $updateLastLogin  Flag to update last_login
     *
     * @return boolean  Consider if Entity-Storage made it; Result from $em->contains()
     */
    public function createUpdateUserRoles(?UserRoles $role = null, string $username, array $roles, bool $updateLastLogin = false): bool
    {
        $roles = $this->sanitizeRoles($roles);

        if (empty($role)) {
            // @codeCoverageIgnoreStart
            $role = new UserRoles($username);
            // @codeCoverageIgnoreEnd
        }
        else {
            $role->setUsername($username);
        }

        $role->setRoles($roles);

        if ($updateLastLogin) {
            $role->setLastLogin(new \DateTime('now'));
        }

        $this->getEntityManager()->persist($role);
        $this->getEntityManager()->flush();

        return $this->getEntityManager()->contains($role);
    }

    /**
     * Method to clean up list of Roles
     *
     * @param  array    $roles
     *
     * @return array
     */
    private function sanitizeRoles(array $roles): array
    {
        $roles = array_filter($roles);
        $roles = array_unique($roles, SORT_STRING);
        sort($roles);

        return $roles;
    }

    /**
     * Method to get list of Role Hierarchy, with simple role-key
     *
     * @throws \Exception  If RoleHierarchyInterface did not return any; possibly malformed security config
     *
     * @return array
     */
    public function getRoleHierarchy(): array
    {
        static $hierarchy;

        if ($hierarchy == null) {
            $hierarchy = array();

            /**
             * Query from the highest position
             * @var string[] Roles
             */
            $roles = $this->roleHierarchy->getReachableRoleNames([UserRoles::ADMIN]);
            if (empty($roles)) {
                // @codeCoverageIgnoreStart
                throw new \Exception('Unable to get Role Hierarchy settings.', 1);
                // @codeCoverageIgnoreEnd
            }

            // generate list of roles based on security.yaml
            foreach ($roles as $role) {
                /**
                 * @var string[] Roles
                 */
                $set = $this->roleHierarchy->getReachableRoleNames([$role]);
                if (empty($set)) {
                    // @codeCoverageIgnoreStart
                    continue;
                    // @codeCoverageIgnoreEnd
                }

                $key = str_replace('ROLE_', '', $role);
                $key = strtolower($key);
                $hierarchy[$key] = array(
                    'name'  => $role,
                    'roles' => array(),
                );
                foreach ($set as $st) {
                    $hierarchy[$key]['roles'][] = $st;
                }
            }
        }

        return $hierarchy;
    }
}
