<?php

namespace App\Repository;

use App\Entity\Country;
use App\Entity\CountryRegions;
use App\Entity\CountryTaxonomy;
use Doctrine\ORM\EntityRepository;

/**
 * Country Region Extended Entity Repository.
 *
 * @since  1.2.0
 */
class CountryRegionsRepository extends EntityRepository
{
    /**
     * Method to query related Regions for a Country
     *
     * @param Country $country
     *
     * @return array<\App\Entity\CountryRegions>
     */
    public function getCountryRegions(Country $country): array
    {
        $results = $this->findBy(['country' => $country]);

        return $results;
    }

    /**
     * Method to query related Countries for a Region
     *
     * @param CountryTaxonomy $region
     *
     * @return array<\App\Entity\Country>
     */
    public function getRegionCountries(CountryTaxonomy $region): array
    {
        $results = $this->findBy(['region' => $region]);

        return $results;
    }
}
