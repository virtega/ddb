<?php

namespace App\Repository\Drug;

use App\Entity\Brand;
use App\Entity\BrandSynonym;
use App\Entity\BrandTypo;
use App\Entity\Generic;
use App\Utility\Repository\DrugRepositoryHelpers as DRHelpers;
use App\Repository\DrugRepository;

/**
 * All Drug Related Extending Drug Repository.
 *
 * @since  1.0.0
 */
class BrandDrugRepository extends DrugRepository
{
    //
    // BRAND
    //

    /**
     * Get Drug, this is an alias to findOneBy wrapped within shared-repository.
     *
     * @param integer   $id
     * @param string    $by    Field name
     *
     * @return Brand|null
     */
    public function getDrug($id, $by = 'id')
    {
        return $this->getEntityManager()
            ->getRepository(Brand::class)
            ->findOneBy(array($by => $id));
    }

    /**
     * Method to get Brand Drug, and its Subs.
     *
     * @param integer    $id
     * @param string     $by    Field name
     *
     * @return array
     */
    public function getDrugFamily($id, $by = 'id'): array
    {
        $drug = array(
            'main'          => array(),
            'synonyms'      => array(),
            'synonyms_keys' => array(),
            'typos'         => array(),
            'typos_keys'    => array(),
        );

        $drug['main'] = $this->getDrug($id, $by);

        if (!empty($drug['main'])) {
            $drug['synonyms'] = $this->getEntityManager()
                ->getRepository(BrandSynonym::class)
                ->findBy(array('brand' => $drug['main']->getId()));

            DRHelpers::reSortEntityKeywords($drug, 'synonyms');

            $drug['typos'] = $this->getEntityManager()
                ->getRepository(BrandTypo::class)
                ->findBy(array('brand' => $drug['main']->getId()));

            DRHelpers::reSortEntityKeywords($drug, 'typos');
        }

        return $drug;
    }

    /**
     * Update BRAND DRUG.
     *
     * @param integer    $id
     * @param array      $data
     *
     * @return Brand
     */
    public function updateDrug($id = 0, $data): Brand
    {
        if (!empty($id)) {
            /**
             * @var Brand
             */
            $drug = $this->getEntityManager()->getRepository(Brand::class)->find($id);
        }
        else {
            /**
             * @var Brand
             */
            $drug = new Brand();
        }

        if (isset($data['name'])) {
            $drug->setName($data['name']);
        }

        if (isset($data['valid'])) {
            $drug->setValid((int) $data['valid']);
        }

        if (isset($data['auto_encode_explude'])) {
            $drug->setAutoencodeexclude((int) $data['auto_encode_explude']);
        }

        if (isset($data['unid'])) {
            $drug->setUid($data['unid']);
        }

        if (isset($data['comments'])) {
            $drug->setComments($data['comments']);
        }

        if (!empty($data['creation_date'])) {
            $data['creation_date'] = \DateTime::createFromFormat(DRHelpers::DATEFORMAT, $data['creation_date']);
        }
        else {
            $data['creation_date'] = new \DateTime();
        }

        $drug->setCreationDate($data['creation_date']);

        // if only given
        if (isset($data['generic'])) {
            if (empty($data['generic'])) {
                // @codeCoverageIgnoreStart
                $data['generic'] = null;
                // @codeCoverageIgnoreEnd
            }
            elseif (!is_object($data['generic'])) {
                $data['generic'] = $this->getEntityManager()
                    ->getRepository(Generic::class)
                    ->findOneBy(array('id' => $data['generic']));
            }
            $drug->setGeneric($data['generic']);
        }

        $this->getEntityManager()->persist($drug);

        return $drug;
    }

    /**
     * Remove Brand Drug, by removing its' subs (keywords) first.
     *
     * @param integer|Brand    $id
     * @param boolean          $keyword    Remove keywords
     *
     * @return integer      count of removed/unlink items
     */
    public function removeDrug($id, $keyword = true): int
    {
        $count = 0;

        $brand = null;
        if ($id instanceof Brand) {
            /**
             * @var Brand
             */
            $brand = $id;
        }
        else {
            /**
             * @var Brand
             */
            $brand = $this->getEntityManager()
                ->getRepository(Brand::class)
                ->find($id);
        }

        if ((!empty($brand)) && ($keyword)) {
            $family = $this->getDrugFamily($brand->getId());

            if (!empty($family['synonyms'])) {
                foreach ($family['synonyms'] as $sub) {
                    $count += $this->removeSynonym($sub);
                }
            }

            if (!empty($family['typos'])) {
                foreach ($family['typos'] as $sub) {
                    $count += $this->removeTypo($sub);
                }
            }
        }

        $this->getEntityManager()->remove($brand);
        ++$count;

        return $count;
    }

    //
    // BRAND - SYNONYM
    //

    /**
     * Update BRAND DRUG - SYNONYM.
     *
     * @param integer    $id
     * @param array      $data
     *
     * @return BrandSynonym
     */
    public function updateSynonym($id = 0, $data): BrandSynonym
    {
        $keyword = null;
        if (!empty($id)) {
            /**
             * Update
             * @var BrandSynonym
             */
            $keyword = $this->getEntityManager()
                ->getRepository(BrandSynonym::class)
                ->find($id);
        }

        if (empty($keyword)) {
            /**
             * New
             * @var BrandSynonym
             */
            $keyword = new BrandSynonym();
        }

        if (isset($data['name'])) {
            $keyword->setName($data['name']);
        }

        if (isset($data['country'])) {
            $keyword->setCountry((empty($data['country']) ? null : $data['country']));
        }

        if (!is_object($data['brand'])) {
            $data['brand'] = $this->getEntityManager()
                ->getRepository(Brand::class)
                ->findOneBy(array('id' => $data['brand']));
        }
        $keyword->setBrand($data['brand']);

        $this->getEntityManager()->persist($keyword);

        return $keyword;
    }

    /**
     * Delete BRAND DRUG - SYNONYM.
     *
     * @param integer|BrandSynonym      $id
     *
     * @return integer      count of removed/unlink items
     */
    public function removeSynonym($id): int
    {
        $keyword = null;
        if ($id instanceof BrandSynonym) {
            /**
             * @var BrandSynonym
             */
            $keyword = $id;
        }
        else {
            /**
             * @var BrandSynonym
             */
            $keyword = $this->getEntityManager()
                ->getRepository(BrandSynonym::class)
                ->find($id);
        }

        if (!empty($keyword)) {
            $this->getEntityManager()->remove($keyword);
        }

        return (int) (!empty($keyword));
    }

    //
    // BRAND - TYPO
    //

    /**
     * Update BRAND DRUG - TYPO.
     *
     * @param integer    $id
     * @param array      $data
     *
     * @return BrandTypo
     */
    public function updateTypo($id = 0, $data): BrandTypo
    {
        $keyword = null;
        if (!empty($id)) {
            /**
             * Update
             * @var BrandTypo
             */
            $keyword = $this->getEntityManager()
                ->getRepository(BrandTypo::class)
                ->find($id);
        }

        if (empty($keyword)) {
            /**
             * New
             * @var BrandTypo
             */
            $keyword = new BrandTypo();
        }

        if (isset($data['name'])) {
            $keyword->setName($data['name']);
        }

        if (!is_object($data['brand'])) {
            $data['brand'] = $this->getEntityManager()
                ->getRepository(Brand::class)
                ->findOneBy(array('id' => $data['brand']));
        }
        $keyword->setBrand($data['brand']);

        $this->getEntityManager()->persist($keyword);

        return $keyword;
    }

    /**
     * Delete BRAND DRUG - TYPO.
     *
     * @param integer|BrandTypo     $id
     *
     * @return integer      count of removed/unlink items
     */
    public function removeTypo($id): int
    {
        $keyword = null;
        if ($id instanceof BrandTypo) {
            /**
             * @var BrandTypo
             */
            $keyword = $id;
        }
        else {
            /**
             * @var BrandTypo
             */
            $keyword = $this->getEntityManager()
                ->getRepository(BrandTypo::class)
                ->find($id);
        }

        if (!empty($keyword)) {
            $this->getEntityManager()->remove($keyword);
        }

        return (int) (!empty($keyword));
    }
}
