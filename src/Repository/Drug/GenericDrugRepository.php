<?php

namespace App\Repository\Drug;

use App\Entity\Generic;
use App\Entity\GenericSynonym;
use App\Entity\GenericTypo;
use App\Entity\GenericRefLevel1;
use App\Entity\GenericRefLevel2;
use App\Entity\GenericRefLevel3;
use App\Entity\GenericRefLevel4;
use App\Entity\Experimental;
use App\Entity\Brand;
use App\Utility\Repository\DrugRepositoryHelpers as DRHelpers;
use App\Repository\DrugRepository;

/**
 * Generic Drug Extending Drug Repository.
 *
 * @since  1.0.0
 */
class GenericDrugRepository extends DrugRepository
{
    //
    // GENERIC
    //

    /**
     * Get Drug, this is an alias to findOneBy wrapped within shared-repository.
     *
     * @param integer   $id
     * @param string    $by     Field name
     *
     * @return Generic|null
     */
    public function getDrug($id, $by = 'id')
    {
        return $this->getEntityManager()
            ->getRepository(Generic::class)
            ->findOneBy(array($by => $id));
    }

    /**
     * Method to get Brand Drug, and its Subs.
     *
     * @param integer   $id
     * @param string    $by     Field name
     *
     * @return array
     */
    public function getDrugFamily($id, $by = 'id'): array
    {
        $drug = array(
            'main'          => array(),
            'synonyms'      => array(),
            'synonyms_keys' => array(),
            'typos'         => array(),
            'typos_keys'    => array(),
        );

        $drug['main'] = $this->getDrug($id, $by);

        if (!empty($drug['main'])) {
            $drug['synonyms'] = $this->getEntityManager()
                ->getRepository(GenericSynonym::class)
                ->findBy(array('generic' => $drug['main']->getId()));

            DRHelpers::reSortEntityKeywords($drug, 'synonyms');

            $drug['typos'] = $this->getEntityManager()
                ->getRepository(GenericTypo::class)
                ->findBy(array('generic' => $drug['main']->getId()));

            DRHelpers::reSortEntityKeywords($drug, 'typos');
        }

        return $drug;
    }

    /**
     * Update GENERIC DRUG.
     *
     * @param integer   $id
     * @param array     $data
     *
     * @return Generic
     */
    public function updateDrug($id = 0, $data): Generic
    {
        if (!empty($id)) {
            /**
             * @var Generic
             */
            $drug = $this->getEntityManager()->getRepository(Generic::class)->find($id);
        }
        else {
            /**
             *
             * @var Generic
             */
            $drug = new Generic();
        }

        if (isset($data['name'])) {
            $drug->setName($data['name']);
        }

        if (isset($data['valid'])) {
            $drug->setValid((int) $data['valid']);
        }

        if (isset($data['no_double_bounce'])) {
            $drug->setNodoublebounce((int) $data['no_double_bounce']);
        }

        if (isset($data['auto_encode_explude'])) {
            $drug->setAutoencodeexclude((int) $data['auto_encode_explude']);
        }

        if (isset($data['level_1'])) {
            $drug->setLevel1($data['level_1']);
        }

        if (isset($data['level_2'])) {
            $drug->setLevel2($data['level_2']);
        }

        if (isset($data['level_3'])) {
            $drug->setLevel3($data['level_3']);
        }

        if (isset($data['level_4'])) {
            $drug->setLevel4($data['level_4']);
        }

        if (isset($data['level_5'])) {
            $drug->setLevel5($data['level_5']);
        }

        if (isset($data['unid'])) {
            $drug->setUid($data['unid']);
        }

        if (isset($data['comments'])) {
            $drug->setComments($data['comments']);
        }

        if (!empty($data['creation_date'])) {
            $data['creation_date'] = \DateTime::createFromFormat(DRHelpers::DATEFORMAT, $data['creation_date']);
        }
        else {
            $data['creation_date'] = new \DateTime();
        }

        $drug->setCreationDate($data['creation_date']);

        if (isset($data['experimental'])) {
            $experimental = null;
            if (!empty($data['experimental'])) {
                if ($data['experimental'] instanceof Experimental) {
                    /**
                     * @var Experimental
                     */
                    $experimental = $data['experimental'];
                }
                elseif (is_numeric($data['experimental'])) {
                    /**
                     * @var Experimental
                     */
                    $experimental = $this->getEntityManager()
                        ->getRepository(Experimental::class)
                        ->findOneBy(array('id' => $data['experimental']));
                }
            }
            if (!empty($experimental)) {
                $drug->setExperimental($experimental);
            }
        }

        $this->getEntityManager()->persist($drug);

        return $drug;
    }

    /**
     * Remove Generic Drug, by removing its' subs (keywords) first.
     *
     * @param integer|Generic   $id
     * @param boolean           $keyword    Remove keywords
     *
     * @return integer  count of removed/unlink items
     */
    public function removeDrug($id, $keyword = true): int
    {
        $count = 0;

        $generic = null;
        if ($id instanceof Generic) {
            /**
             * @var Generic
             */
            $generic = $id;
        }
        else {
            /**
             * @var Generic
             */
            $generic = $this->getEntityManager()
                ->getRepository(Generic::class)
                ->find($id);
        }

        if (!empty($generic)) {
            if ($keyword) {
                $family = $this->getDrugFamily($generic->getId());

                if (!empty($family['synonyms'])) {
                    foreach ($family['synonyms'] as $sub) {
                        $count += $this->removeSynonym($sub);
                    }
                }

                if (!empty($family['typos'])) {
                    foreach ($family['typos'] as $sub) {
                        $count += $this->removeTypo($sub);
                    }
                }
            }

            /**
             * remove uplink
             * @var Brand[]
             */
            $uplinks = $this->getEntityManager()
                ->getRepository(Brand::class)
                ->findBy(array('generic' => $generic->getId()));
            foreach ($uplinks as $uplink) {
                $uplink->setGeneric(null);
                $this->getEntityManager()->persist($uplink);
                ++$count;
            }

            $this->getEntityManager()->remove($generic);
            ++$count;
        }

        return $count;
    }

    //
    // GENERIC - SYNONYM
    //

    /**
     * Update GENERIC DRUG - SYNONYM.
     *
     * @param integer   $id
     * @param array     $data
     *
     * @return GenericSynonym
     */
    public function updateSynonym($id = 0, $data): GenericSynonym
    {
        $keyword = null;
        if (!empty($id)) {
            /**
             * Update
             * @var GenericSynonym
             */
            $keyword = $this->getEntityManager()
                ->getRepository(GenericSynonym::class)
                ->find($id);
        }

        if (empty($keyword)) {
            /**
             * New
             * @var GenericSynonym
             */
            $keyword = new GenericSynonym();
        }

        if (isset($data['name'])) {
            $keyword->setName($data['name']);
        }

        if (isset($data['country'])) {
            $keyword->setCountry((empty($data['country']) ? null : $data['country']));
        }

        if (!is_object($data['generic'])) {
            $data['generic'] = $this->getEntityManager()
                ->getRepository(Generic::class)
                ->findOneBy(array('id' => $data['generic']));
        }
        $keyword->setGeneric($data['generic']);

        $this->getEntityManager()->persist($keyword);

        return $keyword;
    }

    /**
     * Delete GENERIC DRUG - SYNONYM.
     *
     * @param integer|GenericSynonym    $id
     *
     * @return integer  count of removed/unlink items
     */
    public function removeSynonym($id): int
    {
        $keyword = null;
        if ($id instanceof GenericSynonym) {
            /**
             * @var GenericSynonym
             */
            $keyword = $id;
        }
        else {
            /**
             * @var GenericSynonym
             */
            $keyword = $this->getEntityManager()
                ->getRepository(GenericSynonym::class)
                ->find($id);
        }

        if (!empty($keyword)) {
            $this->getEntityManager()->remove($keyword);
        }

        return (int) (!empty($keyword));
    }

    //
    // GENERIC - TYPO
    //

    /**
     * Update GENERIC DRUG - TYPO.
     *
     * @param int       $id
     * @param array     $data
     *
     * @return GenericTypo
     */
    public function updateTypo($id = 0, $data): GenericTypo
    {
        $keyword = null;
        if (!empty($id)) {
            /**
             * Update
             * @var GenericTypo
             */
            $keyword = $this->getEntityManager()
                ->getRepository(GenericTypo::class)
                ->find($id);
        }

        if (empty($keyword)) {
            /**
             * New
             * @var GenericTypo
             */
            $keyword = new GenericTypo();
        }

        if (isset($data['name'])) {
            $keyword->setName($data['name']);
        }

        if (!is_object($data['generic'])) {
            $data['generic'] = $this->getEntityManager()
                ->getRepository(Generic::class)
                ->findOneBy(array('id' => $data['generic']));
        }
        $keyword->setGeneric($data['generic']);

        $this->getEntityManager()->persist($keyword);

        return $keyword;
    }

    /**
     * Delete GENERIC DRUG - TYPO.
     *
     * @param integer|GenericTypo   $id
     *
     * @return integer      count of removed/unlink items
     */
    public function removeTypo($id): int
    {
        $keyword = null;
        if ($id instanceof GenericTypo) {
            /**
             * @var GenericTypo
             */
            $keyword = $id;
        }
        else {
            /**
             * @var GenericTypo
             */
            $keyword = $this->getEntityManager()
                ->getRepository(GenericTypo::class)
                ->find($id);
        }

        if (!empty($keyword)) {
            $this->getEntityManager()->remove($keyword);
        }

        return (int) (!empty($keyword));
    }

    //
    // REFERENCE LEVEL
    //

    /**
     * Method to return Generic Ref Level 1.
     *
     * @return array
     */
    public function getAllGenericRefLevel1(): array
    {
        /**
         * @var GenericRefLevel1[]
         */
        $records = $this->getEntityManager()
            ->getRepository(GenericRefLevel1::class)
            ->findBy(array(), array('code' => 'ASC'));

        $array = array();
        foreach ($records as $record) {
            $array[$record->getCode()] = array(
                'code' => $record->getCode(),
                'name' => $record->getName(),
            );
        }

        return $array;
    }

    /**
     * Method to return Generic Ref Level 2.
     *
     * @return array
     */
    public function getAllGenericRefLevel2(): array
    {
        /**
         * @var GenericRefLevel2[]
         */
        $records = $this->getEntityManager()
            ->getRepository(GenericRefLevel2::class)
            ->findBy(array(), array('code' => 'ASC'));

        $array = array();
        foreach ($records as $record) {
            $array[$record->getCode()] = array(
                'code' => $record->getCode(),
                'name' => $record->getName(),
            );
        }

        return $array;
    }

    /**
     * Method to return Generic Ref Level 3.
     *
     * @return array
     */
    public function getAllGenericRefLevel3(): array
    {
        /**
         * @var GenericRefLevel3[]
         */
        $records = $this->getEntityManager()
            ->getRepository(GenericRefLevel3::class)
            ->findBy(array(), array('code' => 'ASC'));

        $array = array();
        foreach ($records as $record) {
            $array[$record->getCode()] = array(
                'code' => $record->getCode(),
                'name' => $record->getName(),
            );
        }

        return $array;
    }

    /**
     * Method to return Generic Ref Level 4.
     *
     * @return array
     */
    public function getAllGenericRefLevel4(): array
    {
        /**
         * @var GenericRefLevel4[]
         */
        $records = $this->getEntityManager()
            ->getRepository(GenericRefLevel4::class)
            ->findBy(array(), array('code' => 'ASC'));

        $array = array();
        foreach ($records as $record) {
            $array[$record->getCode()] = array(
                'code' => $record->getCode(),
                'name' => $record->getName(),
            );
        }

        return $array;
    }
}
