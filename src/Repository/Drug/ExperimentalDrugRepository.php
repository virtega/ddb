<?php

namespace App\Repository\Drug;

use App\Entity\Experimental;
use App\Entity\Generic;
use App\Repository\DrugRepository;
use App\Utility\Repository\DrugRepositoryHelpers as DRHelpers;

/**
 * Experimental Drug Extending Drug Repository.
 *
 * @since  1.0.0
 */
class ExperimentalDrugRepository extends DrugRepository
{
    /**
     * Get Drug, this is an alias to findOneBy wrapped within shared-repository.
     *
     * @param integer    $id
     * @param string     $by    Field name
     *
     * @return Experimental|null
     */
    public function getDrug($id, $by = 'id')
    {
        return $this->getEntityManager()
            ->getRepository(Experimental::class)
            ->findOneBy(array($by => $id));
    }

    /**
     * Method to return Experimental Drug Family data.
     * ** This Drug don't have family, just formality to other Drug Types.
     *
     * @param integer     $id
     * @param string      $by    Field name
     *
     * @return array    Indexed Array of Experimental object
     */
    public function getDrugFamily($id, $by = 'id'): array
    {
        $drug = array(
            'main' => $this->getDrug($id, $by),
        );

        return $drug;
    }

    /**
     * Update EXPERIMENTAL DRUG.
     *
     * @param integer   $id
     * @param array     $data
     *
     * @return Experimental
     */
    public function updateDrug($id = 0, $data): Experimental
    {
        $drug = null;
        if (!empty($id)) {
            /**
             * @var Experimental
             */
            $drug = $this->getEntityManager()
                ->getRepository(Experimental::class)
                ->find($id);
        }

        if (empty($drug)) {
            /**
             * @var Experimental
             */
            $drug = new Experimental();
        }

        if (isset($data['name'])) {
            $drug->setName($data['name']);
        }

        if (isset($data['type'])) {
            $drug->setType($data['type']);
        }

        if (isset($data['valid'])) {
            $drug->setValid((int) $data['valid']);
        }

        if (isset($data['unid'])) {
            $drug->setUid($data['unid']);
        }

        if (isset($data['comments'])) {
            $drug->setComments($data['comments']);
        }

        if (!empty($data['creation_date'])) {
            $data['creation_date'] = \DateTime::createFromFormat(DRHelpers::DATEFORMAT, $data['creation_date']);
        }
        else {
            $data['creation_date'] = new \DateTime();
        }

        $drug->setCreationDate($data['creation_date']);

        $this->getEntityManager()->persist($drug);

        return $drug;
    }

    /**
     * Remove Experimental Drug.
     *
     * @param int|Experimental     $id
     * @param boolean              $keyword    Remove keywords; this is not in use for now, just standardization
     *
     * @return integer    count of removed/unlink items
     */
    public function removeDrug($id, $keyword = true): int
    {
        $count = 0;

        $experimental = null;
        if ($id instanceof Experimental) {
            /**
             * @var Experimental
             */
            $experimental = $id;
        }
        else {
            /**
             * @var Experimental
             */
            $experimental = $this->getEntityManager()
                ->getRepository(Experimental::class)
                ->find($id);
        }

        if (!empty($experimental)) {
            // remove uplink reference
            /**
             * @var Generic[]
             */
            $uplinks = $this->getEntityManager()
                ->getRepository(Generic::class)
                ->findBy(array('experimental' => $experimental->getId()));

            foreach ($uplinks as $uplink) {
                $uplink->setExperimental(null);
                $this->getEntityManager()->persist($uplink);
                ++$count;
            }

            // remove
            $this->getEntityManager()->remove($experimental);
            ++$count;
        }

        return $count;
    }
}
