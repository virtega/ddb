<?php

namespace App\Repository;

use App\Entity\Experimental;
use App\Entity\Generic;
use App\Entity\GenericSynonym;
use App\Entity\GenericTypo;
use App\Entity\Brand;
use App\Entity\BrandSynonym;
use App\Entity\BrandTypo;
use App\Entity\Variable;
use Doctrine\ORM\EntityRepository;

/**
 * Extending Entity Repository.
 *
 * @since  1.0.0
 */
class VariableRepository extends EntityRepository
{
    /**
     * Method to get a Variable based on name.
     *
     * @param string  $name
     */
    public function getVariable($name)
    {
        return $this->getEntityManager()
            ->getRepository(Variable::class)
            ->findOneBy(array('name' => $name));
    }

    /**
     * Method to add/update Variable value.
     *
     * @param string   $name
     * @param mixed    $value
     * @param string   $type
     * @param boolean  $flush
     *
     * @return Variable
     */
    public function updateWidgetValue(string $name, $value, string $type = 'integer', $flush = false): Variable
    {
        $var = $this->getVariable($name);

        if (empty($var)) {
            $var = new Variable($name);
        }

        $var->setType($type);

        $var->setValue($value);

        $this->getEntityManager()->persist($var);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $var;
    }

    /**
     * Method to return count of Experimental based on filters on Argument.
     *
     * @param string|null $type
     * @param int|null    $valid
     *
     * @return integer
     */
    public function getExperimentalDrugCount(?string $type = null, ?int $valid = null): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select($qb->expr()->count('t.id'))->from(Experimental::class, 't');

        if (null !== $type) {
            $qb->andWhere('t.type = ?1')->setParameter(1, $type);
        }

        if (null !== $valid) {
            $qb->andWhere('t.valid = ?2')->setParameter(2, $valid);
        }

        $qr = $qb->getQuery();

        return (int) $qr->getSingleScalarResult();
    }

    /**
     * Method to return count of Generic based on filters on Argument.
     *
     * @param int|null      $valid
     * @param boolean|null  $orphan
     * @param boolean       $hasCode
     *
     * @return integer
     */
    public function getGenericDrugCount(?int $valid = null, ?bool $orphan = null, bool $hasCode = true): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select($qb->expr()->count('t.id'))->from(Generic::class, 't');

        if ($hasCode) {
            $qb->andWhere(
                $qb->expr()->orx(
                    $qb->expr()->neq('t.level1', '\'\''),
                    $qb->expr()->neq('t.level2', '\'\''),
                    $qb->expr()->neq('t.level3', '\'\''),
                    $qb->expr()->neq('t.level4', '\'\''),
                    $qb->expr()->neq('t.level5', '\'\'')
                )
            );
        }

        if (null !== $orphan) {
            if ($orphan) {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNull('t.experimental'),
                        $qb->expr()->eq('t.experimental', 0)
                    )
                );
            }
            else {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNotNull('t.experimental'),
                        $qb->expr()->gte('t.experimental', 1)
                    )
                );
            }
        }

        if (null !== $valid) {
            $qb->andWhere('t.valid = ?2')->setParameter(2, $valid);
        }

        $qr = $qb->getQuery();

        return (int) $qr->getSingleScalarResult();
    }

    /**
     * Method to return count of Brand based on filters on Argument.
     *
     * @param int|null      $valid
     * @param boolean|null  $orphan
     *
     * @return integer
     */
    public function getBrandDrugCount(?int $valid = null, ?bool $orphan = null): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select($qb->expr()->count('t.id'))->from(Brand::class, 't');

        if (null !== $orphan) {
            if ($orphan) {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNull('t.generic'),
                        $qb->expr()->eq('t.generic', 0)
                    )
                );
            }
            else {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNotNull('t.generic'),
                        $qb->expr()->gte('t.generic', 1)
                    )
                );
            }
        }

        if (null !== $valid) {
            $qb->andWhere('t.valid = ?2')->setParameter(2, $valid);
        }

        $qr = $qb->getQuery();

        return (int) $qr->getSingleScalarResult();
    }

    /**
     * Method to Count Genetic Synonyms.
     *
     * @param boolean  $orphan
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return integer
     */
    public function getGenericSynonymCount($orphan = null, bool $hasCode = true): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select($qb->expr()->count('t.id'))->from(GenericSynonym::class, 't');

        if ($hasCode) {
            $qb->innerJoin('t.generic', 'g');
            $qb->andWhere(
                $qb->expr()->orx(
                    $qb->expr()->neq('g.level1', '\'\''),
                    $qb->expr()->neq('g.level2', '\'\''),
                    $qb->expr()->neq('g.level3', '\'\''),
                    $qb->expr()->neq('g.level4', '\'\''),
                    $qb->expr()->neq('g.level5', '\'\'')
                )
            );
        }

        if (null !== $orphan) {
            if ($orphan) {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNull('t.generic'),
                        $qb->expr()->eq('t.generic', 0)
                    )
                );
            }
            else {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNotNull('t.generic'),
                        $qb->expr()->gte('t.generic', 1)
                    )
                );
            }
        }

        $qr = $qb->getQuery();

        return (int) $qr->getSingleScalarResult();
    }

    /**
     * Method to Count Genetic Typos.
     *
     * @param boolean  $orphan
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return integer
     */
    public function getGenericTypoCount($orphan = null, bool $hasCode = true): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select($qb->expr()->count('t.id'))->from(GenericTypo::class, 't');

        if ($hasCode) {
            $qb->innerJoin('t.generic', 'g');
            $qb->andWhere(
                $qb->expr()->orx(
                    $qb->expr()->neq('g.level1', '\'\''),
                    $qb->expr()->neq('g.level2', '\'\''),
                    $qb->expr()->neq('g.level3', '\'\''),
                    $qb->expr()->neq('g.level4', '\'\''),
                    $qb->expr()->neq('g.level5', '\'\'')
                )
            );
        }

        if (null !== $orphan) {
            if ($orphan) {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNull('t.generic'),
                        $qb->expr()->eq('t.generic', 0)
                    )
                );
            }
            else {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNotNull('t.generic'),
                        $qb->expr()->gte('t.generic', 1)
                    )
                );
            }
        }

        $qr = $qb->getQuery();

        return (int) $qr->getSingleScalarResult();
    }

    /**
     * Method to Count Brand Synonyms.
     *
     * @param boolean  $orphan
     *
     * @return integer
     */
    public function getBrandSynonymCount($orphan = null): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select($qb->expr()->count('t.id'))->from(BrandSynonym::class, 't');

        if (null !== $orphan) {
            if ($orphan) {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNull('t.brand'),
                        $qb->expr()->eq('t.brand', 0)
                    )
                );
            }
            else {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNotNull('t.brand'),
                        $qb->expr()->gte('t.brand', 1)
                    )
                );
            }
        }

        $qr = $qb->getQuery();

        return (int) $qr->getSingleScalarResult();
    }

    /**
     * Method to Count Brand Typos.
     *
     * @param boolean  $orphan
     *
     * @return integer
     */
    public function getBrandTypoCount($orphan = null): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select($qb->expr()->count('t.id'))->from(BrandTypo::class, 't');

        if (null !== $orphan) {
            if ($orphan) {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNull('t.brand'),
                        $qb->expr()->eq('t.brand', 0)
                    )
                );
            }
            else {
                $qb->andWhere(
                    $qb->expr()->orx(
                        $qb->expr()->isNotNull('t.brand'),
                        $qb->expr()->gte('t.brand', 1)
                    )
                );
            }
        }

        $qr = $qb->getQuery();

        return (int) $qr->getSingleScalarResult();
    }

    /**
     * Method to get Specific Count which cover all 3 drugs.
     *
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return integer
     */
    public function getSpecificEBCount(bool $hasCode = true): int
    {
        $sql = array();

        $sql[] = 'SELECT';
        $sql[] = 'COUNT(*) AS e_b_specific';

        $sql[] = 'FROM';
        $sql[] = '(';

            $sql[] = 'SELECT';
            $sql[] = 'e.id as experimental,';
            $sql[] = 'g.id as generic';
            $sql[] = 'FROM';
            $sql[] = 'experimental e';
            $sql[] = 'JOIN generic g ON e.id = g.experimental';

            if ($hasCode) {
                $sql[] = ' AND ( ';
                    $sql[] = ' g.level1 <> \'\' OR ';
                    $sql[] = ' g.level2 <> \'\' OR ';
                    $sql[] = ' g.level3 <> \'\' OR ';
                    $sql[] = ' g.level4 <> \'\' OR ';
                    $sql[] = ' g.level5 <> \'\' ';
                $sql[] = ' ) ';
            }

            $sql[] = 'JOIN brand b ON g.id = b.generic';
            $sql[] = 'GROUP BY e.id, g.id';

        $sql[] = ') c';
        $sql = implode(' ', $sql);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get Specific Count which cover Experimental-Generic drugs.
     *
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return integer
     */
    public function getSpecificEGCount(bool $hasCode = true): int
    {
        $sql = array();

        $sql[] = 'SELECT';
        $sql[] = 'COUNT(*) AS e_g_specific';

        $sql[] = 'FROM';
        $sql[] = '(';

            $sql[] = 'SELECT';
            $sql[] = 'e.id';
            $sql[] = 'FROM';
            $sql[] = 'experimental e';
            $sql[] = 'JOIN generic g ON e.id = g.experimental';

            if ($hasCode) {
                $sql[] = ' AND ( ';
                    $sql[] = ' g.level1 <> \'\' OR ';
                    $sql[] = ' g.level2 <> \'\' OR ';
                    $sql[] = ' g.level3 <> \'\' OR ';
                    $sql[] = ' g.level4 <> \'\' OR ';
                    $sql[] = ' g.level5 <> \'\' ';
                $sql[] = ' ) ';
            }

            $sql[] = 'WHERE';
            $sql[] = 'g.id NOT IN (';

                $sql[] = 'SELECT';
                $sql[] = 'g.id as generic';
                $sql[] = 'FROM';
                $sql[] = 'experimental e';
                $sql[] = 'JOIN generic g ON e.id = g.experimental';
                $sql[] = 'JOIN brand b ON g.id = b.generic';
                $sql[] = 'GROUP BY g.id';

            $sql[] = ')';

        $sql[] = 'GROUP BY e.id';
        $sql[] = ') c';

        $sql = implode(' ', $sql);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get Specific Count which cover Generic-Brand drugs.
     *
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return integer
     */
    public function getSpecificGBCount(bool $hasCode = true): int
    {
        $sql = array();

        $sql[] = 'SELECT';
        $sql[] = 'COUNT(*) AS g_b_specific';

        $sql[] = 'FROM';
        $sql[] = '(';

            $sql[] = 'SELECT';
            $sql[] = 'g.id';

            $sql[] = 'FROM';
            $sql[] = 'generic g';
            $sql[] = 'JOIN brand b ON g.id = b.generic';

            $sql[] = 'WHERE';
            $sql[] = 'b.id NOT IN (';

                $sql[] = 'SELECT';
                $sql[] = 'b.id as brand';
                $sql[] = 'FROM';
                $sql[] = 'experimental e';
                $sql[] = 'JOIN generic g ON e.id = g.experimental';
                $sql[] = 'JOIN brand b ON g.id = b.generic';
                $sql[] = 'GROUP BY b.id';

            $sql[] = ')';

            if ($hasCode) {
                $sql[] = ' AND ( ';
                    $sql[] = ' g.level1 <> \'\' OR ';
                    $sql[] = ' g.level2 <> \'\' OR ';
                    $sql[] = ' g.level3 <> \'\' OR ';
                    $sql[] = ' g.level4 <> \'\' OR ';
                    $sql[] = ' g.level5 <> \'\' ';
                $sql[] = ' ) ';
            }

        $sql[] = 'GROUP BY g.id';

        $sql[] = ') c';

        $sql = implode(' ', $sql);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get Specific Count which cover Brand-Generic drugs.
     *
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return integer
     */
    public function getSpecificBGCount(bool $hasCode = true): int
    {
        $sql = array();

        $sql[] = 'SELECT';
        $sql[] = 'COUNT(*) AS b_g_specific';

        $sql[] = 'FROM';
        $sql[] = '(';

            $sql[] = 'SELECT';
            $sql[] = 'b.id';

            $sql[] = 'FROM';
            $sql[] = 'brand b';
            $sql[] = 'JOIN generic g ON g.id = b.generic';

            $sql[] = 'WHERE';
            $sql[] = 'g.id NOT IN (';

                $sql[] = 'SELECT';
                $sql[] = 'g.id as generic';
                $sql[] = 'FROM';
                $sql[] = 'experimental e';
                $sql[] = 'JOIN generic g ON e.id = g.experimental';
                $sql[] = 'JOIN brand b ON g.id = b.generic';
                $sql[] = 'GROUP BY g.id';

            $sql[] = ')';

            if ($hasCode) {
                $sql[] = ' AND ( ';
                    $sql[] = ' g.level1 <> \'\' OR ';
                    $sql[] = ' g.level2 <> \'\' OR ';
                    $sql[] = ' g.level3 <> \'\' OR ';
                    $sql[] = ' g.level4 <> \'\' OR ';
                    $sql[] = ' g.level5 <> \'\' ';
                $sql[] = ' ) ';
            }

        $sql[] = 'GROUP BY b.id';

        $sql[] = ') c';

        $sql = implode(' ', $sql);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get Specific Count which Standalone Experimental drugs.
     *
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return integer
     */
    public function getSpecificExperimentalCount(bool $hasCode = true): int
    {
        $sql = array();

        $sql[] = 'SELECT';
        $sql[] = 'COUNT(*) AS e_specific';

        $sql[] = 'FROM';
        $sql[] = 'experimental e';
        $sql[] = 'LEFT JOIN generic g ON e.id = g.experimental';

        if ($hasCode) {
            $sql[] = ' AND ( ';
                $sql[] = ' g.level1 <> \'\' OR ';
                $sql[] = ' g.level2 <> \'\' OR ';
                $sql[] = ' g.level3 <> \'\' OR ';
                $sql[] = ' g.level4 <> \'\' OR ';
                $sql[] = ' g.level5 <> \'\' ';
            $sql[] = ' ) ';
        }

        $sql[] = 'WHERE';
        $sql[] = 'g.experimental IS null';

        $sql = implode(' ', $sql);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get Specific Count which Standalone Generic drugs.
     *
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return integer
     */
    public function getSpecificGenericCount(bool $hasCode = true): int
    {
        $sql = array();

        $sql[] = 'SELECT';
        $sql[] = 'COUNT(*) AS g_specific';

        $sql[] = 'FROM';
        $sql[] = 'generic g';
        $sql[] = 'LEFT JOIN brand b ON g.id = b.generic';

        $sql[] = 'WHERE';
        $sql[] = 'g.experimental IS null';
        $sql[] = 'AND';
        $sql[] = 'b.generic IS null';

        if ($hasCode) {
            $sql[] = ' AND ( ';
                $sql[] = ' g.level1 <> \'\' OR ';
                $sql[] = ' g.level2 <> \'\' OR ';
                $sql[] = ' g.level3 <> \'\' OR ';
                $sql[] = ' g.level4 <> \'\' OR ';
                $sql[] = ' g.level5 <> \'\' ';
            $sql[] = ' ) ';
        }

        $sql = implode(' ', $sql);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get Specific Count which Standalone Brand drugs.
     *
     * @param boolean  $hasCode  If Generic has Full Code
     *
     * @return integer
     */
    public function getSpecificBrandCount(bool $hasCode = true): int
    {
        $sql = array();

        $sql[] = 'SELECT';
        $sql[] = 'COUNT(*) AS b_specific';

        $sql[] = 'FROM';
        $sql[] = 'brand b';
        $sql[] = 'LEFT JOIN generic g ON g.id = b.generic';

        if ($hasCode) {
            $sql[] = ' AND ( ';
                $sql[] = ' g.level1 <> \'\' OR ';
                $sql[] = ' g.level2 <> \'\' OR ';
                $sql[] = ' g.level3 <> \'\' OR ';
                $sql[] = ' g.level4 <> \'\' OR ';
                $sql[] = ' g.level5 <> \'\' ';
            $sql[] = ' ) ';
        }

        $sql[] = 'WHERE';
        $sql[] = 'b.generic IS null';

        $sql = implode(' ', $sql);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get Populated Count of field in Drug Table.
     *
     * @param string  $drug   Drug Table name, Expects: experimental, generic, brand
     * @param string  $field  Table Field name
     *
     * @return integer
     */
    public function getDrugSpecificFieldCount($drug, $field): int
    {
        $sql = array();

        $sql[] = 'SELECT';
        $sql[] = 'COUNT(*) AS `count`';

        $sql[] = 'FROM';
        $sql[] = "`{$drug}`";

        $sql[] = 'WHERE';
        $sql[] = "`{$field}` IS NOT null";
        $sql[] = 'OR';
        $sql[] = "`{$field}` != ''";

        $sql = implode(' ', $sql);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }
}
