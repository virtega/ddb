<?php

namespace App\Repository;

use App\Entity\Brand;
use App\Entity\Experimental;
use App\Entity\Generic;
use App\Utility\ExportCsv;
use App\Utility\Repository\DrugRepositoryHelpers as DRHelpers;
use App\Service\Factory\SearchServiceFactory;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Extending Entity Repository.
 *
 * @since  1.0.0
 */
class DrugRepository extends EntityRepository
{
    /**
     * App\Utility\ExportCsv.
     *
     * @var object File
     */
    private $csvExportFile;

    /**
     * Method to trigger Db Persist.
     *
     * @param mixed     $entity     Any Related Object
     */
    public function persist($entity): void
    {
        $this->getEntityManager()->persist($entity);
    }

    /**
     * Method to trigger Db Flush.
     */
    public function flushAllNow(): void
    {
        $this->getEntityManager()->flush();
    }

    /**
     * Method to query all related drug, base on given Entity & ID.
     *
     * @param string        $entity         Expected 'experimental', 'generic', 'brand'
     * @param integer       $id             Entity ID
     * @param boolean       $idOnly         Flag to Keep Redundant data, avoid Concatenation
     * @param boolean       $complete       ($keepRelation == false only) Flag to return as object
     *
     * @return array    $complete: - false*: Associative array with list of ID
     *                             - true: Associative array with list of Entity objects
     */
    public function queryDrugFamily(string $entity, int $id, bool $idOnly = false, bool $complete = false): array
    {
        $ent_key = substr($entity, 0, 1);

        $sql = array();
        if (!$idOnly) {
            $sql[] = 'SELECT ';
            $sql[] = "  GROUP_CONCAT(DISTINCT(`experimental`) SEPARATOR '||') AS `experimental`,";
            $sql[] = "  GROUP_CONCAT(DISTINCT(`generic`) SEPARATOR '||') AS `generic`,";
            $sql[] = "  GROUP_CONCAT(DISTINCT(`brand`) SEPARATOR '||') AS `brand`";
            $sql[] = 'FROM (';
        }

        $sql[] = 'SELECT ';
        $sql[] = '  `e`.`id` AS `experimental`, ';
        $sql[] = '  `g`.`id` AS `generic`, ';
        $sql[] = '  `b`.`id` AS `brand`';
        $sql[] = 'FROM';

        switch ($entity) {
            case 'experimental':
                $sql[] = '  `experimental` `e`';
                $sql[] = '  LEFT JOIN `generic` `g` ON `g`.`experimental` = `e`.`id`';
                $sql[] = '  LEFT JOIN `brand` `b` ON `b`.`generic` = `g`.`id`';
                break;

            case 'generic':
                $sql[] = '  `generic` `g`';
                $sql[] = '  LEFT JOIN `experimental` `e` ON `g`.`experimental` = `e`.`id`';
                $sql[] = '  LEFT JOIN `brand` `b` ON `b`.`generic` = `g`.`id`';
                break;

            case 'brand':
                $sql[] = '  `brand` `b`';
                $sql[] = '  LEFT JOIN `generic` `g` ON `b`.`generic` = `g`.`id`';
                $sql[] = '  LEFT JOIN `experimental` `e` ON `g`.`experimental` = `e`.`id`';
                break;
        }

        $sql[] = 'WHERE';
        $sql[] = "  `{$ent_key}`.`id` = {$id}";

        if (!$idOnly) {
            $sql[] = ') `q`';
            $sql[] = 'GROUP BY ';
            $sql[] = "  `q`.`{$entity}`";
        }

        $sql = implode(' ', $sql);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        if (!$idOnly) {
            // concatenated
            $result = $stmt->fetch();
        }
        else {
            // redundant data
            $result = $stmt->fetchAll();
        }
        if (false === $result) {
            return array();
        }

        $result = (array) $result;

        if (!$idOnly) {
            foreach ($result as $rk => $res) {
                $result[$rk] = explode('||', $res);
                $result[$rk] = array_filter($result[$rk]);
            }

            if ($complete) {
                foreach ($result as $type => $list) {
                    foreach ($list as $li => $id) {
                        $repo               = $this->_getDrugRepo($type);
                        $result[$type][$li] = $repo->getDrugFamily($id);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Method for querying Search for either Search Page / API / Auto-Complete
     *
     * @param  SearchServiceFactory $src
     *
     * @return array
     * - For Export: $src->isExport()
     *     - "file-path"     -> string path to exported file; @see ExportCsv::getFilePath()
     *
     * - For Auto-Complete: $src->isAutoComplete()
     *     - "overall-count"  -> boolean    Always FALSE
     *     - "search-results" -> array      List of result
     *
     *  - For Search Page / API
     *      - "overall-count"  -> integer   Original number of found Drug, limited by SearchServiceFactory::getLimitMain()
     *     - "search-results"  -> array     List of result
     */
    public function queryDrugSearch(SearchServiceFactory $src): array
    {
        $results = array();
        if (false == $src->hasGivenList()) {
            // FULL SQL
            $sql  = DRHelpers::getDrugSearchSql($src, false);
            $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
            $stmt->execute();

            // EXECUTE
            $results = $stmt->fetchAll();
        }

        // OUTPUT FOR EXPORT
        if ($src->isExport()) {
            $data = array();

            if ($src->hasGivenList()) {
                $data = $src->getGivenList();
                // reformat IDs into array-keys with value 0
                foreach ($data as $type => $ids) {
                    $data[$type] = array_combine(array_values($ids), array_fill(0, count($ids), 0));
                }
            }
            else {
                foreach ($results as $ri => $result) {
                    DRHelpers::processSearchResultForDownload($data, $result);
                }
            }

            return array(
                'file-path' => $this->processDataForDownload($data, $src->isExportComplete(), $src->getExportPrefix()),
            );
        }

        // OUTPUT FOR SEARCH PAGE/API OR AUTO-COMPLETE
        $count = 0;
        if (false == $src->hasGivenList()) {
            $stmt = $this->getEntityManager()->getConnection()->prepare('SELECT FOUND_ROWS()');
            $stmt->execute();
            $count = $stmt->fetchColumn();
        }

        $output = array();
        foreach ($results as $ri => $result) {
            if ($src->isAutoComplete()) {
                $output[] = DRHelpers::processSearchResultForAutoComplete($result, $src->getLimitSubs());
            }
            else {
                $output[] = DRHelpers::processSearchResultForSearchPage($result, $src->getLimitSubs());
            }
        }

        return array(
            'overall-count'  => $count,
            'search-results' => $output,
        );
    }

    /**
     * Method to prepare result for Download Array.
     * For most uses this is Step 2, consist of following,
     * but CoreService::queryFindByIdsForExport can jump directly here.
     *    - ensure no duplicate result
     *    - query Entity.
     *
     * @param array     $data       List of Ids within and Entity keys
     * @param boolean   $complete   Flag for download-full
     * @param string    $csvPrefix  CSV Filename Prefix
     *
     * @return string
     */
    public function processDataForDownload(array $data, bool $complete, string $csvPrefix = 'drug-search-export'): string
    {
        // gathering ids; family loop, this process will convert uneven family members to rows
        // so it will gather; "e.id|g.id|b.id"; having this inline, then only time to query the entity for print
        $data_sets_keys = array();

        // max number of keywords, for CSV headings - prepareExportHeadings()
        $max_keyword_count = array();
        if ($complete) {
            $max_keyword_count = array(
                'generic' => array(
                    'synonym' => 0,
                    'typo'    => 0,
                ),
                'brand' => array(
                    'synonym' => 0,
                    'typo'    => 0,
                ),
            );
        }

        // gather data each type, list of IDS
        foreach ($data as $type => $sets) {
            // each type-set
            foreach ($sets as $drug_id => $status) {
                // in collective ids
                $family_ids = array();
                if (0 == $status) {
                    // get family info
                    $family = $this->queryDrugFamily($type, $drug_id, true, false);
                    if (empty($family)) {
                        // @codeCoverageIgnoreStart
                        // falback safe: discard futher process
                        continue;
                        // @codeCoverageIgnoreEnd
                    }

                    // loop thru for $data_sets_keys
                    foreach ($family as $family_type => $family_set) {
                        foreach ($family_set as $fs_type => $fs_id) {
                            if (empty($fs_id)) {
                                // overwrite, null
                                $family_set[$fs_type] = 0;
                                // done
                                continue;
                            }
                            // only with value
                            if ($complete) {
                                if (in_array($fs_type, array('generic', 'brand'))) {
                                    // just collect for now
                                    if (!isset($family_ids[$fs_type])) {
                                        $family_ids[$fs_type] = array();
                                    }
                                    $family_ids[$fs_type][] = $fs_id;
                                }
                            }
                        }

                        // gather keys here; collate; NO DUPLICATION
                        $key = implode('|', $family_set);
                        // set as data_set for next process
                        $data_sets_keys[$key] = 0;
                    }

                    // register for max from family data
                    if ($complete) {
                        foreach (array('generic', 'brand') as $var_type) {
                            foreach (array('synonym', 'typo') as $sub_type) {
                                if (!empty($family_ids[$var_type])) {
                                    $keyword_max = $this->getKeywordByParentAggregate($var_type, $sub_type, $family_ids[$var_type], 'max');
                                    $max_keyword_count[$var_type][$sub_type] = max(array(
                                        $max_keyword_count[$var_type][$sub_type],
                                        $keyword_max,
                                    ));
                                }
                            }
                        }
                    }
                }
            }

            unset($data[$type]);
        }

        $this->csvExportFile = new ExportCsv($csvPrefix);
        $this->csvExportFile->writeRowCells(
            DRHelpers::prepareExportHeadings($complete, $max_keyword_count)
        );

        $prev_experimental = false;
        $prev_generic      = false;
        $prev_row          = false;

        foreach ($data_sets_keys as $row => $stat) {
            $drugs = DRHelpers::readDataSetKeys($row);

            // reach row drugs
            foreach ($drugs as $type => $id) {
                if (!empty($id)) {
                    // may have from previous run
                    $prev = "prev_{$type}";
                    if (
                        (!empty($$prev)) && (
                            // base on structure
                            (
                                ($complete) &&
                                ($$prev['main']->getId() == $id)
                            ) || (
                                (!$complete) &&
                                (is_object($$prev)) &&
                                ($$prev->getId() == $id)
                            )
                        )
                    ) {
                        // fallback safe: reapply
                        // @codeCoverageIgnoreStart
                        $drugs[$type] = $$prev;
                        // @codeCoverageIgnoreEnd
                    }
                    else {
                        $repo = $this->_getDrugRepo($type);
                        if ($complete) {
                            $drugs[$type] = $$prev = $repo->getDrugFamily($id, 'id');
                        }
                        else {
                            $drugs[$type] = $$prev = $repo->getDrug($id, 'id');
                        }
                    }
                }
            }

            if ($complete) {
                $this->csvExportFile->writeRowCells(
                    DRHelpers::downloadWriteDrugCollumnsForComplete($drugs, $max_keyword_count)
                );
            }
            else {
                $this->csvExportFile->writeRowCells(
                    DRHelpers::downloadWriteDrugCollumnsForLess($drugs)
                );
            }

            unset($drugs);
            unset($data_sets_keys[$row]);
        }

        $this->csvExportFile->closeFile();

        return $this->csvExportFile->getFilePath();
    }

    /**
     * @codeCoverageIgnore This is not use at this stage
     *
     * Method to query several-type-of-output Keywords query.
     *
     * @param string            $type       Expect: 'generic' or 'brand' only
     * @param string            $keyword    Expect: 'synonym' or 'typo' only
     * @param array|integer     $ids        Parent ($type) IDs
     * @param boolean           $count      - false*: generate Array of keyword 'id', type 'id' and 'name'
     *                                      - true: generate Array of count of keywords by type 'id'
     *
     * @throws \Exception   If required item not meet expected criteria
     *
     * @return array
     */
    private function getKeywordByParent(string $type, string $keyword, $ids, bool $count = false): array
    {
        if (!in_array($type, array('generic', 'brand'), true)) {
            throw new \Exception("Unknown Type: `{$type}`.");
        }
        if (!in_array($keyword, array('synonym', 'typo'), true)) {
            throw new \Exception("Unknown Keyword: `{$keyword}`.");
        }
        if (empty($ids)) {
            throw new \Exception('Empty IDs.');
        }

        $ids = (array) $ids;
        if (1 == count($ids)) {
            $ids = end($ids);
            $ids = "= {$ids}";
        }
        else {
            $ids = implode(',', $ids);
            $ids = "IN ({$ids})";
        }

        if ($count) {
            $sql = "SELECT COUNT(*) AS `count`, `{$type}` FROM `{$type}_{$keyword}` WHERE `{$type}` {$ids} GROUP BY `{$type}`;";
        }
        else {
            $sql = "SELECT * FROM `{$type}_{$keyword}` WHERE `{$type}` {$ids};";
        }

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Method to query several-type-of-output Keywords query.
     *
     * @param string    $type       Expect: 'generic' or 'brand' only
     * @param string    $keyword    Expect: 'synonym' or 'typo' only
     * @param array     $ids        Parent ($type) IDs
     * @param string    $output     Expect: 'min', 'max', 'sum'
     *                              - min: generate Integer of Minimum keywords count
     *                              - max: generate Integer of Maximum keywords count
     *                              - sum: generate Integer of Sum of keywords count
     *
     * @throws \Exception   If required item not meet expected criteria
     *
     * @return int
     */
    private function getKeywordByParentAggregate(string $type, string $keyword, array $ids, string $output): int
    {
        // @codeCoverageIgnoreStart
        // fallback safe: Method changed to Private use only, should be predefined
        if (!in_array($type, array('generic', 'brand'), true)) {
            throw new \Exception("Unknown Type: `{$type}`.");
        }
        if (!in_array($keyword, array('synonym', 'typo'), true)) {
            throw new \Exception("Unknown Keyword: `{$keyword}`.");
        }
        if (!in_array($output, array('min', 'max', 'sum'), true)) {
            throw new \Exception("Unknown Output: `{$output}`.");
        }
        if (empty($ids)) {
            throw new \Exception('Empty IDs.');
        }
        // @codeCoverageIgnoreEnd

        if (1 == count($ids)) {
            $ids = end($ids);
            $ids = "= {$ids}";
        }
        else {
            // @codeCoverageIgnoreStart
            $ids = implode(',', $ids);
            $ids = "IN ({$ids})";
            // @codeCoverageIgnoreEnd
        }

        $func = strtoupper($output);
        $sql  = array(
            "SELECT {$func}(`q`.`count`) AS `{$output}` FROM (",
                "SELECT COUNT(*) AS `count`, `{$type}`",
                "FROM `{$type}_{$keyword}` WHERE `{$type}` {$ids}",
                "GROUP BY `{$type}`",
            ') `q`',
        );

        $sql  = implode(' ', $sql);
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get corresponding Repository to Drug Entity.
     *
     * @param string    $entity     Expects 'experimental', 'generic', 'brand'
     *
     * @return ObjectRepository
     */
    private function _getDrugRepo(string $entity): ObjectRepository
    {
        $entity = strtolower($entity);

        switch ($entity) {
            case 'experimental':
                $repo = $this->getEntityManager()->getRepository(Experimental::class);
                break;

            case 'generic':
                $repo = $this->getEntityManager()->getRepository(Generic::class);
                break;

            case 'brand':
                $repo = $this->getEntityManager()->getRepository(Brand::class);
                break;

            // @codeCoverageIgnoreStart
            case 'any':
            default:
                $repo = $this->getEntityManager()->getRepository(Experimental::class);
                break;
            // @codeCoverageIgnoreEnd
        }

        return $repo;
    }
}
