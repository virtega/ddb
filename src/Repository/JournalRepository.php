<?php

namespace App\Repository;

use App\Entity\Country;
use App\Entity\CountryRegions;
use App\Entity\CountryTaxonomy;
use App\Entity\Journal;
use App\Utility\Helpers as H;
use App\Utility\Repository\DrugRepositoryHelpers;
use Doctrine\ORM\EntityRepository;

/**
 * Journal Repository.
 *
 * @since  1.2.0
 */
class JournalRepository extends EntityRepository
{
    /**
     * Defaulting search WHERE condition.
     *
     * @var string
     */
    private $searchConditionBinder = 'AND';

    /**
     * Method to change search binder
     *
     * @param string $binder Expecting 'AND' / 'OR' only.
     *
     * @throws \Exception  If other value given
     */
    public function changeSearchConditionsBinder(?string $binder = 'AND'): void
    {
        $binder = $binder ?? 'AND';

        if (!in_array(strtolower($binder), ['and', 'or'])) {
            throw new \Exception('Invalid search binder.');
        }

        $this->searchConditionBinder = $binder;
    }

    /**
     * Method to handle search query.
     *
     * @param  array  $terms
     * @param  int    $limit
     * @param  int    $offset
     *
     * @return Journal[]
     */
    public function search(array $terms, int $limit = 1000, int $offset = 0): array
    {
        $sql = $this->buildSearchQuery($terms, false, $limit, $offset);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        $ids = $stmt->fetchAll();
        $ids = array_column($ids, 'id');

        return $this->findBy(['id' => $ids], ['name' => 'ASC']);
    }

    /**
     * Method to check search query result count
     *
     * @param  array  $terms
     *
     * @return int
     */
    public function searchResultCount(array $terms): int
    {
        $sql = $this->buildSearchQuery($terms, true);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to count Journal records count.
     *
     * @return int
     */
    public function countOverallCount(): int
    {
        $sql = 'SELECT COUNT(*) AS `count` FROM `journal`;';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to count Corrupted Journal records count.
     *
     * @return int
     */
    public function countCorruptedCount(): int
    {
        $sql = 'SELECT COUNT(*) AS `count` FROM `journal` WHERE '
            . '`name` IS NULL OR `name` = "" '
            . ' OR '
            . '`abbreviation` IS NULL OR `abbreviation` = "" ';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to calculate int-indexed Journal records count.
     *
     * @param string $field
     * @param int    $value
     *
     * @return int
     */
    public function countKeyedCount(string $field, int $value): int
    {
        $sql = 'SELECT COUNT(*) AS `count` FROM `journal` WHERE '
            . "`{$field}` = {$value} ";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get Maximum Relationship count of Journal's Specialty
     *
     * @return int
     */
    public function countMaxSpecialtiesConnection(): int
    {
        $sql = 'SELECT COUNT(*) AS `count` FROM `journal_specialties` GROUP BY `journal_id` ORDER BY `count` DESC LIMIT 1;';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * Method to get a Journal Country if attached on Details
     *
     * @param Journal $journal
     *
     * @return Country|null
     */
    public function getJournalDetailsCountry(Journal $journal): ?Country
    {
        $country = null;

        if (!empty($journal->getDetailsCountry())) {
            /**
             * @var \App\Repository\CountryRepository<Country>
             */
            $countryRepo = $this->getEntityManager()->getRepository(Country::class);

            /**
             * Country|null
             */
            $country = $countryRepo->findOneBy(['isoCode' => $journal->getDetailsCountry()]);
        }

        return $country;
    }

    /**
     * Method to get a Journal CountryTaxonomy if attached on Details
     *
     * @param Journal $journal
     *
     * @return CountryTaxonomy|null
     */
    public function getJournalDetailsCountryTaxonomy(Journal $journal): ?CountryTaxonomy
    {
        $countryTaxonomy = null;

        if (!empty($journal->getDetailsRegion())) {
            /**
             * @var \App\Repository\CountryTaxonomyRepository<CountryTaxonomy>
             */
            $countryTaxRepo = $this->getEntityManager()->getRepository(CountryTaxonomy::class);

            /**
             * CountryTaxonomy|null
             */
            $region = $countryTaxRepo->findOneBy(['id' => $journal->getDetailsRegion()]);

            if (!empty($region)) {
                $countryTaxonomy = $region;
            }
        }

        return $countryTaxonomy;
    }

    /**
     * Method to build Journal search SQL Query.
     * Query should return relevant Journal IDs.
     *
     * @uses  $this->searchConditionBinder  to concatenate search conditions
     *
     * @param  array    $terms      note by-reference
     * @param  boolean  $countOnly  select count(), instead of ID
     * @param  int      $limit
     * @param  int      $offset
     *
     * @return string
     */
    private function buildSearchQuery(array &$terms, bool $countOnly = false, int $limit = 1, int $offset = 0): string
    {
        $terms = [
            'name'               => $terms['name']               ?? '',
            'volume'             => $terms['volume']             ?? '',
            'status'             => $terms['status']             ?? '',
            'issn'               => $terms['issn']               ?? '',
            'specialty'          => $terms['specialty']          ?? [],
            'ids'                => $terms['ids']                ?? '',
            'ismedline'          => $terms['ismedline']          ?? '',
            'isdgabstract'       => $terms['isdgabstract']       ?? '',
            'issecondline'       => $terms['issecondline']       ?? '',
            'notbeingupdated'    => $terms['notbeingupdated']    ?? '',
            'drugbeingmonitored' => $terms['drugbeingmonitored'] ?? '',
            'globaledition'      => $terms['globaledition']      ?? '',
            'deleted'            => $terms['deleted']            ?? '',
            'showcorrupted'      => $terms['showcorrupted']      ?? '',
        ];

        if ($countOnly) {
            $sql = "SELECT COUNT(DISTINCT(`j`.`id`)) AS `count` FROM `journal` `j` ";
        }
        else {
            $sql = "SELECT DISTINCT(`j`.`id`) FROM `journal` `j` ";
        }

        $andWhere = [];
        if (!empty($term = $terms['name'])) {
            DrugRepositoryHelpers::translateQueryToSql($term);
            $sub = [
                "(`j`.`name` {$term})",
                "(`j`.`abbreviation` {$term})"
            ];
            $andWhere[] = '(' . implode(' OR ', $sub) . ')';
        }

        if (!empty($term = $terms['volume'])) {
            DrugRepositoryHelpers::translateQueryToSql($term);
            $sub = [
                "(`j`.`volume` {$term})",
                "(`j`.`number` {$term})",
            ];
            $andWhere[] = '(' . implode(' OR ', $sub) . ')';
        }

        if ('' !== $terms['status']) {
            $andWhere[] = "(`j`.`status` = {$terms['status']})";
        }

        if (!empty($term = $terms['issn'])) {
            DrugRepositoryHelpers::translateQueryToSql($term);
            $andWhere[] = "(`j`.`medline_issn` {$term})";
        }

        if (!empty($term = $terms['ids'])) {
            DrugRepositoryHelpers::translateQueryToSql($term);
            $sub = [
                "(`j`.`id` {$term})",
                "(`j`.`unid` {$term})",
            ];
            $andWhere[] = '(' . implode(' OR ', $sub) . ')';
        }

        if ($terms['ismedline'] != '') {
            $term = filter_var($terms['ismedline'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`j`.`is_medline` = {$term})";
        }

        if ($terms['isdgabstract'] != '') {
            $term = filter_var($terms['isdgabstract'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`j`.`is_dgabstract` = {$term})";
        }

        if ($terms['issecondline'] != '') {
            $term = filter_var($terms['issecondline'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`j`.`is_second_line` = {$term})";
        }

        if ($terms['notbeingupdated'] != '') {
            $term = filter_var($terms['notbeingupdated'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`j`.`not_being_updated` = {$term})";
        }

        if ($terms['drugbeingmonitored'] != '') {
            $term = filter_var($terms['drugbeingmonitored'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`j`.`drugs_monitored` = {$term})";
        }

        if ($terms['globaledition'] != '') {
            $term = filter_var($terms['globaledition'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`j`.`global_edition_journal` = {$term})";
        }

        if ($terms['deleted'] != '') {
            $term = filter_var($terms['deleted'], FILTER_VALIDATE_BOOLEAN);
            $term = ($term ? 1 : 0);
            $andWhere[] = "(`j`.`deleted` = {$term})";
        }

        if ($terms['showcorrupted'] != '') {
            $term = filter_var($terms['showcorrupted'], FILTER_VALIDATE_BOOLEAN);
            if (false == $term) {
                $andWhere[] = "(`j`.`name` IS NOT NULL)";
                $andWhere[] = "(`j`.`name` !=  \"\")";
            }
        }

        if (!empty($terms['specialty'])) {
            $ids = $names = [];
            foreach ($terms['specialty'] as $term) {
                if (is_numeric($term)) {
                    $ids[] = $term;
                }
                else {
                    $names[] = $term;
                }
            }

            if (!empty($ids)) {
                foreach ($ids as $idx => $id) {
                    $sql .= "
                        INNER JOIN
                            `journal_specialties` `jsid{$idx}`
                        ON (
                            `jsid{$idx}`.`journal_id` = `j`.`id`
                            AND
                            `jsid{$idx}`.`specialty_id` = {$id}
                        )
                    ";
                }
            }

            if (!empty($names)) {
                $st = [];
                foreach ($names as $name) {
                    $st[] = "(`st`.`specialty` LIKE \"%{$name}%\")";
                }
                $st = '((`st`.`id` = `js`.`specialty_id`) AND (' . implode(' OR ', $st) . '))';

                $sql .= "
                    INNER JOIN `journal_specialties` `js` ON `js`.`journal_id` = `j`.`id`
                    INNER JOIN `specialty_taxonomy` `st` ON {$st}
                ";
            }
        }

        $limiter = '';
        if (false == $countOnly) {
            $limiter = " LIMIT {$limit} OFFSET {$offset} ";
        }

        if (!empty($andWhere)) {
            $andWhere = ' WHERE ' . implode(' ' . $this->searchConditionBinder . ' ', $andWhere);
        }
        else {
            $andWhere = '';
        }

        $sql = H::removeExcessWhitespace($sql . $andWhere . $limiter);

        return $sql;
    }
}
