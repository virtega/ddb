<?php

namespace App\Repository;

use App\Entity\Country;
use Doctrine\ORM\EntityRepository;

/**
 * Country Extended Entity Repository.
 *
 * @since  1.0.0
 */
class CountryRepository extends EntityRepository
{
    /**
     * Method to return list of Country.
     *
     * @param array $orderBy
     *
     * @return array
     */
    public function getCountryList(array $orderBy = ['name' => 'ASC']): array
    {
        $countries = $this->findBy(array(), $orderBy);

        return $countries;
    }

    /**
     * Method to query Capital Cities name unique from Country.
     * Note: Country may share same City Name.
     *
     * @return string[]
     */
    public function getDistictCapitalCities(): array
    {
        static $cities;

        if (empty($cities)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qr = $qb->select(['c.capitalCity'])
                ->from(Country::class, 'c')
                ->distinct(true)
                ->orderBy('c.capitalCity', 'ASC')
                ->where( $qb->expr()->isNotNull('c.capitalCity') )
                ->getQuery();

            $cities = $qr->getResult();
            $cities = array_column($cities, 'capitalCity');
        }

        return $cities;
    }

    /**
     * Method to return list of Group Country.
     *
     * @return array
     */
    public function getCountryGroupList(): array
    {
        static $groupCountries;

        if (empty($groupCountries)) {
            $groupCountries = array();

            // query
            $raw_countries = $this->getCountryList();

            // classified them
            foreach ($raw_countries as $country) {
                $set = 'Others';
                if (in_array($country->getIsoCode(), array('DE', 'ES', 'FR', 'GB', 'IT'))) {
                    $set = 'EU5';
                }
                elseif ('US' == $country->getIsoCode()) {
                    $set = 'US';
                }

                $groupCountries[$set][] = array(
                    'iso_code' => $country->getIsoCode(),
                    'name'     => $country->getName(),
                );
            }
        }

        return $groupCountries;
    }
}
