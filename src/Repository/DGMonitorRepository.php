<?php

namespace App\Repository;

use App\Entity\DGmonitorStats;
use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use PDO;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DGMonitorRepository extends ServiceEntityRepository
{
    public const STATS_SIZE = 100;

    /**
     * @var string
     */
    public $tableName;
    /**
     * DGMonitorRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DGmonitorStats::class);
        $this->tableName = $this->getClassMetadata()->getTableName();
    }

    /**
     * @param string    $date
     * @param array     $ids
     *
     * @return bool
     */
    public function bulkInsertIds(string $date, array $ids): bool
    {
        $inserts = [];
        foreach ($ids as $id) {
            $tmp = [
                "'{$date}'",
                $id['id'],
                $this->getEntityManager()->getConnection()->quote($id['title'], PDO::PARAM_STR),
            ];
            $tmp = implode(', ', $tmp);
            $inserts[] = "({$tmp})";
        }
        $inserts = implode(', ', $inserts);

        $sql = 'INSERT INTO `' . $this->tableName . '` (`date`, `fb_id`, `title`) VALUES ' . $inserts . ';';

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);

        return $stmt->execute();
    }

    /**
     * @return array
     */
    public function getDGMonitorStats(): array
    {
        $fields = ['s.date', 's.title'];
        $query = $this->createQueryBuilder('s')
            ->select($fields)
            ->orderBy('s.date', 'DESC');
        $query->setMaxResults(self::STATS_SIZE * 100);
        $result = $query->getQuery()->getResult();

        return $result;
    }

    /**
     * @param string $date
     *
     * @return mixed
     */
    public function delete(string $date)
    {
        $query = $this->createQueryBuilder('s')
            ->delete()
            ->where('s.date = :date')
            ->setParameter('date', $date);

        $result = $query->getQuery()->execute();

        return $result;
    }
}
