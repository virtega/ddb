/*
===================================
= WEBPACK ENCORE DISABLED.
===================================
- Issue found on several required plugins, as it did not works well with WebPack Packaging.
- See these same setup under GruntJs instead, gruntfile.js
- Twig/Assetic still expects manifest.json, this will also provided by Grunt.



var Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');
Encore
    .setOutputPath('public/build/')

    .setPublicPath('/build')
    .addPlugin(new CopyWebpackPlugin([
            { from: './assets/img', to: 'img' }
    ]))

    .addEntry('base-loader', './assets/js/base-loader.js')
    .addEntry('bare-loader', './assets/js/bare-loader.js')
    .autoProvidejQuery()
    .enableSourceMaps(!Encore.isProduction())

    .enableVersioning()

    .cleanupOutputBeforeBuild()
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        'window.$': 'jquery'
    })
;

module.exports = Encore.getWebpackConfig();
*/