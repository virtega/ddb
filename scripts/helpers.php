<?php
/**
 * DrugDb: EXTERNAL SCRIPT HELPERS.
 */
function outprint($prefix = '', $data = false, $count = false, $prefix_ext = false)
{
    if (!empty($prefix_ext)) {
        $prefix = str_pad($prefix, $prefix_ext, ' ', STR_PAD_RIGHT);
    }
    $prefix = str_pad($prefix, 50, ' ', STR_PAD_LEFT);

    if (false === $data) {
        $data = '';
    }
    else {
        if ($count) {
            $data = count($data);
        }

        if (is_numeric($data)) {
            $data = number_format($data, 0);
        }

        $data = str_pad($data, 12, ' ', STR_PAD_LEFT);

        if (false !== strpos($data, '.')) {
            $data = str_repeat(' ', 3) . $data;
        }

        if (false !== strpos($data, '%')) {
            $data = str_repeat(' ', 1) . $data;
        }
        $data = ' : ' . $data;
    }

    global $config;
    $time = abs(microtime(true) - $config->time_start);
    $time = number_format($time, 1);
    $time = str_pad("{$time}", 6, ' ', STR_PAD_LEFT);
    print_r("[{$time}s]" . $prefix . $data . PHP_EOL);
}

function log_folder($path)
{
    $path = rtrim($path, '/');

    if (is_dir($path)) {
        $files = glob($path . '/*');
        foreach ($files as $file) {
            unlink($file);
        }
        rmdir($path);
    }

    mkdir($path);
}
