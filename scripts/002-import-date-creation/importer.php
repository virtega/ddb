<?php
(PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) && die('ERROR: CLI only.');

/**
 * External Script to Import dates from data.txt, inject directly to database.
 * Drug will be search by name first, found match will be updated.
 *
 * @since  1.0.0
 */

// get configuration file
$config = __DIR__ . '/../config.php';
if (!file_exists($config)) {
    die('ERROR: Missing Configuration File.');
}
include_once $config;

$import = new importer($config);
$import->execute();

class importer
{
    private $config;
    private $db;
    private $data_file;
    private $data_array   = array();
    private $drug_records = array();
    private $stats        = array();

    private static $main_tables = array(
        'experimental',
        'generic',
        'brand',
    );

    private static $main_fields = array(
        'brandid',
        'genericid',
        'unid',
    );

    private static $db_update_chunk = 5000;

    public function __construct($config)
    {
        $this->config = clone $config;

        // log folder
        $this->config->log = __DIR__ . '/logs';
        log_folder($this->config->log);
        outprint('Config Loaded...', false, false, 40);
    }

    /**
     * Process Kick off.
     */
    public function execute()
    {
        // BEGINS
        outprint('== SCRIPT BEGINS ======================>', false);

        $this->dbStart();

        $this->loadData();

        $this->removeDuplicateData();

        // TODO: REVIEW IF DB uid IS CLASHING
        outprint('== DB QUERY DUPLICATES UID (skipped) ==>', false);
        // $this->removeDuplicateUNID();

        // get latest count
        $this->stats['Usable Data Record Count']      = count($this->drug_records);
        $this->stats['Usable Data Record Percentage'] = number_format(
            (($this->stats['Usable Data Record Count'] / $this->stats['Data Record Count']) * 100), 2
        ) . '%';

        // using EQUAL unid
        $missed_records = $this->processingUpdateInQueue('unid', $this->drug_records, '03-not-applied-uid.txt');

        // using LIKE name
        $missed_records = (array) $missed_records;
        $missed_records = $this->processingUpdateInQueue('name', $missed_records, '04-not-applied-name.txt');

        // final status
        outprint('== FINAL STATUS =======================>', false);
        if (isset($this->stats['Used Record'])) {
            $this->stats['Used Record Percentage'] = number_format(
                (($this->stats['Used Record'] / $this->stats['Usable Data Record Count']) * 100), 2) . '%';
            }

            if (isset($this->stats['Updated Db Drugs Count'])) {
                $this->stats['Updated Db Drugs Percentage'] = number_format(
                    (($this->stats['Updated Db Drugs Count'] / $this->stats['Usable Data Record Count']) * 100), 2
                ) . '%';

                // set on db option
                $this->db->query('INSERT INTO `variable` (`name`, `type`, `value`) VALUES
                    (
                        "ext-script-import-creation-date-last",
                        "datetime",
                        "' . date('Y-m-d H:i:s') . '"
                    ),
                    (
                        "ext-script-import-creation-date-count",
                        "integer",
                        "' . $this->stats['Updated Db Drugs Count'] . '"
                    )
                    ON DUPLICATE KEY
                        UPDATE `type`=VALUES(`type`), `value` = VALUES(`value`);'
                );
        }

        foreach ($this->stats as $key => $value) {
            outprint('===> ' . $key, $value, false, 35);
        }

        $this->dbStop();

        // ENDS
        outprint('== SCRIPT ENDS ========================>', false);
    }

    /**
     * Method to Initiate Database Connection.
     */
    private function dbStart()
    {
        // create db connections
        $this->db = new mysqli(
        $this->config->dbhost,
        $this->config->dbuser,
        $this->config->dbpass,
        $this->config->dbname
    );

        if ($this->db->connect_error) {
            outprint('ERROR: Connection failed; ' . $this->db->connect_error);
            exit(1);
        }

        outprint('DB Connected...', false, false, 40);

        // optimize table
        outprint('Db Optimization...', false, false, 40);
        foreach (self::$main_tables as $table) {
            $this->db->query("ALTER TABLE `{$table}` ADD INDEX(`uid`);");
            $this->db->query("ALTER TABLE `{$table}` ADD INDEX(`name`);");
            $this->db->query("OPTIMIZE TABLE `{$table}`;");
        }
    }

    /**
     * Method to Disconnect Database Connection.
     */
    private function dbStop()
    {
        if ($this->db) {
            // de-optimize table
            outprint('Db De-Optimization...', false, false, 40);
            foreach (self::$main_tables as $table) {
                $this->db->query("ALTER TABLE `{$table}` DROP INDEX `uid`;");
                $this->db->query("ALTER TABLE `{$table}` DROP INDEX `name`;");
                $this->db->query("OPTIMIZE TABLE `{$table}`;");
            }

            // close db
            $this->db->close();

            $this->db = null;
        }
        outprint('DB Closed...', false, false, 40);
    }

    /**
     * Method to Extract File into Working Data.
     */
    private function loadData()
    {
        // check required file
        $this->data_file = __DIR__ . '/data.txt';
        if (!file_exists($this->data_file)) {
            outprint('ERROR: Missing Data File.');
            exit(1);
        }

        // read file
        $this->data_file  = file_get_contents($this->data_file);
        $this->data_array = array();
        preg_match_all('/(.{38})(.{20})(.{20})(.{62})(.*$)/m', $this->data_file, $this->data_array);

        $count = (count($this->data_array[0]) - 1);
        outprint('ORIGINAL DATA SIZE', $count);
        $this->stats['Data Record Count'] = $count;
    }

    /**
     * Method to remove Duplicates in the Working Data.
     */
    private function removeDuplicateData()
    {
        $this->drug_records = array();
        $duplicated_names   = array();
        foreach ($this->data_array[0] as $index => $line) {
            // csv heading
            if ($index > 0) {
                // unpack date
                $date = DateTime::createFromFormat('d/m/Y h:i:s A', trim($this->data_array[1][$index]));
                // check date if readable
                if (empty($date)) {
                    continue;
                }

                $set = array(
                    'name'      => trim($this->data_array[5][$index]),
                    'brandid'   => trim($this->data_array[2][$index]),
                    'genericid' => trim($this->data_array[3][$index]),
                    'unid'      => trim($this->data_array[4][$index]),
                    'date'      => $date->format('Y-m-d H:i:s'),
                    'ori-index' => $index,
                    'found'     => array(),
                );

                $hash = md5(
                    $set['brandid']
                    . $set['genericid']
                    . $set['unid']
                );

                // remove & ignore duplicated
                if (
                    (isset($this->drug_records[$hash])) ||
                    (isset($duplicated_names[$hash]))
                ) {
                    if (!isset($duplicated_names[$hash])) {
                        $duplicated_names[$hash]   = array();
                        $duplicated_names[$hash][] = $this->data_array[0][$this->drug_records[$hash]['ori-index']];
                    }
                    $duplicated_names[$hash][] = $line;
                    if (isset($this->drug_records[$hash])) {
                        unset($this->drug_records[$hash]);
                    }
                    continue;
                }

                $this->drug_records[$hash] = $set;
            }
        }

        $dup = 0;
        foreach ($duplicated_names as $hash => $found) {
            foreach ($found as $f) {
                file_put_contents($this->config->log . '/01-duplicated.txt', $f . "\r\n", FILE_APPEND);
                ++$dup;
            }
        }
        outprint('Duplicated Drugs (01-duplicated.txt)', $dup);
        outprint('Unique Drugs', $this->drug_records, true);
    }

    /**
     * Method to remove Drugs in Db which duplicated by UNID.
     */
    private function removeDuplicateUNID()
    {
        outprint('== DB QUERY DUPLICATES UID ============>', false);

        $matched_duplicated_key = 0;
        foreach (self::$main_tables as $type) {
            $res = $this->db->query("SELECT `uid`, COUNT(*) as `count` FROM `{$type}` GROUP BY `uid` HAVING `count` > 1;");

            if ($res->num_rows > 0) {
                while ($row = $res->fetch_assoc()) {
                    foreach ($this->drug_records as $hash => $drug_set) {
                        foreach (self::$main_fields as $field) {
                            if ($drug_set[$field] == $row['uid']) {
                                $typestr = str_pad("[{$type}]", 15, ' ', STR_PAD_RIGHT);
                                file_put_contents(
                                    $this->config->log . '/02-matched-duplicated-uid.txt',
                                    $typestr . $this->data_array[0][$drug_set['ori-index']] . "\r\n",
                                    FILE_APPEND
                                );
                                ++$matched_duplicated_key;
                                unset($this->drug_records[$hash]);
                            }
                        }
                    }
                }
            }
        }

        outprint('FOUND MULTIPLES (02-matched-duplicated-uid.txt)', $matched_duplicated_key);
        outprint('UNIQUE DRUGS', $this->drug_records, true);
    }

    /**
     * Method to check, and push changes to DrugDb.
     *
     * @param string $type       Type of request, expect "unid" & "name" ony
     * @param array  $records    List of Drug Records to Process
     * @param string $missed_log Relative Path to Log file, for untraced Drug
     *
     * @return array List of Drug which did not fount & synced
     */
    private function processingUpdateInQueue($type, $records, $missed_log)
    {
        $queue_list = array_chunk($records, self::$db_update_chunk);

        outprint('== DB MASS UPDATE (' . $type . ') ==============>', false);

        if (empty($queue_list)) {
            outprint('>> Skipped', false);
        }

        $updated = array_combine(
            self::$main_tables,
            array_fill(0, count(self::$main_tables), 0)
        );
        $updated_records = 0;
        $missed_records  = array();

        foreach ($queue_list as $q_index => $queue) {
            $updated_on_queue      = 0;
            $updated_queue_records = 0;

            outprint('Updating... '
                . number_format($q_index + 1, 0)
                . ' / '
                . number_format(count($queue_list), 0),
                false
            );

            foreach ($queue as $hash => $drug_set) {
                $updated_drug_set = false;

                if ('unid' == $type) {
                    foreach (self::$main_fields as $field) {
                        if (
                            (!empty($drug_set[$field])) &&
                            (!empty($drug_set['date']))
                        ) {
                            foreach (self::$main_tables as $table) {
                                $res = $this->db->query("UPDATE `{$table}` SET `creation_date` = '{$drug_set['date']}' WHERE `uid` = '{$drug_set[$field]}';");
                                if ($res && $this->db->affected_rows) {
                                    $updated_drug_set = true;
                                    ++$updated_on_queue;
                                    $updated[$table] += $this->db->affected_rows;
                                }
                            }
                        }
                    }
                }
                elseif ('name' == $type) {
                    if (
                        (!empty($drug_set['name'])) &&
                        (!empty($drug_set['date']))
                    ) {
                        foreach (self::$main_tables as $table) {
                            $res = $this->db->query("UPDATE `{$table}` SET `creation_date` = '{$drug_set['date']}' WHERE `name` LIKE '{$drug_set['name']}';");
                            if ($res && $this->db->affected_rows) {
                                $updated_drug_set = true;
                                ++$updated_on_queue;
                                $updated[$table] += $this->db->affected_rows;
                            }
                        }
                    }
                }
                else {
                    die('Unknown Type: ' . $type);
                }

                if (!$updated_drug_set) {
                    $missed_records[] = $drug_set;
                    file_put_contents($this->config->log . '/' . $missed_log, $this->data_array[0][$drug_set['ori-index']] . "\r\n", FILE_APPEND);
                }
                else {
                    ++$updated_queue_records;
                    if (!isset($this->stats['Used Record'])) {
                        $this->stats['Used Record'] = 0;
                        $this->stats['Used Record Percentage'] = 0;
                    }
                    ++$this->stats['Used Record'];
                }
            }

            outprint('=>  Record', $updated_queue_records);
            outprint('>> Updated', $updated_on_queue);

            $updated_records += $updated_queue_records;
        }

        // status
        outprint('== UPDATED DB RECORDS (' . $type . ') ==========>', false);
        foreach ($updated as $table => $count) {
            outprint('===> Updated ' . ucwords($table), $count, false, 29);
        }
        $sum = array_sum($updated);
        outprint('===> Total Updated Record', $updated_records, false, 35);
        outprint('===> Total Missed Record', $missed_records, true, 35);
        outprint('===> Total Updated Db', $sum, false, 35);

        if (!isset($this->stats['Updated Db Drugs Count'])) {
            $this->stats['Updated Db Drugs Count'] = 0;
            $this->stats['Updated Db Drugs Percentage'] = 0;
        }
        $this->stats['Updated Db Drugs Count'] += $sum;

        return $missed_records;
    }
}
