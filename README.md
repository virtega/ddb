Drug Database
=============
Replacing Lotus Drug Database
-----------------------------
 - Managing Drugs details, includes its relations with Synonyms & Typos.
 - Main Drugs are Experimental -> Generic -> Brand.
 - Other Entities; Journal & Conference (Congress)
 - App Db on remote MsSQL.
 - Initial Drug Sync Down.
 - Continuous Drug Up Sync.
 - Open API for database clients.

Requirements
------------
- **PHP** 7.2
- **Symfony** 4.3.5
- **MySQL** Database (Local)
- MsSQL Database 9 (Remote - Lotus)
- **Composer**
- **Yarn**
 -- *jQuery*, *Bootstrap*, *Material*, see `package.js` & `webpack.config.js` for more details
- *PHP Modules*: **Zip**, **hash**, **mbstring**, **mysqli**, **PDO**+FreeTDS/Sybase, **Reflection** (dev), **session** & other basics
- **UnZip** (or any that capable to unpack ZIP)
- **NPM**, **GruntJs**
- `[DEV]` **PHPUnit**
- `[DEV]` **PHPStan**

* * * * * * * *

Deployment
==========
**Move to Production (MTP) Steps Refer ticket at the following, which covers:**

- Code-base deployment
- Configuration
- UI-base deployment
- One Time Drug Sync-Down (Delta)
- One Time External-Data Sync ("Creation-Date" - Full)

Refers:
- Release 1.0: [DDB-16](https://psl.jira.pslgroup.com/browse/DDB-16)
- Release 1.2: [DDB-32](https://psl.jira.pslgroup.com/browse/DDB-32)

MTP-1: PHP Libraries
------------------------------

[PHP 7.2 ldap](https://centos.pkgs.org/7/ius-archive-x86_64/php72u-ldap-7.2.2-1.ius.centos7.x86_64.rpm.html)
```sh
$ wget https://centos7.iuscommunity.org/ius-release.rpm
$ rpm -Uvh ius-release*rpm
$ yum --enablerepo=ius-archive install php72u-ldap
$ php -m | grep pdo_dblib
```

[PHP 7.2 pdo_dblib](https://centos.pkgs.org/7/ius-archive-x86_64/php72u-pdo-dblib-7.2.1-1.ius.centos7.x86_64.rpm.html)
```sh
$ wget https://centos7.iuscommunity.org/ius-release.rpm
$ rpm -Uvh ius-release*rpm
$ yum --enablerepo=ius-archive install php72u-pdo-dblib
$ php -m | grep pdo_dblib
```

MTP-2: Codebase
------------------------------

```sh
# [OPT] install composer
# $ curl -sS https://getcomposer.org/installer | php
$ sudo mv composer.phar /usr/local/bin/composer

# Get repository
$ git clone https://XXXX.XXXX@stash.pslgroup.com/scm/DDB/ddb.git
$ git checkout -b ...

# Install Supporting Libraries
$ composer install [--no-dev]

# [PROD / STAGE] Create Config File
$ composer dump-env prod
$ cat .env.local.php

# [DEV] Edit Config File - with `composer dev` option
$ composer dump-env test [OR]
$ cp .env .evn.local
$ nano .evn.local
$ composer dump-env dev [OPT]
```

MTP-3: UI-base: Package & Copy Resources under public/build
------------------------------

```sh
# [OPT] install npm globally
# Install NodeJs - https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-a-centos-7-server

$ npm install grunt-cli -g

# Install UI Libraries
$ yarn install --prod [--dev/-D] [--no-bin-links]

# Install font repo via GIT submodule
# - added then; "$ git submodule add https://github.com/JulietaUla/Montserrat.git assets/fonts/montserrat"
# Run the following once local git version pulled
$ git submodule update --init --recursive

# [OPT] Sometimes simply filesystem changed on submodule, lets mute them; not local config only
$ git config diff.ignoreSubmodules dirty

# [PROD / STAGE]
$ grunt production           # carried out tasks basic & safe tasks only

# [DEV] for any changes of CSS/JS
$ grunt genie-clean          # will run all, clean up, copy, and update all
$ grunt watch                # use watch, run on tasks 'clean:build', 'cssmin', 'uglify', 'assets_versioning'

# [DEV] For troubleshooting, enable JS/CSS sourceMap by adding `env` with `"dev"` as option.
# [DEV] For more detail, check `gruntfile.js`, disable "mangle" and enable "beautification" for JS.
$ grunt dev --env=dev
$ grunt watch --env=dev
```

MTP-4: Local Database Implementation
------------------------------

Basic Setup

```sh
# [DEV] Assuming the database need a clean setup.
$ php bin/console doctrine:database:drop --force

# [OPT] Assuming the database is not created yet, and user have the rights to do so.
$ php bin/console doctrine:database:create

# [DEV] Create fresh structure based on ORM.
# Note, Migration 0 provide the structure
$ php bin/console doctrine:schema:create
```

Create Database, Base Structures, Import Countries, and Alteration

```sh
$ php bin/console doctrine:migrations:migrate -n
# [OPT] Recheck if Created DB fit to ORM
$ php bin/console doctrine:schema:validate
```

MTP-5: Remote / Lotus database
------------------------------
See ./.env for require configs which tokens as follows:

|        KEY        |   TYPE  |                DESCRIPTION                 |
|-------------------|---------|--------------------------------------------|
| `LOTUS_DB_HOST`   | string  | CONSIDER Domain NAME `XXXXXX.pslgroup.com` |
| `LOTUS_DB_PORT`   | integer | normally `1433`                            |
| `LOTUS_DB_NAME`   | string  | database name                              |
| `LOTUS_DB_USER`   | string  | MUST Include Domain prefix `PSL\XXXXXX`    |
| `LOTUS_DB_UPSYNC` | boolean | Up Sync feature Kill Switch, default true  |

MTP-6: LDAP Authentication
------------------------------
See ./.env for require configs which tokens as follows:

|        KEY         |   TYPE  |                                              DESCRIPTION                                               |
|--------------------|---------|--------------------------------------------------------------------------------------------------------|
| `LDAP_HOST`        | string  | AD host/ip name without protocol, CONSIDER Domain NAME `XXXXXX.pslgroup.com`                           |
| `LDAP_PORT`        | integer | AD port number - PSL use 389                                                                           |
| `LDAP_ENCRYPT`     | string  | AD protocol - PSL use `none`. Available: [none* / ssl (if host secured "ldaps://") / tls (n/a to PSL)] |
| `LDAP_BASE_DN`     | string  | AD Binding Base DN - General use "DC=pslgroup,DC=com"                                                  |
| `LDAP_EXTRA_QUERY` | string  | AD additional query, may inject special group requirement, use `(objectClass=user)` as default         |
|                    |         | : IE: `(memberOf=CN= ?? ,OU=Applications,OU=Security Groups,OU=PSLIE,DC=pslgroup,DC=com)`              |

MTP-7: Raw Role Assignment
------------------------------

Assigning ANY user with ANY role use Command below;
- PSL username without PREFIX "PSL\". on attribute `sAMAccountName`.
- Available roles

|   KEY   |        PERMISSION       | APP-LEVEL ID |
|---------|-------------------------|--------------|
| admin   | Managing LDAP User Role | ROLE_ADMIN   |
| editor  | Managing Drug CRUD      | ROLE_EDITOR  |
| user    | View Only               | ROLE_USER    |
| visitor | None - just login       | ROLE_VISITOR |

```sh
$ php bin/console app:assign-role <username> <role>
$ php bin/console app:assign-role John.Doe admin     # Assign Admin (ROLE_ADMIN) role to "John.Doe"
$ php bin/console app:assign-role John.Doe user      # Remove User (ROLE_USER) role to "John.Doe"
```


* * * * * * * *


Features
========

F1: Available Commands
-----------------------

```sh
# Import Drugs 
$ php bin/console app:drugs-sync-down --help

# Import Journal 
$ php bin/console app:journals-sync-down --help

# Import Conference
$ php bin/console app:conferences-sync-down --help

# Update Widgets
$ php bin/console app:update-widgets --help

# Drug Data Exports
$ php bin/console app:export-data --help
```

F2: API
-----------------------

> Developer, I use [POSTMAN](https://www.getpostman.com/) for development of API for DrugDb.
> 
> It allow us to share this single file, for reference/changes; simply import the following.

Postman Export file: `./.dev-sources/DrugDb-API-V1.postman_collection.json`

#### F2.1: API Authentication:

- INTERNAL: Session enabled for login form using PSL Official Email Credential.
- EXTERNAL: header key `X-AUTH-TOKEN` or `X-API-TOKEN` with `[*SECURITY_API_TOKEN*]`.
- EXTERNAL: header key `Authentication` or Realm authentication of PSL Official Username (username - with/out PREFIX /email), and Password.

For **HTTP_AUTHENTICATION**, note Apache **might** REMOVE from Header Request. Here is **Apache 2.4 VHOST** suggestion;

```sh
ServerName drugdb.site.domain
DocumentRoot "/drugdb-site-path/public"             # notice ./public
<Directory "/drugdb-site-path/public">              # notice ./public
  Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Options -Indexes +FollowSymLinks
    Require all granted
    <IfModule mod_rewrite.c>
      RewriteEngine On
        RewriteCond %{HTTP:Authorization} ^(.*)     #  add these 2 lines:
        RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]  #  - to allow it to carry forward to application
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^(.*)$ /index.php [QSA,L]
    </IfModule>
</Directory>
```

F3: Crypto Class
-----------------------
See `./.env` for require configs which tokens as follows. These keys should be the same for both Server & Client.

> Try to avoid complex symbol mix, use single-quote; see more on dotenv escape characters.

- `APP_SECRET`         : string: A 30-39 characters
- `SECURITY_API_TOKEN` : string: a 16-32 characters

F4: External Tasks Script
-----------------------
#### F4.1: First Configuration Note:

```sh
# Make Copy of Script Config
$ cp scripts/config.php.dist scripts/config.php
# Add Database connection details
$ nano scripts/config.php
....
# Once complete using the task script, best to remove config
$ rm scripts/config.php
```

#### F4.2: Import Creation Date
This to import Creation Date from Text Base Data:

```sh
$ unzip scripts/002-import-date-creation/data.zip -d scripts/002-import-date-creation
$ php scripts/002-import-date-creation/importer.php
$ rm scripts/002-import-date-creation/data.txt
$ rm -rf scripts/002-import-date-creation/logs
$ php bin/console app:update-widgets
```

F5: Permission Control
-----------------------

|               NAME              |  METHOD  |               PATH              | MIN-PERMISSION |
|---------------------------------|----------|---------------------------------|----------------|
| >>>> API                        |          |                                 |                |
| api_index @ api_hello           | POST/GET | /v1/index @ /v1/hello           | ROLE_VISITOR   |
| api_get_country_list            | GET      | /v1/get-country-list            | ROLE_USER      |
| api_get_country_group_list      | GET      | /v1/get-country-group-list      | ROLE_USER      |
| api_get_countries_regions       | GET      | /v1/get-countries-regions       | ROLE_USER      |
| api_get_a_country_details       | POST     | /v1/get-a-country-details       | ROLE_USER      |
| api_get_a_city_countries        | POST     | /v1/get-a-city-countries        | ROLE_USER      |
| api_get_a_country_regions       | POST     | /v1/get-a-country-regions       | ROLE_USER      |
| api_get_a_region_countries      | POST     | /v1/get-a-region-countries      | ROLE_USER      |
| api_get_drug                    | POST     | /v1/get-drug                    | ROLE_USER      |
| api_get_drug_family             | POST     | /v1/get-drug-family             | ROLE_USER      |
| api_get_drug_entire_family      | POST     | /v1/get-drug-entire-family      | ROLE_USER      |
| api_add_new_drug                | POST     | /v1/add-new-drug                | ROLE_EDITOR    |
| api_delete_drug                 | POST     | /v1/remove-drug                 | ROLE_EDITOR    |
| api_export_name_search          | POST     | /v1/export-name-search          | ROLE_EDITOR    |
| api_export_id_search            | POST     | /v1/export-id-search            | ROLE_EDITOR    |
| api_get_map_drug                | POST     | /v1/get-map-drug                | ROLE_USER      |
| api_add_new_map_drug            | POST     | /v1/add-new-map-drug            | ROLE_EDITOR    |
| api_update_drug_relations       | POST     | /v1/map-update-drug-relations   | ROLE_EDITOR    |
| api_search_by_name              | POST/GET | /v1/search-by-name              | ROLE_USER      |
| api_journal_search_by_name      | POST/GET | /v1/search-journal-by-name      | ROLE_USER      |
| api_conference_search_by_name   | POST/GET | /v1/search-conference-by-name   | ROLE_USER      |
| api_update_widgets              | GET      | /v1/widgets-update              | ROLE_EDITOR    |
| api_user_roles_list             | GET      | /v1/user-roles-list             | ROLE_ADMIN     |
| api_set_user_role               | POST     | /v1/set-user-role               | ROLE_ADMIN     |
| api_remove_user_role            | POST     | /v1/remove-user-role            | ROLE_ADMIN     |
| api_ncbi_search_journal_by_name | POST     | /v1/ncbi-search-journal-by-name | ROLE_USER      |
| api_ncbi_search_journal_by_issn | POST     | /v1/ncbi-search-journal-by-issn | ROLE_USER      |
|                                 |          |                                 |                |
| >>>> LOBBY                      |          |                                 |                |
| root                            | ANY      | /                               | ANONYMOUS      |
| login                           | ANY      | /login                          | ANONYMOUS      |
| maintenance                     | ANY      | /maintenance                    | ANONYMOUS      |
| app_logout                      | ANY      | /logout                         | ANONYMOUS      |
|                                 |          |                                 |                |
| >>>> UI                         |          |                                 |                |
| dashboard                       | ANY      | /dashboard                      | ROLE_VISITOR   |
| drug_register_experimental      | ANY      | /register/experimental          | ROLE_EDITOR    |
| drug_register_generic           | ANY      | /register/generic               | ROLE_EDITOR    |
| drug_register_brand             | ANY      | /register/brand                 | ROLE_EDITOR    |
| drug_edit_experimental          | GET      | /modify/experimental/{id}       | ROLE_USER      |
| drug_edit_experimental          | POST     | /modify/experimental/{id}       | ROLE_EDITOR    |
| drug_edit_generic               | GET      | /modify/generic/{id}            | ROLE_USER      |
| drug_edit_generic               | POST     | /modify/generic/{id}            | ROLE_EDITOR    |
| drug_edit_brand                 | GET      | /modify/brand/{id}              | ROLE_USER      |
| drug_edit_brand                 | POST     | /modify/brand/{id}              | ROLE_EDITOR    |
| drug_open_map                   | ANY      | /drug/map/open                  | ROLE_USER      |
| drug_map                        | ANY      | /drug/map/{type}/{id}           | ROLE_USER      |
| drug_search                     | ANY      | /drug/search                    | ROLE_USER      |
| admin_manage_roles              | ANY      | /admin/manage-roles             | ROLE_ADMIN     |
| conference_search               | ANY      | /conference/search              | ROLE_USER      |
| conference_registry             | ANY      | /conference/registry            | ROLE_EDITOR    |
| conference_modify               | ANY      | /conference/modify/{id}         | ROLE_EDITOR    |
| conference_view                 | ANY      | /conference/view/{id}           | ROLE_USER      |
| conference_remove               | ANY      | /conference/remove/{id}         | ROLE_EDITOR    |
| journal_search                  | ANY      | /journal/search                 | ROLE_USER      |
| journal_registry                | ANY      | /journal/registry               | ROLE_EDITOR    |
| journal_modify                  | ANY      | /journal/modify/{id}            | ROLE_EDITOR    |
| journal_view                    | ANY      | /journal/view/{id}              | ROLE_USER      |
| journal_remove                  | ANY      | /journal/remove/{id}            | ROLE_EDITOR    |

F6: Maintenance Mode
-----------------------

Enabling Maintenance Mode will push all user out to `/maintenance` page.
>> This also will cancel all API request.

However Super Admin still be able to access the site.
If Super need to login, append login URL with "admin" argument.

```sh
# Enable
$ cp ./maintenance.lock.dist ./maintenance.lock

# Example URI for Super Login
# "[URI]/login?admin"

# Disable
$ rm./maintenance.lock
```